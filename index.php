<?php
include "application.php";
db::connect();
db::qry("SET sql_mode = ''");
app::set_default($act, "");

   $ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
else
    $ipaddress = 'UNKNOWN';
app::load_lib('url', 'msg', 'form', 'usrlib', 'file', 'parse');
if (isset($act) && $act=="logout"):
	if(!empty($_SESSION[$app['session']])):
		// print_r($_SESSION[$app['session']]);
		$data = app::unserialize64($_SESSION[$app['session']]);
		// print_r($data);
		// echo "<br><br>";
		// echo "disini";
		// print_r($app['me']);
		// exit;
        // db::ins_call_func("insert_log", $data['id'], "Logout", "logout", $data['id'], "");
        db::ins_call_func("insert_log", $data['id'], "Logout", "logout", "", "",$ipaddress, "");
		$_SESSION[$app['session']] = array();
		unset($_SESSION[$app['session']]);
		msg::set_message('success', app::getliblang('logout'));
		msg::build_msg(1);
	endif;
    header("location: ". $app['webmin']);
	exit;
endif;
if(isset($_SESSION[$app['session']]))
	 header("location: ". $app['webmin'] ."/dashboard.do");
else
	header("location: ". $app['webmin'] ."/auth.do");

$config = db::get_record("configuration", "id", 1);
if (empty($act)):
	form::populate($form);
	if(isset($_SESSION[$app['session']])){
		//header("location: ". $app['webmin'] ."/dashboard.do");
	}else{
		header("location: ". $app['webmin'] ."/auth.do");
	}
	exit;
endif;
header("location: ". $app['www'] ."/auth.do"); 
$config = db::get_record('configuration', 'id', 1);
?>
