<?php 
// require_once "include/DB_Function.php";
// $db = new DB_Function();
// exit;
include "../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib', 'parse','api');
## START #######################################################################
// app::set_default($step, 'browse');
// app::set_default($step, 1);
db::connect();
// isset($_GET['act'])?$step=$_GET['act']:$step="tampil";
isset($step)?$step= $step:$step="tampil";
$app['me'] 		= app::unserialize64($_SESSION[$app['session']]);
$response		= array("error" => FALSE);
function rupiah($angka){
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
}
function format_tgl($date){
$date =explode(" ", $date);
$date_tgl =explode("-", $date[0]);
// $bulan = array (
// 		'Januari',
// 		'Februari',
// 		'Maret',
// 		'April',
// 		'Mei',
// 		'Juni',
// 		'Juli',
// 		'Agustus',
// 		'September',
// 		'Oktober',
// 		'November',
// 		'Desember'
// 	);
$bulan = array (
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'Mei',
		'Jun',
		'Jul',
		'Agu',
		'Sep',
		'Okt',
		'Nov',
		'Des'
	);
// $total = $date_tgl[2]."-".$bulan[$date_tgl[1]-1]."-".$date_tgl[0];
	return $date_tgl[2]." ".$bulan[$date_tgl[1]-1]." ".$date_tgl[0]; 
}
function format_tgl_waktu($date){
$date =explode(" ", $date);
$date_tgl =explode("-", $date[0]);
$bulan = array (
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'Mei',
		'Jun',
		'Jul',
		'Agu',
		'Sep',
		'Okt',
		'Nov',
		'Des'
	);
	return $date_tgl[2]." ".$bulan[$date_tgl[1]-1]." ".$date_tgl[0]." | ".$date[1]; 
}
function format_tgl_bahasa($date,$bahasa){
$date_tgl =explode("-", $date);

if ($bahasa == "id") {
$bulan = array (
		'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
}elseif ($bahasa == "en") {
$bulan = array (
		'January',
		'February',
		'March',
		'April',
		'May',
		'Juni',
		'June',
		'August',
		'September',
		'October',
		'November',
		'December'
	);
}elseif($bahasa == "chn"){
$bulan = array (
		'一月',
		'二月',
		'三月',
		'四月',
		'五月',
		'六月',
		'七月',
		'八月',
		'九月',
		'十月',
		'十一月',
		'十二月'
	);
}
/*$bulan = array (
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'Mei',
		'Jun',
		'Jul',
		'Agu',
		'Sep',
		'Okt',
		'Nov',
		'Des'
	);*/
// $total = $date_tgl[2]."-".$bulan[$date_tgl[1]-1]."-".$date_tgl[0];
	return $date_tgl[2]." / ".$bulan[$date_tgl[1]-1]." / ".$date_tgl[0]; 
	// return $date_tgl[2]." - ".$date_tgl[1]." - ".$date_tgl[0]; 
}
/*if($act=="beacukai"){
	if ($step=="simpan") {
			// isset($_GET['pfpd'])?$pfpd=$_GET['pfpd']:null;
			// isset($_GET['status'])?$status=$_GET['status']:null;
			// isset($_GET['tanggal'])?$tanggal=$_GET['tanggal']:null;
			isset($pfpd)?$pfpd=$pfpd:null;
			isset($status)?$status=$status:null;
			isset($tanggal)?$tanggal=$tanggal:null;
			// $status 		= $_GET['status'];
			// $tanggal 		= $_GET['tanggal'];
			// isset($_GET['tanggal_sppb'])?$tanggal_sppb=$_GET['tanggal_sppb']:null;
			// isset($_GET['date_delivery'])?$date_delivery=$_GET['date_delivery']:null;
			// isset($_GET['create_by'])?$create_by=$_GET['create_by']:null;
			isset($tanggal_sppb)?$tanggal_sppb=$tanggal_sppb:null;
			isset($date_delivery)?$date_delivery=$date_delivery:null;
			isset($create_by)?$create_by=$create_by:null;
			$id 	= rand(1, 100).date("dmYHis");
			$id_n 	= rand(1, 100).date("dmYHis");
					// $user = $db->simpanBeaCukai($status, $tanggal_sppb, $pfpd, $tanggal, $date_delivery, $create_by);
			$sql 	= "INSERT INTO ".$app['table']['beacukai']." 
					   (id,tanggal,id_import,pfpd,tanggal_sppb,date_delivery,created_by,created_at,nomer_sptnp,tgl_sptnp,nilai_sptnp,status) 
					   VALUES 
			 		   ('$id','$tanggal','$id_import','$pfpd','$tanggal_sppb','$date_delivery','$create_by',now(),'$nomer_sptnp','$tgl_sptnp','$nilai_sptnp','$status')";
			// $sql 	= "INSERT INTO ".$app['table']['beacukai']." 
			// 		   (id,status,tanggal,id_import,pfpd,tanggal_sppb,date_delivery,created_by,created_at,nomer_sptnp,tgl_sptnp,nilai_sptnp) 
			// 		   VALUES 
			//  		   ('$id','$status','$tanggal','$id_import','$pfpd','$tanggal_sppb','$date_delivery','$create_by',now(),'$nomer_sptnp','$tgl_sptnp','$nilai_sptnp')";
			$sql2 	= "INSERT INTO ".$app['table']['notif']." 
					   (id,id_rule,id_import,status,pesan,created_by,created_at) 
					   VALUES 
			 		   ('$id_n','5403092018114622','$id_import','1','Beacukai Dengan pfpd = $pfpd','$create_by',now())";
			// $sqlss = "INSERT into ".$app['table']['status']." 
			// 		(id,id_import,positions,created_by,created_at) values
			// 		('$id_n','$id_import','".app::getliblang('proses_beacukai')."','$create_by',now())";
			$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
			db::query($sql_lang, $rs['lang'], $nr['lang']);
			while ($row321	= db::fetch($rs['lang'])) {
				$sqlss = "UPDATE ".$app['table']['status']." set 
						  positions=concat('".app::getliblang('proses_urus_beacukai',$row321['alias']).",','<br>',positions),
						  updated_by = '$create_by',
						  updated_at = now()
					  	  WHERE id_import ='$id_import' lang='".$row321['alias']."' ";
				db::qry($sqlss);
			}
			db::qry($sql);
			db::qry($sql2);
			// db::qry($sqlss);
			$response["error"]	 = FALSE;
			// $response["id"] = $user["unique_id"];
			$response["message"] = "Beacukai Berhasil Tersimpan";
	        // app::response("200", ["error"=>FALSE,"items"=>$response])
			
			echo json_encode($response);
	}elseif ($step=="edit") {	
		if (isset($id)) {
			// isset($_GET['id'])?$id=$_GET['id']:null;
			// isset($_GET['pfpd'])?$pfpd=$_GET['pfpd']:null;
			// isset($_GET['status'])?$status=$_GET['status']:null;
			// isset($_GET['tanggal'])?$tanggal=$_GET['tanggal']:null;
			// isset($_GET['tanggal_sppb'])?$tanggal_sppb=$_GET['tanggal_sppb']:null;
			// isset($_GET['date_delivery'])?$date_delivery=$_GET['date_delivery']:null;
			// isset($_GET['create_by'])?$create_by=$_GET['create_by']:null;
			isset($pfpd)?$pfpd=$pfpd:null;
			isset($status)?$status=$status:null;
			isset($tanggal)?$tanggal=$tanggal:null;
			isset($tanggal_sppb)?$tanggal_sppb=$tanggal_sppb:null;
			isset($date_delivery)?$date_delivery=$date_delivery:null;
			isset($create_by)?$create_by=$create_by:null;
			$sql 	= "UPDATE ".$app['table']['beacukai']." set 
						pfpd = '$pfpd', 
						status = '$status', 
						tanggal = '$tanggal', 
						tanggal_sppb = '$tanggal_sppb', 
						date_delivery = '$date_delivery',
						nomer_sptnp = '$nomer_sptnp',
						tgl_sptnp = '$tgl_sptnp',
						nilai_sptnp = '$nilai_sptnp', 
						update_by = '$create_by',
						update_at = now()
						WHERE id = '$id' ";
					   db::qry($sql);
					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Beacukai Berhasil Teredit";
					   echo json_encode($response);
					   // echo json_encode($sql);
		}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Terjadi Kesalahan Saat Menginput";
					   echo json_encode($response);
		}
	}elseif ($step=="delete") {	
		if (isset($id)) {
			// $id 	= $_GET['id'];
			$sql 	= "DELETE FROM ".$app['table']['beacukai']." WHERE id ='". $id ."' ";
					   db::qry($sql);
					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Data Berhasil di Delete";
					   echo json_encode($response);
					   // echo json_encode($sql);
		}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Terjadi Kesalahan Saat Menginput";
					   echo json_encode($response);
		}
	}else{
			// $halaman_max 	= 2;
			// $pagenya 	 	= isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
			// $page 			= isset($_GET['page']) ? $_GET['page'] : 1 ;
			// $mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$q=null;
			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['beacukai']." ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);            
	  		// $sqlnya = "select * from dmg_container LIMIT $mulai, $halaman";
	  		// db::query($sqlnya, $rs['row'], $nr['row']);
			if (isset($cari)) {
				$q = "where pfpd LIKE '%".$cari."%' OR tanggal_sppb LIKE '%".$cari."%' OR tanggal LIKE '%".$cari."%' ";
			}
			// else{
			// 	$q = " LIMIT $mulai, $halaman";
			// }

			// $sql = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
			// 		a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
			// 		b.name name FROM ".$app['table']['beacukai']." a
			// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q LIMIT 5";
		
		/*	$sql   = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
					  a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					  b.name name,a.nilai_sptnp nilai_sptnp,a.nomer_sptnp nomer_sptnp,a.tgl_sptnp tgl_sptnp FROM ".$app['table']['beacukai']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q  LIMIT $mulai, $halaman ";//

			$sql   = "SELECT a.*,a.status status,a.tanggal tanggal,a.pfpd pfpd,
					  a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					  b.name name,a.nilai_sptnp nilai_sptnp,a.nomer_sptnp nomer_sptnp,a.tgl_sptnp tgl_sptnp FROM ".$app['table']['beacukai']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q  LIMIT $mulai, $halaman ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				$user[] = $row;
			}
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					if ($user) {
						$response["error"] = FALSE;
						// $response["sql"] = $sql;
						// $response["beacukai"] = $user["beacukai"]['status'];
						$response["beacukai"]["total_tiap_halaman"] = $nr['row'];
						$response["beacukai"]["total"] = $nr['rows'];
						$no=0;
							foreach ($user as $key) {
								// $response["beacukai"][$nonya]["status"] 		= $user[$no]['status'];
								$response["beacukai"][$nonya]["status"] 		= $key['status'];
								// $response["beacukai"][$nonya]["tanggal"]		= $user[$no]['tanggal'];
								$response["beacukai"][$nonya]["tanggal"]		= $key['tanggal'];
								$response["beacukai"][$nonya]["pfpd"] 			= $key['pfpd'];
								$response["beacukai"][$nonya]["tanggal_sppb"] 	= $key['tanggal_sppb'];
								$response["beacukai"][$nonya]["date_delivery"]  = $key['date_delivery'];
								$response["beacukai"][$nonya]["nomer_sptnp"] 	= $key['nomer_sptnp'];
								$response["beacukai"][$nonya]["tgl_sptnp"] 		= $key['tgl_sptnp'];
								$response["beacukai"][$nonya]["nilai_sptnp"]	= $key['nilai_sptnp'];
								$response["beacukai"][$nonya]["name"] 			= $key['name'];
								// $response["beacukai"][$no]["no"] 			= $no;
								$no++;
								$nonya++;
							}
	// foreach ($user["beacukai"] as $key => $value) {
	// 	$response["beacukai"][$key] = $value;
	// }
							// while ($user = $user) {
							// //  // $response["id"] = $user["unique_id"];
							// // 	// $response["beacukai"]["status"] = $user["status"];
							// 	$response["beacukai"]["tanggal"] = $user["tanggal"];
							// // 	// $response["beacukai"]["pfpd"] 	= $user["pfpd"];
							// // 	// $response["beacukai"]["tanggal_sppb"] = $user["tanggal_sppb"];
							// // 	// $response["beacukai"]["date_delivery"] = $user["date_delivery"];
							// }
						// $response["message"] = "Beacukai Berhasil Termuat".$step;
						$response["message"] = "Beacukai Berhasil Termuat";
						// echo json_encode($sql);
	        			app::response("200", ["error"=>FALSE,"items"=>$user]);
						// echo json_encode($response);
					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Belum Input Data";
						echo json_encode($response);
	        			// app::response("200", ["error"=>FALSE,"items"=>$response]);
						// echo json_encode($sql);
					}else{
						$response["error"]=TRUE;
						$response["error_message"] = "Terjadi Kesalahan Saat Menyimpan";
						echo json_encode($response);
					}
	}
}*/if($act=="pelayaran"){
		$app['me'] 	= app::unserialize64($_SESSION[$app['session']]);

		// $form = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);

	if ($step=="simpan") {
			// isset($_GET['pfpd'])?$pfpd=$_GET['pfpd']:null;
			// isset($_GET['status'])?$status=$_GET['status']:null;
			// isset($_GET['tanggal'])?$tanggal=$_GET['tanggal']:null;
			isset($p_date_receipt_do)?$p_date_receipt_do=$p_date_receipt_do:null;
			isset($p_expired_do)?$p_expired_do=$p_expired_do:null;
			isset($me)?$me=$me:null;
			isset($id_imp)?$id_imp=$id_imp:null;
			// $status 		= $_GET['status'];
			// $tanggal 		= $_GET['tanggal'];
			// isset($_GET['tanggal_sppb'])?$tanggal_sppb=$_GET['tanggal_sppb']:null;
			// isset($_GET['date_delivery'])?$date_delivery=$_GET['date_delivery']:null;
			// isset($_GET['create_by'])?$create_by=$_GET['create_by']:null;
			isset($do)?$do = $_FILES['do']['name']:$do=null;
			isset($p_no_do)?$p_no_do = $p_no_do:$p_no_do=null;
			// isset($create_by)?$create_by=$create_by:null;
			$form_imp = db::get_record("import", "id", $id_imp);

		if (strtotime($p_expired_do) < time()) {
			$statusnya_column=",status";
			$status_do = ",'expired'";	
		}
		$id 			  = rand(1, 100).date("dmYHis");
		$ids 			  = rand(1, 100).date("dmYHis");
		$id_c 			  = rand(1, 100).date("dmYHis");

/*		if ($_FILES["attachment"]["name"]) {
			$nama_gambar= $_FILES["attachment"]["name"];
		    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/do/".$nama_gambar);
		}*/
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
/*		$p_date_receipt_do = date("Y-m-d",strtotime($get_do));
		$p_expired_do = date("Y-m-d",strtotime($exp_do));*/
		$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
		$p_expired_do = date("Y-m-d",strtotime($p_expired_do));
		// $sql = "insert into ".$app['table']['delivery_order']."
		// 		(id, no_do, date_receipt_do, id_expired_do, created_by, created_at,id_import,do$statusnya_column) values
		// 		('$id', '$p_no_do', '$p_date_receipt_do', '$ids', '$me', now(),
		// 		'$id_imp','$do'$statusnya_column)";
					// expired_date 		= '$p_expired_do',
		 $sql = "insert into ".$app['table']['delivery_order']."
				(id, date_receipt_do, id_expired_do, created_at,id_import,expired_date$statusnya_column) values
				('$id', '$p_date_receipt_do', '$ids', now(),
				'$id_imp','$p_expired_do'$status_do)";

			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);

		$sqls = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_at) values ('$ids', '$id','$p_expired_do',  now())";

 		// $sqlsss ="insert into ".$app['table']['notif']." 
			// 	(id,id_rule,id_import,status,pesan,created_by,created_at) values
			// 	('$id_c','5403092018114622','$id_imp',1,'DO Terbuat dengan dengan no pib = ".$form_imp['num_pib']." expired pada = $p_expired_do','$me',now())";

/*  	$status_kirim_s = "";
	$asdqwezxcasd= "SELECT * FROM ".$app['table']['import']." where id = '".$id_imp."' ";
	db::query($asdqwezxcasd, $rs11['row'], $nr11['row']);
	while($row11 = db::fetch($rs11['row']))
	{
		$cek_selesai = db::lookup("GROUP_CONCAT(stat_kir)","container", "id_import", $row11['id']);
		if (!preg_match("/0/i", $cek_selesai)) {
			// db::qry("update ".$app['table']['import']." set	send_status = 'selesai' where id='".$row11['id']."' ");
  			$status_kirim_s = "semua";
		}elseif (preg_match("/1/i", $cek_selesai)) {
			// db::qry("update ".$app['table']['import']." set	send_status = 'sebagian' where id='".$row11['id']."' ");
  			$status_kirim_s = "sebagian";
		}else{
  			$status_kirim_s = "sebagian";
		}
	}*/
/* $container_status 	= "SELECT id,num_container FROM ". $app['table']['container'] ." WHERE id_import = '".$id_imp."' ";
	db::query($container_status, $rs['abcdef'], $nr['abcdef']);
	while ($row456	= db::fetch($rs['abcdef'])) {*/	

			$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
			db::query($sql_lang, $rs2['lang'], $nr2['lang']);
			while ($row321	= db::fetch($rs2['lang'])) {
			// echo "asdasd";exit;
	/*			$data_positions = db::lookup("positions","status","id_import ='$id_importnya' AND lang = '".$row321['alias']."' AND id_container = '$id' ");

				db::qry("DELETE FROM ". $app['table']['status'] ." WHERE id_import ='$id_importnya' AND lang = '".$row321['alias']."' AND id_container = '$id' ");

				$id_c2 = rand(1, 100).date("dmYHis");
		 		$sqlss321 ="insert into ".$app['table']['status']." 
							(id,id_import,id_container,positions,created_by,created_at,lang) values
							('$id_c2','$idm','$id',concat('".app::getliblang('proses_jadwal_truck',$row321['alias'])." : <b>".format_tgl_bahasa($date_kirim,$row321['alias'])."</b>,',$data_positions),'".$app['me']['id']."',now(),'".$row['alias']."')";
				db::qry($sqlss321);*/
	/*	 		$sqlssssss321 ="UPDATE ".$app['table']['status']." set 
					positions=concat('".app::getliblang('proses_tebus_do',$row321['alias']).",','".app::getliblang('proses_periksa_status',$row321['alias']).",',positions),
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
					WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' AND id_container = '".$row456['id']."' ";*/
	/*	 		$sqlssssss321 ="UPDATE ".$app['table']['status']." set 
					positions=concat('".app::getliblang('proses_periksa_status',$row321['alias']).",','".app::getliblang('proses_tebus_do',$row321['alias']).",',positions),
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
					WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' ";*/

				$id_c321 = rand(1, 100).date("dmYHis");

	/*			$multiple_insert .= "('$id_c321','$id_imp','".$row456['id']."','".app::getliblang('proses_periksa_status',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."'),";*/	
/*				$multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_periksa_status',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."'),";*/	
				$multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_periksa_status',$row321['alias'])."','".$me."',now(),'".$row321['alias']."'),";			
	/* 	 		$sqlss321 ="insert into ".$app['table']['status']." 
							(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values
							('$id_c321','$id_imp','".$row456['id']."','".app::getliblang('proses_periksa_status',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."')";	*/

				// db::qry($sqlss321);
				$id_c321 = rand(101, 200).date("dmYHis")."2";
	/* 	 		$sqlss321 ="insert into ".$app['table']['status']." 
							(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values
							('$id_c321','$id_imp','".$row456['id']."','".app::getliblang('proses_tebus_do',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."')";	

				db::qry($sqlss321);*/
	/*
				$multiple_insert .= "('$id_c321','$id_imp','".$row456['id']."','".app::getliblang('proses_tebus_do',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."'),";*/
/*				$multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_tebus_do',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."'),";*/
				$multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_tebus_do',$row321['alias'])."','".$me."',now(),'".$row321['alias']."'),";
				// db::qry($sqlssssss321);
					// $test321 .=$sqlssssss321;
			}
	// }
	$multiple_insert = rtrim($multiple_insert,",");	
/* 	$sqlss321 = "insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values $multiple_insert";*//*
 	$sqlss321 = "insert into ".$app['table']['status']." 
						(id,id_import,positions,created_by,created_at,lang,num_container) values $multiple_insert";*/
 	$sqlss321 = "insert into ".$app['table']['status']." 
						(id,id_import,positions,created_by,created_at,lang) values $multiple_insert";
	db::qry($sqlss321);

	$num_bil = db::lookup("num_bl","import","id",$id_imp);
	$cek_customer = db::lookup("id_customer","import","id",$id_imp);
	$rs['devices'] = db::get_recordset("user_device","id_user='".$cek_customer."' AND status='active'");
	while($user_devices = db::fetch($rs['devices'])){
		$datapush = array(
			"to" => db::lookup("token","device","id",$user_devices['id_device']),
			"notification" => array(
				"title" => "Status Updated",
				"text" => "$num_bil : ".app::getliblang('proses_periksa_status',"id").", ".app::getliblang('proses_tebus_do',"id"),
				"sound" => "default"
			),
			'data' => array(
				'method' => 'change_status',
				'id' => $id_imp,
				"title" => "Status Updated",
				"body" => "$num_bil : ".app::getliblang('proses_periksa_status',"id").", ".app::getliblang('proses_tebus_do',"id")
			)
		);
		app::pushNotifClientSend($datapush);
	}

// 	echo "khalid321";
// print_r($test321);
// exit;
		if ($form_imp['status_sppb'] == "1") {
			$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					// "to" => db::lookup("token","device","id",$user_devices['id_device']),
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Pelabuhan/EIR",
						"text" => $form_imp['num_pib']." : Buat Kelengkapan Container",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelabuhan',
						'ncr_id' => $id_imp,
						"title" => "Admin Pelabuhan/EIR",
						"body" => $form_imp['num_pib']." : Buat Kelengkapan Container"
					)
				);
				app::pushNotifSend($datapush);
			}
			$id_notif   = rand(1, 100).date("dmYHis");
			$sql_notif = "insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_notif','5403092018114622','".$form_imp['id']."',1,'".$form_imp['num_pib']." : Buat Kelengkapan Container','".$app['me']['id']."',now())";
			db::qry($sql_notif);
		}
		db::qry($sql);
		db::qry($sqls);
		db::qry($sqlsss);
		$message  .= "<p>Date Receipt DO :<b>". $p_date_receipt_do ."</b></p>";
		$message  .= "<p>Expired DO : <b>". $p_expired_do ."</b></p>";
		// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='5403092018114622' ";
		$path_do = $app['doc_path']."/do/".$do;

		db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
		while($row = db::fetch($rs['sql_bea_cukai'])){
			app::sendmail($row['email'], "Ada Dokumen Delivery Order Masuk", $message, $path_do);
		}

		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while ($row321	= db::fetch($rs['lang'])) {
			// $sqlss ="insert into ".$app['table']['status']." 
			// 		(id,id_import,positions,created_by,created_at,lang) values
			// 		('$id_c','$id_imp','".app::getliblang('proses_pelayaran',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
			// $sqlss ="UPDATE ".$app['table']['status']." SET positions = '".app::getliblang('proses_pelabuhan_eir',$row['alias'])."',updated_by = '$me',updated_at = now() WHERE id_import = '$id_imp' ";
/*		$sqlss ="UPDATE ".$app['table']['status']." set 
				positions=concat('".app::getliblang('proses_pelayaran',$row321['alias']).",','<br>',positions),
				updated_by = '". $me."',
				updated_at = now()
				WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' ";
			db::qry($sqlss);*/
		}
			// db::qry($sqlss);
			$response["error"]	 = FALSE;
			// $response["id"] = $user["unique_id"];
			$response["message"] = "Delivery Order Berhasil Tersimpan";
	        // app::response("200", ["error"=>FALSE,"items"=>$response]);
	        app::response("200", ["error"=>0,"data"=>["message"=>$response['message']]]);
			
			// echo json_encode($response);
	}elseif ($step=="edit") {	
		if (isset($id_imp)) {
			// isset($_GET['id'])?$id=$_GET['id']:null;
			// isset($_GET['pfpd'])?$pfpd=$_GET['pfpd']:null;
			// isset($_GET['status'])?$status=$_GET['status']:null;
			// isset($_GET['tanggal'])?$tanggal=$_GET['tanggal']:null;
			// isset($_GET['tanggal_sppb'])?$tanggal_sppb=$_GET['tanggal_sppb']:null;
			// isset($_GET['date_delivery'])?$date_delivery=$_GET['date_delivery']:null;
			// isset($_GET['create_by'])?$create_by=$_GET['create_by']:null;
			isset($p_date_receipt_do)?$p_date_receipt_do=$p_date_receipt_do:null;
			isset($p_expired_do)?$p_expired_do=$p_expired_do:null;
			isset($id_idv_ord)?$id_idv_ord=$id_idv_ord:null;
			isset($me)?$me=$me:null;
			isset($id_imp)?$id_imp=$id_imp:null;
			isset($do)?$do = $_FILES['do']['name']:null;

			$id_idv_ord =db::lookup("id","delivery_order","id_import",$id_imp);
			// $p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
			// $p_expired_do = date("Y-m-d",strtotime($p_expired_do));

			$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
			$p_expired_do = date("Y-m-d",strtotime($p_expired_do));

			$form_imp = db::get_record("import", "id", $id_imp);
			// if (strtotime($p_expired_do) >= time()) {
			if (strtotime($p_expired_do) >= strtotime('now')) {
				$status_do = ",status = 'active' ";
			}else{
				$status_do = ",status = 'expired' ";	
			}
			// $total = db::lookup('COUNT(id)', 'expired_do', 'id_delivery_order = '.$id_idv_ord);
			// if($total < 3) $exp_date2 = "date_receipt_do 	= '$p_date_receipt_do',";

// 		if ($_FILES["attachment"]["name"]) {
// 			$delete_file = db::get_record("delivery_order","id_import",$id_imp);
// 			@unlink($app['doc_path']."/do/". $delete_file['do']);
// 			$nama_gambar= $_FILES["attachment"]["name"];
// /*			    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/do/".$nama_gambar);
// 		    $abc="do 	= '$nama_gambar',";*/
// 		    if (move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/do/".$nama_gambar)) {
// 			    $abc="eir 	= '$nama_gambar',";
// 		    }else{
// 			    	// echo "gagal ganti gambar";
//    				    $response["message"] = "Gambar Gagal Di Upload";
// 			        app::response("404", ["error"=>TRUE,"data"=>["destination"=>$response]]);
// 			    	exit;
// 		    }
// 		}

		// if ($_FILES["attachment"]["name"]) {
		// 	$nama_gambar= $_FILES["attachment"]["name"];
		//     move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/do/".$nama_gambar);
		// }

			/*else{

			}*/
			$cek_revisi = db::lookup("revisi","delivery_order","id_import",$id_imp);
			if ($cek_revisi==1) {

			$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
				while($user_devices = db::fetch($rs['devices'])){
					$datapush = array(
						// "to" => db::lookup("token","device","id",$user_devices['id_device']),
						"to" => db::lookup("token","device","id",$user_devices['id_device']),
						"notification" => array(
							"title" => "Admin Pelabuhan/EIR",
							"text" => $form_imp['num_pib']." : Telah Direvisi Oleh Admin Pelayaran",
							"sound" => "default"
						),
						'data' => array(
							'method' => 'pelabuhan',
							'ncr_id' => $form_imp['id'],
							"title" => "Admin Pelabuhan/EIR",
							"body" => $form_imp['num_pib']." : Telah Direvisi Oleh Admin Pelayaran"
						)	
					);
					app::pushNotifSend($datapush);
				}

				$id_notif   = rand(1, 100).date("dmYHis");
				$sql_notif = "insert into ".$app['table']['notif']." 
						(id,id_rule,id_import,status,pesan,created_by,created_at) values
						('$id_notif','5403092018114622','".$form_imp['id']."',1,'".$form_imp['num_pib']."  : Telah Direvisi Oleh Admin Pelayaran','".$app['me']['id']."',now())";
				db::qry($sql_notif);
			}
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$cek_pelayaran = db::lookup("id","delivery_order","id_import",$id_imp);
			if ($cek_pelayaran) {
				$sql = "update ". $app['table']['delivery_order'] ."
					set date_receipt_do 	= '$p_date_receipt_do',
						expired_date 		= '$p_expired_do',
						revisi 				= '0',
						updated_at 			= now()
						$status_do
						where id_import = '$id_imp'";
				db::qry($sql);

				$id_idv_ord = db::lookup("id","delivery_order","id_import",$id_imp);
				if(!empty($p_expired_do)){
					$ids = rand(1, 100).date("dmYHis");
					$id_notif = rand(1, 100).date("dmYHis");
					$sqls = "update ".$app['table']['expired_do']." set
							 status = 'inactive' where id_delivery_order = '$id_idv_ord' ";
					db::qry($sqls);
					$sqlss = "insert into ".$app['table']['expired_do']."
							(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id_idv_ord', '$p_expired_do','$me',now())";
					db::qry($sqlss);

				$sql = "update ". $app['table']['delivery_order'] ."
						set id_expired_do 	= '$ids',
						where id_import = '$id_imp'";
				db::qry($sql);

	/*				$sqls32 = "insert into ".$app['table']['notif']." 
								(id,id_rule,id_import,status,pesan,created_by,created_at) values
								('$id_notif','5403092018114622','$id',1,'Expired DO dengan no pib : ".$form_imp['num_pib'].", Telah diperpanjang sampai : ". $p_expired_do ."','$me',now())";
					db::qry($sqls32);*/

					/*$rs['devices'] = db::get_recordset("device","category='pelabuhan' AND status='active'");
					while($user_devices = db::fetch($rs['devices'])){
						$datapush = array(
							// "to" => db::lookup("token","device","id",$user_devices['id_device']),
							"to" => db::lookup("token","device","category","pelabuhan"),
							"notification" => array(
								"title" => "Admin Pelabuhan/eir",
								"text" => $form_imp['num_pib']." : Buat Kelengkapan Container",
								"sound" => "default"
							),
							'data' => array(
								'method' => 'pelabuhan',
								'ncr_id' => $id,
								"title" => "Admin Pelabuhan/eir",
								"body" => $form_imp['num_pib']." : Buat Kelengkapan Container"
							)
						);
						app::pushNotifSend($datapush);
					}*/

					$message  .= "<p>Expired DO dengan no pib : ".$form_imp['num_pib'].", Telah diperpanjang sampai : <b>". $p_expired_do ."</b></p>";
					// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
					$message .= "<p>Terima kasih</p>";
					$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
								   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
								   where a.id_rule ='5403092018114622' ";
					db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
					while($row = db::fetch($rs['sql_bea_cukai'])){
						app::sendmail($row['email'], "dokumen delivery order masuk", $message);
					}
				}
			}else{

		$id 			  = rand(1, 100).date("dmYHis");
		$ids 			  = rand(1, 100).date("dmYHis");
		$id_c 			  = rand(1, 100).date("dmYHis");

		if (strtotime($p_expired_do) <= strtotime('now')) {
			$statusnya_column=",status";
		 	$status_do = ",'expired'";	
		}else{
			$status_do ="";
		}

		  	$sql = "insert into ".$app['table']['delivery_order']."
					(id, date_receipt_do, id_expired_do, created_at,id_import,expired_date$statusnya_column) values
					('$id', '$p_date_receipt_do', '$ids', now(),
					'$id_imp','$p_expired_do'$status_do)";

			$sqls = "insert into ".$app['table']['expired_do']."
					(id, id_delivery_order, expired_do, created_at) values ('$ids', '$id','$p_expired_do',  now())";

/*	 		$sqlsss ="insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_c','5403092018114622','$id_imp',1,'DO Terbuat dengan dengan no pib = ".$form_imp['num_pib']." expired pada = $p_expired_do','$me',now())";*/

/*		 	$container_status 	= "SELECT id,num_container FROM ". $app['table']['container'] ." WHERE id_import = '".$id_imp."' ";
			db::query($container_status, $rs['abcdef'], $nr['abcdef']);
			while ($row456	= db::fetch($rs['abcdef'])) {	*/
				$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
				db::query($sql_lang, $rs2['lang'], $nr2['lang']);
				while ($row321	= db::fetch($rs2['lang'])) {
					$id_c321 = rand(1, 100).date("dmYHis");

					// $multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_periksa_status',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."'),";	
					$multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_periksa_status',$row321['alias'])."','".$me."',now(),'".$row321['alias']."'),";			

/*					$id_c321 = rand(101, 200).date("dmYHis")."2";
					$multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_tebus_do',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."'),";*/
					$id_c321 = rand(101, 200).date("dmYHis")."2";
					$multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_tebus_do',$row321['alias'])."','".$me."',now(),'".$row321['alias']."'),";

				}
			// }

			// }
			$multiple_insert = rtrim($multiple_insert,",");	

/*		 	$sqlss321 = "insert into ".$app['table']['status']." 
								(id,id_import,positions,created_by,created_at,lang,num_container) values $multiple_insert";*/
		 	$sqlss321 = "insert into ".$app['table']['status']." 
								(id,id_import,positions,created_by,created_at,lang) values $multiple_insert";
			db::qry($sqlss321);

			$num_bil = db::lookup("num_bl","import","id",$id_imp);
			$cek_customer = db::lookup("id_customer","import","id",$id_imp);
			$rs['devices'] = db::get_recordset("user_device","id_user='".$cek_customer."' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Status Updated",
						"text" => "$num_bil : ".app::getliblang('proses_periksa_status',"id").", ".app::getliblang('proses_tebus_do',"id")."
								   ",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'change_status',
						'id' => $id_imp,
						"title" => "Status Updated",
						"body" => "$num_bil : ".app::getliblang('proses_periksa_status',"id").", ".app::getliblang('proses_tebus_do',"id")
					)
				);
				app::pushNotifClientSend($datapush);
			}

				if ($form_imp['status_sppb'] == "1") {
					$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
					while($user_devices = db::fetch($rs['devices'])){
						$datapush = array(
							// "to" => db::lookup("token","device","id",$user_devices['id_device']),
							"to" => db::lookup("token","device","id",$user_devices['id_device']),
							"notification" => array(
								"title" => "Admin Pelabuhan/EIR",
								"text" => $form_imp['num_pib']." : Buat Kelengkapan Container",
								"sound" => "default"
							),
							'data' => array(
								'method' => 'pelabuhan',
								'ncr_id' => $id_imp,
								"title" => "Admin Pelabuhan/EIR",
								"body" => $form_imp['num_pib']." : Buat Kelengkapan Container"
							)
						);
						app::pushNotifSend($datapush);
					}
						$id_notif   = rand(1, 100).date("dmYHis");
						$sql_notif = "insert into ".$app['table']['notif']." 
								(id,id_rule,id_import,status,pesan,created_by,created_at) values
								('$id_notif','5403092018114622','".$form_imp['id']."',1,'".$form_imp['num_pib']." : Buat Kelengkapan Container','".$app['me']['id']."',now())";
						db::qry($sql_notif);
				}
				db::qry($sql);
				db::qry($sqls);
				db::qry($sqlsss);
				$message  .= "<p>Date Receipt DO :<b>". $p_date_receipt_do ."</b></p>";
				$message  .= "<p>Expired DO : <b>". $p_expired_do ."</b></p>";
				// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
				$message .= "<p>Terima kasih</p>";
				$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
							   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
							   where a.id_rule ='5403092018114622' ";
				$path_do = $app['doc_path']."/do/".$do;

				db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
				while($row = db::fetch($rs['sql_bea_cukai'])){
					app::sendmail($row['email'], "Ada Dokumen Delivery Order Masuk", $message, $path_do);
				}
			}
				   $response["error"] = FALSE;
				   // $response["id"] = $user["unique_id"];
				   $response["message"] = "Delivery Order Berhasil Teredit";
				   // echo json_encode($response);
			       // app::response("200", ["error"=>FALSE,"data"=>["destination"=>$response]]);
			       app::response("200", ["error"=>0,"data"=>["message"=>$response["message"]]]);
				   // echo json_encode($sql);
		}else{
				   $response["error"] = TRUE;
				   // $response["id"] = $user["unique_id"];
					$response["error_message"] = "Terjadi Kesalahan Saat Mengupdate";
				   // echo json_encode($response);
			       app::response("500", ["error"=>FALSE,"data"=>["message"=>$response]]);
		}
	}elseif ($step=="detail") {	
	if ($id) {
			// $sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  where a.id = '$id' ";
			// $sql   = "SELECT a.*,b.name created_by FROM ".$app['table']['import']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
			// 		  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (c.id_import=a.id)  where a.id = '$id' ";
/*			$sql   = "SELECT a.qty qty, a.num_container num_container,a.note note, a.value value, b.num_pib name, b.num_bl num_bl, b.name_ship name_ship, b.eta eta,b.id_customer id_customer,b.tanggal_sptnp tanggal_sptnp,b.nomor_sptnp nomor_sptnp,b.nilai_sptnp nilai_sptnp,b.amount_payment amount_payment FROM ".$app['table']['container']." a
				LEFT JOIN ". $app['table']['import'] ." b ON (a.id_import=b.id)  where b.id = '$id' limit 1 ";*/

			$sqlupt		  = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '".$id_rule."' ";
			db::qry($sqlupt);
			$sql   = "SELECT  tgl_nopen,id,num_pib num_pib, status_beacukai, num_bl num_bl,name_ship,eta,nopen,id_customer,tanggal_sptnp, nomor_sptnp,nilai_sptnp, amount_payment,status_sppb status_sppb, pfpd FROM ". $app['table']['import'] ."  where id = '$id' limit 1 ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				$record_pelayaran = db::get_record("delivery_order","id_import",$id);
				$row['tanggal_receipt_format']=format_tgl($record_pelayaran['date_receipt_do']);
				$row['do_file'] = $record_pelayaran['do'];
				$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$record_pelayaran['id']."' AND status = 'active' ");
				$row['tanggal_exp_format']=format_tgl($exp_date['expired_do']);
				$sql2   = "SELECT * FROM ".$app['table']['container']." where id_import ='$id' ";
				// $nonya = $mulai+1;
				// $q ORDER BY a.lang,a.reorder";
				db::query($sql2, $rs2['row'], $nr2['row']);
				$test["length"]= $nr2['row'];
				while($row2 = db::fetch($rs2['row']))
				{
					$test[] = $row2;
				}	
				$row['isi_container'] = $test;
				if ($record_pelayaran['id'] !="") {
					$record_pelayaran['date_receipt_do'] = format_tgl($record_pelayaran['date_receipt_do']);
					$record_pelayaran['expired_date'] = format_tgl($record_pelayaran['expired_date']);
					$row['isi_act'] = $record_pelayaran;
				}else{
					$row['isi_act'] = array('eir' =>"" );
				}
				$row['eta_format'] = format_tgl($row['eta']);
				$row['eta'] = format_tgl($row['eta']);
				$id_customer 	 =	db::get_record("customer","id",$row['id_customer']);
				$row['customer'] = $id_customer['name'];
				if ($row['tanggal_sptnp'] !="" AND $row['tanggal_sptnp'] !="1970-01-01" AND $row['tanggal_sptnp'] !="0000-00-00") 
					$row['tanggal_sptnp_format'] = format_tgl($row['tanggal_sptnp']);
				else $row['tanggal_sptnp_format'] ="";
				$row['format_rupiah'] = rupiah($row['amount_payment']);
				$row['dok_kel']="no";
				$user[] = $row;
			}
			$sql   = "SELECT id,name,id_import FROM ". $app['table']['file_do'] ."
					  where id_import = '$id' and status = 'active' order by created_at DESC";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs2['row'], $nr2['row']);
			$file_tampil['length'] = $nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['url_photo'] = api::urlApiFile_doc_path("do",$row2['name']);
				$file_tampil[] = $row2;
			}
			$sql   = "SELECT id,name,id_import FROM ". $app['table']['file_kel'] ."
					  where id_import = '$id' and status = 'active' order by created_at DESC ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs5['row'], $nr5['row']);
			$file_kel['length']=$nr5['row'];
			while($row6 = db::fetch($rs5['row']))
			{
				$row6['url_photo'] = api::urlApiFile_doc_path("sppb",$row6['name']);
				$file_kel[] = $row6;
			}
	
			if ($user) {
				$response["message"] = "Data Berhasil Termuat";
				// echo json_encode($sql);
				// app::response("200", ["error"=>FALSE,"items"=>$user]);
				app::response("200", ["error"=>0,"data"=>["destination"=>$user,"tampil_file"=>$file_tampil],"response"=>$response]);
				// echo json_encode($response);
			}elseif ($user==0) {
				$response["error"]=FALSE;
				$response["message"] = "Data Masih kosong";
				// echo json_encode($response);
		        app::response("404", ["error"=>0,"data"=>["destination"=>$user,"tampil_file"=>$file_tampil,"file_kel"=>$file_kel],"response"=>$response]);
				// app::response("200", ["error"=>FALSE,"items"=>$response]);
				// echo json_encode($sql);
			}else{
				$response["error"]=TRUE;
				$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
				// echo json_encode($response);
		        app::response("500", ["error"=>500,"data"=>["destination"=>$user],"response"=>$response]);
				// echo json_encode($response);
			}
		}
	}elseif ($step=="revisi") {	
	if ($id_imp) {

/*			$sqlupt = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '".$id_rule."' ";
			db::qry($sqlupt);*/

			$data_import = db::get_record("import","id",$id_imp);
			app::mq_encode('p_alasan_rev');
			$sql = "update ". $app['table']['delivery_order'] ."
					set 
					revisi 				= '1',
					alas_rev 			= '$p_alasan_rev'
					where id_import 	= '$id_imp'";
			db::qry($sql);

			$id321 = rand(1, 100).date("dmYHis");
			$sqlsss321 ="insert into ".$app['table']['notif']." 
					(id, id_rule, id_import, status, pesan, created_by, created_at) values
					('$id321', '2120082018161449', '$id_imp', 1, '".$data_import['num_pib']." : ".$p_alasan_rev."','$me',now())";
			db::qry($sqlsss321);

			$rs['devices'] = db::get_recordset("user_device","category='pelayaran' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					// "to" => db::lookup("token","device","id",$user_devices['id_device']),
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Pelayaran",
						"text" => "Revisi PIB ".$data_import['num_pib']." : ".$p_alasan_rev,
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelayaran',
						'ncr_id' => $id_imp,
						"title" => "Admin pelayaran",
						"body" => "Revisi PIB ".$data_import['num_pib']." : ".$p_alasan_rev
					)	
				);
				app::pushNotifSend($datapush);
			}

				app::response("200", ["error"=>0,"message"=>"Berhasil_tersimpan"]);
		}else{
			$response["error"]=TRUE;
			$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
			// echo json_encode($response);
	        app::response("500", ["error"=>500,"data"=>["destination"=>$user],"response"=>$response]);
		}
	}elseif ($step=="completed_doc") {	

/*			$q 		 = null;
			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['pelabuhan']." ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);     */       
	  		// $sqlnya = "select * from dmg_container LIMIT $mulai, $halaman";
	  		// db::query($sqlnya, $rs['row'], $nr['row']);

############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################

			if (isset($cari)) {
				// $q = "where pfpd LIKE '%".$cari."%' OR tanggal_sppb LIKE '%".$cari."%' OR tanggal LIKE '%".$cari."%' ";
				$q = "AND d.num_pib LIKE '%".$cari."%' OR d.status_beacukai LIKE '%".$cari."%' ";
			}
			// else{
			// 	$q = " LIMIT $mulai, $halaman";
			// }

			// $sql = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
			// 		a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
			// 		b.name name FROM ".$app['table']['beacukai']." a
			// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q LIMIT 5";
		
		/*	$sql   = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
					  a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					  b.name name,a.nilai_sptnp nilai_sptnp,a.nomer_sptnp nomer_sptnp,a.tgl_sptnp tgl_sptnp FROM ".$app['table']['beacukai']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q  LIMIT $mulai, $halaman ";*/

/*			$sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  $q  LIMIT $mulai, $halaman ";*/

			$sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['import'] ." c ON (a.id_import=c.id) where c.status_kiriman = '1' $q ";

			// $nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row2 = db::fetch($rs['row']))
			{
			/*	$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id_idv']."' and status = 'active'");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id_idv']."' ");

				$row['date_do']=$datas['do'];
				if ($row['date_do']!=null&&$row['date_do']!="0000-00-00") {
					$row['date_do']= date("d-m-Y",strtotime($row['date_do']));
				}else{
					$row['date_do']="";
				}

				// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
				// $row['expired_do']= $datas['do'];

				// if (strtotime($datas['do']) < time()) {
				if ($datas['do'] < date("Y-m-d")) {
					isset($row['id_idv'])?$id_exp [] = $row['id_idv']:null;
				}
				else{
					isset($row['id_idv'])?$id_exp_act [] = $row['id_idv']:null;
				}*/

				$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id']."' AND status = 'active' ");
				if ($exp_date['expired_do']!="") {
					$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
				}
				$user[] = $row;
			}
		/*	$id_exp 	= implode("','", $id_exp);
			$id_exp_act = implode("','", $id_exp_act);
			$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
			$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
			db::qry($sql321);
			db::qry($sql1231);*/
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					if ($user) {
						$response["message"] = "Beacukai Berhasil Termuat";
						// echo json_encode($sql);
	        			app::response("200", ["error"=>FALSE,"completed"=>$user,"count_completed"=>$nr['row']]);
						// echo json_encode($response);
					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Data Masih kosong";
						// echo json_encode($response);
				        app::response("404", ["completed"=>$response]);
	        			// app::response("200", ["error"=>FALSE,"items"=>$response]);
						// echo json_encode($sql);
					}else{
						$response["error"]=TRUE;
						$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
						// echo json_encode($response);
				        app::response("500", ["completed"=>$response]);
						// echo json_encode($response);
					}
	}elseif ($step=="delete") {	
		if (isset($id_imp)) {
			// $id 	= $_GET['id'];
			// if ($_FILES["attachment"]["name"]) {
				$delete_file = db::get_record("delivery_order","id_import",$id_imp);
				@unlink($app['doc_path']."/do/". $delete_file['do']);
			// }
			$sql 	= "DELETE FROM ".$app['table']['delivery_order']." WHERE id_import ='". $id ."' ";
			$sql2 	= "DELETE FROM ".$app['table']['expired_do']." WHERE id_delivery_order ='". $id_idv_ord ."' ";
					   db::qry($sql);
					   db::qry($sql2);
					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Data Berhasil di Hapus";
	        			// app::response("200", ["error"=>FALSE,"items"=>$user]);
	        			app::response("200", $response);
					   // echo json_encode($response);
					   // echo json_encode($sql);
		}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Identitas Kosong";
	       				app::response("404", ["items"=>$response]);
					   // echo json_encode($response);
		}
	}elseif ($step=="upload_file") {	
############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################

			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"progress"=>$user,"count_progress"=>$nr['row'],"data_terbaru"=>$user_terbaru,"count_new_notif"=>$count_notif]]);
	}elseif ($step=="home_list") {	
############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				if (date("Y-m-d",strtotime($p_search)) != 1970-01-01 && date("Y-m-d",strtotime($p_search)) != "1970-01-01") {
					$q = "AND (a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
				}else{
					$q = "AND (a.num_pib LIKE '%".$p_search."%') ";
				}
			}
			if ($filter_status) {
				if ($filter_status !="ALL") {
					$tambah_status=" AND a.status_beacukai = '$filter_status' ";
				}
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

			if ($p_awal =="tidak") {
				$pakai_awal = "tidak";
			}else{
				$pakai_awal = "iya";
			}
  			$sql_total   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,b.revisi FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.status = 'expired' OR b.revisi=1)$tambah_status $q order by a.created_at DESC";
			db::query($sql_total, $rs_total['row'], $nr_total['row']);

  		  	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,b.revisi,a.tgl_nopen FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.status = 'expired' OR b.revisi=1)$tambah_status $q order by a.created_at DESC $limit_final";
/*			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,b.id id_del FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					  where (a.status_kiriman = '0' AND b.status ='expired') $q order by created_at DESC";*/
			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs['row'], $nr['row']);
			$user['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				if ($row['status_exp'] == "expired") {
					$row['revisi'] = 1;
					$row['border_div'] = "red";
					$row['back_upl'] = "red";
					$row['bor_upl'] = "red";
					$row['bor_det'] = "red";
				}elseif($row['revisi'] == "1" || $row['revisi'] == 1){
					$row['border_div'] = "#e4cf98";
					$row['back_upl'] = "#fe9700";
					$row['bor_upl'] = "#fe9700";
					$row['bor_det'] = "#e4cf98";
				}else{
					$row['border_div'] = "#698299";
					$row['back_upl'] = "#02489e";
					$row['bor_upl'] = "#5a6f89";
					$row['bor_det'] = "#5a6f89";
				}

				if ($row['created_at'] !="") {
					$row['created_at']=format_tgl($row['created_at']);
				}
				$row['doc_kel']="no";
				if ($terbaru==0 && $pakai_awal =="iya") {
					$user_terbaru[] = $row;
				$user['length']=$user['length']-1;
				}else{
					$user[] = $row;				
				}
				$terbaru++;
			}
/*			$sql2   = "SELECT id,num_bl,num_pib,status_beacukai,pfpd FROM ".$app['table']['import']." where status_kiriman = '1' $q order by created_at DESC";
			db::query($sql2, $rs2['row'], $nr2['row']);
			$user2['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$exp_1 = db::get_record("delivery_order" , "id_import = '".$row2['id']."' AND status = 'active' ");
				$exp_date2 = db::get_record("expired_do" , "id_delivery_order = '".$exp_1['id']."' AND status = 'active' ");
				if ($exp_date2['expired_do']!="") {
					$row2['expired_date'] = date("d-m-Y",strtotime($exp_date2['expired_do']));
				}
				$user2[] = $row2;
			}*/
			if ($nr_total['row'] == 0) {
				$data_kosong="iya";
			}
			$count_notif = db::lookup("count(id)","notif","id_rule = '2120082018161449' AND (status ='1' OR status =1) ");
			$response["message"] = "Pelayaran Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"data_kosong"=>$data_kosong,"progress"=>$user,"count_progress"=>$nr_total['row'],"data_terbaru"=>$user_terbaru,"count_new_notif"=>$count_notif]]);
	}elseif ($step=="doc_list") {	
############################ EXPIRED DELIVERY ORDER ####################################
/*			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}*/
########################################################################################
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				if (date("Y-m-d",strtotime($p_search)) != 1970-01-01 && date("Y-m-d",strtotime($p_search)) != "1970-01-01") {
					$q = "AND (a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
				}else{
					$q = "AND (a.num_pib LIKE '%".$p_search."%') ";
				}
			}

			if ($filter_status) {
				if ($filter_status !="ALL") {
					$tambah_status=" AND a.status_beacukai = '$filter_status' ";
				}
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

			if ($p_category == "todo") {
				$tampilnya ="todo";
			}elseif($p_category == "progress"){
				$tampilnya = "progress";
			}elseif($p_category == "completed"){
				$tampilnya = "completed";
			}else{
				$tampilnya = "all";
				$p_category = "all";
			}
			if ($p_category == "todo" || $p_category == "all") {
				$sql_total1   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,b.revisi,a.tgl_nopen FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
				 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.status = 'expired' OR b.revisi=1)$tambah_status $q order by a.created_at DESC";
				db::query($sql_total1, $rs_total1['row'], $nr_total1['row']);

				$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,b.revisi,a.tgl_nopen FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
				 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.status = 'expired' OR b.revisi=1)$tambah_status $q order by a.created_at DESC $limit_final";
				$nonya = $mulai+1;
				// $q ORDER BY a.lang,a.reorder";
				db::query($sql, $rs['row'], $nr['row']);
				$todo['length']=$nr['row'];
				while($row = db::fetch($rs['row']))
				{
					if ($row['status_exp'] == "expired") {
						$row['revisi'] = 1;
						$row['border_div'] = "red";
						$row['back_upl'] = "red";
						$row['bor_upl'] = "red";
						$row['bor_det'] = "red";
					}elseif($row['revisi'] == "1" || $row['revisi'] == 1){
						$row['border_div'] = "#e4cf98";
						$row['back_upl'] = "#fe9700";
						$row['bor_upl'] = "#fe9700";
						$row['bor_det'] = "#e4cf98";
					}else{
						$row['border_div'] = "#698299";
						$row['back_upl'] = "#02489e";
						$row['bor_upl'] = "#5a6f89";
						$row['bor_det'] = "#5a6f89";
					}
					if ($row['created_at'] !="") {
						$row['created_at']=format_tgl($row['created_at']);
					}
					$row['doc_kel']="no";
					$todo[] = $row;
				}
			}
			if ($p_category == "progress" || $p_category == "all") {
	 			// $sql_total2   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					// 	LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					// 	LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
					// 	where c.id IS NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL AND b.status = 'active')$tambah_status $q order by a.created_at DESC";
	 			$sql_total2   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						where (a.status_kiriman = '0') AND (b.id IS NOT NULL AND b.status = 'active' AND b.revisi != 1)$tambah_status $q order by a.created_at DESC";
				db::query($sql_total2, $rs_total2['row'], $nr_total2['row']);

	 		/*	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
						where c.id IS NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL AND b.status = 'active')$tambah_status $q order by a.created_at DESC $limit_final";*/
	 			$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						where (a.status_kiriman = '0') AND (b.id IS NOT NULL AND b.status = 'active' AND b.revisi != 1)$tambah_status $q order by a.created_at DESC $limit_final";
				db::query($sql, $rs2['row'], $nr2['row']);
				$progress['length']=$nr2['row'];
				while($row2 = db::fetch($rs2['row']))
				{
					$row2['status_exp'] ="";
					$row2['border_div'] = "#698299";
					$row2['back_upl'] = "#02489e";
					$row2['bor_upl'] = "#5a6f89";
					$row2['bor_det'] = "#5a6f89";
					if ($row2['created_at'] !="") {
						$row2['created_at']=format_tgl($row2['created_at']);
					}
					$progress[] = $row2;
				}
			}
			if ($p_category == "completed" || $p_category == "all") {
/*				$sql_total3   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (b.id_import=c.id_import)
						LEFT JOIN ". $app['table']['file_do'] ." d ON (b.id_import=c.id_import)
						where (a.status_kiriman = '1') AND d.id IS NOT NULL AND c.id IS NOT NULL AND (b.id IS NOT NULL AND b.status = 'active' AND b.revisi != 1) $q order by a.created_at DESC";*/
				$sql_total3   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (b.id_import=c.id_import)
						LEFT JOIN ". $app['table']['file_do'] ." d ON (b.id_import=c.id_import)
						where (a.status_kiriman = '1') $q order by a.created_at DESC";
				db::query($sql_total3, $rs_total3['row'], $nr_total3['row']);

/*	 			$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (b.id_import=c.id_import)
						LEFT JOIN ". $app['table']['file_do'] ." d ON (b.id_import=c.id_import)
						where (a.status_kiriman = '1') AND d.id IS NOT NULL AND c.id IS NOT NULL AND (b.id IS NOT NULL AND b.status = 'active' AND b.revisi != 1) $q order by a.created_at DESC $limit_final";*/
	 			$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (b.id_import=c.id_import)
						LEFT JOIN ". $app['table']['file_do'] ." d ON (b.id_import=c.id_import)
						where (a.status_kiriman = '1')";
				db::query($sql, $rs3['row'], $nr3['row']);
				$completed['length']=$nr3['row'];
				while($row3 = db::fetch($rs3['row']))
				{
					$row3['status_exp'] ="";
					$row3['border_div'] = "#698299";
					$row3['back_upl'] = "#02489e";
					$row3['bor_upl'] = "#5a6f89";
					$row3['bor_det'] = "#5a6f89";
					if ($row3['created_at'] !="") {
						$row3['created_at']=format_tgl($row3['created_at']);
					}
					$completed[] = $row3;
				}
			}
			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["tampilnya"=>$tampilnya,"message"=>$response["message"],"todo"=>$todo,"count_todo"=>$nr_total1['row'],"progress"=>$progress,"count_progress"=>$nr_total2['row'],"completed"=>$completed,"count_completed"=>$nr_total3['row']]]);
	}elseif ($step=="doc_list_todo") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

			$sql_total1   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,b.revisi FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.status = 'expired' OR b.revisi=1)$tambah_status $q order by a.created_at DESC";
			db::query($sql_total1, $rs_total1['row'], $nr_total1['row']);

			$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,b.revisi FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.status = 'expired' OR b.revisi=1)$tambah_status $q order by a.created_at DESC $limit_final";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			$todo['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				if ($row['status_exp'] == "expired") {
					$row['revisi'] = 1;
					$row['border_div'] = "red";
					$row['back_upl'] = "red";
					$row['bor_upl'] = "red";
					$row['bor_det'] = "red";
				}elseif($row['revisi'] == "1" || $row['revisi'] == 1){
					$row['border_div'] = "#e4cf98";
					$row['back_upl'] = "#fe9700";
					$row['bor_upl'] = "#fe9700";
					$row['bor_det'] = "#e4cf98";
				}else{
					$row['border_div'] = "#698299";
					$row['back_upl'] = "#02489e";
					$row['bor_upl'] = "#5a6f89";
					$row['bor_det'] = "#5a6f89";
				}
				$row['doc_kel']="no";
				$todo[] = $row;
			}

			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"todo"=>$todo,"count_todo"=>$nr_total1['row']]]);
	}elseif ($step=="doc_list_progress") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}
			
 			$sql_total2   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
					where c.id IS NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL AND b.status = 'active')$tambah_status $q order by a.created_at DESC";
			db::query($sql_total2, $rs_total2['row'], $nr_total2['row']);

 			$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
					where c.id IS NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL AND b.status = 'active')$tambah_status $q order by a.created_at DESC $limit_final";
			db::query($sql, $rs2['row'], $nr2['row']);
			$progress['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['status_exp'] ="";
				$row2['border_div'] = "#698299";
				$row2['back_upl'] = "#02489e";
				$row2['bor_upl'] = "#5a6f89";
				$row2['bor_det'] = "#5a6f89";
				$progress[] = $row2;
			}

			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"progress"=>$progress,"count_progress"=>$nr_total2['row']]]);
	}elseif ($step=="doc_list_completed") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

			$sql_total3   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (b.id_import=c.id_import)
					LEFT JOIN ". $app['table']['file_do'] ." d ON (b.id_import=c.id_import)
					where d.id IS NOT NULL AND c.id IS NULL AND (b.id IS NOT NULL AND b.status = 'active') $q order by a.created_at DESC";
			db::query($sql_total3, $rs_total3['row'], $nr_total3['row']);

 			$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (b.id_import=c.id_import)
					LEFT JOIN ". $app['table']['file_do'] ." d ON (b.id_import=c.id_import)
					where d.id IS NOT NULL AND c.id IS NULL AND (b.id IS NOT NULL AND b.status = 'active') $q order by a.created_at DESC $limit_final";
			db::query($sql, $rs3['row'], $nr3['row']);
			$completed['length']=$nr3['row'];
			while($row3 = db::fetch($rs3['row']))
			{
				$row3['status_exp'] ="";
				$row3['border_div'] = "#698299";
				$row3['back_upl'] = "#02489e";
				$row3['bor_upl'] = "#5a6f89";
				$row3['bor_det'] = "#5a6f89";
				$completed[] = $row3;
			}
			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"completed"=>$completed,"count_completed"=>$nr_total3['row']]]);
	}else{
			// isset($do)?$do = $_FILES['do']['name']:null;
			// print_r ($do);
			// exit;
			// $halaman_max 	= 2;
			// $pagenya 	 	= isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
			// $page 			= isset($_GET['page']) ? $_GET['page'] : 1 ;
			// $mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
	
############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################
			$q=null;
			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['delivery_order']." ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);            
	  		// $sqlnya = "select * from dmg_container LIMIT $mulai, $halaman";
	  		// db::query($sqlnya, $rs['row'], $nr['row']);
			// $p_expired_do = date("Y-m-d",strtotime($exp_do));

			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}
			// else{
			// 	$q = " LIMIT $mulai, $halaman";
			// }

			// $sql = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
			// 		a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
			// 		b.name name FROM ".$app['table']['beacukai']." a
			// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q LIMIT 5";
		
		/*	$sql   = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
					  a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					  b.name name,a.nilai_sptnp nilai_sptnp,a.nomer_sptnp nomer_sptnp,a.tgl_sptnp tgl_sptnp FROM ".$app['table']['beacukai']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q  LIMIT $mulai, $halaman ";*/

/*			$sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  $q  LIMIT $mulai, $halaman ";*/

/*			$sql   = "SELECT a.*,d.id id_exp,c.num_pib num_pib,c.id id,c.status_beacukai status_beacukai FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['import'] ." c ON (a.id_import=c.id)
					  LEFT JOIN ". $app['table']['delivery_order'] ." d ON (a.id_import=d.id_import)  where c.status_kiriman = '0' $q ";*/
			// $sql2   = "SELECT a.*,b.name created_by FROM ".$app['table']['import']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) where a.status_kiriman = '0' $q ";
			$sql   = "SELECT id,num_bl,num_pib,status_beacukai FROM ".$app['table']['import']." where status_kiriman = '0' $q order by created_at DESC";
/*			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,b.id id_del FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					  where (a.status_kiriman = '0' AND b.status ='expired') $q order by created_at DESC";*/
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			$user['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{

				/*$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active'");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");

				$row['date_do']=$datas['do'];
				if ($row['date_do']!=null&&$row['date_do']!="0000-00-00") {
					$row['date_do']= date("d-m-Y",strtotime($row['date_do']));
				}else{
					$row['date_do']="";
				}

				// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
				// $row['expired_do']= $datas['do'];

				// if (strtotime($datas['do']) < time()) {
				if ($datas['do'] < date("Y-m-d")) {
					isset($row['id'])?$id_exp [] = $row['id']:null;
				}
				else{
					isset($row['id'])?$id_exp_act [] = $row['id']:null;
				}*/	
				$exp = db::get_record("delivery_order" , "id_import = '".$row['id']."' AND status = 'expired' ");
				$exp2 = db::get_record("delivery_order" , "id_import = '".$row['id']."' ");
				if ($exp2['id'] =="") {
					$user[] = $row;
				}else{
					if ($exp['id'] !="") {
						$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$exp['id']."' AND status = 'active' ");
						if ($exp_date['expired_do']!="") {
							$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
							$user[] = $row;
						}else{
							$user['length'] = $user['length'] - 1;
						}
					}else{
							$user['length'] = $user['length'] - 1;
					}
				}
			}

			/*$sql2   = "SELECT a.*,c.num_pib num_pib,c.id id,c.status_beacukai status_beacukai,a.id id_exp FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['import'] ." c ON (a.id_import=c.id) where c.status_kiriman = '1' $q ";*/
			$sql2   = "SELECT id,num_bl,num_pib,status_beacukai,pfpd FROM ".$app['table']['import']." where status_kiriman = '1' $q order by created_at DESC";
			db::query($sql2, $rs2['row'], $nr2['row']);
			$user2['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$exp_1 = db::get_record("delivery_order" , "id_import = '".$row2['id']."' AND status = 'active' ");
				$exp_date2 = db::get_record("expired_do" , "id_delivery_order = '".$exp_1['id']."' AND status = 'active' ");
				if ($exp_date2['expired_do']!="") {
					$row2['expired_date'] = date("d-m-Y",strtotime($exp_date2['expired_do']));
				}
				$user2[] = $row2;
			}
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					// if ($user) {
			$response["message"] = "Pelayaran Berhasil Termuat";
			// echo json_encode($sql);
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"progress"=>$user,"count_progress"=>$nr['row'],"completed"=>$user2,"count_completed"=>$nr2['row'],"id_user"=>$app['me']['id']]]);
						// echo json_encode($response);
					// }elseif ($user==0) {
					// 	$response["error"]=FALSE;
					// 	$response["message"] = "Data Masih kosong";
					// 	// echo json_encode($response);
				 //        app::response("404", ["progress"=>$response]);
	    //     			// app::response("200", ["error"=>FALSE,"items"=>$response]);
					// 	// echo json_encode($sql);
					// }else{
					// 	$response["error"]=TRUE;
					// 	$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
					// 	// echo json_encode($response);
				 //        app::response("500", ["progress"=>$response]);
					// 	// echo json_encode($response);
					// }
	}
}elseif($act=="pelabuhan"){
		$app['me'] 	= app::unserialize64($_SESSION[$app['session']]);

		// $form = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);

	if ($step=="simpan") {

		// isset($p_date_receipt_do)?$p_date_receipt_do=$p_date_receipt_do:null;
		isset($get_do)?$p_date_receipt_do=$get_do:null;
		// isset($p_expired_do)?$p_expired_do=$p_expired_do:null;
		isset($exp_do)?$p_expired_do=$exp_do:null;
		isset($me)?$me=$me:null;

		// isset($eir)?$eir = $_FILES['eir']['name']:null;
		isset($filenya)?$eir = $filenya:null;
		isset($kelengkapan)?$kelengkapan = $_FILES['kelengkapan']['name']:null;

		isset($p_no_do)?$p_no_do = $p_no_do:$p_no_do=null;
		isset($id_imp)?$id_imp = $id_imp:$id_imp=null;

		$form_imp = db::get_record("import", "id", $id_imp);
		// if ($form_imp['status_beacukai'] =="SPJM") {

			if (strtotime($p_expired_do) < time()) {
				$statusnya_column=",status";
				$status_do = ",'expired'";	
			}
			$id 			  = rand(1, 100).date("dmYHis");
			$ids 			  = rand(1, 100).date("dmYHis");
			$id_c 			  = rand(1, 100).date("dmYHis");

//			if ($_FILES["attachment"]["name"]) {
//				$nama_gambar= $_FILES["attachment"]["name"];
				/*move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/sppb/". $_FILES["attachment"]["name"].date("dmYsHis"));*/
//			    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/sppb/". $nama_gambar);
//			}
			app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
			$p_date_receipt_do 	= date("Y-m-d",strtotime($p_date_receipt_do));
			$p_expired_do 		= date("Y-m-d",strtotime($p_expired_do));
			// $sql = "insert into ".$app['table']['pelabuhan']."
			// 		(id, date_receipt_do, expired_do, created_by, created_at, id_import, eir,kelengkapan$statusnya_column) values
			// 		('$id', '$p_date_receipt_do', '$p_expired_do', '$me', now(), '$id_imp', '$eir', '$kelengkapan'$status_do)";


			// $sql = "insert into ".$app['table']['pelabuhan']."
			// 		(id, created_by, created_at, id_import, eir, kelengkapan) values
			// 		('$id', '$me', now(), '$id_imp', '$eir', '$kelengkapan')";

/*			$sql = "insert into ".$app['table']['pelabuhan']."
					(id, created_by, created_at, id_import, eir) values
					('$id', '$me', now(), '$id_imp', '$eir')";*/
			#$sql = "insert into ".$app['table']['pelabuhan']."
			#		(id, created_by, created_at, id_import, eir,date_receipt_do,expired_do) values
			#		('$id', '$me', now(), '$id_imp', '$nama_gambar','$p_date_receipt_do','$p_expired_do')";
			$cek_pelabuhan =db::lookup("id","pelabuhan","id_import",$id_imp);
			if (!$cek_pelabuhan) {
				// $sql = "insert into ".$app['table']['pelabuhan']."
				// 	(id, created_by, created_at, id_import, eir, status_eir) values
				// 	('$id', '$me', now(), '$id_imp', '$nama_gambar', '$status_eir')";
				$sql = "insert into ".$app['table']['pelabuhan']."
					(id, created_by, created_at, id_import, status_eir) values
					('$id', '$me', now(), '$id_imp', '$status_eir')";
			}else{
				$sql = "UPDATE ".$app['table']['pelabuhan']."
						SET status_eir = $status_eir,update_at=now()
						WHERE id = '$cek_pelabuhan' ";
			}
			
			// $sqlsss ="insert into ".$app['table']['notif']." 
			// 		(id, id_rule, id_import, status, pesan, created_by, created_at) values
			// 		('$id_c', '3220082018161340', '$id_imp', 1, 'SPPB dengan pib  = ".$form_imp['num_pib']."  Telah di urus','$me',now())";
			$sqlsss ="insert into ".$app['table']['notif']." 
					(id, id_rule, id_import, status, pesan, created_by, created_at) values
					('$id_c', '3220082018161340', '$id_imp', 1, 'Kiriman dengan pib  = ".$form_imp['num_pib']." Bisa di Ubah Statusnya ','$me',now())";
			// $sqlsss ="insert into ".$app['table']['notif']." 
			// 		(id, id_rule, id_import, status, pesan, created_by, created_at) values
			// 		('$id_c', '3220082018161340', '$id_imp', 1, 'SPPB dengan pib  = ".$form_imp['num_pib']."  Telah di urus','$me',now())";
			db::qry($sql);
			db::qry($sqlsss);

			$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
			db::query($sql_lang, $rs['lang'], $nr['lang']);
			while ($row321	= db::fetch($rs['lang'])) {
				// $sqlss ="insert into ".$app['table']['status']." 
				// 		(id,id_import,positions,created_by,created_at,lang) values
				// 		('$id_c','$id_imp','".app::getliblang('proses_pelabuhan_eir',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
	/*			$sqlss ="UPDATE ".$app['table']['status']." SET positions = '".app::getliblang('proses_pelabuhan_eir',$row['alias'])."',updated_by = '$me',updated_at = now() WHERE id_import = '$id_imp' ";*/
/*			$sqlss ="UPDATE ".$app['table']['status']." set 
					positions=concat('".app::getliblang('proses_pelabuhan_eir',$row321['alias']).",','<br>',positions),
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
					WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' ";
				db::qry($sqlss);*/
			}
	/*		$message  .= "<p>Date Receipt DO :<b>". $p_date_receipt_do ."</b></p>";
			$message  .= "<p>Expired DO : <b>". $p_expired_do ."</b></p>";
			// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
			$message .= "<p>Terima kasih</p>";
			$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
						   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
						   where a.id_rule ='5403092018114622' ";
			db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
			while($row = db::fetch($rs['sql_bea_cukai'])){
				app::sendmail($row['email'], "Ada dokumen import masuk", $message);
			}*/
				// db::qry($sqlss);
				$response["error"]	 = FALSE;
				// $response["id"] = $user["unique_id"];
				$response["message"] = "Dokumen Eir Berhasil Tersimpan";
		        // app::response("200", ["error"=>FALSE,"items"=>$response]);
		        app::response("200", ["error"=>0,"data"=>["message"=>$response["message"]]]);
				exit;
				// echo json_encode($response);
	    // }

// 	    else{
// /*				isset($p_date_receipt_do)?$p_date_receipt_do=$p_date_receipt_do:null;
// 				isset($p_expired_do)?$p_expired_do=$p_expired_do:null;
// 				isset($id_idv_ord)?$id_idv_ord=$id_idv_ord:null;
// 				isset($me)?$me=$me:null;
// 				isset($eir)?$eir = $_FILES['eir']['name']:null;

// 				isset($filenya)?$filenya = $_FILES['filenya']['name']:$filenya="";

// 				isset($kelengkapan)?$kelengkapan = $_FILES['kelengkapan']['name']:null;*/

// 				$form_imp = db::get_record("import", "id", $id_imp);
// 				// app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
// 				$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
// 				$p_expired_do = date("Y-m-d",strtotime($p_expired_do));
// 				$id 			  = rand(1, 100).date("dmYHis");

// 				// $sql = "update ". $app['table']['pelabuhan'] ."
// 				// 		updated_by 			= '$me',
// 				// 		eir 		 		= '$eir',
// 				// 		kelengkapan			= '$kelengkapan',
// 				// 		updated_at 			= now()
// 				// 		where id_import = '$id_imp'";
// 				if ($form_imp['status_sppb'] == '1') {
// 					// $sql = "update ". $app['table']['pelabuhan'] ." set
// 					// 		updated_by 		= '$me',
// 					// 		kelengkapan		= '$kelengkapan',
// 					// 		updated_at 		= now()
// 					// 		where id_import = '$id_imp'";
// 					$sql = "insert into ".$app['table']['pelabuhan']."
// 						(id, created_by, created_at, id_import, eir,date_receipt_do,expired_do) values
// 						('$id', '$me', now(), '$id_imp', '$eir','$p_date_receipt_do','$p_expired_do')";
// 					db::qry($sql);
// 					db::qry("UPDATE ". $app['table']['import']." set status_kiriman ='1' where id = '".$row['id_imp']."' ");
// 					// if(!empty($p_expired_do)){
// 						$ids = rand(1, 100).date("dmYHis");
// 						$id_notif = rand(1, 100).date("dmYHis");
// 						$id_notif2 = rand(1, 100).date("dmYHis");
// 						// $sqls = "update ".$app['table']['expired_do']." set
// 						// 		 status = 'inactive' where id_delivery_order = '$id_idv_ord' ";
// 						// db::qry($sqls);
// 						// $sqlss = "insert into ".$app['table']['expired_do']."
// 						// 		(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id_idv_ord', '$p_expired_do','$me',now())";
// 						// db::qry($sqlss);
// 						$sqls32 = "insert into ".$app['table']['notif']." 
// 									(id,id_rule,id_import,status,pesan,created_by,created_at) values
// 									('$id_notif','3220082018161340', '$id_imp', 1, 'Kiriman dengan pib : ".$form_imp['num_pib'].", bisa dijadwalkan','$me',now())";
// 						db::qry($sqls32);
// 						// $sqls32123 = "insert into ".$app['table']['notif']." 
// 						// 			(id,id_rule,id_import,status,pesan,created_by,created_at) values
// 						// 			('$id_notif2','5703092018105342','$id_imp',1,'Kiriman dengan no pib : ".$form_imp['num_pib']." Dokumennya telah di ubah, Bisa di Ubah Statusnya','$me',now())";
// 						// db::qry($sqls32123);
						
// 						$path_kelengkapan = $app['doc_path']."/out_cont/".$kelengkapan;

// 						$message  .= "<p>Dokumen Kelengkapan Container dengan no pib : ".$form_imp['num_pib']."  </p>";
// 						// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
// 						$message .= "<p>Terima kasih</p>";
// 						$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
// 									   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
// 									   where a.id_rule ='5703092018105342' ";
// 						db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
// 						while($row = db::fetch($rs['sql_bea_cukai'])){
// 							app::sendmail($row['email'], "Container diantarkan", $message,$path_kelengkapan);
// 						}

// 					// }
// 						$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
// 						db::query($sql_lang, $rs['lang'], $nr['lang']);
// 						while ($row321	= db::fetch($rs['lang'])) {
// 							// $sqlss ="insert into ".$app['table']['status']." 
// 							// 		(id,id_import,positions,created_by,created_at,lang) values
// 							// 		('$id_c','$id_imp','".app::getliblang('proses_pelabuhan_eir',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
// 				/*			$sqlss ="UPDATE ".$app['table']['status']." SET positions = '".app::getliblang('proses_pelabuhan_eir',$row['alias'])."',updated_by = '$me',updated_at = now() WHERE id_import = '$id_imp' ";*/
// 						$sqlss ="UPDATE ".$app['table']['status']." set 
// 								positions=concat('".app::getliblang('proses_pelabuhan_kelengkapan',$row321['alias']).",','<br>',positions),
// 								updated_by = '". $app['me']['id'] ."',
// 								updated_at = now()
// 								WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' ";
// 							db::qry($sqlss);
// 						}

// 						   $response["error"] = FALSE;
// 						   // $response["id"] = $user["unique_id"];
// 						   $response["message"] = "Pengurusan Kelengkapan Kirim Berhasil terbuat";
// 						   // echo json_encode($response);
// 					       app::response("200", ["error"=>FALSE,"items"=>$response]);
// 						   // echo json_encode($sql);
// 				}else{
// 						   $response["error"] = FALSE;
// 						   // $response["id"] = $user["unique_id"];
// 						   $response["message"] = "Belum Merubah status sppb";
// 						   // echo json_encode($response);
// 					       app::response("200", ["error"=>FALSE,"items"=>$response]);
// 						   // echo json_encode($sql);

// 				}
// 			}else{
// 						   $response["error"] = TRUE;
// 						   // $response["id"] = $user["unique_id"];
// 						   $response["error_message"] = "Terjadi Kesalahan Saat Mengupdate";
// 						   // echo json_encode($response);
// 					        app::response("500", ["items"=>$response]);
// 			}
// 	    }
	}elseif ($step=="simpan_dokumen") {	
		if (isset($id_imp)) {

			isset($p_date_receipt_do)?$p_date_receipt_do=$p_date_receipt_do:null;
			isset($p_expired_do)?$p_expired_do=$p_expired_do:null;
			isset($id_idv_ord)?$id_idv_ord=$id_idv_ord:null;
			isset($me)?$me=$me:null;
			isset($eir)?$eir = $_FILES['eir']['name']:null;

			isset($filenya)?$filenya = $_FILES['filenya']['name']:$filenya="";

			// isset($kelengkapan)?$kelengkapan = $_FILES['kelengkapan']['name']:null;
			isset($filenya)?$kelengkapan = $filenya:null;

			$form_imp = db::get_record("import", "id", $id_imp);
			// app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
			$p_expired_do = date("Y-m-d",strtotime($p_expired_do));

			$ids = rand(1, 100).date("dmYHis");
			$ids2 = rand(1, 100).date("dmYHis");
			// $data_positions = db::lookup("positions","status","id_import ='$id_importnya' AND lang = '".$row321['alias']."' AND id_container = '$id' ");
			$cek_beacukai = db::lookup("1","beacukai","id_import",$id_imp);
			// if ($cek_beacukai != "1" && $cek_beacukai != 1) {
			// 	db::qry("INSERT INTO ".$app['table']['beacukai']."
			// 	(id, id_import, created_by, created_at,kirim) values
			// 	('$ids', '$id_imp','". $me ."', now(),2)");
			// }
			$cek_pelabuhan = db::lookup("1","pelabuhan","id_import",$id_imp);
			if ($cek_pelabuhan != "1" && $cek_pelabuhan != 1) {
				db::qry("INSERT INTO ".$app['table']['pelabuhan']."
				(id, id_import, created_by, created_at) values
				('$ids2', '$id_imp','". $me ."', now())");
			}
			// $sql = "update ". $app['table']['pelabuhan'] ."
			// 		updated_by 			= '$me',
			// 		eir 		 		= '$eir',
			// 		kelengkapan			= '$kelengkapan',
			// 		updated_at 			= now()
			// 		where id_import = '$id_imp'";
			if ($form_imp['status_sppb'] == '1') {

			/*if ($_FILES["attachment"]["name"]) {
				$nama_gambar= $_FILES["attachment"]["name"];
			    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/out_cont/". $nama_gambar);
			}*/
			$sql = "update ". $app['table']['pelabuhan'] ." set
					updated_by 		= '$me',
					updated_at 		= now()
					where id_import = '$id_imp'";
			db::qry($sql);
			// db::qry("UPDATE ". $app['table']['import']." set status_kiriman ='1' where id = '".$row['id_imp']."' ");
			// if(!empty($p_expired_do)){
				$ids = rand(1, 100).date("dmYHis");
				$id_notif = rand(1, 100).date("dmYHis");
				$id_notif2 = rand(1, 100).date("dmYHis");
				// $sqls = "update ".$app['table']['expired_do']." set
				// 		 status = 'inactive' where id_delivery_order = '$id_idv_ord' ";
				// db::qry($sqls);
				// $sqlss = "insert into ".$app['table']['expired_do']."
				// 		(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id_idv_ord', '$p_expired_do','$me',now())";
				// db::qry($sqlss);
				$sqls32 = "insert into ".$app['table']['notif']." 
							(id,id_rule,id_import,status,pesan,created_by,created_at) values
							('$id_notif','3220082018161340', '$id_imp', 1, 'Kiriman dengan pib : ".$form_imp['num_pib'].", bisa dijadwalkan','$me',now())";
				db::qry($sqls32);
				// $sqls32123 = "insert into ".$app['table']['notif']." 
				// 			(id,id_rule,id_import,status,pesan,created_by,created_at) values
				// 			('$id_notif2','5703092018105342','$id_imp',1,'Kiriman dengan no pib : ".$form_imp['num_pib']." Dokumennya telah di ubah, Bisa di Ubah Statusnya','$me',now())";
				// db::qry($sqls32123);
				
				// $path_kelengkapan = $app['doc_path']."/out_cont/".$kelengkapan;

				$message  .= "<p>Dokumen Kelengkapan Container dengan no pib : ".$form_imp['num_pib']."  </p>";
				// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
				$message .= "<p>Terima kasih</p>";
				$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
							   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
							   where a.id_rule ='5703092018105342' ";
				db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
				while($row = db::fetch($rs['sql_bea_cukai'])){
					// app::sendmail($row['email'], "Container diantarkan", $message,$path_kelengkapan);
					app::sendmail($row['email'], "Container diantarkan", $message,"");
				}

			// }
				$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
				db::query($sql_lang, $rs['lang'], $nr['lang']);
				while ($row321	= db::fetch($rs['lang'])) {
					// $sqlss ="insert into ".$app['table']['status']." 
					// 		(id,id_import,positions,created_by,created_at,lang) values
					// 		('$id_c','$id_imp','".app::getliblang('proses_pelabuhan_eir',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
		/*			$sqlss ="UPDATE ".$app['table']['status']." SET positions = '".app::getliblang('proses_pelabuhan_eir',$row['alias'])."',updated_by = '$me',updated_at = now() WHERE id_import = '$id_imp' ";*/
/*				$sqlss ="UPDATE ".$app['table']['status']." set 
						positions=concat('".app::getliblang('proses_pelabuhan_kelengkapan',$row321['alias']).",','<br>',positions),
						updated_by = '". $app['me']['id'] ."',
						updated_at = now()
						WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' ";
					db::qry($sqlss);*/
				}


############# status ################
/* 	$container_status 	= "SELECT id,num_container FROM ". $app['table']['container'] ." WHERE id_import = '".$id_imp."' ";
	db::query($container_status, $rs['lang'], $nr['lang']);*/
	// while ($row456	= db::fetch($rs['lang'])) {



		// echo "asdassadd";exit;
		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs2['lang'], $nr2['lang']);
		while ($row321	= db::fetch($rs2['lang'])) {
		// echo "test21321";exit;
/*			$data_positions = db::lookup("positions","status","id_import ='$id_importnya' AND lang = '".$row321['alias']."' AND id_container = '$id' ");

			db::qry("DELETE FROM ". $app['table']['status'] ." WHERE id_import ='$id_importnya' AND lang = '".$row321['alias']."' AND id_container = '$id' ");

			$id_c2 = rand(1, 100).date("dmYHis");
	 		$sqlss321 ="insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang) values
						('$id_c2','$idm','$id',concat('".app::getliblang('proses_jadwal_truck',$row321['alias'])." : <b>".format_tgl_bahasa($date_kirim,$row321['alias'])."</b>,',$data_positions),'".$app['me']['id']."',now(),'".$row['alias']."')";
			db::qry($sqlss321);*/
/*			$sqlssssss321 ="UPDATE ".$app['table']['status']." set 
				positions=concat('".app::getliblang('proses_out_pel',$row321['alias']).",',positions),
				updated_by = '". $app['me']['id'] ."',
				updated_at = now()
				WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' AND id_container = '".$row456['id']."' ";*/
/*			$sqlssssss321 ="UPDATE ".$app['table']['status']." set 
				positions=concat('".app::getliblang('proses_out_pel',$row321['alias']).",',positions),
				updated_by = '". $app['me']['id'] ."',
				updated_at = now()
				WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' ";*/

			$id_c321 = rand(101, 200).date("dmYHis");
			$multiple_insert .= "('$id_c321','$id_imp','".app::getliblang('proses_out_pel',$row321['alias'])."','".$me."',now(),'".$row321['alias']."','".$row456['num_container']."'),";
				// db::qry($sqlssssss321);
		}



	// }

	$multiple_insert = rtrim($multiple_insert,",");	
/* 	$sqlss321 = "insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values $multiple_insert";*/
 	$sqlss321 = "insert into ".$app['table']['status']." 
						(id,id_import,positions,created_by,created_at,lang,num_container) values $multiple_insert";
	db::qry($sqlss321);

	$num_bil = db::lookup("num_bl","import","id",$id_imp);
	$cek_customer = db::lookup("id_customer","import","id",$id_imp);
	$rs['devices'] = db::get_recordset("user_device","id_user='".$cek_customer."' AND status='active'");
	while($user_devices = db::fetch($rs['devices'])){
		$datapush = array(
			"to" => db::lookup("token","device","id",$user_devices['id_device']),
			"notification" => array(
				"title" => "Status Updated",
				"text" => "$num_bil : ".app::getliblang('proses_out_pel',"id"),
				"sound" => "default"
			),
			'data' => array(
				'method' => 'change_status',
				'id' => $id_imp,
				"title" => "Status Updated",
				"body" => "$num_bil : ".app::getliblang('proses_out_pel',"id")
			)
		);
		app::pushNotifClientSend($datapush);
	}


############# / status ################


					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   // echo "asdasdasdsd";exit;
					   $response["message"] = "Data Berhasil Tersimpan";
					   // echo json_encode($response);
				       // app::response("200", ["error"=>FALSE,"items"=>$response]);
				       app::response("200", ["error"=>0,"data"=>["message"=>$response["message"]]]);
					   // echo json_encode($sql);
			}else{
					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Belum Merubah status sppb";
					   // echo json_encode($response);
				       app::response("200", ["error"=>FALSE,"data"=>["message"=>$response["message"]]]);
					   // echo json_encode($sql);

			}
		}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
					   $response["error_message"] = "Terjadi Kesalahan Saat Mengupdate";
					   // echo json_encode($response);
				        app::response("500", ["message"=>$response["message"]]);
		}
	}elseif ($step=="edit"){	

    /*echo json_encode(['files'=>$_FILES,'Request'=>$_REQUEST,'path'=>$app['doc_path']."/". $_FILES["attachment"]["name"]]);
    exit;*/
		if (isset($id_imp)) {
				isset($p_date_receipt_do)?$p_date_receipt_do=$p_date_receipt_do:null;
				isset($p_expired_do)?$p_expired_do=$p_expired_do:null;
				isset($id_idv_ord)?$id_idv_ord=$id_idv_ord:null;
				isset($me)?$me=$me:null;
				isset($sppb)?$sppb = $_FILES['sppb']['name']:null;
				// isset($filenya)?$filenya = $_FILES['filenya']['name']:$filenya="";
				isset($filenya)?$filenya = $filenya:$filenya="";
/*				$form_imp = db::get_record("import", "id", $id);
				if (strtotime($p_expired_do) >= time()) {
					$status_do = ",status = 'active' ";
				}else{
					$status_do = ",status = 'expired' ";	
				}*/

				////////////////// MOVE file
// 				if ($_FILES["attachment"]["name"] !="") {
// 					$delete_file = db::get_record("pelabuhan","id_import",$id_imp);
// 					@unlink($app['doc_path']."/sppb/". $delete_file['eir']);
// 					$nama_gambar= $_FILES["attachment"]["name"];
// /*				    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/sppb/".$nama_gambar);*/
// 				    if (move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/sppb/".$nama_gambar)) {
// 					    $abc="eir 	= '$nama_gambar',";
// 				    }else{
// 				    	// echo "gagal ganti gambar";
// 	   				    $response["message"] = "Gambar Gagal Di Upload";
// 				        app::response("404", ["error"=>TRUE,"items"=>$response]);
// 				    	exit;
// 				    }
// 				}
// 				if ($_FILES["attachment2"]["name"] !="") {
// 					$delete_file = db::get_record("pelabuhan","id_import",$id_imp);
// 					@unlink($app['doc_path']."/out_cont/". $delete_file['kelengkapan']);
// 					$nama_gambar2=$_FILES["attachment2"]["name"];
// /*					move_uploaded_file($_FILES["attachment2"]["tmp_name"], $app['doc_path']."/out_cont/".$nama_gambar2);*/
// 					if (move_uploaded_file($_FILES["attachment2"]["tmp_name"], $app['doc_path']."/out_cont/".$nama_gambar2)) {
// 					    // $abc="eir 	= '$nama_gambar',";
// 						$def="kelengkapan 	= '$nama_gambar2',";
// 				    }else{
// 				    	// echo "gagal ganti gambar";
// 	   				    $response["message"] = "Gambar Gagal Di Upload";
// 				        app::response("404", ["error"=>TRUE,"items"=>$response]);
// 				    	exit;
// 			    	}
// 				}
////////////////////////////////////////////
				// $total = db::lookup('COUNT(id)', 'expired_do', 'id_delivery_order = '.$id_idv_ord);
				// if($total < 3) $exp_date2 = "date_receipt_do 	= '$p_date_receipt_do',";
				app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
				$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
				$p_expired_do = date("Y-m-d",strtotime($p_expired_do));
/*				$sql = "update ". $app['table']['pelabuhan'] ." set
						updated_by 			= '$me',
						date_receipt_do 	= '$get_do',
						expired_do	 		= '$exp_do',
						$abc
						$def
						updated_at 			= now()
						where id_import 	= '$id_imp'";*/

				$cek_pelabuhan = db::lookup("id","pelabuhan","id_import",$id_imp);
				if (!$cek_pelabuhan) {
					$id_pel = rand(1, 100).date("dmYHis");
					$sql2 = "insert into ".$app['table']['pelabuhan']."
							(id, created_by, created_at, id_import,status_eir) values
							('$id_pel', '$me', now(), '$id_imp','$status_eir')";
					db::qry($sql2);
				}
				$sql = "update ". $app['table']['pelabuhan'] ." set
						updated_by 			= '$me',
						status_eir 			= '$status_eir',
						updated_at 			= now()
						where id_import 	= '$id_imp'";
				db::qry($sql);
			// if(!empty($p_expired_do)){
				$ids = rand(1, 100).date("dmYHis");
				$id_notif = rand(1, 100).date("dmYHis");
				$sqls32 = "insert into ".$app['table']['notif']." 
							(id,id_rule,id_import,status,pesan,created_by,created_at) values
							('$id_notif','3220082018161340','$id_imp',1,'Kiriman dengan no pib : ".$form_imp['num_pib']." Dokumennya telah di ubah, Bisa di Ubah Statusnya','$me',now())";
				db::qry($sqls32);
				/*$message  .= "<p>Expired DO dengan no pib : ".$form_imp['num_pib'].", Telah diperpanjang sampai : <b>". $p_expired_do ."</b></p>";
				// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
				$message 	  .= "<p>Terima kasih</p>";
				$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
							   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
							   where a.id_rule ='5703092018105342' ";
				db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
				while($row = db::fetch($rs['sql_bea_cukai'])){
					app::sendmail($row['email'], "dokumen delivery order masuk", $message);
				}*/
			// }
					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Pelabuhan Order Berhasil Teredit";
					   // echo json_encode($response);
				       // app::response("404", ["error"=>FALSE,"items"=>$response]);
				       app::response("200", ["error"=>FALSE,"data"=>["message"=>$response["message"]]]);
					   // echo json_encode($sql);
		}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Terjadi Kesalahan Saat Mengupdate";
					   // echo json_encode($response);
				        app::response("500", ["items"=>$response]);
		}
	}elseif ($step=="detail"){	
	if ($id) {
			// $sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  where a.id = '$id' ";
			// $sql   = "SELECT a.*,b.name created_by FROM ".$app['table']['import']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
			// 		  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (c.id_import=a.id)  where a.id = '$id' ";
		
			/*$sql   = "SELECT a.qty qty, a.num_container num_container,a.note note, a.value value, b.num_pib pib, b.num_bl num_bl, b.name_ship name_ship, b.eta eta,b.id_customer id_customer FROM ".$app['table']['container']." a
					  LEFT JOIN ". $app['table']['import'] ." b ON (a.id_import=b.id)  where b.id = '$id' limit 1 ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				$id_customer 	 =	db::get_record("customer","id",$row['id_customer']);
				$row['customer'] = $id_customer['name'];
				$user[] = $row;
			}*/

					$sqlupt		  = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '".$id_rule."' ";
					db::qry($sqlupt);
					$sql   = "SELECT  tgl_nopen,id,num_pib, status_beacukai, num_bl,nopen,name_ship,eta,id_customer,tanggal_sptnp, nomor_sptnp,nilai_sptnp, amount_payment, pfpd , status_sppb FROM ". $app['table']['import'] ."  where id = '$id' limit 1 ";
					$nonya = $mulai+1;
					// $q ORDER BY a.lang,a.reorder";
					db::query($sql, $rs['row'], $nr['row']);
					while($row = db::fetch($rs['row']))
					{
						$record_pelabuhan = db::get_record("pelabuhan","id_import",$id);
						$record_pelayaran = db::get_record("delivery_order","id_import",$id);
						$file_kel = db::lookup("id","file_kel","id_import",$id);
						$row["status_expired"] = $record_pelayaran['status'];
						$row["do_file"]= $record_pelayaran['do'];
						$sql2   = "SELECT * FROM ".$app['table']['container']." where id_import ='$id' ";
						// $nonya = $mulai+1;
						// $q ORDER BY a.lang,a.reorder";
						db::query($sql2, $rs2['row'], $nr2['row']);
						$test["length"]= $nr2['row'];
						while($row2 = db::fetch($rs2['row']))
						{
							// $row2['url_photo'] = api::urlApiFile_doc_path("sppb",$row2['name']);
							$row2['surat_jalan_url']=api::urlApiFile_doc_path("do_pdf",$row2['surat_jalan']);
							$test[] = $row2;
						}	
						$row['isi_container'] 		 = $test;
						
						if ($record_pelabuhan['id'] !="") {
							$row['isi_act'] 			 = $record_pelabuhan;
						}else{
							$row['isi_act'] 			 = array('eir' => "" );
							// $row['isi_act'] 			 = "aslkdj;asdsakd";
						}

							// $row['sql'] = $sql2;
						$row['eta_format'] 			 = format_tgl($row['eta']);
						$row['eta'] 			 = format_tgl($row['eta']);
						$id_customer 	 			 =	db::get_record("customer","id",$row['id_customer']);
						$row['customer'] 			 = $id_customer['name'];
						if ($row['tanggal_sptnp'] !="" AND $row['tanggal_sptnp'] !="1970-01-01" AND $row['tanggal_sptnp'] !="0000-00-00") 
							$row['tanggal_sptnp_format'] = format_tgl($row['tanggal_sptnp']);
						else $row['tanggal_sptnp_format'] ="";
						$row['format_rupiah'] = rupiah($row['amount_payment']);
						// if ($file_kel || ($row['status_sppb']==1)) {
						if ($row['status_sppb']==1) {
							$row['dok_kel']="iya";						
						}else{
							$row['dok_kel']="no";	
						}
						$user[] = $row;
					}

			$record_pelayaran = db::get_record("delivery_order","id_import",$id);
			$record_pelayaran['date_receipt_do'] = format_tgl($record_pelayaran['date_receipt_do']);
			$record_pelayaran['expired_date'] = format_tgl($record_pelayaran['expired_date']);
			$sql   = "SELECT id,name,id_import FROM ". $app['table']['file_eir'] ."
					  where id_import = '$id' and status = 'active' order by created_at DESC ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs2['row'], $nr2['row']);
			$file_eir['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['url_photo'] = api::urlApiFile_doc_path("sppb",$row2['name']);
				$file_eir[] = $row2;
			}

			$sql   = "SELECT id,name,id_import FROM ". $app['table']['file_do'] ."
					  where id_import = '$id' and status = 'active' order by created_at DESC ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs3['row'], $nr3['row']);
			$file_do['length']=$nr3['row'];
			while($row3 = db::fetch($rs3['row']))
			{
				$row3['url_photo'] = api::urlApiFile_doc_path("do",$row3['name']);
				$file_do[] = $row3;
			}

			$sql   = "SELECT id,name,id_import FROM ". $app['table']['file_kel'] ."
					  where id_import = '$id' and status = 'active' order by created_at DESC ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs4['row'], $nr4['row']);
			$file_test['length']=$nr4['row'];
			while($row4 = db::fetch($rs4['row']))
			{
				$row4['url_photo'] = api::urlApiFile_doc_path("out_cont",$row4['name']);
				$file_test[] = $row4;
			}

			$sql   = "SELECT id,name,id_import FROM ". $app['table']['file_bea'] ."
					  where id_import = '$id' and status = 'active' order by created_at DESC ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs5['row'], $nr5['row']);
			$file_bea['length']=$nr5['row'];
			while($row5 = db::fetch($rs5['row']))
			{
				$row5['url_photo'] = api::urlApiFile_doc_path("sppb",$row5['name']);
				$file_bea[] = $row5;
			}

			if ($user) {
				$response["message"] = "Detail Berhasil Termuat";
				// echo json_encode($sql);
    			// app::response("200", ["error"=>FALSE,"items"=>$user]);
    			app::response("200", ["error"=>0,"data"=>["destination"=>$user],"data_pel"=>$record_pelayaran,"file_eir"=>$file_eir,"file_kel"=>$file_test,"file_bea"=>$file_bea,"file_do"=>$file_do,"response"=>$response]);
				// echo json_encode($response);
			}elseif ($user==0) {
				$response["error"]=FALSE;
				$response["message"] = "Data Masih kosong";
				// echo json_encode($response);
		        app::response("404", ["error"=>0,"data"=>["destination"=>$user],"data_pel"=>$record_pelayaran,"file_eir"=>$file_eir,"file_kel"=>$file_test,"file_bea"=>$file_bea,"file_do"=>$file_do,"response"=>$response]);
    			// app::response("200", ["error"=>FALSE,"items"=>$response]);
				// echo json_encode($sql);
			}else{
				$response["error"]=TRUE;
				$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
				// echo json_encode($response);
		        app::response("500", ["error"=>500,"data"=>["destination"=>$user],"response"=>$response]);
				// echo json_encode($response);
			}
		}
	}elseif ($step=="revisi") {	
	if ($id_imp) {

/*			$sqlupt = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '".$id_rule."' ";
			db::qry($sqlupt);*/

			$data_import = db::get_record("import","id",$id_imp);
			app::mq_encode('p_alasan_rev');
			$sql = "update ". $app['table']['pelabuhan'] ."
					set 
					revisi 			= '1',
					alas_rev 		= '$p_alasan_rev'
					where id_import = '$id_imp'";
			db::qry($sql);
			
			$id321 = rand(1, 100).date("dmYHis");
			$sqlsss321 ="insert into ".$app['table']['notif']." 
					(id, id_rule, id_import, status, pesan, created_by, created_at) values
					('$id321', '5403092018114622', '$id_imp', 1, '".$data_import['num_pib']." : ".$p_alasan_rev."','$me',now())";
			db::qry($sqlsss321);

			$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					// "to" => db::lookup("token","device","id",$user_devices['id_device']),
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Pelabuhan",
						"text" => "Revisi PIB ".$data_import['num_pib']." : ".$p_alasan_rev,
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelabuhan',
						'ncr_id' => $id_imp,
						"title" => "Admin Pelabuhan/EIR",
						"body" => "Revisi PIB ".$data_import['num_pib']." : ".$p_alasan_rev
					)
				);
				app::pushNotifSend($datapush);
			}
				app::response("200", ["error"=>0,"message"=>"Berhasil_tersimpan"]);
		}else{
			$response["error"]=TRUE;
			$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
			// echo json_encode($response);
	        app::response("500", ["error"=>500,"data"=>["destination"=>$user],"response"=>$response]);
		}
	}elseif ($step=="doc_list") {	
############################ EXPIRED DELIVERY ORDER ####################################
			// $tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			// if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
			// 	$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
			// 		while($row = db::fetch($datas['expired_data'])){
			// 		$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
			// 		$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
			// 			if ($id_importnya321123['status_kiriman'] == "0") {
			// 				if (strtotime($row['expired_do']) < time()) {
			// 					$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
			// 						$id_notif   = rand(1, 100).date("dmYHis");
			// 						$sqls = "insert into ".$app['table']['notif']." 
			// 								(id,id_rule,id_import,status,pesan,created_by,created_at) values
			// 								('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
			// 						db::qry($sqls);
			// 				}else{
			// 					$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
			// 				}
			// 			}
			// 		}

			// 		$id_exp = implode("','", $id_exp321);
			// 		$id_exp_act = implode("','", $id_exp_act321);
			// 		$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
			// 		$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
			// 		db::qry($sql321123);
			// 		db::qry($sql1231321);
			// 		db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
			// 	}
########################################################################################
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				if (date("Y-m-d",strtotime($p_search)) != 1970-01-01 && date("Y-m-d",strtotime($p_search)) != "1970-01-01") {
					$q = "AND (a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
				}else{
					$q = "AND (a.num_pib LIKE '%".$p_search."%') ";
				}
			}

			if ($filter_status) {
				if ($filter_status !="ALL") {
					$tambah_status=" AND a.status_beacukai = '$filter_status' ";
				}
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

			if ($p_category == "todo") {
				$tampilnya ="todo";
			}elseif($p_category == "progress"){
				$tampilnya = "progress";
			}elseif($p_category == "completed"){
				$tampilnya = "completed";
			}else{
				$tampilnya = "all";
				$p_category = "all";
			}
			if ($p_category == "todo" || $p_category=="all") {

/*	   		  	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,a.tgl_nopen FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_eir'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['pelabuhan'] ." d ON (a.id=d.id_import)	
					LEFT JOIN ". $app['table']['file_kel'] ." e ON (a.id=e.id_import)
			 where ((a.status_beacukai = 'SPJM' AND (c.`id` IS NULL OR (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)))) OR (a.status_beacukai != 
			 'SPJM' AND (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)) )) AND (a.status_kiriman = '0')$tambah_status $q ORDER BY a.created_at DESC $limit_final";*/
	   		  	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,a.tgl_nopen FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_eir'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['pelabuhan'] ." d ON (a.id=d.id_import)	
					LEFT JOIN ". $app['table']['file_kel'] ." e ON (a.id=e.id_import)
			 where a.status_kiriman = '0' AND ((a.status_beacukai = 'SPJM' AND (c.`id` IS NULL OR (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)))) OR (a.status_beacukai != 
			 'SPJM' AND (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)) ))$tambah_status $q ORDER BY a.created_at DESC $limit_final";

	/*			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,b.id id_del FROM ".$app['table']['import']." a
						  LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						  where (a.status_kiriman = '0' AND b.status ='expired') $q order by created_at DESC";*/
				$nonya = $mulai+1;
				$terbaru = 0;
				db::query($sql, $rs['row'], $nr['row']);
				$todo['length']=$nr['row'];
				while($row = db::fetch($rs['row']))
				{
					$row['revisi'] == "";
					$data_pelabuhan = db::get_record("pelabuhan","id_import",$row['id']);
					
					$data_kel = db::get_record("file_kel","id_import",$row['id']);
					$data_eir = db::get_record("file_eir","id_import",$row['id']);
					if ($row['status_beacukai']=="SPJM" && $data_eir !="") {
						$row['doc_kel']="iya";
					}
					$row['border_div'] = "#698299";
					$row['back_upl'] = "#02489e";
					$row['bor_upl'] = "#5a6f89";
					$row['bor_det'] = "#5a6f89";

					$row['doc_kel']="no";
					if (($row['status_sppb']==1 && !$data_kel['id']) && $row['status_exp'] !="expired") {
						$row['doc_kel']="iya";
					}
					if ($row['created_at'] !="") {
						$row['created_at']=format_tgl($row['created_at']);
					}
					$todo[] = $row;		
					/*if (!$data_eir['id'] && $row['status_beacukai'] == "SPJM") {
							$todo[] = $row;		
					}else{
						if (($row['status_sppb']==1 && !$data_kel['id']) && $row['id_del']) {
								$row['doc_kel']="iya";
								$todo[] = $row;	
							}else{
								$todo['length']=$todo['length']-1;
							}
					}*/
				}


/*	   		  	$sql_total1   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_eir'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['pelabuhan'] ." d ON (a.id=d.id_import)	
					LEFT JOIN ". $app['table']['file_kel'] ." e ON (a.id=e.id_import)
			 where ((a.status_beacukai = 'SPJM' AND (c.`id` IS NULL OR (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)))) OR (a.status_beacukai != 
			 'SPJM' AND (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)) )) AND (a.status_kiriman = '0')$tambah_status $q ORDER BY a.created_at DESC";*/
	   		  	$sql_total1   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,a.tgl_nopen FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_eir'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['pelabuhan'] ." d ON (a.id=d.id_import)	
					LEFT JOIN ". $app['table']['file_kel'] ." e ON (a.id=e.id_import)
			 where a.status_kiriman = '0' AND ((a.status_beacukai = 'SPJM' AND (c.`id` IS NULL OR (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)))) OR (a.status_beacukai != 
			 'SPJM' AND (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)) ))$tambah_status $q ORDER BY a.created_at DESC";
				db::query($sql_total1, $rs['row'], $nr321['row']);
				$user_total1=$nr321['row'];
				while($row = db::fetch($rs['row']))
				{
					$row['revisi'] == "";
					$data_pelabuhan = db::get_record("pelabuhan","id_import",$row['id']);
					
					$data_kel = db::get_record("file_kel","id_import",$row['id']);
					$data_eir = db::get_record("file_eir","id_import",$row['id']);
					if ($row['status_beacukai']=="SPJM" && $data_eir !="") {
						$row['doc_kel']="iya";
					}
					$row['border_div'] = "#698299";
					$row['back_upl'] = "#02489e";
					$row['bor_upl'] = "#5a6f89";
					$row['bor_det'] = "#5a6f89";

					$row['doc_kel']="no";
					if (!$data_eir['id'] && $row['status_beacukai'] == "SPJM") {
							// $todo[] = $row;		
					}else{
						if (($row['status_sppb']==1 && !$data_kel['id']) && $row['id_del']) {
								$row['doc_kel']="iya";
								// $todo[] = $row;	
							}else{
								$user_total1=$user_total1-1;
							}
					}
				}
			}
			if ($p_category == "progress" || $p_category=="all") {
	   		  	/*$sql_total2   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
						LEFT JOIN ". $app['table']['file_eir'] ." d ON (a.id=d.id_import)
				 where (c.id IS NULL OR c.id ='') AND (a.status_kiriman = '0' AND (d.id !='' AND d.id IS NOT NULL) ) AND (b.id IS NULL OR b.status != 'expired')$tambah_status $q order by a.created_at DESC";*/
	   		  	$sql_total2   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
						LEFT JOIN ". $app['table']['file_eir'] ." d ON (a.id=d.id_import)
				 where a.status_kiriman = '0' AND  ((a.status_beacukai ='SPJM' AND ( (d.id !='' AND d.id IS NOT NULL AND a.status_sppb !=1) OR (c.id !='' AND c.id IS NOT NULL) )) OR (a.status_beacukai !='SPJM' AND (c.id !='' AND c.id IS NOT NULL))) AND (b.id IS NULL OR b.status != 'expired')$tambah_status $q order by a.created_at DESC";
				db::query($sql_total2, $rs_total2['row'], $nr_total2['row']);

	   		  	/*$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
						LEFT JOIN ". $app['table']['file_eir'] ." d ON (a.id=d.id_import)
				 where (c.id IS NULL OR c.id ='') AND (a.status_kiriman = '0' AND (d.id !='' AND d.id IS NOT NULL) ) AND (b.id IS NULL OR b.status != 'expired')$tambah_status $q order by a.created_at DESC $limit_final";*/
	   		   	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
						LEFT JOIN ". $app['table']['file_eir'] ." d ON (a.id=d.id_import)
				 where a.status_kiriman = '0' AND  ((a.status_beacukai ='SPJM' AND ( (d.id !='' AND d.id IS NOT NULL AND a.status_sppb !=1) OR (c.id !='' AND c.id IS NOT NULL) )) OR (a.status_beacukai !='SPJM' AND (c.id !='' AND c.id IS NOT NULL))) AND (b.id IS NULL OR b.status != 'expired')$tambah_status $q order by a.created_at DESC $limit_final";
				db::query($sql, $rs2['row'], $nr2['row']);
				$progress['length']=$nr2['row'];
				while($row2 = db::fetch($rs2['row']))
				{
					$row2['status_exp'] ="";
					$row2['border_div'] = "#698299";
					$row2['back_upl'] = "#02489e";
					$row2['bor_upl'] = "#5a6f89";
					$row2['bor_det'] = "#5a6f89";
					if ($row2['created_at'] !="") {
						$row2['created_at']=format_tgl($row2['created_at']);
					}
					$progress[] = $row2;
				}
			}
			if ($p_category == "completed" || $p_category=="all") {
/*	   		   	$sql_total3   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
				 where (c.id IS NOT NULL OR c.id !='') AND b.status != 'expired'$tambah_status $q order by a.created_at DESC";*/
	   		   	$sql_total3   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
				 where a.status_kiriman = '1'$tambah_status $q order by a.created_at DESC";
				db::query($sql_total3, $rs_total3['row'], $nr_total3['row']);
/*	   		   	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
				 where (c.id IS NOT NULL OR c.id !='') AND b.status != 'expired'$tambah_status $q order by a.created_at DESC $limit_final";*/
	   		   	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
				 where a.status_kiriman = '1'$tambah_status $q order by a.created_at DESC $limit_final";
				db::query($sql, $rs3['row'], $nr3['row']);
				$completed['length']=$nr3['row'];
				while($row3 = db::fetch($rs3['row']))
				{
					$row3['status_exp'] ="";
					$row3['border_div'] = "#698299";
					$row3['back_upl'] = "#02489e";
					$row3['bor_upl'] = "#5a6f89";
					$row3['bor_det'] = "#5a6f89";
					if ($row3['created_at'] !="") {
						$row3['created_at']=format_tgl($row3['created_at']);
					}
					$completed[] = $row3;
				}
			}
			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["tampilnya"=>$tampilnya,"message"=>$response["message"],"todo"=>$todo,"count_todo"=>$user_total1,"progress"=>$progress,"count_progress"=>$nr_total2['row'],"completed"=>$completed,"count_completed"=>$nr_total3['row']]]);
	}elseif ($step=="doc_list_todo") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

   		  	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.status != 'expired')$tambah_status $q order by a.created_at DESC $limit_final";
/*			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,b.id id_del FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					  where (a.status_kiriman = '0' AND b.status ='expired') $q order by created_at DESC";*/
			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs['row'], $nr['row']);
			$todo['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				$row['revisi'] == "";
				$data_pelabuhan = db::get_record("pelabuhan","id_import",$row['id']);
				
				$data_kel = db::get_record("file_kel","id_import",$row['id']);
				$data_eir = db::get_record("file_eir","id_import",$row['id']);
				if ($row['status_beacukai']=="SPJM" && $data_eir !="") {
					$row['doc_kel']="iya";
				}
				$row['border_div'] = "#698299";
				$row['back_upl'] = "#02489e";
				$row['bor_upl'] = "#5a6f89";
				$row['bor_det'] = "#5a6f89";

				$row['doc_kel']="no";
				if (!$data_eir['id'] && $row['status_beacukai'] == "SPJM") {
						$todo[] = $row;		
				}else{
					if (($row['status_sppb']==1 && !$data_kel['id']) && $row['id_del']) {
							$row['doc_kel']="iya";
							$todo[] = $row;	
						}else{
							$todo['length']=$todo['length']-1;
						}
				}
			}


   		  	$sql_total1   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.status != 'expired')$tambah_status $q order by a.created_at DESC";
			db::query($sql_total1, $rs['row'], $nr321['row']);
			$user_total1=$nr321['row'];
			while($row = db::fetch($rs['row']))
			{
				$row['revisi'] == "";
				$data_pelabuhan = db::get_record("pelabuhan","id_import",$row['id']);
				
				$data_kel = db::get_record("file_kel","id_import",$row['id']);
				$data_eir = db::get_record("file_eir","id_import",$row['id']);
				if ($row['status_beacukai']=="SPJM" && $data_eir !="") {
					$row['doc_kel']="iya";
				}
				$row['border_div'] = "#698299";
				$row['back_upl'] = "#02489e";
				$row['bor_upl'] = "#5a6f89";
				$row['bor_det'] = "#5a6f89";

				$row['doc_kel']="no";
				if (!$data_eir['id'] && $row['status_beacukai'] == "SPJM") {
						// $todo[] = $row;		
				}else{
					if (($row['status_sppb']==1 && !$data_kel['id']) && $row['id_del']) {
							$row['doc_kel']="iya";
							// $todo[] = $row;	
						}else{
							$user_total1=$user_total1-1;
						}
				}
			}
			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"todo"=>$todo,"count_todo"=>$user_total1]]);
	}elseif ($step=="doc_list_progress") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

   		  	$sql_total2   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['file_eir'] ." d ON (a.id=d.id_import)
			 where (c.id IS NULL OR c.id ='') AND (a.status_kiriman = '0' AND (d.id !='' AND d.id IS NOT NULL) ) AND (b.id IS NULL OR b.status != 'expired')$tambah_status $q order by a.created_at DESC";
			db::query($sql_total2, $rs_total2['row'], $nr_total2['row']);

   		  	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['file_eir'] ." d ON (a.id=d.id_import)
			 where (c.id IS NULL OR c.id ='') AND (a.status_kiriman = '0' AND (d.id !='' AND d.id IS NOT NULL) ) AND (b.id IS NULL OR b.status != 'expired')$tambah_status $q order by a.created_at DESC $limit_final";
			db::query($sql, $rs2['row'], $nr2['row']);
			$progress['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['status_exp'] ="";
				$row2['border_div'] = "#698299";
				$row2['back_upl'] = "#02489e";
				$row2['bor_upl'] = "#5a6f89";
				$row2['bor_det'] = "#5a6f89";
				$progress[] = $row2;
			}

			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"progress"=>$progress,"count_progress"=>$nr_total2['row']]]);
	}elseif ($step=="doc_list_completed") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

   		   	$sql_total3   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
			 where (c.id IS NOT NULL OR c.id !='') AND b.status != 'expired'$tambah_status $q order by a.created_at DESC";
			db::query($sql_total3, $rs_total3['row'], $nr_total3['row']);
   		   	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
			 where (c.id IS NOT NULL OR c.id !='') AND b.status != 'expired'$tambah_status $q order by a.created_at DESC $limit_final";
			db::query($sql, $rs3['row'], $nr3['row']);
			$completed['length']=$nr3['row'];
			while($row3 = db::fetch($rs3['row']))
			{
				$row3['status_exp'] ="";
				$row3['border_div'] = "#698299";
				$row3['back_upl'] = "#02489e";
				$row3['bor_upl'] = "#5a6f89";
				$row3['bor_det'] = "#5a6f89";
				$completed[] = $row3;
			}
			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"completed"=>$completed,"count_completed"=>$nr_total3['row']]]);
	}elseif ($step=="home_list") {	
############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				if (date("Y-m-d",strtotime($p_search)) != 1970-01-01 && date("Y-m-d",strtotime($p_search)) != "1970-01-01") {
					$q = "AND (a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
				}else{
					$q = "AND (a.num_pib LIKE '%".$p_search."%') ";
				}
			}
			$tambah_status="";
			if ($filter_status) {
				if ($filter_status !="ALL") {
					$tambah_status=" AND a.status_beacukai = '$filter_status' ";
				}
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}
			///////  Non Aktifkan Data Terbaru /////////////
			if ($p_awal =="tidak") {
				$pakai_awal = "tidak";
			}else{
				$pakai_awal = "iya";
			}
// 			////////////////////
//    		  	$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
// 					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
// 			 where (a.status_kiriman = '0') AND (b.status != 'expired')$tambah_status $q order by a.created_at DESC $limit_final";
// /*			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,b.id id_del FROM ".$app['table']['import']." a
// 					  LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
// 					  where (a.status_kiriman = '0' AND b.status ='expired') $q order by created_at DESC";*/
// 			$nonya = $mulai+1;
// 			$terbaru = 0;
// 			db::query($sql, $rs['row'], $nr['row']);
// 			$user['length']=$nr['row'];
// 			while($row = db::fetch($rs['row']))
// 			{
// 				$row['revisi'] == "";
// 				$data_pelabuhan = db::get_record("pelabuhan","id_import",$row['id']);
// 				$data_kel = db::get_record("file_kel","id_import",$row['id']);
// 				$data_eir = db::get_record("file_eir","id_import",$row['id']);
			
// 				$row['border_div'] = "#698299";
// 				$row['back_upl'] = "#02489e";
// 				$row['bor_upl'] = "#5a6f89";
// 				$row['bor_det'] = "#5a6f89";
// 				$row['doc_kel']="no";
// 				if (!$data_eir['id'] && $row['status_beacukai'] == "SPJM") {
// 					if ($terbaru==0 && $pakai_awal =="iya") {
// 						$user_terbaru[] = $row;
// 						$user['length']=$user['length']-1;
// 						$terbaru++;
// 					}else{
// 						$user[] = $row;	
// 						$terbaru++;			
// 					}
// 				}else{
// 					if (($row['status_sppb']==1 && !$data_kel['id']) && $row['id_del']) {
// 						if ($terbaru==0 && $pakai_awal =="iya") {
// 							$row['doc_kel']="iya";
// 							$user_terbaru[] = $row;
// 							$user['length']=$user['length']-1;
// 							$terbaru++;
// 						}else{
// 							$row['doc_kel']="iya";
// 							$user[] = $row;		
// 							$terbaru++;		
// 						}
// 					}else{
// 						$user['length']=$user['length']-1;
// 					}
// 				}
// 			}
//////////////////////////////////////////////

   		$sql   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at,a.tgl_nopen FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_eir'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['pelabuhan'] ." d ON (a.id=d.id_import)	
					LEFT JOIN ". $app['table']['file_kel'] ." e ON (a.id=e.id_import)
			 where ((a.status_beacukai = 'SPJM' AND (c.`id` IS NULL OR (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)))) OR (a.status_beacukai != 
			 'SPJM' AND (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)) )) AND (a.status_kiriman = '0')$tambah_status $q ORDER BY a.created_at DESC $limit_final";

			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs['row'], $nr['row']);
			$user['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				$row['revisi'] == "";
				$data_pelabuhan = db::get_record("pelabuhan","id_import",$row['id']);
				$data_kel = db::get_record("file_kel","id_import",$row['id']);
				$data_eir = db::get_record("file_eir","id_import",$row['id']);
			
				$row['border_div'] = "#698299";
				$row['back_upl'] = "#02489e";
				$row['bor_upl'] = "#5a6f89";
				$row['bor_det'] = "#5a6f89";
				$row['doc_kel']="no";

				if ($row['created_at'] !="") {
					$row['created_at']=format_tgl($row['created_at']);
				}
				// if (!$data_eir['id'] && $row['status_beacukai'] == "SPJM") {
				if ($row['status_sppb']==1 && !$data_kel['id'] && $row['status_exp'] !="expired") {
					$row['doc_kel']="iya";
				}
					if ($terbaru==0 && $pakai_awal =="iya") {
						$user_terbaru[] = $row;
						$user['length']=$user['length']-1;
						$terbaru++;
					}else{
						$user[] = $row;	
						$terbaru++;			
					}
				/*}else{
					if (($row['status_sppb']==1 && !$data_kel['id']) && $row['id_del']) {
						if ($terbaru==0 && $pakai_awal =="iya") {
							$row['doc_kel']="iya";
							$user_terbaru[] = $row;
							$user['length']=$user['length']-1;
							$terbaru++;
						}else{
							$row['doc_kel']="iya";
							$user[] = $row;		
							$terbaru++;		
						}
					}else{
						$user['length']=$user['length']-1;
					}
				}*/
			}


   		  /*	$sql_total   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.status != 'expired')$tambah_status $q order by a.created_at DESC";*/

   		  /*	$sql_total   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.status != 'expired')$tambah_status $q order by a.created_at DESC";*/
   		  	$sql_total   = "SELECT DISTINCT a.id,b.id id_del,a.status_sppb,a.num_bl,a.num_pib,a.status_beacukai,b.status status_exp,b.expired_date,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_eir'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['pelabuhan'] ." d ON (a.id=d.id_import)	
					LEFT JOIN ". $app['table']['file_kel'] ." e ON (a.id=e.id_import)
			 where ((a.status_beacukai = 'SPJM' AND (c.`id` IS NULL OR (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)))) OR (a.status_beacukai != 
			 'SPJM' AND (b.status != 'expired' AND (a.status_sppb =1 AND e.id IS NULL)) )) AND (a.status_kiriman = '0')$tambah_status $q ORDER BY a.created_at DESC";
			db::query($sql_total, $rs['row'], $nr['row']);
			$user_total = $nr['row'];
/*			$sql2   = "SELECT id,num_bl,num_pib,status_beacukai,pfpd FROM ".$app['table']['import']." where status_kiriman = '1' $q order by created_at DESC";
			db::query($sql2, $rs2['row'], $nr2['row']);
			$user2['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$exp_1 = db::get_record("delivery_order" , "id_import = '".$row2['id']."' AND status = 'active' ");
				$exp_date2 = db::get_record("expired_do" , "id_delivery_order = '".$exp_1['id']."' AND status = 'active' ");
				if ($exp_date2['expired_do']!="") {
					$row2['expired_date'] = date("d-m-Y",strtotime($exp_date2['expired_do']));
				}
				$user2[] = $row2;
			}*/
			if ($user_total == 0) {
				$data_kosong="iya";
			}
			$count_notif = db::lookup("count(id)","notif","id_rule = '5403092018114622' AND (status ='1' OR status =1) ");
			$response["message"] = "Pelayaran Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"data_kosong"=>$data_kosong,"progress"=>$user,"count_progress"=>$user_total,"data_terbaru"=>$user_terbaru,"count_new_notif"=>$count_notif]]);
	}elseif ($step=="delete") {	
		if (isset($id_imp)) {
			// $id 	= $_GET['id'];
			// if ($_FILES["attachment"]["name"]) {
				$delete_file = db::get_record("pelabuhan","id_import",$id_imp);
				@unlink($app['doc_path']."/sppb/". $delete_file['eir']);
				@unlink($app['doc_path']."/out_cont/". $delete_file['kelengkapan']);
/*				$nama_gambar=$_FILES["attachment"]["name"].date("dmYsHis");
			    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/do/".$nama_gambar);
			    $abc="do 	= '$nama_gambar',";*/
			// }
			$sql 	= "DELETE FROM ".$app['table']['pelabuhan']." WHERE id_import ='". $id ."' ";
					   db::qry($sql);
					   db::qry($sql2);
					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Data Berhasil di Hapus";
	        			// app::response("200", ["error"=>FALSE,"items"=>$user]);
	        			app::response("200", $response);
					   // echo json_encode($response);
					   // echo json_encode($sql);
		}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Identitas Kosong";
	       				app::response("404", ["items"=>$response]);
					   // echo json_encode($response);
		}
	}elseif ($step=="completed_doc") {	

/*			$q 		 = null;
			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['pelabuhan']." ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);     */       
	  		// $sqlnya = "select * from dmg_container LIMIT $mulai, $halaman";
	  		// db::query($sqlnya, $rs['row'], $nr['row']);

############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################

			if (isset($cari)) {
				// $q = "where pfpd LIKE '%".$cari."%' OR tanggal_sppb LIKE '%".$cari."%' OR tanggal LIKE '%".$cari."%' ";
				$q = "AND d.num_pib LIKE '%".$cari."%' OR d.status_beacukai LIKE '%".$cari."%' ";
			}
			// else{
			// 	$q = " LIMIT $mulai, $halaman";
			// }

			// $sql = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
			// 		a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
			// 		b.name name FROM ".$app['table']['beacukai']." a
			// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q LIMIT 5";
		
		/*	$sql   = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
					  a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					  b.name name,a.nilai_sptnp nilai_sptnp,a.nomer_sptnp nomer_sptnp,a.tgl_sptnp tgl_sptnp FROM ".$app['table']['beacukai']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q  LIMIT $mulai, $halaman ";*/

/*			$sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  $q  LIMIT $mulai, $halaman ";*/
			$sql   = "SELECT a.*,c.id id_exp FROM ".$app['table']['pelabuhan']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import)
					  LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id)
					  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id_import=e.id_import) where d.status_kiriman = '1' $q ";
			// $nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
			/*	$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id_idv']."' and status = 'active'");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id_idv']."' ");

				$row['date_do']=$datas['do'];
				if ($row['date_do']!=null&&$row['date_do']!="0000-00-00") {
					$row['date_do']= date("d-m-Y",strtotime($row['date_do']));
				}else{
					$row['date_do']="";
				}

				// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
				// $row['expired_do']= $datas['do'];

				// if (strtotime($datas['do']) < time()) {
				if ($datas['do'] < date("Y-m-d")) {
					isset($row['id_idv'])?$id_exp [] = $row['id_idv']:null;
				}
				else{
					isset($row['id_idv'])?$id_exp_act [] = $row['id_idv']:null;
				}*/
				$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
				if ($exp_date['expired_do']!="") {
					$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
				}
				$user[] = $row;
			}
		/*	$id_exp 	= implode("','", $id_exp);
			$id_exp_act = implode("','", $id_exp_act);
			$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
			$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
			db::qry($sql321);
			db::qry($sql1231);*/
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					if ($user) {
						$response["message"] = "Pelabuhan Berhasil Termuat";
						// echo json_encode($sql);
	        			app::response("200", ["error"=>FALSE,"completed"=>$user,"count_completed"=>$nr['row']]);
						// echo json_encode($response);
					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Data Masih kosong";
						// echo json_encode($response);
				        app::response("404", ["completed"=>$response]);
	        			// app::response("200", ["error"=>FALSE,"items"=>$response]);
						// echo json_encode($sql);
					}else{
						$response["error"]=TRUE;
						$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
						// echo json_encode($response);
				        app::response("500", ["completed"=>$response]);
						// echo json_encode($response);
					}
	}elseif ($step=="view_2") {	
		$q=null;
		$halaman = 2;
		$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
		$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
		$result  = "SELECT * FROM ".$app['table']['pelabuhan']." ";
		db::query($result, $rs['rows'], $nr['rows']);
	  	$pages 	 = ceil($nr['rows']/$halaman);
		if (isset($cari)) {
			$q = "AND d.num_pib LIKE '%".$cari."%' OR status_sppb LIKE '%".$cari."%' ";
		}
		$sql = "SELECT a.*,d.status_beacukai status_sppb,c.id id_idv FROM ".$app['table']['pelabuhan']." a
				LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
				LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import) 
				LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id) 
				where c.status = 'active' $q ";
		$nonya = $mulai+1;
		db::query($sql, $rs['row'], $nr['row']);
		while($row = db::fetch($rs['row']))
		{
				$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id_idv']."' and status = 'active'");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id_idv']."' ");

				$row['date_do']=$datas['do'];
				if ($row['date_do']!=null&&$row['date_do']!="0000-00-00") {
					$row['date_do']= date("d-m-Y",strtotime($row['date_do']));
				}else{
					$row['date_do']="";
				}

				// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
				// $row['expired_do']= $datas['do'];

				// if (strtotime($datas['do']) < time()) {
				if ($datas['do'] < date("Y-m-d")) {
					isset($row['id_idv'])?$id_exp [] = $row['id_idv']:null;
				}
				else{
					isset($row['id_idv'])?$id_exp_act [] = $row['id_idv']:null;
				}
			$view[] = $row;
		}
			$id_exp 	= implode("','", $id_exp);
			$id_exp_act = implode("','", $id_exp_act);
			$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
			$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
			db::qry($sql321);
			db::qry($sql1231);
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
				if ($view) {
					$response["message"] = "Beacukai Berhasil Termuat";
	       			app::response("200", ["error"=>FALSE,"items"=>$view]);
					// echo json_encode($response);
				}elseif ($view==0) {
					$response["error"]=FALSE;
					$response["message"] = "Data Masih kosong";
					// echo json_encode($response);
			        app::response("404", ["items"=>$response]);
	       			// app::response("200", ["error"=>FALSE,"items"=>$response]);
					// echo json_encode($sql);
				}else{
					$response["error"]=TRUE;
					$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
			        app::response("500", ["items"=>$response]);
					// echo json_encode($response);
				}
	}else{
			$q=null;
			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['pelabuhan']." ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);            
	  		// $sqlnya = "select * from dmg_container LIMIT $mulai, $halaman";
	  		// db::query($sqlnya, $rs['row'], $nr['row']);

############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################

			if (isset($p_search)) {
				// $q = "where pfpd LIKE '%".$cari."%' OR tanggal_sppb LIKE '%".$cari."%' OR tanggal LIKE '%".$cari."%' ";
				#$q = "AND (a.num_pib LIKE '%".$p_search."%' OR a.status_beacukai LIKE '%".$p_search."%') ";
				$q = "AND (a.num_pib LIKE '%".$p_search."%' OR a.status_beacukai LIKE '%".$p_search."%' OR a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}
			// else{
			// 	$q = " LIMIT $mulai, $halaman";
			// }

			// $sql = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
			// 		a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
			// 		b.name name FROM ".$app['table']['beacukai']." a
			// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q LIMIT 5";
		
		/*	$sql   = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
					  a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					  b.name name,a.nilai_sptnp nilai_sptnp,a.nomer_sptnp nomer_sptnp,a.tgl_sptnp tgl_sptnp FROM ".$app['table']['beacukai']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q  LIMIT $mulai, $halaman ";*/

/*			$sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  $q  LIMIT $mulai, $halaman ";*/
			// $sql   = "SELECT a.*,c.id id_idv FROM ".$app['table']['pelabuhan']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
			// 		  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import)
			// 		  LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id)
			// 		  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id_import=e.id_import)  where d.status_kiriman = '0' $q ";

					  
/* bisa dipakai 			$sql   = "SELECT a.*,c.id id_exp,d.num_pib num_pib,d.status_beacukai status_beacukai,d.id id,a.id id_pelabuhan FROM ".$app['table']['pelabuhan']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import)
					  LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id)
					  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id_import=e.id_import)  where d.status_kiriman = '0' $q ";					  */
			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_sppb status_sppb,a.status_beacukai status_beacukai,c.id id_exp,c.status status_delivery,a.num_pib num_pib,a.status_beacukai status_beacukai,a.id id FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id = c.id_import)
					  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id=e.id_import)  where a.status_kiriman = '0' $q order by a.created_at DESC ";
			db::query($sql, $rs['row'], $nr['row']);
			$user['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				$row["status_expired"] ="";
				$pelabuhan_id =  db::lookup("id", "pelabuhan", "id_import='". $row['id'] ."' ");
				$pelabuhan_kel =  db::lookup("kelengkapan", "pelabuhan", "id_import='". $row['id'] ."' ");	
				$pelabuhan_eir =  db::lookup("eir", "pelabuhan", "id_import='". $row['id'] ."' ");	
				$status_eir =  db::lookup("status_eir", "pelabuhan", "id_import='". $row['id'] ."' ");	

				if ($row['status_delivery'] == "expired") {
					$row["status_expired"] = "expired";
				}

				// if ($status_beacukai =="" AND $id_exp !="") {
				if ($row['status_beacukai'] =="" AND $row['id_exp'] !="") {
					$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
					if ($exp_date['expired_do']!="") {
						$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
						$user[] = $row;	
					}else{
						$user['length']=$user['length']-1;
					}
				}else{
					if ($pelabuhan_kel !="") {
						$user['length']=$user['length']-1;
					}else{
						if ($pelabuhan_id !="") {
							// if ($row['status_beacukai'] =="SPJM") {
							if ($pelabuhan_eir !="" || $status_eir =="ya") {
								// exit;
								if ($row['status_sppb'] =="0") {
									if ($pelabuhan_kel =="") {
										$user['length']=$user['length']-1;
									}else{
										$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
										if ($exp_date['expired_do']!="") {
											$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
											$user[] = $row;	
										}else{
											$user['length']=$user['length']-1;
										}
									}
								}else{
									$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
									if ($exp_date['expired_do']!="") {
										$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
										$user[] = $row;	
									}else{
										$user['length']=$user['length']-1;
									}
								}
							}else{	
								$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
								if (($exp_date['expired_do']!="" || $exp_date['expired_do'] !="1970-01-01") && $row['id_exp'] !="" ) {
									$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
								# 	if ($row['status_sppb'] =="1") {
								##	if ($row['status_sppb'] =="0") {
										$user[] = $row;
								#	}else{
								#		$user['length']=$user['length']-1;
								#	}
								}else{
									$user['length']=$user['length']-1;
								}
							}
						}
						else{
							if ($row['status_beacukai'] =="SPJM") {
								//$pelabuhan_kel =="0"
								if ($pelabuhan_eir !="" || $status_eir =="ya") {
								// if ($row['status_sppb'] =="0") {
									$user['length']=$user['length']-1;
								}else{
									$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
									if ($exp_date['expired_do']!="") {
										$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
										$user[] = $row;	
									}else{
										$user['length']=$user['length']-1;
									}
								}
							}else{
									$user['length']=$user['length']-1;
							}
						}
					}
				}
			}

			// $sql2   = "SELECT a.*,c.id id_exp,d.status_beacukai status_beacukai,d.num_pib num_pib,d.id id,a.id id_pelabuhan FROM ".$app['table']['pelabuhan']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
			// 		  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import)
			// 		  LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id)
			// 		  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id_import=e.id_import) where d.status_kiriman = '1' $q ";
/*			$sql2   = "SELECT a.*,c.id id_exp,a.status_beacukai status_beacukai,a.num_pib num_pib,a.id id FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id=c.id_import)
					  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id=e.id_import) where a.status_kiriman = '1' $q ";*/
			$sql2   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,c.id id_exp,a.status_beacukai status_beacukai,a.num_pib num_pib,a.id id FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id=c.id_import)
					  where a.status_kiriman = '1' $q order by a.created_at DESC";
			db::query($sql2, $rs2['row'], $nr2['row']);
			$user2['length'] = $nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$exp_date2 = db::get_record("expired_do" , "id_delivery_order = '".$row2['id_exp']."' AND status = 'active' ");
				if ($exp_date2['expired_do']!="") {
					$row2['expired_date'] = date("d-m-Y",strtotime($exp_date2['expired_do']));
				}
				$user2[] = $row2;
			}
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					// if ($user) {
			$response["message"] = "Pelabuhan Berhasil Termuat";
			// echo json_encode($sql);
/*			app::response("200", ["error"=>FALSE,"progress"=>$user,"count_progress"=>$nr['row'],"completed"=>$user2,"count_completed"=>$nr2['row'],"sql"=>$sql,"sql2"=>$sql2]);*/
	
			// app::response("200", ["error"=>FALSE,"progress"=>$user,"count_progress"=>$nr['row'],"completed"=>$user2,"count_completed"=>$nr2['row']]);			
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"progress"=>$user,"count_progress"=>$nr['row'],"completed"=>$user2,"count_completed"=>$nr2['row']]]);
						// echo json_encode($response);
					// }elseif ($user==0) {
					// 	$response["error"]=FALSE;
					// 	$response["message"] = "Data Masih kosong";
					// 	// echo json_encode($response);
				 //        app::response("404", ["progress"=>$response]);
	    //     			// app::response("200", ["error"=>FALSE,"items"=>$response]);
					// 	// echo json_encode($sql);
					// }else{
					// 	$response["error"]=TRUE;
					// 	$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
					// 	// echo json_encode($response);
				 //        app::response("500", ["progress"=>$response]);
					// 	// echo json_encode($response);
					// }
		}
}elseif($act=="beacukai"){
		// $app['me'] 	= app::unserialize64($_SESSION[$app['session']]);

		// $form = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);

	if ($step=="simpan") {

		// isset($p_date_receipt_do)?$p_date_receipt_do=$p_date_receipt_do:null;
		isset($get_do)?$p_date_receipt_do=$get_do:null;
		// isset($p_expired_do)?$p_expired_do=$p_expired_do:null;
		isset($exp_do)?$p_expired_do=$exp_do:null;
		isset($me)?$me=$me:null;

		// isset($eir)?$eir = $_FILES['eir']['name']:null;
		isset($filenya)?$eir = $filenya:null;
		isset($kelengkapan)?$kelengkapan = $_FILES['kelengkapan']['name']:null;

		isset($p_no_do)?$p_no_do = $p_no_do:$p_no_do=null;
		isset($id_imp)?$id_imp = $id_imp:$id_imp=null;

		$form_imp = db::get_record("import", "id", $id_imp);
		// if ($form_imp['status_beacukai'] !="SPJM") {
// echo "khalid";
			if (strtotime($p_expired_do) < time()) {
				$statusnya_column=",status";
				$status_do = ",'expired'";	
			}
			$id 			  = rand(1, 100).date("dmYHis");
			$ids 			  = rand(1, 100).date("dmYHis");
			$id_c 			  = rand(1, 100).date("dmYHis");

			if ($_FILES["attachment"]["name"]) {
			#	$nama_gambar= $_FILES["attachment"]["name"];
				/*move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/sppb/". $_FILES["attachment"]["name"].date("dmYsHis"));*/
			#    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/sppb/". $nama_gambar);


		#if ($_FILES["attachment"]["name"]) {
		#	$dokumen= $_FILES["attachment"]["name"];
		#   move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/sppb/".$dokumen);
		#}

				// $dokumen = $_FILES["attachment"]["name"];
	 		// 	copy($_FILES["attachment"]['tmp_name'], $app['doc_path']."/sppb/".$dokumen);



	 			// if (!@copy($_FILES["attachment"]['tmp_name'], $app['doc_path']."/sppb/".$dokumen))
				// header("location: error.do&ref=411");
			}
			app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');

		$status_beacukai_beacukai =db::lookup("status_beacukai","import","id",$id_imp);
		$form_import = db::get_record("import", "id", $id_imp);

		$ids = rand(1, 100).date("dmYHis");
		$id_pel = rand(1, 100).date("dmYHis");
		app::mq_encode('p_dokumen');
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
	/*	$sql = "insert into ".$app['table']['beacukai']."
				(id, id_import ,sppb, created_by, created_at,kirim) values
				('$ids', '$id_imp','$dokumen','$me', now(),2)";*/
		$sql = "insert into ".$app['table']['beacukai']."
				(id, id_import , created_by, created_at,kirim) values
				('$ids', '$id_imp','$me', now(),2)";
		db::qry($sql);
		#if ($status_beacukai_beacukai != "SPJM") {
/*			$sql2 = "insert into ".$app['table']['pelabuhan']."
					(id, created_by, created_at, id_import, eir) values
					('$id_pel', '". $app['me']['id'] ."', now(), '$id_imp', '$dokumen')";*/
			$cek_pelabuhan = db::lookup("id","pelabuhan","id_import",$id_imp);
			if (!$cek_pelabuhan) {
				$sql2 = "insert into ".$app['table']['pelabuhan']."
						(id, created_by, created_at, id_import) values
						('$id_pel', '$me', now(), '$id_imp')";
				db::qry($sql2);
/*				$id_notif   = rand(1, 100).date("dmYHis");
				$sqls = "insert into ".$app['table']['notif']." 
						(id,id_rule,id_import,status,pesan,created_by,created_at) values
						('$id_notif','5403092018114622','".$id_imp."',1,'DO Dokumen No. PIB ".$form_import['num_pib']." perlu dibuat Dokumen Kelengkapan','".$app['me']['id']."',now())";
				db::qry($sqls);*/
			}
		#}
		
		// $sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		// db::query($sql_lang, $rs['lang'], $nr['lang']);
		// while ($row321	= db::fetch($rs['lang'])) {
		// 	$sqls ="UPDATE ".$app['table']['status']." set 
		// 			positions=concat('".app::getliblang('proses_urus_beacukai',$row321['alias']).",','<br>',positions),
		// 			updated_by = '". $app['me']['id'] ."',
		// 			updated_at = now()
		// 			WHERE id_import ='$p_pib' AND lang = '".$row321['alias']."' ";
		// 		db::qry($sql);	
		// }
		$message  .= "<p>Kiriman dengan pib : ".$form_import['num_pib']." Bisa di buatkan delivery ordernya </p>";
		// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='5403092018114622' ";

/*		$rs['devices'] = db::get_recordset("device","category='pelabuhan' AND status='active'");
		while($user_devices = db::fetch($rs['devices'])){
			$datapush = array(
				// "to" => db::lookup("token","device","id",$user_devices['id_device']),
				"to" => db::lookup("token","device","category","pelabuhan"),
				"notification" => array(
					"title" => "Admin Pelabuhan/eir",
					"text" => "$p_num_pib : Status SPPB Telah Dirubah",
					"sound" => "default"
				),
				'data' => array(
					'method' => 'pelabuhan',
					'ncr_id' => $id,
					"title" => "Admin Pelabuhan/eir",
					"body" => "$p_num_pib : Status SPPB Telah Dirubah"
				)
			);
			app::pushNotifSend($datapush);
		}*/
					   
		$path_do = $app['doc_path']."/sppb/".$dokumen;

		db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
		while($row = db::fetch($rs['sql_bea_cukai'])){
			// app::sendmail($row['email'], "Ada dokumen sppb baru", $message, $path_do);
#			app::sendmail($row['email'], "Kiriman dengan pib : ".$form_import['num_pib']." Bisa dibuatkan do ", $message, $path_do);
		}

			#$p_date_receipt_do 	= date("Y-m-d",strtotime($p_date_receipt_do));
			#$p_expired_do 		= date("Y-m-d",strtotime($p_expired_do));
			// $sql = "insert into ".$app['table']['pelabuhan']."
			// 		(id, date_receipt_do, expired_do, created_by, created_at, id_import, eir,kelengkapan$statusnya_column) values
			// 		('$id', '$p_date_receipt_do', '$p_expired_do', '$me', now(), '$id_imp', '$eir', '$kelengkapan'$status_do)";

			// $sql = "insert into ".$app['table']['pelabuhan']."
			// 		(id, created_by, created_at, id_import, eir, kelengkapan) values
			// 		('$id', '$me', now(), '$id_imp', '$eir', '$kelengkapan')";

/*			$sql = "insert into ".$app['table']['pelabuhan']."
					(id, created_by, created_at, id_import, eir) values
					('$id', '$me', now(), '$id_imp', '$eir')";*/
			#$sql = "insert into ".$app['table']['pelabuhan']."
			#		(id, created_by, created_at, id_import, eir,date_receipt_do,expired_do) values
			#		('$id', '$me', now(), '$id_imp', '$nama_gambar','$p_date_receipt_do','$p_expired_do')";

/*			$sql = "insert into ".$app['table']['pelabuhan']."
					(id, created_by, created_at, id_import, eir, status_eir) values
					('$id', '$me', now(), '$id_imp', '$nama_gambar', '$status_eir')";*/
			
			// $sqlsss ="insert into ".$app['table']['notif']." 
			// 		(id, id_rule, id_import, status, pesan, created_by, created_at) values
			// 		('$id_c', '3220082018161340', '$id_imp', 1, 'SPPB dengan pib  = ".$form_imp['num_pib']."  Telah di urus','$me',now())";

/*			$sqlsss ="insert into ".$app['table']['notif']." 
					(id, id_rule, id_import, status, pesan, created_by, created_at) values
					('$id_c', '3220082018161340', '$id_imp', 1, 'Kiriman dengan pib  = ".$form_imp['num_pib']." Bisa di Ubah Statusnya ','$me',now())";*/


			// $sqlsss ="insert into ".$app['table']['notif']." 
			// 		(id, id_rule, id_import, status, pesan, created_by, created_at) values
			// 		('$id_c', '3220082018161340', '$id_imp', 1, 'SPPB dengan pib  = ".$form_imp['num_pib']."  Telah di urus','$me',now())";
/*			db::qry($sql);
			db::qry($sqlsss);*/

/*			$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
			db::query($sql_lang, $rs['lang'], $nr['lang']);
			while ($row321	= db::fetch($rs['lang'])) {*/
				// $sqlss ="insert into ".$app['table']['status']." 
				// 		(id,id_import,positions,created_by,created_at,lang) values
				// 		('$id_c','$id_imp','".app::getliblang('proses_pelabuhan_eir',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
	/*			$sqlss ="UPDATE ".$app['table']['status']." SET positions = '".app::getliblang('proses_pelabuhan_eir',$row['alias'])."',updated_by = '$me',updated_at = now() WHERE id_import = '$id_imp' ";*/
/*			$sqlss ="UPDATE ".$app['table']['status']." set 
					positions=concat('".app::getliblang('proses_pelabuhan_eir',$row321['alias']).",','<br>',positions),
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
					WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' ";
				db::qry($sqlss);*/
			/*}*/
	/*		$message  .= "<p>Date Receipt DO :<b>". $p_date_receipt_do ."</b></p>";
			$message  .= "<p>Expired DO : <b>". $p_expired_do ."</b></p>";
			// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
			$message .= "<p>Terima kasih</p>";
			$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
						   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
						   where a.id_rule ='5403092018114622' ";
			db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
			while($row = db::fetch($rs['sql_bea_cukai'])){
				app::sendmail($row['email'], "Ada dokumen import masuk", $message);
			}*/
				// db::qry($sqlss);
				$response["error"]	 = FALSE;
				// $response["id"] = $user["unique_id"];
				$response["message"] = "Data Berhasil Tersimpan";
		        // app::response("200", ["error"=>FALSE,"items"=>$response]);
		        app::response("200", ["error"=>0,"data"=>["message"=>$response["message"]]]);
				
				// echo json_encode($response);
	    // }

	}elseif ($step=="simpan_dokumen") {	
		if (isset($id_imp)) {

			isset($p_date_receipt_do)?$p_date_receipt_do=$p_date_receipt_do:null;
			isset($p_expired_do)?$p_expired_do=$p_expired_do:null;
			isset($id_idv_ord)?$id_idv_ord=$id_idv_ord:null;
			isset($me)?$me=$me:null;
			isset($eir)?$eir = $_FILES['eir']['name']:null;

			isset($filenya)?$filenya = $_FILES['filenya']['name']:$filenya="";

			// isset($kelengkapan)?$kelengkapan = $_FILES['kelengkapan']['name']:null;
			isset($filenya)?$kelengkapan = $filenya:null;

			$form_imp = db::get_record("import", "id", $id_imp);
			// app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
			$p_expired_do = date("Y-m-d",strtotime($p_expired_do));

			// $sql = "update ". $app['table']['pelabuhan'] ."
			// 		updated_by 			= '$me',
			// 		eir 		 		= '$eir',
			// 		kelengkapan			= '$kelengkapan',
			// 		updated_at 			= now()
			// 		where id_import = '$id_imp'";
			if ($form_imp['status_sppb'] == '1') {

			if ($_FILES["attachment"]["name"]) {
				$nama_gambar= $_FILES["attachment"]["name"];
			    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/out_cont/". $nama_gambar);
			}
			$sql = "update ". $app['table']['pelabuhan'] ." set
					updated_by 		= '$me',
					kelengkapan		= '$nama_gambar',
					updated_at 		= now()
					where id_import = '$id_imp'";
			db::qry($sql);
			db::qry("UPDATE ". $app['table']['import']." set status_kiriman ='1' where id = '".$row['id_imp']."' ");
			// if(!empty($p_expired_do)){
				$ids = rand(1, 100).date("dmYHis");
				$id_notif = rand(1, 100).date("dmYHis");
				$id_notif2 = rand(1, 100).date("dmYHis");
				// $sqls = "update ".$app['table']['expired_do']." set
				// 		 status = 'inactive' where id_delivery_order = '$id_idv_ord' ";
				// db::qry($sqls);
				// $sqlss = "insert into ".$app['table']['expired_do']."
				// 		(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id_idv_ord', '$p_expired_do','$me',now())";
				// db::qry($sqlss);
				$sqls32 = "insert into ".$app['table']['notif']." 
							(id,id_rule,id_import,status,pesan,created_by,created_at) values
							('$id_notif','3220082018161340', '$id_imp', 1, 'Kiriman dengan pib : ".$form_imp['num_pib'].", bisa dijadwalkan','$me',now())";
				db::qry($sqls32);
				// $sqls32123 = "insert into ".$app['table']['notif']." 
				// 			(id,id_rule,id_import,status,pesan,created_by,created_at) values
				// 			('$id_notif2','5703092018105342','$id_imp',1,'Kiriman dengan no pib : ".$form_imp['num_pib']." Dokumennya telah di ubah, Bisa di Ubah Statusnya','$me',now())";
				// db::qry($sqls32123);
				
				$path_kelengkapan = $app['doc_path']."/out_cont/".$kelengkapan;

				$message  .= "<p>Dokumen Kelengkapan Container dengan no pib : ".$form_imp['num_pib']."  </p>";
				// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
				$message .= "<p>Terima kasih</p>";
				$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
							   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
							   where a.id_rule ='5703092018105342' ";
				db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
				while($row = db::fetch($rs['sql_bea_cukai'])){
					app::sendmail($row['email'], "Container diantarkan", $message,$path_kelengkapan);
				}

			// }
				$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
				db::query($sql_lang, $rs['lang'], $nr['lang']);
				while ($row321	= db::fetch($rs['lang'])) {
					// $sqlss ="insert into ".$app['table']['status']." 
					// 		(id,id_import,positions,created_by,created_at,lang) values
					// 		('$id_c','$id_imp','".app::getliblang('proses_pelabuhan_eir',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
		/*			$sqlss ="UPDATE ".$app['table']['status']." SET positions = '".app::getliblang('proses_pelabuhan_eir',$row['alias'])."',updated_by = '$me',updated_at = now() WHERE id_import = '$id_imp' ";*/
/*				$sqlss ="UPDATE ".$app['table']['status']." set 
						positions=concat('".app::getliblang('proses_pelabuhan_kelengkapan',$row321['alias']).",','<br>',positions),
						updated_by = '". $app['me']['id'] ."',
						updated_at = now()
						WHERE id_import ='$id_imp' AND lang = '".$row321['alias']."' ";
					db::qry($sqlss);*/
				}

					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Data Berhasil Tersimpan";
					   // echo json_encode($response);
				       // app::response("200", ["error"=>FALSE,"items"=>$response]);
				       app::response("200", ["error"=>FALSE,"data"=>["message"=>$response["message"]]]);
					   // echo json_encode($sql);
			}else{
					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Belum Merubah status sppb";
					   // echo json_encode($response);
				       app::response("200", ["error"=>FALSE,"data"=>["message"=>$response["message"]]]);
					   // echo json_encode($sql);

			}
		}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
					   $response["error_message"] = "Terjadi Kesalahan Saat Menyimpan";
					   // echo json_encode($response);
				        app::response("500", ["error"=>TRUE,"data"=>["message"=>$response["message"]]]);
		}
	}elseif ($step=="edit"){	

    /*echo json_encode(['files'=>$_FILES,'Request'=>$_REQUEST,'path'=>$app['doc_path']."/". $_FILES["attachment"]["name"]]);
    exit;*/
		if (isset($id_imp)) {

				$ids 			= rand(1, 100).date("dmYHis");
				$cek_beacukai 	= db::lookup("id","beacukai","id_import",$id_imp);

				$form_imp = db::get_record("import", "id", $id_imp);
				$cek_revisi = db::lookup("revisi","beacukai","id_import",$id_imp);
				if ($cek_revisi==1) {

				$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
					while($user_devices = db::fetch($rs['devices'])){
						$datapush = array(
							// "to" => db::lookup("token","device","id",$user_devices['id_device']),
							"to" => db::lookup("token","device","id",$user_devices['id_device']),
							"notification" => array(
								"title" => "Admin Pelabuhan/EIR",
								"text" => $form_imp['num_pib']." : Telah Direvisi Oleh Admin Beacukai",
								"sound" => "default"
							),
							'data' => array(
								'method' => 'pelabuhan',
								'ncr_id' => $form_imp['id'],
								"title" => "Admin Pelabuhan/EIR",
								"body" => $form_imp['num_pib']." : Telah Direvisi Oleh Admin Beacukai"
							)	
						);
						app::pushNotifSend($datapush);
					}
						$id_notif   = rand(1, 100).date("dmYHis");
						$sql_notif = "insert into ".$app['table']['notif']." 
								(id,id_rule,id_import,status,pesan,created_by,created_at) values
								('$id_notif','5403092018114622','".$form_imp['id']."',1,'".$form_imp['num_pib']."  : Telah Direvisi Oleh Admin Beacukai','".$app['me']['id']."',now())";
						db::qry($sql_notif);
				}
				if (!$cek_beacukai) {
					$sql = "insert into ".$app['table']['beacukai']."
							(id, id_import , created_by, created_at,kirim) values
							('$ids', '$id_imp','$me', now(),2)";
				}else{
					$sql = "update ". $app['table']['beacukai'] ." set
							revisi 				= 0,
							update_at 			= now()
							where id_import 	= '$id_imp'";
				}
				db::qry($sql);
				$cek_pelabuhan = db::lookup("id","pelabuhan","id_import",$id_imp);
				if (!$cek_pelabuhan) {
					$id_pel = rand(1, 100).date("dmYHis");
					$sql2 = "insert into ".$app['table']['pelabuhan']."
							(id, created_by, created_at, id_import) values
							('$id_pel', '$me', now(), '$id_imp')";
					db::qry($sql2);
					$form_import = db::get_record("import", "id", $id_imp);
/*					$id_notif   = rand(1, 100).date("dmYHis");
					$sqls = "insert into ".$app['table']['notif']." 
							(id,id_rule,id_import,status,pesan,created_by,created_at) values
							('$id_notif','5403092018114622','".$id_imp."',1,'DO Dokumen No. PIB ".$form_import['num_pib']." perlu dibuat Dokumen Kelengkapan','".$app['me']['id']."',now())";
					db::qry($sqls);*/
				}
					   $response["error"] = 0;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Data Berhasil Teredit";
					   // echo json_encode($response);
				       // app::response("404", ["error"=>FALSE,"items"=>$response]);
				       app::response("200", ["error"=>0,"data"=>["message"=>$response["message"]]]);
					   // echo json_encode($sql);
		}else{
					   $response["error"] = 1;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Terjadi Kesalahan Saat Mengedit";
					   // echo json_encode($response);
				        app::response("500", ["error"=>1,"data"=>["message"=>$response["error_message"]]]);
		}
	}elseif ($step=="doc_list") {	
############################ EXPIRED DELIVERY ORDER ####################################
		/*	$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}*/
########################################################################################
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				if (date("Y-m-d",strtotime($p_search)) != 1970-01-01 && date("Y-m-d",strtotime($p_search)) != "1970-01-01") {
					$q = "AND (a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
				}else{
					$q = "AND (a.num_pib LIKE '%".$p_search."%') ";
				}
			}

			if ($filter_status) {
				if ($filter_status !="ALL") {
					$tambah_status=" AND a.status_beacukai = '$filter_status' ";
				}
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

			if ($p_category == "todo") {
				$tampilnya ="todo";
			}elseif($p_category == "progress"){
				$tampilnya = "progress";
			}elseif($p_category == "completed"){
				$tampilnya = "completed";
			}else{
				$tampilnya = "all";
				$p_category = "all";
			}
			if ($p_category == "todo" || $p_category == "all") {
	   		 	$sql_total1   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at,b.revisi,a.tgl_nopen FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
				 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.revisi=1 OR c.id IS NULL)$tambah_status $q order by a.created_at DESC";
				db::query($sql_total1, $rs_total1['row'], $nr_total1['row']);

	   		 	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at,b.revisi,a.tgl_nopen FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
				 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.revisi=1 OR c.id IS NULL)$tambah_status $q order by a.created_at DESC $limit_final";
	/*			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,b.id id_del FROM ".$app['table']['import']." a
						  LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
						  where (a.status_kiriman = '0' AND b.status ='expired') $q order by created_at DESC";*/
				$nonya = $mulai+1;
				$terbaru = 0;
				db::query($sql, $rs['row'], $nr['row']);
				$todo['length']=$nr['row'];
				while($row = db::fetch($rs['row']))
				{
					$row['status_exp']="";
					$row['expired_date'] = db::lookup("expired_date","delivery_order","id_import",$row['id']);
					if ($row['status_exp'] == "expired") {
						$row['border_div'] = "red";
						$row['back_upl'] = "red";
						$row['bor_upl'] = "red";
						$row['bor_det'] = "red";
					}elseif($row['revisi'] == "1" || $row['revisi'] == 1){
						$row['border_div'] = "#e4cf98";
						$row['back_upl'] = "#fe9700";
						$row['bor_upl'] = "#fe9700";
						$row['bor_det'] = "#e4cf98";
					}else{
						$row['border_div'] = "#698299";
						$row['back_upl'] = "#02489e";
						$row['bor_upl'] = "#5a6f89";
						$row['bor_det'] = "#5a6f89";
					}
					if ($row['created_at'] !="") {
						$row['created_at']=format_tgl($row['created_at']);
					}
					$row['doc_kel']="no";
					$todo[] = $row;				
				}
			}
			if ($p_category == "progress" || $p_category == "all") {
/*				$sql_total2   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." d ON (a.id=d.id_import)
				 where d.id IS NULL AND c.id IS NOT NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL OR b.revisi != 1)$tambah_status $q order by a.created_at DESC";*/
				$sql_total2   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
				 where (a.status_kiriman = '0') AND c.id IS NOT NULL AND (b.id IS NOT NULL AND b.revisi != 1)$tambah_status $q order by a.created_at DESC";
				db::query($sql_total2, $rs_total2['row'], $nr_total2['row']);

	   		 /* 	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." d ON (a.id=d.id_import)
				 where d.id IS NULL AND c.id IS NOT NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL OR b.revisi != 1)$tambah_status $q order by a.created_at DESC $limit_final";*/
/*				Bisa di pakai 12-06-2019
	   		  	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
				 where c.id IS NOT NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL OR b.revisi != 1)$tambah_status $q order by a.created_at DESC $limit_final";*/
	   		  	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
				 where (a.status_kiriman = '0') AND c.id IS NOT NULL AND (b.id IS NOT NULL AND b.revisi != 1)$tambah_status $q order by a.created_at DESC $limit_final";
				db::query($sql, $rs2['row'], $nr2['row']);
				$progress['length']=$nr2['row'];
				while($row2 = db::fetch($rs2['row']))
				{
					$row2['status_exp'] ="";
					$row2['border_div'] = "#698299";
					$row2['back_upl'] = "#02489e";
					$row2['bor_upl'] = "#5a6f89";
					$row2['bor_det'] = "#5a6f89";
					if ($row2['created_at'] !="") {
						$row2['created_at']=format_tgl($row2['created_at']);
					}
					$progress[] = $row2;
				}
			}
			if ($p_category == "completed" || $p_category == "all") {
/*	   		  	$sql_total3   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
				 where (a.status_kiriman = '1') AND c.id IS NOT NULL AND (b.id IS NOT NULL  OR b.revisi != 1)$tambah_status $q order by a.created_at DESC";
				db::query($sql_total3, $rs_total3['row'], $nr_total3['row']);*/
	   		  	$sql_total3   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
				 where (a.status_kiriman = '1') $tambah_status $q order by a.created_at DESC";
				db::query($sql_total3, $rs_total3['row'], $nr_total3['row']);

/*	   		  	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
				 where (a.status_kiriman = '1') AND c.id IS NOT NULL AND (b.id IS NOT NULL  OR b.revisi != 1)$tambah_status $q order by a.created_at DESC $limit_final";*/
	   		  	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
						LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
						LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
				 where (a.status_kiriman = '1')$tambah_status $q order by a.created_at DESC $limit_final";

				db::query($sql, $rs3['row'], $nr3['row']);
				$completed['length']=$nr3['row'];
				while($row3 = db::fetch($rs3['row']))
				{
					$row3['status_exp'] ="";
					$row3['border_div'] = "#698299";
					$row3['back_upl'] = "#02489e";
					$row3['bor_upl'] = "#5a6f89";
					$row3['bor_det'] = "#5a6f89";
					if ($row3['created_at'] !="") {
						$row3['created_at']=format_tgl($row3['created_at']);
					}
					$completed[] = $row3;
				}
			}
			$response["message"] = "Pelayaran Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["tampilnya"=>$tampilnya,"message"=>$response["message"],"todo"=>$todo,"count_todo"=>$nr_total1['row'],"progress"=>$progress,"count_progress"=>$nr_total2['row'],"completed"=>$completed,"count_completed"=>$nr_total3['row']]]);
	}elseif ($step=="doc_list_todo") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}
   		 	$sql_total1   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at,b.revisi FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.revisi=1)$tambah_status $q order by a.created_at DESC";
			db::query($sql_total1, $rs_total1['row'], $nr_total1['row']);

   		 	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at,b.revisi FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.revisi=1)$tambah_status $q order by a.created_at DESC $limit_final";
/*			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,b.id id_del FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					  where (a.status_kiriman = '0' AND b.status ='expired') $q order by created_at DESC";*/
			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs['row'], $nr['row']);
			$todo['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				$row['status_exp']="";
				$row['expired_date'] = db::lookup("expired_date","delivery_order","id_import",$row['id']);
				if ($row['status_exp'] == "expired") {
					$row['border_div'] = "red";
					$row['back_upl'] = "red";
					$row['bor_upl'] = "red";
					$row['bor_det'] = "red";
				}elseif($row['revisi'] == "1" || $row['revisi'] == 1){
					$row['border_div'] = "#e4cf98";
					$row['back_upl'] = "#fe9700";
					$row['bor_upl'] = "#fe9700";
					$row['bor_det'] = "#e4cf98";
				}else{
					$row['border_div'] = "#698299";
					$row['back_upl'] = "#02489e";
					$row['bor_upl'] = "#5a6f89";
					$row['bor_det'] = "#5a6f89";
				}
				$row['doc_kel']="no";
				$todo[] = $row;				
			}

			$response["message"] = "Pelayaran Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"todo"=>$todo,"count_todo"=>$nr_total1['row']]]);
	}elseif ($step=="doc_list_progress") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}
   		

			$sql_total2   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." d ON (a.id=d.id_import)
			 where d.id IS NULL AND c.id IS NOT NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL OR b.revisi != 1)$tambah_status $q order by a.created_at DESC";
			db::query($sql_total2, $rs_total2['row'], $nr_total2['row']);

   		  	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_bea'] ." c ON (a.id=c.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." d ON (a.id=d.id_import)
			 where d.id IS NULL AND c.id IS NOT NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL OR b.revisi != 1)$tambah_status $q order by a.created_at DESC $limit_final";
			db::query($sql, $rs2['row'], $nr2['row']);
			$progress['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['status_exp'] ="";
				$row2['border_div'] = "#698299";
				$row2['back_upl'] = "#02489e";
				$row2['bor_upl'] = "#5a6f89";
				$row2['bor_det'] = "#5a6f89";
				$progress[] = $row2;
			}

			$response["message"] = "Pelayaran Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"progress"=>$progress,"count_progress"=>$nr_total2['row']]]);
	}elseif ($step=="doc_list_completed") {	
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}

			if ($filter_status) {
				$tambah_status=" AND a.status_beacukai = '$filter_status' ";
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}
   		  	$sql_total3   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
			 where c.id IS NOT NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL  OR b.revisi != 1)$tambah_status $q order by a.created_at DESC";
			db::query($sql_total3, $rs_total3['row'], $nr_total3['row']);

   		  	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
					LEFT JOIN ". $app['table']['file_kel'] ." c ON (a.id=c.id_import)
			 where c.id IS NOT NULL AND (a.status_kiriman = '0') AND (b.id IS NOT NULL  OR b.revisi != 1)$tambah_status $q order by a.created_at DESC $limit_final";
			db::query($sql, $rs3['row'], $nr3['row']);
			$completed['length']=$nr3['row'];
			while($row3 = db::fetch($rs3['row']))
			{
				$row3['status_exp'] ="";
				$row3['border_div'] = "#698299";
				$row3['back_upl'] = "#02489e";
				$row3['bor_upl'] = "#5a6f89";
				$row3['bor_det'] = "#5a6f89";
				$completed[] = $row3;
			}
			$response["message"] = "Data Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"completed"=>$completed,"count_completed"=>$nr_total3['row']]]);
	}elseif ($step=="detail"){	
	if ($id) {
			// $sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  where a.id = '$id' ";
			// $sql   = "SELECT a.*,b.name created_by FROM ".$app['table']['import']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
			// 		  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (c.id_import=a.id)  where a.id = '$id' ";
		
			/*$sql   = "SELECT a.qty qty, a.num_container num_container,a.note note, a.value value, b.num_pib pib, b.num_bl num_bl, b.name_ship name_ship, b.eta eta,b.id_customer id_customer FROM ".$app['table']['container']." a
					  LEFT JOIN ". $app['table']['import'] ." b ON (a.id_import=b.id)  where b.id = '$id' limit 1 ";
			$nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				$id_customer 	 =	db::get_record("customer","id",$row['id_customer']);
				$row['customer'] = $id_customer['name'];
				$user[] = $row;
			}*/

					$sqlupt		  = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '".$id_rule."' ";
					db::qry($sqlupt);
					$sql   = "SELECT  tgl_nopen,id,num_pib, status_beacukai, num_bl,nopen,name_ship,eta,id_customer,tanggal_sptnp, nomor_sptnp,nilai_sptnp, amount_payment, pfpd , status_sppb FROM ". $app['table']['import'] ."  where id = '$id' limit 1 ";
					$nonya = $mulai+1;
					// $q ORDER BY a.lang,a.reorder";
					db::query($sql, $rs['row'], $nr['row']);
					while($row = db::fetch($rs['row']))
					{
						$record_pelabuhan = db::get_record("pelabuhan","id_import",$id);
						$record_pelayaran = db::get_record("delivery_order","id_import",$id);
						$record_beacukai  = db::lookup("sppb","beacukai","id_import",$id);
						// $row["do_file"]= $record_pelayaran['do'];
						$row["do_file"]= $record_beacukai;
						$sql2   = "SELECT * FROM ".$app['table']['container']." where id_import ='$id' ";
						// $nonya = $mulai+1;
						// $q ORDER BY a.lang,a.reorder";
						db::query($sql2, $rs2['row'], $nr2['row']);
						$test["length"]= $nr2['row'];
						while($row2 = db::fetch($rs2['row']))
						{
							$test[] = $row2;
						}	
						$row['isi_container'] 		 = $test;
						
						if ($record_pelabuhan['id'] !="") {
							$row['isi_act'] 			 = $record_pelabuhan;
						}else{
							$row['isi_act'] 			 = array('eir' => "" );
							// $row['isi_act'] 			 = "aslkdj;asdsakd";
						}

							// $row['sql'] = $sql2;
						$row['eta_format'] 			 = format_tgl($row['eta']);
						$id_customer 	 			 =	db::get_record("customer","id",$row['id_customer']);
						$row['customer'] 			 = $id_customer['name'];
						if ($row['tanggal_sptnp'] !="" AND $row['tanggal_sptnp'] !="1970-01-01" AND $row['tanggal_sptnp'] !="0000-00-00") 
							$row['tanggal_sptnp_format'] = format_tgl($row['tanggal_sptnp']);
						else $row['tanggal_sptnp_format'] ="";
						$row['format_rupiah'] = rupiah($row['amount_payment']);
						$row['dok_kel']="no";	
						$user[] = $row;
					}
			
					$sql   = "SELECT id,name,id_import FROM ". $app['table']['file_bea'] ."
							  where id_import = '$id' and status = 'active' order by created_at DESC ";
					$nonya = $mulai+1;
					// $q ORDER BY a.lang,a.reorder";
					db::query($sql, $rs5['row'], $nr5['row']);
					$file_bea['length']=$nr5['row'];
					while($row5 = db::fetch($rs5['row']))
					{
						$row5['url_photo'] = api::urlApiFile_doc_path("sppb",$row5['name']);
						$file_bea[] = $row5;
					}

					$sql   = "SELECT id,name,id_import FROM ". $app['table']['file_kel'] ."
							  where id_import = '$id' and status = 'active' order by created_at DESC ";
					$nonya = $mulai+1;
					// $q ORDER BY a.lang,a.reorder";
					db::query($sql, $rs1['row'], $nr1['row']);
					$file_kel['length']=$nr1['row'];
					while($row6 = db::fetch($rs1['row']))
					{
						$row6['url_photo'] = api::urlApiFile_doc_path("sppb",$row6['name']);
						$file_kel[] = $row6;
					}

					if ($user) {
						$response["message"] = "Beacukai Berhasil Termuat";
						// echo json_encode($sql);
	        			// app::response("200", ["error"=>FALSE,"items"=>$user]);
	        			app::response("200", ["error"=>0,"data"=>["destination"=>$user,"file_bea"=>$file_bea,"file_kel"=>$file_kel],"response"=>$response]);
						// echo json_encode($response);
					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Data Masih kosong";
						// echo json_encode($response);
				        app::response("404", ["error"=>0,"data"=>["destination"=>$user,"file_bea"=>$file_bea],"response"=>$response]);
	        			// app::response("200", ["error"=>FALSE,"items"=>$response]);
						// echo json_encode($sql);
					}else{
						$response["error"]=TRUE;
						$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
						// echo json_encode($response);
				        app::response("500", ["error"=>500,"data"=>["destination"=>$user],"response"=>$response]);
						// echo json_encode($response);
					}
		}
	}elseif ($step=="revisi") {	
	if ($id_imp) {

/*			$sqlupt = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '".$id_rule."' ";
			db::qry($sqlupt);*/

			$data_import = db::get_record("import","id",$id_imp);
			app::mq_encode('p_alasan_rev');
			$sql = "update ". $app['table']['beacukai'] ."
					set 
					revisi 				= '1',
					alasan_revisi 		= '$p_alasan_rev'
					where id_import = '$id_imp'";
			db::qry($sql);

			$id321 = rand(1, 100).date("dmYHis");
			$sqlsss321 ="insert into ".$app['table']['notif']." 
					(id, id_rule, id_import, status, pesan, created_by, created_at) values
					('$id321', '7903092018114332', '$id_imp', 1, '".$data_import['num_pib']." : ".$p_alasan_rev."','$me',now())";
			db::qry($sqlsss321);

			$rs['devices'] = db::get_recordset("user_device","category='beacukai' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					// "to" => db::lookup("token","device","id",$user_devices['id_device']),
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Beacukai",
						"text" => "Revisi PIB ".$data_import['num_pib']." : ".$p_alasan_rev,
						"sound" => "default"
					),
					'data' => array(
						'method' => 'beacukai',
						'ncr_id' => $id_imp,
						"title" => "Admin Beacukai",
						"body" => "Revisi PIB ".$data_import['num_pib']." : ".$p_alasan_rev
					)
				);
				app::pushNotifSend($datapush);
			}
				app::response("200", ["error"=>0,"message"=>"Berhasil_tersimpan"]);
		}else{
			$response["error"]=TRUE;
			$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
			// echo json_encode($response);
	        app::response("500", ["error"=>500,"data"=>["destination"=>$user],"response"=>$response]);
		}
	}elseif ($step=="home_list") {	
############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				if (date("Y-m-d",strtotime($p_search)) != 1970-01-01 && date("Y-m-d",strtotime($p_search)) != "1970-01-01") {
					$q = "AND (a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
				}else{
					$q = "AND (a.num_pib LIKE '%".$p_search."%') ";
				}
			}
			if ($filter_status) {
				if ($filter_status !="ALL") {
					$tambah_status=" AND a.status_beacukai = '$filter_status' ";
				}
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}

			if ($p_awal =="tidak") {
				$pakai_awal = "tidak";
			}else{
				$pakai_awal = "iya";
			}
  			$sql_total   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at,b.revisi FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.revisi=1)$tambah_status $q order by a.created_at DESC";
			db::query($sql_total, $rs_total['row'], $nr_total['row']);

  		 	$sql   = "SELECT DISTINCT a.id,a.num_bl,a.num_pib,a.status_beacukai,a.created_at,b.revisi,a.tgl_nopen FROM ".$app['table']['import']." a
					LEFT JOIN ". $app['table']['beacukai'] ." b ON (a.id=b.id_import)
			 where (a.status_kiriman = '0') AND (b.id IS NULL OR b.revisi=1)$tambah_status $q order by a.created_at DESC $limit_final";
/*			$sql   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,b.id id_del FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." b ON (a.id=b.id_import)
					  where (a.status_kiriman = '0' AND b.status ='expired') $q order by created_at DESC";*/
			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs['row'], $nr['row']);
			$user['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				$row['status_exp']="";
				$row['expired_date'] = db::lookup("expired_date","delivery_order","id_import",$row['id']);
				if ($row['status_exp'] == "expired") {
					$row['border_div'] = "red";
					$row['back_upl'] = "red";
					$row['bor_upl'] = "red";
					$row['bor_det'] = "red";
				}elseif($row['revisi'] == "1" || $row['revisi'] == 1){
					$row['border_div'] = "#e4cf98";
					$row['back_upl'] = "#fe9700";
					$row['bor_upl'] = "#fe9700";
					$row['bor_det'] = "#e4cf98";
				}else{
					$row['border_div'] = "#698299";
					$row['back_upl'] = "#02489e";
					$row['bor_upl'] = "#5a6f89";
					$row['bor_det'] = "#5a6f89";
				}

				if ($row['created_at'] !="") {
					$row['created_at']=format_tgl($row['created_at']);
				}
				$row['doc_kel']="no";
				if ($terbaru==0 && $pakai_awal == "iya") {
					$user_terbaru[] = $row;
					$user['length']=$user['length']-1;
				}else{
					$user[] = $row;				
				}
				$terbaru++;
			}
/*			$sql2   = "SELECT id,num_bl,num_pib,status_beacukai,pfpd FROM ".$app['table']['import']." where status_kiriman = '1' $q order by created_at DESC";
			db::query($sql2, $rs2['row'], $nr2['row']);
			$user2['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$exp_1 = db::get_record("delivery_order" , "id_import = '".$row2['id']."' AND status = 'active' ");
				$exp_date2 = db::get_record("expired_do" , "id_delivery_order = '".$exp_1['id']."' AND status = 'active' ");
				if ($exp_date2['expired_do']!="") {
					$row2['expired_date'] = date("d-m-Y",strtotime($exp_date2['expired_do']));
				}
				$user2[] = $row2;
			}*/
			if ($nr_total['row'] == 0) {
				$data_kosong="iya";
			}
			$count_notif = db::lookup("count(id)","notif","id_rule = '7903092018114332' AND (status ='1' OR status =1) ");
			$response["message"] = "Pelayaran Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"data_kosong"=>$data_kosong,"progress"=>$user,"count_progress"=>$nr_total['row'],"data_terbaru"=>$user_terbaru,"count_new_notif"=>$count_notif]]);
	}elseif ($step=="delete") {	
		if (isset($id_imp)) {
			// $id 	= $_GET['id'];
			// if ($_FILES["attachment"]["name"]) {
				$delete_file = db::get_record("pelabuhan","id_import",$id_imp);
				@unlink($app['doc_path']."/sppb/". $delete_file['eir']);
				@unlink($app['doc_path']."/out_cont/". $delete_file['kelengkapan']);
/*				$nama_gambar=$_FILES["attachment"]["name"].date("dmYsHis");
			    move_uploaded_file($_FILES["attachment"]["tmp_name"], $app['doc_path']."/do/".$nama_gambar);
			    $abc="do 	= '$nama_gambar',";*/
			// }
			$sql 	= "DELETE FROM ".$app['table']['pelabuhan']." WHERE id_import ='". $id ."' ";
					   db::qry($sql);
					   db::qry($sql2);
					   $response["error"] = FALSE;
					   // $response["id"] = $user["unique_id"];
					   $response["message"] = "Data Berhasil di Hapus";
	        			// app::response("200", ["error"=>FALSE,"items"=>$user]);
	        			app::response("200", $response);
					   // echo json_encode($response);
					   // echo json_encode($sql);
		}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Identitas Kosong";
	       				app::response("404", ["items"=>$response]);
					   // echo json_encode($response);
		}
	}elseif ($step=="completed_doc") {	

/*			$q 		 = null;
			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['pelabuhan']." ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);     */       
	  		// $sqlnya = "select * from dmg_container LIMIT $mulai, $halaman";
	  		// db::query($sqlnya, $rs['row'], $nr['row']);

############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################

			if (isset($cari)) {
				// $q = "where pfpd LIKE '%".$cari."%' OR tanggal_sppb LIKE '%".$cari."%' OR tanggal LIKE '%".$cari."%' ";
				$q = "AND d.num_pib LIKE '%".$cari."%' OR d.status_beacukai LIKE '%".$cari."%' ";
			}
			// else{
			// 	$q = " LIMIT $mulai, $halaman";
			// }

			// $sql = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
			// 		a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
			// 		b.name name FROM ".$app['table']['beacukai']." a
			// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q LIMIT 5";
		
		/*	$sql   = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
					  a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					  b.name name,a.nilai_sptnp nilai_sptnp,a.nomer_sptnp nomer_sptnp,a.tgl_sptnp tgl_sptnp FROM ".$app['table']['beacukai']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q  LIMIT $mulai, $halaman ";*/

/*			$sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  $q  LIMIT $mulai, $halaman ";*/
			$sql   = "SELECT a.*,c.id id_exp FROM ".$app['table']['pelabuhan']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import)
					  LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id)
					  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id_import=e.id_import) where d.status_kiriman = '1' $q ";
			// $nonya = $mulai+1;
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
			/*	$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id_idv']."' and status = 'active'");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id_idv']."' ");

				$row['date_do']=$datas['do'];
				if ($row['date_do']!=null&&$row['date_do']!="0000-00-00") {
					$row['date_do']= date("d-m-Y",strtotime($row['date_do']));
				}else{
					$row['date_do']="";
				}

				// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
				// $row['expired_do']= $datas['do'];

				// if (strtotime($datas['do']) < time()) {
				if ($datas['do'] < date("Y-m-d")) {
					isset($row['id_idv'])?$id_exp [] = $row['id_idv']:null;
				}
				else{
					isset($row['id_idv'])?$id_exp_act [] = $row['id_idv']:null;
				}*/
				$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
				if ($exp_date['expired_do']!="") {
					$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
				}
				$user[] = $row;
			}
		/*	$id_exp 	= implode("','", $id_exp);
			$id_exp_act = implode("','", $id_exp_act);
			$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
			$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
			db::qry($sql321);
			db::qry($sql1231);*/
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					if ($user) {
						$response["message"] = "Pelabuhan Berhasil Termuat";
						// echo json_encode($sql);
	        			app::response("200", ["error"=>FALSE,"completed"=>$user,"count_completed"=>$nr['row']]);
						// echo json_encode($response);
					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Data Masih kosong";
						// echo json_encode($response);
				        app::response("404", ["completed"=>$response]);
	        			// app::response("200", ["error"=>FALSE,"items"=>$response]);
						// echo json_encode($sql);
					}else{
						$response["error"]=TRUE;
						$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
						// echo json_encode($response);
				        app::response("500", ["completed"=>$response]);
						// echo json_encode($response);
					}
	}elseif ($step=="view_2") {	
		$q=null;
		$halaman = 2;
		$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
		$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
		$result  = "SELECT * FROM ".$app['table']['pelabuhan']." ";
		db::query($result, $rs['rows'], $nr['rows']);
	  	$pages 	 = ceil($nr['rows']/$halaman);
		if (isset($cari)) {
			$q = "AND d.num_pib LIKE '%".$cari."%' OR status_sppb LIKE '%".$cari."%' ";
		}
		$sql = "SELECT a.*,d.status_beacukai status_sppb,c.id id_idv FROM ".$app['table']['pelabuhan']." a
				LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
				LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import) 
				LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id) 
				where c.status = 'active' $q ";
		$nonya = $mulai+1;
		db::query($sql, $rs['row'], $nr['row']);
		while($row = db::fetch($rs['row']))
		{
				$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id_idv']."' and status = 'active'");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id_idv']."' ");

				$row['date_do']=$datas['do'];
				if ($row['date_do']!=null&&$row['date_do']!="0000-00-00") {
					$row['date_do']= date("d-m-Y",strtotime($row['date_do']));
				}else{
					$row['date_do']="";
				}

				// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
				// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
				// $row['expired_do']= $datas['do'];

				// if (strtotime($datas['do']) < time()) {
				if ($datas['do'] < date("Y-m-d")) {
					isset($row['id_idv'])?$id_exp [] = $row['id_idv']:null;
				}
				else{
					isset($row['id_idv'])?$id_exp_act [] = $row['id_idv']:null;
				}
			$view[] = $row;
		}
			$id_exp 	= implode("','", $id_exp);
			$id_exp_act = implode("','", $id_exp_act);
			$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
			$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
			db::qry($sql321);
			db::qry($sql1231);
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
				if ($view) {
					$response["message"] = "Beacukai Berhasil Termuat";
	       			app::response("200", ["error"=>FALSE,"items"=>$view]);
					// echo json_encode($response);
				}elseif ($view==0) {
					$response["error"]=FALSE;
					$response["message"] = "Data Masih kosong";
					// echo json_encode($response);
			        app::response("404", ["items"=>$response]);
	       			// app::response("200", ["error"=>FALSE,"items"=>$response]);
					// echo json_encode($sql);
				}else{
					$response["error"]=TRUE;
					$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
			        app::response("500", ["items"=>$response]);
					// echo json_encode($response);
				}
	}else{
			$q=null;
			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['pelabuhan']." ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);            
	  		// $sqlnya = "select * from dmg_container LIMIT $mulai, $halaman";
	  		// db::query($sqlnya, $rs['row'], $nr['row']);

############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
########################################################################################

			if (isset($p_search)) {
				// $q = "where pfpd LIKE '%".$cari."%' OR tanggal_sppb LIKE '%".$cari."%' OR tanggal LIKE '%".$cari."%' ";
				#$q = "AND (a.num_pib LIKE '%".$p_search."%' OR a.status_beacukai LIKE '%".$p_search."%') ";
				$q = "AND (a.num_pib LIKE '%".$p_search."%' OR a.status_beacukai LIKE '%".$p_search."%' OR a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}
			// else{
			// 	$q = " LIMIT $mulai, $halaman";
			// }

			// $sql = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
			// 		a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
			// 		b.name name FROM ".$app['table']['beacukai']." a
			// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q LIMIT 5";
		
		/*	$sql   = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
					  a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					  b.name name,a.nilai_sptnp nilai_sptnp,a.nomer_sptnp nomer_sptnp,a.tgl_sptnp tgl_sptnp FROM ".$app['table']['beacukai']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) $q  LIMIT $mulai, $halaman ";*/

/*			$sql   = "SELECT a.* FROM ".$app['table']['delivery_order']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  $q  LIMIT $mulai, $halaman ";*/
			// $sql   = "SELECT a.*,c.id id_idv FROM ".$app['table']['pelabuhan']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
			// 		  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import)
			// 		  LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id)
			// 		  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id_import=e.id_import)  where d.status_kiriman = '0' $q ";

					  
/* bisa dipakai 			$sql   = "SELECT a.*,c.id id_exp,d.num_pib num_pib,d.status_beacukai status_beacukai,d.id id,a.id id_pelabuhan FROM ".$app['table']['pelabuhan']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import)
					  LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id)
					  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id_import=e.id_import)  where d.status_kiriman = '0' $q ";					  */


		/* 	$sql = "SELECT a.id, a.status, a.num_pib AS num_pib, b.name as created_by,a.created_at,a.updated_at,a.status_beacukai sb
				FROM ". $app['table']['import'] ." a 	
				LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  $q ORDER BY a.created_at DESC";*/
			$sql   = "SELECT a.id id,a.num_bl num_bl, a.status status, a.num_pib num_pib,a.status_sppb status_sppb,a.status_beacukai status_beacukai,c.id id_exp,a.num_pib num_pib,a.status_beacukai status_beacukai,a.id id FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id = c.id_import)
					  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id=e.id_import)  where a.status_kiriman = '0' $q order by a.created_at DESC ";
			db::query($sql, $rs['row'], $nr['row']);
			$user['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				#$pelabuhan_id =  db::lookup("id", "pelabuhan", "id_import='". $row['id'] ."' ");
				$beacukai_id =  db::lookup("1", "pelabuhan", "id_import='". $row['id'] ."' ");
				// if ($beacukai_id !='1') {

				#$exp = db::get_record("delivery_order" , "id_import = '".$row['id']."' AND status = 'expired' ");
				$exp2 = db::get_record("delivery_order" , "id_import = '".$row['id']."' ");
				// if ($exp2['id'] =="") {
				// 	$user[] = $row;
				// }else{
					if ($exp2['id'] !="") {
						$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$exp2['id']."' AND status = 'active' ");
						if ($exp_date['expired_do']!="") {
							$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
							$user[] = $row;
						}else{
							// $row['expired_date'] = "";
							$user[] = $row;
						}
					}else{
						// $row['expired_date'] = "";
						$user[] = $row;
					}
				// }

					#$user[] = $row;	
/*				}else{
					$user['length'] = $user['length'] -1;
				}*/
				#$pelabuhan_kel =  db::lookup("kelengkapan", "pelabuhan", "id_import='". $row['id'] ."' ");	
				#$pelabuhan_eir =  db::lookup("eir", "pelabuhan", "id_import='". $row['id'] ."' ");	

				// if ($status_beacukai =="" AND $id_exp !="") {
				/*if ($row['status_beacukai'] =="" AND $row['id_exp'] !="") {
					$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
					if ($exp_date['expired_do']!="") {
						$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
					}
					$user[] = $row;	
				}else{
					if ($pelabuhan_kel !="") {
						$user['length']=$user['length']-1;
					}else{
						if ($pelabuhan_id !="") {
							// if ($row['status_beacukai'] =="SPJM") {
							if ($pelabuhan_eir !="") {
								// exit;
								if ($row['status_sppb'] =="0") {
									if ($pelabuhan_kel =="") {
										$user['length']=$user['length']-1;
									}else{
										$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
										if ($exp_date['expired_do']!="") {
											$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
										}
										$user[] = $row;	
									}
								}else{
									$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
									if ($exp_date['expired_do']!="") {
										$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
									}
									$user[] = $row;	
								}
							}else{	
								$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
								if (($exp_date['expired_do']!="" || $exp_date['expired_do'] !="1970-01-01") && $row['id_exp'] !="" ) {
									$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
									if ($row['status_sppb'] =="1") {
										$user[] = $row;
									}else{
										$user['length']=$user['length']-1;
									}
								}else{
									$user['length']=$user['length']-1;
								}
							}
						}else{
							if ($row['status_beacukai'] =="SPJM") {
								//$pelabuhan_kel =="0"
								if ($pelabuhan_eir !="") {
								// if ($row['status_sppb'] =="0") {
									$user['length']=$user['length']-1;
								}else{
									$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
									if ($exp_date['expired_do']!="") {
										$row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
									}
									$user[] = $row;	
								}
							}else{
									$user['length']=$user['length']-1;
							}
						}
					}
				}*/
			}

			// $sql2   = "SELECT a.*,c.id id_exp,d.status_beacukai status_beacukai,d.num_pib num_pib,d.id id,a.id id_pelabuhan FROM ".$app['table']['pelabuhan']." a
			// 		  LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
			// 		  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import=c.id_import)
			// 		  LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import=d.id)
			// 		  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id_import=e.id_import) where d.status_kiriman = '1' $q ";
/*			$sql2   = "SELECT a.*,c.id id_exp,a.status_beacukai status_beacukai,a.num_pib num_pib,a.id id FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id=c.id_import)
					  LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id=e.id_import) where a.status_kiriman = '1' $q ";*/

			$sql2   = "SELECT a.id id,a.num_bl num_bl,a.num_pib num_pib,a.status_beacukai status_beacukai,c.id id_exp,a.status_beacukai status_beacukai,a.num_pib num_pib,a.id id FROM ".$app['table']['import']." a
					  LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id=c.id_import)
					  where a.status_kiriman = '1' $q order by a.created_at DESC";
			db::query($sql2, $rs2['row'], $nr2['row']);
			$user2['length'] = $nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$exp_date2 = db::get_record("expired_do" , "id_delivery_order = '".$row2['id_exp']."' AND status = 'active' ");
				if ($exp_date2['expired_do']!="") {
					$row2['expired_date'] = date("d-m-Y",strtotime($exp_date2['expired_do']));
				}
				$user2[] = $row2;
			}
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					// if ($user) {
			$response["message"] = "Pelabuhan Berhasil Termuat";
			// echo json_encode($sql);
/*			app::response("200", ["error"=>FALSE,"progress"=>$user,"count_progress"=>$nr['row'],"completed"=>$user2,"count_completed"=>$nr2['row'],"sql"=>$sql,"sql2"=>$sql2]);*/
	
			// app::response("200", ["error"=>FALSE,"progress"=>$user,"count_progress"=>$nr['row'],"completed"=>$user2,"count_completed"=>$nr2['row']]);			
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"progress"=>$user,"count_progress"=>$nr['row'],"completed"=>$user2,"count_completed"=>$nr2['row']]]);
						// echo json_encode($response);
					// }elseif ($user==0) {
					// 	$response["error"]=FALSE;
					// 	$response["message"] = "Data Masih kosong";
					// 	// echo json_encode($response);
				 //        app::response("404", ["progress"=>$response]);
	    //     			// app::response("200", ["error"=>FALSE,"items"=>$response]);
					// 	// echo json_encode($sql);
					// }else{
					// 	$response["error"]=TRUE;
					// 	$response["error_message"] = "Terjadi Kesalahan Saat Memuat";
					// 	// echo json_encode($response);
				 //        app::response("500", ["progress"=>$response]);
					// 	// echo json_encode($response);
					// }
		}
}elseif ($act=="eir") {	
		if (isset($id)) {
			// $id 	= $_GET['id'];
			$sql = "SELECT a.status status,a.tanggal tanggal,a.pfpd pfpd,
					a.tanggal_sppb tanggal_sppb,a.date_delivery date_delivery,
					b.name name FROM ".$app['table']['beacukai']." a
					LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) WHERE id ='$id' ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				$user[] = $row;
			}
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					if ($user) {
						$response["error"] = FALSE;
						$response["beacukai"] = $user["beacukai"]['status'];
						$no=0;
							foreach ($user as $key) {
								$response["beacukai"][$no]["status"] 		= $user[$no]['status'];
								$response["beacukai"][$no]["tanggal"]		= $user[$no]['tanggal'];
								$response["beacukai"][$no]["pfpd"] 			= $user[$no]['pfpd'];
								$response["beacukai"][$no]["tanggal_sppb"] 	= $user[$no]['tanggal_sppb'];
								$response["beacukai"][$no]["date_delivery"] = $user[$no]['date_delivery'];
								$response["beacukai"][$no]["nomer_sptnp"] 	= $user[$no]['nomer_sptnp'];
								$response["beacukai"][$no]["tgl_sptnp"] 	= $user[$no]['tgl_sptnp'];
								$response["beacukai"][$no]["nilai_sptnp"]	= $user[$no]['nilai_sptnp'];
								$response["beacukai"][$no]["name"] 			= $user[$no]['name'];
								// $response["beacukai"][$no]["no"] 			= $no;
								$no++;
							}
						$response["message"] = "Beacukai Berhasil Termuat";
						echo json_encode($response);
					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Belum Input Data";
						echo json_encode($response);
						// echo json_encode($sql);
					}
			}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Terjadi Kesalahan Saat Menginput";
					   echo json_encode($response);
			}
	}elseif($act == "notifikasi"){
		if (isset($id_rule)) {
/*			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['notif']." WHERE id_rule ='$id_rule' ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);     */       

			// $id 	= $_GET['id'];
			/* $sql = "SELECT a.*,b.name nama FROM ".$app['table']['notif']." a LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) WHERE id_rule ='$id_rule' ORDER BY a.created_at DESC LIMIT $mulai, $halaman ";*/

############################ EXPIRED DELIVERY ORDER ####################################
	/*		$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}*/
########################################################################################

				/*	$sql = "SELECT a.*,a.id_import id_imp,b.name nama,c.id id_exp 
					FROM ".$app['table']['notif']." a 
					LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
					LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import = c.id_import) 
					WHERE id_rule ='$id_rule' ORDER BY a.created_at DESC";*/
/*			$sql = "SELECT a.*,a.id_import id_imp,d.status_sppb status_sppb,b.name nama,c.id id_exp,d.num_pib num_pib,d.num_bl num_bl,d.status_beacukai status_beacukai FROM ".$app['table']['notif']." a 
					LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
					LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import = c.id_import)
					LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import = d.id) 
					WHERE id_rule ='$id_rule' and a.status ='1' ORDER BY a.created_at ASC LIMIT 1";*/
			$sql = "SELECT a.*,a.id_import id_imp,d.status_sppb status_sppb,b.name nama,c.id id_exp,d.num_pib num_pib,d.num_bl num_bl,d.status_beacukai status_beacukai FROM ".$app['table']['notif']." a 
					LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
					LEFT JOIN ". $app['table']['delivery_order'] ." c ON (a.id_import = c.id_import)
					LEFT JOIN ". $app['table']['import'] ." d ON (a.id_import = d.id) 
					WHERE id_rule ='$id_rule' and a.status ='1' ORDER BY a.created_at ASC LIMIT 1";
					// $q ORDER BY a.lang,a.reorder";
					db::query($sql, $rs['row'], $nr['row']);
					// $no=0;
					while($row = db::fetch($rs['row']))
					{
						// $exp_date = db::get_record("expired_do","id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");

						$pelabuhan_eir =  db::lookup("eir", "pelabuhan", "id_import='". $row['id_imp'] ."' ");	
						if ($id_rule == "5403092018114622") {
							if ($row['status_beacukai'] == "SPJM") {
								$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
								if ($exp_date['expired_do']!="") {
									// $row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
									$row['expired_date'] = format_tgl($exp_date['expired_do']);
								}
								// print_r($row['id']);
								// print_r($row);
								// print_r($row['nama']);
								$user[] = $row;
							}else{
								$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
								if ($exp_date['expired_do']!="" && $row['id_exp'] !="") {
									// $row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
									$row['expired_date'] = format_tgl($exp_date['expired_do']);
									if ($row['status_sppb'] =="1") {
										$user[] = $row;
									}
								}
								// print_r($row['id']);
								// print_r($row);
								// print_r($row['nama']);
							}
						}else{
							$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$row['id_exp']."' AND status = 'active' ");
							if ($exp_date['expired_do']!="") {
								// $row['expired_date'] = date("d-m-Y",strtotime($exp_date['expired_do']));
								$row['expired_date'] = format_tgl($exp_date['expired_do']);
							}
							// print_r($row['id']);
							// print_r($row);
							// print_r($row['nama']);
							$user[] = $row;
						}
						// $no++;
					}
					// $req_dump = print_r($sql, TRUE);
					// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
					// fwrite($myfile,$req_dump);
					// fclose($myfile);
					if ($user) {
	        			app::response("200", ["error"=>FALSE,"message"=>"Notifikasi Berhasil Termuat","data"=>["notification"=>$user]]);
					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Belum Input Data";
						// echo json_encode($response);
	        			app::response("404", ["error"=>FALSE,"message"=>$response]);
						// echo json_encode($sql);
					}
			}else{
						$response["error"] = TRUE;
						// $response["id"] = $user["unique_id"];
						$response["error_message"] = "Terjadi Kesalahan Saat Menginput";
						// echo json_encode($response);
	        			app::response("500", ["error"=>TRUE,"message"=>$response]);
			}

	}elseif($act == "forgot"){
		if (isset($username)) {
/*			$halaman = 2;
			$page 	 = isset($_GET["halaman"])?(int)$_GET["halaman"] : 1;
			$mulai 	 = ($page>1) ? ($page * $halaman) - $halaman : 0;
			$result  = "SELECT * FROM ".$app['table']['notif']." WHERE id_rule ='$id_rule' ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($result, $rs['rows'], $nr['rows']);
	  		$pages 	 = ceil($nr['rows']/$halaman);     */       

			// $id 	= $_GET['id'];
			/* $sql = "SELECT a.*,b.name nama FROM ".$app['table']['notif']." a LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) WHERE id_rule ='$id_rule' ORDER BY a.created_at DESC LIMIT $mulai, $halaman ";*/

			// app::sendmail($row['email'], "Ada Dokumen Delivery Order Masuk", $message, $path_do);
			$sql = "SELECT * FROM ".$app['table']['user']." WHERE username ='$username' LIMIT 1 ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				$user[] = $row;
			}			
			/*$sql = "SELECT a.*,b.name nama FROM ".$app['table']['notif']." a LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) WHERE id_rule ='$id_rule' ORDER BY a.created_at DESC LIMIT 1 ";
			// $q ORDER BY a.lang,a.reorder";
			db::query($sql, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				$user[] = $row;
			}*/

			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					if ($user) {
						$message  .= "<p>Klik link untuk mengubah password dengan username : ".$user[0]['username']."</p>";
						$message .= "<p><a href='". $app['http'] ."/forgot.mod&act=forgot&step=1&id=".$user[0]['id']."'>Ubah Password</a></p>";
						$message .= "<p>Terima kasih</p>";
						app::sendmail($user[0]['email'], "Penggantian Password User Aplikasi Dermaga", $message);
						/*$response["error"] = FALSE;
						// $response["beacukai"] = $user["beacukai"]['status'];
						$no=0;
							foreach ($user as $key) {
								$response["notifikasi"][$no]["id"] 			= $user[$no]['id'];
								$response["notifikasi"][$no]["id_rule"]		= $user[$no]['id_rule'];
								$response["notifikasi"][$no]["id_import"] 	= $user[$no]['id_import'];
								$response["notifikasi"][$no]["status"]		= $user[$no]['status'];
								$response["notifikasi"][$no]["pesan"] 		= $user[$no]['pesan'];
								$response["notifikasi"][$no]["nama"]		= $user[$no]['nama'];
								$response["notifikasi"][$no]["created_at"] 	= $user[$no]['created_at'];
								$response["notifikasi"][$no]["no"] 			= $no;
								$no++;
							}
						$response["message"] = "Notifikasi Berhasil Termuat";
						echo json_encode($response);*/
	        			// app::response("200", ["error"=>FALSE,"message"=>"Permintaan Telah Dikirim","items"=>$user]);
	        			app::response("200", ["error"=>FALSE,"email"=>$user[0]['email'],"message"=>"Cek email yg terdaftar untuk mengganti password"]);
					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Email Tidak Terdaftar";
						// echo json_encode($response);
	        			app::response("500", ["error"=>FALSE,"message"=>$response]);
						// echo json_encode($sql);
					}
			}else{
						$response["error"] = TRUE;
						// $response["id"] = $user["unique_id"];
						$response["error_message"] = "Terjadi Kesalahan Saat Menginput";
						// echo json_encode($response);
	        			app::response("500", ["error"=>TRUE,"message"=>$response]);
			}
	}elseif($act == "status_proses"){
		if (isset($keyword) AND $keyword !="" ) {
			// $lang_id =db::get_record("lang","alias",$lang);
			// $sql = "SELECT * FROM ".$app['table']['status']." WHERE id_import ='$statusnya' and lang = '".$lang_id['id']."' ";
			$id_containernya = db::lookup("id","container","num_container",$keyword);

			#$sql = "SELECT * FROM ".$app['table']['status']." WHERE num_container ='$keyword' and lang = '".$lang."' ";
			$sql = "SELECT * FROM ".$app['table']['status']." WHERE id_container ='$id_containernya' and lang = '".$lang."' ";
			db::query($sql, $rs['row'], $nr['row']);

			$id_container = db::lookup("id_container","status","num_container ='".$keyword."' ");

			$sql = "SELECT a.num_container num_container,b.num_pib num_pib,b.num_bl num_bl,b.id_customer id_customer,b.status_kiriman status_kiriman,b.name_ship name_ship,b.eta eta,b.amount_payment amount_payment,b.nilai_sptnp nilai_sptnp,a.address address,a.ket_cust ket_cust FROM ".$app['table']['container']." a
					LEFT JOIN ". $app['table']['import'] ." b ON (a.id_import=b.id) 
					WHERE a.id ='$id_container' ";
			// $q ORDER BY a.lang,a.reorder";
			while($row = db::fetch($rs['row']))
			{
				$status_pengiriman = explode(",",$row['positions']);
				if ($status_pengiriman[0] !="") {
					$row['proses_dokumen'] = $status_pengiriman[0];
				}else{
					$row['proses_dokumen_kosong'] = app::getliblang('proses_dokumen_kosong',$lang);
				}
				$row['proses_jadwal']  = $status_pengiriman[1];
/*				print_r($status_pengiriman);
				exit;
				format_tgl
				*/
				$row['created_at_bahasa']  = format_tgl_bahasa($row['created_at'],$lang);

				$row['update_at_bahasa']  	= format_tgl_bahasa($row['updated_at'],$lang);
				$user[] = $row;
			}
			db::query($sql, $rs['row'], $nr2['row']);
			while($row = db::fetch($rs['row']))
			{
				$row['customer'] = db::lookup("name","customer","id='".$row['id_customer']."' ");
				$user2[] = $row;
			}
			// $req_dump = print_r($sql, TRUE);
			// $myfile = fopen("qweasdzxc.txt", "w") or die("Unable to open file!");
			// fwrite($myfile,$req_dump);
			// fclose($myfile);
					if ($user) {
						$response["error"] = FALSE;
						$response["status"]["total"] = $nr['row'];
						// $response["beacukai"] = $user["beacukai"]['status'];
						$no=0;
						foreach ($user as $key) {
						//	$response["status"][$no]["id"] 			= $user[$no]['id'];
						// 	$response["status"][$no]["id"] 			 = $key['id'];
						// 	$response["status"][$no]["id_import"]	 = $key['id_import'];
						// 	$response["status"][$no]["id_container"] = $key['id_container'];
						// 	$response["status"][$no]["positions"] 	 = $key['positions'];
						// 	$response["status"][$no]["lang"] 		 = $key['lang'];
						// 	// $response["notifikasi"][$no]["pesan"] 		= $user[$no]['pesan'];
						// 	// $response["notifikasi"][$no]["nama"]		= $user[$no]['nama'];
						// 	// $response["notifikasi"][$no]["created_at"] 	= $user[$no]['created_at'];
						// 	// $response["notifikasi"][$no]["no"] 			= $no;
						// 	$no++;
						}
						// $response["message"] = "Notifikasi Berhasil Termuat";
						$response["error"] = 0;
						$response["items"]=$user;
						$response["items2"]=$user2;
						$response["items_length"]=$nr['row'];
						$response["item2_length"]=$nr2['row'];
						// echo json_encode($response);
	        			// app::response("200", ["error"=>0,"items"=>$user,"item2"=>$user2]);

						app::response("200", ["error"=>0,"data"=>["items"=>$user,"items_length"=>$nr['row'],"item2"=>$user2,"item2_length"=>$nr2['row'],"keyword_not_match"=>""]]);


					}elseif ($user==0) {
						$response["error"]=FALSE;
						$response["message"] = "Belum Input Data";
						$keyword_not_match = app::getliblang('keyword_not_match',$lang);
						// echo json_encode($response);
						#error 300
	        			app::response("200", ["error"=>"300","data"=>["message"=>$response,"keyword_not_match"=>$keyword_not_match]]);
						// echo json_encode($sql);
					}
			}else{
					   $response["error"] = TRUE;
					   // $response["id"] = $user["unique_id"];
						$response["error_message"] = "Terjadi Kesalahan Saat Menginput";
					   // echo json_encode($response);
	        			app::response("500", ["error"=>TRUE,"message"=>$response]);
			}
	}elseif ($act=="notif_baru") {	
			if ($id_rule) {
############################ EXPIRED DELIVERY ORDER ####################################
/*			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}*/
########################################################################################
			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
			}
   		 	$sql   = "SELECT DISTINCT id,id_rule,id_import,id_container,status,pesan,created_at 
   		 			  FROM ".$app['table']['notif']."
					  where (DATE(created_at) = CURDATE()) AND id_rule ='$id_rule' ORDER BY created_at DESC";
			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs['row'], $nr['row']);
			$today['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				$row['created_at'] = format_tgl_waktu($row['created_at']);
				$today[] = $row;				
			}
   		 	$sql   = "SELECT DISTINCT id,id_rule,id_import,id_container,status,pesan,created_at 
   		 			  FROM ".$app['table']['notif']."
					  where (DATE(created_at) = CURDATE()-1) AND id_rule ='$id_rule' ORDER BY created_at DESC";
			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs2['row'], $nr2['row']);
			$yesterday['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['created_at'] = format_tgl_waktu($row2['created_at']);
				$yesterday[] = $row2;				
			}
   		 	$sql   = "SELECT DISTINCT id,id_rule,id_import,id_container,status,pesan,created_at 
  		 			  FROM ".$app['table']['notif']."
					  where ((DATE(created_at) < CURDATE()-1) AND (DATE(created_at) > CURDATE()-7)) AND id_rule ='$id_rule' ORDER BY created_at DESC";
			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs3['row'], $nr3['row']);
			$week['length']=$nr3['row'];
			while($row3 = db::fetch($rs3['row']))
			{
				$row3['created_at'] = format_tgl_waktu($row3['created_at']);
				$week[] = $row3;				
			}
   		 	$sql   = "SELECT DISTINCT id,id_rule,id_import,id_container,status,pesan,created_at 
   		 			  FROM ".$app['table']['notif']."
					  where (DATE(created_at) < CURDATE()-7) AND id_rule ='$id_rule' ORDER BY created_at DESC";
			$nonya = $mulai+1;
			$terbaru = 0;
			db::query($sql, $rs4['row'], $nr4['row']);
			$other['length']=$nr4['row'];
			while($row4 = db::fetch($rs4['row']))
			{
				$row4['created_at'] = format_tgl_waktu($row4['created_at']);
				$other[] = $row4;				
			}
			$response["message"] = "Pelayaran Berhasil Termuat";
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"today"=>$today,"count_today"=>$nr['row'],"yesterday"=>$yesterday,"count_yesterday"=>$nr2['row'],"week"=>$week,"count_week"=>$nr3['row'],"other"=>$other,"count_other"=>$nr4['row']]]);
		}else{
		   $response["error"] = TRUE;
			$response["error_message"] = "Terjadi Kesalahan Saat Memproses";
			app::response("500", ["error"=>TRUE,"message"=>$response]);
		}
	}elseif($act == "login"){
	  	form::init();
	  	form::serialize_form();
	  	// form::validate('empty', 'p_username,p_password');
	  	if (form::is_error()){
				msg::set_message('error', app::getliblang('login_invalid'));
	    		form::set_error(1);
	    		header("location: ". $app['webmin'] ."/auth.do&error=1");
	    		exit;
	  	}
	  	if( db::anti_sql_injection($_POST['p_username']) and db::anti_sql_injection($_POST['p_password']))
	  	{
			$passwordhash = md5(serialize($p_password));
		    $sql = "SELECT * FROM " .$app['table']['user'] ." WHERE username = '$p_username' AND passwordhash = '$passwordhash' AND `status` = 'active' LIMIT 1";
		    $user_login=db::get_record("user","username = '$p_username' AND passwordhash = '$passwordhash' AND `status` = 'active'");
		    $rule_login=db::get_record("user_det","id_user",$user_login['id']);

		    if ($rule_login['id_rule'] == "2120082018161449") {
		    	$p_category 	="pelayaran";
		    }elseif ($rule_login['id_rule'] == "5403092018114622") {
		    	$p_category 	="pelabuhan";
		    }elseif ($rule_login['id_rule'] == "7903092018114332") {
		    	$p_category 	="beacukai";
		    }
		    db::query($sql, $rs['login'], $nr['login']);
			if($nr['login']){
				$member =db::fetch($rs['login']);
	            form::reset();



				$config = db::get_record("configuration","id","1");
				$cek_device['id'] = db::lookup("count(id)","user_device","id_user='".$member['id']."' AND status='active' ");

//////////////////// bisa di buka saat firebase bisa digunakan ////////////////

				if(!isset($p_version) && $config['app_version'] != $p_version){
					$response['error'] = 212;
/*					$callback['message'] = "Distributor Apps telah mengalami pembaruan.\r
					Silahkan hapus/uninstall aplikasi anda, kemudian download & install kembali pada link berikut:\r\n
					http://distributor.kalsi.co.id/distributor.apk
					\r\n
					Anda tetap bisa login dengan akun sebelumnya.";
					$callback['url'] = "http://distributor.kalsi.co.id/distributor.apk";*/
				}else{
					if(!isset($member['status']) || empty($member['status']) || $member['status']=="inactive"){
						$response['error'] = 1;
						$response['message'] = "Akun anda sedang dinonaktifkan";
					}else{
						if($cek_device['id'] > 0){
							if($p_isVirtual == "false"){
								$response['message'] = "Akun pada device lain telah di logout";
							}else{
								$response['message'] = "Login berhasil.";
							}
						}else{
							$response['message'] = "Login berhasil.";
						}
/*						$member['distributor_name'] = (is_null($member['distributor_name'])?"Owner":$member['distributor_name']);
						$member['address'] = (is_null($member['address'])?"":$member['distributor_name']);*/
						$data = $member;

						$device = db::get_record("device","uuid='$p_uuid' and category='$p_category' and serial='$p_serial'");
						if(!isset($device['id']) || empty($device['id'])){
							$id = rand(1, 100).rand(1, 100).date("dmYHis");
							$sql = "INSERT INTO ".$app['table']['device']." (id, category, uuid, serial, platform, model, manufacture, isVirtual, token, created_at) VALUES
							('$id', '$p_category', '$p_uuid', '$p_serial', '$p_platform', '$p_model', '$p_manufacturer', '$p_isVirtual', '$p_token', now())";
							db::qry($sql);
							$device['id'] = $id;
							$device['token'] = $p_token;
						}else{
							if($p_isVirtual == "false"){
								$sql = "UPDATE ".$app['table']['device']."
										SET token = '$p_token',
											updated_at = now()
										WHERE id = '".$device['id']."'";
								db::qry($sql);
							}
						}
						$user_device = db::get_record("user_device","id_user='".$member['id']."' and id_device='".$device['id']."'");
						if(!isset($user_device['id_user']) || empty($user_device['id_user'])){
							$sql = "INSERT INTO ".$app['table']['user_device']." (id_user, id_device, status,app_version,category,created_at) VALUES ('".$data['id']."', '".$device['id']."', 'active','$p_version','$p_category',now())";
							db::qry($sql);
						}else{
							if($p_isVirtual == "false"){
								$sql = "UPDATE ".$app['table']['user_device']."
											SET notif = 'true', 
												status='active', 
												updated_at = now(),
												app_version = '$p_version'
										WHERE id_device = '".$device['id']."' and id_user='".$data['id']."'";
								db::qry($sql);
							}else{
								$sql = "UPDATE ".$app['table']['user_device']."
											SET notif = 'true', 
												status='active', 
												updated_at = now(),
												app_version = '$p_version'
										WHERE id_device = '".$device['id']."' and id_user='".$data['id']."'";
								db::qry($sql);
							}
						}
						$device['notif'] = true;

						$data['notif'] = $device['notif'];
						if($p_isVirtual == "false"){
							$rs['devices'] = db::get_recordset("user_device","id_user='".$member['id']."' AND status='active' AND id_device!='".$device['id']."'");
							while($user_devices = db::fetch($rs['devices'])){
								$datapush = array(
									"to" => db::lookup("token","device","id",$user_devices['id_device']),
									"notification" => array(
										"title" => "Upaya login dari $p_model",
										"text" => "Akun barusaja digunakan untuk login pada perangkat $p_model",
										"sound" => "default"
									),
									'data' => array(
										'method' => 'logout',
										"title" => "Auto Logout",
										"body" => ""
									)
								);
								app::pushNotifSend($datapush);
							}
							$sql = "UPDATE ".$app['table']['user_device']."
										SET status='inactive',
											updated_at = now()
									WHERE id_user='".$member['id']."' AND id_device!='".$device['id']."'";
							db::qry($sql);
						}

					}
				}
/////////////////////////////////////////////////////////////////

				$response["error"] = FALSE;
				// $response["id"] = $user["unique_id"];
				$response["message"] = "Berhasil Login";
	        	// app::response("200", ["error"=>FALSE,"response"=>$response,"id_user"=>$user_login['id'],"rule"=>$login,"id_rule"=>$rule_login['id_rule'],"name"=>$user_login['name'],"session"=>db::fetch($rs['login'])]);
	        	app::response("200", ["error"=>$response["error"],"message"=>$response["message"],"data"=>["id_user"=>$user_login['id'],"rule"=>$p_category,"id_rule"=>$rule_login['id_rule'],"name"=>$user_login['name'],"session"=>db::fetch($rs['login'])]]);

	            // header("location: ". $app['webmin'] ."/dashboard.do");
			}else{
/*	            msg::set_message('error', app::getliblang('login_invalid'));
	            msg::build_msg();*/
				$response["error"] = FALSE;
				// $response["id"] = $user["unique_id"];
				$response["message"] = "Username atau Password Salah";
	        	app::response("200", ["error"=>500,"message"=>$response["message"]]);
	            // header("location: ". $app['webmin'] ."/auth.do&error=1");
			}
	  	    exit;
	  	}
	  	else
	  	{
			// $response["error"] = FALSE;
			// $response["id"] = $user["unique_id"];
			$response["message"] = "Gagal Login";
	        msg::set_message('error', app::getliblang('login_invalid'));
     		app::response("500", ["error"=>TRUE,"items"=>$response]);
	        // header("location: ". $app['webmin'] ."/auth.do&error=1");
	        // exit;
	  	}
	}elseif($act == "login_customer"){
	  	form::init();
	  	form::serialize_form();
	  	// form::validate('empty', 'p_username,p_password');
	  	if (form::is_error()){
				msg::set_message('error', app::getliblang('login_invalid'));
	    		form::set_error(1);
	    		header("location: ". $app['webmin'] ."/auth.do&error=1");
	    		exit;
	  	}
	  	if( db::anti_sql_injection($_POST['p_username']) and db::anti_sql_injection($_POST['p_password']))
	  	{
			$passwordhash = md5(serialize($p_password));
		    $sql = "SELECT * FROM " .$app['table']['customer'] ." WHERE username = '$p_username' AND password = '$passwordhash' AND `status` = 'active' LIMIT 1";

		    $user_login=db::get_record("customer","username = '$p_username' AND password = '$passwordhash' AND `status` = 'active'");
		    db::query($sql, $rs['login'], $nr['login']);
			if($nr['login']){
				$member =db::fetch($rs['login']);
	            form::reset();

				$config = db::get_record("configuration","id","1");
				$cek_device['id'] = db::lookup("count(id)","user_device","id_user='".$member['id']."' AND status='active' ");
				

				$response["error"] = FALSE;
				// $response["id"] = $user["unique_id"];
				$response["message"] = "Berhasil Login";
	        	// app::response("200", ["error"=>FALSE,"response"=>$response,"id_user"=>$user_login['id'],"rule"=>$login,"id_rule"=>$rule_login['id_rule'],"name"=>$user_login['name'],"session"=>db::fetch($rs['login'])]);
	        	app::response("200", ["error"=>0,"message"=>$response["message"],"data"=>["id_user"=>$user_login['id'],"name"=>$user_login['name'],"session"=>db::fetch($rs['login'])]]);

	            // header("location: ". $app['webmin'] ."/dashboard.do");
			}else{
/*	            msg::set_message('error', app::getliblang('login_invalid'));
	            msg::build_msg();*/
				$response["error"] = FALSE;
				// $response["id"] = $user["unique_id"];
				$response["message"] = "Username atau Password Salah";
	        	app::response("200", ["error"=>1,"message"=>$response["message"]]);
	            // header("location: ". $app['webmin'] ."/auth.do&error=1");
			}
	  	    exit;
	  	}
	  	else
	  	{
			// $response["error"] = FALSE;
			// $response["id"] = $user["unique_id"];
			$response["message"] = "Gagal Login";
	        msg::set_message('error', app::getliblang('login_invalid'));
     		app::response("500", ["error"=>TRUE,"items"=>$response]);
	        // header("location: ". $app['webmin'] ."/auth.do&error=1");
	        // exit;
	  	}
	}elseif($act == "login_customer_app"){
	  	form::init();
	  	form::serialize_form();
	  	// form::validate('empty', 'p_username,p_password');
	  	if (form::is_error()){
				msg::set_message('error', app::getliblang('login_invalid'));
	    		form::set_error(1);
	    		header("location: ". $app['webmin'] ."/auth.do&error=1");
	    		exit;
	  	}
	  	if( db::anti_sql_injection($_POST['p_username']) and db::anti_sql_injection($_POST['p_password']))
	  	{
			$passwordhash = md5(serialize($p_password));
		    $sql = "SELECT * FROM " .$app['table']['customer'] ." WHERE username = '$p_username' AND password = '$passwordhash' AND `status` = 'active' LIMIT 1";

		    $user_login=db::get_record("customer","username = '$p_username' AND password = '$passwordhash' AND `status` = 'active'");
		    db::query($sql, $rs['login'], $nr['login']);
			if($nr['login']){
				$member =db::fetch($rs['login']);
	            form::reset();
	            $p_category = "customer";
				$config = db::get_record("configuration","id","1");
				$cek_device['id'] = db::lookup("count(id)","user_device","id_user='".$member['id']."' AND status='active' ");
				
//////////////////// bisa di buka saat firebase bisa digunakan ////////////////

				if(!isset($p_version) && $config['app_version'] != $p_version){
					$response['error'] = 212;
/*					$callback['message'] = "Distributor Apps telah mengalami pembaruan.\r
					Silahkan hapus/uninstall aplikasi anda, kemudian download & install kembali pada link berikut:\r\n
					http://distributor.kalsi.co.id/distributor.apk
					\r\n
					Anda tetap bisa login dengan akun sebelumnya.";
					$callback['url'] = "http://distributor.kalsi.co.id/distributor.apk";*/
				}else{
					if(!isset($member['status']) || empty($member['status']) || $member['status']=="inactive"){
						$response['error'] = 1;
						$response['message'] = "Akun anda sedang dinonaktifkan";
					}else{
						if($cek_device['id'] > 0){
							if($p_isVirtual == "false"){
								$response['message'] = "Akun pada device lain telah di logout";
							}else{
								$response['message'] = "Login berhasil.";
							}
						}else{
							$response['message'] = "Login berhasil.";
						}
/*						$member['distributor_name'] = (is_null($member['distributor_name'])?"Owner":$member['distributor_name']);
						$member['address'] = (is_null($member['address'])?"":$member['distributor_name']);*/
						$data = $member;

	 					$device = db::get_record("device","uuid='$p_uuid' and category='$p_category' and serial='$p_serial'");
						if(!isset($device['id']) || empty($device['id'])){
							$id = rand(1, 100).rand(1, 100).date("dmYHis");
							$sql = "INSERT INTO ".$app['table']['device']." (id, category, uuid, serial, platform, model, manufacture, isVirtual, token, created_at) VALUES
							('$id', '$p_category', '$p_uuid', '$p_serial', '$p_platform', '$p_model', '$p_manufacturer', '$p_isVirtual', '$p_token', now())";
							db::qry($sql);
							$device['id'] = $id;
							$device['token'] = $p_token;
						}else{
							if($p_isVirtual == "false"){
								$sql = "UPDATE ".$app['table']['device']."
										SET token = '$p_token',
											updated_at = now()
										WHERE id = '".$device['id']."'";
								db::qry($sql);
							}
						}
					 	$user_device = db::get_record("user_device","id_user='".$member['id']."' and id_device='".$device['id']."'");
						if(!isset($user_device['id_user']) || empty($user_device['id_user'])){
 						$sql = "INSERT INTO ".$app['table']['user_device']." (id_user, id_device, status,app_version,category,created_at) VALUES ('".$data['id']."', '".$device['id']."', 'active','$p_version','$p_category',now())";
							db::qry($sql);
						}else{
							if($p_isVirtual == "false"){
								$sql = "UPDATE ".$app['table']['user_device']."
											SET notif = 'true', status='active', 
												app_version = '$p_version',
												updated_at = now()
										WHERE id_device = '".$device['id']."' and id_user='".$data['id']."'";
								db::qry($sql);
							}else{
								$sql = "UPDATE ".$app['table']['user_device']."
											SET notif = 'true', 
												status='active', 
												app_version = '$p_version',
												updated_at = now()
										WHERE id_device = '".$device['id']."' and id_user='".$data['id']."'";
								db::qry($sql);
							}
						}
						$device['notif'] = true;

						$data['notif'] = $device['notif'];
						if($p_isVirtual == "false"){
							$rs['devices'] = db::get_recordset("user_device","id_user='".$member['id']."' AND status='active' AND id_device!='".$device['id']."'");
							while($user_devices = db::fetch($rs['devices'])){
								$datapush = array(
									"to" => db::lookup("token","device","id",$user_devices['id_device']),
									"notification" => array(
										"title" => "Upaya login dari $p_model",
										"text" => "Akun barusaja digunakan untuk login pada perangkat $p_model",
										"sound" => "default"
									),
									'data' => array(
										'method' => 'logout_live',
										"title" => "Auto Logout",
										"body" => ""
									)
								);
								app::pushNotifClientSend($datapush);
							}
							$sql = "UPDATE ".$app['table']['user_device']."
										SET status='inactive',
											updated_at = now()
									WHERE id_user='".$member['id']."' AND id_device!='".$device['id']."'";
							db::qry($sql);
						}

					}
				}
/////////////////////////////////////////////////////////////////

				$response["error"] = FALSE;
				// $response["id"] = $user["unique_id"];
				// $response["message"] = "Berhasil Login";
	        	// app::response("200", ["error"=>FALSE,"response"=>$response,"id_user"=>$user_login['id'],"rule"=>$login,"id_rule"=>$rule_login['id_rule'],"name"=>$user_login['name'],"session"=>db::fetch($rs['login'])]);
	        	app::response("200", ["error"=>$response["error"],"message"=>$response["message"],"data"=>["id_user"=>$user_login['id'],"name"=>$user_login['name'],"address"=>$member['address'],"phone"=>$member['phone'],"created_at"=>$member['created_at'],"username"=>$member['username']]]);

	            // header("location: ". $app['webmin'] ."/dashboard.do");
			}else{
/*	            msg::set_message('error', app::getliblang('login_invalid'));
	            msg::build_msg();*/
				$response["error"] = FALSE;
				// $response["id"] = $user["unique_id"];
				$response["message"] = "Username atau Password Salah";
	        	app::response("200", ["error"=>500,"message"=>$response["message"]]);
	            // header("location: ". $app['webmin'] ."/auth.do&error=1");
			}
	  	    exit;
	  	}
	  	else
	  	{
			// $response["error"] = FALSE;
			// $response["id"] = $user["unique_id"];
			$response["message"] = "Gagal Login";
	        msg::set_message('error', app::getliblang('login_invalid'));
     		app::response("500", ["error"=>TRUE,"items"=>$response]);
	        // header("location: ". $app['webmin'] ."/auth.do&error=1");
	        // exit;
	  	}
	}elseif($act == "list_container"){
	  	form::init();
	  	form::serialize_form();
	  	// form::validate('empty', 'p_username,p_password');
	  	if (form::is_error()){
				msg::set_message('error', app::getliblang('login_invalid'));
	    		form::set_error(1);
	    		header("location: ". $app['webmin'] ."/auth.do&error=1");
	    		exit;
	  	}

			if (isset($p_search)) {
				#$q = "AND (num_pib LIKE '%".$p_search."%' OR status_beacukai LIKE '%".$p_search."%' OR tgl_nopen LIKE '%".$p_search."%' ) ";
				/////
				// if (date("Y-m-d",strtotime($p_search)) != 1970-01-01 && date("Y-m-d",strtotime($p_search)) != "1970-01-01") {
				// 	$q = "AND (a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($p_search))."%' ) ";
				// }else{
					$q = "AND (num_bl LIKE '%".$p_search."%') ";
				// }
			}

			if(isset($p_count) && !empty($p_count)){
				app::mq_encode('p_count');
				if(isset($p_start) && !empty($p_start)){
					app::mq_encode('p_start');
					$limit[]=$p_start;
				}
				$limit[]=$p_count;
			}

			$limit_final = "";
			if(count($limit)>0){
				$limit_final = "LIMIT ".implode(",",$limit);
			}
		    $sql = "SELECT * FROM " .$app['table']['import'] ." WHERE id_customer = '$p_customer_id' AND `status` = 'active' $q order by created_at desc";
		    db::query($sql, $rs2['data'], $nr2['data']);

		    $sql = "SELECT * FROM " .$app['table']['import'] ." WHERE id_customer = '$p_customer_id' AND `status` = 'active' $q order by created_at desc $limit_final";

		    #$user_login=db::get_record("customer","username = '$p_username' AND password = '$passwordhash' AND `status` = 'active'");
		    db::query($sql, $rs['login'], $nr['login']);
		    $user['length'] = $nr['login'];
			while ($row321	= db::fetch($rs['login'])) {
				$row321['alamat_cust'] = db::lookup("address","customer","id",$p_customer_id);
				$row321['eta'] =date("d-m-Y", strtotime($row321['eta']));
/*				if ($row321['status_kiriman']==0) {
					$row321['status_kiriman'] == "Belum";
				}else{
					$row321['status_kiriman'] == "Sudah";
				}*/
				$row321['status_kiriman'] = db::lookup_order("positions","status","id_import = '".$row321['id']."' AND lang = 'id'","created_at");
				$row321['created_at_formated'] = format_tgl_waktu($row321['created_at']);
				$user[] = $row321;	
			}		
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"total_item"=>$nr2['data'],"item"=>$user,"item_count"=>$nr['login']]]);
	  	    exit;
	  /*	}
	  	else
	  	{
			// $response["error"] = FALSE;
			// $response["id"] = $user["unique_id"];
			$response["message"] = "Gagal Login";
	        msg::set_message('error', app::getliblang('login_invalid'));
     		app::response("500", ["error"=>TRUE,"items"=>$response]);
	        // header("location: ". $app['webmin'] ."/auth.do&error=1");
	        // exit;
	  	}*/
	}elseif($act == "detail_tracking"){
	  	form::init();
	  	form::serialize_form();
	  	// form::validate('empty', 'p_username,p_password');
	  	if (form::is_error()){
				msg::set_message('error', app::getliblang('login_invalid'));
	    		form::set_error(1);
	    		header("location: ". $app['webmin'] ."/auth.do&error=1");
	    		exit;
	  	}
/*	  	if( db::anti_sql_injection($_POST['p_username']) and db::anti_sql_injection($_POST['p_password']))
	  	{*/
			// $passwordhash = md5(serialize($p_password));

/*			$req_dump = print_r($_REQUEST, TRUE);
			$myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
			fwrite($myfile,$req_dump);
			fclose($myfile);*/

		    $sql = "SELECT * FROM " .$app['table']['container'] ." WHERE id_import = '$id_imp' order by created_at desc";
		    #$user_login=db::get_record("customer","username = '$p_username' AND password = '$passwordhash' AND `status` = 'active'");
		    db::query($sql, $rs2['login'], $nr2['login']);
			while ($row321	= db::fetch($rs2['login'])) {
				$user2[] = $row321;	
			}
		    $sql = "SELECT * FROM " .$app['table']['status'] ." WHERE id_import = '$id_imp' AND lang = '".$lang."' ORDER BY created_at DESC ";
		    #$user_login=db::get_record("customer","username = '$p_username' AND password = '$passwordhash' AND `status` = 'active'");
		    db::query($sql, $rs3['login'], $nr3['login']);
		    $user3['length']=$nr3['login'];
			while ($row321	= db::fetch($rs3['login'])) {
				$row321['alamat_cust'] = db::lookup("address","customer","id",$p_customer_id);
				// $row321['created_at_bahasa']  = format_tgl_bahasa($row321['created_at'],$lang);
				$row321['created_at_bahasa']  = format_tgl_bahasa(date("Y-m-d", strtotime($row321['created_at'])),$lang);
				$row321['created_at_formated'] = format_tgl_waktu($row321['created_at']);
				$user3[] = $row321;	
			}

/*			if ($lang == "id") {
			    $lang = "in";
			}else{
			    $lang = $lang;
			}*/
			$bahasanya = db::lookup('id', 'lang', 'alias',$lang);

		    $sql = "SELECT * FROM " .$app['table']['import'] ." WHERE id = '$id_imp'";

		    #$user_login=db::get_record("customer","username = '$p_username' AND password = '$passwordhash' AND `status` = 'active'");
		    db::query($sql, $rs['login'], $nr['login']);

			while ($row321	= db::fetch($rs['login'])) {
				$row321['alamat_cust'] = db::lookup("address","customer","id",$row321['id_customer']);
				// $row321['status_cust'] = "";
				$row321['eta'] =date("d-m-Y", strtotime($row321['eta']));
				if ($row321['send_status'] == "belum") {
					$row321['status_cust'] = app::i18n(['belum_cust',$bahasanya]);
				}elseif ($row321['send_status'] == "sebagian") {
					$row321['status_cust'] = app::i18n(['sebagian_cust',$bahasanya]);
				}elseif ($row321['send_status'] == "selesai") {
					$row321['status_cust'] = app::i18n(['selesai_cust',$bahasanya]);
				}
				$user[] = $row321;	
			}
			app::response("200", ["error"=>0,"data"=>["message"=>$response["message"],"item"=>$user,"item_length"=>$nr['login'],"item2"=>$user2,"item2_length"=>$nr2['login'],"item3"=>$user3,"item_length3"=>$nr3['login']]]);
	  	    exit;
	  /*	}
	  	else
	  	{
			// $response["error"] = FALSE;
			// $response["id"] = $user["unique_id"];
			$response["message"] = "Gagal Login";
	        msg::set_message('error', app::getliblang('login_invalid'));
     		app::response("500", ["error"=>TRUE,"items"=>$response]);
	        // header("location: ". $app['webmin'] ."/auth.do&error=1");
	        // exit;
	  	}*/
	}elseif($act == "logout"){
		// if (isset($act) && $act=="logout"):
			// if(!empty($_SESSION[$app['session']])):
/*				$_SESSION[$app['session']] = array();
				unset($_SESSION[$app['session']]);
				msg::set_message('success', app::getliblang('logout'));
				msg::build_msg(1);*/

//////////////////// bisa di pakai jika firebase sdh bisa ////////////////
				$id_device = db::lookup("id","device","uuid='$p_uuid' and category='$p_category' and serial='$p_serial'");
				$sql = "UPDATE ".$app['table']['user_device']."
							SET status='inactive',
								updated_at = now()
						WHERE id_device='".$id_device."'";
				db::qry($sql);
///////////////////////////////////////////////////////////////////
     		app::response("200", ["error"=>0,"data"=>["message"=>"Berhasil Logout"]]);
exit;
			// endif;
		    // header("location: ". $app['webmin']);
			// exit;
		// endif;
	}elseif($act == "error_pelabuhan_out_con"){
     		app::response("200", ["error"=>TRUE,"data"=>["message"=>"Belum Mendapat SPPB"]]);

	}


	if($act=="add-file"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'p_photo,id_imp');
		if(form::is_error()):
			$callback['error'] = count($_SESSION['error_flag']);
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;
		app::mq_encode('p_photo');
		if($callback['error']<=0):
			$pic = explode(',', $p_photo);
			if(isset($pic[1]) && !empty($pic[1])){
				if ($p_category == "do") {
					$p_photo = "photo_" . strtoupper(md5(uniqid(rand(), true))) . ".jpg";
					api::base64_to_image($pic[1],$app['doc_path']."/do/".$p_photo);
					$id = rand(1, 100).rand(1, 100).date("dmYHis");
					$sql = "INSERT INTO ".$app['table']['file_do']." (id, id_import,name, created_at) VALUES ('$id', '$id_imp','$p_photo', now()) ";
					db::qry($sql);
					$data['photo_name'] = $p_photo;
					$data['photo'] = api::urlApiFile_doc_path("do",$p_photo);

				}elseif ($p_category == "bea") {
					$p_photo = "photo_" . strtoupper(md5(uniqid(rand(), true))) . ".jpg";
					api::base64_to_image($pic[1],$app['doc_path']."/sppb/".$p_photo);
					$id = rand(1, 100).rand(1, 100).date("dmYHis");
					$sql = "INSERT INTO ".$app['table']['file_bea']." (id, id_import, name, created_at) VALUES ('$id', '$id_imp', '$p_photo', now()) ";
					db::qry($sql);
					$data['photo_name'] = $p_photo;
					$data['photo'] = api::urlApiFile_doc_path("sppb",$p_photo);

				}elseif ($p_category == "eir") {
					$p_photo = "photo_" . strtoupper(md5(uniqid(rand(), true))) . ".jpg";
					api::base64_to_image($pic[1],$app['doc_path']."/sppb/".$p_photo);
					$id = rand(1, 100).rand(1, 100).date("dmYHis");
					$sql = "INSERT INTO ".$app['table']['file_eir']." (id, id_import, name, created_at) VALUES ('$id', '$id_imp', '$p_photo', now()) ";
					db::qry($sql);
					$data['photo_name'] = $p_photo;
					$data['photo'] = api::urlApiFile_doc_path("sppb",$p_photo);

				}elseif ($p_category == "kel") {
					$p_photo = "photo_" . strtoupper(md5(uniqid(rand(), true))) . ".jpg";
					api::base64_to_image($pic[1],$app['doc_path']."/out_cont/".$p_photo);
					$id = rand(1, 100).rand(1, 100).date("dmYHis");
					$sql = "INSERT INTO ".$app['table']['file_kel']." (id, id_import, name, created_at) VALUES ('$id', '$id_imp', '$p_photo', now()) ";
					db::qry($sql);
					$data['photo_name'] = $p_photo;
					$data['photo'] = api::urlApiFile_doc_path("out_cont",$p_photo);

				}else{
					$callback['error'] = '1';
					$callback['message']=="Category Photo Salah";
				}
			}
		endif;
		if(isset($data)){
			$callback['data']=$data;
			$callback['message'] = "File Telah Terunggah.";
		}
		echo json_encode($callback);
		exit;
	}

	if($act=="add-file-confirm"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'p_photo,id_imp');
		if(form::is_error()):
			$callback['error'] = count($_SESSION['error_flag']);
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;
		app::mq_encode('p_photo');
		if($callback['error']<=0):
			$pic = explode(',', $p_photo);
			if(isset($pic[1]) && !empty($pic[1])){
				if ($p_category == "do") {
					$p_photo = "photo_" . strtoupper(md5(uniqid(rand(), true))) . ".jpg";
					api::base64_to_image($pic[1],$app['doc_path']."/do/".$p_photo);
					$id = rand(1, 100).rand(1, 100).date("dmYHis");
					$sql = "INSERT INTO ".$app['table']['file_do']." (id, id_import,name, created_at) VALUES ('$id', '$id_imp','$p_photo', now()) ";
					db::qry($sql);

					$sql   = "SELECT id,id_import,name,created_at FROM ".$app['table']['file_do']." WHERE id_import='$id_imp'";
					db::query($sql, $rs['row'], $nr['row']);
					$file['length']=$nr['row'];
					while($row = db::fetch($rs['row']))
					{
						$row['url_photo'] = api::urlApiFile_doc_path("do",$row['name']);
						$file[]=$row;
					}
					$data['photo_name'] = $p_photo;
					$data['id'] = $id;
					$data['photo'] = api::urlApiFile_doc_path("do",$p_photo);

				}elseif ($p_category == "bea") {
					$p_photo = "photo_" . strtoupper(md5(uniqid(rand(), true))) . ".jpg";
					api::base64_to_image($pic[1],$app['doc_path']."/sppb/".$p_photo);
					$id = rand(1, 100).rand(1, 100).date("dmYHis");
					$sql = "INSERT INTO ".$app['table']['file_bea']." (id, id_import, name, created_at) VALUES ('$id', '$id_imp', '$p_photo', now()) ";
					db::qry($sql);

					$sql   = "SELECT id,id_import,name,created_at FROM ".$app['table']['file_bea']." WHERE id_import='$id_imp'";
					db::query($sql, $rs['row'], $nr['row']);
					$file['length']=$nr['row'];
					while($row = db::fetch($rs['row']))
					{
						$row['url_photo'] = api::urlApiFile_doc_path("sppb",$row['name']);
						$file[]=$row;
					}
					$data['photo_name'] = $p_photo;
					$data['id'] = $id;
					$data['photo'] = api::urlApiFile_doc_path("sppb",$p_photo);

				}elseif ($p_category == "eir") {
					$p_photo = "photo_" . strtoupper(md5(uniqid(rand(), true))) . ".jpg";
					api::base64_to_image($pic[1],$app['doc_path']."/sppb/".$p_photo);
					$id = rand(1, 100).rand(1, 100).date("dmYHis");
					$sql = "INSERT INTO ".$app['table']['file_eir']." (id, id_import, name, created_at) VALUES ('$id', '$id_imp', '$p_photo', now()) ";
					db::qry($sql);

					$sql   = "SELECT id,id_import,name,created_at FROM ".$app['table']['file_eir']." WHERE id_import='$id_imp'";
					db::query($sql, $rs['row'], $nr['row']);
					$file['length']=$nr['row'];
					while($row = db::fetch($rs['row']))
					{
						$row['url_photo'] = api::urlApiFile_doc_path("sppb",$row['name']);
						$file[]=$row;
					}
					$data['photo_name'] = $p_photo;
					$data['id'] = $id;
					$data['photo'] = api::urlApiFile_doc_path("sppb",$p_photo);

				}elseif ($p_category == "kel") {
					$p_photo = "photo_" . strtoupper(md5(uniqid(rand(), true))) . ".jpg";
					api::base64_to_image($pic[1],$app['doc_path']."/out_cont/".$p_photo);
					$id = rand(1, 100).rand(1, 100).date("dmYHis");
					$sql = "INSERT INTO ".$app['table']['file_kel']." (id, id_import, name, created_at) VALUES ('$id', '$id_imp', '$p_photo', now()) ";
					db::qry($sql);

					$sql   = "SELECT id,id_import,name,created_at FROM ".$app['table']['file_kel']." WHERE id_import='$id_imp'";
					db::query($sql, $rs['row'], $nr['row']);
					$file['length']=$nr['row'];
					while($row = db::fetch($rs['row']))
					{
						$row['url_photo'] = api::urlApiFile_doc_path("out_cont",$row['name']);
						$file[]=$row;
					}
					$data['photo_name'] = $p_photo;
					$data['id'] = $id;
					$data['photo'] = api::urlApiFile_doc_path("out_cont",$p_photo);

				}else{
					$callback['error'] = '1';
					$callback['message']=="Category Photo Salah";
				}
			}
		endif;
		if(isset($data)){
			$callback['data']=$data;
			$callback['file']=$file;
			$callback['message'] = "File Berhasil Terupload.";
		}
		echo json_encode($callback);
		exit;
	}


	if($act=="del-photo"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'p_photo');
		if(form::is_error()):
			$callback['error'] = count($_SESSION['error_flag']);
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;

		app::mq_encode('p_photo');
		if($callback['error']<=0):
			if ($p_category == "do") {
				$arr_photo = explode(",",$p_photo);
				foreach($arr_photo as $p_photo){
					$sql = "DELETE FROM ".$app['table']['file_do']." WHERE name='$p_photo' ";
					db::qry($sql);
					@unlink($app['doc_path']."/do/".$p_photo);
				}
			}elseif ($p_category == "bea") {
				$arr_photo = explode(",",$p_photo);
				foreach($arr_photo as $p_photo){
					$sql = "DELETE FROM ".$app['table']['file_bea']." WHERE name='$p_photo' ";
					db::qry($sql);
					@unlink($app['doc_path']."/sppb/".$p_photo);
				}
			}elseif ($p_category == "eir") {
				$arr_photo = explode(",",$p_photo);
				foreach($arr_photo as $p_photo){
					$sql = "DELETE FROM ".$app['table']['file_eir']." WHERE name='$p_photo' ";
					db::qry($sql);
					@unlink($app['doc_path']."/sppb/".$p_photo);
				}
			}elseif ($p_category == "kel") {
				$arr_photo = explode(",",$p_photo);
				foreach($arr_photo as $p_photo){
					$sql = "DELETE FROM ".$app['table']['file_kel']." WHERE name='$p_photo' ";
					db::qry($sql);
					@unlink($app['doc_path']."/out_cont/".$p_photo);
				}
			}
		endif;
		$callback['message'] = "File Telah Terhapus.";
		echo json_encode($callback);
		exit;
	}


	if($act=="del-photo-confirm"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'p_photo');
		if(form::is_error()):
			$callback['error'] = count($_SESSION['error_flag']);
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;

		app::mq_encode('p_photo');
		if($callback['error']<=0):
			if ($p_category == "do") {
				$arr_photo = explode(",",$p_photo);
				foreach($arr_photo as $p_photo){
					if (!preg_match("/\b".$p_photo."\b/i",$p_image_before)) {
						$id=db::lookup("id","file_do","id",$p_photo);
						$sql = "DELETE FROM ".$app['table']['file_do']." WHERE id='$p_photo' ";
						db::qry($sql);
						@unlink($app['doc_path']."/do/".$p_photo);
						$data .= $id.",";
					}
				}
				$data = rtrim($data,",");
				$data = explode(",", $data);
			}elseif ($p_category == "bea") {
				$arr_photo = explode(",",$p_photo);
				foreach($arr_photo as $p_photo){
					if (!preg_match("/\b".$p_photo."\b/i",$p_image_before)) {
						$id=db::lookup("id","file_bea","id",$p_photo);
						$sql = "DELETE FROM ".$app['table']['file_bea']." WHERE id='$p_photo' ";
						db::qry($sql);
						@unlink($app['doc_path']."/sppb/".$p_photo);
						$data .= $id.",";
					}
				}
				$data = rtrim($data,",");
				$data = explode(",", $data);
			}elseif ($p_category == "eir") {
				$arr_photo = explode(",",$p_photo);
				foreach($arr_photo as $p_photo){
					if (!preg_match("/\b".$p_photo."\b/i",$p_image_before)) {
						$id=db::lookup("id","file_eir","id",$p_photo);
						$sql = "DELETE FROM ".$app['table']['file_eir']." WHERE id='$p_photo' ";
						db::qry($sql);
						@unlink($app['doc_path']."/sppb/".$p_photo);
						$data .= $id.",";
					}
				}
				$data = rtrim($data,",");
				$data = explode(",", $data);
			}elseif ($p_category == "kel") {
				$arr_photo = explode(",",$p_photo);
				foreach($arr_photo as $p_photo){
					if (!preg_match("/\b".$p_photo."\b/i",$p_image_before)) {
						$id=db::lookup("id","file_kel","id",$p_photo);
						$sql = "DELETE FROM ".$app['table']['file_kel']." WHERE id='$p_photo' ";
						db::qry($sql);
						@unlink($app['doc_path']."/out_cont/".$p_photo);
						$data .= $id.",";
					}
				}
				$data = rtrim($data,",");
				$data = explode(",", $data);
			}
		endif;
		if (count($data)>0) {
			// $callback['data'] = $data;
		}
		$callback['message'] = "File Telah Terhapus.";
		echo json_encode($callback);
		exit;
	}



	if($act=="view-file"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'id_imp');
		if(form::is_error()):
			$callback['error'] = count($_SESSION['error_flag']);
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;
		// app::mq_encode('p_photo');
		if($callback['error']<=0):
				$hitung =0;
				if ($p_category == "do") {
					$sql   = "SELECT id,id_import,name,created_at FROM ".$app['table']['file_do']." WHERE id_import='$id_imp' ORDER BY created_at DESC ";
					db::query($sql, $rs['row'], $nr['row']);
					$data['length']=$nr['row'];
					while($row = db::fetch($rs['row']))
					{
						$row['url_photo'] = api::urlApiFile_doc_path("do",$row['name']);
						$data[]=$row;
						if ($hitung>0) {
							$data_lama.=$row['id'].",";
						}
						$hitung++;
					}
				}elseif ($p_category == "bea") {
					$sql   = "SELECT id,id_import,name,created_at FROM ".$app['table']['file_bea']." WHERE id_import='$id_imp' ORDER BY created_at DESC";
					db::query($sql, $rs['row'], $nr['row']);
					$data['length']=$nr['row'];
					while($row = db::fetch($rs['row']))
					{
						$row['url_photo'] = api::urlApiFile_doc_path("sppb",$row['name']);
						$data[]=$row;
						if ($hitung>0) {
							$data_lama.=$row['id'].",";
						}
						$hitung++;
					}
				}elseif ($p_category == "eir") {
					$sql   = "SELECT id,id_import,name,created_at FROM ".$app['table']['file_eir']." WHERE id_import='$id_imp' ORDER BY created_at DESC";
					db::query($sql, $rs['row'], $nr['row']);
					$data['length']=$nr['row'];
					while($row = db::fetch($rs['row']))
					{
						$row['url_photo'] = api::urlApiFile_doc_path("sppb",$row['name']);
						$data[]=$row;
						if ($hitung>0) {
							$data_lama.=$row['id'].",";
						}
						$hitung++;
					}
	
				}elseif ($p_category == "kel") {
					$sql   = "SELECT id,id_import,name,created_at FROM ".$app['table']['file_kel']." WHERE id_import='$id_imp' ORDER BY created_at DESC";
					db::query($sql, $rs['row'], $nr['row']);
					$data['length']=$nr['row'];
					while($row = db::fetch($rs['row']))
					{
						$row['url_photo'] = api::urlApiFile_doc_path("out_cont",$row['name']);
						$data[]=$row;
						if ($hitung>0) {
							$data_lama.=$row['id'].",";
						}
						$hitung++;
					}
				}else{
					$callback['error'] = '1';
					$callback['message']=="Category Photo Salah";
				}
		endif;
		if(isset($data)){
			$callback['data']=$data;
			$data_lama = rtrim($data_lama,",");	
			$callback['p_image_before']=$data_lama;
			$callback['message'] = "File Telah Termuat.";
		}
		echo json_encode($callback);
		exit;
	}


	if($act=="form-do"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'id_imp');
		if(form::is_error()):
			$callback['error'] = count($_SESSION['error_flag']);
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;
		app::mq_encode('p_photo');
		if($callback['error']<=0):

  		 	$sql   = "SELECT DISTINCT date_receipt_do,expired_date,revisi,created_at,id FROM ".$app['table']['delivery_order']." where id_import = '$id_imp' LIMIT 1 ";
			db::query($sql, $rs['row'], $nr['row']);
			$data['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				$row['date_receipt_do_format'] = format_tgl($row['date_receipt_do']);
				$row['expired_date_format'] 	= format_tgl($row['expired_date']);
				$data[]=$row;
			}

  		 	$sql   = "SELECT DISTINCT name,id,created_at FROM ".$app['table']['file_do']." where id_import = '$id_imp' ORDER BY created_at DESC ";
			db::query($sql, $rs2['row'], $nr2['row']);
			$file['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['url_photo'] = api::urlApiFile_doc_path("do",$row2['name']);
				$file[]=$row2;
			}
		endif;
		if(isset($data)){
			$callback['data']=$data;
			$callback['num_pib']=db::lookup("num_pib","import","id",$id_imp);
			$callback['file']=$file;
			$callback['message'] = "File Telah Termuat.";
		}
		echo json_encode($callback);
		exit;
	}

	if($act=="form-eir"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'id_imp');
		if(form::is_error()):
			$callback['error'] = count($_SESSION['error_flag']);
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;
		app::mq_encode('p_photo');
		if($callback['error']<=0):

  		 	$sql   = "SELECT DISTINCT id,revisi,status_eir FROM ".$app['table']['pelabuhan']." where id_import = '$id_imp' LIMIT 1 ";
			db::query($sql, $rs['row'], $nr['row']);
			$data['length']=$nr['row'];
		    if($nr['row'] == 0){
			    $data[0]["status_eir"] =0;
		    }
			while($row = db::fetch($rs['row']))
			{
				$data[]=$row;
			}

  		 	$sql   = "SELECT DISTINCT name,id,created_at FROM ".$app['table']['file_eir']." where id_import = '$id_imp' ORDER BY created_at DESC ";
			db::query($sql, $rs2['row'], $nr2['row']);
			$file['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['url_photo'] = api::urlApiFile_doc_path("sppb",$row2['name']);
				$file[]=$row2;
			}
		endif;
		if(isset($data)){
			$callback['data']=$data;
			$callback['num_pib']=db::lookup("num_pib","import","id",$id_imp);
			$callback['file']=$file;
			$callback['message'] = "File Telah Termuat.";
		}
		echo json_encode($callback);
		exit;
	}

	if($act=="form-kel"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'id_imp');
		if(form::is_error()):
			$callback['error'] = count($_SESSION['error_flag']);
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;
		app::mq_encode('p_photo');
		if($callback['error']<=0):

  		 	$sql   = "SELECT DISTINCT id,status_eir FROM ".$app['table']['pelabuhan']." where id_import = '$id_imp' LIMIT 1 ";
			db::query($sql, $rs['row'], $nr['row']);
			$data['length']=$nr['row'];
			while($row = db::fetch($rs['row']))
			{
				$data[]=$row;
			}

  		 	$sql   = "SELECT DISTINCT name,id,created_at FROM ".$app['table']['file_kel']." where id_import = '$id_imp' ORDER BY created_at DESC ";
			db::query($sql, $rs2['row'], $nr2['row']);
			$file['length']=$nr2['row'];
			while($row2 = db::fetch($rs2['row']))
			{
				$row2['url_photo'] = api::urlApiFile_doc_path("sppb",$row2['name']);
				$file[]=$row2;
			}
  		 	$sql   = "SELECT DISTINCT id,num_container,seal FROM ".$app['table']['container']." where id_import = '$id_imp' ORDER BY created_at DESC ";
			db::query($sql, $rs2['row'], $nr2['row']);
			$container['length']=$nr2['row'];
			while($row3 = db::fetch($rs2['row']))
			{
				// $row2['url_photo'] = api::urlApiFile_doc_path("sppb",$row2['name']);
				$container[]=$row3;
			}
		endif;
		if(isset($data)){
			$callback['data']=$data;
			$callback['num_pib']=db::lookup("num_pib","import","id",$id_imp);
			$callback['sta_bea']=db::lookup("status_beacukai","import","id",$id_imp);
			$callback['file']=$file;
			$callback['container']=$container;
			$callback['message'] = "File Telah Termuat.";
		}
		echo json_encode($callback);
		exit;
	}
	if($act=="register-device"){
		form::init();
		$callback = array("error"=>0, "message"=>array(), "data"=>"");

		form::validate(null, 'p_category,p_uuid,p_serial,p_token');
		app::mq_encode('p_category,p_uuid,p_serial,p_token');
		if (form::is_error()):
			$callback['error'] = count($_SESSION['error_msg']);
			$callback['message'] = $_SESSION['error_msg'];
		endif;
		if($callback['error']<=0){
			$device = db::get_record("device","uuid='$p_uuid' and category='$p_category' and serial='$p_serial'");
			if(!isset($device['id']) || empty($device['id'])){
				$id = rand(1, 100).rand(1, 100).date("dmYHis");
				$sql = "INSERT INTO ".$app['table']['device']." (id, category, uuid, serial, platform, model, manufacturer, isVirtual, token, post_date) VALUES
				('$id', '$p_category', '$p_uuid', '$p_serial', '$p_platform', '$p_model', '$p_manufacturer', '$p_isVirtual', '$p_token', now())";
				db::qry($sql);
			}else{
				if($device['token']!=$p_token){
					$sql = "update ".$app['table']['device']."
							set token = '$p_token',
								modify_date = now()
							where id = '".$device['id']."'";
					db::qry($sql);
				}
			}
			$callback['message'] = "Registrasi device berhasil.";
		}
		if(isset($data)){
			$callback['data']=$data;
		}
		echo json_encode($callback);
		exit;
	}

	if($act=="register-device-customer"){
		form::init();
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate(null, 'p_category,p_uuid,p_serial,p_token');
		app::mq_encode('p_category,p_uuid,p_serial,p_token');
		if (form::is_error()):
			$callback['error'] = count($_SESSION['error_msg']);
			$callback['message'] = $_SESSION['error_msg'];
		endif;
		if($callback['error']<=0){
			$device = db::get_record("device","uuid='$p_uuid' and category='$p_category' and serial='$p_serial'");
			if(!isset($device['id']) || empty($device['id'])){
				$id = rand(1, 100).rand(1, 100).date("dmYHis");
				$sql = "INSERT INTO ".$app['table']['device']." (id, category, uuid, serial, platform, model, manufacturer, isVirtual, token, post_date) VALUES
				('$id', '$p_category', '$p_uuid', '$p_serial', '$p_platform', '$p_model', '$p_manufacturer', '$p_isVirtual', '$p_token', now())";
				db::qry($sql);
			}else{
				if($device['token']!=$p_token){
					$sql = "update ".$app['table']['device']."
							set token = '$p_token',
								modify_date = now()
							where id = '".$device['id']."'";
					db::qry($sql);
				}
			}
			$callback['message'] = "Registrasi device berhasil.";
		}
		if(isset($data)){
			$callback['data']=$data;
		}
		echo json_encode($callback);
		exit;
	}
	/*
		CHECK DEVICE CUSTOMER
	*/
	if($act=="checkdevice_customer"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");

		$device = db::get_record("device","uuid='$p_uuid' AND category='$p_category' AND serial='$p_serial'");

		$user_device = db::get_record("user_device","id_user='".$p_id_user."' AND id_device='".$device['id']."'");

		if($user_device['status'] == "inactive"){
			$datapush = array(
				"to" => $device['token'],
				"notification" => array(
					"title" => "Logout",
					"text" => "Akun sudah digunakan untuk login pada perangkat lain",
					"sound" => "default"
				),
				'data' => array(
					'method' => 'logout_live',
					"title" => "Auto Logout",
					"body" => ""
				)
			);
			app::pushNotifSend($datapush);
		}
		echo json_encode($callback);
		exit;
	}
	/*
		CHECK DEVICE INTERN
	*/
	if($act=="checkdevice_intern"){
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");

		$device = db::get_record("device","uuid='$p_uuid' AND category='$p_category' AND serial='$p_serial'");

		$user_device = db::get_record("user_device","id_user='".$p_id_user."' AND id_device='".$device['id']."'");

		if($user_device['status'] == "inactive"){
			$datapush = array(
				"to" => $device['token'],
				"notification" => array(
					"title" => "Logout",
					"text" => "Akun sudah digunakan untuk login pada perangkat lain",
					"sound" => "default"
				),
				'data' => array(
					'method' => 'logout',
					"title" => "Auto Logout",
					"body" => ""
				)
			);
			app::pushNotifSend($datapush);
		}
		echo json_encode($callback);
		exit;
	}
	if ($act=="simpan_seal") {
		form::init();
		$_SESSION['error_validator'] = null;
		$callback = array("error"=>0, "message"=>array(), "data"=>"");
		form::validate('empty', 'id_imp');
		if(form::is_error()):
			// $callback['error'] = count($_SESSION['error_flag']);
			$callback['error'] = 1;
			$message = "";foreach($_SESSION['error_validator'] AS $val){$message .= $val."\r\n";}$callback['message'] = $message;
		endif;
		app::mq_encode('p_keterangan');
		if($callback['error']<=0){
			// $id = rand(1, 100).date("dmYHis");
			$form_imp = db::get_record("import","id",$id_imp);
			$no=0;
			foreach ($_REQUEST as $key => $value) {
/*				if ($key == "id_cont".$no) {
					$campur[$value]= $_REQUEST['keterangan'.$no];
					$no++;
				}*/
				if (stripos($key, "id_cont") !== FALSE) {;
					preg_match_all('/([0-9]+|[a-zA-Z]+)/',$key,$sama);
					$campur[$value]= $_REQUEST['keterangan'.$sama[0][2]];
				}
			}
			$no2=0;
			foreach ($campur as $key2 => $value2) {
				// if (!empty($value2)) {
					$sql="update ". $app['table']['container'] ."
							set seal 	= '$value2'
								where id ='$key2' ";			
					// $sql ="insert into ".$app['table']['seal']."
					// 	(id, id_import, id_container, created_at,keterangan) values 
					// 	('$id', '$id_imp', '$id_con', now(),'$p_keterangan')";
					db::qry($sql);
				// }
				$no2++;
			}
			$ids2 = rand(1, 100).date("dmYHis");
			
			$cek_pelabuhan = db::lookup("1","pelabuhan","id_import",$id_imp);
			if ($cek_pelabuhan != "1" && $cek_pelabuhan != 1) {
				db::qry("INSERT INTO ".$app['table']['pelabuhan']."
				(id, id_import, created_by, created_at) values
				('$ids2', '$id_imp','". $me ."', now())");
			}

			if ($form_imp['status_sppb'] == '1') {
				$sql = "update ". $app['table']['pelabuhan'] ." set
						updated_by 		= '$me',
						updated_at 		= now()
						where id_import = '$id_imp'";
				db::qry($sql);
				// db::qry("UPDATE ". $app['table']['import']." set status_kiriman ='1' where id = '".$row['id_imp']."' ");
				// if(!empty($p_expired_do)){
				$ids = rand(1, 100).date("dmYHis");
				$id_notif = rand(1, 100).date("dmYHis");
				$id_notif2 = rand(1, 100).date("dmYHis");

				$sqls32 = "insert into ".$app['table']['notif']." 
							(id,id_rule,id_import,status,pesan,created_by,created_at) values
							('$id_notif','3220082018161340', '$id_imp', 1, 'Kiriman dengan pib : ".$form_imp['num_pib'].", bisa dijadwalkan','$me',now())";
				db::qry($sqls32);
				// $sqls32123 = "insert into ".$app['table']['notif']." 
				// 			(id,id_rule,id_import,status,pesan,created_by,created_at) values
				// 			('$id_notif2','5703092018105342','$id_imp',1,'Kiriman dengan no pib : ".$form_imp['num_pib']." Dokumennya telah di ubah, Bisa di Ubah Statusnya','$me',now())";
				// db::qry($sqls32123);
				
				// $path_kelengkapan = $app['doc_path']."/out_cont/".$kelengkapan;

				// $message  .= "<p>Dokumen Kelengkapan Container dengan no pib : ".$form_imp['num_pib']."  </p>";
				// // $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
				// $message .= "<p>Terima kasih</p>";
				// $sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
				// 			   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
				// 			   where a.id_rule ='5703092018105342' ";
				// db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
				// while($row = db::fetch($rs['sql_bea_cukai'])){
				// 	// app::sendmail($row['email'], "Container diantarkan", $message,$path_kelengkapan);
				// 	app::sendmail($row['email'], "Container diantarkan", $message,"");
				// }
			}
			$callback['message'] = "Data berhasil tersimpan.";
		}else{
			$callback['message'] = "Gagal menyimpan Data.";
		}
/*		if(isset($data)){
			$callback['data']=$data;
			$callback['num_pib']=db::lookup("num_pib","import","id",$id_imp);
			$callback['file']=$file;
			$callback['container']=$container;
			$callback['message'] = "File Telah Termuat.";
		}*/
		echo json_encode($callback);
		exit;
	}
?>	