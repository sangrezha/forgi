<?php
## TABLES ##
$prefix = "cmwi_";

$app['table_prefix'] 					= $prefix;
$app['table']['member'] 				= $prefix."member"; 
$app['table']['title'] 		    		= $prefix."title"; 
$app['table']['section'] 				= $prefix."section"; 
$app['table']['departement'] 			= $prefix."departement"; 
$app['table']['cost_centre'] 			= $prefix."cost_centre"; 
$app['table']['kategori'] 	    		= $prefix."kategori"; 
$app['table']['sistem_informasi'] 		= $prefix."sistem_informasi"; 
$app['table']['form_cpassword']     	= $prefix."form_cpassword"; 
$app['table']['form_acc_folder']    	= $prefix."form_acc_folder"; 
$app['table']['form_tmp_access']    	= $prefix."form_tmp_access"; 
$app['table']['form_perubahan_si']  	= $prefix."form_perubahan_si"; 
$app['table']['form_abnormal_si']   	= $prefix."form_abnormal_si"; 
$app['table']['form_get_soft']      	= $prefix."form_get_soft"; 
$app['table']['form_recovery_data'] 	= $prefix.'form_recovery_data';
$app['table']['data_form_software']     = $prefix.'data_form_software';
$app['table']['data_form_hardware']     = $prefix.'data_form_hardware';
$app['table']['form_hardware_software'] = $prefix.'form_hardware_software';
$app['table']['form_pengajuan_user']    = $prefix.'form_pengajuan_user';
$app['table']['form_register_device']        = $prefix."form_register_device";
$app['table']['data_form_register_device']   = $prefix."data_form_register_device";


$app['table']['template_dokumen']   = $prefix."template_dokumen";
$app['table']['log_document']       = $prefix.'log_document';
$app['table']['module_file']        = $prefix.'module_file';
$app['table']['notif']              = $prefix.'notif';
$app['table']['feedback_recov']     = $prefix.'feedback_recov';
$app['table']['data_soft']          = $prefix.'data_soft';
$app['table']['folder_user']        = $prefix."folder_user"; 
$app['table']['log']                = $prefix."log"; 
$app['table']['user'] 				= $prefix."user"; 
$app['table']['user_det'] 			= $prefix."user_detail"; 
$app['table']['lang'] 				= $prefix."lang";
$app['table']['i18n'] 				= $prefix."i18n";
// $app['table']['i18n'] 				= $prefix."library_lang";
$app['table']['configuration'] 		= $prefix."configuration";
$app['table']['rule'] 				= $prefix."rule"; 
$app['table']['rule_det'] 			= $prefix."rule_detail";
// $app['view']['rule'] 				= "view_rule"; 
/* Enter ini jangan dihapus untuk instalasi modul */
$app['table']['hari_ini'] 			= $prefix."hari_ini";
$app['table']['seal'] 				= $prefix."seal";
$app['table']['armada'] 			= $prefix."armada";
$app['table']['coba'] 				= $prefix."coba";
$app['table']['beacukai'] 			= $prefix."beacukai";
$app['table']['pelabuhan'] 			= $prefix."pelabuhan";
$app['table']['company'] 			= $prefix."company";
$app['table']['container'] 			= $prefix."container";
$app['table']['customer'] 			= $prefix."customer";
$app['table']['delivery_order']		= $prefix."delivery_order";
$app['table']['expired_do']			= $prefix."expired_do";
$app['table']['import'] 			= $prefix."import";
$app['table']['ship']	 			= $prefix."ship";
$app['table']['port']	 			= $prefix."port";
$app['table']['status']	 			= $prefix."status";
$app['table']['trucking']	 		= $prefix."trucking";
$app['table']['truck']		 		= $prefix."truck";
$app['table']['notif']		 		= $prefix."notif";
$app['table']['return_container']	= $prefix."return_container";
$app['table']['user_device']		= $prefix."user_device";
$app['table']['device']				= $prefix."device";
$app['table']['file_bea']			= $prefix."file_bea";
$app['table']['file_do']			= $prefix."file_do";
$app['table']['file_eir']			= $prefix."file_eir";
$app['table']['file_kel']			= $prefix."file_kel";
## TABLES ##
$prefixs = "view_";
$app['table']['all_name']		= $prefixs."all_name";
//////////////////////////////////
$app['view']['form'] 				= $prefix.$prefixs."form"; 
$app['view']['rule'] 				= $prefix.$prefixs."rule"; 
$app['view']['user'] 				= $prefix.$prefixs."user"; 
//////////////////////////////////
 ?>