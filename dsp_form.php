<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	// datepickerlib
	?>
	<style>
			.monthselect{
				color: black;
			}
			.yearselect{
				color: black;
			}
		</style> <?php
	admlib::get_component('datepickerlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>$act=="add"?$app['me']['name']:$form['name']
			)
		);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"company",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['company'])
		// 	)
		// );
		admlib::get_component('inputtext',
			array(
				"name"=>"nik",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>$act=="add"?$app['me']['nik']:$form['nik']
			)
		);
		$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section",
				"value"=>$act=="add"?$app['me']['id_section']:$form['id_section'],
				"items"=>$rs['section']
			)
		);
		admlib::get_component('datepicker',
			array(
				"name"=>"tgl_pengajuan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>$act=="add"?date("Y-m-d"):$form['tgl_pengajuan']
			)
		);
		admlib::get_component('radio',
			array(
				"name"=>"kat_si",
				"datas"=>["js","andon","email","tp"],
				"validate"=>true,
				"value"=>app::ov($form['kat_si'])
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"saat_ini",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['saat_ini'])
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"di_inginkan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['di_inginkan'])
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"tujuan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['tujuan'])
			)
		);
		
		admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			"value"=> app::ov($form['attach']),
			"filemedia"=>true
			)
		);
		if($form['status_progress'] == "progress" || $form['status_progress'] == "finished"){
				admlib::get_component('textarea',
					array(
						"name"=>"note",
						// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
						// "value"=>"",
						"validate"=>true,
						"value"=>app::ov($form['note'])
					)
				);
		}

		// admlib::get_component('submit',
		// 	array(
		// 		"id"=>(isset($id))?$id:"",
		// 		"act"=>$act
		// 	)
		// );
	
		$cek_creator = db::lookup("created_by",$module,"id",$id);
		if(!empty($id)){
			if($cek_creator==$app['me']["id"]){
				admlib::get_component('submit',
					array(
						"id"=>(isset($id))?$id:"",
						"act"=>$act
					)
				);
			}else{
				admlib::get_component('submit',
					array(
						"id"=>(isset($id))?$id:"",
						"no_submit"=>"iya",
						"act"=>$act
					)
				);
			}
		}else{
			admlib::get_component('submit',
				array(
					"id"=>(isset($id))?$id:"",
					"act"=>$act
				)
			);
		}
	admlib::get_component('formend');
	?>
	
<script>

$(document).ready(function () {
	<?php //if(empty($form['used_sys']) || $form['used_sys'] == "other"){ 
		  if($form['used_sys'] != "other"){ 
		?>
		$("#g_other").hide();
	<?php } ?>
		$('input[type=radio][name=p_used_sys]').change(function() {
			if (this.value == 'other') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_other").show();
			}
			else{
				// alert("Transfer Thai Gayo");
              	$("#g_other").hide();
				  $('#other').value(""); 
			}
		});
});

</script>
	<?php
admlib::display_block_footer();
?>