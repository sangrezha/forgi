<?php

/*******************************************************************************
* Filename : db.php
* Description : db library (mysql)
*******************************************************************************/

class db
{
    /***************************************************************************
    * Description : constructor
    ***************************************************************************/
    public static function connect()
    {
    	global $app;
        $app['db']['connection'] = @mysqli_connect($app['db']['server'], $app['db']['username'], $app['db']['password']);
		if (!$app['db']['connection']):
			return FALSE;
		endif;
		if (!@mysqli_select_db($app['db']['connection'], $app['db']['name'])):
			return FALSE;
		endif;
		return TRUE;
    }

    /***************************************************************************
    * Description : perform default query
    ***************************************************************************/
    public static function query($sql, &$result, &$nr)
    {
        global $app;
        //if (!stristr($sql,"union")):
		// echo $sql;
			$result = @mysqli_query($app['db']['connection'], $sql);
	    	if (stristr($sql,"select")):
	    	    $nr = @mysqli_num_rows($result);
	    	endif;
			if (mysqli_connect_error()):
				if ($app['debug']):
					$err[] = "SQL : $sql";
					$err[] = "ERROR : " . mysqli_connect_error();
					app::debug($err);
					exit;
				endif;
			endif;
		//endif;
    }
    /***************************************************************************
    * Description : perform default query
    ***************************************************************************/
    public static function query_ex($sql, &$result, &$nr, $con)
    {
        global $app;
        //if (!stristr($sql,"union")):
			$result = @mysqli_query($con, $sql);
	    	if (stristr($sql,"select")):
	    	    $nr = @mysqli_num_rows($result);
	    	endif;
			if (mysqli_error($app['db']['connection'])):
				if ($app['debug']):
					$err[] = "SQL : $sql";
					$err[] = "ERROR : " . mysqli_error($app['db']['connection']);
					app::debug($err);
					exit;
				endif;
			endif;
		//endif;
    }

    /***************************************************************************
    * Description : perform only simple query (delete/update)
    ***************************************************************************/
    public static function qry($sql)
    {
        global $app;
       //if (!stristr($sql,"union")):
	        $rs = @mysqli_query($app['db']['connection'], $sql);
			if (mysqli_error($app['db']['connection'])):
				if ($app['debug']):
					$err[] = "SQL : $sql";
					$err[] = "ERROR : " . mysqli_error($app['db']['connection']);
					app::debug($err);
					exit;
				endif;
			endif;
			return true;
		//endif;
		//unset($rs);
    }
    public static function enc($param)
  	{
        global $app;
		return "AES_ENCRYPT(AES_ENCRYPT('$param','".$app['enckey']."'),'".$app['enckey']."')";
		
  	}
    public static function dec($param,$lower = 0)
  	{
        global $app;
		if($lower){
			return "LOWER(CONVERT(AES_DECRYPT(AES_DECRYPT($param, '".$app['enckey']."'),'".$app['enckey']."') USING 'utf8'))";
		}else{
			return "AES_DECRYPT(AES_DECRYPT($param,'".$app['enckey']."'),'".$app['enckey']."')";
		}
  	}
	/***************************************************************************
    * Description : fetching single record from database [simple]
    ***************************************************************************/
    public static function get_record()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 2:
				$sql = "select * from ".$app['table'][$args[0]]." where $args[1] limit 1";
				break;
			case 3:
				$sql = "select * from ".$app['table'][$args[0]]." where $args[1] = '$args[2]' limit 1";
				break;
			case 4:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] = '$args[3]' limit 1";
				break;
			case 6:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] = '$args[3]' AND $args[4] = '$args[5]' limit 1";
				break;
		endswitch;
		// return $sql;
		// echo $sql;
		if (!stristr($sql,"union")):
			db::query($sql, $rs, $nr);
		endif;
    	if ($nr):
    	    $record = db::fetch($rs);
    	else:
    	    $record[0] = "";
    	endif;
		//unset($rs);
    	return $record;
    }
    public static function field_record()
    {
        global $app;
    		$args = func_get_args();
    		switch (func_num_args()):
    			case 3:
    				$sql = "select ". $args[0] ." from ".$app['table'][$args[1]]." where $args[2] limit 1";
    				break;
    			case 4:
    				$sql = "select ". $args[0] ." from ".$app['table'][$args[1]]." where $args[2] = '$args[3]' limit 1";
    				break;
    		endswitch;
    		if (!stristr($sql,"union")):
    			db::query($sql, $rs, $nr);
    		endif;
      	if ($nr):
      	    $record = db::fetch($rs);
      	else:
      	    $record[0] = "";
      	endif;
  		//unset($rs);
      	return $record;
    }
/***************************************************************************
    * Description : fetching single record from database [simple] filtered
    ***************************************************************************/
    public static function get_record_filter()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 3:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] limit 1";
				break;
			case 4:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] = '$args[3]' limit 1";
				break;
		endswitch;
		if (!stristr($sql,"union")):
			db::query($sql, $rs, $nr);
		endif;
    	if ($nr):
    	    $record = db::fetch($rs);
    	else:
    	    $record[0] = "";
    	endif;
		//unset($rs);
    	return $record;
    }

	/***************************************************************************
    * Description : fetching recorset from database
    ***************************************************************************/
    public static function get_recordset()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 1:
				$sql = "select * from ".$app['table'][$args[0]]."";
				break;
			case 2:
				$sql = "select * from ".$app['table'][$args[0]]." where $args[1]";
				break;
			case 3:
				$sql = "select * from ".$app['table'][$args[0]]." where $args[1] = '$args[2]'";
				break;
		endswitch;
		// echo $sql;
		if (!stristr($sql,"union")):
    		db::query($sql, $rs, $nr);
    	endif;
    	return $rs;
    }

	/***************************************************************************
    * Description : fetching recorset from database
    ***************************************************************************/
    public static function get_record_select()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 2:
				$sql = "select $args[0] from ".$app['table'][$args[1]]."";
				break;
			case 3:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2]";
				break;
			case 4:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] = '$args[3]'";
				break;
			case 5:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." inner JOIN ".$app['table'][$args[2]]." on $args[3] = $args[4] ";
				break;
			case 7:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." inner JOIN ".$app['table'][$args[2]]."   on $args[3] = $args[4] where $args[5] = $args[6] ";
				break;

		endswitch;
				// echo $sql;
		if (!stristr($sql,"union")):
    		self::query($sql, $rs, $nr);
    	endif;
    	return $rs;
    	// return $sql;
    }


    /***************************************************************************
    * Description : deleting record on specified table
	* Param : table_name, arr_record_id (using id fields)
	*         table_name, field, value
    ***************************************************************************/
    public static function delete_record()
    {
        global $app;
		$numargs = func_num_args();
		$args = func_get_args();
		$sql = "delete from ".$app['table'][$args[0]]." where 0!=0 ";
		if ($numargs == 2):
			if (is_array($args[1])):
				for ($x=0; $x < count($args[1]); $x++):
					$sql .= "or id = '".$args[1][$x]."' ";
				endfor;
			else:
				$sql .= "or id = '$args[1]' ";
			endif;
		else:
			if (is_array($args[2])):
				for ($x=0; $x < count($args[2]); $x++):
					$sql .= "or $args[1] = '".$args[2][$x]."' ";
				endfor;
			else:
				$sql .= "or $args[1] = '$args[2]' ";
			endif;
		endif;
		//echo $sql; exit;
    	db::qry($sql);
    }

    /***************************************************************************
    * Description : fetching query result to array + move pointer 1 level
    ***************************************************************************/
    public static function fetch($result)
    {
        global $app;
        return @mysqli_fetch_assoc($result);
    }

    /***************************************************************************
    * Description : seek db
    ***************************************************************************/
    public static function seek(&$result, $pointer=0)
    {
        global $app;
        @mysqli_data_seek($result, $pointer);
    }

    /***************************************************************************
    * Description : close connection, this is unnecessary public static function
    ***************************************************************************/
    public static function close()
    {
        global $app;
    	@mysqli_close($app['db']['connection']);
    }
	/***************************************************************************
    * Description : lookup
	* Param : field, table, field_key, $field_value
	*         field, table, condition
    ***************************************************************************/
    public static function viewlookup()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 3:
				$sql = "select $args[0] from ".$app['view'][$args[1]]." where $args[2] limit 1";
				break;
			case 4:
				$sql = "select $args[0] from ".$app['view'][$args[1]]." where $args[2] = '$args[3]' limit 1";
				break;
		endswitch;
    	db::query($sql, $rs, $nr);
		$record="";
    	if ($nr):
    	    $record = db::fetch($rs);
			$record = $record[$args[0]];
    	endif;
    	return $record;
    }
    /***************************************************************************
    * Description : lookup
	* Param : field, table, field_key, $field_value
	*         field, table, condition
    ***************************************************************************/
    public static function lookup()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 3:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] limit 1";
				break;
			case 4:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] = '$args[3]' limit 1";
				break;
			case 5:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." 
						left join ".$app['table'][$args[2]]." ON $args[3] where $args[4] limit 1";
				break;
		endswitch;
		//echo $sql;
    	db::query($sql, $rs, $nr);
		$record="";
    	if ($nr):
    	    $record = db::fetch($rs);
			$record = $record[$args[0]];
    	endif;
    	return $record;
    	// return $sql;
	}
	/**************************************** 
	 * description : cek id
	 * param : id, tabel, fied where
	*/
	public static function cek_id($id,$tabel,$where="id"){
		$cek_id = self::lookup("id",$tabel,$where,$id);
		if(!empty($cek_id)){
			// $id= 
			$id = rand(1, 100).rand(1, 100).date("dmYHis");
			return self::cek_id($id,$tabel,$where);
		}else{
			return $id;
		}
	}
    /***************************************************************************
	* Description : perform insert using sql function
	example :  db::ins_call_func("insert_log", $app['me']['id'], "page", "VIW");
    ***************************************************************************/
    public static function ins_call_func($name_func, $data1=null, $data2=null, $data3=null, $data4=null, $data5=null)
    {
        global $app;
       //if (!stristr($sql,"union")):
			 $sql = "CALL $name_func('$data1','$data2','$data3', '$data4', '$data5')";
			// exit;
			// return $sql;
			$rs = @mysqli_query($app['db']['connection'], $sql);
	        // $rs = @mysqli_query($app['db']['connection'], "CALL $name_func('$data1','$data2','$data3')");
			if (mysqli_error($app['db']['connection'])):
				if ($app['debug']):
					$err[] = "SQL : $sql";
					$err[] = "ERROR : " . mysqli_error($app['db']['connection']);
					app::debug($err);
					exit;
				endif;
			endif;
			return true;
		//endif;
		//unset($rs);
    }

    /***************************************************************************
    * Description : lookup
	* Param : field, table, field_key, $field_value
	*         field, table, condition
    ***************************************************************************/
    public static function lookup_order()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 3:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] limit 1";
				break;
			// case 4:
			// 	$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] = '$args[3]' limit 1";
			// 	break;
			case 4:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] order by $args[3] DESC limit 1";
				break;
		endswitch;
		//echo $sql;
    	db::query($sql, $rs, $nr);
		$record="";
    	if ($nr):
    	    $record = db::fetch($rs);
			$record = $record[$args[0]];
    	endif;
    	return $record;
    }
    /***************************************************************************
    * Description : lookup
	* Param : field, table, field_key, $field_value
	*         field, table, condition
    ***************************************************************************/
    public static function lookup_ex()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 4:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] limit 1";
				break;
		endswitch;
		//echo $sql;
    	db::query_ex($sql, $rs, $nr, $args[3]);
		$record="";
    	if ($nr):
    	    $record = db::fetch($rs);
			$record = $record[$args[0]];
    	endif;
    	return $record;
    }

    /***************************************************************************
    * Description : lookup
	* Param : field, table, field_key, $field_value
	*         field, table, condition
    ***************************************************************************/
    public static function alookup()
    {
        global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 3:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2]";
				break;
			case 4:
				$sql = "select $args[0] from ".$app['table'][$args[1]]." where $args[2] = '$args[3]'";
				break;
		endswitch;
    	db::query($sql, $rs, $nr);
    	if ($nr):
    		while ($row = @db::fetch($rs)):
    	    	$record[] = $row[$args[0]];
    	    endwhile;
    	endif;
    	return $record;
    }

    /***************************************************************************
    * Description : perform num rows
    ***************************************************************************/
    public static function nr(&$rs)
    {
        global $app;
        return @mysqli_num_rows($rs);
    }

    /***************************************************************************
    * Description : to concatenation array database result
    ***************************************************************************/
	public static function db_implode($param,$string,$as_string=false)
	{
		global $app;
		if (strlen($param)!=0):
			if ($as_string):
				foreach($string as $k=>$v){
					$string[$k] = "'".$v."'";
				}
			endif;
			$hasil=implode($param,$string);
		else:
			$hasil="''";
		endif;
		return $hasil;
	}

    /***************************************************************************
    * Description : count record from table
    ***************************************************************************/
    public static function count_record() {
    	global $app;
		$args = func_get_args();
		switch (func_num_args()):
			case 3:
				$sql = "select count($args[0]) as jumlah from ".$app['table'][$args[1]]." $args[2]";
				break;
		endswitch;
		db::query($sql, $rs, $nr);
    	if ($nr):
    		$row = @db::fetch($rs);
   	    	$record = $row['jumlah'];
    	endif;
    	return $record;
    }

	/***************************************************************************
    * Description : set status
    * Parameters : id & status
    ***************************************************************************/
	public static function set_status($tabel, $pid, $field) {
		global $app;
		/**/
		$status = db::lookup($field, $tabel, "id", $pid);
		if ( $status == "active" ):
			$statusnya = "inactive";
		elseif ( $status == "inactive" OR $status == "draft" ):
			$statusnya = "active";
		endif;

		$sql = "update ".$app['table'][$tabel]."
				set $field = '$statusnya'
				where id = '$pid'";
		db::qry($sql);
	}
    /***************************************************************************
    * Description : stripos
    ***************************************************************************/
	public static function stripos($haystack, $needle, $offset = 0) {
	   // first we need also the php4 compatible public static function for stripos()
	   return strpos(strtolower($haystack), strtolower($needle),$offset);
	}

    /***************************************************************************
    * Description : anti sql injection
    ***************************************************************************/
	public static function anti_sql_injection($input) {
	    // daftarkan perintah-perintah SQL yang tidak boleh ada
	    // dalam query dimana SQL Injection mungkin dilakukan
	    $aforbidden = array ("insert", "select", "update", "delete", "truncate",
	    					 "replace", "drop", " or ", ";", "#", "--", "=" );

	    // lakukan cek, input tidak mengandung perintah yang tidak boleh
	    $breturn=true;
	    foreach($aforbidden as $cforbidden) {
	        if(db::stripos($input, $cforbidden)) {
	            $breturn=false;
	            break;
	        }
	    }
	    return $breturn;
	}

	public static function sub($result)
	{
		if($result == 0)
		return "()";
		else
		{
		$row = mysqli_fetch_row($result);
		$strResult = "( $row[0]";
			while($row = mysqli_fetch_row($result))
				$strResult = "$strResult , $row[0]";

		$strResult = "$strResult )";
			return $strResult;
		}
	}

	public static function mysql_subquery($query)
	{
		$substart = strpos($query, " ( ");
		if($substart != false)
		{
			$subend = strrpos($query, " ) ");
			$before = substr($query, 0, $substart);
			$after = substr($query, $subend+1, -1);
			$subquery = substr($query, $substart+2, $subend-$substart-1);

			$subresult = db::mysql_subquery($subquery);
			$subtext = db::sub($subresult);
			return mysqli_query($before . $subtext . $after);
		}
		else return mysqli_query($query);
	}
	/*******************************************************************************
	* Description : running query and get its result and numrows
	* Parameters : $sql, &$result, &$numrows
	* Return : &$result, &$numrows
	*******************************************************************************/
	public static function run_query($sql,&$result,&$numrows)
	{
	    $result = mysqli_query($sql);
		echo mysqli_error($app['db']['connection']);
		if (stristr( $sql,"select")|| stristr($sql,"SELECT")):
		    $numrows = mysqli_num_rows($result);
		endif;
	}
	public static function table($name){
		global $app;
		return $app['table'][$name];
	}
	public static function view($name){
		global $app;
		return $app['view'][$name];
	}
}

?>
