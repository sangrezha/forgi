<?php

/*******************************************************************************
* Filename : usrlib.php
* Description : user library
*******************************************************************************/

class usrlib{
	function display_header(){
	    global $app;
		//$local = app::get_lang();
		include $app['path'] ."/block/blk_header.php";
	}	
	function display_page($page){
	    global $app;
		include $app['path'] ."/codebase/page.php";
	}
	// function display_footer_contact(){
	    // global $app;
		// include $app['path'] ."/block/blk_footer_contact.php";
	// }
	function display_footer($model = ''){
	    global $app;
		if (!$model):
			include $app['path']."/block/blk_footer.php";
		else:
			include $app['path']."/block/blk_footer_".$model.".php";
		endif;
	}
	function display_menu($model = ''){
		global $app,$GLOBALS;
		foreach($GLOBALS as $k=> $v){
			$$k=$v;
		}
		
		$rs['language'] = db::get_recordset("lang","status='active' ORDER BY reorder ASC");
		$rs['menu'] = db::get_recordset("menu","lang='".$app['langid']."' AND id_menu='' AND status='active' ORDER BY reorder ASC");
		
		$config = db::get_record("configuration", "id", 1);
		if (!$model):
			include $app['path']."/block/blk_menu.php";
		else:
			include $app['path']."/block/blk_menu_".$model.".php";
		endif;
	}	
	public static function get_template($name){
		global $app;
		if($name AND file_exists($app['template_path'] ."/". $name .".stpl"))
		{
			include $app['template_path']."/".$name.".stpl";
		}	
	}	
	/*******************************************************************************
	* Description : validating member
	* Parameters : $application
	* Variables : $_SESSION[membersession] = member session variables
	*******************************************************************************/
	function validate($application = '')
	{
		global $app;
		#print_r($_SESSION[membersession]);
		if (!strlen($_SESSION['membersession'])):			
			usrlib::display_msg($app['lang']['error']['title'], $app['lang']['error']['member_not_login']);
		endif;
		$app[member] = app::unserialize64($_SESSION['membersession']);
	}
	
	/***************************************************************************
    * Description : display msg
    * Parameters : $msg_title, $msg_content
    ***************************************************************************/
    function display_msg($msg_title, $msg_content){
		global $app;
		include "$app[path]/dsp_msg.php";
		exit;
    }
	
	/***************************************************************************
    * Description : get parent in product
    * Parameters : id
    ***************************************************************************/
	function get_top_parent($id, &$parent)
	{
		global $app;
		$data = db::get_record('product_parent', 'id', $id);
		$parent[] = $data[name];		
		if ($data['parent_id']!=0):
			 usrlib::get_top_parent($data['parent_id'],$parent);
		else:
			$parent = array_reverse($parent);
			$parent = implode(" / ", $parent);
		endif;
		return $parent;
	}

	function get_bottom_parent($id, &$child)
	{
		global $app;
		$dat = db::get_recordset('product_parent', 'parent_id', $id);
		while ($record = db::fetch($dat)):				
				$child[] = $record[id];			
		endwhile;

		while (list($k, $v) = @each($child)):
			usrlib::get_bottom_parent($v, $child);
		endwhile;
		return $child;
	}
	function get_meta_header($pid){
		global $app;
		$meta_header = db::get_record("meta_header", "page", $pid);	 
		$app['title'] = $meta_header['meta_title_'.$app['bahasa']];
		$app['lead'] = $meta_header['meta_description_'.$app['bahasa']];
		$app['detail'] = $meta_header['meta_keyword_'.$app['bahasa']];
		return;
	}
	function build_include($name){
		global $app;
		include_once $app['include_path']."/".strtolower($name).".php";
		return $output;
	}
	function get_include($subject){
		if(preg_match_all("/{[^}]*}/", $subject, $matches)){
			$output = str_replace("{","",$matches[0][0]);
			$output = str_replace("}","",$output);
			$output = str_replace($matches[0][0],app::build_form($output),$subject);
		}else{
			$output = $subject;
		}
		return $output;
	}
	function active(){
		global $GLOBALS;
		foreach($GLOBALS as $k=> $v) $$k=$v;
		$numargs = func_num_args();
		$args = func_get_args();
		$same[] = $act;
		$same[] = $p_id;
		$same[] = $sub;
		$same[] = $step;
		$same[] = $det;
		$cek = true;
		for($i=0;$i<count($args);$i++){
			if($app['action'][$args[$i]]!=$same[$i]){
				$cek = false;
				break;
			}
		}
		if($cek==true){
			$active = "class='active'";
		}else{
			$active = null;
		}
		echo $active;
	}
	function set_s_o($p){
		global $app;
		$sImg = "<img src=\"".$app['layout_image']."/ic_standart.png\" />";
		$oImg = "<img src=\"".$app['layout_image']."/ic_accessories.png\" />";
		if(stristr($p,"s|")==true){
			$output = str_replace("s|",$sImg."<br/>",$p);
			$output = str_replace("S|",$sImg."<br/>",$output);
		}else{
			if(strlen($p) == 1){
				if($p == "s"){
					$output = $sImg;
				}elseif($p == "o"){
					$output = "-";
				}
				else{
					$output = "-";
				}	
			}else{
				if(!empty($p)) $output = $p;
				else $output= "-";
			}
		}
		return $output;			
	}
	public static function getliblang($alias, $lang){
		global $app;
		return db::lookup('title','liblang',"published='active' AND alias='$alias' AND lang='$lang'");
	} 
}

?>