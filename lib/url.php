<?php

/*******************************************************************************
* Filename : url.php
* Description : url libary
*******************************************************************************/

class url
{
    function get_referer()
    {
        global $referer;
        if ($referer):
            $referer = ereg_replace ("&referer(.*)", "", $referer);
            if (!ereg("\?", $referer)):
                $referer .= "?";
            endif;
            return $referer;
        else:
            return $_SERVER['HTTP_REFERER'];
        endif;

    }
	
	function strip_querystring($url) 
	{
		if ($commapos = strpos($url, '?')):
			return substr($url, 0, $commapos);
		else:
			return $url;
		endif;
	}

	function get_short_referer() 
	{
		return url::strip_querystring($_SERVER['HTTP_REFERER']);
	}

	function me() 
	{
		if (getenv("REQUEST_URI")): 
			$me = getenv("REQUEST_URI");
		elseif (getenv("PATH_INFO")):
			$me = getenv("PATH_INFO");

		elseif ($GLOBALS["PHP_SELF"]):
			$me = $GLOBALS["PHP_SELF"];
		endif;
		return url::strip_querystring($me);
	}
	
	function complete_me() {
		if (getenv("REQUEST_URI")) {
			$me = getenv("REQUEST_URI");

		} elseif (getenv("PATH_INFO")) {
			$me = getenv("PATH_INFO");

		} elseif ($GLOBALS["PHP_SELF"]) {
			$me = $GLOBALS["PHP_SELF"];
		}
		return $me;
	}

	function navigator_url($field, $title, $color = "white") 
	{
		global $app;
		$url = url::complete_me();
		if (ereg('webadmin', $url)):
			$path_ext = "webadmin/";
		endif;
		if (!ereg('\?', $url)):
			$url .= "?";
		endif;
		
		$url = preg_replace("|offset=.*?&|", "offset=0&", $url);
		$url = str_replace('&sort=asc', '', $url);
		$url = str_replace('&sort=desc', '', $url);
		$url = str_replace("&order=$field", '', $url);
		$var = $_GET;
		if ($var['order'] == $field):
			if ($var['sort'] == 'asc'):
			$out = "<a href='$url&order=$field&sort=desc'><font color=$color>$title</font></a> <img src='$app[www]/{$path_ext}img/arrow-asc.gif'>";
			else:
				$out = "<a href='$url&order=$field&sort=asc'><font color=$color>$title</font></a> <img src='$app[www]/{$path_ext}img/arrow-desc.gif'>";
			endif;
		else:
			$out = "<a href='$url&order=$field&sort=asc'><font color=$color>$title</font></a>";
		endif;
		return $out;
	}
	function curPageURL() {
		$pageURL = 'http';
		if ((!empty($_SERVER['HTTPS'])) AND ($_SERVER['HTTPS'] == "on")) {$pageURL .= "s";}
			$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	function friendlyURL($string){
		$string = preg_replace("`\[.*\]`U","",$string);
		$string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
		$string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);
		return strtolower(trim($string, '-'));
		//return (trim($string, '-'));
	}	
	function defriendlyURL($string){
		$string = str_replace("-", " ",$string);
		return ucwords($string);
		//return (trim($string, '-'));
	}	
	function friendlyFolder($string){
		$string = preg_replace("`\[.*\]`U","",$string);
		$string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
		$string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);
		return strtolower(trim($string, '_'));
		//return (trim($string, '-'));
	}	
	function parseVideoEntry($entry) {      
      $obj= new stdClass;
      
    
      // get nodes in media: namespace for media information
      $media = $entry->children('http://search.yahoo.com/mrss/');
      $obj->title = $media->group->title;
      $obj->description = $media->group->description;
      
      // get video player URL
      $attrs = $media->group->player->attributes();
      $obj->watchURL = $attrs['url']; 
      
      // get video thumbnail
      $attrs = $media->group->thumbnail[0]->attributes();
      $obj->thumbnailURL = $attrs['url']; 
	  
    // get <yt:duration> node for video length
      $yt = $media->children('http://gdata.youtube.com/schemas/2007');
      $attrs = $yt->duration->attributes();
      $obj->length = $attrs['seconds']; 
      
      // get <yt:stats> node for viewer statistics
      $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
      $attrs = $yt->statistics->attributes();
      $obj->viewCount = $attrs['viewCount']; 
      
      // get <gd:rating> node for video ratings
      $gd = $entry->children('http://schemas.google.com/g/2005'); 
      if ($gd->rating) { 
        $attrs = $gd->rating->attributes();
        $obj->rating = $attrs['average']; 
      } else {
        $obj->rating = 0;         
      }
	
    
      // return object to caller  
      return $obj;      
    } 	
}