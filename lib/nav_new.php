<?php

class nav_new {
	var $max_page = 4;
    var $page_text = "";
    var $out_of_text = "";
    var $first_text = "&#171;";
    var $last_text = "&#187;";
    var $prev_text = "&#8249;";
    var $next_text = "&#8250;";
    var $page_offset = 0;
    var $page_now = 0;
    var $page_total = 0;
    var $param = "";
    var $url = "";
    var $navbar = "";
    
    function init($offset, $total, $param="") {
		global $app;
		$page = explode("/page/",$param);
		$param = $page[0]."page/";
		
		$this->page_now = $offset;
		$this->page_total = $total;
		$this->page_text = $page_text;
		$this->out_of_text = $out_of_text;
    }
	function build($data_size,$data_total) {
		$page_now = $this->page_now;
		$page_total = $this->page_total;
		if ($data_total <= $data_size):
			$this->navbar = "";
			return;
		endif;
	    $out = '';
	    if ($page_now > 1):
	        $out .= "<a style='cursor:pointer;' href='" . $this->param . 1 . "/' class='next_prev_pagination'>". $this->first_text ."</a>\n";
	        $out .= "<a style='cursor:pointer;' href='" . $this->param . ($page_now - 1) . "/'  class='next_prev_pagination'>". $this->prev_text ."</a>\n";
        else:
            $out .= "";
	    endif;
		
		$range = $this->max_page + $page_now;

		 
		// $start = $page_now;
		// if($start > ceil($this->max_page / 2)){
			// $start = $page_now - ceil($this->max_page / 2);
		// }else{
			// $start = $page_now;
		// }
		// if($range >= $page_total){
			// $range = $page_total;
		// }else{
			// $range = $range - ceil($this->max_page / 2);
			// // $range = $range;
			
		// }
		$range = $this->max_page + $page_now;
		$start = $page_now;
		
		if($start > ceil($this->max_page / 2)){
			$start = $page_now - ceil($this->max_page / 2);
			$range = $range - ceil($this->max_page / 2);
		}
		if($range >= $page_total){
			$range = $page_total;
			$start = $page_total - $this->max_page;
		}
		
		$start = ($start < 1)? 1: $start;
		for ($i = $start; $i <= $range; $i++):
	        if ($i == $page_now):
				$class = "active";
	        else:
				$class = "text";
	        endif;
			
	        $out .= "<a style='cursor:pointer;' href='" . $this->param . $i . "/' class='$class'>" . $i . "</a>\n";
	    endfor;
		
	    if ($page_now < $page_total):
	        $out .= "<a style='cursor:pointer;' href='" . $this->param . ($page_now + 1)  . "/' class='next_prev_pagination'>". $this->next_text ."</a>\n";
			$out .= "<a style='cursor:pointer;' href='" . $this->param . $page_total . "/' class='next_prev_pagination'>". $this->last_text ."</a>\n";
	    endif;

        $this->navbar = $out.'<br>';
	}
	
	function navbar() {
		return $this->navbar;
	}

	function navbar_text() {
		global $app;
		if ($this->navbar):
			$out = "{$app[lang][txt][page]} " . ($this->page_offset / $this->page_size + 1) . " {$app[lang][txt][out_of]} " . ceil($this->page_total / $this->page_size) . "&nbsp;";
		endif;
		return $out;
	}

	function up(){
		global $app;
		$img = "<img src=\"{$app[www]}/img/up.gif\" align=\"right\"  onClick=\"javasript:location.replace('#')\" title=\"To Upper Line\">";
		return $img;
	}
}

?>