<?php

//use \Firebase\JWT\JWT;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

// Load Composer's autoloader
// require $app['path'].'/vendor/autoload.php';

require $app['path_vendors'].'/PHPMailer/src/Exception.php';
require $app['path_vendors'].'/PHPMailer/src/PHPMailer.php';
require $app['path_vendors'].'/PHPMailer/src/SMTP.php';


/*
include $app['path']."/vendor/PHPMailer/src/PHPMailer.php";
include $app['path']."/vendor/PHPMailer/src/SMTP.php";
include $app['path']."/vendor/PHPMailer/src/Exception.php";
*/
class app {
	public static $lang = 'en';
	/***************************************************************************
	* Description : load lib
	***************************************************************************/
	public static function load_lib()
	{
		global $app;
		$numargs = func_num_args();
		$args = func_get_args();
		for ($i = 0; $i < $numargs; $i++) {
			$libfile = "$args[$i].php";
			include_once $app['path'] ."/lib/$libfile";
		}
	}
	/***************************************************************************
	* Description : strtoupper
	***************************************************************************/
	public static function strtoupper($param)
	{
		$param = @explode(',', $param);
		while (list(, $var) = @each($param)):
			$cmd = "global \$$var;";
			eval($cmd);
			$cmd = "\$$var = trim(strtoupper(\$$var));";
			eval($cmd);
		endwhile;
	}

	/***************************************************************************
	* Description : set null value
	***************************************************************************/
	public static function set_null($param)
	{
		$param = @explode(',', $param);
		while (list(, $var) = @each($param)):
			$cmd = "global \$$var;";
			eval($cmd);
			$cmd = "\$testvar = trim(\$$var);";
			eval($cmd);
			if (strlen($testvar) == 0):
				$cmd = "\$$var = 'NULL';";
				eval($cmd);
			endif;
		endwhile;
	}

	/***************************************************************************
	* Description : serialize64
	***************************************************************************/
	public static function serialize64($var)
	{
		return base64_encode(serialize($var));
	}

	/***************************************************************************
	* Description : unserialize64
	***************************************************************************/
	public static function unserialize64($var)
	{
		return unserialize(base64_decode($var));
	}

	/***************************************************************************
	* Description : generate uid
	***************************************************************************/
	public static function generate_unique_id($table_name, $field_name, $length)
	{
		$str = " ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		$lstr = 36;
		$luid = $length;
		while (TRUE):
			$id = "";
			for ($i = 0; $i < $luid; $i++):
				srand((double)microtime()*100000);
				$index = rand(1,$lstr);
				$id .= $str[$index];
			endfor;
			if (!db::lookup($field_name, $table_name, $field_name, $id)):
				break;
			endif;
		endwhile;
		return $id;
	}
	/***************************************************************************
	* Description : string helpers
	***************************************************************************/
	public static function set_default(&$var, $default = '')
	{
		if (!isset($var) || $var == ''):
			$var = $default;
		endif;
	}

	public static function nvl(&$var, $default = '')
	{
		return isset($var) ? $var : $default;
	}

	public static function ov(&$var) {
		return isset($var) ? htmlspecialchars(stripslashes($var)) : "";
	}

	public static function pv(&$var) {
		echo isset($var) ? htmlspecialchars(stripslashes($var)) : "";
	}

	public static function o($var) {
		return empty($var) ? "" : htmlspecialchars(stripslashes($var));
	}

	public static function p($var) {
		echo empty($var) ? "" : htmlspecialchars(stripslashes($var));
	}

	/***************************************************************************
	* Description : for debugging
	***************************************************************************/
	public static function debug($var, $exit = 0)
	{
		global $app;
		if ($app['debug']):
			echo "<pre>";
			print_r($var);
			echo "</pre>";
		endif;
		if ($exit):
			exit;
		endif;
	}

	/***************************************************************************
	* Description : set navigation bar
	***************************************************************************/
	public static function set_navigator(&$sql, &$nav, $pagesize, $param)
	{
		if (isset($_GET['page_offset'])):
			$pageoffset = $_GET['page_offset'];
		else:
			$pageoffset = 0;
		endif;
		if (isset($_GET['page_total'])):
			$pagetotal = $_GET['page_total'];
		else:
			db::query($sql, $rs, $nr);
			$pagetotal = $nr;
		endif;
		$cari_limit = preg_match_all('|limit (.*)|sm',$sql,$hasil);
		if (isset($hasil[1][0])):
			$sql = str_replace("".$hasil[0][0]."","",$sql);
		endif;
		if ($pageoffset+$pagesize>$pagetotal):
			$nilai = $pagetotal-$pageoffset;
			$sql = $sql . " limit $pageoffset, $nilai";
		else:
			$sql = $sql . " limit $pageoffset, $pagesize";
		endif;

		$nav = new nav;
		$nav->init($pageoffset, $pagetotal, $param);
		$nav->param = $param;
		$nav->build($pagesize);
	}
	/***************************************************************************
	* Description : set navigation bar
	***************************************************************************/
	function set_pagings(&$sql, &$nav, $pagesize, $param, $attr='', $attrPls='')
	{
		if(!preg_match("/page/i",$param)){ $param = $param."page/"; }
		$page = explode("/page/",$param);
		$param = $page[0]."/page/";
		$page = explode("/",$page[1]);
		$page = (int) $page[0];
		$pagenow = ($page < 1)? 1: $page;

		if ($pagenow > 1):
			$pagestart = (($pagenow-1)*$pagesize);
		else:
			$pagestart = 0;
		endif;

		db::query($sql, $rs, $nr);
		$pagetotal = ceil($nr/$pagesize);

		$sql = $sql . " limit $pagestart, $pagesize";

		$nav = new nav_new;
		$nav->init($pagenow, $pagetotal, $param, $attr, $attrPls);
		$nav->param = $param;
		$nav->build($pagesize,$nr);
	}
	/***************************************************************************
	* Description : helper function for delimited data
	* Notes : -
	***************************************************************************/
	public static function array_delimiter($op, $delimiter, $item, $data = '')
	{
		if ($op == 'add'):
			if (!is_array($item)):
				if (!trim($data)):
					return $delimiter . $item . $delimiter;
				else:
					if (ereg($delimiter . $item . $delimiter, $data)) return $data;
					return $data . $item . $delimiter;
				endif;
			else:
				return $delimiter . @implode($delimiter, $item) . $delimiter;
			endif;
		endif;
		if ($op == 'del'):
			$tmp = str_replace($item.$delimiter, '', $data);
			if (trim($tmp) == $delimiter):
				return '';
			else:
				return $tmp;
			endif;
		endif;
		if ($op == 'get'):
			$tmp1 = explode("$delimiter", $data);
			while (list(, $v) = each($tmp1)):
				if (!empty($v)):
					$tmp2[] = $v;
				endif;
			endwhile;
			return $tmp2;
		endif;
	}

	/***************************************************************************
	* Description : trim an array (remove empty)
	***************************************************************************/
	public static function trim_array(&$param)
	{
		while (list($k, $v) = @each($param)):
			if (!trim($v)):
				unset($param[$k]);
			endif;
		endwhile;
		reset($param);
	}

	/***************************************************************************
	* Description : check IE
	***************************************************************************/
    public static function is_ie()
    {
		global	$HTTP_USER_AGENT;
    	if (eregi("MSIE", $HTTP_USER_AGENT)):
            return true;
        else:
        	return false;
        endif;
    }

	/***************************************************************************
	* Description : date convert
	***************************************************************************/
	public static function format_date_base($date)
	{
		$d = explode('-', $date);
		return "$d[2]/$d[1]/$d[0]";
	}
    /***************************************************************************
    * Description : format date
    * Parameters : $date (YYYY-MM-DD)
	*			   $country : eng, ina
	*			   $format : use built in PHP date format
	*			   $long : Y, N (long format)
    ***************************************************************************/
    public static function format_date($date, $country, $long = 'N')
    {
        if ($long == 'N'):
			$format = "l, F d Y";
		elseif ($long == 'A'):
			$format = "d F Y";
		elseif ($long == 'Ab'):
			$format = "d F";
		elseif ($long == 'B'):
			$format = "F d ,Y";
		elseif ($long == 'C'):
			$format = "F d";
		elseif ($long == 'D'):
			$format = "Y-m-d";
		elseif ($long == 'E'):
			$format = "d/m/Y";
		elseif ($long == 'F'):
			$format = "Y-m";
		elseif ($long == 'MMM'):
			$format = "d-m-Y";
		elseif ($long == 'G'):
			$format = "d";
		else:
			$format = "l, d F Y";
		endif;
		$out = date($format, strtotime($date));
		if ($country == 'id'):
			$eng = array("/January/", "/February/", "/March/", "/April/", "/May/", "/June/", "/July/", "/August/", "/September/", "/October/", "/November/", "/December/");
        	$ina = array("Januari", "Pebruari", "Maret", "April", "Mei","Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember");
			$out = preg_replace($eng, $ina, $out);
			if ($long != 'N'):
				$eng = array("/Monday/", "/Tuesday/", "/Wednesday/", "/Thursday/", "/Friday/", "/Saturday/", "/Sunday/");
				$ina = array("Senin", "Selasa", "Rabu", "Kamis", "Jum'at","Sabtu", "Minggu");
				$out = preg_replace($eng, $ina, $out);
			endif;
		endif;
		return $out;
    }
    /***************************************************************************
    * Description : format datetime
    * Parameters : $date (YYYY-MM-DD HH:MM:SS)
	*			   $country : eng, ina
	*			   $format : use built in PHP date format
	*			   $long : Y, N (long format)
    ***************************************************************************/
    public static function format_datetime($date, $country, $long = 'N')
    {
        if ($long == 'N'):
			$format = "F d, Y H:i:s";
		elseif ($long == 'NN'):
			$format = "l, d F Y";
		elseif ($long == 'NN_1'):
			$format = "d/m/Y";
		elseif ($long == 'M'):
			$format = "l d,Y / H:i";
		elseif ($long == 'MM'):
			$format = "l, d F Y | H:i";
		elseif ($long == 'MMM'):
			$format = "d-m-Y | H:i";
		else:
			$format = "l, d F Y H:i:s";
		endif;

		$out = date($format, strtotime($date));
		if ($country == 'ina'):
			$eng = array("/January/", "/February/", "/March/", "/April/", "/May/", "/June/", "/July/", "/August/", "/September/", "/October/", "/November/", "/December/");
        	$ina = array("Januari", "Pebruari", "Maret", "April", "Mei","Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember");
			$out = preg_replace($eng, $ina, $out);
			if ($long != 'N'):
				$eng = array("/Monday/", "/Tuesday/", "/Wednesday/", "/Thursday/", "/Friday/", "/Saturday/", "/Sunday/");
				$ina = array("Senin", "Selasa", "Rabu", "Kamis", "Jum'at","Sabtu", "Minggu");
				$out = preg_replace($eng, $ina, $out);
			endif;
		endif;
		return $out;
    }
    /***************************************************************************
    * Description : partstr
    * Parameters : $str : string
	*			   $length : length
    ***************************************************************************/
    public static function partstr($str, $length)
	{
		return (strlen($str)>$length) ? substr($str, 0, $length)."..." : $str;
	}

    /***************************************************************************
    * Description : mq_encode
    * Parameters : $param : var
    ***************************************************************************/
    public static function mq_encode($param)
	{
		if (!get_magic_quotes_gpc()):
			$fields = "\$".str_replace(",", ",\$", $param);
			eval("global $fields;");
			$arr = explode(",", $fields);
            while (list($k, $v) = each($arr)):
                eval("$v = addslashes($v);");
            endwhile;
		endif;
	}

	/**
	 * @param int $pass_len The length of the password
	 * @param bool $pass_num Include numeric chars in the password?
	 * @param bool $pass_alpha Include alpha chars in the password?
	 * @param bool $pass_mc Include mixed case chars in the password?
	 * @param string $pass_exclude Chars to exclude from the password
	 * @return string The password
	 */

	public static function make_uniqid($pass_len = 16, $pass_num = true, $pass_alpha = true, $pass_mc = true, $pass_exclude = '')
	{
	    // Create the salt used to generate the password
	    $salt = '';
	    if ($pass_alpha) { // a-z
	        $salt .= 'abcdefghijklmnopqrstuvwxyz';
	        if ($pass_mc) { // A-Z
	            $salt .= strtoupper($salt);
	        }
	    }

	    if ($pass_num) { // 0-9
	        $salt .= '0123456789';
	    }

	    // Remove any excluded chars from salt
	    if ($pass_exclude) {
	        $exclude = array_unique(preg_split('//', $pass_exclude));
	        $salt = str_replace($exclude, '', $salt);
	    }
	    $salt_len = strlen($salt);

	    // Seed the random number generator with today's seed & password's unique settings for extra randomness
	    mt_srand ((int) date('y')*date('z')+date('His')*($salt_len+$pass_len));

	    // Generate today's random password
	    $pass = '';
	    for ($i=0; $i<$pass_len; $i++) {
	        $pass .= substr($salt, mt_rand() % $salt_len, 1);
	    }

	    return $pass;
	}

	/***************************************************************************
	* Description : format harga
	***************************************************************************/
	public static function tulis_harga($harga)
	{
		global $app;
		$dollar = db::lookup("nilai","preference","nama='dollar'");
		$min = db::lookup("nilai","preference","nama='minimal'");
		if ($harga>$min){
			$hasil = "Rp. ".number_format($harga, 2, ',', '.');
		}else{
			$hasil = "Rp. ".number_format(($harga*$dollar), 2, ',', '.');
		}
		return $hasil;
	}
	/***************************************************************************
	* Description : interval waktu
	***************************************************************************/
	public static function interval_time($start, $end){
		$tanggal = $start;
		if (!$tanggal) $tanggal = $end;

		$waktu = strtotime($tanggal);
		$sekarang = strtotime($end);

		if ($waktu > $sekarang){
			$jarak = $waktu - $sekarang;
		}else{
			$jarak = $sekarang - $waktu;
		}

		$arrdata = array("tahun" => 31104000,
						 "bulan" => 2592000,
						 "hari" => 86400,
						 "jam" => 3600,
						 "menit" => 60,
						 "detik" => 0);

		$teksstr = array();

		foreach($arrdata as $kata => $constant){
			$r = ($constant) ? floor( $jarak / $constant) : $jarak;
			if ($r){
				$jarak -= $r * $constant;
				$teksstr[] = $r." ".$kata;
			}else{
				$teksstr[] = "";
			}
		}
		$hasil = implode(" ", $teksstr);

		return $hasil;
	}
	/*************************************************************************
	/***************************************************************************
	* Description : generate password
	***************************************************************************/
	public static function randomNumber ($length){
		$password = "";
  		$possible = "0123456789bcdfghjkmnpqrstvwxyz";
  		$i = 0;
  		while ($i < $length) {
   			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
    		if (!strstr($password, $char)) {
      			$password .= $char;
      			$i++;
    		}
  		}
 		return $password;
	}
	public static function init_lang(){
		$_SESSION['lang'] = null;
	}
	public static function set_lang($local){
		self::init_lang();
	    $_SESSION['lang'] = $local;
	}
	public static function get_lang(){
	   return $_SESSION['lang'];
	}

	/***************************************************************************
	* Description : pos_char
	***************************************************************************/
	public static function pos_char($kata, $kalimat){
		$exp_kal = explode(" ", $kalimat);
		$end = count($exp_kal);
		//$imp_kal = implode(" ", $kalimat);
		$hasil[0] = 0;
		$b=0;
		for($a=0; $a<$end; $a++){
			if(eregi($kata,$exp_kal[$a])):
				$hasil[$b] = $a;
				$b++;
			endif;
		};
		return $hasil[0];
	}
	/***************************************************************************
	* Description : get_lead
	***************************************************************************/
	public static function get_lead($no, $kalimat, $start=0){
		$exp_kal = explode(" ", $kalimat);
		if($start>0):
		$end = $no+$start;
		else:
		$end = $no;
		endif;
		//$imp_kal = implode(" ", $kalimat);
		$hasil = "";
		for($a=$start; $a<$end; $a++){
			$hasil = $hasil.$exp_kal[$a]." ";
		}
		$hasil = strip_tags($hasil);
		return $hasil;
	}
	/***************************************************************************
	* Description : replace_char
	***************************************************************************/
	public static function tag_on_char($kata, $kalimat, $str_tag, $end_tag){
		$kalimat = strip_tags($kalimat);
		$exp_kal = explode(" ", $kalimat);
		$end = count($exp_kal);
		//$imp_kal = implode(" ", $kalimat);
		$hasil = "";
		for($a=0; $a<$end; $a++){
			if(eregi($kata,$exp_kal[$a])):
				$all_big = strtoupper($kata);
				$big_small = ucwords($kata);
				$all_small = strtolower($kata);
				$char = $exp_kal[$a];
				if(stristr($all_big,$char)):
					$replace = $str_tag.$all_big.$end_tag;
					$char = str_replace($all_big,$replace,$char);
						$hasil = $hasil.$char." ";
				elseif(stristr($big_small,$char)):
					$replace = $str_tag.$big_small.$end_tag;
					$char = str_replace($big_small,$replace,$char);
						$hasil = $hasil.$char." ";
				elseif(stristr($all_smal,$charl)):
					$replace = $str_tag.$all_small.$end_tag;
					$char = str_replace($all_small,$replace,$char);
						$hasil = $hasil.$char." ";
				else:
					$hasil = $hasil.$str_tag.$char.$end_tag." ";
				endif;
			else:
				$hasil = $hasil.$exp_kal[$a]." ";
			endif;
			/*if( strstr($kata, $exp_kal[$a]) {
				$hasil = $hasil.$exp_kal[$a]." ";
			}*/
		}
		return $hasil;
	}
	public static function sanitize($param){
		if (!get_magic_quotes_gpc()):
			$fields = "\$".str_replace(",", ",\$", $param);
			eval("global $fields;");
			$arr = explode(",", $fields);
            while (list($k, $v) = each($arr)):
                eval("\$kal = strip_tags($v);");
							$kal = htmlentities($kal);
							$kal = preg_replace('/[\n\r\t]+/', '', $kal);
							$kal = str_replace(chr(0xCA), '', $kal);
							$kal = str_replace('\\\$', '\$', $kal);
							$kal = str_replace('\r', '', $kal);
							$kal = preg_replace('/&amp;#([0-9]+);/s', '&#\\1;', $kal);
							$kal = preg_replace('/\\\(?!&amp;#|\?#)/', "\\", $kal);
                eval("$v = \$kal;");
            endwhile;
		endif;
	}
	public static function get_extension($string){
		$ex_x = explode(".",$string);
		$extention = $ex_x[count($ex_x)-1];
		$search  = array('!', '@', '#', '%', '$', '&', '*', '(', ')', '<', '>', '?', '/', '\\', '=', ':', ';', '~', '`', '|', '+', '-');
		$replace = array('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
		$extention = str_replace($search, $replace, $extention);

		return $extention;
	}
	public static function multisort($data,$keys){
	  // List As Columns
	  foreach ($data as $key => $row) {
	    foreach ($keys as $k){
	      $cols[$k['key']][$key] = $row[$k['key']];
	    }
	  }
	  // List original keys
	  $idkeys=array_keys($data);
	  // Sort Expression
	  $i=0;
	  foreach ($keys as $k){
	    if($i>0){$sort.=',';}
	    $sort.='$cols['.$k['key'].']';
	    if($k['sort']){$sort.=',SORT_'.strtoupper($k['sort']);}
	    if($k['type']){$sort.=',SORT_'.strtoupper($k['type']);}
	    $i++;
	  }
	  $sort.=',$idkeys';
	  // Sort Funct
	  $sort='array_multisort('.$sort.');';
	  eval($sort);
	  // Rebuild Full Array
	  foreach($idkeys as $idkey){
	    $result[$idkey]=$data[$idkey];
	  }
	  return $result;
	}
	public static function bold_search($content, $p_key, $count){
		$count = ($count=="all")? strlen($content) : $count ;
		if(count($p_key)>0){
			$strpos = stripos($content, $p_key[0]);
			$lengstr = strlen($p_key[0]);
		}else{
			$strpos = 0;
			$lengstr = 0;
		}
		$output = "";
		$findContent = substr(($content), $strpos, $count);
		$findContent = substr($findContent,0,strrpos($findContent," "));

		for($i=0;$i<count($p_key);$i++){
			$findContent = str_ireplace($p_key[$i],"<span style=\"background-color:#ff0;padding:0px;font-size: inherit;\">".$p_key[$i]."</span>", $findContent);
		}
		$output = ($strpos>0)? "...".$findContent."..." : $findContent."..." ;
		return $output;
	}
	public static function searchx($colm, $key){
		$hsl = "(".$colm." like '%".implode(" ",$key)."%'";
		if(count($key)>0){
			for($i=0;$i<count($key);$i++){
				$hsl .= " or ".$colm." like '%".$key[$i]."%'";
			}
			$hsl .= ")";
		}else{
			$hsl = "(".$colm." like '%".$key."%')";
		}
		return $hsl;
	}
	public static function search_key($queryString){
		$queryString = strip_tags(preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $queryString));
		$queryString = str_replace("\r\n"," ",$queryString);
		$queryString = htmlspecialchars_decode($queryString);
		$queryString = str_replace('"',"",$queryString);
		$queryString = str_replace("'","",$queryString);
		$queryString = str_replace("&rsquo;","",$queryString);
		$queryString = preg_replace('/&(?:[a-z\d]+|#\d+|#x[a-f\d]+);/i', ' ', $queryString);
		$queryString = trim($queryString);
		$key = explode(' ',$queryString);

		return $key;
	}
	public static function array_to_xml($data, &$xml_data) {
		foreach($data as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml_data->addChild($key);
					array_to_xml($value, $subnode);
				}else{
					array_to_xml($value, $xml_data);
				}
			}else{

				$xml_data->addChild($key,str_replace("\n","",htmlentities($value)));
			}
		}
		return $xml_data;
	}
	public static function getliblang($alias,$bahasa="en"){
		global $app;
		//$lang = $app['me']['lang']
		// $lang = db::lookup('id', 'lang', 'alias', 'en');
		$lang = db::lookup('id', 'lang', 'alias', $bahasa);
//		return db::lookup('name','i18n',"status='active' AND alias='$alias' AND lang='". $lang ."'");
		return db::lookup('name','i18n',"status='active' AND alias='$alias' AND lang='". $lang ."'");
	}
	public static function slugify($text)
	{
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  // trim
	  $text = trim($text, '-');

	  // remove duplicate -
	  $text = preg_replace('~-+~', '-', $text);

	  // lowercase
	  $text = strtolower($text);

	  if (empty($text)) {
		return 'n-a';
	  }

	  return $text;
	}
	public static  function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' kB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}
	public static function Jdecode($response){
		global $app;
		return (array) json_decode($response, true);
	}
	public static function rangeMonth($datestr) {
		date_default_timezone_set(date_default_timezone_get());
		$dt = strtotime($datestr);
		$res['start'] = date('Y-m-d', strtotime('first day of this month', $dt));
		$res['end'] = date('Y-m-d', strtotime('last day of this month', $dt));
		return $res;
	}
	public static function encrypt($data) {
		// $key   = self::serialize64(PHP_Crypt::createKey(PHP_Crypt::RAND, 16));
		// $crypt = new PHP_Crypt($key, PHP_Crypt::CIPHER_AES_128, PHP_Crypt::MODE_CBC);
		// $iv    = $crypt->createIV();
		// $encrypt = $crypt->encrypt($data);
		// return self::serialize64([$key, self::serialize64($iv), self::serialize64($encrypt)]);
	}
	public static function decrypt($arg){
		// $args     = self::unserialize64($arg);
		// $crypt 	  = new PHP_Crypt($args[0], PHP_Crypt::CIPHER_AES_128, PHP_Crypt::MODE_CBC);
		// $crypt->IV(self::unserialize64($args[1]));
		// $decrypt 	= $crypt->decrypt(self::unserialize64($args[2]));
		// return $decrypt;
	}
	public static function rangeWeek($datestr) {
		date_default_timezone_set(date_default_timezone_get());
		$dt = strtotime($datestr);
		$res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
		$res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
		return $res;
	}
	public static function get_token($secret)
	{
		global $app;
      	$output = db::lookup('token', 'client', 'secret', $secret);
      	$output   = str_replace($app['key'],"", $output);
		return $output;
	}
	public static function get_obj($secret)
	{
		try {
			$token  	= self::get_token($secret);
			$result   	= JWT::decode($token, $secret, array('HS256'));
			return $result;
		}
		//catch exception
		catch(Exception $e) {
			return 401;
		}
	}
	public static function parse($params){
		echo json_encode(['results'=>$params]);
	}
	// public static function response($code, $args = []){
	// 	global $app;

	// 	header("HTTP/1.1 ". $code ." ". $app['rest']['status'][$code]);
	// 	header('Content-type: application/json');
	// 	$respons = ['code'=>$code, 'status'=>$app['rest']['status'][$code]];
	// 	if(count($args) > 0)
	// 	{
	// 		$respons = array_merge($respons, ['results'=>$args]);
	// 	}
	// 	echo json_encode($respons);
	// }
	/***************************************************************************
	* Description : Send Push
	***************************************************************************/
	public static function pushNotifSend($data) 
	{
		global $app;
		$url = "https://fcm.googleapis.com/fcm/send";
		$headers = array(
			'Authorization:key = '.$app['FirebaseKey'],
			'Content-Type: application/json'
		);
		$pesan = json_encode($data);
		$ch = curl_init();
		curl_setopt($ch , CURLOPT_URL, $url);
		curl_setopt($ch , CURLOPT_POST, true);
		curl_setopt($ch , CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch , CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch , CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch , CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch , CURLOPT_POSTFIELDS,$pesan);
		$result = curl_exec($ch);
		if($result === false){
			die('curl failed : '.curl_error($ch) );
		}
		curl_close($ch);
		return $result;
	}
	/***************************************************************************
	* Description : Send Push client
	***************************************************************************/
	public static function pushNotifClientSend($data) 
	{
		global $app;
		$url = "https://fcm.googleapis.com/fcm/send";
		$headers = array(
			'Authorization:key = '.$app['FirebaseKey_client'],
			'Content-Type: application/json'
		);
		$pesan = json_encode($data);
		$ch = curl_init();
		curl_setopt($ch , CURLOPT_URL, $url);
		curl_setopt($ch , CURLOPT_POST, true);
		curl_setopt($ch , CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch , CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch , CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch , CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch , CURLOPT_POSTFIELDS,$pesan);
		$result = curl_exec($ch);
		if($result === false){
			die('curl failed : '.curl_error($ch) );
		}
		curl_close($ch);
		return $result;
	}
	/***************************************************************************
	* Description : date convert
	***************************************************************************/
	public static function urlApiFile($folder,$file) 
	{
		global $app;
		$filename = $app['data_path']."/".$folder."/".$file;
		if(!empty($file) && file_exists($filename)){
			$file = $app['data_http']."/".$folder."/".$file;
		}else{
			$file = "";
		}
		return $file;
	}
	/***************************************************************************
	* Description : Random String
	***************************************************************************/
	public static function random_string($length = 6) {
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		// if(preg_match('/^[A-Z][0-9][a-z]+$/', $randomString)) {
			$output = $randomString;
		// }else{
			// $output = self::random_string();
		// }
		return $output;
	}
	public static function welcome($time) {
		/* This sets the $time variable to the current hour in the 24 hour clock format */
		// $time = date("H");
		/* Set the $timezone variable to become the current timezone */
		$timezone = date("e");
		/* If the time is less than 1200 hours, show good morning */
		if ($time < "12") {
			$welcome = "Selamat pagi";
		} else
		/* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
		if ($time >= "12" && $time < "17") {
			$welcome = "Selamat siang";
		} else
		/* Should the time be between or equal to 1700 and 1900 hours, show good evening */
		if ($time >= "17" && $time < "19") {
			$welcome = "Selamat sore";
		} else
		/* Finally, show good night if the time is greater than or equal to 1900 hours */
		if ($time >= "19") {
			$welcome = "Selamat malam";
		}
		
		return $welcome;
	}
	public static function i18n($param){
		global $app;
		if(is_array($param))
		{
			return db::lookup('name','i18n',"status='active' AND alias='". $param[0] ."' AND lang='". $param[1] ."'");
		}
		else
		{
			return db::lookup('name','i18n',"status='active' AND alias='$param' AND lang='". db::lookup("id", "lang", "alias", self::$lang) ."'");
		}
	}
// 	public static function sendmail($p_email, $subject, $content, $path_file = null){
// 			global $app;
// 		// require $app['path_vendors'].'/PHPMailer/PHPMailerAutoload.php';
// 		// echo $app['path_vendors'].'/PHPMailer/PHPMailerAutoload.php';
			
// 		// $myfile = fopen("zxcasd.txt", "w") or die("Unable to open file!");
// 		// fwrite($myfile, $sqlssss);
// 		// fclose($myfile);
// 			// $p_email = explode(",",$p_email);
// 			// if(count($p_email) == 0)
// 			// {
// 			// 	return false;
// 			// 	exit;
// 			// }
// 		// echo "disini ada ";
// 		// 	print_r($p_email);
// 			// exit;
// // print_r($p_email);
// // echo "<br><br><br>";
// // print_r($subject);
// // echo "<br><br><br>";
// // print_r($content);
// // echo "<br><br><br>";
// // exit;
// 			$config = db::get_record("configuration","id","1");
// 			$mail = new PHPMailer;
// 			$mail->isSMTP();

// 			// $mail->SMTPDebug = 2;
// 			// // Ask for HTML-friendly debug output

// 			// $mail->SMTPAuth 	= $app['smtp_auth'];
// 			// $mail->SMTPAuth 	= TRUE;
// 			// if($app['smtp_auth']){
// 			// 	// sets the prefix to the servier
// 			// 	$mail->Username 	= $app['smtp_user'];
// 			// 	$mail->Password 	= $app['smtp_pass'];
// 			// }
// 			// $mail->Host 		= $app['smtp_host'];
// 			// $mail->Port 		= $app['smtp_port'];
// 			// $mail->SMTPSecure 	= false;
// 			// print_r($config);
// 			/*echo $config['smtp_host'];
// 			echo "<br>";
// 			echo $config['email_send'];
// 			echo "<br>";
// 			echo $config['smtp_pass'];
// 			echo "<br>";
// 			echo $config['smtp_port'];
// 			echo "<br>";
// 			echo $config['smtp_name'];
// 			echo "<br>";
// 			echo $subject;
// 			echo "<br>";
// 			echo $content;
// 			echo "<br>";*/
// 			// $mail->SMTPSecure = "ssl";
// 			$mail->SMTPAuth = true;
// 			$mail->Host 		= "smtp.gmail.com";
// 			$mail->Username 	= "sam.developer009@gmail.com";
// 			$mail->Password 	= "WoWandAHA";
// 			$mail->Port 		= "587";
// 			// $mail->SMTPDebug = 2;
// 			$mail->SMTPSecure = "tls";
// 			// $mail->SMTPAutoTLS = true;
// // exit;
// 			// echo ;
// 			$mail->setFrom("sam.developer009@gmail.com", "sam Test");
// 			// foreach($p_email as $mailto)
// 			// {
// 			// echo $p_email;
// 			// exit;
// 			// echo $p_email;
// 			$mail->AddAddress($p_email);
// 				// $mail->addAddress("alid.naruto@gmail.com");
// 			// }
// 			// $mail->addAddress($p_email);
// 			// $mail->addReplyTo($config['email_send'], $config['smtp_name']);
// 			$mail->isHTML(true); // Set email format to HTML

// 			$mail->Subject = $subject;
// 			$mail->Body    = $content;
// /*			if(count($path_file) > 0)
// 			{
// 				foreach($path_file AS $file){*/
// 		// $myfile = fopen("zxcasd.txt", "w") or die("Unable to open file!");
// 		// fwrite($myfile, "Khalid : ". $path_file);
// 		// fclose($myfile);
// 					// $mail->addAttachment($path_file);
// 			// 	}
// 			// }

// 			//send the message, check for errors
// 			if ($mail->send()) {

// 				// $myfile = fopen("zxcasd.txt", "w") or die("Unable to open file!");
// 				// fwrite($myfile, "mail gagal : ");
// 				// fclose($myfile);
// 				return true;
				
// 				// return "Mailer Error: " . $mail->ErrorInfo;
// 				// echo "Mailer Error: " . $mail->ErrorInfo;
// 			} else {
// 				// $myfile = fopen("zxcasd.txt", "w") or die("Unable to open file!");
// 				// fwrite($myfile, "mail berhasil : ");
// 				// fclose($myfile);
// 				// return true;
// 				echo $mail->ErrorInfo;
// 				return false;
// 		// $myfile = fopen("zxcasd.txt", "w") or die("Unable to open file!");
// 		// fwrite($myfile, "error : ". $mail->ErrorInfo);
// 		// fclose($myfile);
// 			}
// /*
// 		$headers = 'From:' ."sam.developer009@gmail.com";
// 		$msg = "First line of text\nSecond line of text";

// 		// use wordwrap() if lines are longer than 70 characters
// 		$msg = wordwrap($msg,70);

// 		// send email
// 		// mail("sam.developer009@gmail.com","Testing 234",$msg);
// 		if (mail("sam.developer009@gmail.com","Testing",$msg)) {
// 			// echo "berhasil";
// 			return true;
// 		}else{
// 			// echo "gagal";
// 			return false;
// 		}*/
// 	}
	public static function sendmail2($p_email, $subject = null, $content, $path_file = null){
		return true;
			global $app;
			
			//use PHPMailer\PHPMailer\PHPMailer;
			//use PHPMailer\PHPMailer\Exception;
			$mail = new PHPMailer(true);

			//$config = db::get_record("configuration","1","1");
        
            ## CMWI SMTP ##
            $config['smtp_host'] = "smtp.office365.com";
            $config['smtp_user'] = "ForGi_systems@cmwi.co.id";
            $config['smtp_pass'] = "forg12021!";
            $config['ssl'] = "tls";
            $config['smtp_port'] = "587";
            
			try {
			    //Server settings
			    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;      
				$mail->CharSet  ="utf-8";                             // Enable verbose debug output
			    $mail->isSMTP();                                      // Set the SMTP server to send 
			    $mail->Host       = $config['smtp_host'];   
			    $mail->SMTPAuth   = true;                              
			    $mail->Username   = $config['smtp_user'];                    // SMTP username
			    $mail->Password   = $config['smtp_pass'];                    // SMTP password
			    $mail->SMTPSecure = $config['ssl'];     
			    $mail->Port       = $config['smtp_port'];   
                
                //Recipients
			    $mail->setFrom($config['smtp_user'], 'ForgiSystem');
			    $mail->addAddress($p_email);
			    // $mail->addAddress('sam.developer009@gmail.com', 'Reciver_2');
			    // $mail->addAddress('dm@cmwi.co.id', 'Reciver_3');     // Add a recipient
			    // $mail->addAddress('ellen@example.com');               // Name is optional
			    // $mail->addReplyTo('info@example.com', 'Information');
			    // $mail->addCC('cc@example.com');
			    // $mail->addBCC('bcc@example.com');

			    // Attachments
			    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			    // Content
			    $mail->isHTML(true);                                   // Set email format to HTML
			    $mail->Subject = $subject;
			    $mail->Body    = $content;
			    $mail->AltBody = $content;
                // $mail->SMTPDebug = 2;
			    $mail->send();
                
			    // echo 'Message has been sent';
			} catch (Exception $e) {
			    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			}
			
	}
	public static function response($code, $params = []){
        global $app;
        $status = [
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            204 => 'No Content',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            411 => 'Length Required',
            500 => 'Internal Server Error'
        ];

        header("HTTP/1.1 ". $code ." ". $status[$code]);
        header('Content-type: application/json');

        echo json_encode($params);
    }
}
?>
