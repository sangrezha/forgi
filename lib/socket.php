<?php
    use ElephantIO\Client;
    use ElephantIO\Engine\SocketIO\Version2X;
    
    class Socket extends App{
        static function emit($to, $args = []) {
            global $app;
            try
            {
                $client = new Client(new Version2X($app['socket_connection'], []));
                $client->initialize();
                $client->emit($to, $args);
                $client->close();
            }
            catch (ServerConnectionFailureException $e)
            {
                echo 'Server Connection Failure!!';
            }
        }
    }
 ?>
