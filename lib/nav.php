<?php

class nav {
	var $max_page = 10;
    var $page_text = "";
    var $out_of_text = "";
    var $page_offset_var = "page.offset";
    var $page_total_var = "page.total";
    var $page_size_var = "page.size";
    var $first_text = "first";
    var $last_text = "last";
    var $prev_text = "prev";
    var $next_text = "next";
    var $page_offset = 0;
    var $page_total = 0;
    var $param = "";
    var $url = "";
    var $navbar = "";
    
    function init($offset, $total, $param="") {
		global $app;
		if(stristr($param,"http://")):
		$select_url = str_replace("http://","",$param);
		$script = explode('/',$select_url);
		$filePhp = explode('?',$script[1]);
		//$this->url = "http://".$script[0]."/".$filePhp[0]."/".$filePhp[1];
		$this->url = $app['http']."/";
		else:
		$script = @explode('/', $_SERVER["PHP_SELF"]);
		$this->url = $script[@count($script) - 1];
		endif;
		$this->page_offset = $offset;
		$this->page_total = $total;
		// $this->page_text = $app[lang][txt][page_text];
		// $this->out_of_text = $app[lang][txt][out_of_text];
		// $this->attr = $attr;
		$this->page_text = "";
		$this->out_of_text = "";
		$this->attr = "";
		$final_attr="";
		if(isset($attrPls)){
			
			if(stristr($attrPls,";")):
				$exp_attr = explode(";",$attrPls);
				for($i=0;$i<count($exp_attr);$i++){
					$exp_v = explode(":",$exp_attr[$i]);
						$final_attr.=$exp_v[0]."='".$exp_v[1]."'\n";
				}
			else:
				$exp_attr = explode(":",$attrPls);
				$final_attr.=$exp_attr[0]."='".$exp_attr[1]."'\n";
			endif;
		}
				//print_r($final_attr);
		$this->attrPls = $final_attr;
    }
	
	function get_offset() {
		return $this->page_offset; 
	}
    
	function build($size) {
		$this->page_size = $size;
		$offset = $this->page_offset;
		$length = $this->page_total;
		if ($length <= $size):
			$this->navbar = "";
			return;
		endif;
		
		/*if(stristr($this->param,"http://")):
			$sel_par = explode('?',$this->param);
			$this->param = "&".$sel_par[1];
		endif;
		
		if($this->attr == ""):
			$this->attr = "href";
		endif;*/
		
        //$pref = (stristr($this->url,"\?")) ? "&" : "?";
        $pref = "&";
	    $out = '';
	    if ($offset > 0):
	        $out .= "<a style='cursor:pointer;' href='" . $this->param . $pref . $this->page_offset_var . "=" . 0 . "&". $this->page_total_var . "=" . $length . "&". $this->page_size_var . "=" . $size . "' class='btn btn-default'>". $this->first_text ."</a>\n";
	        $out .= "<a style='cursor:pointer;' href='" . $this->param . $pref . $this->page_offset_var . "=" . ($offset - $size) . "&". $this->page_total_var . "=" . $length . "&". $this->page_size_var . "=" . $size . "' class='btn btn-default'>". $this->prev_text ."</a>\n";
        else:
            $out .= "";
	    endif;

	    $radius = floor($this->max_page / 2 * $size);
	    if ($offset < $radius):
	        $start = 0;
	    elseif ($offset < $length - $radius):
	        $start = $offset - $radius;
	    else:
	        $start = (floor($length / $size) - $this->max_page) * $size + $size;
            if ($start < 0):
                $start = $size;
            endif;
	    endif;

		for ($i = $start; (($i < $length) && ($i <= $start + $this->max_page * $size)); $i += $size):
	        if ($i == $offset):
	            $out .= "<a style='cursor:pointer;' href='" . $this->param . $pref . $this->page_offset_var . "=" . $i . "&" . $this->page_total_var . "=" . $length . "&". $this->page_size_var . "=" . $size . "' class='btn btn-primary active'>" . ($i / $size + 1) . "</a>\n";
	        else:
	            $out .= "<a style='cursor:pointer;' href='" . $this->param . $pref . $this->page_offset_var . "=" . $i . "&" . $this->page_total_var . "=" . $length . "&". $this->page_size_var . "=" . $size . "' class='btn btn-primary text'>" . ($i / $size + 1) . "</a>\n";
	        endif;
	    endfor;
	    if ($offset < $length - $size):
	        $out .= "<a style='cursor:pointer;' href='" . $this->param . $pref . $this->page_offset_var . "=" . ($offset + $size) . "&" . $this->page_total_var . "=" . $length . "&". $this->page_size_var . "=" . $size . "' class='btn btn-default'>". $this->next_text ."</a>\n";
	        //$out .= "<a href='" . $this->url . $pref . $this->page_offset_var . "=" . (floor($length/$size) - 1) * $size . "&". $this->page_total_var . "=" . $length . "&". $this->page_size_var . "=" . $size . $this->param . "'>". $this->last_text ."</a>\n";
			$out .= "<a style='cursor:pointer;' href='" . $this->param . $pref . $this->page_offset_var . "=" . (ceil($length/$size) - 1) * $size . "&". $this->page_total_var . "=" . $length . "&". $this->page_size_var . "=" . $size . "' class='btn btn-default'>". $this->last_text ."</a>\n";
	    endif;

        $this->navbar = $out.'<br>';
	}
	
	function navbar() {
		return $this->navbar;
	}

	function navbar_text() {
		global $app;
		if ($this->navbar):
			$out = "{$app[lang][txt][page]} " . ($this->page_offset / $this->page_size + 1) . " {$app[lang][txt][out_of]} " . ceil($this->page_total / $this->page_size) . "&nbsp;";
		endif;
		return $out;
	}

	function up(){
		global $app;
		$img = "<img src=\"{$app[www]}/img/up.gif\" align=\"right\"  onClick=\"javasript:location.replace('#')\" title=\"To Upper Line\">";
		return $img;
	}
}

?>