<?php

/*******************************************************************************
* Filename : config.php
* Description : configuration
*******************************************************************************/
## DEBUG ##
$app['debug'] = 0;

## MAGIC QUOTES ON/OFF ##
$app['magic_quote'] = 1;

$app['rest']['status'] = [
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        204 => 'No Content',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        411 => 'Length Required',
        500 => 'Internal Server Error'
];

$app['plan'] = [
        'silver'=>'2',
        'premium'=>'10',
        'gold'=>'n'
];

$app['result_step'] = [
        'trash','lost','won','other'
];

$app['quarter'] = [
        'Q1'=>[
                'start'=>'01',
                'end'=>'03'
        ],
        'Q2'=>[
                'start'=>'04',
                'end'=>'06'
        ],
        'Q3'=>[
                'start'=>'07',
                'end'=>'09'
        ],
        'Q4'=>[
                'start'=>'10',
                'end'=>'12'
        ]
];