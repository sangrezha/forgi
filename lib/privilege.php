<?php
class privilege{
	function init(){
		return array(
			array(
				"ico"		=>"fa-key",
				"level"		=>"9",
				"title"		=>"Admin",
				"items"=>array(
					array(
						"status"	=>true,
						"level"		=>"9",
						"system"	=>"user",
						"title"		=>"User",
						"acc" 		=> array('View'=>'DSPL', 'Create'=>'CRT', 'Update'=>'UPDT', 'Delete'=>'DEL', 'Change Password'=>'CP')
					),
					/*array(
						"status"	=>true,
						"level"		=>"9",
						"system"	=>"rule",
						"title"		=>"Rule",
						"acc" 		=> array('View'=>'DSPL', 'Create'=>'CRT', 'Update'=>'UPDT', 'Delete'=>'DEL')
					)*/
				)
			),
			// array(
				// "ico"		=>"fa fa-address-book-o",
				// "title"		=>"Master User",
				// "items"=>array(
					
				// )
			// ),
			array(
				"ico"		=>"fa fa-database",
				"level"		=>"4",
				"title"		=>"Master User",
				"items"=>array(
					array(
						"status" => true,
						"module" => "member",
						"level"		=>"4",
						"max_level"	=>"4",
						"title" => "Member",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"level"	=>"4",
						"max_level"	=>"4",
						"module" => "title",
						"title" => "Title",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"level"	=>"4",
						"max_level"	=>"4",
						"module" => "section",
						"title" => "Section",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"level"	=>"4",
						"max_level"	=>"4",
						"module" => "departement",
						"title" => "Departement",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					)
				)
			),
			array(
				"ico"		=>"fa fa-database",
				"level"		=>"4",
				"code_user" => "it",
				"title"		=>"Master Data",
				"items"=>array(
					array(
						"status" => true,
						"level"	=>"4",
						"max_level"	=>"4",
						"module" => "cost_centre",
						"title" => "Cost Centre",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"module" => "kategori",
						"level"	=>"4",
						"max_level"	=>"4",
						"title" => "Kategori",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"module" => "sistem_informasi",
						"title" => "Infrastruktur",
						"level"	=>"4",
						"max_level"	=>"4",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					/*array(
						"status" => true,
						"module" => "template_dokumen",
						"title" => "Template no dokumen(Digital Document)",
						"level"	=>"4",
						"max_level"	=>"4",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),*/
				)
			),
			// array(
			// 	"ico"		=>"fa fa-archive",
			// 	"title"		=>"Status Pengerjaan",
			// 	"items"=>array(
			// 		array(
			// 			"status" => true,
			// 			"module" => "status",
			// 			"title" => "Status",
			// 			"acc" => array(
			// 				"View" => "DSPL",
			// 				"Create" => "CRT",
			// 				"Update" => "UPDT",
			// 				"Delete" => "DEL"

			// 		)
			// 	)
			// )
			// ),
			array(
				"ico"		=>"fa fa-archive",
				"level"		=>"1",
				"title"		=>"Form",
				"items"=>array(
					// array(
					// 	"status" => true,
					// 	"module" => "container_schedule",
					// 	"title" => "Container Schedule",
					// 	"acc" => array(
					// 		"View" => "DSPL",
					// 		"Update" => "UPDT",
					// 	)
					// ),form_cpassword
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"3",
						"module" => "form_hardware_software",
						// "title" => "Form pengajuan Hardware & Software",
						"title"=>"Permintaan Hardware & Software",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"3",
						"module" => "form_abnormal_si",
						"title" => "Informasi abnormal sistem informasi",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"3",
						"module" => "form_tmp_access",
						"it_no_add" => "yes",
						"title" => "Pengajuan temporary access",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"3",
						"module" => "form_perubahan_si",
						"title" => "Permintaan perubahan sistem informasi",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"2",
						"max_level"	=>"4",
						"module" => "form_pengajuan_user",
						"title" => "Pengajuan user komputer",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"3",
						"module" => "form_acc_folder",
						"it_no_add" => "yes",
						"title" => "Permintaan akses folder File Sharing Server",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"3",
						"module" => "form_get_soft",
						"title" => "Memasukkan/mengeluarkan data softcopy",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"5",
						"module" => "form_register_device",
						"title" => "Registrasi perangkat penyimpanan data",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"3",
						"module" => "form_cpassword",
						"it_no_add" => "yes",
						"title" => "Pengajuan penggantian password",
						"acc" => array(
							"no_it" => "yes",
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					// array(
					// 	"status" => true,
					// 	"level"	=>"1",
					// 	"module" => "form_perubahan_si",
					// 	"title" => "Form Perubahan SI",
					// 	"acc" => array(
					// 		"View" => "DSPL",
					// 		"Create" => "CRT",
					// 		"Update" => "UPDT",
					// 		"Delete" => "DEL"
					// 		// ,
					// 		// "Import view"=>"APPR",
					// 		// "Shipping view" => "ASV",
					// 		// "Truck View" => "ATV"
					// 	)
					// ),
					array(
						"status" => true,
						"level"	=>"1",
						"max_level"	=>"3",
						"module" => "form_recovery_data",
						"title" => "Permintaan recovery data",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					)
				)
			),
			// array(
			// 	"ico"		=>"fa fa-ship",
			// 	"title"		=>"Shipping",
			// 	"items"=>array(
			// 		array(
			// 			"status" => true,
			// 			"module" => "delivery_order",
			// 			"title" => "Delivery Order",
			// 			"acc" => array(
			// 				"View" => "DSPL",
			// 				"Create" => "CRT",
			// 				"Update" => "UPDT",
			// 				"Delete" => "DEL"
			// 			)
			// 		)
			// 	)
			// ),
/* 			array(
				"ico"		=>"fa fa-truck",
				"title"		=>"Delivery",
				"items"=>array(
					array(
						"status" => true,
						"module" => "trucking",
						"title" => "Trucking",
						"acc" => array(
							"View" => "DSPL",
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							"Truck View" => "ATV",
							"Delete" => "DEL"
							// "Update" => "UPDT"
						)
					),
					array(
						"status" => true,
						"module" => "return_container",
						"title" => "Return Container",
						"acc" => array(
							"View" => "DSPL",
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							"Truck View" => "ATV",
							"Delete" => "DEL"
							// "Update" => "UPDT"
						)
					)
				)
			), */
			/////////////////////////////////////
			array(
				"ico"		=>"fa fa-file-text",
				"level"		=>"1",
				"code_user" => "all",
				"title"		=>"Digital Document",
				"items"=>array(
					array(
						"status"	=>true,
						"module"	=>"module_file",
						"level"		=>"1",
						"max_level"	=>"5",
						"title"		=>"List",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status"	=>true,
						"module"	=>"de_module_file",
						"level"		=>"5",
						"max_level"	=>"5",
						"code_user" => "it",
						"title"		=>"Deactive Document",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					)
				)
			),
			///////////////////////////////////
          //  array(
			//	"ico"		=>"fa fa-file-text",
			//	"level"		=>"4",
			//	"code_user" => "all",
			//	"status" => true,
			//	"acc" => array(
			//		"View" => "DSPL",
			//		"Create" => "CRT",
			//		"Update" => "UPDT",
			//		"Delete" => "DEL"
			//	),
			//	"module" 	=> "module_file",
			//	"title"		=>"Digital Document"
				/*"items"=>array(
					array(
						"ico"    =>"fa fa-file-text",
						"status" => true,
						"module" => "module_file",
						"level"		=>"4",
						"max_level"	=>"4",
						"title" => "Module File",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
				)*/
			//),

			/////////////////////////////////
            
			array(
				"ico"		=>"fa fa-bar-chart",
				"level"		=>"4",
				"title"		=>"Report",
				"code_user" => "it",
				"items"=>array(
					array(
						"status" => true,
						"module" => "report_form",
						"level"		=>"4",
						"max_level"	=>"5",
						"title" => "Form",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"module" => "report_document",
						"level"		=>"4",
						"max_level"	=>"5",
						"title" => "Digital Document",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"module" => "log_user",
						"level"		=>"4",
						"max_level"	=>"5",
						"title" => "Log User",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					)
				)
			),
			
			array(
				"status"	=>false,
				"title"		=>"Others Menu",
				"items"=>array(
					// array(
						// "status" => false,
						// "system" => "lang",
						// "title" => "Language",
						// "acc" => array(
							// 'View'=>'DSPL', 
							// 'Create'=>'CRT', 
							// 'Update'=>'UPDT', 
							// 'Delete'=>'DEL'
						// )
					// ),
					// array(
						// "status" => false,
						// "system"	=>"i18n",
						// "title"		=>"i18n (System Library Language)",
						// "acc" => array(
							// 'View'=>'DSPL', 
							// 'Create'=>'CRT', 
							// 'Update'=>'UPDT', 
							// 'Delete'=>'DEL'
						// )
					// ),
					array(
						"status" => false,
						"system"	=>"preference",
						"title"		=>"Preference",
						"acc" => array(
							'View'=>'DSPL',
							'Update'=>'UPDT'
						)
					)
				)
			)
		);
	}
}
?>
