<?php

/*******************************************************************************
* Filename : form.php
* Description : form library
*******************************************************************************/

class form
{
    /***************************************************************************
    * Description : init
    ***************************************************************************/
	public static function init()
	{
		global $error;
		form::set_error(0);
		if (!$error):
			form::reset();
		endif;
	}

    /***************************************************************************
    * Description : is error
    ***************************************************************************/
	public static function is_error()
	{
		if (isset($_SESSION['error_flag']) AND $_SESSION['error_flag']):
			return TRUE;
		endif;
		return FALSE;
	}

    /***************************************************************************
    * Description : set_error
    ***************************************************************************/
	public static function set_error($flag)
	{
		$_SESSION['error_flag'] = $flag;
	}

    /***************************************************************************
    * Description : reset
    ***************************************************************************/
	public static function reset()
	{
		$_SESSION['error_flag'] = 0;
		$_SESSION['form_value'] = '';
		$_SESSION['error_msg'] = array();
	}

    /***************************************************************************
    * Description : bundle form value
    ***************************************************************************/
    public static function serialize_form()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET'):
            $_SESSION['form_value'] = app::serialize64($_GET);
        else:
            $_SESSION['form_value'] = app::serialize64($_POST);
        endif;
    }

    /***************************************************************************
    * Description : unbundle form value
    ***************************************************************************/
    public static function unserialize_form()
    {
        $frm = unserialize64($_SESSION['form_value']);
        return $frm;
    }

    /***************************************************************************
    * Description : populate form
    ***************************************************************************/
    public static function populate(&$form)
    {
		global $error;
		if (form::is_error() || $error):
        	$frm = form::get_value();
			while (list($k, $v) = @each($frm)):
				if (in_array($k , array('act', 'step', 'referer'))):
					$field = $k;
				else:
					$field = substr($k, 2);
				endif;
				$form[$field] = $v;
			endwhile;
		endif;
    }

    /***************************************************************************
    * Description : get value
    ***************************************************************************/
    public static function get_value()
    {
        return app::unserialize64($_SESSION['form_value']);
    }
    /***************************************************************************
    * Description : validate form
    ***************************************************************************/
    public static function validate($type, $fields, $param = '')
    {
        global $app;
        $fields = "\$".str_replace(",", ",\$", $fields);
        eval("global $fields;");
        $arr = explode(",", $fields);

        if ($type == 'empty'):
            while (list($k, $v) = each($arr)):
                $field = substr($v, 3);
                $cmd = "\$v = $v;";
                eval($cmd);
                if (!trim($v)):
					msg::set_message($field, app::getliblang($field) .' '. app::getliblang('require'));
                    $_SESSION['error_flag'] = 1;
                endif;
            endwhile;
        endif;
        if ($type == ''):
            while (list($k, $v) = each($arr)):
                $field = substr($v, 3);
                $cmd = "\$v = $v;";
                eval($cmd);
                if (!trim($v)):
					msg::set_msg( app::getliblang($field) .' '. app::getliblang('require'));
                    $_SESSION['error_flag'] = 1;
                endif;
            endwhile;
        endif;
        if ($type == 'checkbox'):
            while (list($k, $v) = each($arr)):
                $field = substr($v, 3);
                $cmd = "\$v = $v;";
                eval($cmd);
                if (!@count($v)):
					msg::set_msg($app['lang'][$field] ." ". $app['lang']['require_checkbox']);
                    $_SESSION['error_flag'] = 1;
                endif;
            endwhile;
		endif;
        if ($type == 'select'):
			while (list($k, $v) = each($arr)):
                $field = substr($v, 3);
				eval("\$v = $v;");
				if (!trim($v)):
					msg::set_msg($app['lang'][$field] ." ". $app['lang']['require_select']);
                    $_SESSION['error_flag'] = 1;
				endif;
			endwhile;
        endif;
        if ($type == 'email'):
			while (list($k, $v) = each($arr)):
                $field = substr($v, 3);
				eval("\$v = $v;");
				if (!preg_match("^(.+)@(.+)\\.(.+)$^", $v)):
					//msg::set_msg($app['lang'][$field] ." ". $app['lang']['require_email']);
					msg::set_message($field, app::getliblang($field) .' '. app::getliblang('require_format'));
                    $_SESSION['error_flag'] = 1;
				endif;
			endwhile;
        endif;
        if ($type == 'date'):
			while (list($k, $v) = each($arr)):
                $field = substr($v, 3);
				eval("\$v = $v;");
				list($year, $month, $date) = explode('-', $v);
				if (!checkdate($month, $day, $year)):
					msg::set_msg($app['lang'][$field] ." ". $app['lang']['require_select']);
                    $_SESSION['error_flag'] = 1;
				endif;
			endwhile;
        endif;
        if ($type == 'image'):
			while (list($k, $v) = each($arr)):
                $field = substr($v, 3);
				$var = substr($v, 1);
				eval("\$v = $v;");

				list($file_max_size, $min_width, $max_width, $min_height, $max_height) = explode('|', $param);

				$file['tmp_name'] = $_FILES[$var]['tmp_name'];
				$file['name'] = $_FILES[$var]['name'];
				$file['size'] = $_FILES[$var]['size'];

				if ($file['size'] > 0):
					$pict = getimagesize($file['tmp_name']);
					//print_r($pict);exit;
					/*if (!(($pict[2] == 1) || ($pict[2] == 2) || ($pict[2] == 13))):
						$error = 'ERR_TYPE';
						if ($error):
							msg::set_msg("{$app[lang][field][$field]} {$app[lang][error]['image.'.$error]}");
							$_SESSION[error_flag] = 1;
						endif;
					endif;*/
					if (($pict[0] < $min_width) || ($pict[0] > $max_width) || ($pict[1] < $min_height) || ($pict[1] > $max_height)):
						$error = 'ERR_WIDTH';
						if ($error):
							msg::set_msg($app['lang'][$field] ." ". $app['lang']['image.'. $error]);
							$_SESSION['error_flag'] = 1;
						endif;
					endif;
					if ($file[size] > ($file_max_size * 1024)):
						$error = 'ERR_SIZE';
						if ($error):
							msg::set_msg("".$app['lang'][$field]." ".$app['lang']['image.'.$error]."");
							$_SESSION['error_flag'] = 1;
						endif;
					endif;
				endif;
			endwhile;
        endif;
		if ($type == 'file'):
			while (list($k, $v) = each($arr)):
                $field = substr($v, 3);
				$var = substr($v, 1);
				eval("\$v = $v;");

				list($file_max_size) = explode('|', $param);

				$file['tmp_name'] = $_FILES[$var]['tmp_name'];
				$file['name'] = $_FILES[$var]['name'];
				$file['size'] = $_FILES[$var]['size'];

				if ($file['size'] > 0):
					$pict = getimagesize($file['tmp_name']);
					if ($file['size'] > ($file_max_size * 1024)):
						$error = 'ERR_SIZE';
						if ($error):
							msg::set_msg($app['lang']['file'. $error]);
							$_SESSION['error_flag'] = 1;
						endif;
					endif;
				endif;
			endwhile;
        endif;
    }

	public static function validate_count_char($fields, $length)
    {
        global $app;
        $fields = "\$".str_replace(",", ",\$", $fields);
        eval("global $fields;");
        $arr = explode(",", $fields);
        while (list($k, $v) = each($arr)):
			$field = substr($v, 3);
			$cmd = "\$v = $v;";
			eval($cmd);
			$char = strlen($v);
			//$char = count_chars($v);
			if ($char > $length):
				msg::set_msg($app['lang'][$field] ." ". $app['lang']['char'] ." Your $field : $char characters." );
				$_SESSION['error_flag'] = 1;
			endif;
       endwhile;
	}

	public static function editor(){
		global $app,$GLOBALS;
		foreach($GLOBALS as $k=> $v){
			$$k=$v;
		}
		$fcomp = $app['component'] ."/editor.cmp";
		if($name AND file_exists($fcomp))
		{
			$component = file_get_contents($fcomp);
			echo $component;
		}
	}
}

?>
