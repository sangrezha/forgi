<?php
class privilege{
	function init(){
		return array(
			array(
				"ico"		=>"fa-key",
				"level"		=>"9",
				"title"		=>"Admin",
				"items"=>array(
					array(
						"status"	=>true,
						"level"		=>"9",
						"system"	=>"user",
						"title"		=>"User",
						"acc" 		=> array('View'=>'DSPL', 'Create'=>'CRT', 'Update'=>'UPDT', 'Delete'=>'DEL', 'Change Password'=>'CP')
					),
					array(
						"status"	=>true,
						"level"		=>"9",
						"system"	=>"rule",
						"title"		=>"Rule",
						"acc" 		=> array('View'=>'DSPL', 'Create'=>'CRT', 'Update'=>'UPDT', 'Delete'=>'DEL')
					)
				)
			),
			// array(
				// "ico"		=>"fa fa-address-book-o",
				// "title"		=>"Master User",
				// "items"=>array(
					
				// )
			// ),
			array(
				"ico"		=>"fa fa-database",
				"level"		=>"3",
				"title"		=>"Master User",
				"items"=>array(
					array(
						"status" => true,
						"module" => "member",
						"level"		=>"3",
						"title" => "Member",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"level"	=>"4",
						"module" => "title",
						"title" => "Title",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"level"	=>"4",
						"module" => "section",
						"title" => "Section",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"level"	=>"4",
						"module" => "departement",
						"title" => "Departement",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					)
				)
			),
			array(
				"ico"		=>"fa fa-database",
				"level"		=>"1",
				"title"		=>"Master Data",
				"items"=>array(
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "cost_centre",
						"title" => "Cost Centre",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"module" => "kategori",
						"level"	=>"1",
						"title" => "Kategori",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
					array(
						"status" => true,
						"module" => "sistem_informasi",
						"title" => "Sistem Informasi",
						"level"	=>"1",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
						)
					),
				)
			),
			// array(
			// 	"ico"		=>"fa fa-archive",
			// 	"title"		=>"Status Pengerjaan",
			// 	"items"=>array(
			// 		array(
			// 			"status" => true,
			// 			"module" => "status",
			// 			"title" => "Status",
			// 			"acc" => array(
			// 				"View" => "DSPL",
			// 				"Create" => "CRT",
			// 				"Update" => "UPDT",
			// 				"Delete" => "DEL"

			// 		)
			// 	)
			// )
			// ),
			array(
				"ico"		=>"fa fa-archive",
				"level"		=>"1",
				"title"		=>"Form",
				"items"=>array(
					// array(
					// 	"status" => true,
					// 	"module" => "container_schedule",
					// 	"title" => "Container Schedule",
					// 	"acc" => array(
					// 		"View" => "DSPL",
					// 		"Update" => "UPDT",
					// 	)
					// ),form_cpassword
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_cpassword",
						"it_no_add" => "yes",
						"title" => "Form Change Password",
						"acc" => array(
							"no_it" => "yes",
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_acc_folder",
						"title" => "Form ACC Folder",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_tmp_access",
						"title" => "Form Tmp Access",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_perubahan_si",
						"title" => "Form perubahan SI",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_abnormal_si",
						"title" => "Form abnormal SI",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_get_soft",
						"title" => "Form mengeluarkan data softcopy perusahaan",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_recovery_data",
						"title" => "Form recovery data",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_hardware_software",
						"title" => "Form pengajuan Hardwere & Softwere",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_pengajuan_user",
						"title" => "Form pengajuan user",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					),
					array(
						"status" => true,
						"level"	=>"1",
						"module" => "form_register_device",
						"title" => "Form register devices",
						"acc" => array(
							"View" => "DSPL",
							"Create" => "CRT",
							"Update" => "UPDT",
							"Delete" => "DEL"
							// ,
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							// "Truck View" => "ATV"
						)
					)
					// 
				)
			),
			// array(
			// 	"ico"		=>"fa fa-ship",
			// 	"title"		=>"Shipping",
			// 	"items"=>array(
			// 		array(
			// 			"status" => true,
			// 			"module" => "delivery_order",
			// 			"title" => "Delivery Order",
			// 			"acc" => array(
			// 				"View" => "DSPL",
			// 				"Create" => "CRT",
			// 				"Update" => "UPDT",
			// 				"Delete" => "DEL"
			// 			)
			// 		)
			// 	)
			// ),
/* 			array(
				"ico"		=>"fa fa-truck",
				"title"		=>"Delivery",
				"items"=>array(
					array(
						"status" => true,
						"module" => "trucking",
						"title" => "Trucking",
						"acc" => array(
							"View" => "DSPL",
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							"Truck View" => "ATV",
							"Delete" => "DEL"
							// "Update" => "UPDT"
						)
					),
					array(
						"status" => true,
						"module" => "return_container",
						"title" => "Return Container",
						"acc" => array(
							"View" => "DSPL",
							// "Import view"=>"APPR",
							// "Shipping view" => "ASV",
							"Truck View" => "ATV",
							"Delete" => "DEL"
							// "Update" => "UPDT"
						)
					)
				)
			), */
			array(
				"status"	=>false,
				"title"		=>"Others Menu",
				"items"=>array(
					// array(
						// "status" => false,
						// "system" => "lang",
						// "title" => "Language",
						// "acc" => array(
							// 'View'=>'DSPL', 
							// 'Create'=>'CRT', 
							// 'Update'=>'UPDT', 
							// 'Delete'=>'DEL'
						// )
					// ),
					// array(
						// "status" => false,
						// "system"	=>"i18n",
						// "title"		=>"i18n (System Library Language)",
						// "acc" => array(
							// 'View'=>'DSPL', 
							// 'Create'=>'CRT', 
							// 'Update'=>'UPDT', 
							// 'Delete'=>'DEL'
						// )
					// ),
					array(
						"status" => false,
						"system"	=>"preference",
						"title"		=>"Preference",
						"acc" => array(
							'View'=>'DSPL',
							'Update'=>'UPDT'
						)
					)
				)
			)
		);
	}
}
?>
