<?php
/*******************************************************************************
* Filename : admlib.php
* Description : admin library
*******************************************************************************/
class admlib
{
	public static $page_active = array();
  public static function display_block_header($forgot="")
	{
	    	global $app;
			$config = db::get_record("configuration", "id", 1);
			include $app['pwebmin'] ."/block/blk_header.php";
	}
	public static function display_pagination($nr, $total, $nav)
	{
	    global $app;
			include $app['pwebmin'] ."/include/blk_pagination.php";
	}
	public static function display_block_footer()
	{
	    global $app;
			$config = db::get_record("configuration", "id", 1);
	    include $app['pwebmin'] ."/block/blk_footer.php";
	}
	public static function display_block_menu()
	{
	    global $app;
			$privilege = privilege::init();
			$config = db::get_record("configuration", "id", 1);
			include $app['pwebmin'] ."/block/blk_menu.php";
	}

	// public static function display_block_grid($prefix="")
	public static function display_block_grid($prefix="",$search_date="",$custom_search="")
	{
	    global $app;
	    $success 	= msg::get_message('success');
	    $error 		= msg::get_message('error');
	    include $app['pwebmin'] ."/include/blk_grid$prefix.php";
	}
	public static function get_component($name, $param = null)
  {
			global $app;
			include $app['component'] ."/". $name .".html";
  }
  /***************************************************************************
  * Description : display msg
  * Parameters : $msg_title, $msg_content
  ***************************************************************************/
  public static function display_msg($msg_title=null, $msg_content=null, $msg_back=null)
  {
			global $app;
			include $app['pwebmin'] ."/dsp_msg.php";
			exit;
  }
	/*******************************************************************************
	* Description : validating admin
	* Parameters : $application
	* Variables : $_SESSION[ktisession] = admin session variables
	*******************************************************************************/
	public static function validate($acc = null)
	{
		global $app;
		if (!strlen($_SESSION[$app['session']])):
			header("location: " . $app['webmin']);
			exit;
		endif;
		$app['me'] 			= app::unserialize64($_SESSION[$app['session']]);
		//$rs['usr_adm']	= db::get_recordset("user","id", $me['id']);
		//$encr_ 					= app::serialize64(db::fetch($rs['usr_adm']));
		//$app['me'] 			= app::unserialize64($encr_);
		// app::$lang			= db::lookup('alias', 'lang', 'id', $app['me']['lang']);
		app::$lang			= "en";
		if( $acc )
		{
			if(isset(self::$page_active['module'])){
				$akses = self::$page_active['module'];
			}else{
				$akses = self::$page_active['system'];
			}
			if( $app['me']['id'] != 1)
			{
				// $_acc = explode(",", $acc);
				//$sqlacc = "SELECT srd.rule FROM ".$app['table']['rule_det']." srd LEFT JOIN ".$app['table']['user_det']." sud ON (srd.id_rule = sud.id_rule) WHERE sud.id_user = '". $app['me']['id'] ."' AND srd.module='". $akses ."' AND srd.rule IN ('". implode("','", $_acc) ."')";
				// db::query($sqlacc, $rs['ruleacc'], $nr['ruleacc ']);
				// $rowacc = db::fetch($rs['ruleacc']);
				// $chk_sub = $rowacc['rule'];
				if((!self::acc($acc, $akses)) && empty($app['me']['level']) )
				{
					header("location: error.do&ref=101");
					exit;
				}
			}
			// print_r($_REQUEST);
			// print_r($_POST);
			// print_r($_GET);
			// if((count($_REQUEST) > 0 && $acc != 'DSPL') || (count($_REQUEST) > 0 && $akses == "complaint") ){				
			// 	$sqllog="INSERT INTO ".$app['table']['user_activity']." (created_by, validate, module, act, data, created_at) VALUES ('".$app['me']['id']."', '".self::acc($acc, $akses)."', '". $akses .".do', '".$_REQUEST['act']."', '".addslashes(json_encode($_REQUEST))."', now() )";
			// 	db::qry($sqllog);
			// }
		}
	}
    /***************************************************************************
    * Description : display budget on left pane
    * Parameters : -
    ***************************************************************************/
	public static function array_is_associative ($array)
	{
		if ( is_array($array) && ! empty($array) )
		{
			for ( $iterator = count($array) - 1; $iterator; $iterator-- )
			{
				if ( ! array_key_exists($iterator, $array) ) { return true; }
			}
			return ! array_key_exists(0, $array);
		}
		return false;
	}
	public static function Strip($value)
	{
		if(get_magic_quotes_gpc() != 0)
		{
			if(is_array($value))
				if ( admlib::array_is_associative($value) )
				{
					foreach( $value as $k=>$v)
						$tmp_val[$k] = stripslashes($v);
					$value = $tmp_val;
				}
				else
					for($j = 0; $j < sizeof($value); $j++)
						$value[$j] = stripslashes($value[$j]);
			else
				$value = stripslashes($value);
		}
		return $value;
	}
	public static function create_file($filename,$content){
		$fh = fopen($filename , "w+");
		if($fh==false)
			die("unable to create file");

		if (is_writable($filename)) {
			 if (!$handle = fopen($filename, 'a')) {
				echo "Cannot open file ($filename)";
				exit;
			}
		if (fwrite($handle, $content) === FALSE) {
			echo "Cannot write to file ($filename)";
			exit;
		}
		fclose($handle);
		} else {
			 echo "The file $filename is not writable";
		}
	}
	public static function array_to_xml($data, &$xml_data) {
		foreach($data as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml_data->addChild($key);
					array_to_xml($value, $subnode);
				}else{
					$subnode = $xml_data->addChild("row");
					array_to_xml($value, $subnode);
				}
			}else{
				$xml_data->addChild($key,str_replace("\n","",htmlentities($value)));
			}
		}
		return $xml_data;
	}
	public static function getSrcImg($img){
		preg_match( '/src="([^"]*)"/i', $img, $array );
		$img = str_replace("data:image/png;base64,", "", $array[1]);
		$img2 = str_replace("data:image/jpg;base64,", "", $img);
		return $img2;
	}
	public static function init_file($acc, $name = "default", $data)
	{
		global $app;
		if($acc == 'json')
		{
			$file = $app['api_path']."/".$name.".json";
			$fh = fopen($file, 'w') or die("can't open file");
			fwrite($fh, json_encode($data));
			fclose($fh);
		}
		elseif($acc == 'xml')
		{
			$xml_data 	= new SimpleXMLElement("<?xml version=\"1.0\"  encoding=\"UTF-8\"?><data></data>");
			$xmlx 		= admlib::array_to_xml($dat,$xml_data);
			$file 		= $app['api_path']."/".$name .".xml";
			$fh 		= fopen($file, 'w') or die("can't open file");
			fwrite($fh, $xmlx->asXML());
			fclose($fh);
		}
	}
	public static function get_items($string , $status = "save"){
		preg_match_all('/\[items[^\]](style)=("[^"]*")*\](.*?)\[\/items\]/si',$string, $out);
		if($status == "view"){
			for($i = 2;$i < count($out);$i++){
				$item[] = $out[$i];
			}
			$combine = array_combine($item[1], $item[0]);
			$output = null;
			$property_ = array("b"=>"font-weight:bold","sz"=>"font-size","bg"=>"background:url()");
			foreach($combine as $k => $v){
				$exp1[$k] = explode(",",str_replace('"',"",$v));
				$style_[$k] .= "style='";
				foreach($exp1[$k] as $t[$k]){
					$t_ = $t[$k];
					$exp11[$k][$t_] = explode(":",$t_);
					switch($exp11[$k][$t_][0]){
						case "b":
							if($exp11[$k][$t_][1] == 1){
								$style_[$k] .= "font-weight:bold;";
							}else{
								$style_[$k] .= null;
							}
							break;
						case "u":
							if($exp11[$k][$t_][1] == 1){
								$style_[$k] .= "text-decoration:underline;";
							}else{
								$style_[$k] .= null;
							}
							break;
						case "i":
							if($exp11[$k][$t_][1] == 1){
								$style_[$k] .= "font-style:italic;";
							}else{
								$style_[$k] .= null;
							}
							break;
						case "cl":
							$style_[$k] .= "color:".$exp11[$k][$t_][1].";";
							break;
						case "fml":
							$style_[$k] .= "font-family:".$exp11[$k][$t_][1].";";
							break;
						case "sz":
							$style_[$k] .= "font-size:".$exp11[$k][$t_][1]."px;";
							break;
						case "bg":
							if($exp11[$k][$t_][1] == "trans"){
								$style_[$k] .= "background:url(images/bg_transparent.png);padding:10px;padding-bottom:25px;";
							}else{
								$style_[$k] .= "background:".$exp11[$k][$t_][1].";padding:10px;padding-bottom:25px;";
								//$style_[$k] .= null;
							}
							break;
						default:
							$style_[$k] .= null;
							break;
					}

				}
				$style_[$k] .= "'";
				$output .= "<div ".$style_[$k]."  >".$k."</div>";
			}
			return 	$output;
		}else{
			return 	$out[0];
		}
	}
	public static function set_src($content){
		global $app;
		return str_replace("images/",$app['layout_image']."/",$content);
	}
	public static function list_tree($xFilter = null, $xParent = 0 , $xDelimiter = "|--", $xSelected = null){
		global $app;
		$output = null;
		if(!empty($xFilter)) $filter = "AND `id` NOT IN (SELECT id_menu FROM ".$app['table'][$xFilter].")";
		else $filter = null;
		$sql = "SELECT * FROM ".$app['table']['page_menu']."
			WHERE id != 1 AND parent_id = '".$xParent."' AND published = 'active' $filter ORDER BY priority";
		db::query($sql, $rs['tree'], $nr['tree']);
		while ($row = db::fetch($rs['tree'])) {
			if($row['id'] == $xSelected and $xSelected != null)	$selected = "selected";
			else $selected = null;
			$output .= "<option value='".$row['id']."' {$selected}>".$xDelimiter.":".$row['title']."</option>";
			$output .= admlib::list_tree($xFilter ,$row['id'], $xDelimiter."|--", $xSelected);
		}
		return $output;
	}
	public static function generate_list($array, $parent = 0) {
		print "<ul>";
		foreach ($array as $row) {
			if ($row['parent_id'] == $parent) {
				print "<li>".$row['title'];
					return admlib::generate_list($array, $row['id']);  # recurse
				print "</li>";
			}
		}
		print "</ul>";
	}
	public static function init_log($name)
	{
		global $app;
		$chk = db::lookup('name','log','name',$name);
		if(!$chk)
			$sql = "INSERT INTO ".$app['table']['log']." (tipe,name,date) VALUES (3,'$name',now())";
		else
			$sql = "UPDATE ".$app['table']['log']." SET date=now() WHERE name='".$name."'";
		db::qry($sql);
	}
	public static function acc($role, $acc = null)
	{
		global $app;
		if(isset($app['me']['acc']) && $app['me']['acc'] == 'member' OR $app['me']['id'] == '1'){
	        //if((isset($app['me']['acc']) && $app['me']['acc'] == 'member') OR $app['me']['id'] == '1'){
			return $role;
		}else{
			if( $acc ){
				$module = $acc;
			}else{
				if(isset(self::$page_active['module'])){
					$module = self::$page_active['module'];
				}else{
					$module = self::$page_active['system'];
				}
			}
			$sql = "SELECT rule FROM ". $app['view']['rule'] ." WHERE user='". $app['me']['id'] ."' AND module='". $module ."' AND rule='". $role ."' GROUP BY module";
			// echo $sql."<br />";
			// $sql = "SELECT srd.rule FROM ".$app['table']['rule_det']." srd LEFT JOIN ".$app['table']['user_det']." sud ON (srd.id_rule = sud.id_rule) WHERE sud.id_user = '". $app['me']['id'] ."' AND srd.module = '". $module ."' AND srd.rule = '". $role ."' GROUP BY module";
			db::query($sql, $rs['rule'], $nr['rule']);
			$row = db::fetch($rs['rule']);
			return $row['rule'];
		}
	}
	public static function getext(){
		$module = self::$page_active['module'];
		if($module)
		{
			$mod = admlib::$page_active['module'] . ".mod";
		}
		else
		{
			$mod = admlib::$page_active['system'] . ".do";
		}
		return $mod;
	}
	public static function clean($string){
		return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string));
	}

	/* update multipleimage */
	public static function updatemultipleimage($mod, $place, $field, $table, $field_id, $id){
		global $app;

		$p_gallery_mod = $mod;
		if(count($p_gallery_mod) > 0){
			$rs['delete_gallery'] = db::get_record_select($field,$table,$field . " NOT IN ('".implode("','",$p_gallery_mod)."') AND  ".$field_id." ='".$id."' ");
			while($row = db::fetch($rs['delete_gallery'])){
				if($row[$field] != ""){
					@unlink($app['doc_path'] .$place. $row[$field]);
				}
			}
			db::qry("DELETE FROM ".$app['table'][$table]." WHERE ".$field." NOT IN ('".implode("','",$p_gallery_mod)."') AND  ".$field_id." ='".$id."'");
		}else{
			$rs['delete_gallery'] = db::get_record_select($field,$table, $field_id." = '".$id."'");
			while($row = db::fetch($rs['delete_gallery'])){
				if($row[$field] != ""){
					@unlink($app['doc_path'] .$place. $row[$field]);
				}
			}
			db::qry("DELETE FROM ".$app['table'][$table]." WHERE ".$field_id." ='".$id."'");	
		}
	}
}
?>
