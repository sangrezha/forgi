<?php

/*******************************************************************************
* Filename : route.php
* Description : route libary
*******************************************************************************/

class Route
{
	function get($param, $function = null)
    {
        global $app;
		if($_SERVER['REQUEST_METHOD'] == GET)
		{
			$act = $_GET['act'];
			if(stristr($param, $act))
			{
				include $app['api_messanger'] .'/'. $act .'.php';
				$cl = new $act(); 
				if (method_exists($cl, $function)) {
					$cl->$function();
				} else {
					if (method_exists($cl, 'index')) {
						$cl->index();
					} else {
						echo "function not available.\n";
					}
				}
			}else{
				echo "endpoint ". $act ." not found\n";
			}
		}
    }
    function post($param, $function = null)
    {
        global $app;
        if($_SERVER['REQUEST_METHOD'] == POST)
		{
			$act = $_GET['act'];
			if(stristr($param, $act))
			{
				include $app['api_messanger'] .'/'. $act .'.php';
				$cl = new $act(); 
				if (method_exists($cl, $function)) {
					$cl->$function();
				} else {
					if (method_exists($cl, 'store')) {
						$cl->store();
					} else {
						echo "function not available.\n";
					}
				}
			}else{
				echo "endpoint ". $act ." not found\n";
			}
		}
    }
}