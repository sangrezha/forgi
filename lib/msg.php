<?php

/*******************************************************************************
* Filename : msg.php
* Description : message library
*******************************************************************************/

class msg
{
    /***************************************************************************
    * Description : get message
    ***************************************************************************/
    public static function get_message($name)
    {
        $out = null;
        if(!empty($_SESSION['error_validator'])){
            $out = $_SESSION['error_validator'][$name];
        }
        msg::reset_validator($name);
        return $out;
    }
    
    /***************************************************************************
    * Description : reset
    ***************************************************************************/
    public static function reset_validator($name)
    {
        $_SESSION['error_validator'][$name] = null;
    }

    /***************************************************************************
    * Description : set msg
    * Parameters : $msg
    ***************************************************************************/
    public static function set_message($name, $message)
    {
        if($message) $_SESSION['error_validator'][$name] = $message;
    }

    /***************************************************************************
    * Description : get message
    ***************************************************************************/
    public static function get_msg()
    {
        $out = '';
        // if(!empty($_SESSION['msg'])):
            // $out = "<ul>";
            // foreach ($_SESSION['msg'] as $value) {
               // $out .= "<li>" . $value . "</li>";
            // }
            // $out .= "</ul>";
            // //$out = $_SESSION['msg'];
        // endif;
		if(!empty($_SESSION['msg'])):
			$out = $_SESSION['msg'];
		endif;
        $_SESSION['msg'] = '';
        msg::reset();
        return $out;
    }
    
    /***************************************************************************
    * Description : reset
    ***************************************************************************/
    public static function reset()
    {
		$_SESSION['error_msg'] = array();
		$_SESSION['msg'] = '';
    }

    /***************************************************************************
    * Description : set msg
    * Parameters : $msg
    ***************************************************************************/
    public static function set_msg($msg, $succ = 0)
    {
		if ($succ):
			$_SESSION['msg'] = $msg;
		else:
			$_SESSION['error_msg'][] = $msg;
		endif;
    }

    /***************************************************************************
    * Description : build msg
    * Parameters : -
    ***************************************************************************/
    public static function build_msg($succ = 0)
    {
        global $app;
		if ($succ):
			$out = $_SESSION['msg'];
		else:
			$out = $_SESSION['error_msg'];
		endif;
		$_SESSION['msg'] = $out;
    }
    /***************************************************************************
    * Description : build msg clean
    * Parameters : -
    ***************************************************************************/
    public static function build_msg_clean($succ = 0)
    {
        global $app;
		if ($succ):
			$out = $_SESSION['msg'];
		else:
			$out = $_SESSION['error_msg'];
		endif;
		$_SESSION['msg'] = $out;
    }

	/***************************************************************************
    * Description : build popup msg
    * Parameters : -
    ***************************************************************************/
    public static function build_popup_msg($succ = 0)
    {
        global $app;
		if ($succ):
			$out = $_SESSION['msg'];
		else:
			$out = $_SESSION['error_msg'];
		endif;
		$_SESSION['msg'] = $out;
    }

	/***************************************************************************
    * Description : display msg
    * Parameters : -
    ***************************************************************************/
    public static function display_popup_msg($type, $who, $lang = 0)
    {
        global $app;
		if ($type == 0):
			if ($who == 0):
				if ($lang):
					include $app['path']."/ina/dsp_error.php";
				else:
					include $app['path']."/dsp_error.php";
				endif;
				exit;
			endif;
		endif;
    }
	/***************************************************************************
    * Description : display msg
    * Parameters : -
    ***************************************************************************/
    public static function display_notif_msg($type, $msg)
    {
        return '<div class="alert alert-'.$type.' alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>'.$msg.'</strong></div>';
			exit;
    }
}

?>