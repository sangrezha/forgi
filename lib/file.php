<?php

/*******************************************************************************
* Filename : file.php
* Description : file library
*******************************************************************************/

class file
{

	/***************************************************************************
	* Description : save picture to server
	* Notes : $filename = field of file
	*         $folder = folder position where file saved
	*         $prefix =
	***************************************************************************/
	public static function save_picture($filename, $folder, $prefix = null)
	{
		global $app;
		//$width	= $app[$prefix]['w'];
		//$height	= $app[$prefix]['h'];
		$field = substr($filename, 2);
		$new_name = $prefix . "_" . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'], -4, 4);
		if (!@copy($_FILES[$filename]['tmp_name'], $folder .'/'. $new_name)):
			return 'error';
		endif;
		return $new_name;
	}


	/***************************************************************************
	* Description : save file to server
	* Notes : $filename = temporary file on server / file handle
	*         $folder = folder position where file saved
	*         $prefix =
	***************************************************************************/
	public static function save_file($filename, $folder, $prefix = '')
	{
		global $app;
		$field = substr($filename, 2);
		$new_name = $prefix . "_" . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'], -4, 4);

		if (!@copy($_FILES[$filename]['tmp_name'], $folder.'/'. $new_name)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		return $new_name;
	}
	function save_file_rename($filename, $folder, $name)
	{
		global $app;
		$field = substr($filename, 2);
		$path_name = $_FILES[$filename]['name'];
		$ext = pathinfo($path_name, PATHINFO_EXTENSION);
		if (!@copy($_FILES[$filename]['tmp_name'], $folder.'/'. $name.".".$ext)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		// return $name;
		return $name.".".$ext;
	}
	// function save_file_orig($filename, $folder)
	// {
	// 	global $app;
	// 	$field = substr($filename, 2);
	// 	if (!@copy($_FILES[$filename]['tmp_name'], $folder.'/'. $_FILES[$filename]['name'])):
	// 		//admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
	// 	endif;
	// 	return $_FILES[$filename]['name'];
	// }
	function save_file_orig($filename, $folder)
	{
		global $app;
		$field = substr($filename, 2);
		if (!@copy($_FILES[$filename]['tmp_name'], $folder.'/'. $_FILES[$filename]['name'])):
			header("location: error.do&ref=411");
		endif;
		return $_FILES[$filename]['name'];
	}
	function save_file_orig_multiple($filename, $folder, $ke)
	{
		global $app;
		$field = substr($filename, 2);
		if (!@copy($_FILES[$filename]['tmp_name'][$ke], $folder.'/'. $_FILES[$filename]['name'][$ke])):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		return $_FILES[$filename]['name'][$ke];
	}
	function save_picture_multiple_rename($filename, $folder, $rename, $ke)
	{
		global $app;
		$field = substr($filename, 2);
		$new_name = $rename . substr($_FILES[$filename]['name'][$ke], -4, 4);
		// $new_name	= $_FILES[$filename]['name'][$ke];
		if (!@copy($_FILES[$filename]['tmp_name'][$ke], $folder.'/'. $new_name)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		return $new_name;
	}
	function save_picture_multiple($filename, $folder, $prefix = null, $ke)
	{
		global $app;
		$field = substr($filename, 2);
		$new_name = $prefix . "_" . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'][$ke], -4, 4);
		//$new_name	= $_FILES[$filename]['name'][$ke];
		if (!@copy($_FILES[$filename]['tmp_name'][$ke], $folder.'/'. $new_name)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		return $new_name;
	}
	function save_picture_multiple_rand_name($filename, $folder, $prefix = null, $ke)
	{
		global $app;
		$field = substr($filename, 2);
/*		$new_name = $prefix . "_" . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'][$ke], -4, 4);*/
		$new_name = $prefix . "_" . strtoupper(md5(uniqid(rand(), true))). ".jpg";
		//$new_name	= $_FILES[$filename]['name'][$ke];
		if (!@copy($_FILES[$filename]['tmp_name'][$ke], $folder.'/'. $new_name)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		return $new_name;
	}
	function update_picture_multiple_rename($filename, $folder, $rename, $key)
	{
		global $app;
		$field = substr($filename, 2);
		//$new_name = $prefix . "_" . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'][$key][0], -4, 4);
		$new_name	= $rename;
		if (!@copy($_FILES[$filename]['tmp_name'][$key][0], $folder.'/'. $new_name)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		return $new_name;
	}
	function update_picture_multiple($filename, $folder, $prefix = "", $key)
	{
		global $app;
		$field = substr($filename, 2);
		//$new_name = $prefix . "_" . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'][$key][0], -4, 4);
		$new_name	= $_FILES[$filename]['name'][$key][0];
		if (!@copy($_FILES[$filename]['tmp_name'][$key][0], $folder.'/'. $new_name)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		return $new_name;
	}
	function format_size($filename)
	{
		$file_size =  @filesize($filename);
		if ($file_size >= 1073741824):
			$size = round($file_size / 1073741824 * 100) / 100 . " Gb";
		elseif ($file_size >= 1048576):
			$size = round($file_size / 1048576 * 100) / 100 . " Mb";
		elseif ($file_size >= 1024):
			$size = round($file_size / 1024 * 100) / 100 . " Kb";
		elseif ($file_size > 0):
			$size = $file_size . " byte";
		else:
			$size ="0";
		endif;
		return $size;
	}
	function format_icon($filename)
	{
		$extension = substr($filename, -3);
		if ($extension == "avi"):
			$extension = "avi.png";
		elseif($extension == "flv"):
			$extension = "flv.png";
		elseif ($extension == "bmp"):
			$extension = "bmp.png";
		elseif ($extension == "doc"):
			$extension = "doc.png";
		elseif ($extension == "exe"):
			$extension = "exe.png";
		elseif ($extension == "swf"):
			$extension = "swf.png";
		elseif ($extension == "gif"):
			$extension = "gif.png";
		elseif ($extension == "jpg"):
			$extension = "jpg.png";
		elseif ($extension == "mov"):
			$extension = "mov.png";
		elseif ($extension == "mp3"):
			$extension = "mp3.png";
		elseif ($extension == "mpg"):
			$extension = "mpg.png";
		elseif ($extension == "pdf"):
			$extension = "pdf.png";
		elseif ($extension == "ram"):
			$extension = "ram.png";
		elseif ($extension == "rar"):
			$extension = "rar.png";
		elseif ($extension == "txt"):
			$extension = "txt.png";
		elseif ($extension == "wav"):
			$extension = "wav.png";
		elseif ($extension == "xls"):
			$extension = "xls.png";
		elseif ($extension == "zip"):
			$extension = "zip.png";
		elseif ($extension == "sql"):
			$extension = "txt.png";
		else:
			$extension = "";
		endif;
		return $extension;
	}
	function create_folder($path, $folder){
		if (!@mkdir($path."/".$folder)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		chmod($path."/".$folder, 0777);
	}
	function change_folder($path, $olddirname, $newdirname){
		if (!@rename($path."/".$olddirname, $path."/".$newdirname)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
		chmod($path."/".$newdirname, 0777);
	}
	function delete_folder($path, $dirname){
		if (!@rmdir($path."/".$dirname)):
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		endif;
	}
	function save_imageResizeCrop($filename, $folder, $prefix = '',  $thumb_w, $thumb_h, $quality=75){
		global $app;
		$field = substr($filename, 2);
		$new_name = $prefix . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'], -4, 4);

		$original_image_url = $_FILES[$filename]['tmp_name'];
		$thumb_image_url = $folder.'/'.$new_name;
		// ACQUIRE THE ORIGINAL IMAGE: http://php.net/manual/en/function.imagecreatefromjpeg.php
		$original = imagecreatefromjpeg($original_image_url);
		if (!$original) return FALSE;

		// GET ORIGINAL IMAGE DIMENSIONS
		list($original_w, $original_h) = getimagesize($original_image_url);

		// RESIZE IMAGE AND PRESERVE PROPORTIONS
		$thumb_w_resize = $thumb_w;
		$thumb_h_resize = $thumb_h;
		if ($original_w > $original_h)
		{
			$thumb_h_ratio  = $thumb_h / $original_h;
			$thumb_w_resize = (int)round($original_w * $thumb_h_ratio);
		}
		else
		{
			$thumb_w_ratio  = $thumb_w / $original_w;
			$thumb_h_resize = (int)round($original_h * $thumb_w_ratio);
		}
		if ($thumb_w_resize < $thumb_w)
		{
			$thumb_h_ratio  = $thumb_w / $thumb_w_resize;
			$thumb_h_resize = (int)round($thumb_h * $thumb_h_ratio);
			$thumb_w_resize = $thumb_w;
		}

		// CREATE THE PROPORTIONAL IMAGE RESOURCE
		$thumb = imagecreatetruecolor($thumb_w_resize, $thumb_h_resize);
		if (!imagecopyresampled($thumb, $original, 0,0,0,0, $thumb_w_resize, $thumb_h_resize, $original_w, $original_h)) return FALSE;

		// ACTIVATE THIS TO STORE THE INTERMEDIATE IMAGE

		// CREATE THE CENTERED CROPPED IMAGE TO THE SPECIFIED DIMENSIONS
		$final = imagecreatetruecolor($thumb_w, $thumb_h);

		$thumb_w_offset = 0;
		$thumb_h_offset = 0;
		if ($thumb_w < $thumb_w_resize)
		{
			$thumb_w_offset = (int)round(($thumb_w_resize - $thumb_w) / 2);
		}
		else
		{
			$thumb_h_offset = (int)round(($thumb_h_resize - $thumb_h) / 2);
		}

		if (!imagecopy($final, $thumb, 0,0, $thumb_w_offset, $thumb_h_offset, $thumb_w_resize, $thumb_h_resize)) return FALSE;

		// STORE THE FINAL IMAGE - WILL OVERWRITE $thumb_image_url
		if (!imagejpeg($final, $thumb_image_url, $quality)){
			admlib::display_msg($app['lang']['error']['title'], "".$app['lang']['field'][$field]."".$app['lang']['error']['ERR_COPY']."");
		}
		return $new_name;
	}
	function save_pictureResize($filename, $folder, $prefix = '', $width=0, $height=0, $start=1, $max=1){
		global $app;
		$field = substr($filename, 2);
		$new_name = $prefix . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'], -4, 4);
		$Thumb = new phpThumb();
		$image_filename = $_FILES[$filename]['tmp_name'];
		rename($image_filename, $folder.'/'.$_FILES[$filename]['name']);
		$Thumb->setSourceFilename($folder.'/'.$_FILES[$filename]['name']);
		$Thumb->setParameter('wp', $width);
		$Thumb->setParameter('hp', $height);
		$Thumb->setParameter('f', 'png');
		$Thumb->setParameter('far', '1');
		$Thumb->setParameter('bg', "#00FFFF");
		$Thumb->setParameter('aoe', '1');
		$Thumb->setParameter('q', '100');
		$Thumb->setParameter('config_output_format', 'jpeg');
		if ($Thumb->GenerateThumbnail()) {
			if ($Thumb->RenderToFile($folder.'/'.$new_name) && $start==$max) {
				@unlink($folder.'/'.$_FILES[$filename]['name']);
			}
		}
		return $new_name;
	}
	function save_pictureResize_multi($filename, $folder, $prefix = '', $width=0, $height=0, $multi){
		global $app;
		$field = substr($filename, 2);
		$new_name = $prefix . strtoupper(md5(uniqid(rand(), true))) . substr($_FILES[$filename]['name'][$multi], -4, 4);
		require_once $app['path'].'/lib/phpthumb/phpthumb.class.php';

		$Thumb = new phpThumb();

		// set data
		$image_filename = $_FILES[$filename]['tmp_name'][$multi];
		//chmod($path."/".$newdirname, 0777);
		rename($image_filename, $folder.'/'.$_FILES[$filename]['name'][$multi]);
		//print_r($_FILES);
		$Thumb->setSourceFilename($folder.'/'.$_FILES[$filename]['name'][$multi]);

		$Thumb->setParameter('wp', $width);
		$Thumb->setParameter('hp', $height);
		$Thumb->setParameter('f', 'png');
		$Thumb->setParameter('far', '1');
		$Thumb->setParameter('bg', "#00FFFF");
		$Thumb->setParameter('aoe', '1');
		$Thumb->setParameter('q', '100');
		$Thumb->setParameter('config_output_format', 'jpeg');

		if ($Thumb->GenerateThumbnail()) {
			if ($Thumb->RenderToFile($folder.'/'.$new_name)) {
				@unlink($folder.'/'.$_FILES[$filename]['name'][$multi]);
			} else {
				// do something with debug/error messages
				//echo 'Failed:<pre>'.implode("\n\n", $Thumb->debugmessages).'</pre>';exit;
			}
		} else {
			// do something with debug/error messages
			//echo 'Failed:<pre>'.$Thumb->fatalerror."\n\n".implode("\n\n", $Thumb->debugmessages).'</pre>';exit;
		}
		//echo $new_name;exit;
		return $new_name;
	}
	function label_size($param){
		global $app;
		if($app[$param]['w'] > 0 and $app[$param]['h'] > 0){
			echo "<br/><p>Width : ".$app[$param]['w']."px | Height : ".$app[$param]['h']."px</p><br/>";
		}
	}
}

?>
