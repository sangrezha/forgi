<?php
class api {
	public static $lang = 'en';
	/***************************************************************************
	* Description : base64 to IMG
	***************************************************************************/
	public static function base64_to_image($base64_string, $output_file)
	{
		$ifp = fopen($output_file, "wb"); 
		fwrite($ifp, base64_decode(str_replace(' ', '+', $base64_string))); 
		fclose($ifp); 
		return $output_file; 
	}
	/***************************************************************************
	* Description : URL api File
	***************************************************************************/
	public static function urlApiFile($folder,$file) 
	{
		global $app;
		$filename = $app['data_path']."/".$folder."/".$file;
		if(!empty($file) && file_exists($filename)){
			$file = $app['data_http']."/".$folder."/".$file;
		}else{
			$file = "";
		}
		return $file;
	}
	/***************************************************************************
	* Description : URL api File doc_path
	***************************************************************************/
	public static function urlApiFile_doc_path($folder,$file) 
	{
		global $app;
		$filename = $app['doc_path']."/".$folder."/".$file;
		if(!empty($file) && file_exists($filename)){
			$file = $app['doc_http']."/".$folder."/".$file;
		}else{
			$file = "";
		}
		return $file;
	}
	/***************************************************************************
	* Description : URL api File
	***************************************************************************/
	public static function cek_id($id,$prefix){ 
		global $app;
		$idx = db::lookup("id","complaint","id='$id'");
		if($idx != ""){
			$id = $prefix.date("my").rand(10, 99).rand(10, 99);
			self::cek_id($id,$prefix);
		}else{
			return $id;
		}
	}
	
	/***************************************************************************
	* Description : PHP Mail Attachment
	***************************************************************************/
	public static function phpmail($to, $subject, $htmlContent, $filepath = array()){ 
		global $app;
		// ini_set('display_errors', 1);
		//sender
		$from = 'kalsi@wowandaha.com';
		$fromName = 'Kalsi';
		
		//header for sender info
		$headers = "From: $fromName"." <".$from.">";

		//boundary 
		$semi_rand = md5(time()); 
		$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 

		//headers for attachment 
		$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 

		//multipart boundary 
		$message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" .
		"Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n"; 

		//preparing attachment
		if(count($filepath) > 0){
			foreach($filepath as $file){
			if(is_file($file)){
				$message .= "--{$mime_boundary}\n";
				$fp =    @fopen($file,"rb");
				$data =  @fread($fp,filesize($file));

				@fclose($fp);
				$data = chunk_split(base64_encode($data));
				$message .= "Content-Type: application/octet-stream; name=\"".basename($file)."\"\n" . 
				"Content-Description: ".basename($file)."\n" .
				"Content-Disposition: attachment;\n" . " filename=\"".basename($file)."\"; size=".filesize($file).";\n" . 
				"Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
			}
			}
		}
		$message .= "--{$mime_boundary}--";
		$returnpath = "-f" . $from;
		
		//send email
		$mail = @mail($to, $subject, $message, $headers, $returnpath); 

		//email sending status
		return $mail?1:0;
	}
}
?>
