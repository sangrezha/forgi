<?php
/*******************************************************************************
* Filename : application.php
* Description : global variables
*******************************************************************************/
ini_set('display_errors', 0);
session_start();
date_default_timezone_set("Asia/Jakarta");
## VAR #########################################################################
if (!ini_get('register_globals')) {
	while(list($key,$value)=each($_FILES)) $GLOBALS[$key]=$value;
	while(list($key,$value)=each($_ENV)) $GLOBALS[$key]=$value;
	while(list($key,$value)=each($_GET)) $GLOBALS[$key]=$value;
	while(list($key,$value)=each($_POST)) $GLOBALS[$key]=$value;
	while(list($key,$value)=each($_COOKIE)) $GLOBALS[$key]=$value;
	while(list($key,$value)=each($_SERVER)) $GLOBALS[$key]=$value;
	while(list($key,$value)=@each($_SESSION)) $GLOBALS[$key]=$value;
	foreach($_FILES as $key => $value){
		$GLOBALS[$key]=$_FILES[$key]['tmp_name'];
		foreach($value as $ext => $value2){
			$key2 = $key . '_' . $ext;
			$GLOBALS[$key2] = $value2;
		}
	}
}

include "init.php";
 ## PATH ##
$app['path']          = $_SERVER["DOCUMENT_ROOT"] . $app['www'];
$app['http']          = "http://". $_SERVER["HTTP_HOST"].$app['www'];
$app['http_no_www']  = "http://". $_SERVER["HTTP_HOST"].$app['www'];
$app['webmin']        = $app['www'];
$app['webmin_http']   = $app['http'];
$app['pwebmin']       = $app['path']."/src";
$app['src_path']      = $app['pwebmin'];
$app['lib_path']      = $app['path']."/lib";
$app['lib_www']       = $app['www']."/lib";
$app['data_lib']      = $app['webmin'] ."/src/assets/files";
$app['data_lib_http'] = $app['http'] ."/src/assets/files";
$app['data_lib_path'] = $app['src_path'] ."/assets/files";
$app['logo_lib_path'] = $app['src_path'] ."/assets/files";
$app['logo_lib']      = $app['webmin'] ."/src/assets/files";
$app['data_www']      = $app['www']."/_imgs";
$app['data_path']     = $app['pwebmin']."/assets/imgs";
$app['data_http']     = $app['http']."/_imgs";
$app['path_vendors']  = $app['src_path']."/assets/vendor";
$app['component']     = $app['src_path'] ."/assets/component";
$app['doc_path']	  = $app['path']."/src/assets/docs";
$app['doc_www']	 	  = $app['www']."/src/assets/docs";
$app['doc_http']	  = $app['http']."/src/assets/docs";
$app['mail_subject']          = "ForgiSystem - Pemberitahuan";
#=========================#

// $config = db::get_record('configuration', 'id', 1);
// $app['api_messanger'] = $app['path'] ."/ws";
// $app['enckey'] = "EBPI2017";

#SERVER KEY PUSH NOTIF#
/* OLD VERSION KEY */
// $app['FirebaseKey'] = "AAAA2A-kdno:APA91bFgcsbh-jUTu5jBPD0V8WFXOPu4SFTxrPJdraFQ3DsPyiTagYcTx2xl-t9rEY5nf95EWvbnG6ic3_9yfUC36n3_Asuh_K0FD-OqHQsi1KPax4IIlPjR0GhFZDJRYxydHTUwKP8A";
/* NEW VERSION KEY */
$app['FirebaseKey'] = "AAAAZOHr42M:APA91bFavbo8ztf-gIJHoB1rqXIPOkXy0pRygeq8RKe0EpB1crd-dkK5wixdnu8NF5jZKpiKkK6KGQJoJgcisMdmQNem1H8cxdUrsTV4PwZC2LXTiB6FtmLmD-ykwQvg-izF-yBA3R1G";
/* Client KEY */
$app['FirebaseKey_client'] = "AAAAgZ5aReQ:APA91bG1n11aXJKkIcDUuuOIX-i4TxNkAzZig8k68o39CULizzO4I1S5Mt7P7yrJrle-F4WuCcbZK6nJO9Ce1HKHL_4bHpMpyGhyVzvAM9i5FgX_Q3ON0aXSjXeO6VXvXSwlK_lKbZAG";
#=========================#

## SMTP MAIL CONFIG ##
// $app['smtp_host'] 	 = 'crm.kalsi.co.id';
// $app['smtp_auth'] 	 = true;
// $app['smtp_user'] 	 = 'ebpi@crm.kalsi.co.id';
// $app['smtp_pass'] 	 = '@Gresik01!';
// $app['smtp_port'] 	 = 25; 
// $app['smtp_email']	 = 'ebpi@crm.kalsi.co.id';
// $app['smtp_name']	 = 'KALSI';
#=========================#

$app['session']       = 'webapp_dermaga';

#WEBMIN#
$app['_template']     = $app['webmin']."/_template";
$app['_static']       = $app['webmin']."/_static";
$app['_styles']       = $app['webmin']."/_styles";
$app['_scripts']      = $app['webmin']."/_scripts";
$app['_vendors']      = $app['webmin']."/_vendors";
$app['_imgs']         = $app['webmin']."/_imgs";
$app['http_imgs']     = $app['http']."/_imgs";
$app['http_static']   = $app['http']."/_static";
#=========================#
## TABLES & PRIVILEGE ##########################################################
include $app['path']."/tables.php";
include $app['path']."/lib/privilege.php";
## CORE LIBRARY ################################################################
include $app['path']."/lib/config.php";
include $app['path']."/lib/app.php";
include $app['path']."/lib/db.php";
//include $app['path_vendors']."/phpJWT/init.php";
// include $app['path_vendors'].'/PHPMailer_lama/PHPMailerAutoload.php';
// include $app['path_vendors'].'/PHPMailer/src/PHPMailer.php';
?>
