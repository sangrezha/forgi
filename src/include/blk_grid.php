<?php admlib::display_block_header();
admlib::get_component('datepickerlib');
admlib::get_component('select2lib');
$rs['customer'] = db::get_record_select("id, name AS name","customer","status='active' ORDER BY name ASC");
if ($search_date =='yes')admlib::get_component('datepickerlib');
?>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tool_box add_fix">
                    <!-- <div class="pull-left"> -->
                    <div class="pull-right">
                			  <?php if(admlib::acc('CRT') || !empty($app['me']['level'])){ ?>
                  					<a href="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>&act=add">
                  						<button type="button" class="btn btn-default add"><i class="fa fa-plus"></i> Add</button>
                  					</a>
                  			<?php } ?>
                  			<?php //if(admlib::acc('CRT')){ ?>
                  				<!--	<a id="duplicate">
                  						<button type="button" class="btn btn-default copy"><i class="fa fa-copy"></i> Copy</button>
                  					</a> -->
                  			<?php //} ?>
                       <!--<a href="#"><button type="button" class="btn btn-default sort"><i class="fa fa-sort"></i> Sort</button></a>-->
                			   <a href="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>" >
                					   <button type="button" class="btn btn-default sort"><i class="fa fa-refresh"></i> Refresh</button>
                				 </a>
                        <!-- <div class="pull-right">  -->
                				<?php //if(admlib::acc('DEL') || !empty($app['me']['level'])){
	                				  if(admlib::acc('DEL')){ ?>
                					<a id="delete" class="btn btn-default add" action="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>&act=delete">
									<i class="fa fa-trash"></i> Delete
										</a>
                  			<?php } ?>
	                     <!-- </div>  -->
                    </div>
                     <!--<div class="pull-right">
                				<?php if(admlib::acc('DEL') || $app['me']['code'] == "it" ){ ?>
                					<a id="delete" class="btn btn-default add" action="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>&act=delete">
									<i class="fa fa-trash"></i> Delete
										</a>
                  			<?php } ?>
                    </div>-->
                </div>
            </div>
        </div>
		    <div class="page-title">
		    <?php if ($custom_search == "import") 
					{ ?>
				<div>
				<?php }else{
						?>
	            <div class="title_left">
					<?php					
					$module = admlib::$page_active['module'];
					}
					?>
                <h3 style="color:#10069f;"><?php echo admlib::$page_active['caption'] ?></h3>
                <!-- <h4><?= app::i18n("desc_".$module) ?></h4> -->
                
                <span style="font-size:14px;color:#f58220;">
                <?php 
                // print_r($get_config);
                // $get_config = db::get_record("config","id","1");
				$get_config = db::get_record("configuration", "id", 1);
                switch ($module) {
                	case 'form_cpassword':
                		$desk_tampil =$get_config['desk_form_cpassword'] ;
                		break;
                	case 'form_acc_folder':
                		$desk_tampil =$get_config['desk_form_acc_folder'] ;
                		break;
                	case 'form_tmp_access':
                		$desk_tampil =$get_config['desk_form_tmp_access'] ;
                		break;
                	case 'form_perubahan_si':
                		$desk_tampil =$get_config['desk_form_perubahan_si'] ;
                		break;
                	case 'form_abnormal_si':
                		$desk_tampil =$get_config['desk_form_abnormal_si'] ;
                		break;
                	case 'form_get_soft':
                		$desk_tampil =$get_config['desk_form_get_soft'] ;
                		break;
                	case 'form_recovery_data':
                		$desk_tampil =$get_config['desk_form_recovery_data'] ;
                		break;
                	case 'form_hardware_software':
                		$desk_tampil =$get_config['desk_form_hardware_software'] ;
                		break;
                	case 'form_pengajuan_user':
                		$desk_tampil =$get_config['desk_form_pengajuan_user'] ;
                		break;
                	case 'form_register_device':
                		$desk_tampil =$get_config['desk_form_register_device'] ;
                		break;
                	
                	default:
                	$desk_tampil = "";
                		break;
                }
                // app::i18n("desc_".$module)
                echo $desk_tampil;
                ?></span>
                <!-- <h4><?php echo admlib::$page_active['caption'] ?></h4> -->
            </div>
            <?php 
	            if ($custom_search == "import" || $custom_search == "beacukai" || $custom_search == "trucking" || $custom_search == "ret_con") { ?>
		            <div>
		                <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
				<?php }else{ ?>
		            <div class="title_right">
		                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				<?php
	            }
             ?>
            				<form class="navbar-form navbar-left" role="search">
            					<?php 
            						if ($custom_search == "import") {
        							?>
        							<div class="form-group"style="width: 10%;">
										<label for="sel1">Status beacukai:</label>
										<select name="p_stat_bea" id="p_stat_bea" class="form-control">
											<option value="all" selected>All</option>
											<option value="spjm">SPJM</option>
											<option value="spjk">SPJK</option>
											<option value="spjh">SPJH</option>
										</select>
									</div> 
									<!-- <div class="col-md-2 col-sm-3 col-xs-12"> -->
									<div class="form-group">
										<label>Customer</label>
										<div class="control-group">
											<select class="select2 form-control" name="p_customer" id="p_customer">
												<option value="all" >All</option>
												<?php while ($row=db::fetch($rs['customer'])){ ?>
													<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?>
												</option>
											<?php } ?>
											</select>
										</div>
									</div>
        							<div class="form-group" style="width: 15%;">
										<label for="sel1">Tanggal Nopen:</label>
            							<input type="text" class="form-control datepickers2" name="p_tgl_nopen" id="p_tgl_nopen" placeholder="Tgl Nopen"/>
            						</div>
        							<div class="form-group" style="width: 25%;">
										<label for="sel1">Status Proses:</label>
										<select name="p_stat_kir" id="p_stat_kir" class="form-control">
											<option value="all" selected>All</option>
											<option value="1">Selesai</option>
											<option value="0">Belum Selesai</option>
										</select>
									</div> 
	            					<div class="input-group" style="width: 15%;top: 25px;">
										<label for="sel1">Search :</label>
	            						<input type="text" class="form-control <?php echo ($search_date=='yes'?'datepickers2':null); ?>" id="key" placeholder="<?php
	            						// echo app::getliblang('search'); 
		            						if($custom_search == "import"){
		            							echo "NO PIB / Nopen";
		            						}
	            						?>"/>
										<label for="sel1" style="position: relative;">- Tulis No pib / nopen yang ingin di cari</label>
	            					</div>
        							<?php
            						}
            						elseif($custom_search == "beacukai"){
            						?>
	        							<div class="form-group"style="width: 10%;">
											<label for="sel1">Status beacukai:</label>
											<select name="p_stat_bea" id="p_stat_bea" class="form-control">
												<option value="all" selected>All</option>
												<option value="spjm">SPJM</option>
												<option value="spjk">SPJK</option>
												<option value="spjh">SPJH</option>
											</select>
										</div>

										<div class="form-group">
											<label>Customer</label>
											<div class="control-group">
												<select class="select2 form-control" name="p_customer" id="p_customer">
													<option value="all" >All</option>
													<?php while ($row=db::fetch($rs['customer'])){ ?>
														<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?>
													</option>
												<?php } ?>
												</select>
											</div>
										</div> 
	            					<div class="input-group" style="width: 15%;margin-top: 10px;">
										<label for="sel1">Search :</label>
	            						<input type="text" class="form-control <?php echo ($search_date=='yes'?'datepickers2':null); ?>" id="key" placeholder="NO PIB"/>
	            					</div>
            						<?php
            						}elseif($custom_search == "trucking"){
	        							?>
	        							<div class="form-group" style="width: 15%;">
											<label for="sel1">Tanggal Pengiriman:</label>
	            							<input type="text" class="form-control datepickers2" name="p_tgl_nopen" id="p_tgl_nopen" placeholder="Tanggal Pengiriman"/>
	            						</div>

										<div class="form-group">
											<label>Customer</label>
											<div class="control-group">
												<select class="select2 form-control" name="p_customer" id="p_customer">
													<option value="all" >All</option>
													<?php while ($row=db::fetch($rs['customer'])){ ?>
														<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?>
													</option>
												<?php } ?>
												</select>
											</div>
										</div> 
	            					<div class="input-group" style="width: 15%;margin-top: 10px;">
										<label for="sel1">Search :</label>
	            						<input type="text" class="form-control <?php echo ($search_date=='yes'?'datepickers2':null); ?>" id="key" placeholder="NO PIB / Container"/>
	            					</div>

	        							<?php
            						}elseif($custom_search == "ret_con"){ ?>
	        							<div class="form-group" style="width: 15%;">
											<label for="sel1">Tanggal Pengembalian:</label>
	            							<input type="text" class="form-control datepickers2" name="p_tgl_nopen" id="p_tgl_nopen" placeholder="Tanggal Pengembalian"/>
	            						</div>

										<div class="form-group">
											<label>Customer</label>
											<div class="control-group">
												<select class="select2 form-control" name="p_customer" id="p_customer">
													<option value="all" >All</option>
													<?php while ($row=db::fetch($rs['customer'])){ ?>
														<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?>
													</option>
												<?php } ?>
												</select>
											</div>
										</div> 
	            					<div class="input-group" style="width: 15%;margin-top: 10px;">
										<label for="sel1">Search :</label>
	            						<input type="text" class="form-control <?php echo ($search_date=='yes'?'datepickers2':null); ?>" id="key" placeholder="NO PIB / Container"/>
	            					</div>

	        							<?php
            						}else{
            							?>
             					<div class="input-group">
									<label for="sel1">Search :</label>
            						<input type="text" class="form-control <?php echo ($search_date=='yes'?'datepickers2':null); ?>" id="key" placeholder="<?php echo app::getliblang('search'); ?>"/>
            					</div>
            							<?php
            						}
            					 ?>
<!--              					<div class="input-group">
									<label for="sel1">Search :</label>
            						<input type="text" class="form-control <?php echo ($search_date=='yes'?'datepickers2':null); ?>" id="key" placeholder="<?php echo app::getliblang('search'); ?>"/>
            					</div> -->
<!--             					<div class="input-group">
									<label for="sel1">Search :</label>
            						<input type="text" class="form-control <?php echo ($search_date=='yes'?'datepickers2':null); ?>" id="key" placeholder="<?php
            						// echo app::getliblang('search'); 
	            						if($custom_search == "import"){
	            							echo "NO PIB / Nopen";
	            						}
            						?>"/>
            					</div> -->
            					<?php if ($custom_search == "import" || $custom_search == "beacukai" || $custom_search == "trucking" || $custom_search == "ret_con") { ?>
            						<a href="#" name="submit_search" id="submit_search" class="btn btn-warning" <?php if ($custom_search == "beacukai") { echo "style='margin-top: 28px;'"; }elseif ($custom_search == "trucking") {  echo "style='margin-top: 28px;'"; }elseif ($custom_search == "ret_con") {  echo "style='margin-top: 28px;'"; } ?> >
            							Submit
            						</a>
            						<!-- <button name="submit_search" id="submit_search" class="btn btn-warning">Submit</button> -->
            					<?php } ?>
            				</form>
                </div>
            </div>
        </div>

		<div class="notif-msg" style="margin:70px 0 15px 0;"></div>
		<div class="clearfix"></div>
				<div class="x_content">
					<!-- <div id="grid">Wait...</div> -->
					<div id="grid"><img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" /></div>
				</div>
			</div>
          </div>
        </div>
        <!-- /page content -->
<link rel="stylesheet" type="text/css" href="<?php echo $app['_scripts'] ?>/tablesorted/theme.default.min.css">
<script type="text/javascript" src="<?php echo $app['_scripts'] ?>/tablesorted/jquery.tablesorter.min.js"></script>
<script src="<?php  echo $app['_scripts'] ?>/jqui/jquery-ui.js"></script>
<script type="text/javascript">
	function onkonfirm(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin meng approve "+name+" ?");
		if (r == true) { 
			location.href = href; 
		} 
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
	function on_reject(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin mereject ?");
		if (r == true) { 
			location.href = href; 
		} 
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
	function onkonfirm_reject(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin meng approve "+name+" ?");
		var z = confirm("Apakah Anda yakin mereject "+name+" ?");
		if (r == true) { 
			location.href = href; 
		} else{
			if(z == true){
				alert(href+"&reject=1");
				location.href = href+"&reject=1"; 
			}
		}
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
	function onkonfirm_note(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin meng approve "+name+" ?");
		if (r == true) { 
			// location.href = href; 
			var item =null;
			item = prompt("Note", "");
			if (item != null) {
				// alert(href+"&note="+item);
				location.href = href+"&note="+item;
			}
			return false;
		} 
	}
	function onkonfirm_note_reject(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin meng mereject ?");
		if (r == true) { 
			// location.href = href; 
			var item =null;
			item = prompt("Note", "");
			if (item != null) {
				// alert(href+"&note="+item);
				location.href = href+"&note="+item;
			}
			return false;
		} 
		// else{
		// 	if(z == true){
		// 		alert(href+"&reject=1");
		// 		location.href = href+"&reject=1"; 
		// 	}
		// }
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
  $(document).ready(function() {
  	function tombolKirimBeacukai(id_import,status,pib){
		var confirmnya = confirm('Apakah Anda Yakin Ingin Mengirim Data Import ini ?');
		if (confirmnya==true) {
			// $.get('<?php echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>', { act : "status",  id : id_pelatihan,status : status} );
	        var postingan =	$.post('<?php echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>', { act : "send_email_beacukai",  id : id_import,status : status,pib : pib } );
			postingan.done(function( data ) {
				// alert("Hide");
				// location.reload(); 
				// document.location.href = '<?php //echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>';
				// return false;
				document.location.href = "<?php echo $app['webmin']."/".admlib::$page_active['module'].".mod"; ?>";
			});
		}
}
	 var checkItem = function(){
		var chkall      = $('#checkall');
		var checkitem 	= $('.checkitem');
		chkall.bind('change',function(){
			if(this.checked)
			{
				checkitem.prop('checked', true);
			}
			else
			{
				checkitem.prop('checked', false);
			}
		});
		checkitem.bind('change',function(){
			if(this.checked)
			{
				$('.'+ $(this).attr('identity')).prop('checked', true);
			}
			else
			{
				$('.'+ $(this).attr('identity')).prop('checked', false);
			}
		});
	},
	ckeckDel = function(){
		$('#delete').click(function(){
			 if($('.checkitem:checked').length > 0)
			{
					var lsform = $('#listForm');
					lsform.attr('action', $(this).attr('action')).submit();
					return true;
			}
			else
			{
				$(".notif-msg").html('<?php echo msg::display_notif_msg("warning", app::getliblang('opt_first') ) ?>');
			}
		});
	};
	$('#duplicate').click(function(){
		if($('.checkitem:checked').length > 0)
		{
			var lsform = $('#listForm');
			lsform.attr('action', $(this).attr('action')).submit();
			return false;
		}
		else
		{
			$(".notif-msg").html('<?php echo msg::display_notif_msg("warning", app::getliblang('opt_first') ) ?>');
		}
	});
	<?php if( $success ){ ?>
		$(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::getliblang('success')." ".$success ) ?>');
	<?php } ?>
	<?php if( $error ){ ?>
		$(".notif-msg").html('<?php echo msg::display_notif_msg("danger", app::getliblang('error')." ".$error ) ?>');
	<?php } ?>
      var loadData = function(url,args){
//alert("disini");
//alert(url);
          $.ajax({
              url: url,
              type : 'POST', cache: false,
              data : args,
              dataType : 'html',
              success: function(data) {
                  $('#grid').html(data);
					$("#table-grid,.table-grid").tablesorter({
						widgets        : ['columns'],
						usNumberFormat : false,
						sortReset      : true,
						sortRestart    : true
					});
					$( "#sortable" ).sortable({
						update: function() {
							$.post( $('#listForm').attr('action') , $(this).sortable("serialize") +"&act=reorder",function(data){
									if(data){
										window.location.href=$('#listForm').attr('action');
										$(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::i18n('success_reorder') ) ?>');
									}
							});
						}
					});
                  checkItem();
                  ckeckDel();
                  $('.pagination a').click(function(){
                      loadData( $(this).attr('href') , {act:'data', ref:$('#key').val()});
                      return false;
                  });
              }
        });
      };
<?php if ($search_date=='yes') { ?>

   //    	$('.datepickers2').datepicker({
   //    	  // showOn: "button",
   //     	 //  buttonText: "kalender",
   //     	  dateFormat: 'yy-mm-dd'
   //      }, function(start, end) {
			// loadData(_url, {act:'data', ref:$('#key').val()});
   //      });
        $('.datepickers2').daterangepicker({
      	  // showOn: "button",
       	 //  buttonText: "kalender",
          singleDatePicker: true,
          calender_style: "picker_3",
		  // format: 'YYYY-MM-DD'
		  format: 'DD-MM-YYYY'
        }, function(start, end) {
			loadData(_url, {act:'data', ref:$('#key').val()});
        });
<?php } ?>
		var _url = $('#listForm').attr('action');
//alert("disini "+_url);
		loadData(_url, {act:'data', ref:''});

		// search awal
		// $('#key').keyup(function(){
		// 	loadData(_url, {act:'data', ref:$(this).val()});
		// });
		<?php if ($custom_search == "import") { ?>
        $('.datepickers2').daterangepicker({
      	  // showOn: "button",
       	 //  buttonText: "kalender",
          singleDatePicker: true,
          calender_style: "picker_3",
		  // format: 'YYYY-MM-DD'
		  format: 'DD-MM-YYYY'
        }, function(start, end) {
        	// alert(_url+"data"+$('#key').val());
			// loadData(_url, {act:'data', ref:$('#key').val()});
        });
		$('#submit_search').click(function(){
			// alert("disini");
			// alert("search:"+$("#key").val()+" & stat_bea:"+$("#p_stat_bea").val()+" & p_customer_s:"+$("#p_customer").val()+" & p_tgl_nopen_s:"+$("#p_tgl_nopen").val()+" & p_stat_kir:"+$("#p_stat_kir").val());
            $('#grid').html('<img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" />');
			loadData(_url, {act:'data', act_search:"import", p_search:$("#key").val(), p_stat_bea:$("#p_stat_bea").val(), p_customer_s:$("#p_customer").val(), p_tgl_nopen_s:$("#p_tgl_nopen").val(), p_stat_kir:$("#p_stat_kir").val()});
		});
		<?php }elseif($custom_search == "beacukai"){ ?>
			$('#submit_search').click(function(){
            	$('#grid').html('<img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" />');
				loadData(_url, {act:'data', act_search:"beacukai", p_search:$("#key").val(), p_stat_bea:$("#p_stat_bea").val(), p_customer_s:$("#p_customer").val()});
			});
		<?php }elseif($custom_search == "trucking"){ ?>
	        $('.datepickers2').daterangepicker({
	      	  // showOn: "button",
	       	 //  buttonText: "kalender",
	          singleDatePicker: true,
	          calender_style: "picker_3",
			  // format: 'YYYY-MM-DD'
			  format: 'DD-MM-YYYY'
	        }, function(start, end) {
	        	// alert(_url+"data"+$('#key').val());
				// loadData(_url, {act:'data', ref:$('#key').val()});
	        });
			$('#submit_search').click(function(){
				// alert("disini");
				// alert("search:"+$("#key").val()+" & stat_bea:"+$("#p_stat_bea").val()+" & p_customer_s:"+$("#p_customer").val()+" & p_tgl_nopen_s:"+$("#p_tgl_nopen").val()+" & p_stat_kir:"+$("#p_stat_kir").val());
	            $('#grid').html('<img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" />');
				loadData(_url, {act:'data', act_search:"trucking", p_search:$("#key").val(), p_customer_s:$("#p_customer").val(), p_tgl_nopen_s:$("#p_tgl_nopen").val()});
			});
		<?php }elseif($custom_search == "ret_con"){ ?>
				$('.datepickers2').daterangepicker({
					  // showOn: "button",
					 //  buttonText: "kalender",
				  singleDatePicker: true,
				  calender_style: "picker_3",
				  // format: 'YYYY-MM-DD'
				  format: 'DD-MM-YYYY'
				}, function(start, end) {
					// alert(_url+"data"+$('#key').val());
					// loadData(_url, {act:'data', ref:$('#key').val()});
				});
				$('#submit_search').click(function(){
					// alert("disini");
					// alert("search:"+$("#key").val()+" & stat_bea:"+$("#p_stat_bea").val()+" & p_customer_s:"+$("#p_customer").val()+" & p_tgl_nopen_s:"+$("#p_tgl_nopen").val()+" & p_stat_kir:"+$("#p_stat_kir").val());
				    $('#grid').html('<img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" />');
					loadData(_url, {act:'data', act_search:"ret_con", p_search:$("#key").val(), p_customer_s:$("#p_customer").val(), p_tgl_nopen_s:$("#p_tgl_nopen").val()});
				});
		<?php }else{ ?>
			$('#key').keyup(function(){
				loadData(_url, {act:'data', ref:$(this).val()});
			});
		<?php } ?>

});

</script>
<?php admlib::display_block_footer(); ?>
