<?php 
admlib::display_block_header();
admlib::get_component('formstart')
?>
		<div class="row">
			<div class="col-md-10">
				<ul class="collection">
					<li class="collection-item">
						<blockquote>
							<?php echo app::getliblang('confirm_delete');  ?>
						</blockquote>
					</li>
					<?php while($row=db::fetch($rs['row'])){ ?>
						<li class="collection-item">
							<?php echo $row['id']; ?>
							<input type="hidden" name="p_id[]" value="<?php echo $row['id']; ?>" />
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
<?php 
		admlib::get_component('submit', 
			array(
				"id"=>(isset($id)?$id:null), 
				"act"=>"delete"
			)
		);
admlib::get_component('formend');
admlib::display_block_footer();

?>