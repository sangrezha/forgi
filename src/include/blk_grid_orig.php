<?php admlib::display_block_header();if ($search_date =='yes')admlib::get_component('datepickerlib');?>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tool_box add_fix">
                    <div class="pull-left">
                			  <?php if(admlib::acc('CRT')){ ?>
                  					<a href="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>&act=add">
                  						<button type="button" class="btn btn-default add"><i class="fa fa-plus"></i> Add</button>
                  					</a>
                  			<?php } ?>
                  			<?php //if(admlib::acc('CRT')){ ?>
                  				<!--	<a id="duplicate">
                  						<button type="button" class="btn btn-default copy"><i class="fa fa-copy"></i> Copy</button>
                  					</a> -->
                  			<?php //} ?>
                       <!--<a href="#"><button type="button" class="btn btn-default sort"><i class="fa fa-sort"></i> Sort</button></a>-->
                			   <a href="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>" >
                					   <button type="button" class="btn btn-default sort"><i class="fa fa-refresh"></i> Refresh</button>
                				 </a>
                       
                    </div>
                    <div class="pull-right">
                				<?php if(admlib::acc('DEL')){ ?>
                					<a id="delete" class="btn btn-default add" action="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>&act=delete">
									<i class="fa fa-trash"></i> Delete
										</a>
                  			<?php } ?>
                    </div>
                </div>
            </div>
        </div>
		    <div class="page-title">
            <div class="title_left">
                <h3><?php echo admlib::$page_active['caption'] ?></h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            				<form class="navbar-form navbar-left" role="search">
            					<div class="input-group">
            						<input type="text" class="form-control <?php echo ($search_date=='yes'?'datepickers2':null); ?>" id="key" placeholder="<?php echo app::getliblang('search'); ?>"/>
            					</div>
            				</form>
                </div>
            </div>
        </div>

		<div class="notif-msg"></div>
		<div class="clearfix"></div>
				<div class="x_content">
					<div id="grid">Wait...</div>
				</div>
			</div>
          </div>
        </div>
        <!-- /page content -->
<link rel="stylesheet" type="text/css" href="<?php echo $app['_scripts'] ?>/tablesorted/theme.default.min.css">
<script type="text/javascript" src="<?php echo $app['_scripts'] ?>/tablesorted/jquery.tablesorter.min.js"></script>
<script src="<?php  echo $app['_scripts'] ?>/jqui/jquery-ui.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
  	function tombolKirimBeacukai(id_import,status,pib){
		var confirmnya = confirm('Apakah Anda Yakin Ingin Mengirim Data Import ini ?');
		if (confirmnya==true) {
			// $.get('<?php echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>', { act : "status",  id : id_pelatihan,status : status} );
	        var postingan =	$.post('<?php echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>', { act : "send_email_beacukai",  id : id_import,status : status,pib : pib } );
			postingan.done(function( data ) {
				// alert("Hide");
				// location.reload(); 
				// document.location.href = '<?php //echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>';
				// return false;
				document.location.href = "<?php echo $app['webmin']."/".admlib::$page_active['module'].".mod"; ?>";
			});
		}
}
	 var checkItem = function(){
		var chkall      = $('#checkall');
		var checkitem 	= $('.checkitem');
		chkall.bind('change',function(){
			if(this.checked)
			{
				checkitem.prop('checked', true);
			}
			else
			{
				checkitem.prop('checked', false);
			}
		});
		checkitem.bind('change',function(){
			if(this.checked)
			{
				$('.'+ $(this).attr('identity')).prop('checked', true);
			}
			else
			{
				$('.'+ $(this).attr('identity')).prop('checked', false);
			}
		});
	},
	ckeckDel = function(){
		$('#delete').click(function(){
			 if($('.checkitem:checked').length > 0)
			{
					var lsform = $('#listForm');
					lsform.attr('action', $(this).attr('action')).submit();
					return true;
			}
			else
			{
				$(".notif-msg").html('<?php echo msg::display_notif_msg("warning", app::getliblang('opt_first') ) ?>');
			}
		});
	};
	$('#duplicate').click(function(){
		if($('.checkitem:checked').length > 0)
		{
			var lsform = $('#listForm');
			lsform.attr('action', $(this).attr('action')).submit();
			return false;
		}
		else
		{
			$(".notif-msg").html('<?php echo msg::display_notif_msg("warning", app::getliblang('opt_first') ) ?>');
		}
	});
	<?php if( $success ){ ?>
		$(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::getliblang('success')." ".$success ) ?>');
	<?php } ?>
	<?php if( $error ){ ?>
		$(".notif-msg").html('<?php echo msg::display_notif_msg("danger", app::getliblang('error')." ".$error ) ?>');
	<?php } ?>
      var loadData = function(url,args){
          $.ajax({
              url: url,
              type : 'POST', cache: false,
              data : args,
              dataType : 'html',
              success: function(data) {
                  $('#grid').html(data);
					$("#table-grid,.table-grid").tablesorter({
						widgets        : ['columns'],
						usNumberFormat : false,
						sortReset      : true,
						sortRestart    : true
					});
					$( "#sortable" ).sortable({
						update: function() {
							$.post( $('#listForm').attr('action') , $(this).sortable("serialize") +"&act=reorder",function(data){
									if(data){
										window.location.href=$('#listForm').attr('action');
										$(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::i18n('success_reorder') ) ?>');
									}
							});
						}
					});
                  checkItem();
                  ckeckDel();
                  $('.pagination a').click(function(){
                      loadData( $(this).attr('href') , {act:'data', ref:$('#key').val()});
                      return false;
                  });
              }
        });
      };
<?php if ($search_date=='yes') { ?>

   //    	$('.datepickers2').datepicker({
   //    	  // showOn: "button",
   //     	 //  buttonText: "kalender",
   //     	  dateFormat: 'yy-mm-dd'
   //      }, function(start, end) {
			// loadData(_url, {act:'data', ref:$('#key').val()});
   //      });
        $('.datepickers2').daterangepicker({
      	  // showOn: "button",
       	 //  buttonText: "kalender",
          singleDatePicker: true,
          calender_style: "picker_3",
		  // format: 'YYYY-MM-DD'
		  format: 'DD-MM-YYYY'
        }, function(start, end) {
			loadData(_url, {act:'data', ref:$('#key').val()});
        });
<?php } ?>
		var _url = $('#listForm').attr('action');
		loadData(_url, {act:'data', ref:''});

		$('#key').keyup(function(){
			loadData(_url, {act:'data', ref:$(this).val()});
		});
});

</script>
<?php admlib::display_block_footer(); ?>
