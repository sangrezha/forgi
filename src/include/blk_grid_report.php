<?php admlib::display_block_header();
admlib::get_component('datepickerlib');
admlib::get_component('select2lib');
$rs['customer'] = db::get_record_select("id, name AS name","customer","status='active' ORDER BY name ASC");
if ($search_date =='yes')admlib::get_component('datepickerlib');
?>
 <!-- page content -->
	<style>
			.monthselect{
				color: black;
			}
			.yearselect{
				color: black;
			}
		</style>
        <div class="right_col" role="main">
          <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tool_box add_fix">
                    <div class="pull-left">
                			  <?php if(admlib::acc('CRT') || !empty($app['me']['level'])){ ?>
                  					<!-- <a href="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>&act=add">
                  						<button type="button" class="btn btn-default add"><i class="fa fa-plus"></i> Add</button>
                  					</a> -->
                  			<?php } ?>
                  			<?php //if(admlib::acc('CRT')){ ?>
                  				<!--	<a id="duplicate">
                  						<button type="button" class="btn btn-default copy"><i class="fa fa-copy"></i> Copy</button>
                  					</a> -->
                  			<?php //} ?>
                       <!--<a href="#"><button type="button" class="btn btn-default sort"><i class="fa fa-sort"></i> Sort</button></a>-->
                			   <a href="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>" >
                					   <button type="button" class="btn btn-default sort"><i class="fa fa-refresh"></i> Refresh</button>
                				 </a>
                       
                    </div>
                    <!-- <div class="pull-right">
                				<?php if(admlib::acc('DEL') || !empty($app['me']['level'])){ ?>
                					<a id="delete" class="btn btn-default add" action="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>&act=delete">
									<i class="fa fa-trash"></i> Delete
										</a>
                  			<?php } ?>
                    </div> -->
                </div>
            </div>
        </div>
		    <div class="page-title">
		    <?php if ($custom_search == "import") 
					{ ?>
				<div>
				<?php }else{
						?>
	            <div class="title_left">
					<?php					
					}
					?>
                <h3><?php echo admlib::$page_active['caption'] ?></h3>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php $module = admlib::$page_active['module']; ?>
				<!-- <form action="<?= $app['webmin'] ."/". $module .".mod" ?>" method="POST"> -->
					<div class="col-md-2 col-sm-1 col-xs-12">
							Start Date<span class="required"></span>
							<?php  ?>
							<div class="controls">
								<!-- <input type="text" name="p_tgl_awal" id="tgl_awal" value="<?php// echo (isset($app['ref1']))?$app['ref1']:date("Y-m-01", strtotime("-1 month",strtotime(date("Y-m-d")))) ?>" class="form-control has-feedback-left datepickers active" validate="" aria-describedby="tgl_awal" rel="tgl_awal"><i class="fa fa-calendar form-control-feedback left" aria-hidden="true" style="top:15px"></i> -->
								<input type="text" name="p_tgl_awal" id="tgl_awal" value="<?= (empty($_REQUEST['start_date'])?date('Y-m')."-1":$_REQUEST['start_date']) ?>" class="form-control has-feedback-left datepickers active" validate="" aria-describedby="tgl_awal" rel="tgl_awal"><i class="fa fa-calendar form-control-feedback left" aria-hidden="true" style="top:15px"></i>
								</div>
							</div>	
						<div class="col-md-2 col-sm-1 col-xs-12">
							End Date<span class="required"></span>
							<div class="controls"> 
								<!-- <input type="text" name="p_tgl_akhir" id="tgl_akhir" value="<?php echo (isset($app['ref2']))?$app['ref2']:date("Y-m-t", strtotime(date("Y-m-d"))) ?>" class="form-control has-feedback-left datepickers active" validate="" aria-describedby="tgl_akhir" rel="p_tgl_akhir"><i class="fa fa-calendar form-control-feedback left" aria-hidden="true" style="top:15px"></i> -->
								<input type="text" name="p_tgl_akhir" id="tgl_akhir" value="<?= (empty($_REQUEST['end_date'])?date("Y-m-t"):$_REQUEST['end_date']) ?>" class="form-control has-feedback-left datepickers active" validate="" aria-describedby="tgl_akhir" rel="p_tgl_akhir"><i class="fa fa-calendar form-control-feedback left" aria-hidden="true" style="top:15px"></i>
								</div>
							</div>
						<!-- <div class="col-md-2 col-sm-1 col-xs-12">
							End Date<span class="required"></span>
							<div class="control-group"> 
								<! -- <input type="text" name="p_tgl_akhir" id="tgl_akhir" value="<?php echo (isset($app['ref2']))?$app['ref2']:date("Y-m-t", strtotime(date("Y-m-d"))) ?>" class="form-control has-feedback-left datepickers active" validate="" aria-describedby="tgl_akhir" rel="p_tgl_akhir"><i class="fa fa-calendar form-control-feedback left" aria-hidden="true" style="top:15px"></i> -- >
									<select class="select2 form-control" name="p_distributor" id="distributor">
										<option value="all" >All</option>
										<?php// while ($row=db::fetch($rs['distributor'])){?>
											<!-- <option value="<?php echo $row['id']; ?>" <?php echo (preg_match("/".$row['id']."/i",$app['ref3'])?'selected':null); ?>><?php echo $row['name']; ?>
								</option> -- >
							<?php// } ?>
									</select>
								</div>
							</div> -->
						<!-- <div class="col-md-2 col-sm-1 col-xs-12">
							End Date<span class="required"></span>
							<div class="control-group"> 
								
									<select class="select2 form-control" name="p_distributor" id="distributor">
										<option value="all" >All</option>
									</select>
								</div>
							</div> -->

					<div class="col-md-2 col-sm-1 col-xs-12">
					Jenis Form<span class="required"></span></label>
							<div class="control-group">
								<select class="select2 form-control" name="p_jenis_form" id="jenis_form">
									<option value="all" >All</option>
									<option value="form_abnormal_si" <?= (($_REQUEST['jenis_form'] == "form_abnormal_si")?"selected":null) ?> >Informasi abnormal sistem informasi</option>
									<option value="form_acc_folder" <?= (($_REQUEST['jenis_form'] == "form_acc_folder")?"selected":null) ?>>Permintaan akses folder File Sharing Server</option>
									<option value="form_cpassword" <?= (($_REQUEST['jenis_form'] == "form_cpassword")?"selected":null) ?>>Pengajuan penggantian password</option>
									<option value="form_get_soft" <?= (($_REQUEST['jenis_form'] == "form_get_soft")?"selected":null) ?>>Memasukkan/mengeluarkan data softcopy</option>
									<option value="form_hardware_software" <?= (($_REQUEST['jenis_form'] == "form_hardware_software")?"selected":null) ?>>Permintaan Hardware & Software</option>
									<option value="form_pengajuan_user" <?= (($_REQUEST['jenis_form'] == "form_pengajuan_user")?"selected":null) ?>>Pengajuan user komputer</option>
									<option value="form_perubahan_si" <?= (($_REQUEST['jenis_form'] == "form_perubahan_si")?"selected":null) ?>>Permintaan perubahan sistem informasi</option>
									<option value="form_recovery_data" <?= (($_REQUEST['jenis_form'] == "form_recovery_data")?"selected":null) ?>>Permintaan recovery data</option>
									<option value="form_register_device" <?= (($_REQUEST['jenis_form'] == "form_register_device")?"selected":null) ?>>Registrasi perangkat penyimpanan data</option>
									<option value="form_tmp_access" <?= (($_REQUEST['jenis_form'] == "form_tmp_access")?"selected":null) ?>>Pengajuan temporary access</option>
								</select>
								<input type="hidden" style="display: none;" name="act" id="act" value="data">
							</div>
					</div>
					<div class="col-md-2 col-sm-1 col-xs-12">
					Section<span class="required"></span></label>
							<div class="control-group">
								<select class="select2 form-control" name="p_section" id="section">
									<option value="all" >All</option>
									<?php 
									$rs['section'] = db::get_record_select("id, name", "section", "status='active'");
									while ($row=db::fetch($rs['section'])){?>
										<!-- <option value="<?php echo $row['id']; ?>"  -->
										<option value="<?php echo $row['id']; ?>" <?= (($_REQUEST['section'] == $row['id'])?"selected":null) ?>><?php echo $row['name']; ?>
										</option> 
						<?php } ?>
								</select>
							</div>

							
					</div>
					<div class="col-md-2 col-sm-3 col-xs-12">
					Status<span class="required"></span></label>
							<div class="control-group">
								<select class="select2 form-control" name="p_status" id="status">
									<option value="all" <?= ($_REQUEST['status_p'] =="all" ?"selected":"") ?> ><?= app::getliblang("table_all") ?></option>
									<option value="unverified" <?= ($_REQUEST['status_p'] =="unverified" ?"selected":"") ?> ><?= app::getliblang("table_unverified") ?></option>
									<option value="partly" <?= ($_REQUEST['status_p'] =="partly" ?"selected":"") ?> ><?= app::getliblang("table_partly") ?></option>
									<option value="fully" <?= ((empty($_REQUEST['status_p']) || $_REQUEST['status_p'] =="fully") && $app['me']['level']=="1" ?"selected":"") ?> ><?= app::getliblang("table_fully") ?></option>
									<option value="rejected" <?= ($_REQUEST['status_p'] =="rejected" ?"selected":"") ?> ><?= app::getliblang("table_rejected") ?></option>
									<option value="accepted" <?= ( $_REQUEST['status_p'] =="accepted" || (empty($_REQUEST['status_p']) || $_REQUEST['status_p'] =="accepted") && $app['me']['level']=="3" ?"selected":"") ?> ><?= app::getliblang("table_accepted") ?></option>
									<option value="progress" <?= ($_REQUEST['status_p'] =="progress" ?"selected":"") ?> ><?= app::getliblang("table_progress") ?></option>
									<option value="finished" <?= ($_REQUEST['status_p'] =="finished" ?"selected":"") ?> ><?= app::getliblang("table_finished") ?></option>
									
								</select>
							</div>

							
					</div>
					<button type="submit" style="margin-top: 18px;" class="btn btn-warning" onclick="onsearch2()">Filter</button>
					<button type="submit" class="btn " style="background-color: #17703f !important;margin-top: 18px;color: white;" onclick="onexport()"> <img src="/cmwi-eform/webapp/src/assets/imgs/imgs/excel_icon.png" style="margin-left: -1px;" width="21px" ><span style="margin-left: 8px;"> Export to excel</span></button>
				<!-- </form> -->
		            <div>
            </div>
            </div>
        </div>

		<div class="notif-msg"></div>
		<div class="clearfix"></div>
				<div class="x_content">
					<!-- <div id="grid">Wait...</div> -->
					<div id="grid"><img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" /></div>
				</div>
			</div>
          </div>
        </div>
        <!-- /page content -->
<link rel="stylesheet" type="text/css" href="<?php echo $app['_scripts'] ?>/tablesorted/theme.default.min.css">
<script type="text/javascript" src="<?php echo $app['_scripts'] ?>/tablesorted/jquery.tablesorter.min.js"></script>
<script src="<?php  echo $app['_scripts'] ?>/jqui/jquery-ui.js"></script>
<script type="text/javascript">
	function onsearch2() {
		var start_date = $('#tgl_awal').val();
		var end_date = $('#tgl_akhir').val();
		var jenis_form = $('#jenis_form').val();
		var section = $('#section').val();
		var status_p = $('#status').val();
		var link;
		link = "<?= $app['http'] ?>/<?= "report_form" ?>.mod";
		if(start_date){
			link += "&start_date="+start_date;
		}	
		if(end_date){
			link += "&end_date="+end_date;
		}	
		if(jenis_form){
			link += "&jenis_form="+jenis_form;
		}	
		if(section){
			link += "&section="+section;
		}	
		if(status_p){
			link += "&status_p="+status_p;
		}	
		// alert(link);
		location.href = link; 
		// } 
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
	function onexport() {
		var start_date = $('#tgl_awal').val();
		var end_date = $('#tgl_akhir').val();
		var jenis_form = $('#jenis_form').val();
		var section = $('#section').val();
		var section = $('#section').val();
		var status_p = $('#status').val();
		var link;
		link = "<?= $app['http'] ?>/<?= "report_form" ?>.mod&act=export";
		if(start_date){
			link += "&start_date="+start_date;
		}	
		if(end_date){
			link += "&end_date="+end_date;
		}	
		if(jenis_form){
			link += "&jenis_form="+jenis_form;
		}	
		if(section){
			link += "&section="+section;
		}	
		if(status_p){
			link += "&status_p="+status_p;
		}	
		// alert(link);
		location.href = link; 
		// } 
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
	// onsearch2();
	function onkonfirm(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin meng approve "+name+" ?");
		if (r == true) { 
			location.href = href; 
		} 
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
	function on_reject(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin mereject ?");
		if (r == true) { 
			location.href = href; 
		} 
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
	function onkonfirm_reject(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin meng approve "+name+" ?");
		var z = confirm("Apakah Anda yakin mereject "+name+" ?");
		if (r == true) { 
			location.href = href; 
		} else{
			if(z == true){
				alert(href+"&reject=1");
				location.href = href+"&reject=1"; 
			}
		}
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
	function onkonfirm_note(href=null,name=null) {
		var txt;
		var r = confirm("Apakah Anda yakin meng approve "+name+" ?");
		if (r == true) { 
			// location.href = href; 
			var item =null;
			item = prompt("Note", "");
			if (item != null) {
			}
			location.href = href+"&note="+item;
		} 
		// else{
		// 	if(z == true){
		// 		alert(href+"&reject=1");
		// 		location.href = href+"&reject=1"; 
		// 	}
		// }
		return false
		// document.getElementById("demo").innerHTML = txt;
	}
  $(document).ready(function() {
  	function tombolKirimBeacukai(id_import,status,pib){
		var confirmnya = confirm('Apakah Anda Yakin Ingin Mengirim Data Import ini ?');
		if (confirmnya==true) {
			// $.get('<?php echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>', { act : "status",  id : id_pelatihan,status : status} );
	        var postingan =	$.post('<?php echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>', { act : "send_email_beacukai",  id : id_import,status : status,pib : pib } );
			postingan.done(function( data ) {
				// alert("Hide");
				// location.reload(); 
				// document.location.href = '<?php //echo $app['webmin'] ."/".admlib::$page_active['module'].".mod"; ?>';
				// return false;
				document.location.href = "<?php echo $app['webmin']."/".admlib::$page_active['module'].".mod"; ?>";
			});
		}
}
	 var checkItem = function(){
		var chkall      = $('#checkall');
		var checkitem 	= $('.checkitem');
		chkall.bind('change',function(){
			if(this.checked)
			{
				checkitem.prop('checked', true);
			}
			else
			{
				checkitem.prop('checked', false);
			}
		});
		checkitem.bind('change',function(){
			if(this.checked)
			{
				$('.'+ $(this).attr('identity')).prop('checked', true);
			}
			else
			{
				$('.'+ $(this).attr('identity')).prop('checked', false);
			}
		});
	},
	ckeckDel = function(){
		$('#delete').click(function(){
			 if($('.checkitem:checked').length > 0)
			{
					var lsform = $('#listForm');
					lsform.attr('action', $(this).attr('action')).submit();
					return true;
			}
			else
			{
				$(".notif-msg").html('<?php echo msg::display_notif_msg("warning", app::getliblang('opt_first') ) ?>');
			}
		});
	};
	$('#duplicate').click(function(){
		if($('.checkitem:checked').length > 0)
		{
			var lsform = $('#listForm');
			lsform.attr('action', $(this).attr('action')).submit();
			return false;
		}
		else
		{
			$(".notif-msg").html('<?php echo msg::display_notif_msg("warning", app::getliblang('opt_first') ) ?>');
		}
	});
	<?php if( $success ){ ?>
		$(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::getliblang('success')." ".$success ) ?>');
	<?php } ?>
	<?php if( $error ){ ?>
		$(".notif-msg").html('<?php echo msg::display_notif_msg("danger", app::getliblang('error')." ".$error ) ?>');
	<?php } ?>
      var loadData = function(url,args){
          $.ajax({
              url: url,
              type : 'POST', cache: false,
              data : args,
              dataType : 'html',
              success: function(data) {
                  $('#grid').html(data);
					$("#table-grid,.table-grid").tablesorter({
						widgets        : ['columns'],
						usNumberFormat : false,
						sortReset      : true,
						sortRestart    : true
					});
					$( "#sortable" ).sortable({
						update: function() {
							$.post( $('#listForm').attr('action') , $(this).sortable("serialize") +"&act=reorder",function(data){
									if(data){
										window.location.href=$('#listForm').attr('action');
										$(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::i18n('success_reorder') ) ?>');
									}
							});
						}
					});
                  checkItem();
                  ckeckDel();
                  $('.pagination a').click(function(){
                      loadData( $(this).attr('href') , {act:'data', ref:$('#key').val()});
                      return false;
                  });
              }
        });
      };
<?php if ($search_date=='yes') { ?>

   //    	$('.datepickers2').datepicker({
   //    	  // showOn: "button",
   //     	 //  buttonText: "kalender",
   //     	  dateFormat: 'yy-mm-dd'
   //      }, function(start, end) {
			// loadData(_url, {act:'data', ref:$('#key').val()});
   //      });
        $('.datepickers2').daterangepicker({
      	  // showOn: "button",
       	 //  buttonText: "kalender",
          singleDatePicker: true,
          calender_style: "picker_3",
		  // format: 'YYYY-MM-DD'
		  format: 'DD-MM-YYYY'
        }, function(start, end) {
			loadData(_url, {act:'data', ref:$('#key').val()});
        });
<?php } ?>
		var _url = $('#listForm').attr('action');
		loadData(_url, {act:'data', ref:''});

		// search awal
		// $('#key').keyup(function(){
		// 	loadData(_url, {act:'data', ref:$(this).val()});
		// });
		<?php if ($custom_search == "import") { ?>
        $('.datepickers2').daterangepicker({
      	  // showOn: "button",
       	 //  buttonText: "kalender",
          singleDatePicker: true,
          calender_style: "picker_3",
		  // format: 'YYYY-MM-DD'
		  format: 'DD-MM-YYYY'
        }, function(start, end) {
        	// alert(_url+"data"+$('#key').val());
			// loadData(_url, {act:'data', ref:$('#key').val()});
        });
		$('#submit_search').click(function(){
			// alert("disini");
			// alert("search:"+$("#key").val()+" & stat_bea:"+$("#p_stat_bea").val()+" & p_customer_s:"+$("#p_customer").val()+" & p_tgl_nopen_s:"+$("#p_tgl_nopen").val()+" & p_stat_kir:"+$("#p_stat_kir").val());
            $('#grid').html('<img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" />');
			loadData(_url, {act:'data', act_search:"import", p_search:$("#key").val(), p_stat_bea:$("#p_stat_bea").val(), p_customer_s:$("#p_customer").val(), p_tgl_nopen_s:$("#p_tgl_nopen").val(), p_stat_kir:$("#p_stat_kir").val()});
		});
		<?php }elseif($custom_search == "beacukai"){ ?>
			$('#submit_search').click(function(){
            	$('#grid').html('<img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" />');
				loadData(_url, {act:'data', act_search:"beacukai", p_search:$("#key").val(), p_stat_bea:$("#p_stat_bea").val(), p_customer_s:$("#p_customer").val()});
			});
		<?php }elseif($custom_search == "trucking"){ ?>
	        $('.datepickers2').daterangepicker({
	      	  // showOn: "button",
	       	 //  buttonText: "kalender",
	          singleDatePicker: true,
	          calender_style: "picker_3",
			  // format: 'YYYY-MM-DD'
			  format: 'DD-MM-YYYY'
	        }, function(start, end) {
	        	// alert(_url+"data"+$('#key').val());
				// loadData(_url, {act:'data', ref:$('#key').val()});
	        });
			$('#submit_search').click(function(){
				// alert("disini");
				// alert("search:"+$("#key").val()+" & stat_bea:"+$("#p_stat_bea").val()+" & p_customer_s:"+$("#p_customer").val()+" & p_tgl_nopen_s:"+$("#p_tgl_nopen").val()+" & p_stat_kir:"+$("#p_stat_kir").val());
	            $('#grid').html('<img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" />');
				loadData(_url, {act:'data', act_search:"trucking", p_search:$("#key").val(), p_customer_s:$("#p_customer").val(), p_tgl_nopen_s:$("#p_tgl_nopen").val()});
			});
		<?php }elseif($custom_search == "ret_con"){ ?>
				$('.datepickers2').daterangepicker({
					  // showOn: "button",
					 //  buttonText: "kalender",
				  singleDatePicker: true,
				  calender_style: "picker_3",
				  // format: 'YYYY-MM-DD'
				  format: 'DD-MM-YYYY'
				}, function(start, end) {
					// alert(_url+"data"+$('#key').val());
					// loadData(_url, {act:'data', ref:$('#key').val()});
				});
				$('#submit_search').click(function(){
					// alert("disini");
					// alert("search:"+$("#key").val()+" & stat_bea:"+$("#p_stat_bea").val()+" & p_customer_s:"+$("#p_customer").val()+" & p_tgl_nopen_s:"+$("#p_tgl_nopen").val()+" & p_stat_kir:"+$("#p_stat_kir").val());
				    $('#grid').html('<img src="<?php echo $app['www']."/src/assets/imgs/static/Spinner-1s-200px.gif" ?>" alt="animated" />');
					loadData(_url, {act:'data', act_search:"ret_con", p_search:$("#key").val(), p_customer_s:$("#p_customer").val(), p_tgl_nopen_s:$("#p_tgl_nopen").val()});
				});
		<?php }else{ ?>
			$('#key').keyup(function(){
				loadData(_url, {act:'data', ref:$(this).val()});
			});
		<?php } ?>

});

</script>
<?php admlib::display_block_footer(); ?>
