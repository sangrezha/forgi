<?php admlib::display_block_header(); 
admlib::get_component('datepickerlib');
admlib::get_component('select2lib');
$rs['distributor'] = db::get_record_select("id, CONCAT(company,' ',".db::dec("name").") AS name","distributor","status='active' ORDER BY ".db::dec("name")." ASC");
$rs['category'] = db::get_record_select("id, ".db::dec("name")." AS name","category","status='active' ORDER BY ".db::dec("name")." ASC");
?>
<style>

.table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>th {
    padding: 8px 20px 8px 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
}
</style>
 <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="tool_box add_fix">
                    <div class="pull-left">
                			  <?php if(admlib::acc('CRT')){ ?>
                  					<a href="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>&act=add">
                  						<button type="button" class="btn btn-default add"><i class="fa fa-plus"></i> Add</button>
                  					</a>
                  			<?php } ?>
                  			<?php //if(admlib::acc('CRT')){ ?>
                  				<!--	<a id="duplicate">
                  						<button type="button" class="btn btn-default copy"><i class="fa fa-copy"></i> Copy</button>
                  					</a> -->
                  			<?php //} ?>
                       <!--<a href="#"><button type="button" class="btn btn-default sort"><i class="fa fa-sort"></i> Sort</button></a>-->
                			   <a href="<?php echo $app['webmin'] .'/'. admlib::getext(); ?>" >
                					   <button type="button" class="btn btn-default sort"><i class="fa fa-refresh"></i> Refresh</button>
                				 </a>
                        <?php
                          if(count($app['buttons'])>0){
                            foreach($app['buttons'] as $button){
                        ?>
                             <a href="<?php echo $button['url']; ?>" >
                               <button type="button" class="btn btn-default sort"><i class="fa fa-<?php echo $button['icon']; ?>"></i> <?php echo $button['caption']; ?></button>
                             </a>
                        <?php
                            }
                          }
                        ?>
                    </div>
                    <div class="pull-right">
                				<?php if(admlib::acc('DEL')){ ?>
                					<a id="delete">
                						<button type="button" class="btn btn-default add"><i class="fa fa-trash"></i> Delete</button>
                					</a>
                  			<?php } ?>
                    </div>
                </div>
            </div>
        </div>
		</div>
		</div>
		<div class="title_left">
                <h3><?php echo admlib::$page_active['caption'] ?></h3>
            </div>
              
				<!--BARU-->
				<div class="notif-msg"></div>
				<div class="col-md-2 col-sm-3 col-xs-12">
					Start Date<span class="required"></span>
					<div class="controls">
						<input type="text" name="p_tgl_awal" id="tgl_awal" value="<?php echo date("Y-m-01", strtotime("-1 month",strtotime(date("Y-m-d")))) ?>" class="form-control has-feedback-left datepickers active" validate="" aria-describedby="tgl_awal" rel="tgl_awal"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" style="top:15px"></span>
						</div>
					</div>	
				<div class="col-md-2 col-sm-3 col-xs-12">
					End Date<span class="required"></span>
					<div class="controls"> 
						<input type="text" name="p_tgl_akhir" id="tgl_akhir" value="<?php echo date("Y-m-t", strtotime(date("Y-m-d"))) ?>" class="form-control has-feedback-left datepickers active" validate="" aria-describedby="tgl_akhir" rel="p_tgl_akhir"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" style="top:15px"></span>
						</div>
					</div>
					


				<div class="col-md-2 col-sm-3 col-xs-12">
				Distributor<span class="required"></span></label>
						<div class="control-group">
							<select class="select2 form-control" name="p_distributor" id="distributor">
								<option value="all" >All</option>
								<?php while ($row=db::fetch($rs['distributor'])){?>
									<option value="<?php echo $row['id']; ?>" <?php echo (preg_match("/".$row['id']."/i",$form['distributor'])?'selected':null); ?>><?php echo $row['name']; ?>
						</option>
					<?php } ?>
							</select>
						</div>
						
				</div>
				<div class="col-md-2 col-sm-3 col-xs-12">
				Category<span class="required"></span></label>
						<div class="control-group">
							<select class="select2 form-control" name="p_category" id="category">
								<option value="all" >All</option>
								<?php while ($row=db::fetch($rs['category'])){?>
									<option value="<?php echo $row['id']; ?>" <?php echo (preg_match("/".$row['id']."/i",$form['category'])?'selected':null); ?>><?php echo $row['name']; ?>
						</option>
					<?php } ?>
							</select>
						</div>
						
				</div>
				  <div class="col-md-2 col-sm-3 col-xs-12">
				  Search<span class="required"></span></label>
            		<div class="input-group">
            			<input type="text" placeholder="NCR ID / DN" class="form-control" id="dn" placeholder="<?php echo app::getliblang('search'); ?>"/>
            		</div>
                </div>
				<div class="col-md-1 col-sm-3 col-xs-12">
				&nbsp;&nbsp;&nbsp;
				<span class="required"></span></label>
					<button type="button" class="btn btn-primary" id="tombol"><?php echo (admlib::$page_active['module'] == "reportcomplaint"?"Filter":"Search") ?></button>
				</div>
				<?php if(admlib::acc('EXP')){ ?>
				<div class="col-md-1 col-sm-3 col-xs-12">
				&nbsp;&nbsp;&nbsp;
				<span class="required"></span></label>
					<button type="button" class="btn btn-success" id="export-excel">Export</button>
				</div>
				<?php } ?>
				<!-- SINI -->


		<div class="clearfix"></div>
			<div class="x_content">
				<div id="grid">Wait...</div>
			</div>	
    </div>
        <!-- /page content -->
<link rel="stylesheet" type="text/css" href="<?php echo $app['_scripts'] ?>/tablesorted/theme.default.min.css">
<script type="text/javascript" src="<?php echo $app['_scripts'] ?>/tablesorted/jquery.tablesorter.min.js"></script>
<script src="<?php  echo $app['_scripts'] ?>/jqui/jquery-ui.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
	 var checkItem = function(){
		var chkall      = $('#checkall');
		var checkitem 	= $('.checkitem');
		chkall.bind('change',function(){
			if(this.checked)
			{
				checkitem.prop('checked', true);
			}
			else
			{
				checkitem.prop('checked', false);
			}
		});
	},
	ckeckDel = function(){
		$('#delete').click(function(){
			 if($('.checkitem:checked').length > 0)
			{
				var lsform = $('#listForm');
				lsform.attr('action', lsform.attr('action') + '&act=delete').submit();
				return false;
			}
			else
			{
				$(".notif-msg").html('<?php echo msg::display_notif_msg("warning", app::getliblang('opt_first') ) ?>');
			}
		});
	};
	$('#duplicate').click(function(){
		if($('.checkitem:checked').length > 0)
		{
			var lsform = $('#listForm');
			lsform.attr('action', lsform.attr('action') + '&act=duplicate').submit();
			return false;
		}
		else
		{
			$(".notif-msg").html('<?php echo msg::display_notif_msg("warning", app::getliblang('opt_first') ) ?>');
		}
	});
	<?php if( $success ){ ?>
		$(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::getliblang('success')." ".$success ) ?>');
	<?php } ?>
	<?php if( $error ){ ?>
		$(".notif-msg").html('<?php echo msg::display_notif_msg("danger", app::getliblang('error')." ".$error ) ?>');
	<?php } ?>
      var loadData = function(url,args){
          $.ajax({
              url: url,
              type : 'POST', cache: false,
              data : args,
              dataType : 'html',
              success: function(data) {
                  $('#grid').html(data);
					$("#table-grid,.table-grid").tablesorter({
						widgets        : ['columns'],
						usNumberFormat : false,
						sortReset      : true,
						sortRestart    : true
					});
					$( "#sortable" ).sortable({
						update: function() {
							$.post( $('#listForm').attr('action') , $(this).sortable("serialize") +"&act=reorder",function(data){
								if(data) $('#refresh').trigger('click');
								if(data) $(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::getliblang('success_reorder') ) ?>');
							});
						}
					});
                  checkItem();
                  ckeckDel();
                  $('.pagination a').click(function(){
                      loadData( $(this).attr('href') , {act:'data', ref:$('#key').val()});
                      return false;
                  });
              }
        });
      };
		var _url = $('#listForm').attr('action');
		loadData(_url, {act:'data', ref:''});
		$('#keys').keyup(function(){
			loadData(_url, {act:'data', ref:$(this).val()});
		});
//
function tombol(){
		var distributor = $('#distributor').val();
		var category = $('#category').val();
		var tgl_awal = $('#tgl_awal').val();
		var tgl_akhir = $('#tgl_akhir').val();
		var dn = $('#dn').val();
		$('#listForm').html('Loading...');
		var _url = $('#listForm').attr('action');
		loadData(_url, {act:'data', ref3:distributor, ref4:category, ref1:tgl_awal, ref2:tgl_akhir, ref5:dn});
		// alert (distributor);
		// alert(category)
		// alert(tgl_awal)
		// alert(tgl_akhir)
	}
	$('#tombol').click(function(){
		tombol();
	});
	function exportexcel(){
		var distributor = $('#distributor').val();
		var category = $('#category').val();
		var tgl_awal = $('#tgl_awal').val();
		var tgl_akhir = $('#tgl_akhir').val();
		var dn = $('#dn').val();
		var _url = $('#listForm').attr('action');
		 // $.post(_url, {act:'export', p_id_distributor:distributor, p_id_category:category, p_start_date:tgl_awal, p_end_date:tgl_akhir, p_search:dn});
		 window.location.href = _url+"&act=export&p_start_date="+tgl_awal+"&p_end_date="+tgl_akhir+"&p_id_distributor="+distributor+"&p_id_category="+category;
	}
	$('#export-excel').click(function(){
		exportexcel();
	});


		

});

</script>
<?php admlib::display_block_footer(); ?>
