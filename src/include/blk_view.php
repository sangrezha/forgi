<?php 
admlib::display_block_header();
admlib::get_component('formstart')
?>
		<div class="row">
			<div class="col-md-10">
				<ul class="collection">
					<li class="collection-item">
						<blockquote>
							<?php echo app::getliblang('view');  ?>
						</blockquote>
					</li>
					<?php while($row=db::fetch($rs['row'])){ ?>
						<li class="collection-item">
							<?php echo $app['http']."/".db::lookup("alias","lang","id",$row['lang'])."/".$row['alias']."/"; ?>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
<?php 
admlib::get_component('formend');
admlib::display_block_footer();

?>