<form method="post" id="listForm" action="<?php echo $app['webmin'] ."/". admlib::getext(); ?>">
	<table id="table-grid" class="table tablesorter">
		<thead>
			<tr>
				<?php if(admlib::acc('DEL') || !empty($app['me']['level'])){ ?>
					<th data-sorter="false"><input type="checkbox" id="checkall" /></th>
				<?php }else{ ?>
					<th data-sorter="false">No</th>
				<?php } ?>
				<?php if((admlib::acc('UPDT') || !empty($app['me']['level'])) AND empty($option['no_edit'])){
						if(!isset($option)){ ?>
				<th data-sorter="false" >&nbsp;</th>
						<?php }elseif(isset($option)){
							?>
				<th>&nbsp;</th>			
							<?php
						}?>
				<?php } ?>
				<?php if(admlib::acc('UPDT') || !empty($app['me']['level'])){ ?>
				 <?php if(!isset($statusx) AND $option['status']==""){ ?>
					  <th data-sorter="false"><center><?php echo app::getliblang('status'); ?></center></th>		 
							<?php } ?>
				<?php } ?>
				<?php foreach($columns as $column){ ?>
					<?php // if ($column!="melakukan_apa"){ ?>
						<th><?php echo app::getliblang($column); ?></th>
					<?php// }else{ ?>
						<!-- <th colspan="3"><?php echo app::getliblang($column); ?></th> -->
					<?php// } ?>
				<?php } ?>
				<?php if(!isset($unpost)){ ?>
				<th><?php echo app::getliblang('created_by'); ?></th>
				<th><?php echo app::getliblang('created_at'); ?></th>
				<?php } ?>
				<?php if(!isset($unmodify)){ ?>
				<th><?php echo app::getliblang('updated_by'); ?></th>
				<th><?php echo app::getliblang('updated_at'); ?></th>
				<?php } ?>
			</tr>
		</thead>
		<tbody id="<?php echo ($option['sortable'])? "sortable":null ?>">
			<?php if($nr['row'] > 0){
			$no=1; ?>
				<?php foreach($results as $row){ ?>
						<tr id="reorder_<?php echo stripslashes($row['id']) ?>">
							<?php if(admlib::acc('DEL') || !empty($app['me']['level'])){ ?>
								<td align="center"><input type="checkbox" class="checkitem" name="p_del[]" value="<?php echo $row['id']; ?>" /></td>
							<?php }else{ ?>
								<td><?php echo $no++; ?></td>
							<?php } ?>
							<?php if((admlib::acc('UPDT') || !empty($app['me']['level'])) && empty($option['no_edit'])){ ?>
								<td align="center"><a title="Edit" href="<?php echo admlib::getext(); ?>&act=edit&id=<?php echo $row['id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
							<?php } ?>
							<?php if(admlib::acc('CRT') || !empty($app['me']['level'])){ ?>
								<?php if(!isset($option)){ ?>
							<!--	<td align="center"><a title="Duplicate" href="<?php echo admlib::getext(); ?>&act=duplicate&id=<?php echo $row['id']; ?>"><i class="fa fa-copy" aria-hidden="true"></i></a></td> -->
								<?php } ?>
							<?php } ?>

							<?php if(admlib::acc('UPDT') || !empty($app['me']['level'])){ ?>
							<?php if(!isset($statusx) AND $option['status']==""){ ?>
							  <?php if($row['status'] == 'active'){ ?>
									<td align="center" title="Aktif" class="status_yes"><a href="<?php echo admlib::getext(); ?>&act=status&id=<?php echo $row['id']; ?>&status=<?php echo $row['status']; ?>"><i style="color:#4CAF50" class="fa fa-check" aria-hidden="true"></i></a></td>
							  <?php }elseif($row['status'] == "read"){ ?>
							  <td align="center" title="read" class="status_yes"><a href="#"><i style="color:#4CAF50" class="fa fa-envelope-open-o" aria-hidden="true"></i></a></td>
							  <?php }elseif($row['status'] == "unread"){ ?>
							  <td align="center" title="unread" class="status_yes"><a href="#"><i style="color:#4CAF50" class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
							  <?php }else{ ?>
									<td align="center" title="Tidak Aktif" class="status_no"><a href="<?php echo admlib::getext(); ?>&act=status&id=<?php echo $row['id']; ?>&status=<?php echo $row['status']; ?>"><i style="color:#F44336" class="fa fa-close" aria-hidden="true"></i></a></td>
							  <?php } ?>
							<?php } ?>
							<?php } ?>
<?php if ($option['bold']==""){ ?>
		<?php foreach($columns as $column){ ?>
		<td style="text-align:left;"><?php echo (isset($row[$column])?$row[$column]:""); ?></td>
							<?php } 
}else{
?>
		<?php foreach($columns as $column){ 
		if ($column=="positions") {
echo "<td style='text-align:left;'>";
			foreach (explode(",", $row[$column]) as $key => $value) {
	if ($key==0)
	echo"<span style='color:red'>$value</span>";
	else
	echo"$value";
			// print_r($key." adalah ".$value);
	}
echo "</td>";
		}else{
			?>
		<td style="text-align:left;"><?php echo (isset($row[$column])?$row[$column]:""); ?></td>
		<?php }
	}
} ?>
	




							<?php if(!isset($unpost)){ ?>
							<td><?php echo $row['created_by']; ?></td>
							<td>
								<?php echo date_format(date_create($row['created_at']), 'd/m/Y H:i:s'); ?>
								</td>
							<?php } ?>
							<?php if(!isset($unmodify)){ ?>
							<td><?php echo $row['updated_by']; ?></td>
							<td>
								  <?php
										if(isset($row['updated_at']) AND $row['updated_at'] != '0000-00-00 00:00:00')
										{
											  echo date_format(date_create($row['updated_at']), 'd/m/Y H:i:s');
										}
								  ?>
							</td>
							<?php } ?>
					  </tr>
				<?php } ?>
		  <?php }else{ ?>
				<tr><td colspan="<?php echo 8+count($columns); ?>" align="center"><?php echo app::getliblang('nodata'); ?></td></tr>
		  <?php } ?>
		</tbody>
	</table>
</form>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<nav>
			<?php if( $nr['row'] < $total){ ?>
				<a class="btn btn-primary" href="<?php echo (isset(admlib::$page_active['module'])?admlib::$page_active['module'].'.mod':admlib::$page_active['system'] .'.do'); ?>&all=1"><?php echo app::getliblang('showall'); ?></a>
			<?php } ?>
				<div class="pagination">
					<?php echo $nr['row'] .' - '. app::getliblang('from') .' '. $total; ?>
				</div>
			</nav>
		</div>
		<div class="col-md-9">
			<nav class="pull-right">
				<div class="pagination btn-group">
				<?php echo $nav->navbar(); ?>
				</div>
			</nav>
		</div>
	</div>
</div>
