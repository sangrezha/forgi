<div class="container">
	<div class="row">
		<div class="col-md-3">
			<nav>
			
			<?php if( (int) $nr['row'] < (int) $total){ 
				if(isset($_GET['all']) && $_GET['all'] ==1){
					?>
					<a id="btn-show-page" class="btn btn-primary" href="<?php echo (isset(admlib::$page_active['module'])?admlib::$page_active['module'].'.mod':admlib::$page_active['system'] .'.do'); ?>"><?php echo app::getliblang('back'); ?></a>
					<?php
				}else{ ?>
				<a id="tampil_semua" class="btn btn-primary" href="<?php echo (isset(admlib::$page_active['module'])?admlib::$page_active['module'].'.mod':admlib::$page_active['system'] .'.do'); ?>&all=1"><?php echo app::getliblang('showall'); ?></a>
				<?php } ?>
				
			<?php } ?>
			
				<div class="pagination">
					<?php echo $nr .' - '. app::getliblang('from') .' '. $total; ?>
				</div>
			</nav>
		</div>
		<div class="col-md-9">
			<nav class="pull-right">
				<div class="pagination btn-group">
				<?php echo $nav->navbar(); ?>
				</div>
			</nav>
		</div>
	</div>
</div>
<script>
/* $(document).ready(function(){
	if(<?php echo $_GET['all']; ?> == 1){
		$("#tampil_semua").hide();
		$("#tampil_sebagian").show();
	}
	$("#btn-show-page").on("click",function(){
		$(this).hide();
		$("#tampil_semua").show();
	});
});  */
</script>