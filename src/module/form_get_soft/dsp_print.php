<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM MENGELUARAN SOFT COPY</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">
    <section class="sheet padding-10mm">
        <table width="100%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;font-size: 12px;">
            <tbody>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td width="65%">&nbsp;</td>
                </tr>
                <tr>
                    <td>	        	
                        <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                    <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 0px;color:#fff;">	        	
                    FORM MEMASUKKAN/MENGELUARAN <br>
                        DATA SOFT COPY PERUSAHAAN
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="70%" style="font-size:12px;float:left;" cellpadding="3">
                            <tbody>
                                <tr style="">
                                    <td>Jenis Aktivitas</td>
                                    <td>:</td>
                                    <td colspan="7">
                                    <input type="radio" id="baru" name="p_rework" value="js" <?= ($form['jenis_data']=="Memasukkan Data"?"checked":"") ?> disabled>&nbsp;
                                    <label for="baru">Memasukan Data</label>&nbsp;
                                    <input type="radio" id="Rework" name="p_rework" value="rework" <?= ($form['jenis_data']=="Mengeluarkan Data"?"checked":"") ?> disabled>&nbsp;
                                    <label for="Rework">Mengeluarkan Data</label></td>
                                </tr>	
                                <tr>
                                    <td width="25%">Pemohon</td>
                                    <td width="5%">:</td>
                                    <td width="70%"><?php 
                                    $Section = db::lookup("name","section","id",$form['id_section']);
                                    $nik = db::lookup("nik","member","id",$form['created_by']);
                                    echo $nik." - ".$form['name_p']." - ".$section; ?></td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td>:</td>
                                    <td><?=  ucwords($jabatan) ?></td>
                                </tr>
                                <tr>
                                    <td>Tgl Pelaksanaan</td>
                                    <td>:</td>
                                    <td><?=  $form['tgl_pengeluaran'] ?></td>
                                </tr>
                                <tr>
                                    <td align="top">Tujuan</td>
                                    <td align="top">:</td>
                                    <td><?=  $form['tujuan_0'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <!-- <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                            <tbody>
                                <tr align="center">
                                    <td>
                                        Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
                                            
                    
                                        </td>
                                </tr>
                            </tbody>
                        </table> -->
                        <table cellspacing="1" cellpadding="3" style="font-size: 12px;" align="right" width="30%">
                        <tbody>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $form['no_pengajuan'] ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Revisi</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                            </tr>
                       </tbody>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:14px; background-color:#e6e6e6;padding:5px;" cellpadding="3">
                            <tbody>
                                <tr>
                                    <td width="25%">Media</td>
                                    <td width="5%">:</td>
                                    <td width="70%"><?= strtoupper(str_replace(";"," - ",$form['media'])) ?></td>
                                </tr>
                                <tr>
                                    <td>Perangkat</td>
                                    <td>:</td>
                                    <td>
                                    <?= strtoupper($form['perangkat']) ?>
                                    </td> 
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:12px;" cellpadding="5">
                            <tbody>
                                <tr align="center">
                                    <td style="background:#10069f;color:#fff;" width="5%">No</td>
                                    <td style="background:#10069f;color:#fff;" width="75%">Deskripsi</td>
                                    <td style="background:#10069f;color:#fff;" width="20%">Jenis_file</td>
                                </tr>
                                <?php 
                                $no = 1;
                                while($row = db::fetch($rs['adding_soft'])){
                                ?>
                                <tr>
                                    <td style="background:#e6e6e6;color:#222;" align="center"><?= $no ?></td>
                                    <td style="background:#e6e6e6;color:#222;"><?= $row['deskripsi'] ?></td>
                                    <td style="background:#e6e6e6;color:#222;" align="center"><?= ucwords($row['jenis_file']) ?></td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:12px;" cellpadding="5">
                            <tbody>
                                <tr align="center">
                                  <td width="24%" style="background:#000;color:#fff;">Pemohon</td>
                                  <td width="24%" style="background:#000;color:#fff;">Manager</td>
                                  <td width="4%">&nbsp;</td>
                                  <td width="24%" style="background:#000;color:#fff;">Staff IT</td>
                                  <td width="24%" style="background:#000;color:#fff;">Manager IT</td>
                                </tr>
                                <tr align="center" style="height:75px;">
                                  <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td><span style="font-size:20px;font-weight:bold;">&#8594;</span></td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr align="center">
                                  <td style="background:#000;color:#fff;"><?= $form['name_p'] ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                                  <td></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
</body>
</html>