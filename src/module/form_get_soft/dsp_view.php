<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	?>
	<style>
			.monthselect{
				color: black;
			}
			.yearselect{
				color: black;
			}
		</style> <?php
	admlib::get_component('select2lib');
	admlib::get_component('uploadlib');
	admlib::get_component('datepickerlib');
	admlib::get_component('formstart');
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"status_f",
		// 		"datas"=>["baru","ada"],
		// 		"value"=>app::ov($module['status_f'])
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"name_f",
		// 		"value"=>app::ov($module['name_f']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"type"=>"hidden",
		// 		"name"=>"name",
		// 		"value"=>app::ov($module['name']),
		// 	)
		// );
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"status_f",
		// 		"datas"=>["baru","ada"],
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['status_f'])
		// 	)
		// );
		// echo "disini";
		// print_r($_SESSION["add_soft"]);
		if($form['no_pengajuan']){
			admlib::get_component('view',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"validate"=>true,
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('view',
			array(
				"name"=>"name_p",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['name_p'])
			)
		);
		// print_r($app['me']);
		// $rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		// $departement = db::lookup("name","departement","id",$form['id_departement']);
		// admlib::get_component('view',
		// 	array(
		// 		"name"=>"id_departement",
		// 		"value"=>app::ov($departement)
		// 	)
		// );
		// $rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		$section = db::lookup("name","section","id",$form['id_section']);
		admlib::get_component('view',
			array(
				"name"=>"id_section",
				"value"=>app::ov($section)
			)
		);	
		$title = db::lookup("name","title","id",$form['id_title']);
		admlib::get_component('view',
			array(
				"name"=>"id_title",
				"value"=>app::ov($title)
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"tgl_pengeluaran",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['tgl_pengeluaran'])
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"tujuan_0",
				"validate"=>true,
				"value"=>app::ov($form['tujuan_0'])
			)
		);
		// $rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		// admlib::get_component('select',
		// 	array(
		// 		"name"=>"id_departement",
		// 		"value"=>app::ov($app['me']['id_departement']),
		// 		"items"=>$rs['departement']
		// 	)
		// );
		/*admlib::get_component('checkbox',
			array(
				"name"=>"media",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"datas"=>["fd","hdd","cdrw","dvdrw","pc","cloud"],
				"value"=>app::ov($form['media'])
			)
		);*/
		admlib::get_component('select',
			array(
				"name"=>"media",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "datas"=>["fd","hdd","cdrw","dvdrw","pc","cloud"],
				"readonly"=>"yes",
				"items"=>[["id"=>"Flash Disk","name"=>"Flash Disk (FD)"],["id"=>"HDD Eksternal","name"=>"HDD Eksternal(HDD)"],["id"=>"CD-R/RW","name"=>"CD-R/RW"],["id"=>"DVD-R/RW","name"=>"DVD-R/RW"],["id"=>"PC/Notebook","name"=>"PC/Notebook"],["id"=>"Cloud Storage","name"=>"Cloud Storage"]],
				"value"=>app::ov($form['media'])
			)
		);
/*		admlib::get_component('radio',
			array(
				"name"=>"perangkat",
				"datas"=>["cmwi","eksternal"],
				"validate"=>true,
				"value"=>app::ov($form['perangkat'])
			)
		);*/
		admlib::get_component('select',
			array(
				"name"=>"perangkat",
				// "datas"=>["cmwi","eksternal"],
				// "items"=>[["id"=>"cmwi","name"=>"cmwi"],["id"=>"eksternal","name"=>"eksternal"]],
				"items"=>[["id"=>"Perusahaan (CMWI)","name"=>"Perusahaan (CMWI)"],["id"=>"Eksternal","name"=>"Eksternal"]],
				// "validate"=>true,
				"readonly"=>"yes",
				"value"=>app::ov($form['perangkat'])
			)
		);
		if($form['perangkat']=="eksternal"){
			admlib::get_component('view',
				array(
					"name"=>"other_dev",
					"validate"=>true,
					"value"=>app::ov($form['other_dev'])
				)
		);
		}
		 ?>
		<div class="form-group">
		<label class="control-label col-md-1 col-sm-12 col-xs-12" for="tgl_countainer">&nbsp;</label>
		<!-- <div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="num_container">Container<span class="required"></span>		</label>
			<input type="text" name="p_num_container" id="num_container" value="" class="form-control has-feedback-left " validate="">
			<span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span>
		</div> -->
		<!-- <div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="jenis_container">Deskripsi data<span class="required"></span>		</label>
			<select id="nik" name="p_nik" class="select2 form-control">
				<option value="" disabled selected><?php echo app::i18n('select');  ?></option>
				<?php 	
				// $sql = "SELECT id,nik,name,id_section FROM ". $app['table']["member"] ."  ORDER BY nik asc	";
				// db::query($sql, $rs['row'], $nr['row']);
				// while($row = db::fetch($rs['row'])){ ?>
						<option value="<?= $row['id']; ?>"><?= $row['nik']; ?> - <?= $row['name']; ?></option>
				<?php// } ?>
		</select> -->

		</div>
		<!-- <div class="col-md-4 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="name_member">Deskripsi<span class="required"></span>		</label>
			<input type="text" name="p_deskripsi" id="deskripsi" value="" class="form-control " validate="">
			<! -- <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span> - - >
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="name_member">Jenis<span class="required"></span></label>
			<input type="text" name="p_jenis" id="jenis" value="" class="form-control " validate="">
			<! -- <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span> - - >
		</div> -->
		<!-- <div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="jenis_container">Section<span class="required"></span>		</label>
			<select id="id_section" name="p_id_section" class="form-control">
				<option value=""><?php echo app::i18n('select');  ?></option>
				<?php 	
				// $sql = "SELECT id,name FROM ". $app['table']["section"] ."  ORDER BY name asc";
				// db::query($sql, $rs['row'], $nr['row']);
				// while($row = db::fetch($rs['row'])){ ?>
				// 		<option value="<?= $row['id']; ?>"><?= $row['name']; ?></option>
				<?php //} ?>
			</select>
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label" for="jenis_container">Hak Akses<span class="required"></span>		</label>
			<select id="premission" name="p_premission" class="form-control">
				<option value="" selected disabled><?php echo app::i18n('select');  ?></option>
				<?php 	
				// $sql = "SELECT id,name FROM ". $app['table']["section"] ."  ORDER BY name asc";
				// db::query($sql, $rs['row'], $nr['row']);
				// while($row = db::fetch($rs['row'])){ ?>
						<option value="<?= "r" ?>">Read</option>
						<option value="<?= "w" ?>">Read & Write</option>
				<?php // } ?>
			</select>
		</div> -->
		<!-- <div style="margin-top:28px;">
			<! -- <button class="btn btn-success" onclick="adding_soft()">add</button> -- >
			<a href="#" class="btn btn-success" onclick="adding_soft()">add</a>
		</div> -->
		<br>
				<div id="user_total"></div>
		
<?php 

		// admlib::get_component('submit',
		// 	array(
		// 		"id"=>(isset($id))?$id:"",
		// 		"no_submit"=>"yes",
		// 		"act"=>$act
		// 	)
		// );
		if ($app['me']['level']=="1") {
			admlib::get_component('submit_aprove',
				array(
					"id"=>(isset($id))?$id:"",
					// "no_submit"=>"iya",
					"act"=>"approve_it"
				)
			);
		}
		if ($app['me']['level']=="3") {
			admlib::get_component('submit_aprove',
				array(
					"id"=>(isset($id))?$id:"",
					// "no_submit"=>"iya",
					"act"=>"approve_it_3"
				)
			);
		}
	admlib::get_component('formend');
	?>
	
<script>

$(document).ready(function () {

	<?php //if(empty($form['used_sys']) || $form['used_sys'] == "other"){ 
		  if($form['perangkat'] != "Eksternal"){ 
		?>
		$("#g_other_dev").hide();
	<?php } ?>
			$('#id_departement').each(function(){
				var id_section = $(this).val();
				var dis = '<?= $form['id_section'] ?>';
				// alert(dis);
				// console.log(dis);
				var urlcustomer = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
				// var urlmaker = "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_customer;
				// console.log("ID " + urlcustomer);
			get_section(id_section, dis, urlcustomer);
				// get_makermodel(id_customer, dis, urlmaker);
			});

			$('#id_departement').change(function(){
				// alert("disini");
				$('#id_section').select("val","null");
				// $('#id_title').select("val","null");
				var id_section = $(this).val();
				var dis = '';
				// console.log(dis);
				var urlsection = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
				// alert(urlsection);
				// var urlmaker = "<?php //echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_departement;
				// console.log("ID section: " + urlsection);
				// console.log("ID Maker: " + urlmaker);
				get_section(id_section, dis, urlsection);
				// get_makermodel(id_section, dis, urlmaker);
			});

	<?php // if(empty($form['status_f']) || $form['status_f'] == "ada"){ ?>
		// $("#g_function").hide();
	<?php // } ?>
		// $('input[type=radio][name=p_status_f]').change(function() {
		// 	if (this.value == 'baru') {
		// 		// alert("Allot Thai Gayo Bhai");
		// 		$("#g_function").show();
		// 	}
		// 	else if (this.value == 'ada') {
		// 		// alert("Transfer Thai Gayo");
        //       	$("#g_function").hide();
		// 	}
		// });
		$('#nik').change(function() {
			// alert("<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value);
			$.ajax({
			type:"GET",
			url:"<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value,
				success:function(res){         
						var result = JSON.parse(res);
						if(result.error == 0){
							// var loop = "";
							// console.log(result);12 
							// "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section
							console.log(JSON.stringify(result));
							// alert(JSON.stringify(result));
							// alert(result.member.id_section);
							$("#id_m").val(result.member.id);
							$("#name_m").val(result.member.name);
							$("#nik_m").val(result.member.nik);
							$("#name_member").val(result.member.name);
							$("[name=p_id_section]").val(result.member.id_section);
							// $('select[name="p_id_section"]').find('option:contains("'+result.member.id_section+'")').attr("selected",true);


							// loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
							// $.each(result.section,function(key,value){
							// 	loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
							// });
							// $("#id_title").html(loop);
							// console.log("loop " + loop);
						}else{
							alert("error");
							// $("#id_title").html('');
						}
				}
			}); 
		});
});

function get_section(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						// console.log(result); 
						// if(dis){
						// 	loop = "<option disabled><?php echo app::i18n('select');?></option>";
						// }else{
							loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						// }
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						// alert(loop);
						$("#id_section").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_section").html('');
						$("#id_title").html('');
					}
			}
		});     
	}
		function addData2(act, param){
			// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>');
			// alert(param);
			var _param = $.extend({ act : act, step : 	'add' }, param);
			// alert('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			// console.log('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			console.log("<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>/"+JSON.stringify(_param));
			$.post('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){ loadData() });
			
			// $.ajax({
			// type:"POST",
			// url:"<?php echo admlib::$page_active['module'] ?>.mod",
			// data:_param,
			// 	success:function(res){ 
			// 		loadData();
			// 	}
			// }); 
			return false;
		}
		function adding_soft(){
			var act = "adding_soft", deskripsi = $('#deskripsi').val(), jenis_file = $('#jenis').val();
			// alert(jenis_file);
			// alert("disini");
			// alert(act);
			// alert(name);
			// alert(id);
			// alert(id_section);
			// alert(nik);
			if(!act || !deskripsi || !jenis_file){
				// if (!pib || !cont) 
				// {
					alert('tidah boleh kosong');
					return;
				// }
			}
			addData2(act, {id: Math.floor(Math.random() * 100000000000000000), deskripsi : deskripsi, jenis_file : jenis_file });
			return false;
		
    };
	function loadData(){
		$('#user_total').load('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : "adding_soft" });
	};
	loadData();
	function delete_member(id_member){
		var r = confirm("Apakah anda yang ingin menghapus ?");
		// alert(id_member);
		// var _param = $.extend({ act : "adding_soft", step : 'delete', id : id_member }, param);
		var _param = { act : "adding_soft", step : 'delete', id : id_member };
		// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>'+_param);
		if (r == true) {
			$.post('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){  loadData(); });
		}
		return false;

	}
</script>
	<?php
admlib::display_block_footer();
?>