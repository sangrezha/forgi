<?php
$container = $_SESSION[$act];
if(count($container) > 0){ 
?>
<div class="table-responsive"> 
    <table class="table table-bordered"> 
        <thead> 
            <tr> 
                <th>#</th> 
                <th>Container</th> 
                <th>QTY</th> 
                <th>Keterangan</th>
                <th>Nilai</th>  
				<th>&nbsp;</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
                $total = 0;
                $no = 0;
                foreach($container as $key => $val){ $no++;
                    $total = $total + $val['n'];     
            ?>
                <tr> 
                    <th width="5%" scope="row"><?php echo $no;?></th> 
                    <td><?php echo $val['c'];?></td> 
                    <td><?php echo $val['j'];?></td> 
                    <td><?php echo $val['d'];?></td> 
                    <td>Rp <?php echo number_format($val['n'], 2, ',', '.'); ?></td> 
                    <td width="5%"><a href="#" act="<?php echo $act;?>" val="<?php echo $key;?>" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></td>
                </tr> 
            <?php 
                    }
            ?>
        </tbody> 
    </table> 
</div>
<?php } ?>