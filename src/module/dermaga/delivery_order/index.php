<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'delivery_order','caption'=>'Delivery Order');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$option = ['sortable'=>true];
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'delivery_order', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.id, a.status, a.no_do as no_do, a.date_receipt_do as date_receipt_do, b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
	FROM ". $app['table']['delivery_order'] ." a
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id) $q ORDER BY a.reorder ASC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['send','no_do','date_receipt_do','expired_do'];
	while($row = db::fetch($rs['row']))
	{
		$row['send'] = '<a title="View" href="/dermaga/delivery_order.mod?act=send&id='.$row['id'].'"><center><i class="fa fa-paper-plane" aria-hidden="true"></i></a>';
		$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
		$datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
		$row['expired_do']			= $datas['do'];
		$results[] 					= $row;
		if (strtotime($row['expired_do']) < time()) {
			$id_exp [] = $datas['id_exp'];
		}
		else{
			$id_exp_act [] = $datas['id_exp'];	
		}
	}
	$id_exp = implode("','", $id_exp);
	$id_exp_act = implode("','", $id_exp_act);
	$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
	$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
	db::qry($sql321);

	db::qry($sql1231);
	$option = ['sortable'=>true];
	include $app['pwebmin'] ."/include/blk_list_mod.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		$validate ='p_no_do,p_date_receipt_do,p_expired_do';
		form::serialize_form();
		form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		if (strtotime($p_expired_do) < time()) {
			$status_do = ",'expired'";	
		}
		$id = rand(1, 100).date("dmYHis");
		$ids = rand(1, 100).date("dmYHis");
		app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
		$reorder = db::lookup("max(reorder)","delivery_order","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$sql = "insert into ".$app['table']['delivery_order']."
				(id, no_do, date_receipt_do, id_expired_do, reorder, created_by, created_at,status) values
				('$id', '$p_no_do', '$p_date_receipt_do', '$ids', '$reorder', '". $app['me']['id'] ."', now()$status_do)";	
		$sqls = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id', '$p_expired_do', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		db::qry($sqls);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record_filter("id, no_do, date_receipt_do ","delivery_order", "id", $id);
		$rs['exp_date'] = db::get_record_select("expired_do", "expired_do", "id_delivery_order='$id' order by reorder ASC");
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		form::validate('empty','p_no_do,p_date_receipt_do');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		if (strtotime($p_expired_do) > time()) {
			$status_do = ",status = 'active' ";
		}
		else{
			$status_do = ",status = 'inactive' ";	
		}
		app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
		$sql = "update ". $app['table']['delivery_order'] ."
				set no_do 				= '$p_no_do',
					date_receipt_do 	= '$p_date_receipt_do',
					updated_by 			= '". $app['me']['id'] ."',
					updated_at 			= now()
					$status_do
				where id = '$id'";
		db::qry($sql);
		if(!empty($p_expired_do)){
		$ids = rand(1, 100).date("dmYHis");
		$sqls = "update ".$app['table']['expired_do']." set
				 status = 'inactive' where id_delivery_order = '".$id."' ";
		db::qry($sqls);
		$sqlss = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id', '$p_expired_do','". $app['me']['id'] ."', now())";
		db::qry($sqlss);
		}
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "expired";
	elseif ( $status == "expired" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['delivery_order']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['delivery_order']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT id,no_do as title FROM ". $app['table']['delivery_order'] ." WHERE id IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql 	= "DELETE FROM ". $app['table']['delivery_order'] ." WHERE `id` IN ('". $delid ."')";
// exit;
		db::qry($sql);
		$sqls 	= "DELETE FROM ". $app['table']['expired_do'] ." WHERE `id_delivery_order` IN ('". $delid ."')";
		db::qry($sqls);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;
?>
