<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'import','caption'=>'Import');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$_SESSION['container'] 	= null;
	$option = ['sortable'=>true];
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'import', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.id, a.status, a.num_pib AS num_pib, a.num_bl as num_bl, a.name_ship as name_ship,  b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
	FROM ". $app['table']['import'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id) $q ORDER BY a.reorder ASC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['num_pib','num_bl','name_ship'];
	while($row = db::fetch($rs['row']))
	{
		$results[] 		= $row;
	}
	$option = ['sortable'=>false];
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		$validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen';
		form::serialize_form();
		form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		if(count($container)>0)
		{
			foreach($container as $row){
			$ids = rand(1, 100).date("dmYHis");
				$t_container 	= $t_container+$row['n'];
				$reorders = db::lookup("max(reorder)","container","1=1");
				if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
				$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now())";
				db::qry($sqlx);
			}
		}
		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_date_receipt,p_amount_payment,p_nopen');
		$reorder = db::lookup("max(reorder)","import","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$sql = "insert into ".$app['table']['import']."
				(id, num_pib, num_bl, name_ship, eta, id_customer, id_company, date_receipt, amount_payment, nopen, reorder, created_by, created_at) values
				('$id', '$p_num_pib', '$p_num_bl', '$p_name_ship', '$p_eta', '$p_customer_name', '$p_company', '$p_date_receipt', '$p_amount_payment', '$p_nopen', '$reorder', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
		
		$rs['container'] = db::get_record_select("num_container as c, qty as j, note as d, value as n", "container", "id_import", $id);
		$_SESSION['container'] = [];
		while($row = db::fetch($rs['container'])){
			$_SESSION['container'][] = $row;
		}
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		// print_r($_SESSION['income']);
		form::validate('empty','p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		if(count($container)>0)
		{
			db::qry('DELETE FROM '. $app['table']['container'] .' WHERE id_import="'. $id .'"');
			foreach($container as $row){
			$ids = rand(1, 100).date("dmYHis");
				$t_container 	= $t_container+$row['n'];
				$reorders = db::lookup("max(reorder)","container","1=1");
				if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
				$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now())";
				db::qry($sqlx);
			}
		}
		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$sql = "update ". $app['table']['import'] ."
				set num_pib 		= '$p_num_pib',
					num_bl 			= '$p_num_bl',
					name_ship 		= '$p_name_ship',
					eta		 		= '$p_eta',
					id_customer 	= '$p_customer_name',
					id_company	 	= '$p_company',
					date_receipt 	= '$p_date_receipt',
					amount_payment 	= '$p_amount_payment',
					nopen		 	= '$p_nopen',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['import']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['import']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT id,num_pib as title FROM ". $app['table']['import'] ." WHERE id IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql 	= "DELETE FROM ". $app['table']['import'] ." WHERE `id` IN ('". $delid ."')";
		$sqls 	= "DELETE FROM ". $app['table']['container'] ." WHERE `id_import` IN ('". $delid ."')";
		db::qry($sql);
		db::qry($sqls);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;

if($act == 'container')
{	
	if($step == 'add')
	{
		$nominal = str_replace('Rp ', '', $nom);
		$nominal = str_replace(',', '', $nominal);
		$_SESSION['container'][] = ['c'=>$num, 'j'=>$jenis, 'd'=>$desc, 'n'=>$nominal];
		exit;
	}elseif($step == 'delete'){
		unset($_SESSION['container'][$indx]);
		exit;
	}
	include "dsp_list.php";
}
?>
