<?php admlib::display_block_header(); 

admlib::get_component('select2lib');
admlib::get_component('datepickerlib');
?>
<script language="javascript" type="text/javascript" src="<?php echo $app['www'] ?>/_scripts/slick/slick.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<style>
p {
    margin: 8px 0px 0px;
}
.equalsheight .control-label{
    position: relative;
    min-height: 1px;
    float: left;
    padding-right: 3px;
    padding-left: 6px;
	width: 31%;
}
.equalsheight input[type=text],input[type=number]{width:205px;}
.equalsheight{position:relative;}
/*input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}*/
input[type=number]::-webkit-outer-spin-button,
input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance:textfield;
}
.gm-style .place-card-large {
    display: none !important;
}
.shorcut a {transition: all 0.5s ease;background-color: #fff;}
</style>
<div class="right_col" role="main">
	<div class="">
		<div class="row">
			  <div class="col-md-12 col-sm-12 col-xs-12">
			
				<div class="x_panel">
					<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="COMDET">
						<h2 style="float:none;text-align:center;color:#666">History Complaint</h2>
					</div>
					<div class="clearfix"></div>
				</div>
				  
				 <div class="x_content">
				<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
<?php

		$html ='
		
			NCR ID &nbsp;:<b> '.$rowkl['id'].'</b><br />
			DN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<b> '.$rowkl['dn'].'</b><br />
		
		';
		// print_r($data);
		foreach($data as $form){
	
		$html .='
			<div class="list-group">
				<div class="list-group-item"> <b>'.$form['title'].'</b><br />
				'.$form['note'].'<br />
				<font size="2">'.$form['post_date'].'</font></div>
			</div>
					
		';
		}
	echo $html;	
	?>
						</form>
					</div>
				</div>
			</div>
		</div>
<!-- END APPROVAL Result -->
	</div><!--right col-->

	</div>
</div>
<script>
$(document).ready(function(){
	
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none",
		iframe: {
			preload: false
		}
    });
	
	
});
   
</script>
<?php
admlib::display_block_footer();
?>