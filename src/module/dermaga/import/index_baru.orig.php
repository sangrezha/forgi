<?php 
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib', 'api');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'import','caption'=>'Import');
$app['config'] = db::get_record("configuration", "id", 1);

/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		$validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen';
		form::serialize_form();
		form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		if(count($container)>0)
		{
			foreach($container as $row){
			$ids = rand(1, 100).date("dmYHis");
				$t_container 	= $t_container+$row['n'];
				$reorders = db::lookup("max(reorder)","container","1=1");
				if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
				$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now())";
				db::qry($sqlx);
			}
		}
		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_date_receipt,p_amount_payment,p_nopen');
		$reorder = db::lookup("max(reorder)","import","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$sql = "insert into ".$app['table']['import']."
				(id, num_pib, num_bl, name_ship, eta, id_customer, id_company, date_receipt, amount_payment, nopen, reorder, created_by, created_at) values
				('$id', '$p_num_pib', '$p_num_bl', '$p_name_ship', '$p_eta', '$p_customer_name', '$p_company', '$p_date_receipt', '$p_amount_payment', '$p_nopen', '$reorder', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'complaint', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$unpost = 1;
	$unmodify = 1;
	
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.id, a.status, a.num_pib AS num_pib, a.num_bl as num_bl, a.name_ship as name_ship,  b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
	FROM ". $app['table']['import'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id) $q ORDER BY a.reorder ASC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['num_pib','num_bl','name_ship','created_at','status','created_by','step','note'];
	$columns = array_merge($columns,['view']);
	$thstyle = ["status"=>"background-color:#ffdf00;color:#666;","created_by"=>"background-color:#ffdf00;color:#666;","step"=>"background-color:#ffdf00;color:#666;","note"=>"background-color:#ffdf00;width:150px;color:#666;"];
	while($row = db::fetch($rs['row']))
	{
		$datas = db::get_record("complaint_status","id_complaint='".$row['ncr_id']."' AND type='".$row['approve_by']."' ORDER BY created_at DESC");
		$row['step']			= app::getliblang($row['approval']);
		$row['created_by']			= app::getliblang($row['approve_by']);
		if($row['code_cat'] == "TB" && $row['approval'] == "qc"){
			$row['step']			= app::getliblang('logistic');
		}
		if($row['code_cat'] == "TB" && $row['approve_by'] == "qc"){
			$row['created_by']			= app::getliblang('logistic');
		}
		$row['note']			= $datas['note'];
		$row['created_at']			= app::format_datetime($row['created_at'],"ina","MMM");
		$row['status']			= app::getliblang($row['status']);
		if(admlib::acc('APPR')){
			if($row['status_crm'] == "yes"){
				$row['status_crm']	= $row['no_crmmax'];
			}else{				
				$row['status_crm']	= "-";
			}
			
		}
		$row['view']				= '<center><a title="CRM MAX" href="'.admlib::getext().'&act=view&id='.$row['id'].'"><i class="fa fa-eye" aria-hidden="true"></i></a></center>';
		$results[] 	= $row;
	}
	// $sortable = true;
	$statusx = true;
	include $app['pwebmin'] ."/include/blk_list_complaint.php";
	exit;
endif;

/*******************************************************************************
* Action : View
*******************************************************************************/
if($act == 'view'):
	// admlib::validate('APPR');
	admlib::validate('DSPL');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
		$rs['container'] = db::get_recordset("container","id_import='".$id."'");
		$do = db::get_record("delivery_order", "id_import", $id);
		$rs['exp_date'] = db::get_record_select("expired_do", "expired_do", "id_delivery_order='".$do['id']."' order by reorder ASC");
		
		$rs['containers'] = db::get_record_select(" id, CONCAT(num_container,', ',qty,', ',note) AS num_container" , "container","id_import='".$id."' AND id NOT IN ('". $cont ."') AND status='stay' ORDER BY reorder ASC");
		$rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS armada" , "armada","status='stay' ORDER BY reorder ASC");
		form::populate($form);
		include "dsp_view.php";
		exit;
	endif;

	if ($step == 2):
		admlib::validate('APPR');
		form::serialize_form();
		// form::validate('empty', 'p_no_do,p_date_receipt_do,p_expired_do');
		// if (form::is_error()):
			// msg::build_msg();
			// header("location: ". admlib::$page_active['module'] .".mod&act=view&error=1&id=" . $id);
			// exit;
		// endif;
		$do = db::get_record("delivery_order", "id_import", $id);
		// print_r($_POST);
		// echo "<br>";
		// echo "<br>";
		// echo count($do);
		// print_r($do);
		// exit;
		
		if($do['id_import'] == $id){
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$sql = "update ". $app['table']['delivery_order'] ."
					set no_do 				= '$p_no_do',
						date_receipt_do 	= '$p_date_receipt_do',
						updated_by 			= '". $app['me']['id'] ."',
						updated_at 			= now()
					where id = '".$do['id']."'";
			db::qry($sql);
			if(!empty($p_expired_do)){
				$ids = rand(1, 100).date("dmYHis");
				$sqls = "insert into ".$app['table']['expired_do']."
						(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '".$do['id']."', '$p_expired_do', '". $app['me']['id'] ."', now())";
				db::qry($sqls);
			}
		}else{
			$ids = rand(1, 100).date("dmYHis");
			$idx = rand(1, 100).date("dmYHis");
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$reorder = db::lookup("max(reorder)","delivery_order","1=1");
			if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
			$sql = "insert into ".$app['table']['delivery_order']."
					(id, id_import, no_do, date_receipt_do, id_expired_do, reorder, created_by, created_at) values
					('$ids', '$id', '$p_no_do', '$p_date_receipt_do', '$idx', '$reorder', '". $app['me']['id'] ."', now())";
			db::qry($sql);
			if(!empty($p_expired_do)){
				$sqls = "insert into ".$app['table']['expired_do']."
						(id, id_delivery_order, expired_do, created_by, created_at) values ('$idx', '$ids', '$p_expired_do', '". $app['me']['id'] ."', now())";
				db::qry($sqls);
			}
		}
		
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;

/*******************************************************************************
* Action : Trucking
*******************************************************************************/
if($act == 'trucking'):
	// admlib::validate('APPR');
	// admlib::validate('DSPL');
	// form::init();
	

	
endif;




?>
