<?php
$income = $_SESSION[$act];
if(count($income) > 0){ 
admlib::get_component('datepickerlib');
?>
<div class="table-responsive"> 
    <table class="table table-bordered"> 
        <thead> 
            <tr> 
                <th>#</th> 
                <th>Container</th> 
                <th>QTY</th> 
                <th>Keterangan</th>
                <th>Nilai</th>  
                <th>Delivery Date</th>  
                <th>Address</th>  
                <?php if($act == 'expense'){ ?>
                    <th>Expense Type</th> 
                <?php } ?>
                <!-- <th>&nbsp;</th> -->
                <th>Delete</th> 
                <th>Update</th>
            </tr> 
        </thead> 
        <tbody> 
            <?php 
                $total = 0;
                $no = 0;
                foreach($income as $key => $val){ $no++;
                    $total = $total + $val['n'];     
            ?>
                <tr> 
                    <th width="5%" scope="row"><?php echo $no;?></th> 
                    <td >
                        <!-- <div class="col-md-12 col-sm-6 col-xs-12"> -->
                    <input  type="text" style="width:100px" name="p_container" id="container" value="<?php echo $val['c'] ?>" class="" validate="">
                    <!-- <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true"></span> -->
                <!-- </div> -->
        </td>
                          <td >
                        <!-- <div class="col-md-12 col-sm-6 col-xs-12"> -->
                    <input  type="text" style="width:50px" name="p_qty" id="qty" value="<?php echo $val['j'] ?>" class="" validate="">
                    <!-- <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true"></span> -->
                <!-- </div> -->
        </td> 
                    <td>
 <div class="col-md-12 col-sm-6 col-xs-12">
        <textarea class="form-control" id="keterangan" value=""><?php echo $val['d'] ?></textarea>
               </div>
            </div>
        </td> 
                    <td>   
<!--             <div class="form-group" >
                <div class="col-md-12 col-sm-6 col-xs-12"> -->
                    <input type="text" style="width:100px" name="p_nilai" id="nilai" value="<?php echo $val['n'] ?>" class="money" validate="">
                    <!-- <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span> -->
                <!-- </div>
            </div> -->
        </td> 

<td> 
<!--     <div class="col-md-12 col-sm-12 col-xs-12" >      <fieldset >
          <div class="control-group">
            <div class="controls">
 -->                <input type="text"  style="width:100px" name="p_delivery_date" value="<?php echo $val['dd'] ?>" class=" datepickersingle" aria-describedby="delivery_date" >
             <!--    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" id="delivery_date"></span> -->
<!--                 <span id="<?php echo $param['name'] ?>" class="sr-only">(success)</span>
       </div>
          </div> 
        </fieldset>
                   <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
    </div>-->
</td>
     <td>
        <!-- <div class="col-md-12 col-sm-6 col-xs-12"> -->
        <textarea class="form-control" id="alamat"><?php echo $val['adr'] ?></textarea>
            <!-- </div> -->
        </td> 
                    <?php if($act == 'expense'){ ?>
                        <td><?php echo db::lookup('name', 'expense', 'id', $val['e']); ?></td> 
                    <?php } ?>
                    <td width="5%" align="center"><a href="#" act="<?php echo $act;?>" val="<?php echo $key;?>" class="delete"><span class="fa fa-trash" aria-hidden="true"></span></td>
                        <td width="5%" align="center"><a href="#" act="<?php echo $act;?>" val="<?php echo $key;?>" class="update"><span class="fa fa-pencil" aria-hidden="true"></span></td>
                </tr> 
            <?php 
                    }
            ?>
            <tr> 
                <th scope="row">&nbsp;</th> 
                <th scope="row">&nbsp;</th> 
                <td><b>Total</b></td> 
                <td><b>:</b></td> 
                <td><b>Rp <?php echo number_format($total, 2, ',', '.');?></b></td> 
                <td colspan="4">&nbsp;</td>
            </tr> 
        </tbody> 
    </table> 
</div>
<?php } ?>
<script>
        $(function(){
        var formatCurrency = function(number){
            var n = number.split('').reverse().join("");
            var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
            return 'Rp ' + n2.split('').reverse().join('');
        };
        $( document ).ready(function() {
        $('.money').on('input', function(e){
            $(this).val(formatCurrency(this.value.replace(/[,Rp ]/g,'')));
        }).on('keypress',function(e){
            if(!$.isNumeric(String.fromCharCode(e.which))) e.preventDefault();
        }).on('paste', function(e){
            var cb = e.originalEvent.clipboardData || window.clipboardData;
            if(!$.isNumeric(cb.getData('text'))) e.preventDefault();
        });
        $('#amount_payment').val(formatCurrency($('.money').val().replace(/[,Rp ]/g,'')));
    });
    });

</script>