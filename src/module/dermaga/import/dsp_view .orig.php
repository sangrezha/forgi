<?php admlib::display_block_header(); 

admlib::get_component('select2lib');
admlib::get_component('datepickerlib');
?>
<script language="javascript" type="text/javascript" src="<?php echo $app['www'] ?>/_scripts/slick/slick.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<style>
p {
    margin: 8px 0px 0px;
}
.equalsheight .control-label{
    position: relative;
    min-height: 1px;
    float: left;
    padding-right: 3px;
    padding-left: 6px;
	width: 31%;
}
.equalsheight input[type=text],input[type=number]{width:205px;}
.equalsheight{position:relative;}
/*input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}*/
input[type=number]::-webkit-outer-spin-button,
input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance:textfield;
}
.shorcut a {transition: all 0.5s ease;background-color: #fff;}
</style>
<div class="right_col" role="main">
	<div class="">
		<div class="row">
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<div class="btn-group navbar-static-top shorcut" role="group" style="top:10px;left:50%;width:auto">
					<a id="comdet" href="#COMDET" class="btn btn-default">Import</a>
					<a id="crmmax" href="#CRMMAX" class="btn btn-default">Shiping</a>
					<a id="apr" href="#APR" class="btn btn-default">Trucking</a>
                </div>
				<div class="x_panel">
					<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="COMDET">
						<h2 style="float:none;text-align:center;color:#666">Import</h2>
					</div>
					<div class="clearfix"></div>
				</div>
				  
				  <div class="x_content">
				<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
<?php
		admlib::get_component('view', 
					array(
						"name"=>"num_pib", 
						"value"=>app::ov($form['num_pib'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"num_bl", 
						"value"=>app::ov($form['num_bl'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"name_ship", 
						"value"=>app::ov($form['name_ship'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"eta",
						"value"=>app::format_datetime($form['eta'],"ina","MM")
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"customer_name", 
						"value"=>app::ov($form['id_customer'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"company", 
						"value"=>app::ov($form['id_company'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"date_receipt", 
						"value"=>app::format_datetime($form['date_receipt'],"ina","MM")
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"amount_payment", 
						"value"=>app::ov($form['amount_payment'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"nopen", 
						"value"=>app::format_datetime($form['nopen'],"ina","MM")
					)
				);
?>
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-12 col-xs-12" for="tgl_countainer">Container :</label>
				<div class="col-md-9 col-sm-12 col-xs-12">
					<div class="table-responsive"> 
						<table class="table table-bordered"> 
							<thead> 
								<tr> 
									<th>#</th> 
									<th>Container</th> 
									<th>QTY</th> 
									<th>Note</th>
								</tr> 
							</thead> 
							<tbody> 
								<?php 
									$no = 0;
									while($row = db::fetch($rs['container'])){ $no++;
										// print_r($row);     
								?>
									<tr> 
										<th width="5%" scope="row"><?php echo $no;?></th> 
										<td><?php echo $row['num_container'];?></td> 
										<td><?php echo $row['qty'];?></td> 
										<td><?php echo $row['note'];?></td> 
									</tr> 
								<?php 
										}
								?>
							</tbody> 
						</table> 
					</div>
				</div>
				
			</div>
<?php if(admlib::acc('APPR')){ ?>
					</div><!--x-content-->
				</div> <!--md12 detil complaint-->
				
						
						
						
						
				</form>
				
			</div> <!--row-->
		</div> <!--div kosong-->
<!-- END EDIT IMPORT -->

<!-- EDIT SHIPPING! -->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		
		<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="CRMMAX">
			<h2 style="float:none;text-align:center;color:#666">Shipping</h2>
		</div>
		<div class="clearfix"></div>
		
		<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="crmForm" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
	
<?php			
		
			admlib::get_component('inputtext',
				array(
					"name"=>"no_do",
					"value"=>app::ov($do['no_do']),
					"validate"=>"required"
				)
			);
			admlib::get_component('datepicker',
				array(
					"name"=>"date_receipt_do",
					"value"=>app::ov($do['date_receipt_do']),
					"validate"=>"required"
				)
			);
			admlib::get_component('datepicker',
				array(
					"name"=>"expired_do",
					"validate"=>"required"
				)
			);
		$exp = db::nr($rs['exp_date']);
		if($exp > 0){
?>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-12 col-xs-12" for="tgl_countainer">&nbsp;</label>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class="table-responsive"> 
						<table class="table table-bordered"> 
							<thead> 
								<tr> 
									<th>#</th> 
									<th>Expired DO</th> 
								</tr> 
							</thead> 
							<tbody> 
								<?php 
									$no = 0;
									while($row = db::fetch($rs['exp_date'])){ $no++;
								?>
									<tr> 
										<th width="5%" scope="row"><?php echo $no;?></th> 
										<td><?php echo $row['expired_do'];?></td> 
									</tr> 
								<?php 
										}
								?>
							</tbody> 
						</table> 
					</div>
				</div>
			</div>
<?php
		}
		admlib::get_component('submit',
			array(
				"id"=>isset($id)?$id:null,
				"act"=>$act
			)
		);
?>
		</form>
	</div>
</div>
<!-- END SHIPPING! -->

		
		<!-- EDIT TRUCKING! -->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		
		<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="APR">
			<h2 style="float:none;text-align:center;color:#666">Trucking</h2>
		</div>
		<div class="clearfix"></div>
		
		<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="crmForm" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
	
<?php			
		
		admlib::get_component('view',
			array(
				"name"=>"num_pib",
				"value"=>app::ov($form['num_pib'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"pib",
				"value"=>app::ov($form['id']),
				"type"=>"hidden"
			)
		);
?>		
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-12 col-xs-12" for="cont_trucking">&nbsp;</label>
			<div class="col-md-5 col-sm-12 col-xs-12">
				<label class="control-label col-md-4" for="cont_trucking"><?php echo app::getliblang('num_container'); ?><span class="required"></span>		</label>
				<select name="cont_trucking" id="cont_trucking" class="form-control">
					<option value="">Select</option>
					<?php while($row = db::fetch($rs['container'])){ ?>
					<option value="<?php echo $row['id'] ?>"><?php echo $row['num_container'] ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<label class="control-label col-md-2" for="arm_trucking"><?php echo app::getliblang('armada'); ?><span class="required"></span>		</label>
				<select name="arm_trucking" id="arm_trucking" class="form-control">
					<option value="">Select</option>
					<?php while($rows = db::fetch($rs['armada'])){ ?>
					<option value="<?php echo $rows['id'] ?>"><?php echo $rows['armada'] ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
			<div class="form-group">
			<label class="control-label col-md-3 col-sm-12 col-xs-12" for="add-trucking">&nbsp;</label>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<a href="#" class="btn btn-default add" val="trucking"><span class="fa fa-plus" aria-hidden="true"></span> Add</a>
				<br/>
				<div id="trucking"></div>
			</div>
		</div>
<?php
		admlib::get_component('submit',
			array(
				"id"=>isset($id)?$id:null,
				"act"=>"trucking"
			)
		);
?>
		</form>
	</div>
</div>
<!-- END TRUCKING! -->

<?php } ?>
	</div><!--right col-->

	</div>
</div>
<script>
<?php if($form['code_cat'] == "TB"){ ?>
document.getElementById('qc_process').nextSibling.nodeValue = 'Logistic Check';
<?php } ?>
$(document).ready(function(){
	$(function() { $('a[href*="#"]:not([href="#"])').click(function() { if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) { var target = $(this.hash); target = target.length ? target : $('[name=' + this.hash.slice(1) +']'); if (target.length) { $('html, body').animate({ scrollTop: target.offset().top-70}, 1000); return false; } } });});
	
	
	$("#comdet").click(function(){
        $(this).css('background-color', '#ffdf00');
        $('#crmmax').css('background-color', '#fff');
        $('#apr').css('background-color', '#fff');
    });
	$("#crmmax").click(function(){
		$(this).css('background-color', '#ffdf00');
        $('#comdet').css('background-color', '#fff');
        $('#apr').css('background-color', '#fff');
    });
	$("#apr").click(function(){
		 $(this).css('background-color', '#ffdf00');
        $('#comdet').css('background-color', '#fff');
        $('#crmmax').css('background-color', '#fff');
    });
	$('form').submit('click', function(e){
		var r = confirm("Apakah data yang anda masukan sudah benar");
		if (r === false) {
			e.preventDefault();
		}
    });
	
// var label = document.getElementById("qc_process");
// alert(label.innerHTML);
    // Select and loop the container element of the elements you want to equalise
    // $('.equalsheight').each(function(){  
      
      // Cache the highest
      var highestBox = 280;
      
      // Select and loop the elements you want to equalise
      $('.equalsheight', this).each(function(){
        
        // If this box is higher than the cached highest then store it
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }
      
      });  
            
      // Set the height of all those children to whichever was highest 
      $('.equalsheight',this).height(highestBox);
                    
      // Cache the highest
      var sheight = 100;
      
      // Select and loop the elements you want to equalise
      $('.sheight', this).each(function(){
        
        // If this box is higher than the cached highest then store it
        if($(this).height() > sheight) {
          sheight = $(this).height(); 
        }
      
      });  
            
      // Set the height of all those children to whichever was highest 
      $('.sheight',this).height(sheight);
                    
    // }); 

});
function OnlyNumbers(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;

    if (
        //0~9
        charCode >= 48 && charCode <= 57 ||
       //number pad 0~9
       // charCode >= 96 && charCode <= 105 ||
        //backspace
       charCode == 8 ||
        //tab
        charCode == 9 ||
        //enter
        charCode == 13 || 
        //left, right, delete..
        charCode >= 35 && charCode <= 46
    )
    {
        //make sure the new value below 20
        // if(parseInt(this.value+String.fromCharCode(charCode), 10) <= 20) 
            return true;
    }
    
    evt.preventDefault();
    evt.stopPropagation();
    
    return false;
}
$(document).ready(function(){
<?php if($form['approval'] == "sbo"){ ?>
$("*", "#qcForm").prop('disabled',true);
$("*", "#resultForm").prop('disabled',true);
<?php }elseif($form['approval'] == "qc"){ ?>
// $("*", "#sboForm").prop('disabled',true);
$("*", "#resultForm").prop('disabled',true);
<?php }elseif($form['approval'] == "result"){ ?>
$("*", "#sboForm").prop('disabled',true);
$("*", "#qcForm").prop('disabled',true);
<?php }else{ ?>
$("*", "#sboForm").prop('disabled',true);
$("*", "#qcForm").prop('disabled',true);
$("*", "#resultForm").prop('disabled',true);
<?php } ?>
});
$("input[name=p_accepted]").bind('keyup', function(e) {
	var max = <?php echo $form['qty'] ?>;
	var nilai = $(this).val();
	if(nilai > max){
		$(this).val(max);
		alert("Maximum Qty : "+max);
		var nilai = max;
	}
	var rejected = max - nilai;
	$("#qcForm input[name=p_rejected]").val(rejected);
	$("#qcForm input[name=p_rejected_val]").val(rejected);
});

$("#crmForm input[name=p_no_crmmax]").attr('maxlength', '10');
$("#crmForm input[name=p_no_crmmax]").bind('keypress', function(e) {OnlyNumbers(e)});
$("#qcForm input[name=p_accepted]").bind('keypress', function(e) {OnlyNumbers(e)});
$("#qcForm input[name=p_rejected]").prop('disabled',true);
$("#qcForm input[name=p_status]").click(function(){
	var status = $(this).val();
	if(status == "closed"){
		$("#g_accepted").show();
		$("#g_rejected").show();
	}else{
		$("#g_accepted").hide();		
		$("#g_rejected").hide();		
	}
});	
$("#resultForm input[name=p_status]").click(function(){
	var status = $(this).val();
	if(status == "closed"){
<?php if($qc['accepted'] > 0){ ?>
		$("#g_cn_number").show();
<?php }else{ ?>
		$("#g_cn_number").hide();		
<?php } ?>
	}else{
		$("#g_cn_number").hide();		
	}
});
$("#sboForm input[name=p_status]").click(function(){
	var status = $(this).val();
	if(status == "revised"){
		$("#g_revision_to").show();
	}else{
		$("#g_revision_to").hide();		
	}
});
<?php if($qc['status'] == "closed"){ ?>
		$("#g_accepted").show();
		$("#g_rejected").show();
<?php }else{ ?>
		$("#g_accepted").hide();
		$("#g_rejected").hide();
<?php } ?>
<?php if($result['status'] == "closed"){ ?>
<?php if($qc['accepted'] > 0){ ?>
		$("#g_cn_number").show();
<?php }else{ ?>
		$("#g_cn_number").hide();		
<?php } ?>
<?php }else{ ?>
		$("#g_cn_number").hide();
<?php } ?>

<?php if($sbo['status'] == "revised"){ ?>
		$("#g_revision_to").show();
<?php }else{ ?>
		$("#g_revision_to").hide();
<?php } ?>

		$(".slider").slick({
			adaptiveHeight: true
		});

$(document).ready(function(){
	
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
    $(".fancyboxdn").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
	
	
});
   
</script>
<?php
admlib::display_block_footer();
?>