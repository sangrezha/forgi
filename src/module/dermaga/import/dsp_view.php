<?php admlib::display_block_header(); 

admlib::get_component('select2lib');
admlib::get_component('datepickerlib');
?>
<script language="javascript" type="text/javascript" src="<?php echo $app['www'] ?>/_scripts/slick/slick.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<style>
p {
    margin: 8px 0px 0px;
}
.equalsheight .control-label{
    position: relative;
    min-height: 1px;
    float: left;
    padding-right: 3px;
    padding-left: 6px;
	width: 31%;
}
.equalsheight input[type=text],input[type=number]{width:205px;}
.equalsheight{position:relative;}
/*input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}*/
input[type=number]::-webkit-outer-spin-button,
input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance:textfield;
}
.shorcut a {transition: all 0.5s ease;background-color: #fff;}
</style>
<div class="right_col" role="main">
	<div class="">
		<div class="row">
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<div class="btn-group navbar-static-top shorcut" role="group" style="top:10px;left:50%;width:auto">
					<a id="comdet" href="#COMDET" class="btn btn-default">Import</a>
					<?php // if(admlib::acc('ASV')){ ?>
					<!-- <a id="crmmax" href="#CRMMAX" class="btn btn-default">Shiping</a> -->
					<?php // }
$record_beacukai = db::get_record("beacukai","id_import",$form['id']);
$record_pelayaran = db::get_record("delivery_order","id_import",$form['id']);
$record_pelabuhan = db::get_record("pelabuhan","id_import",$form['id']);

					if (admlib::acc('APPR') && $record_beacukai['id'] !="") { ?>
					<a id="apr" href="#Beacukai" class="btn btn-default">Beacukai</a>
				<?php } 
					if (admlib::acc('APPR') && $record_pelayaran['id'] !="") { ?>
					<a id="apr" href="#Pelayaran" class="btn btn-default">Pelayaran</a>
				<?php } 
					if (admlib::acc('APPR') && $record_pelabuhan['id'] !="") { ?>
					<a id="apr" href="#Pelabuhan" class="btn btn-default">Pelabuhan</a>
				<?php } #
					// if (admlib::acc('APPR') && $pelabuhan['kelengkapan'] !="") {
					if (admlib::acc('APPR') && $dok_kel !="") { ?>
					<a id="apr" href="#APR" class="btn btn-default">Trucking</a>
				<?php } ?>
                </div>
				<div class="x_panel">
					<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="COMDET">
						<h2 style="float:none;text-align:center;color:#666">Import</h2>
					</div>
					<div class="clearfix"></div>
				</div>
				  
				  <div class="x_content">
				<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
				<?php if(admlib::acc('APPR')){
$abc="abc";
include "dsp_form.php";
?>

<?php 
}else{
		admlib::get_component('view', 
					array(
						"name"=>"num_pib", 
						"value"=>app::ov($form['num_pib'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"num_bl", 
						"value"=>app::ov($form['num_bl'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"name_ship", 
						"value"=>app::ov($form['name_ship'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"eta",
						"value"=>app::format_datetime(date("d-m-Y",strtotime($form['eta'])),"ina","MM")
					)
				);
				// admlib::get_component('view', 
				// 	array(
				// 		"name"=>"eta",
				// 		"value"=>app::format_datetime($form['eta'],"ina","MM")
				// 	)
				// );
				admlib::get_component('view', 
					array(
						"name"=>"customer_name", 
						"value"=>app::ov($form['id_customer'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"company", 
						"value"=>app::ov($form['id_company'])
					)
				);
				// admlib::get_component('view', 
				// 	array(
				// 		"name"=>"date_receipt", 
				// 		"value"=>app::format_datetime(date("d-m-Y",strtotime($form['date_receipt'])),"ina","MM")
				// 	)
				// );
				admlib::get_component('view', 
					array(
						"name"=>"amount_payment", 
						"value"=>app::ov($form['amount_payment'])
					)
				);
				//app::format_datetime($form['nopen'],"ina","MM")
				admlib::get_component('view', 
					array(
						"name"=>"nopen", 
						"value"=>app::ov($form['nopen'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"status_beacukai", 
						"value"=>$form['status_beacukai']
					)
				);
?>
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-12 col-xs-12" for="tgl_countainer">Container :</label>
				<div class="col-md-9 col-sm-12 col-xs-12">
					<div class="table-responsive"> 
						<table class="table table-bordered"> 
							<thead> 
								<tr> 
									<th>#</th> 
									<th>Container</th> 
									<th>QTY</th> 
									<th>Note</th>
									<th>Address</th>
									<th>Delivery Date</th>
								</tr> 
							</thead> 
							<tbody> 
								<?php 
									$no = 0;
									while($row = db::fetch($rs['container'])){ $no++;
										// print_r($row);     
								?>
									<tr> 
										<th width="5%" scope="row"><?php echo $no;?></th> 
										<td><?php echo $row['num_container'];?></td> 
										<td><?php echo $row['qty'];?></td> 
										<td><?php echo $row['note'];?></td> 
										<td><?php echo $row['address']; ?></td>
										<td><?php $row['delivery_date'] != "0000-00-00" AND $row['delivery_date'] != ""?$date_contnya = date("d-m-Y",strtotime($row['delivery_date'])):$date_contnya = "<span style='font-size:350%;'><center><b>-</b></center></span>";echo $date_contnya ; ?></td>
									</tr> 
								<?php 
										} 
								?>
							</tbody> 
						</table> 
					</div>
				</div>
				
			</div>
		<?php } ?>
					</div><!--x-content-->
				</div> <!--md12 detil complaint-->
				
						
						
						
						
				</form>
				
			</div> <!--row-->
		</div> <!--div kosong-->
<!-- END EDIT IMPORT -->

<!-- EDIT SHIPPING! -->

<?php /*if(admlib::acc('ASV')){*/
 ?>
<!-- <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		
		<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="CRMMAX">
			<h2 style="float:none;text-align:center;color:#666">Shipping</h2>
		</div>
		<div class="clearfix"></div> -->
		
<!-- 		<form method="post" action="<?php // echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="crmForm" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
 -->		<!-- <form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="crmForm" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
<input type='hidden' name='id_imp' value='<?php echo app::ov($form['id']) ?>'/>
<input type='hidden' name='id_idv_ord' value='<?php echo $do['id'] ?>'/>
<input type='hidden' name='me' value='<?php echo $app['me']['id'] ?>'/> -->
 <?php // if ($do['no_do']=="") echo "<input type='hidden' name='step' value='add'/>";
// 		else echo "<input type='hidden' name='step' value='edit'/>";
// 			admlib::get_component('inputtext',
// 				array(
// 					"name"=>"no_do",
// 					"value"=>app::ov($do['no_do']),
// 					"validate"=>"required"
// 				)
// 			);
// 			admlib::get_component('inputtext',
// 				array(
// 					"name"=>"id_imp",
// 					"type"=>"hidden",
// 					"value"=>app::ov($form['id'])
// 				)
// 			);
// 			(isset($do['date_receipt_do'])?$date_receipt_do321 = date("d-m-Y",strtotime($do['date_receipt_do'])):null);
// 			admlib::get_component('datepicker',
// 				array(
// 					"name"=>"date_receipt_do",
// 					"value"=>app::ov($date_receipt_do321),
// 					"validate"=>"required"
// 				)
// 			);
// 			// if ($total2 < 3) {
// 				admlib::get_component('datepicker',
// 					array(
// 						"name"=>"expired_do",
// 						"validate"=>"required"
// 					)
// 				);
// 			// }
// 		$exp = db::nr($rs['exp_date']);
// 		if($exp > 0){
?>
<!-- 			<div class="form-group">
				<label class="control-label col-md-3 col-sm-12 col-xs-12" for="tgl_countainer">&nbsp;</label>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class="table-responsive"> 
						<table class="table table-bordered"> 
							<thead> 
								<tr> 
									<th>#</th> 
									<th>Expired DO</th> 
								</tr> 
							</thead> 
							<tbody> 
								<?php 
									$no = 0;
									while($row = db::fetch($rs['exp_date'])){ $no++;
								?>
									<tr> 
										<th width="5%" scope="row"><?php echo $no;?></th> 
										<td><?php echo date('d-m-Y',strtotime($row['expired_do']));?></td> 
									</tr> 
								<?php 
										}
								?>
							</tbody> 
						</table> 
					</div>
				</div>
			</div> -->
<?php
		// }
		// admlib::get_component('submit',
		// 	array(
		// 		"id"=>isset($id)?$id:null,
		// 		"no_hidden" => "yes",
		// 		"act"=>"del_or"
		// 	)
		// );
?>
		<!-- </form>
	</div>
</div> -->
<!-- END SHIPPING! -->
		<!-- EDIT TRUCKING! -->
<?php 
if ($record_beacukai['id'] !="") {
 ?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		
		<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="Beacukai">
			<h2 style="float:none;text-align:center;color:#666">Beacukai</h2>
		</div>
		<div class="clearfix"></div>
		
		<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="crmForm" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
	
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Download Beacukai Bundle : </label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?php 
	#		echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$app['doc_www'] .'/sppb/'.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 
			#echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 
			$sql321="SELECT * FROM ".$app['table']['file_bea']." WHERE id_import ='".$form['id']."' ORDER BY created_at DESC ";
			db::query($sql321, $rs['row'], $nr['row']);
			while ($row = db::fetch($rs['row'])) {
				echo '<a href="'.$app['doc_http'].'/sppb/'.$row['name'].'" download>'.$row['name'].'</a><br>'; 
			}
			?>
		</div>
	</div>
<?php

		// admlib::get_component('view',
		// 	array(
		// 		"name"=>"expired_do",
		// 		"value"=>app::format_datetime($exp_date['expired_do'],"ina","MM")
		// 	)
		// );
/*		admlib::get_component('inputtext',
			array(
				"name"=>"pib",
				"value"=>app::ov($form['id']),
				"type"=>"hidden"
			)
		);*/
?>
		</form>
	</div>
</div>
<?php 
}
		if ($record_pelayaran['id'] !="") {
 ?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		
		<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="Pelayaran">
			<h2 style="float:none;text-align:center;color:#666">Pelayaran</h2>
		</div>
		<div class="clearfix"></div>
		
		<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="crmForm" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
	
<?php			
		
		// print_r($record_pelayaran);
		$exp_date = db::get_record("expired_do" , "id_delivery_order = '".$record_pelayaran['id']."' AND status = 'active' ");
		admlib::get_component('view',
			array(
				"name"=>"date_receipt",
				"value"=>app::format_datetime($record_pelayaran['date_receipt_do'],"ina","MM")
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"expired_do",
				"value"=>app::format_datetime($exp_date['expired_do'],"ina","MM")
			)
		);

/*		admlib::get_component('inputtext',
			array(
				"name"=>"pib",
				"value"=>app::ov($form['id']),
				"type"=>"hidden"
			)
		);*/
?>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Download Delivery Order : </label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<?php 
			#		echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$app['doc_www'] .'/sppb/'.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 
					#echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 

					$sql321="SELECT * FROM ".$app['table']['file_do']." WHERE id_import ='".$form['id']."' ORDER BY created_at DESC ";
					db::query($sql321, $rs['row'], $nr['row']);
					while ($row = db::fetch($rs['row'])) { 
						echo '<a href="'.$app['doc_http'].'/do/'.$row['name'].'" download>'.$row['name'].'</a><br>'; 
					}
					?>
				</div>
			</div>

		</form>
	</div>
</div>
<?php 
}
if ($record_pelabuhan['id'] !="") { ?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		
		<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="Pelabuhan">
			<h2 style="float:none;text-align:center;color:#666">Pelabuhan</h2>
		</div>
		<div class="clearfix"></div>
		
		<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="crmForm" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
	
<?php			
if ($form['status_beacukai'] =="SPJM") {
		// print_r($record_pelayaran);
	if ($record_pelabuhan['status_eir'] =="ya") {
		$status_eir = "Sudah";
	}else{
		$status_eir = "Belum";
	}
		admlib::get_component('view',
			array(
				"name"=>"status_eir",
				"value"=>app::ov($status_eir)
			)
		);
?>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Download EIR Bundle : </label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?php 
	#		echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$app['doc_www'] .'/sppb/'.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 
			#echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 
			
			$sql321="SELECT * FROM ".$app['table']['file_eir']." WHERE id_import ='".$form['id']."' ORDER BY created_at DESC";
			db::query($sql321, $rs['row'], $nr['row']);
			while ($row = db::fetch($rs['row'])) { 
				echo '<a href="'.$app['doc_http'].'/sppb/'.$row['name'].'" download>'.$row['name'].'</a><br>'; 
			}
			?>
		</div>
	</div>
	<?php 
}
	 ?>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Download Kelengkapan Dokumen: </label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?php 
	#		echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$app['doc_www'] .'/sppb/'.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 
			#echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 
			$sql321 = "SELECT * FROM ". $app['table']['file_kel'] ." WHERE id_import ='".$form['id']."'  ORDER BY created_at DESC";
			db::query($sql321, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				echo '<a href="'.$app['doc_http'].'/out_cont/'.$row['name'].'" download>'.$row['name'].'</a><br>'; 
			}
			?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Download Surat Jalan: </label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?php 
			$sql321 = "SELECT surat_jalan,num_container FROM ". $app['table']['container'] ." WHERE id_import ='".$form['id']."'  ORDER BY created_at DESC";
			db::query($sql321, $rs['row'], $nr['row']);
			while($row = db::fetch($rs['row']))
			{
				if ($row['surat_jalan'] !="") {
					echo '<a href="'.$app['doc_http'].'/do_pdf/'.$row['surat_jalan'].'" download="'.$row['num_container'].'">'.$row['num_container'].'</a><br>'; 				
				}
			}
			?>
		</div>
	</div>
<?php

		// admlib::get_component('view',
		// 	array(
		// 		"name"=>"expired_do",
		// 		"value"=>app::format_datetime($exp_date['expired_do'],"ina","MM")
		// 	)
		// );
/*		admlib::get_component('inputtext',
			array(
				"name"=>"pib",
				"value"=>app::ov($form['id']),
				"type"=>"hidden"
			)
		);*/
?>
		</form>
	</div>
</div>


		<?php 
}
// if(admlib::acc('ATV')){
if(admlib::acc('APPR') &&  $dok_kel !=""){
if (!admlib::acc('APPR')){
$abc="def";
include "dsp_form.php";
}

		 ?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		
		<div class="x_title alert alert-warning" style="background-color:#ffdf00;border-color:#ffdf00" id="APR">
			<h2 style="float:none;text-align:center;color:#666">Trucking</h2>
		</div>
		<div class="clearfix"></div>
		
		<form method="post" action="<?php echo (isset(admlib::$page_active['system'])?admlib::$page_active['system'] .'.do':admlib::$page_active['module'] .'.mod'); ?>" enctype="multipart/form-data" id="crmForm" data-parsley-validate class="form-horizontal form-label-left" name="samForm" >
	
<?php			
		
		admlib::get_component('view',
			array(
				"name"=>"num_pib",
				"value"=>app::ov($form['num_pib'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"pib",
				"value"=>app::ov($form['id']),
				"type"=>"hidden"
			)
		);
?>		
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-12 col-xs-12" for="cont_trucking">&nbsp;</label>
			<div class="col-md-5 col-sm-12 col-xs-12">
				<label class="control-label col-md-4" for="cont_trucking"><?php echo app::getliblang('num_container'); ?><span class="required"></span>		</label>
				<select name="cont_trucking" id="cont_trucking" class="form-control">
					<option value="">Select</option>
					<?php while($row = db::fetch($rs['containers'])){ ?>
					<!-- <option value="<?php echo $row['id'] ?>"><?php echo $row['num_container'] ?></option> -->
					<option value="<?php echo $row['id'] ?>"><?php echo $row['num_container'] ?></option>
					<?php } ?>
				</select>
			</div>
<!-- 			<div class="col-md-4 col-sm-12 col-xs-12">
				<label class="control-label col-md-2" for="arm_trucking"><?php echo app::getliblang('armada'); ?><span class="required"></span>		</label>
				<select name="arm_trucking" id="arm_trucking" class="form-control">
					<option value="">Select</option>
					<?php while($rows = db::fetch($rs['armada'])){ ?>
					<option value="<?php echo $rows['id'] ?>"><?php echo $rows['armada'] ?></option>
					<?php } ?>
				</select>
			</div> -->
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12" style="position: relative;left: 280px">
				<label class="control-label col-md-5 col-sm-5 col-xs-12" for="date_delivery"><?php echo app::getliblang('date_delivery'); ?><span class="required"></span>		</label>
				<input type="text" id="p_date_delivery" name="p_date_delivery"  class="form-control has-feedback-left datepickers" aria-describedby="date_delivery" >
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12" style="position: relative;left: 280px">
				<label class="control-label col-md-5 col-sm-5 col-xs-12" for="ket_doc"><?php echo app::getliblang('ket_doc'); ?><span class="required"></span>		</label>
				<textarea id="p_ket_doc" name="ket_doc"  class="form-control" aria-describedby="ket_doc" ></textarea>
			</div>
		</div>
			<div class="form-group">
			<label class="control-label col-md-3 col-sm-12 col-xs-12" for="add-trucking">&nbsp;</label>
			<br>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<a href="#" class="btn btn-default add" val="trucking"><span class="fa fa-plus" aria-hidden="true"></span> Add</a>
				<br/>
				<div id="trucking"></div>
			</div>
		</div>
<?php
		// admlib::get_component('submit',
		// 	array(
		// 		"id"=>isset($id)?$id:null,
		// 		"act"=>"trucking"
		// 	)
		// );
?>
		</form>
	</div>
</div>
<?php 

///////setelah kerjakan hapus komentar di bawah
}	 



?>
<!-- END TRUCKING! -->
	</div><!--right col-->

	</div>
</div>
<script>
<?php if($form['code_cat'] == "TB"){ ?>
document.getElementById('qc_process').nextSibling.nodeValue = 'Logistic Check';
<?php } ?>
$(document).ready(function(){
	$(function() { $('a[href*="#"]:not([href="#"])').click(function() { if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) { var target = $(this.hash); target = target.length ? target : $('[name=' + this.hash.slice(1) +']'); if (target.length) { $('html, body').animate({ scrollTop: target.offset().top-70}, 1000); return false; } } });});
	
	
	$("#comdet").click(function(){
        $(this).css('background-color', '#ffdf00');
        $('#crmmax').css('background-color', '#fff');
        $('#apr').css('background-color', '#fff');
    });
	$("#crmmax").click(function(){
		$(this).css('background-color', '#ffdf00');
        $('#comdet').css('background-color', '#fff');
        $('#apr').css('background-color', '#fff');
    });
	$("#apr").click(function(){
		 $(this).css('background-color', '#ffdf00');
        $('#comdet').css('background-color', '#fff');
        $('#crmmax').css('background-color', '#fff');
    });
	$('form').submit('click', function(e){
		var r = confirm("Apakah data yang anda masukan sudah benar");
		if (r === false) {
			e.preventDefault();
		}
    });
	
// var label = document.getElementById("qc_process");
// alert(label.innerHTML);
    // Select and loop the container element of the elements you want to equalise
    // $('.equalsheight').each(function(){  
      
      // Cache the highest
      var highestBox = 280;
      
      // Select and loop the elements you want to equalise
      $('.equalsheight', this).each(function(){
        
        // If this box is higher than the cached highest then store it
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }
      
      });  
            
      // Set the height of all those children to whichever was highest 
      $('.equalsheight',this).height(highestBox);
                    
      // Cache the highest
      var sheight = 100;
      
      // Select and loop the elements you want to equalise
      $('.sheight', this).each(function(){
        
        // If this box is higher than the cached highest then store it
        if($(this).height() > sheight) {
          sheight = $(this).height(); 
        }
      
      });  
            
      // Set the height of all those children to whichever was highest 
      $('.sheight',this).height(sheight);
                    
    // }); 

});
function OnlyNumbers(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;

    if (
        //0~9
        charCode >= 48 && charCode <= 57 ||
       //number pad 0~9
       // charCode >= 96 && charCode <= 105 ||
        //backspace
       charCode == 8 ||
        //tab
        charCode == 9 ||
        //enter
        charCode == 13 || 
        //left, right, delete..
        charCode >= 35 && charCode <= 46
    )
    {
        //make sure the new value below 20
        // if(parseInt(this.value+String.fromCharCode(charCode), 10) <= 20) 
            return true;
    }
    
    evt.preventDefault();
    evt.stopPropagation();
    
    return false;
}
$(document).ready(function(){
<?php if($form['approval'] == "sbo"){ ?>
$("*", "#qcForm").prop('disabled',true);
$("*", "#resultForm").prop('disabled',true);
<?php }elseif($form['approval'] == "qc"){ ?>
// $("*", "#sboForm").prop('disabled',true);
$("*", "#resultForm").prop('disabled',true);
<?php }elseif($form['approval'] == "result"){ ?>
$("*", "#sboForm").prop('disabled',true);
$("*", "#qcForm").prop('disabled',true);
<?php }else{ ?>
$("*", "#sboForm").prop('disabled',true);
$("*", "#qcForm").prop('disabled',true);
$("*", "#resultForm").prop('disabled',true);
<?php } ?>
});
$("input[name=p_accepted]").bind('keyup', function(e) {
	var max = <?php echo $form['qty'] ?>;
	var nilai = $(this).val();
	if(nilai > max){
		$(this).val(max);
		alert("Maximum Qty : "+max);
		var nilai = max;
	}
	var rejected = max - nilai;
	$("#qcForm input[name=p_rejected]").val(rejected);
	$("#qcForm input[name=p_rejected_val]").val(rejected);
});

$("#crmForm input[name=p_no_crmmax]").attr('maxlength', '10');
$("#crmForm input[name=p_no_crmmax]").bind('keypress', function(e) {OnlyNumbers(e)});
$("#qcForm input[name=p_accepted]").bind('keypress', function(e) {OnlyNumbers(e)});
$("#qcForm input[name=p_rejected]").prop('disabled',true);
$("#qcForm input[name=p_status]").click(function(){
	var status = $(this).val();
	if(status == "closed"){
		$("#g_accepted").show();
		$("#g_rejected").show();
	}else{
		$("#g_accepted").hide();		
		$("#g_rejected").hide();		
	}
});	
$("#resultForm input[name=p_status]").click(function(){
	var status = $(this).val();
	if(status == "closed"){
<?php if($qc['accepted'] > 0){ ?>
		$("#g_cn_number").show();
<?php }else{ ?>
		$("#g_cn_number").hide();		
<?php } ?>
	}else{
		$("#g_cn_number").hide();		
	}
});
$("#sboForm input[name=p_status]").click(function(){
	var status = $(this).val();
	if(status == "revised"){
		$("#g_revision_to").show();
	}else{
		$("#g_revision_to").hide();		
	}
});
<?php if($qc['status'] == "closed"){ ?>
		$("#g_accepted").show();
		$("#g_rejected").show();
<?php }else{ ?>
		$("#g_accepted").hide();
		$("#g_rejected").hide();
<?php } ?>
<?php if($result['status'] == "closed"){ ?>
<?php if($qc['accepted'] > 0){ ?>
		$("#g_cn_number").show();
<?php }else{ ?>
		$("#g_cn_number").hide();		
<?php } ?>
<?php }else{ ?>
		$("#g_cn_number").hide();
<?php } ?>

<?php if($sbo['status'] == "revised"){ ?>
		$("#g_revision_to").show();
<?php }else{ ?>
		$("#g_revision_to").hide();
<?php } ?>

		$(".slider").slick({
			adaptiveHeight: true
		});

$(document).ready(function(){
	
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
    $(".fancyboxdn").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
	
	
});
   
</script>
<?php
admlib::display_block_footer();
?>