<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'import','caption'=>'Import');
function cek_container_id($id){
    $cek_id = db::lookup('id', 'container', 'id',$id);
    if(isset($cek_id) && $cek_id !=""){
	    $ids = rand(1, 100).date("dmYHis").rand(1, 100).rand(1, 100).$i;
        cek_container_id($ids);
    }else{
        return $id;
    }
}
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$_SESSION['container'] 	= null;
	$option = ['sortable'=>true];
	// admlib::display_block_grid();
	admlib::display_block_grid("","","import");
    exit;
endif;
///////////////////////////////////////////////////////////////////////////////
include($app['lib_path'] .'/MPDF/mpdf.php');

function format_tgl_bahasa($date,$bahasa){
$date =explode(" ", $date);
$date_tgl =explode("-", $date[0]);

if ($bahasa == "id") {
$bulan = array (
		'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
}elseif ($bahasa == "en") {
$bulan = array (
		'January',
		'February',
		'March',
		'April',
		'May',
		'Juni',
		'June',
		'August',
		'September',
		'October',
		'November',
		'December'
	);
}elseif($bahasa == "chn"){
$bulan = array (
		'一月',
		'二月',
		'三月',
		'四月',
		'五月',
		'六月',
		'七月',
		'八月',
		'九月',
		'十月',
		'十一月',
		'十二月'
	);
}
/*$bulan = array (
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'Mei',
		'Jun',
		'Jul',
		'Agu',
		'Sep',
		'Okt',
		'Nov',
		'Des'
	);*/
// $total = $date_tgl[2]."-".$bulan[$date_tgl[1]-1]."-".$date_tgl[0];
	return $date_tgl[2]." / ".$bulan[$date_tgl[1]-1]." / ".$date_tgl[0]; 
	// return $date_tgl[2]." - ".$date_tgl[1]." - ".$date_tgl[0]; 
}


/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	// admlib::validate('DSPL');
	// $total = db::lookup('COUNT(id)', 'import', '1=1');
	// app::set_default($page_size, (isset($all)?$total:10));
	// $q = null;
	// if($ref)
	// {
	// 	$q = "WHERE a.name LIKE '%". $ref ."%'";
	// }
	// $sql = "SELECT a.id, a.status, a.num_pib AS num_pib, a.num_bl as num_bl, a.name_ship as name_ship,  b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
	// FROM ". $app['table']['import'] ." a 	
	// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
	// 		LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id) $q ORDER BY a.reorder ASC";
	// app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	// db::query($sql, $rs['row'], $nr['row']);
	// $results = [];
	// $columns = ['num_pib','num_bl','name_ship'];
	// while($row = db::fetch($rs['row']))
	// {
	// 	$results[] 		= $row;
	// }
	// $option = ['sortable'=>false];
	// include $app['pwebmin'] ."/include/blk_list.php";

// index baru orig
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'import', '1=1');
	// app::set_default($page_size, (isset($all)?$total:10));
	if (($act_search !="") && ($p_search !="" || $p_customer_s !="all" || $p_tgl_nopen_s !="" || $p_stat_kir !="all" || $p_stat_bea !="all")) {
		app::set_default($page_size, $total);
	}else{
		app::set_default($page_size, (isset($all)?$total:10));		
	}
	$unpost = 1;
	$unmodify = 1;
	
/*	if($ref)
	{
		// $q = "WHERE a.name LIKE '%". $ref ."%'";
		$q = "WHERE a.pfpd LIKE '%". $ref ."%' OR a.num_pib LIKE '%". $ref ."%' OR b.name LIKE '%". $ref ."%'";
	}*/
	if ($act_search == "import") {
		$q = "WHERE 1=1";
		if ($p_search !="") {
			$q .=" AND (a.num_pib LIKE '%". $p_search ."%' or a.nopen LIKE '%". $p_search ."%')";
		}
		if ($p_stat_bea !="" && $p_stat_bea !="all" ) {
			$q .=" AND (a.status_beacukai LIKE '%".strtoupper($p_stat_bea)."%')";
		}
		if ($p_customer_s !="" && $p_customer_s !="all") {
			$q .=" AND (a.id_customer LIKE '%".$p_customer_s."%')";
		}
		if ($p_tgl_nopen_s !="" && $p_tgl_nopen_s !="all") {
			$q .=" AND (a.tgl_nopen LIKE '%".date("Y-m-d", strtotime($p_tgl_nopen_s))."%')";
		}
		if ($p_stat_kir !="" && $p_stat_kir !="all") {
			$q .=" AND (a.status_kiriman LIKE '%".$p_stat_kir."%')";
		}
	}
	$sql = "SELECT DISTINCT a.id, a.status, a.num_pib AS num_pib, a.num_bl as num_bl, a.name_ship as name_ship,  b.name as created_by,a.created_at,c.name as updated_by,a.updated_at, d.id id_idv,d.status status_deliv,d.do do,a.status_beacukai sb,a.status_sppb status_sppb,a.status_kiriman status_kiriman,e.sppb sppbnya
			FROM ". $app['table']['import'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
			LEFT JOIN ". $app['table']['delivery_order'] ." d ON (a.id=d.id_import)  
			LEFT JOIN ". $app['table']['beacukai'] ." e ON (a.id=e.id_import)  
			$q ORDER BY a.created_at DESC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	// $columns = ['num_pib','num_bl','name_ship','created_at','status','created_by','step','note'];
	// $columns = ['num_pib','num_bl','name_ship','status_do','created_by','step','view'];
	// $columns = ['view','send_email','status_beacukai', 'status_sppb','num_pib','num_bl','name_ship','created_by'];
	#$columns = ['view','send_email','status_beacukai', 'status_sppb','num_pib','num_bl','name_ship','status_kiriman','created_by'];
	$columns = ['view','status_beacukai', 'status_sppb','num_pib','num_bl','name_ship','status_kiriman','created_by'];
	// $columns = array_merge($columns,['view']);
	// $thstyle = ["date_do"=>"background-color:#ffdf00;color:#666;","created_by"=>"background-color:#ffdf00;color:#666;","step"=>"background-color:#ffdf00;color:#666;","status_do"=>"background-color:#ffdf00;color:#666;","note"=>"background-color:#ffdf00;width:150px;color:#666;"];
	while($row = db::fetch($rs['row']))
	{
		$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id_idv']."' and status = 'active'");
		if ($row['status_kiriman'] == "0") {
			if ($datas['do'] < date("Y-m-d")) {
				isset($row['id_idv'])?$id_exp [] = $row['id_idv']:null;
			}else{
				isset($row['id_idv'])?$id_exp_act [] = $row['id_idv']:null;
			}
		}
		$datas321 = db::get_record("pelabuhan","id_import='".$row['id']."' ");
		$datas_eir = db::lookup("id","file_eir","id_import='".$row['id']."' ");
	#	if ($datas321['eir'] !="" || $datas321['status_eir'] == "ya") {
	#	if (($row['sb']=="SPJM" || $row['sb']=="SPJK")  && $datas321['eir'] !="" || $datas321['status_eir'] == "ya") {
		if (($row['sb']=="SPJM")  && ($datas321['status_eir'] == "ya") && $row['status_deliv'] =="active") {
			// $row['ubah_status']="<center><a href='".admlib::getext()."&act=ubah_status&id=".$row['id']."&num_pib=".$row['num_pib']."&status=".$row['sb']."'><i class='fa fa-paper-plane'></i></a></center>";
			if ($row['status_sppb'] == "0") {
				$abcdef="<center><a class='btn btn-success' href='".admlib::getext()."&act=ubah_status&id=".$row['id']."&num_pib=".$row['num_pib']."&status=".$row['sb']."'>Ubah ke SPPB</a></center>";
			// $row['status_sppb']="<center><a class='btn btn-success' href='".admlib::getext()."&act=ubah_status&id=".$row['id']."&num_pib=".$row['num_pib']."&status=".$row['sb']."'>Ubah ke SPPB</a></center>";
			}else{
				$abcdef ="<center><font color='green'><b>Sudah</b></font></center>";
				// $row['status_sppb'] = "<center><font color='green'><b>Sudah</b></font></center>";
			}
		}elseif ( ($row['sb']!="SPJM" AND !empty($row['sb']) AND $row['sb'] !="" ) AND $row['status_deliv'] =="active") {
			if ($row['status_sppb'] == "0") {
				$abcdef="<center><a class='btn btn-success' href='".admlib::getext()."&act=ubah_status&id=".$row['id']."&num_pib=".$row['num_pib']."&status=".$row['sb']."'>Ubah ke SPPB</a></center>";
			}else{
				$abcdef ="<center><font color='green'><b>Sudah</b></font></center>";
			}
		}else if ($row['status_sppb'] == "0") {
				$abcdef ="<center><font color='red'><b>Proses</b></font></center>";
		}else if($row['status_sppb'] == "1"){
				$abcdef ="<center><font color='green'><b>Sudah</b></font></center>";
		}$row['status_sppb'] =$abcdef;
		$datas = db::get_record("complaint_status","id_complaint='".$row['ncr_id']."' AND type='".$row['approve_by']."' ORDER BY created_at DESC");
		$row['step']			= app::getliblang($row['approval']);
		//$row['created_by']			= app::getliblang($row['approve_by']);
		if($row['code_cat'] == "TB" && $row['approval'] == "qc"){
			$row['step']		= app::getliblang('logistic');
		}
		if($row['code_cat'] == "TB" && $row['approve_by'] == "qc"){
			$row['created_by']	= app::getliblang('logistic');
		}
		$row['note']			= $datas['note'];
		$row['send_email'] = "<center><a href='".admlib::getext()."&act=send_email&id=".$row['id']."&num_pib=".$row['num_pib']."&status=".$row['sb']."'><i class='fa fa-paper-plane'></i></a></center>";

		$row['created_at']	= app::format_datetime($row['created_at'],"ina","MMM");
		$row['status_do'] 	= db::lookup("status","delivery_order","id_import='".$row['id']."' ");

		if ($row['status_kiriman'] == "1") {
			$row['status_kiriman'] = "<font color='green'>Selesai</font>";
		}elseif($row['status_kiriman'] == "0"){
			$row['status_kiriman'] = "<font color='red'>Belum Selesai</font>";
		}

		if ($row['sb'] == "SPJH") {
			$row['sb'] = "<font color='green'>".$row['sb']."</font>";
		}elseif($row['sb'] == "SPJK"){
			$row['sb'] = "<font color='orange'>".$row['sb']."</font>";
		}elseif($row['sb'] == "SELESAI"){
			$row['sb'] = "<font color='blue'>".$row['sb']."</font>";
		}else{
			$row['sb'] = "<font color='red'>".$row['sb']."</font>";
		}
		$row['status_beacukai'] = $row['sb'];

		// app::getliblang($row['status_do']);
		
		// if(admlib::acc('APPR')){
		// 	if($row['status_crm'] == "yes"){
		// 		$row['status_crm']	= $row['no_crmmax'];
		// 	}else{				
		// 		$row['status_crm']	= "-";
		// 	}
			
		// }
		// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id_idv']."' and status = 'active'");
		$datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id_idv']."' ");

		$row['date_do']=$datas['do'];
		if ($row['date_do']!=null&&$row['date_do']!="0000-00-00") {
			$row['date_do']= date("d-m-Y",strtotime($row['date_do']));
		}else{
			$row['date_do']="";
		}

		// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
		// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
		// $row['expired_do']= $datas['do'];

		// if (strtotime($datas['do']) < time()) {
/*		if ($row['sb'] != "SELESAI") {
			if ($datas['do'] < date("Y-m-d")) {
				isset($row['id_idv'])?$id_exp [] = $row['id_idv']:null;
			}else{
				isset($row['id_idv'])?$id_exp_act [] = $row['id_idv']:null;
			}
		}*/
		$row['view']	= '<center><a href="'.admlib::getext().'&act=view&id='.$row['id'].'"><i class="fa fa-eye" aria-hidden="true"></i></a></center>';
		$results[] 	= $row;
	}
	$id_exp 	= implode("','", $id_exp);
	$id_exp_act = implode("','", $id_exp_act);
	$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
	$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
	db::qry($sql321);
	db::qry($sql1231);

######################   ubah status kirim   ################################
	$asdqwezxcasd= "SELECT * FROM ".$app['table']['import']." ORDER BY a.id_import DESC";
	db::query($asdqwezxcasd, $rs11['row'], $nr11['row']);
	while($row11 = db::fetch($rs11['row']))
	{
		$cek_selesai = db::lookup("GROUP_CONCAT(stat_kir)","container", "id_import", $row11['id']);
		if (!preg_match("/0/i", $cek_selesai)) {
			db::qry("update ".$app['table']['import']." set	send_status = 'selesai' where id='".$row11['id']."' ");
		}elseif (preg_match("/1/i", $cek_selesai)) {
			db::qry("update ".$app['table']['import']." set	send_status = 'sebagian' where id='".$row11['id']."' ");
		}else{
			db::qry("update ".$app['table']['import']." set	send_status = 'belum' where id='".$row11['id']."' ");
		}
	}
######################   /ubah status kirim   #################################

	// $sortable = true;
	$statusx = true;
	include $app['pwebmin'] ."/include/blk_list_complaint.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
//		$validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen';
		// $validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen,p_pfpd';
		$validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment';
		form::serialize_form();
		// form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		$id_c = rand(1, 100).date("dmYHis");
		$id_notif = rand(1, 100).date("dmYHis");
		$id_notif2 = rand(1, 100).date("dmYHis");
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		// echo count($container);
		// echo "<br>";
		// print_r($container);
		// exit;
		$i=0;
		if(count($container)>0)
		{
			foreach($container as $row)
			{
				if ($row['idm']=="") 
				{
					isset($row['dd'])?$row['dd'] = date("Y-m-d",strtotime($row['dd'])):null;
					$ids = rand(1, 100).date("dmYHis").$i;
					// echo $ids;
					// echo "<br><br>";
					$ids = cek_container_id($ids);
					// echo $ids;
					// exit;
					$t_container 	= $t_container+$row['n'];
					$reorders = db::lookup("max(reorder)","container","1=1");
					if ($row['dd'] > 1) {
						$da_lev=",delivery_date";
						$deliverynya = ",'". $row['dd'] ."'";
					}
					if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
					// $sqlx = "insert into ". $app['table']['container'] ."
					// (id, id_import, num_container, qty, note, value, reorder, created_by, created_at,address,delivery_date) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now(),'". $row['adr'] ."','". $row['dd'] ."'")";
					// db::qry($sqlx);
					
					/*$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
					db::query($sql_lang, $rs['lang'], $nr['lang']);
					while ($row	= db::fetch($rs['lang'])) {
						$sqlss ="insert into ".$app['table']['status']." 
								(id,id_import,positions,created_by,created_at,lang) values
								('$id_c','$id','".app::getliblang('proses_import',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
						db::qry($sqlss);
					}*/
					$num_container = $row['c'];
					$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at,address$da_lev) values ('$ids', '$id', '". $num_container ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now(),'". $row['adr'] ."'$deliverynya)";
					db::qry($sqlx);
					// $sqlnya .= $sqlx."<br>";

/*
					$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'];
					db::query($sql_lang, $rs['lang'], $nr['lang']);
					while ($row	= db::fetch($rs['lang'])) {
						$id_c = rand(1, 100).date("dmYHis");
				 		$sqlss321 ="insert into ".$app['table']['status']." 
									(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values
									('$id_c','$id','$ids','".app::getliblang('proses_import',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."','".$num_container."')";
						db::qry($sqlss321);
					}*/
					$i++;

				}
			}
		}else{
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=add&error=55");
			exit;

		}		

		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'];
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while ($row	= db::fetch($rs['lang'])) {
			$id_c = rand(1, 100).date("dmYHis");
	 		$sqlss321 ="insert into ".$app['table']['status']." 
						(id,id_import,positions,created_by,created_at,lang,num_container) values
						('$id_c','$id','".app::getliblang('proses_import',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."','".$num_container."')";
			db::qry($sqlss321);
		}
		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_date_receipt,p_amount_payment,p_nopen');
		$reorder = db::lookup("max(reorder)","import","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$p_date_receipt = date("Y-m-d",strtotime($p_date_receipt));
		
		$p_tanggal_sptnp = date("Y-m-d",strtotime($p_tanggal_sptnp));

		$p_tgl_nopen = date("Y-m-d",strtotime($p_tgl_nopen));
		
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);

		$p_nilai_sptnp = str_replace('Rp ', '', $p_nilai_sptnp);
		$p_nilai_sptnp = str_replace(',', '', $p_nilai_sptnp);
		
		// $nama_gambar= $_FILES["p_pfpd"]["name"];
	 //    move_uploaded_file($_FILES["p_pfpd"]["tmp_name"], $app['doc_path']."/pfpd/".$nama_gambar);
		$kode_company = db::lookup("code","company","id='$company'");
		$p_num_pib_change = $p_num_pib.' '.$kode_company ;
		$p_eta = date("Y-m-d",strtotime($p_eta));

		if ($status_beacukai == "spjh") {
			$column_status_sppb =",status_sppb";
			$value_status_sppb =",'1'";
		}
/*		$sql = "insert into ".$app['table']['import']."
				(id, num_pib, num_bl, name_ship, eta, id_customer, id_company, date_receipt, amount_payment, nopen, reorder, created_by, created_at,status_beacukai,tanggal_sptnp,nomor_sptnp,nilai_sptnp,pfpd) values
				('$id', '$p_num_pib', '$p_num_bl', '$p_name_ship', '$p_eta', '$customer_name', '$company', '$p_date_receipt', '$p_amount_payment', '$p_nopen', '$reorder', '". $app['me']['id'] ."', now(),'$status_beacukai','$p_tanggal_sptnp','$p_nomor_sptnp','$p_nilai_sptnp','$p_pfpd')";*/
	 	$sql = "insert into ".$app['table']['import']."
				(id, num_pib, num_bl, name_ship, eta, id_customer, id_company, date_receipt, amount_payment, nopen, reorder, created_by, created_at,status_beacukai,tanggal_sptnp,nomor_sptnp,nilai_sptnp,tgl_nopen,ket_cust,num_pib_asli,pfpd$column_status_sppb) values
				('$id', '$p_num_pib_change', '$p_num_bl', '$p_name_ship', '$p_eta', '$customer_name', '$company', '$p_date_receipt', '$p_amount_payment', '$p_nopen', '$reorder', '". $app['me']['id'] ."', now(),'$status_beacukai','$p_tanggal_sptnp','$p_nomor_sptnp','$p_nilai_sptnp','$p_tgl_nopen','$p_ket_cust','$p_num_pib','$p_pfpd'$value_status_sppb)";
		db::qry($sql);
// echo $sqlnya;
// exit;		
		$num_bil = db::lookup("num_bl","import","id",$id);
		$cek_customer = db::lookup("id_customer","import","id",$id);
		$rs['devices'] = db::get_recordset("user_device","id_user='".$cek_customer."' AND status='active'");
		while($user_devices = db::fetch($rs['devices'])){
			$datapush = array(
				"to" => db::lookup("token","device","id",$user_devices['id_device']),
				"notification" => array(
					"title" => "Status Updated",
					"text" => "$num_bil : ".app::getliblang('proses_import',"id")."
							   ",
					"sound" => "default"
				),
				'data' => array(
					'method' => 'change_status',
					'id' => $id,
					"title" => "Status Updated",
					"body" => "$num_bil : ".app::getliblang('proses_import',"id")
				)
			);
			app::pushNotifClientSend($datapush);
		}
		if ($status_beacukai != "spjm") 
		{

			$id_notif = rand(1, 100).date("dmYHis");
			$sqls = "insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_notif','2120082018161449','$id',1,'Dokumen No. PIB $p_num_pib_change perlu diproses DO','".$app['me']['id']."',now())";

			$id_notif2 = rand(1, 100).date("dmYHis");
		 	$sqls321123 = "insert into ".$app['table']['notif']." 
						(id,id_rule,id_import,status,pesan,created_by,created_at) values
						('$id_notif2','7903092018114332','$id',1,'Dokumen NO. PIB $p_num_pib_change perlu diproses Beacukai','".$app['me']['id']."',now())";

			$rs['devices'] = db::get_recordset("user_device","category='pelayaran' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					// "to" => db::lookup("token","device","category","pelayaran"),
					"notification" => array(
						"title" => "Admin Pelayaran",
						"text" => "$p_num_pib_change : Buat DO",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelayaran',
						'ncr_id' => $id,
						"title" => "Admin Pelayaran",
						"body" => "$p_num_pib_change : Buat DO"
					)
				);
				app::pushNotifSend($datapush);
			}

			$rs['devices'] = db::get_recordset("user_device","category='beacukai' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					// "to" => db::lookup("token","device","category","pelayaran"),
					"notification" => array(
						"title" => "Admin Beacukai",
						"text" => "$p_num_pib_change : Buat Dokumen Beacukai",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'beacukai',
						'ncr_id' => $id,
						"title" => "Admin Beacukai",
						"body" => "$p_num_pib_change : Buat Dokumen Beacukai"
					)
				);
				app::pushNotifSend($datapush);
			}
		
			$message  	   = "<p>Ada kiriman baru dengan PIB <b>". $p_num_pib_change ."</b></p>";
			// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Setujui Pelatihan</a></p>";
			// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Cek Data</a></p>";
			$message 	  .= "<p>Terima kasih</p>";
			$sql_pelayaran = "select c.email from ". $app['table']['user_det'] ." a 
						   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
						   where a.id_rule ='2120082018161449' ";
			db::query($sql_pelayaran, $rs['sql_pelayaran'], $nr['sql_pelayaran']);
			while($row = db::fetch($rs['sql_pelayaran'])){
				app::sendmail($row['email'], "Ada dokumen import masuk", $message);
			}
			$message  = "<p>Ada kiriman baru dengan PIB <b>". $p_num_pib_change ."</b></p>";
			// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
			$message .= "<p>Terima kasih</p>";
			$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
						   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
						   where a.id_rule ='7903092018114332' ";
			db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
			while($row = db::fetch($rs['sql_bea_cukai'])){
				app::sendmail($row['email'], "Ada dokumen import masuk", $message);
			}
			db::qry($sqls);
			db::qry($sqls321123);
		}else{

			$id_notif = rand(1, 100).date("dmYHis");
			$sqls = "insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_notif','2120082018161449','$id',1,'Dokumen No. PIB $p_num_pib_change perlu diproses DO','".$app['me']['id']."',now())";
			db::qry($sqls);

			$id_notif2 = rand(1, 100).date("dmYHis");
			$sqls321123 = "insert into ".$app['table']['notif']." 
						  (id,id_rule,id_import,status,pesan,created_by,created_at) values
						  ('$id_notif2','5403092018114622','$id',1,'Dokumen No. PIB $p_num_pib_change perlu dibuat EIR Bundle','".$app['me']['id']."',now())";
			db::qry($sqls321123);

/*			$id_notif2 = rand(1, 100).date("dmYHis");
		 	$sqls321123 = "insert into ".$app['table']['notif']." 
						(id,id_rule,id_import,status,pesan,created_by,created_at) values
						('$id_notif2','7903092018114332','$id',1,'Kiriman baru dengan no pib = $p_num_pib_change','".$app['me']['id']."',now())";
			db::qry($sqls321123);*/

			$rs['devices'] = db::get_recordset("user_device","category='pelayaran' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					// "to" => db::lookup("token","device","category","pelayaran"),
					"notification" => array(
						"title" => "Admin Pelayaran",
						"text" => "$p_num_pib_change : Buat DO",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelayaran',
						'ncr_id' => $id,
						"title" => "Admin Pelayaran",
						"body" => "$p_num_pib_change : Buat DO"
					)
				);
				app::pushNotifSend($datapush);

				$message  = "<p>Ada kiriman baru dengan PIB <b>". $p_num_pib_change ."</b></p>";
				// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Setujui Pelatihan</a></p>";
				// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Cek Data</a></p>";
				$message .= "<p>Terima kasih</p>";
				$sql_pelayaran = "select c.email from ". $app['table']['user_det'] ." a 
							   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
							   where a.id_rule ='2120082018161449' ";
				db::query($sql_pelayaran, $rs['sql_pelayaran'], $nr['sql_pelayaran']);
				while($row = db::fetch($rs['sql_pelayaran'])){
					app::sendmail($row['email'], "Ada dokumen import masuk", $message);
				}
				$message  = "<p>Ada kiriman baru dengan PIB <b>". $p_num_pib_change ."</b></p>";
				// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Setujui Pelatihan</a></p>";
				$message .= "<p>Terima kasih</p>";
				$sql_eir  = "select c.email from ". $app['table']['user_det'] ." a 
							 inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
							 where a.id_rule ='5403092018114622' ";
				db::query($sql_eir, $rs['sql_eir'], $nr['sql_eir']);
				while($row = db::fetch($rs['sql_eir'])){
					app::sendmail($row['email'], "Ada dokumen import masuk", $message);
				}
			}
			$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					// "to" => db::lookup("token","device","category","pelayaran"),
					"notification" => array(
						"title" => "Admin Pelabuhan/EIR",
						"text" => "$p_num_pib_change : Buat EIR",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelayaran',
						'ncr_id' => $id,
						"title" => "Admin Pelabuhan/EIR",
						"body" => "$p_num_pib_change : Buat EIR"
					)
				);
				app::pushNotifSend($datapush);
			}
		}
			// db::qry($sqls321123);
		// $sqlss ="insert into ".$app['table']['status']." 
		// 		(id,id_import,positions,created_by,created_at) values
		// 		('$id_c','$id','".app::getliblang('proses_import')."','".$app['me']['id']."',now())";
/*		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while ($row	= db::fetch($rs['lang'])) {
		$id_c = rand(1, 100).date("dmYHis");
 		$sqlss321 ="insert into ".$app['table']['status']." 
					(id,id_import,positions,created_by,created_at,lang) values
					('$id_c','$id','".app::getliblang('proses_import',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
			db::qry($sqlss321);
		}*/	
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
		
		// $rs['container'] = db::get_record_select("num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr", "container", "id_import", $id);
		$rs['container'] = db::get_record_select("num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr", "container", "id_import = '".$id."' order by num_container asc");
		$_SESSION['container'] = [];
		while($row = db::fetch($rs['container'])){
			$_SESSION['container'][] = $row;
		}
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		// print_r($_SESSION['income']);
		// print_r($_REQUEST);exit;
		form::validate('empty','p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_amount_payment');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		
		// if ($_FILES["p_pfpd"]["name"]) {
		// 	$delete_file = db::get_record("import","id",$id);
		// 	@unlink($app['doc_path']."/pfpd/". $delete_file['pfpd']);
		// 	$nama_gambar= $_FILES["p_pfpd"]["name"];
		//     if (move_uploaded_file($_FILES["p_pfpd"]["tmp_name"], $app['doc_path']."/sppb/".$nama_gambar)) {
		// 	    $abc="pfpd 			= '".$nama_gambar."',";
		//     }
		// }
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		$i =0;
		if(count($container)>0)
		{
			db::qry('DELETE FROM '. $app['table']['container'] .' WHERE id_import="'. $id .'"');
			foreach($container as $row){
			$ids = rand(1, 100).date("dmYHis").$i;
				$t_container 	= $t_container+$row['n'];
				$reorders = db::lookup("max(reorder)","container","1=1");
				if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
				$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at,address) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now(), '". $row['adr'] ."')";
				db::qry($sqlx);
				$i++;
			}
		}
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);

		$p_nilai_sptnp = str_replace('Rp ', '', $p_nilai_sptnp);
		$p_nilai_sptnp = str_replace(',', '', $p_nilai_sptnp);
		$p_eta = date("Y-m-d",strtotime($p_eta));
		$p_date_receipt = date("Y-m-d",strtotime($p_date_receipt));
		$p_tanggal_sptnp = date("Y-m-d",strtotime($p_tanggal_sptnp));
		$p_tgl_nopen = date("Y-m-d",strtotime($p_tgl_nopen));
		$num_pib_before = db::lookup("num_pib","import","id",$id);
		$perusahaan_before = db::lookup("id_company","import","id",$id);
		$perusahaan_before_kode = db::lookup("code","company","id",$perusahaan_before);

		$p_num_pib_berubah = explode(" ", $p_num_pib);

		if ($p_company_orig != $p_company) {
			// if ($p_num_pib_orig != $p_num_pib ) {
			// 	$kode_company = db::lookup("code","company","id='$company'");
			// 	$p_num_pib = $p_num_pib.' '.$kode_company ;
			// }
			// if ($num_pib_before != $p_num_pib.' '.$perusahaan_before_kode || $num_pib_before != $p_num_pib) {
				$pib_asli = db::lookup("num_pib_asli","import","id='".$id."' ");
				$kode_company = db::lookup("code","company","id='$company'");
				$p_num_pib = $pib_asli.' '.$kode_company ;
			// }
		}

		if ($num_pib_asli != $p_num_pib_berubah[0]) {
		 	$p_num_pib = $p_num_pib_berubah[0];
			$berubah_pib = "num_pib_asli	= '$p_num_pib',";

			$pib_asli = db::lookup("num_pib_asli","import","id='".$id."' ");
			$kode_company = db::lookup("code","company","id='$company'");
		 	$p_num_pib = $p_num_pib.' '.$kode_company ;
		}
		if ($status_beacukai == "spjh") {
			$column_status_sppb ="status_sppb = '1', ";
		}		
 		$sql = "update ". $app['table']['import'] ."
				set num_pib 		= '$p_num_pib',
					num_bl 			= '$p_num_bl',
					name_ship 		= '$p_name_ship',
					eta		 		= '$p_eta',
					ket_cust 		= '$p_ket_cust',
					id_customer 	= '$customer_name',
					id_company	 	= '$company',
					date_receipt 	= '$p_date_receipt',
					pfpd 			= '$p_pfpd',
					amount_payment 	= '$p_amount_payment',
					status_beacukai	= '$status_beacukai',
					$berubah_pib
					tgl_nopen       = '$p_tgl_nopen',
					$column_status_sppb
					nopen		 	= '$p_nopen',
					tanggal_sptnp	= '$p_tanggal_sptnp',
					nomor_sptnp		= '$p_nomor_sptnp',
					nilai_sptnp		= '$p_nilai_sptnp',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '$id'";
		db::qry($sql);
		if ($status_beacukai != "spjm") 
		{

			$id_notif = rand(1, 100).date("dmYHis");
			$sqls = "insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_notif','2120082018161449','$id',1,'Dokumen No. PIB $p_num_pib perlu diproses DO','".$app['me']['id']."',now())";


			$id_notif = rand(1, 100).date("dmYHis");
			$sqls321123 = "insert into ".$app['table']['notif']." 
						(id,id_rule,id_import,status,pesan,created_by,created_at) values
						('$id_notif','7903092018114332','$id',1,'Kiriman dengan no pib = $p_num_pib, statusnya diubah dengan : $status_beacukai','".$app['me']['id']."',now())";
			db::qry($sqls);
			db::qry($sqls321123);

			$rs['devices'] = db::get_recordset("user_devices","category='pelayaran' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					// "to" => db::lookup("token","device","category","pelayaran"),
					"notification" => array(
						"title" => "Admin Pelayaran",
						"text" => "$id : Buat DO",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelayaran',
						'ncr_id' => $id,
						"title" => "Admin Pelayaran",
						"body" => "$id : Buat DO"
					)
				);
				app::pushNotifSend($datapush);
			}

			$rs['devices'] = db::get_recordset("user_device","category='beacukai' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Beacukai",
						"text" => "$p_num_pib : Buat Dokumen Beacukai",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'beacukai',
						'ncr_id' => $id,
						"title" => "Admin Beacukai",
						"body" => "$p_num_pib : Buat Dokumen Beacukai"
					)
				);
				app::pushNotifSend($datapush);
			}
		}else{

			$id_notif = rand(1, 100).date("dmYHis");
			$sqls = "insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_notif','2120082018161449','$id',1,'Dokumen No. PIB $p_num_pib perlu diproses DO','".$app['me']['id']."',now())";
			db::qry($sqls);

			$id_notif = rand(1, 100).date("dmYHis");
			$sqls321123 = "insert into ".$app['table']['notif']." 
						  (id,id_rule,id_import,status,pesan,created_by,created_at) values
						  ('$id_notif','5403092018114622','$id',1,'Dokumen No. PIB $p_num_pib perlu dibuat EIR Bundle','".$app['me']['id']."',now())";
			db::qry($sqls321123);

			$rs['devices'] = db::get_recordset("user_devices","category='pelayaran' AND status='active'");

			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Pelayaran",
						"text" => "$id : Buat DO",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelayaran',
						'ncr_id' => $id,
						"title" => "Admin Pelayaran",
						"body" => "$id : Buat DO"
					)
				);
				app::pushNotifSend($datapush);
			}

			$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					// "to" => db::lookup("token","device","id",$user_devices['id_device']),
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Pelabuhan/EIR",
						"text" => "$p_num_pib : Buat EIR",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelabuhan',
						'ncr_id' => $id,
						"title" => "Admin Pelabuhan/EIR",
						"body" => "$p_num_pib : Buat EIR"
					)
				);
				app::pushNotifSend($datapush);
			}
		}
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		// header("location: " .url::get_referer());
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['import']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : trucking
*******************************************************************************/
if ($act == "trucking"):
admlib::validate('ATV');
	if($step == 'add')
	{
		// $nominal = str_replace('Rp ', '', $nom);
		// $nominal = str_replace(',', '', $nominal);
		$id_c  		= rand(1, 100).date("dmYHis");
		$id_rc  	= rand(1, 100).date("dmYHis");
		$id_notif2  = rand(1, 100).date("dmYHis");
		$date_kirim = $p_dd;
		$p_dd = date("Y-m-d",strtotime($p_dd));
		$sql   = "insert into ".$app['table']['trucking']." 
				(id,id_import,date_delivery,id_container,id_armada,created_by,created_at,ket_doc) values
				('$id_c','$pib','$p_dd','$cont','$arm','".$app['me']['id']."',now(),'$ket_doc')";
		$sqlss ="update ". $app['table']['armada']." set status = 'depart' WHERE id = '$arm'";
		$sqlsss ="update ". $app['table']['container']." set status = 'depart' WHERE id = '$cont'";

		$reorders = db::lookup("max(reorder)","return_container","1=1");
		$sqlsssss="insert into ".$app['table']['return_container']."
			(id,id_container,id_trucking,reorder,created_by,created_at,id_import) values
			('$id_rc','$cont','$id_c','$reorders','".$app['me']['id']."',now(),'$pib')";


		// $form_user_detail = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);
		$form_cont = db::get_record("num_container","container", "id", $cont);
		$form_arm  = db::get_record("name","armada", "id", $arm);
		$form_imp  = db::get_record("import", "id", $pib);

		$nama_cont = $form_cont['num_container'];
		$nama_arm  = $form_arm['name'];
		$nama_imp  = $form_imp['num_pib'];

		// $sqlssss ="insert into ".$app['table']['notif']." 
		// 		(id,id_rule,status,pesan,created_by,created_at) values
		// 		('$id_c','".$form_user_detail['id_rule']."',1,'Kiriman baru dengan no pib = $nama_imp, nama containernya = $nama_cont,pengemudinya = $nama_arm ','".$app['me']['id']."',now())";
		$sqls32123 = "insert into ".$app['table']['notif']." 
				      (id,id_rule,id_import,id_container,status,pesan,created_by,created_at) values
					  ('$id_notif2','5703092018105342','$pib','$cont',1,'Kiriman dengan no pib : ".$nama_imp." Siap Dikirim','$me',now())";
		db::qry($sqls32123);

		

#echo str_replace("world","Peter","Hello world!");
/*		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while ($row321	= db::fetch($rs['lang'])) {
			$sqlssssss ="UPDATE ".$app['table']['status']." set 
				positions=concat('".app::getliblang('proses_jadwal_truck',$row321['alias'])." : <b>".format_tgl_bahasa($date_kirim,$row321['alias'])."</b>,',positions),
				updated_by = '". $app['me']['id'] ."',
				updated_at = now()
				WHERE id_import ='$pib' AND lang = '".$row321['alias']."' AND id_container = '$cont' ";
				db::qry($sqlssssss);
		}*/

		db::qry($sql);
		db::qry($sqlss);
		db::qry($sqlsss);
		db::qry($sqlsssss);


		$cek_sur_jal = db::lookup("surat_jalan","container","id",$cont);
		unlink($app['doc_path'] ."/do_pdf/".$cek_sur_jal);
			$content 	= file_get_contents($app['path'] .'/api/dsp_print.html');
		// $content 	= str_ireplace('{NO}', $num .'/D115200/'. $romawi[date('n')-1] .'.'. date('Y'), $content);
		$form_cont = db::get_record("container","id",$cont);
		$logo_company = db::lookup("logo","company","id",$form_imp['id_company']);
		$cust_name = db::lookup("name","customer","id",$form_imp['id_customer']);
		$date_delivery 	= db::lookup("date_delivery","trucking","id_container = '".$form_cont['id']."' and id_import = '".$form_imp['id']."' ");
		$date_delivery 	= date("d - m - Y",strtotime($date_delivery));
		$pdf_name = $form_imp['num_pib']."-".$form_cont['num_container'].rand(1, 100).".pdf";

		$content 	= str_ireplace('{IMG_COMPANY}', $app['data_lib']."/".$logo_company, $content);
		$content 	= str_ireplace('{NUM_PIB}', $form_imp['num_pib'], $content);
		$content 	= str_ireplace('{DATE_DELIV}', $date_delivery, $content);
		$content 	= str_ireplace('{CUSTOMER}', $cust_name, $content);
		$content 	= str_ireplace('{NAMA_CONT}', $form_cont['num_container'], $content);
		$content 	= str_ireplace('{QTY}', $form_cont['qty'], $content);
		$content 	= str_ireplace('{JEN_BAR}', $form_cont['note'], $content);
		$content 	= str_ireplace('{SEAL}', $form_cont['seal'], $content);
		
		
		$mpdf 		= new mPDF('c', 'A4');
		$mpdf->WriteHTML($content);
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->Output($app['doc_path'] ."/do_pdf/". $pdf_name, 'F');
		// $mpdf->Output($app['doc_path'] ."/do_pdf/test.pdf", 'F');
		
		$sql="update ". $app['table']['container'] ." set surat_jalan = '$pdf_name' where id ='$cont' ";			
		// $sql ="insert into ".$app['table']['seal']."
		// 	(id, id_import, id_container, created_at,keterangan) values 
		// 	('$id', '$id_imp', '$id_con', now(),'$p_keterangan')";
		db::qry($sql);

		$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
		while($user_devices = db::fetch($rs['devices'])){
			$datapush = array(
				// "to" => db::lookup("token","device","id",$user_devices['id_device']),
				"to" => db::lookup("token","device","id",$user_devices['id_device']),
				"notification" => array(
					"title" => "Admin Pelabuhan/EIR",
					"text" => $form_imp['num_pib']." - ".$form_cont['num_container']." : Download surat jalan",
					"sound" => "default"
				),
				'data' => array(
					'method' => 'pelabuhan',
					'ncr_id' => $form_imp['id'],
					"title" => "Admin Pelabuhan/EIR",
					"body" => $form_imp['num_pib']." - ".$form_cont['num_container']." : Download surat jalan"
				)
			);
			app::pushNotifSend($datapush);
		}
		$id_notif   = rand(1, 100).date("dmYHis");
		$sql_notif = "insert into ".$app['table']['notif']." 
				(id,id_rule,id_import,status,pesan,created_by,created_at) values
				('$id_notif','5403092018114622','".$form_imp['id']."',1,'".$form_imp['num_pib']." : Buat Kelengkapan Container','".$app['me']['id']."',now())";
		db::qry($sql_notif);

		// $_SESSION['trucking'][] = ['c'=>$nama_cont, 'a'=>$nama_arm];
		$_SESSION['trucking'][] = ['c'=>$nama_cont, 'a'=>$nama_arm, 'jt'=>$jt,'p_dd'=>$p_dd,'ket_doc'=>$ket_doc];
		exit;
	}elseif($step == 'delete'){
		// $sqls 		= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_import` IN ('". $_SESSION['trucking'][$indx]["im"]."') AND `id` IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND `id_armada` IN ('". $_SESSION['trucking'][$indx]["idarm"]."')";
		// $sqlss 		="update ". $app['table']['armada']." set status = 'stay' WHERE id = '". $_SESSION['trucking'][$indx]["idarm"]."'";
		// $sqlsss 	="update ". $app['table']['container']." set status = 'stay' WHERE id = '". $_SESSION['trucking'][$indx]["ic"]."'";
		// $sqlssss 	= "DELETE FROM ". $app['table']['notif'] ." WHERE `id_import` IN ('". $_SESSION['trucking'][$indx]["im"]."') AND `id` IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND `id_armada` IN ('". $_SESSION['trucking'][$indx]["idarm"]."')";
		// db::qry($sqls);
		// db::qry($sqlss);
		// db::qry($sqlsss);
		// db::qry($sqlssss);
		// unset($_SESSION['trucking'][$indx]);
		$sqls 		= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_import` IN ('". $_SESSION['trucking'][$indx]["im"]."') AND `id` IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND `id_armada` IN ('". $_SESSION['trucking'][$indx]["idarm"]."')";
		$sqlss 		="update ". $app['table']['armada']." set status = 'stay' WHERE id = '". $_SESSION['trucking'][$indx]["idarm"]."'";
		$sqlsss 	="update ". $app['table']['container']." set status = 'stay' WHERE id = '". $_SESSION['trucking'][$indx]["ic"]."'";
		$sqlssss 	= "DELETE FROM ". $app['table']['notif'] ." WHERE `id_import` IN ('". $_SESSION['trucking'][$indx]["im"]."') AND `id` IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND `id_armada` IN ('". $_SESSION['trucking'][$indx]["idarm"]."')";

		$statusnya_adalah = db::lookup("status","return_container","id_import IN ('". $_SESSION['trucking'][$indx]["im"]."') AND id_trucking IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND id_armada IN ('". $_SESSION['trucking'][$indx]["idarm"]."')");

		// if ($statusnya_adalah!="selesai") {
			// $sqlsssss 	= "DELETE FROM ". $app['table']['return_container'] ." WHERE id_trucking IN ('". $_SESSION['trucking'][$indx]['pk']."') AND id_armada IN ('". $_SESSION['trucking'][$indx]['idarm']."')";
			$sqlsssss 	= "DELETE FROM ". $app['table']['return_container'] ." WHERE id_container IN ('".$_SESSION['trucking'][$indx]["ic"]."')";

		db::qry($sqlsssss);
// }

		// $myfile = fopen("abcde.txt", "w") or die("Unable to open file!");
		// fwrite($myfile, $sqlsssss);
		// fclose($myfile);
			db::qry($sqls);
			db::qry($sqlss);
			db::qry($sqlsss);
			db::qry($sqlssss);

		$cek_sur_jal = db::lookup("surat_jalan","container","id",$_SESSION['trucking'][$indx]["ic"]);
		unlink($app['doc_path'] ."/do_pdf/".$cek_sur_jal);

		$sql="update ". $app['table']['container'] ." set surat_jalan = '$pdf_name' where id ='".$_SESSION['trucking'][$indx]["ic"]."' ";
		db::qry($sql);

		unset($_SESSION['trucking'][$indx]);
		exit;
	}
	include "dsp_list.php";
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['import']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
		$items = implode("','", $p_del);
/*		$sql = "
		SELECT a.id,CONCAT(a.num_pib,' - ',b.num_container) as title,b.id id_container,c.id id_delivery FROM ". $app['table']['import'] ." a
		left join ". $app['table']['container'] ." b on a.id=b.id_import
		left join ". $app['table']['delivery_order'] ." c on a.id=c.id_import
		WHERE a.id IN ('". $items ."')";*/
		$sql = "
		SELECT a.id,a.num_pib as title,b.id id_container,c.id id_delivery FROM ". $app['table']['import'] ." a
		left join ". $app['table']['container'] ." b on a.id=b.id_import
		left join ". $app['table']['delivery_order'] ." c on a.id=c.id_import
		WHERE a.id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		$abc="yes";
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 		= implode("','", $p_id);
		$delid2 	= implode("','", $id_delivery);
		$reorders = db::lookup("max(reorder)","container","1=1");
		// $id_importnya = db::lookup("");
		// echo "DELETE FROM ". $app['table']['import'] ." WHERE `id` IN ('". $delid ."')";
		// exit;
		$sql 		= "DELETE FROM ". $app['table']['import'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		$sqls 		= "DELETE FROM ". $app['table']['container'] ." WHERE `id_import` IN ('". $delid ."')";
		$sqlss 		= "DELETE FROM ". $app['table']['delivery_order'] ." WHERE `id_import` IN ('". $delid ."')";
		// $sqlsss 	= "DELETE FROM ". $app['table']['notif'] ." WHERE `id_import` IN ('". $delid ."')";
		$sqlssss	= "DELETE FROM ". $app['table']['expired_do'] ." WHERE id_delivery_order IN ('". $delid2 ."')";
		$sqlsssss	= "DELETE FROM ".$app['table']['trucking']." WHERE `id_import` IN ('". $delid ."')";
		$sqlssssss	= "DELETE FROM ". $app['table']['return_container'] ." WHERE `id_import` IN ('". $delid2 ."')";	
		$sqlssssss2	= "DELETE FROM ". $app['table']['notif'] ." WHERE `id_import` IN ('". $delid ."')";	
		$sqlssssss3	= "DELETE FROM ". $app['table']['status'] ." WHERE `id_import` IN ('". $delid ."')";	
		db::qry($sqlssssss3);

		$sql = "SELECT name FROM ".$app['table']['file_bea']." WHERE `id_import` IN ('". $delid ."') ";
		db::query($sql, $rs['row'], $nr['row']);
		while($row = db::fetch($rs['row']))
		{
			@unlink($app['doc_path']."/sppb/".$row['name']);
		}

		$sql = "SELECT name FROM ".$app['table']['file_do']." WHERE `id_import` IN ('". $delid ."') ";
		db::query($sql, $rs['row'], $nr['row']);
		while($row = db::fetch($rs['row']))
		{
			@unlink($app['doc_path']."/do/".$row['name']);
		}

		$sql = "SELECT name FROM ".$app['table']['file_eir']." WHERE `id_import` IN ('". $delid ."') ";
		db::query($sql, $rs['row'], $nr['row']);
		while($row = db::fetch($rs['row']))
		{
			@unlink($app['doc_path']."/sppb/".$row['name']);
		}

		$sql = "SELECT name FROM ".$app['table']['file_kel']." WHERE `id_import` IN ('". $delid ."') ";
		db::query($sql, $rs['row'], $nr['row']);
		while($row = db::fetch($rs['row']))
		{
			@unlink($app['doc_path']."/out_cont/".$row['name']);
		}

		$sqlssssss3	= "DELETE FROM ". $app['table']['file_bea'] ." WHERE `id_import` IN ('". $delid ."')";	
		db::qry($sqlssssss3);
		$sqlssssss3	= "DELETE FROM ". $app['table']['file_do'] ." WHERE `id_import` IN ('". $delid ."')";	
		db::qry($sqlssssss3);
		$sqlssssss3	= "DELETE FROM ". $app['table']['file_eir'] ." WHERE `id_import` IN ('". $delid ."')";	
		db::qry($sqlssssss3);
		$sqlssssss3	= "DELETE FROM ". $app['table']['file_kel'] ." WHERE `id_import` IN ('". $delid ."')";	
		db::qry($sqlssssss3);
		$sqlssssss3	= "DELETE FROM ". $app['table']['beacukai'] ." WHERE `id_import` IN ('". $delid ."')";	
		db::qry($sqlssssss3);
		$sqlssssss3	= "DELETE FROM ". $app['table']['pelabuhan'] ." WHERE `id_import` IN ('". $delid ."')";	
		db::qry($sqlssssss3);

		db::qry($sqlssssss2);
		db::qry($sqlssssss);
		db::qry($sqlsssss);
		db::qry($sqlssss);
		// db::qry($sqlsss);
		db::qry($sqlss);
		db::qry($sqls);
		// db::qry($sql);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;

if($act == "container")
{	
	admlib::validate('UPDT');
	if($step == "add")
	{
		// $aryant = "gagal";
			$jenis = preg_replace('/[^A-Za-z0-9\-]/', '', $jenis);
			$reorders = db::lookup("max(reorder)","container","1=1");
			$nominal = str_replace('Rp ', '', $nom);
			$nominal = str_replace(',', '', $nominal);
			$id_c  = rand(1, 100).date("dmYHis").rand(1, 100);
			isset($dd)?$dd = date("Y-m-d",strtotime($dd)):null;
			if (isset($dd)) {
					$da_lev=",delivery_date";
					$deliverynya = ",'". $dd ."'";
			}
		if ($idm!="") 
		{
			// $aryant="aryant";
			$sqlzx  = "insert into ". $app['table']['container'] ."
			(id, id_import, num_container, qty, note, value, reorder, created_by, created_at$da_lev,address) values ('$id_c', '$idm', '$num', '$jenis', '$desc', '$nominal', '$reorders','$me',now()$deliverynya,'$adr')";
			db::qry($sqlzx);
			$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
			db::query($sql_lang, $rs['lang'], $nr['lang']);
			while ($row	= db::fetch($rs['lang'])) {
				$id_c2 = rand(1, 100).date("dmYHis");
		 		// $sqlss321 ="insert into ".$app['table']['status']." 
					// 		(id,id_import,id_container,positions,created_by,created_at,lang) values
					// 		('$id_c2','$idm','$id_c','".app::getliblang('proses_import',$row['alias'])."','".$app['me']['id']."',now(),'".$row['alias']."')";
				// db::qry($sqlss321);
			}
		}


// $req_dump = print_r($_REQUEST, TRUE);
// $fp = fopen("abcde123.txt", "w") or die("Unable to open file!");
// fwrite($fp, $req_dump);
// fclose($fp);
		// $myfile = fopen("abcde.txt", "w") or die("Unable to open file!");
		// fwrite($myfile, $sqlzx);
		// fclose($myfile);

		$_SESSION['container'][] = ['c'=>$num, 'j'=>$jenis, 'd'=>$desc, 'n'=>$nominal, 'dd'=>$dd,'adr'=>$adr,'im'=>$idm,'pk'=>$id_c];
		exit;
	}elseif($step == "update"){
		// $jenis = preg_replace('/[^A-Za-z0-9\-]/', '', $jenis);
		$id_importnya = $_SESSION['container'][$indx]["im"];
		$id_pknya =  $_SESSION['container'][$indx]["pk"];
		$reorders = db::lookup("max(reorder)","container","1=1");
		$nominal = str_replace('Rp ', '', $nil);
		$nominal = str_replace(',', '', $nominal);
		$id_c  = rand(1, 100).date("dmYHis");
		$id_not  = rand(1, 100).date("dmYHis");
		$dd = date("Y-m-d",strtotime($dd));

		if ($dd !="1970-01-01" && $dd !="0000-00-00" && $dd !="") {
				$da_lev="delivery_date = '$dd',";
		}
		// $sqlsq  = "update ". $app['table']['container'] ." 
		// 		set num_container 	= '$contain',
		// 			note 			= '$ket',
		// 			value 			= '$nominal',
		// 			qty 	 		= '$qty',
		// 			delivery_date 	= '$dd',
		// 			address 		= '$adr',
		// 			updated_by		= '$me',
		// 			updated_at		= now()
		// 			WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		$sqlsq  = "update ". $app['table']['container'] ." 
				set num_container 	= '$contain',
					note 			= '$ket',
					value 			= '$nominal',
					qty 	 		= '$qty',
					$da_lev
					address 		= '$adr',
					updated_by		= '$me',
					updated_at		= now()
					WHERE `id_import` IN ('". $id_importnya."') AND `id` IN ('". $id_pknya."')";
		db::qry($sqlsq);

		// if (date("Y-m-d") <= $dd) {
		// 	// $cobanya = date("Y-m-d")."<=".$dd = date("Y-m-d",strtotime($dd));
		// 	// $cobanya = "Khalid";
		// $req_dump = print_r($sqlsq, TRUE);
		// $myfile = fopen("abcde.txt", "w") or die("Unable to open file!");
		// fwrite($myfile, $req_dump);
		// fclose($myfile);
		// 	$sqlnya = "INSERT INTO ".$app['table']['notif']."
		// 			  (id,id_rule,id_import,status,pesan,created_by,created_at) values
		// 			  ('$id_not','2120082018161449','$id',1,'Kiriman baru dengan no pib = $p_num_pib','".$app['me']['id']."',now() ";
		// }else{			
		// 	// $cobanya = "Aryant";
		// 	$sqlnya = "";
		// }
		// db::qry($sqlnya);
		if ($dd != "") {
		$_SESSION['container'][$indx] = ['c'=>$contain, 'j'=>$qty, 'd'=>$ket, 'n'=>$nominal, 'dd'=>$dd,'adr'=>$adr,"im" => $id_importnya,"pk" => $id_pknya ];
		}else{
		$_SESSION['container'][$indx] = ['c'=>$contain, 'j'=>$qty, 'd'=>$ket, 'n'=>$nominal,'adr'=>$adr,"im" => $id_importnya,"pk" => $id_pknya];
		}
		exit;
	}elseif($step == "delete"){

 		$sql ="DELETE FROM  ".$app['table']['status']." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')  ";
		db::qry($sql);

		$sqls 		= "DELETE FROM ". $app['table']['container'] ." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		$sqls321 	= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id_container` IN ('". $_SESSION['container'][$indx]["pk"]."')";
	
		$sqlss321 	= "DELETE FROM ". $app['table']['return_container'] ." WHERE `id_container` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		db::qry($sqls);
		db::qry($sqls321);
		db::qry($sqlss321);


		unset($_SESSION['container'][$indx]);
		exit;
	}
	include "dsp_list.php";
}


/*******************************************************************************
* Action : View
*******************************************************************************/
if($act == 'view'):
	// admlib::validate('APPR');
	admlib::validate('DSPL');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$status_beacukai = db::get_record("status","beacukai", "id_import", $id);
		$status_beacukai = strtoupper($status_beacukai["status"]);

		if ($status_beacukai == "SPJM") {
			$status_beacukai = "<font color = 'red'><b>$status_beacukai</b></font>";
		}
		elseif ($status_beacukai == "SPJK") {
			$status_beacukai = "<font color = 'yellow'><b>$status_beacukai</b></font>";
		}
		elseif ($status_beacukai == "SPJH") {
			$status_beacukai = "<font color = 'green'><b>$status_beacukai</b></font>";
		}
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
		$rs['container'] = db::get_recordset("container","id_import='".$id."'");
		$do = db::get_record("delivery_order", "id_import", $id);
		$pelabuhan = db::get_record("pelabuhan", "id_import", $id);
		$dok_kel = db::lookup("name","file_kel", "id_import", $id);
		$rs['exp_date'] = db::get_record_select("expired_do", "expired_do", "id_delivery_order='".$do['id']."' order by reorder ASC");
		// $total2 = db::lookup('COUNT(id)', 'expired_do', 'id_delivery_order = '.$do['id']);		

		// $rs['containers'] = db::get_record_select(" id, CONCAT(num_container,', ',qty,', ',note) AS num_container" , "container","id_import='".$id."' AND status='stay' ORDER BY reorder ASC");
		// $rs['containers'] = db::get_record_select("id, CONCAT(num_container,', ',note,', ',DATE_FORMAT(delivery_date, '%d - %m - %Y')) AS num_container" , "container","id_import='".$id."' AND status='stay' ORDER BY reorder ASC");
		// $rs['containers'] = db::get_record_select("id, CONCAT(num_container,' | ',note) AS num_container" , "container","id_import='".$id."' AND status='stay' ORDER BY reorder ASC");
		$rs['containers'] = db::get_record_select("id, CONCAT(num_container,' | ',note) AS num_container" , "container","id_import='".$id."' ORDER BY reorder ASC");

		// $rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS armada" ,"armada","status='stay' ORDER BY reorder ASC");
		$rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS armada" ,"armada");
		
		// $rs['container123'] = db::get_record_select("num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr", "container", "id_import", $id);
		$rs['container123'] = db::get_record_select("num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr", "container", "id_import = '".$id."' ORDER BY reorder ASC");

		// $sql123321 = "select num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr from ".$app['table']["container"]." where id_import = '$id' ORDER BY delivery_date,id DESC ";
		// db::query($sql123321, $rs['container123'], $nr['container123']);

		$rs['trucking'] = db::get_record_select("a.num_container c,c a", "trucking", "id_import", $id);
		// $sqltr = "select cont.num_container c, arm.name a,tr.id_import im,tr.id_container ic,tr.id pk,tr.id_armada idarm from ".$app['table']['trucking']." tr inner JOIN ".$app['table']['container']." cont on tr.id_container = cont.id inner join ".$app['table']['armada']." arm on tr.id_armada = arm.id where tr.id_import='$id'";
/*		$sqltr = "select cont.num_container c, arm.name a,tr.id_import im,tr.id_container ic,tr.id pk,tr.id_armada idarm,cont.delivery_date jt,cont.address adr, tr.date_delivery p_dd from ".$app['table']['trucking']." tr inner JOIN ".$app['table']['container']." cont on tr.id_container = cont.id inner join ".$app['table']['armada']." arm on tr.id_armada = arm.id where tr.id_import='$id'";*/
		$sqltr = "select cont.num_container c, tr.id_import im,tr.id_container ic,tr.id pk,cont.delivery_date jt,cont.address adr, tr.date_delivery p_dd, tr.ket_doc ket_doc from ".$app['table']['trucking']." tr inner JOIN ".$app['table']['container']." cont on tr.id_container = cont.id  where tr.id_import='$id'";
		db::query($sqltr, $rs['trucking321'], $nr['trucking321']);
// if ($status_n =="inactive") {

		// $form = db::get_record("import", "id", $id);
	// $sqlupt="UPDATE ".$app['table']['notif']." set status = 0 where id = '$id_notif' ";
	$form_id_rule = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);
	$sqlupt		  = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '".$form_id_rule['id_rule']."' ";
	db::qry($sqlupt);
// }
		$_SESSION['container'] = [];
		while($row = db::fetch($rs['container123'])){
			$_SESSION['container'][] = $row;
		}
		$_SESSION['trucking'] = [];
		while($row = db::fetch($rs['trucking321'])){
			$_SESSION['trucking'][] = $row;
		}
		// print_r($_SESSION['container']);
		form::populate($form);
		include "dsp_view.php";
		exit;
	endif;
if ($step == "import") {
		form::serialize_form();
		// print_r($_SESSION['income']);
		// form::validate('empty','p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$t_containter 	  = 0;
		$container 		  = $_SESSION['container'];
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		/*$p_date_receipt = date("Y-m-d",strtotime($p_date_receipt));
		$p_eta = date("Y-m-d",strtotime($p_eta));
		$sql = "update ". $app['table']['import'] ."
				set num_pib 		= '$p_num_pib',
					num_bl 			= '$p_num_bl',
					name_ship 		= '$p_name_ship',
					eta		 		= '$p_eta',
					id_customer 	= '$customer_name',
					id_company	 	= '$company',
					date_receipt 	= '$p_date_receipt',
					amount_payment 	= '$p_amount_payment',
					status_beacukai	= '$status_beacukai',
					nopen		 	= '$p_nopen',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '$id'";*/

		$p_num_pib_berubah = explode(" ", $p_num_pib);

		if ($p_company_orig != $p_company) {	
			// if ($p_num_pib_orig != $p_num_pib ) {
				$pib_asli = db::lookup("num_pib_asli","import","id='".$id."' ");
				$kode_company = db::lookup("code","company","id='$company'");
				$p_num_pib = $pib_asli.' '.$kode_company ;
			// }
		}
		
		if ($num_pib_asli != $p_num_pib_berubah[0]) {
		 	$p_num_pib = $p_num_pib_berubah[0];
			$berubah_pib = "num_pib_asli	= '$p_num_pib',";

			$pib_asli = db::lookup("num_pib_asli","import","id='".$id."' ");
			$kode_company = db::lookup("code","company","id='$company'");
		 	$p_num_pib = $p_num_pib.' '.$kode_company ;
		}
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);
		
		$p_nilai_sptnp = str_replace('Rp ', '', $p_nilai_sptnp);
		$p_nilai_sptnp = str_replace(',', '', $p_nilai_sptnp);
		$p_eta = date("Y-m-d",strtotime($p_eta));
		$p_date_receipt = date("Y-m-d",strtotime($p_date_receipt));
		$p_tanggal_sptnp = date("Y-m-d",strtotime($p_tanggal_sptnp));
		$p_tgl_nopen = date("Y-m-d",strtotime($p_tgl_nopen));

		if ($status_beacukai == "spjh") {
			$column_status_sppb ="status_sppb = '1', ";
		}	
		$sql = "update ". $app['table']['import'] ."
				set num_pib 		= '$p_num_pib',
					num_bl 			= '$p_num_bl',
					name_ship 		= '$p_name_ship',
					eta		 		= '$p_eta',
					id_customer 	= '$customer_name',
					id_company	 	= '$company',
					date_receipt 	= '$p_date_receipt',
					pfpd 			= '$p_pfpd',
					ket_cust 		= '$p_ket_cust',
					amount_payment 	= '$p_amount_payment',
					status_beacukai	= '$status_beacukai',
					nopen		 	= '$p_nopen',
					tgl_nopen		= '$p_tgl_nopen',
					$column_status_sppb
					tanggal_sptnp	= '$p_tanggal_sptnp',
					nomor_sptnp		= '$p_nomor_sptnp',
					nilai_sptnp		= '$p_nilai_sptnp',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '$id'";
		db::qry($sql);
		if ($status_beacukai != "spjm") 
		{
			$id_notif = rand(1, 100).date("dmYHis");
			$sqls = "insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_notif','2120082018161449','$id',1,'Dokumen No. PIB $p_num_pib perlu diproses DO','".$app['me']['id']."',now())";

			$id_notif = rand(1, 100).date("dmYHis");
			$sqls321123 = "insert into ".$app['table']['notif']." 
						(id,id_rule,id_import,status,pesan,created_by,created_at) values
						('$id_notif','7903092018114332','$id',1,'Kiriman dengan no pib = $p_num_pib, statusnya diubah dengan : $status_beacukai','".$app['me']['id']."',now())";
			db::qry($sqls);
			db::qry($sqls321123);

			$rs['devices'] = db::get_recordset("user_devices","category='pelayaran' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					// "to" => db::lookup("token","device","category","pelayaran"),
					"notification" => array(
						"title" => "Admin Pelayaran",
						"text" => "$id : Buat DO",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelayaran',
						'ncr_id' => $id,
						"title" => "Admin Pelayaran",
						"body" => "$id : Buat DO"
					)
				);
				app::pushNotifSend($datapush);
			}
			
			$rs['devices'] = db::get_recordset("user_device","category='beacukai' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					// "to" => db::lookup("token","device","category","pelayaran"),
					"notification" => array(
						"title" => "Admin Beacukai",
						"text" => "$p_num_pib : Buat Dokumen Beacukai",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'beacukai',
						'ncr_id' => $id,
						"title" => "Admin Beacukai",
						"body" => "$p_num_pib : Buat Dokumen Beacukai"
					)
				);
				app::pushNotifSend($datapush);
			}
		}else{

			$id_notif = rand(1, 100).date("dmYHis");
			$sqls = "insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_notif','2120082018161449','$id',1,'Dokumen No. PIB $p_num_pib perlu diproses DO','".$app['me']['id']."',now())";

			$id_notif = rand(1, 100).date("dmYHis");
			$sqls321123 = "insert into ".$app['table']['notif']." 
						  (id,id_rule,id_import,status,pesan,created_by,created_at) values
						  ('$id_notif','5403092018114622','$id',1,'Dokumen No. PIB $p_num_pib perlu dibuat EIR Bundle','".$app['me']['id']."',now())";
			db::qry($sqls);
			db::qry($sqls321123);

			$rs['devices'] = db::get_recordset("user_devices","category='pelayaran' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					// "to" => db::lookup("token","device","category","pelayaran"),
					"notification" => array(
						"title" => "Admin Pelayaran",
						"text" => "$id : Buat DO",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelayaran',
						'ncr_id' => $id,
						"title" => "Admin Pelayaran",
						"body" => "$id : Buat DO"
					)
				);
				app::pushNotifSend($datapush);
			}
			$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					// "to" => db::lookup("token","device","id",$user_devices['id_device']),
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Pelabuhan/EIR",
						"text" => "$p_num_pib : Buat EIR",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelabuhan',
						'ncr_id' => $id,
						"title" => "Admin Pelabuhan/EIR",
						"body" => "$p_num_pib : Buat EIR"
					)
				);
				app::pushNotifSend($datapush);
			}
		}
		msg::set_message('success', app::getliblang('modify'));
		// header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		header("location: " .url::get_referer());
		exit;
}
	if ($step == 2):
		admlib::validate('APPR');
		form::serialize_form();
		// form::validate('empty', 'p_no_do,p_date_receipt_do,p_expired_do');
		// if (form::is_error()):
			// msg::build_msg();
			// header("location: ". admlib::$page_active['module'] .".mod&act=view&error=1&id=" . $id);
			// exit;
		// endif;
		$do = db::get_record("delivery_order", "id_import", $id);
		// print_r($_POST);
		// echo "<br>";
		// echo "<br>";
		// echo count($do);
		// print_r($do);
		// exit;
		
		if($do['id_import'] == $id){
			$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$sql = "update ". $app['table']['delivery_order'] ."
					set no_do 				= '$p_no_do',
						date_receipt_do 	= '$p_date_receipt_do',
						updated_by 			= '". $app['me']['id'] ."',
						updated_at 			= now()
					where id = '".$do['id']."'";
			db::qry($sql);
			if(!empty($p_expired_do)){
				$ids = rand(1, 100).date("dmYHis");
				$sqls = "insert into ".$app['table']['expired_do']."
						(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '".$do['id']."', '$p_expired_do', '". $app['me']['id'] ."', now())";
				db::qry($sqls);
			}
		}else{
			$ids = rand(1, 100).date("dmYHis");
			$idx = rand(1, 100).date("dmYHis");
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$reorder = db::lookup("max(reorder)","delivery_order","1=1");
			if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
			$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
			$sql = "insert into ".$app['table']['delivery_order']."
					(id, id_import, no_do, date_receipt_do, id_expired_do, reorder, created_by, created_at) values
					('$ids', '$id', '$p_no_do', '$p_date_receipt_do', '$idx', '$reorder', '". $app['me']['id'] ."', now())";
			db::qry($sql);
			if(!empty($p_expired_do)){
				$sqls = "insert into ".$app['table']['expired_do']."
						(id, id_delivery_order, expired_do, created_by, created_at) values ('$idx', '$ids', '$p_expired_do', '". $app['me']['id'] ."', now())";
				db::qry($sqls);
			}
		}
		
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
if ($act == "del_or") {
	if($step == "add"){
		form::serialize_form();
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;

		if (strtotime($p_expired_do) < time()) {
			$statusnya_column=",status";
			$status_do = ",'expired'";	
		}
		$t_containter 	  = 0;
		$container 		  = $_SESSION['container'];
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);
		$id 			  = rand(1, 100).date("dmYHis");
		$ids 			  = rand(1, 100).date("dmYHis");
		$id_c 			  = rand(1, 100).date("dmYHis");
		$reorder 		  = db::lookup("max(reorder)","delivery_order","1=1");		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
		$p_expired_do = date("Y-m-d",strtotime($p_expired_do));
		$sql = "insert into ".$app['table']['delivery_order']."
				(id, no_do, date_receipt_do, id_expired_do, reorder, created_by, created_at,id_import$statusnya_column) values
				('$id', '$p_no_do', '$p_date_receipt_do', '$ids', '$reorder', '$me', now(),
				'$id_imp'$status_do)";
		$sqls = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id','$p_expired_do', '$me', now())";
		// $sqlss ="UPDATE ".$app['table']['status']." set 
		// 		positions=concat('Pembuatan Delivery Order,','<br>',positions),
		// 		updated_by = '". $app['me']['id'] ."',
		// 		updated_at = now()
		// 		WHERE id_import ='$id_imp'";
		// $sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		// db::query($sql_lang, $rs['lang'], $nr['lang']);
		// while ($row	= db::fetch($rs['lang'])) {
		// $sqlss ="UPDATE ".$app['table']['status']." set 
		// 		positions=concat('".app::getliblang('proses_del_or',$row['alias']).",','<br>',positions),
		// 		updated_by = '". $app['me']['id'] ."',
		// 		updated_at = now()
		// 		WHERE id_import ='$id_imp' AND alias = '".$row['alias']."'";
		// 		db::qry($sqlss);
		// }
		$sqlsss ="insert into ".$app['table']['notif']." 
				(id,id_rule,id_import,status,pesan,created_by,created_at) values
				('$id_c','5403092018114622','$id_imp',1,'Dokumen No. PIB $p_num_pib' perlu diproses','".$app['me']['id']."',now())";
		db::qry($sql);
		db::qry($sqls);
		db::qry($sqlsss);
		$message  .= "<p>Date Receipt DO :<b>". $p_date_receipt_do ."</b></p>";
		$message  .= "<p>Expired DO : <b>". $p_expired_do ."</b></p>";
		// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='5403092018114622' ";
		db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
		while($row = db::fetch($rs['sql_bea_cukai'])){
			app::sendmail($row['email'], "Ada dokumen import masuk", $message);
		}
		msg::set_message('success', app::getliblang('create'));
		header("location: " .url::get_referer());
		exit;
}
	if ($step == "edit") {
		form::serialize_form();
//		form::validate('empty','p_no_do,p_date_receipt_do');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		if (strtotime($p_expired_do) >= time()) {
			$status_do = ",status = 'active' ";
		}else{
			$status_do = ",status = 'expired' ";	
		}
		// $total = db::lookup('COUNT(id)', 'expired_do', 'id_delivery_order = '.$id_idv_ord);
		// if($total < 3) $exp_date2 = "date_receipt_do 	= '$p_date_receipt_do',";
		app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
		$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
		$p_expired_do = date("Y-m-d",strtotime($p_expired_do));
		$sql = "update ". $app['table']['delivery_order'] ."
				set no_do 			= '$p_no_do',
				$exp_date2
				updated_by 			= '". $app['me']['id'] ."',
				updated_at 			= now()
				$status_do
				where id_import = '$id_imp'";
		db::qry($sql);
		if(!empty($p_expired_do)){
		$ids = rand(1, 100).date("dmYHis");
		$sqls = "update ".$app['table']['expired_do']." set
				 status = 'inactive' where id_delivery_order = '$id_idv_ord' ";
		db::qry($sqls);
		$sqlss = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id_idv_ord', '$p_expired_do','$me',now())";
		db::qry($sqlss);
		$message  .= "<p>Expired DO Telah diperpanjang sampai : <b>". $p_expired_do ."</b></p>";
		// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='5403092018114622' ";
		db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
			while($row = db::fetch($rs['sql_bea_cukai'])){
				app::sendmail($row['email'], "Perubahan Expired Date", $message);
			}
		}
		msg::set_message('success', app::getliblang('modify'));
		header("location: " .url::get_referer());
	}
}
if ($act== "send_email") {

	if ($status != "SPJM") {	
		$message  	   = "<p>Ada kiriman baru dengan PIB <b>". $num_pib ."</b></p>";
		// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Setujui Pelatihan</a></p>";
		// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Cek Data</a></p>";
		$message 	  .= "<p>Terima kasih</p>";
		$sql_pelayaran = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='2120082018161449' ";
		db::query($sql_pelayaran, $rs['sql_pelayaran'], $nr['sql_pelayaran']);
		while($row = db::fetch($rs['sql_pelayaran'])){
			app::sendmail($row['email'], "Ada dokumen import masuk", $message);
		}
		$message  = "<p>Ada kiriman baru dengan PIB <b>". $num_pib ."</b></p>";
		// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='7903092018114332' ";
		db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
		while($row = db::fetch($rs['sql_bea_cukai'])){
			app::sendmail($row['email'], "Ada dokumen import masuk", $message);
		}
		$id_notif = rand(1, 100).date("dmYHis");
		$sqls321123 = "insert into ".$app['table']['notif']." 
					(id,id_rule,id_import,status,pesan,created_by,created_at) values
					('$id_notif','7903092018114332','$id',1,'Kiriman Dengan pib = $p_num_pib, siap mengurus sppbnya','".$app['me']['id']."',now())";
		db::qry($sqls321123);
	}else{	
		$message  = "<p>Ada kiriman baru dengan PIB <b>". $num_pib ."</b></p>";
		// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Setujui Pelatihan</a></p>";
		// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Cek Data</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_pelayaran = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='2120082018161449' ";
		db::query($sql_pelayaran, $rs['sql_pelayaran'], $nr['sql_pelayaran']);
		while($row = db::fetch($rs['sql_pelayaran'])){
			app::sendmail($row['email'], "Ada dokumen import masuk", $message);
		}
		$message  = "<p>Ada kiriman baru dengan PIB <b>". $num_pib ."</b></p>";
		// $message .= "<p><a href='".$app['http']."/import.mod&act=view&id=".$id."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_eir  = "select c.email from ". $app['table']['user_det'] ." a 
					 inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					 where a.id_rule ='5403092018114622' ";
		db::query($sql_eir, $rs['sql_eir'], $nr['sql_eir']);
		while($row = db::fetch($rs['sql_eir'])){
			app::sendmail($row['email'], "Ada dokumen import masuk", $message);
		}
	}
		// app::sendmail('sam.developer009@gmail.com', "Ada dokumen import masuk", $message);
	/*$data_manager = db::get_record("view_user","id_unit_kerja='".$id_unit_kerja."' and privilege = 'RULE_MNG'");
	// print_r($data_manager);
	$data_pelatihan = db::get_record("pelatihan","id = '".$id_pelatihan."'");
	admlib::send_email($data_manager['email'],"Pemberitauan approval","Mohon untuk login dan segera memberi approval untuk inhouse dengan judul ".$data_pelatihan['judul']."");
	// header("location");
	$upd = "update ".$app['table']['persetujuan_pelatihan']." set approval = 'RULE_MNG', status='progress' where id_unit_kerja='".$id_unit_kerja."' and id_pelatihan = '$id_pelatihan'";
	// echo $upd;
	db::qry($upd);*/
	header("location: ".admlib::getext().""); 
	exit;	
}

if ($act =="ubah_status") {
	$num_pibnya=db::lookup("num_pib","import","id",$id);
	db::qry("update ". $app['table']['import'] ."
			set status_sppb	= '1'
			where id = '$id'");	
	$id_notif   = rand(1, 100).date("dmYHis");
	$sqls321123 = "insert into ".$app['table']['notif']." 
				  (id,id_rule,id_import,status,pesan,created_by,created_at) values
				  ('$id_notif','5403092018114622','$id',1,'Dokumen Dokumen No. PIB $num_pibnya perlu dibuat Dokumen Kelengkapan','".$app['me']['id']."',now())";
	db::qry($sqls321123);
	$num_pib = db::lookup("num_pib","import","id",$id);
	$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
	while($user_devices = db::fetch($rs['devices'])){
		$datapush = array(
			"to" => db::lookup("token","device","id",$user_devices['id_device']),
			"notification" => array(
				"title" => "Admin Pelabuhan/EIR",
				"text" => "$num_pib : Buat Dokumen Kelengkapan",
				"sound" => "default"
			),
			'data' => array(
				'method' => 'pelabuhan',
				'ncr_id' => $id,
				"title" => "Admin Pelabuhan/EIR",
				"body" => "$num_pib : Buat Dokumen Kelengkapan"
				// "body" => "Refresh Ada Notifikasi Baru"
			)
		);
		app::pushNotifSend($datapush);
	}
	/*$rs['devices'] = db::get_recordset("device","category='pelabuhan' AND status='active'");
	while($user_devices = db::fetch($rs['devices'])){
		$datapush = array(
			// "to" => db::lookup("token","device","id",$user_devices['id_device']),
			"to" => db::lookup("token","device","category","pelabuhan"),
			"notification" => array(
				"title" => "Admin Pelabuhan/eir",
				"text" => "$p_num_pib : Status SPPB Telah Dirubah",
				"sound" => "default"
			),
			'data' => array(
				'method' => 'pelabuhan',
				'ncr_id' => $id,
				"title" => "Admin Pelabuhan/eir",
				"body" => "$p_num_pib : Status SPPB Telah Dirubah"
			)
		);
		app::pushNotifSend($datapush);
	}*/
	
	header("location: ".admlib::getext().""); 
	exit;	
}
?>