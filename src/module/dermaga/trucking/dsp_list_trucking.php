<?php
$trucking = $_SESSION[$act];
if(count($trucking) > 0){ 
?>
<div class="table-responsive"> 
    <table class="table table-bordered"> 
        <thead> 
            <tr> 
                <th>#</th> 
                <th>Number Container</th> 
                <th>Armada</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
                $no = 0;
                foreach($trucking as $key => $val){ $no++;
					$cont = db::lookup("CONCAT(num_container,', ',qty,', ',note)","container","id='".$val['c']."'");
					$arm = db::lookup("CONCAT(name,', ',nopol)","armada","id='".$val['a']."'");
			?>
                <tr> 
                    <th width="5%" scope="row"><?php echo $no;?></th> 
                    <td><?php echo $cont;?></td> 
                    <td><?php echo $arm;?></td> 
                </tr> 
            <?php 
                    }
            ?>
        </tbody> 
    </table> 
</div>
<?php } ?>