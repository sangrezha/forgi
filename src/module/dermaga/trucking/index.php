<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'trucking','caption'=>'trucking');
/////////////////////////////////////////////////////////////////////////

function format_tgl_bahasa($date,$bahasa){
$date_tgl =explode("-", $date);

if ($bahasa == "id") {
$bulan = array (
		'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
}elseif ($bahasa == "en") {
$bulan = array (
		'January',
		'February',
		'March',
		'April',
		'May',
		'Juni',
		'June',
		'August',
		'September',
		'October',
		'November',
		'December'
	);
}elseif($bahasa == "chn"){
$bulan = array (
		'一月',
		'二月',
		'三月',
		'四月',
		'五月',
		'六月',
		'七月',
		'八月',
		'九月',
		'十月',
		'十一月',
		'十二月'
	);
}
/*$bulan = array (
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'Mei',
		'Jun',
		'Jul',
		'Agu',
		'Sep',
		'Okt',
		'Nov',
		'Des'
	);*/
// $total = $date_tgl[2]."-".$bulan[$date_tgl[1]-1]."-".$date_tgl[0];
	return $date_tgl[2]." / ".$bulan[$date_tgl[1]-1]." / ".$date_tgl[0]; 
	// return $date_tgl[2]." - ".$date_tgl[1]." - ".$date_tgl[0]; 
}
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	if (!empty($id_notif)) {
		$sqluptnot="UPDATE ".$app['table']['notif']." set status = 0 where id = '$id_notif' ";
		db::qry($sqluptnot);
	}
	$_SESSION['container'] 	= null;
	$option = ['sortable'=>true];
	// admlib::display_block_grid("","yes");
	admlib::display_block_grid("","","trucking");
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	// $total = db::lookup('COUNT(id)', 'container', '1=1');
	// $total = db::lookup('COUNT(dmg_delivery_order.id)', 'container', 'delivery_order','dmg_container.id_import = dmg_delivery_order.id_import',"dmg_delivery_order.status='active' AND dmg_container.delivery_date !='' ");
	// $total = db::lookup('COUNT(dmg_delivery_order.id)', 'container', 'delivery_order','dmg_container.id_import = dmg_delivery_order.id_import',"dmg_delivery_order.status='active'	");
	// $total = db::lookup('COUNT(dmg_delivery_order.id)', 'container', 'delivery_order','dmg_container.id_import = dmg_delivery_order.id_import',"dmg_delivery_order.status='active'	");

	// $sql_total ="select COUNT(c.id) total from ".$app['table']['container']." a
	// 			 left join ".$app['table']['delivery_order']." b ON (a.id_import = b.id_import) 
	// 			 left join ".$app['table']['pelabuhan']." c ON (a.id_import = c.id_import) where 
	// 			 b.status='active' limit 1";
	// $sql_total = "SELECT g.id id, g.status,g.num_container num_container, d.num_pib AS num_pib,/* h.date_delivery as delivery_date,*/ b.name as created_by,a.created_at,c.name as updated_by,a.updated_at,d.id id_imp, g.note note,g.address address
	// 		FROM ". $app['table']['pelabuhan'] ." a 	
	// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
	// 		LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 		left JOIN ". $app['table']['import']." d ON (a.id_import=d.id)
	// 		left JOIN ". $app['table']['delivery_order']." f ON (f.id_import=a.id_import)
	// 		LEFT JOIN ". $app['table']['container'] ." g ON (a.id_import=g.id_import)
	// 		/*LEFT JOIN ". $app['table']['trucking'] ." h ON (a.id_import= h.id_import)*/
	// 		where f.status = 'active' AND a.kelengkapan !='' $q ORDER BY a.created_at DESC";


	$sql_total = "SELECT DISTINCT g.id id, g.status,g.num_container num_container, d.num_pib AS num_pib,/* h.date_delivery as delivery_date,*/ b.name as created_by,g.print print,a.created_at,c.name as updated_by,a.updated_at,d.id id_imp, g.note note,g.address address
			FROM ". $app['table']['pelabuhan'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
			left JOIN ". $app['table']['import']." d ON (a.id_import=d.id)
			left JOIN ". $app['table']['delivery_order']." f ON (f.id_import=a.id_import)
			LEFT JOIN ". $app['table']['container'] ." g ON (a.id_import=g.id_import)
			LEFT JOIN ". $app['table']['file_kel'] ." i ON (a.id_import=i.id_import)
			/*LEFT JOIN ". $app['table']['trucking'] ." h ON (a.id_import= h.id_import)*/
			where f.status = 'active' AND i.id is not null $q ORDER BY a.created_at DESC";
	db::query($sql_total, $rs321['row'], $nr321['row']);
	$total =  $nr321['row'];
	// app::set_default($page_size, (isset($all)?$total:10));
	if (($act_search !="") && ($p_search !="" || $p_customer_s !="all" || $p_tgl_nopen_s !="")) {
		app::set_default($page_size, $total);
	}else{
		app::set_default($page_size, (isset($all)?$total:10));		
	}
	$unpost = 1;
	$unmodify = 1;
	
/*	if($ref)
	{
		$q = "AND num_pib LIKE '%". $ref ."%' OR a.delivery_date LIKE '%".date("Y-m-d",strtotime($ref))."%' OR num_container LIKE '%". $ref ."%'";
		// $q = "WHERE a.delivery_date LIKE '%". $ref ."%' ";
	}*/
	if ($act_search == "trucking") {
		// $q = "WHERE 1=1";
		if ($p_customer_s !="" && $p_customer_s !="all") {
			$q .=" AND (d.id_customer LIKE '%".$p_customer_s."%')";
		}
		if ($p_tgl_nopen_s !="" && $p_tgl_nopen_s !="all") {
 			$get_date_delivery = db::lookup("GROUP_CONCAT(id_import)","trucking", "date_delivery LIKE '%".date("Y-m-d", strtotime($p_tgl_nopen_s))."%'");
			$q .=" AND (g.id_import IN ($get_date_delivery))";
		}
		if ($p_search !="") {
			$q .=" AND (d.num_pib LIKE '%". $p_search ."%' or g.num_container LIKE '%". $p_search ."%')";
		}
	}
	// $sql = "SELECT a.id id, a.status,a.num_container num_container, d.num_pib AS num_pib, a.delivery_date as delivery_date, b.name as created_by,a.created_at,c.name as updated_by,a.updated_at,d.id id_imp,e.id id_truck
	// FROM ". $app['table']['container'] ." a 	
	// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
	// 		LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 		left JOIN ". $app['table']['import']." d ON (a.id_import=d.id)
	// 		left JOIN ". $app['table']['trucking']." e ON (e.id_container=a.id)
	// 		 $q ORDER BY a.reorder ASC";




	// $sql = "SELECT a.id id, a.status,a.num_container num_container, d.num_pib AS num_pib, a.delivery_date as delivery_date, b.name as created_by,a.created_at,c.name as updated_by,a.updated_at,d.id id_imp,e.id id_truck,e.status_kirim status_kirim,a.note note,a.address address
	// FROM ". $app['table']['container'] ." a 	
	// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
	// 		LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 		left JOIN ". $app['table']['import']." d ON (a.id_import=d.id)
	// 		left JOIN ". $app['table']['trucking']." e ON (e.id_container=a.id)
	// 		left JOIN ". $app['table']['delivery_order']." f ON (f.id_import=a.id_import)
	// 		where f.status = 'active' AND a.delivery_date !='' $q ORDER BY a.reorder ASC";
/*	 	$sql = "SELECT g.id id, g.status,g.num_container num_container, d.num_pib AS num_pib, g.delivery_date as delivery_date, b.name as created_by,a.created_at,c.name as updated_by,a.updated_at,d.id id_imp, g.note note,g.address address
			FROM ". $app['table']['pelabuhan'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
			left JOIN ". $app['table']['import']." d ON (a.id_import=d.id)
			left JOIN ". $app['table']['delivery_order']." f ON (f.id_import=a.id_import)
			LEFT JOIN ". $app['table']['container'] ." g ON (a.id_import=g.id_import)
			where f.status = 'active' AND a.kelengkapan !='' $q ORDER BY a.created_at DESC";*/
	  	$sql = "SELECT DISTINCT g.id id, g.status,g.num_container num_container, d.num_pib AS num_pib,/* h.date_delivery as delivery_date,*/ b.name as created_by,g.print print,a.created_at,c.name as updated_by,a.updated_at,d.id id_imp, g.note note,g.address address
			FROM ". $app['table']['pelabuhan'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
			left JOIN ". $app['table']['import']." d ON (a.id_import=d.id)
			left JOIN ". $app['table']['delivery_order']." f ON (f.id_import=a.id_import)
			LEFT JOIN ". $app['table']['container'] ." g ON (a.id_import=g.id_import)
			LEFT JOIN ". $app['table']['file_kel'] ." i ON (a.id_import=i.id_import)
			/*LEFT JOIN ". $app['table']['trucking'] ." h ON (a.id_import= h.id_import)*/
			where f.status = 'active' AND i.id is not null $q ORDER BY a.created_at DESC";
######## Lokasi asli page size 
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
######## 
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	// $columns = ['num_pib','num_bl','name_ship','created_at','status','created_by','step','note'];
//	$columns = ['num_pib','num_bl','name_ship','status_do','created_by','step','view'];
	// $columns = ['view','num_pib','num_container','port','delivery_date','status_return'];
	// $columns = ['view','num_pib','num_container','delivery_date','status_return','print'];
	$columns = ['jadwalkan','num_pib','num_container','delivery_date','note','address','ket_doc','status_return','ganti_status'];
	// $columns = array_merge($columns,['view']);
	$thstyle = ["status"=>"background-color:#ffdf00;color:#666;","created_by"=>"background-color:#ffdf00;color:#666;","step"=>"background-color:#ffdf00;color:#666;","status_do"=>"background-color:#ffdf00;color:#666;","note"=>"background-color:#ffdf00;width:150px;color:#666;"];
	// $total_post = 0;
	while($row = db::fetch($rs['row']))
	{
		// $file_kel = db::lookup("name","file_kel","id_import",$row['id_imp']);
		// if ( $row['status_kirim'] == 1) {
		// 	// $row['kiriman_status'] = app::getliblang("send_info");
		// 	$row['kiriman'] = "Telah Terkirim";
		// }else{
		// $row['kiriman'] = "<center><a href='".admlib::getext()."&act=kiriman&id=".$row['id']."&id_imp=".$row['id_imp']."&id_truck=".$row['id_truck']."'><i class='fa fa-paper-plane'></i></a></center>";
		// 	// $row['kiriman_status'] .= "Belum Terkirim";
		// }

		// $row['kiriman'] = "<center><a href='".admlib::getext()."&act=kiriman&id=".$row['id']."&id_imp=".$row['id_imp']."&id_truck=".$row['id_truck']."'><i class='fa fa-paper-plane'></i></a></center>";

		$datas321 = db::get_record("trucking","id_container='".$row['id']."' ");
		if ($datas321['id'] !="") {
			$row['jadwalkan']	= '<center><a href="'.admlib::getext().'&id_trucking='.$datas321['id'].'&act=jadwalkan&id='.$row['id'].'&id_imp='.$row['id_imp'].'"><i class="fa fa-eye" aria-hidden="true"></i></a></center>';
		}

		$datas = db::get_record("complaint_status","id_complaint='".$row['ncr_id']."' AND type='".$row['approve_by']."' ORDER BY created_at DESC");
		$row['step']			= app::getliblang($row['approval']);
		//$row['created_by']			= app::getliblang($row['approve_by']);
		if($row['code_cat'] == "TB" && $row['approval'] == "qc"){
			$row['step']		= app::getliblang('logistic');
		}
		if($row['code_cat'] == "TB" && $row['approve_by'] == "qc"){
			$row['created_by']	= app::getliblang('logistic');
		}
		// $row['note']			= $datas['note'];
		$row['created_at']		= app::format_datetime($row['created_at'],"ina","MMM");
		$row['status_do'] = db::lookup("status","delivery_order","id_import='".$row['id']."' ");

		$row['status_return'] = db::lookup("status","return_container","id_trucking IN ('".$datas321['id']."') AND id_container IN ('".$row['id']."')");


/*		if ($row['delivery_date']!=null&&$row['delivery_date']!="0000-00-00") {
			$row['delivery_date']= date("d-m-Y",strtotime($row['delivery_date']));
		}else{
			$row['delivery_date']="";
		}*/
		if ($datas321['date_delivery']!=null&&$row['date_delivery']!="0000-00-00") {
			$row['delivery_date']= date("d-m-Y",strtotime($datas321['date_delivery']));
		}else{
			$row['delivery_date']="";
		}
		$row['ket_doc']=$datas321['ket_doc'];
		$status_return=db::lookup("status","return_container","id_import='".$row['id']."' ");


		// $row['view']	= '<center><a title="CRM MAX" href="'.admlib::getext().'
		// &id='.$row['id_imp'].'
		// &act=view
		// &id_container321='.$row['id'].'
		// &status_truck321='.$row['status_return'].'">
		// <i class="fa fa-eye" aria-hidden="true"></i></a></center>';

		if ($row['status_return']=="proses") {
			// db::qry("UPDATE ".$app['table']['import']." set status_kiriman ='1' where id = '".$row['id_imp']."' ");
			$proses= app::getliblang('proses');
			$row['status_return']="<font color='red'>$proses</font>";
			/*$row['print']='<center><a href="'.admlib::getext().'
			&id='.$row['id_imp'].'
			&act=print&id_container='.$row['id'].'">
			<i class="fa fa-print" aria-hidden="true"></i></a></center>';*/
		}elseif ($row['status_return']=="terjadwal") {
			// db::qry("UPDATE ".$app['table']['import']." set status_kiriman ='1' where id = '".$row['id_imp']."' ");
			$proses= app::getliblang('terjadwal');
			$row['status_return']="<font color='blue'>$proses</font>";
/*			$row['print']='<center><a href="'.admlib::getext().'
			&id='.$row['id_imp'].'&act=print&id_container='.$row['id'].'">
			<i class="fa fa-print" aria-hidden="true"></i></a></center>';*/
			if ($row['print'] == 1 || $row['print'] == "1") {
				$row['ganti_status']='<center>Terkirim</center>';
			}else{
/*				$row['ganti_status']='<center><a href="'.admlib::getext().'
				&id='.$row['id_imp'].'&act=ganti_status&id_container='.$row['id'].'">
				<i class="fa fa-refresh" aria-hidden="true"></i></a></center>';	*/
				$row['ganti_status']="<center><a class='btn btn-success' href='".admlib::getext()."
				&id=".$row['id_imp']."&act=ganti_status&id_container=".$row['id']."
				'>Dikirim</a></center>";	
				//<center></center>
			}
		}elseif ($row['status_return']=="selesai") {
			db::qry("UPDATE ".$app['table']['import']." set status_kiriman ='1' where id = '".$row['id_imp']."' ");
			$proses= app::getliblang('selesai');
			$row['status_return']="<font color='green'>$proses</font>";
/*			$row['print']='<center><a href="'.admlib::getext().'
			&id='.$row['id_imp'].'&act=print&id_container='.$row['id'].' ">
			<i class="fa fa-print" aria-hidden="true"></i></a></center>';*/

			if ($row['print'] == 1 || $row['print'] == "1") {
				$row['ganti_status']='<center>Terkirim</center>';
			}else{
				// $row['ganti_status']='<center><a href="'.admlib::getext().'
				// &id='.$row['id_imp'].'&act=ganti_status&id_container='.$row['id'].' ">
				// <i class="fa fa-refresh" aria-hidden="true"></i></a></center>';
				$row['ganti_status']="<center><a class='btn btn-success' href='".admlib::getext()."
				&id=".$row['id_imp']."&act=ganti_status&id_container=".$row['id']."
				'>Dikirim</a></center>";
			}
		}else{
			$proses= app::getliblang('belum_buat');
			$row['status_return']="<font color='orange'>$proses</font>";	
		}

		// app::getliblang($row['status_do']);
		
		// if(admlib::acc('APPR')){
		// 	if($row['status_crm'] == "yes"){
		// 		$row['status_crm']	= $row['no_crmmax'];
		// 	}else{				
		// 		$row['status_crm']	= "-";
		// 	}
			
		// }
		$datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id_idv']."' and status = 'active'");
		$datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id_idv']."' ");

		// $datas['do'] = db::lookup("expired_do","expired_do","id_delivery_order='".$row['id']."' and status = 'active' ");
		// $datas['id_exp'] = db::lookup("id_delivery_order","expired_do","id_delivery_order='".$row['id']."' ");
		// $row['expired_do']= $datas['do'];

		if (strtotime($datas['do']) < time()) {
			isset($row['id_idv'])?$id_exp [] = $row['id_idv']:null;
		}else{
			isset($row['id_idv'])?$id_exp_act [] = $row['id_idv']:null;
		}
		// if ($file_kel) {
			$results[] 	= $row;	
			// $total_post++;
		// }
	}
	$id_exp = implode("','", $id_exp);
	$id_exp_act = implode("','", $id_exp_act);
	$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
	$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
	db::qry($sql321);
	db::qry($sql1231);
	// $sortable = true;
	
	$option["edit"]="no";
	$statusx = true;
	$serch_date='yes';
	include $app['pwebmin'] ."/include/blk_list_mod.php";
	exit;
endif;
/*******************************************************************************
* Action : jadwalkan
*******************************************************************************/
if ($act == "jadwalkan") {
	admlib::validate('ATV');
	form::init();
	if ($step == 1) {
		$form = db::get_record("container", "id", $id);
		$form2 = db::get_record("import", "id", $id_imp);
		// $rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['container'] = db::get_recordset("container","id_import='".$id_imp."'");
		$rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS name" ,"armada");

		$sqluptnot="UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id_imp' AND id_container = '$id' ";
		db::qry($sqluptnot);
		form::populate($form);
		// include "dsp_form_baru.php";
		include "dsp_form_view_baru.php";
		exit;
	}elseif ($step == 2) {
		form::serialize_form();
		// print_r($_SESSION['income']);
		// form::validate('empty','p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen');
		form::validate('empty','arm_trucking');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		
		app::mq_encode('p_keterangan,arm_trucking');

		// $p_date_receipt = date("Y-m-d",strtotime($p_date_receipt));		
	 	$sql = "update ". $app['table']['container'] ."
				set armada 		= '$arm_trucking',
					keterangan 	= '$p_keterangan',
					updated_by 	= '". $app['me']['id'] ."',
					updated_at 	= now()
				where id = '$id'";
		db::qry($sql);	

		$status_pengiriman = db::lookup("status","return_container","id_trucking IN ('$p_id_trucking') AND id_container IN ('$id')");
		if ($status_pengiriman != "selesai") {
		  	$sql = "update ". $app['table']['return_container'] ." set
		 			status = 'terjadwal',
					id_armada 		= '$arm_trucking',
					keterangan 	= '$p_keterangan'
					where id_trucking IN ('$p_id_trucking') AND id_container IN ('$id')";
			db::qry($sql);	
	  	}

	  	$sql = "update ". $app['table']['trucking'] ." set
				id_armada 	= '$arm_trucking',
				keterangan 	= '$p_keterangan'
				where id IN ('$p_id_trucking')";
				// where id IN ('$p_id_trucking') AND id_container IN ('$id')";
		db::qry($sql);
		$date_kirim = db::lookup("date_delivery","trucking","id",$p_id_trucking);
		
		$id_importnya =db::lookup("id_import","container","id",$id);


	  	$status_kirim_s = "";
		$asdqwezxcasd= "SELECT * FROM ".$app['table']['import']." where id = '".$id_importnya."' ";
		db::query($asdqwezxcasd, $rs11['row'], $nr11['row']);
		while($row11 = db::fetch($rs11['row']))
		{
			$cek_selesai = db::lookup("GROUP_CONCAT(stat_kir)","container", "id_import", $row11['id']);
			if (!preg_match("/0/i", $cek_selesai)) {
	  			$status_kirim_s = "semua";
			}elseif (preg_match("/1/i", $cek_selesai)) {
	  			$status_kirim_s = "sebagian";
			}else{
	  			$status_kirim_s = "sebagian";
			}
		}

		$num_container = db::lookup("num_container","container", "id", $id);
		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while ($row321	= db::fetch($rs['lang'])) {
/*			$data_positions = db::lookup("positions","status","id_import ='$id_importnya' AND lang = '".$row321['alias']."' AND id_container = '$id' ");

			db::qry("DELETE FROM ". $app['table']['status'] ." WHERE id_import ='$id_importnya' AND lang = '".$row321['alias']."' AND id_container = '$id' ");

			$id_c2 = rand(1, 100).date("dmYHis");
	 		$sqlss321 ="insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang) values
						('$id_c2','$idm','$id',concat('".app::getliblang('proses_jadwal_truck',$row321['alias'])." : <b>".format_tgl_bahasa($date_kirim,$row321['alias'])."</b>,',$data_positions),'".$app['me']['id']."',now(),'".$row['alias']."')";
			db::qry($sqlss321);*/
/*			$sqlssssss ="UPDATE ".$app['table']['status']." set 
				positions=concat('".app::getliblang('proses_jadwal_truck',$row321['alias'])." : <b>".format_tgl_bahasa($date_kirim,$row321['alias'])."</b>,',positions),
				updated_by = '". $app['me']['id'] ."',
				updated_at = now()
				WHERE id_import ='$id_importnya' AND lang = '".$row321['alias']."' AND id_container = '$id' ";*/

			$id_c321 = rand(1, 100).date("dmYHis");
 	 		$sqlss321 ="insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values
						('$id_c321','$id_importnya','$id','".app::getliblang('bar_dir',$row321['alias'])." ".app::getliblang($status_kirim_s,$row321['alias'])."','".$app['me']['id']."',now(),'".$row321['alias']."','".$num_container."')";
			// db::qry($sqlss321);
		}
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		// header("location: " .url::get_referer());
		exit;
	}

}

/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('ATV');
	form::init();
	if ($step == 1):
		form::populate($form);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
//		$validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen';
		$validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen';
		form::serialize_form();
		// form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		if(count($container)>0)
		{
			foreach($container as $row){
			$ids = rand(1, 100).date("dmYHis");
				$t_container 	= $t_container+$row['n'];
				$reorders = db::lookup("max(reorder)","container","1=1");
				if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
				$row['dd'] = date("Y-m-d",strtotime($row['dd']));
				$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at,address,delivery_date) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now(),'". $row['adr'] ."','". $row['dd'] ."')";
				db::qry($sqlx);
			}
		}
		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_date_receipt,p_amount_payment,p_nopen');
		$reorder = db::lookup("max(reorder)","import","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$p_date_receipt = date("Y-m-d",strtotime($p_date_receipt));
		$sql = "insert into ".$app['table']['import']."
				(id, num_pib, num_bl, name_ship, eta, id_customer, id_company, date_receipt, amount_payment, nopen, reorder, created_by, created_at) values
				('$id', '$p_num_pib', '$p_num_bl', '$p_name_ship', '$p_eta', '$customer_name', '$company', '$p_date_receipt', '$p_amount_payment', '$p_nopen', '$reorder', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('ATV');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
		
		$rs['container'] = db::get_record_select("num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr", "container", "id_import", $id);
		$_SESSION['container'] = [];
		while($row = db::fetch($rs['container'])){
			$_SESSION['container'][] = $row;
		}
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		// print_r($_SESSION['income']);
		form::validate('empty','p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		if(count($container)>0)
		{
			db::qry('DELETE FROM '. $app['table']['container'] .' WHERE id_import="'. $id .'"');
			foreach($container as $row){
			$ids = rand(1, 100).date("dmYHis");
				$t_container 	= $t_container+$row['n'];
				$reorders = db::lookup("max(reorder)","container","1=1");
				if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
				$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now())";
				db::qry($sqlx);
			}
		}
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);

		$p_date_receipt = date("Y-m-d",strtotime($p_date_receipt));		
		$sql = "update ". $app['table']['import'] ."
				set num_pib 		= '$p_num_pib',
					num_bl 			= '$p_num_bl',
					name_ship 		= '$p_name_ship',
					eta		 		= '$p_eta',
					id_customer 	= '$customer_name',
					id_company	 	= '$company',
					date_receipt 	= '$p_date_receipt',
					amount_payment 	= '$p_amount_payment',
					nopen		 	= '$p_nopen',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('modify'));
		// header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		header("location: " .url::get_referer());
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('ATV');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['import']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : trucking
*******************************************************************************/
if ($act == "trucking"):
admlib::validate('ATV');
	if($step == 'add')
	{
		// $nominal = str_replace('Rp ', '', $nom);
		// $nominal = str_replace(',', '', $nominal);
		$id_c  = rand(1, 100).date("dmYHis");
		$id_rc  = rand(1, 100).date("dmYHis");
		$jt = date("Y-m-d",strtotime($jt));
		
		// $sql   = "insert into ".$app['table']['trucking']." 
		// 		(id,id_import,id_container,id_armada,created_by,created_at,date_delivery,address) values
		// 		('$id_c','$pib','$cont','$arm','".$app['me']['id']."',now(),'$jt','$adr_at')";

		$sql   = "insert into ".$app['table']['trucking']." 
				  (id,id_import,id_container,id_armada,created_by,created_at) values
				  ('$id_c','$pib','$cont','$arm','".$app['me']['id']."',now())";

		$sqlss ="update ". $app['table']['armada']." set status = 'depart' WHERE id = '$arm'";
		$sqlsss ="update ". $app['table']['container']." set status = 'depart' WHERE id = '$cont'";

		// $form_user_detail = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);
		$form_cont = db::get_record("num_container","container", "id", $cont);
		$form_arm  = db::get_record("name","armada", "id", $arm);
		$form_imp  = db::get_record("num_pib","import", "id", $pib);

		$nama_cont = $form_cont['num_container'];
		$nama_arm  = $form_arm['name'];
		$nama_imp  = $form_imp['num_pib'];

		// $sqlssss ="insert into ".$app['table']['notif']." 
		// 		(id,id_rule,status,pesan,created_by,created_at) values
		// 		('$id_c','".$form_user_detail['id_rule']."',1,'Kiriman baru dengan no pib = $nama_imp, nama containernya = $nama_cont,pengemudinya = $nama_arm ','".$app['me']['id']."',now())";


// 		$sqlssss ="insert into ".$app['table']['notif']." 
// 				(id,id_rule,id_import,status,pesan,created_by,created_at) values
// 				('$id_c','5703092018105342','$pib',1,'Kiriman baru dengan no pib = $nama_imp, nama containernya = $nama_cont,pengemudinya = $nama_arm ','".$app['me']['id']."',now())";



// if ($container_status =="selesai") {
// 		$sqlssss ="UPDATE ".$app['table']['notif']." set pesan='Pesanan $nama_cont telah di ubah' where id_container = '$container_id'
// 					";
// }else{
// 		$sqlssss ="insert into ".$app['table']['notif']." 
// 				(id,id_rule,id_import,id_container,status,pesan,created_by,created_at) values
// 				('$id_c','5703092018105342','','$pib',1,'Kiriman baru dengan no pib = $nama_imp, nama containernya = $nama_cont,pengemudinya = $nama_arm ','".$app['me']['id']."',now())";
// }
		$reorders = db::lookup("max(reorder)","return_container","1=1");
		$sqlsssss="insert into ".$app['table']['return_container']."
			(id,id_import,id_container,id_armada,id_trucking,reorder,created_by,created_at) values
			('$id_rc','$pib','$cont','$arm','$id_c','$reorders','".$app['me']['id']."',now())";
			
		// $sqlssssss ="UPDATE ".$app['table']['status']." set 
		// 	positions=concat('Pesanan Dalam Proses Pengiriman,','<br>',positions),
		// 	updated_by = '". $app['me']['id'] ."',
		// 	updated_at = now()
		// 	WHERE id_import ='$pib'";

/*		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while ($row321	= db::fetch($rs['lang'])) {
		$sqlssssss ="UPDATE ".$app['table']['status']." set 
			positions=concat('".app::getliblang('proses_jadwal_truck',$row321['alias']).",','<br>',positions),
			updated_by = '". $app['me']['id'] ."',
			updated_at = now()
			WHERE id_import ='$pib' AND lang = '".$row321['alias']."' ";
			db::qry($sqlssssss);
		}*/
		db::qry($sql);
		db::qry($sqlss);
		db::qry($sqlsss);
		db::qry($sqlssss);
		db::qry($sqlsssss);

		// $req_dump = print_r($_REQUEST, TRUE);
		// $myfile = fopen("abcde.txt", "w") or die("Unable to open file!");
		// fwrite($myfile, $req_dump);
		// fclose($myfile);

		$_SESSION['trucking'][] = ['c'=>$nama_cont, 'a'=>$nama_arm, 'jt'=>$jt];
		exit;
	}elseif($step == 'delete'){
		$sqls 		= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_import` IN ('". $_SESSION['trucking'][$indx]["im"]."') AND `id` IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND `id_armada` IN ('". $_SESSION['trucking'][$indx]["idarm"]."')";
		$sqlss 		="update ". $app['table']['armada']." set status = 'stay' WHERE id = '". $_SESSION['trucking'][$indx]["idarm"]."'";
		$sqlsss 	="update ". $app['table']['container']." set status = 'stay' WHERE id = '". $_SESSION['trucking'][$indx]["ic"]."'";
		$sqlssss 	= "DELETE FROM ". $app['table']['notif'] ." WHERE `id_import` IN ('". $_SESSION['trucking'][$indx]["im"]."') AND `id` IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND `id_armada` IN ('". $_SESSION['trucking'][$indx]["idarm"]."')";

		$statusnya_adalah = db::lookup("status","return_container","id_import IN ('". $_SESSION['trucking'][$indx]["im"]."') AND id_trucking IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND id_armada IN ('". $_SESSION['trucking'][$indx]["idarm"]."')");

		// if ($statusnya_adalah!="selesai") {
			$sqlsssss 	= "DELETE FROM ". $app['table']['return_container'] ." WHERE id_trucking IN ('". $_SESSION['trucking'][$indx]['pk']."') AND id_armada IN ('". $_SESSION['trucking'][$indx]['idarm']."')";
		db::qry($sqlsssss);
// }

		// $myfile = fopen("abcde.txt", "w") or die("Unable to open file!");
		// fwrite($myfile, $sqlsssss);
		// fclose($myfile);
			db::qry($sqls);
			db::qry($sqlss);
			db::qry($sqlsss);
			db::qry($sqlssss);
		unset($_SESSION['trucking'][$indx]);
		exit;
	}
	include "dsp_list.php";
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['import']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT id,num_container as title FROM ". $app['table']['container'] ." WHERE id IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);
		$sql 	= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_container` IN ('". $delid ."')";
		$sqls 	= "DELETE FROM ". $app['table']['return_container'] ." WHERE `id_container` IN ('". $delid ."')";
		$sqlss 	="update ". $app['table']['container']." set status = 'stay' WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		db::qry($sqls);
		db::qry($sqlss);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;
/*******************************************************************************
* Action : print;
*******************************************************************************/
if ($act == "print"):
	admlib::validate('DEL');
	$form 			= db::get_record("import", "id", $id);
	$form_container = db::get_record("container", "id", $id_container);

	$date_delivery 	= db::lookup("date_delivery","trucking","id_container = '".$form_container['id']."' and id_import = '".$form['id']."' ");
	$date_delivery 	= date("d - m - Y",strtotime($date_delivery));

	$company_logo	= db::lookup("logo","company","id = '".$form['id_company']."'");
	
	$customer		= db::lookup("name","customer","id = '".$form['id_customer']."'");
	// echo "khalid ".$customer;
	$nopol 			= db::lookup("nopol","armada","id = '".$form_container['armada']."'");
	$name_armada 	= db::lookup("name","armada","id = '".$form_container['armada']."'");

	$printnya		= db::lookup("print","container","id = '".$id_container."'");
if ($printnya != 1 && $printnya != "1") {
	  	$status_kirim_s = "";
		$asdqwezxcasd= "SELECT * FROM ".$app['table']['import']." where id = '".$id."' ";
		db::query($asdqwezxcasd, $rs11['row'], $nr11['row']);
		while($row11 = db::fetch($rs11['row']))
		{
			$cek_selesai = db::lookup("GROUP_CONCAT(stat_kir)","container", "id_import", $row11['id']);
			$count_selesai = db::lookup("count(id)","container", "id_import", $row11['id']);
			if (!preg_match("/0/i", $cek_selesai)) {
	  			$status_kirim_s = "bar_dir_all";
			}elseif (preg_match("/1/i", $cek_selesai)) {
	  			$status_kirim_s = "bar_dir_se";
			}elseif($count_selesai =="1" || $count_selesai ==1){
	  			$status_kirim_s = "bar_dir_all";
			}else{
	  			$status_kirim_s = "bar_dir_se";
			}
		}
		$num_container = db::lookup("num_container","container", "id", $id_container);
		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while ($row321	= db::fetch($rs['lang'])) {
			$id_c321 = rand(1, 100).date("dmYHis");
/* 	 		$sqlss321 ="insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values
						('$id_c321','$id_importnya','$id_container','".app::getliblang('bar_dir',$row321['alias'])." ".app::getliblang($status_kirim_s,$row321['alias'])."','".$app['me']['id']."',now(),'".$row321['alias']."','".$num_container."')";*/
 	 		$sqlss321 ="insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values
						('$id_c321','$id','$id_container','".app::getliblang($status_kirim_s,$row321['alias'])."','".$app['me']['id']."',now(),'".$row321['alias']."','".$num_container."')";
			db::qry($sqlss321);
		}
				
		$num_bil = db::lookup("num_bl","import","id",$id);
		$cek_customer = db::lookup("id_customer","import","id",$id);
		$rs['devices'] = db::get_recordset("user_device","id_user='".$cek_customer."' AND status='active'");
		while($user_devices = db::fetch($rs['devices'])){
			$datapush = array(
				"to" => db::lookup("token","device","id",$user_devices['id_device']),
				"notification" => array(
					"title" => "Status Updated",
					"text" => "$num_bil : ".app::getliblang($status_kirim_s,"id")."
							   ",
					"sound" => "default"
				),
				'data' => array(
					'method' => 'change_status',
					'id' => $id,
					"title" => "Status Updated",
					"body" => "$num_bil : ".app::getliblang($status_kirim_s,"id")
				)
			);
			app::pushNotifClientSend($datapush);
		}

		db::qry("UPDATE ".$app['table']['container']." set print = 1 where id = '$id_container' ");
	}


	include 'dsp_print.php';
endif;

/*******************************************************************************
* Action : ganti_status;
*******************************************************************************/
if ($act == "ganti_status"):
	admlib::validate('DEL');
	$form 			= db::get_record("import", "id", $id);
	$form_container = db::get_record("container", "id", $id_container);

	$date_delivery 	= db::lookup("date_delivery","trucking","id_container = '".$form_container['id']."' and id_import = '".$form['id']."' ");
	$date_delivery 	= date("d - m - Y",strtotime($date_delivery));

	$company_logo	= db::lookup("logo","company","id = '".$form['id_company']."'");
	
	$customer		= db::lookup("name","customer","id = '".$form['id_customer']."'");
	// echo "khalid ".$customer;
/*	$nopol 			= db::lookup("nopol","armada","id = '".$form_container['armada']."'");
	$name_armada 	= db::lookup("name","armada","id = '".$form_container['armada']."'");*/

	$printnya		= db::lookup("print","container","id = '".$id_container."'");

	$sqluptnot="UPDATE ".$app['table']['notif']." set status = 0 where id_import = '".$form['id']."' AND id_container = '".$form_container['id']."' ";
	db::qry($sqluptnot);
if ($printnya != 1 && $printnya != "1") {
		db::qry("UPDATE ".$app['table']['container']." set print = 1 where id = '$id_container' ");
	  	$status_kirim_s = "";

		// $asdqwezxcasd= "SELECT * FROM ".$app['table']['import']." where id = '".$id."' ";
		// db::query($asdqwezxcasd, $rs11['row'], $nr11['row']);
		// while($row11 = db::fetch($rs11['row']))
		// {
			$cek_selesai = db::lookup("GROUP_CONCAT(stat_kir)","container", "id_import", $id);
			$count_selesai = db::lookup("count(id)","container", "id_import", $id);
			if (!preg_match("/0/i", $cek_selesai)) {
	  			$status_kirim_s = "bar_dir_all";
			}elseif (preg_match("/1/i", $cek_selesai)) {
	  			$status_kirim_s = "bar_dir_se";
			}elseif($count_selesai =="1" || $count_selesai ==1){
	  			$status_kirim_s = "bar_dir_all";
			}else{
	  			$status_kirim_s = "bar_dir_se";
			}
		// }
		$num_container = db::lookup("num_container","container", "id", $id_container);
		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while ($row321	= db::fetch($rs['lang'])) {
			$id_c321 = rand(1, 100).date("dmYHis");
/* 	 		$sqlss321 ="insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values
						('$id_c321','$id_importnya','$id_container','".app::getliblang('bar_dir',$row321['alias'])." ".app::getliblang($status_kirim_s,$row321['alias'])."','".$app['me']['id']."',now(),'".$row321['alias']."','".$num_container."')";*/
 	 		$sqlss321 ="insert into ".$app['table']['status']." 
						(id,id_import,id_container,positions,created_by,created_at,lang,num_container) values
						('$id_c321','$id','$id_container','".app::getliblang($status_kirim_s,$row321['alias'])."','".$app['me']['id']."',now(),'".$row321['alias']."','".$num_container."')";
			db::qry($sqlss321);
		}
				
		$num_bil = db::lookup("num_bl","import","id",$id);
		$cek_customer = db::lookup("id_customer","import","id",$id);
		$rs['devices'] = db::get_recordset("user_device","id_user='".$cek_customer."' AND status='active'");
		while($user_devices = db::fetch($rs['devices'])){
			$datapush = array(
				"to" => db::lookup("token","device","id",$user_devices['id_device']),
				"notification" => array(
					"title" => "Status Updated",
					"text" => "$num_bil : ".app::getliblang($status_kirim_s,"id")."
							   ",
					"sound" => "default"
				),
				'data' => array(
					'method' => 'change_status',
					'id' => $id,
					"title" => "Status Updated",
					"body" => "$num_bil : ".app::getliblang($status_kirim_s,"id")
				)
			);
			app::pushNotifClientSend($datapush);
		}

	}

	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	// include 'dsp_print.php';
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if($act == 'container')
{	
	admlib::validate('ATV');
	if($step == 'add')
	{
		$jenis = preg_replace('/[^A-Za-z0-9\-]/', '', $jenis);
		$reorders = db::lookup("max(reorder)","container","1=1");
		$nominal = str_replace('Rp ', '', $nom);
		$nominal = str_replace(',', '', $nominal);
		$id_c  = rand(1, 100).date("dmYHis");
		$dd = date("Y-m-d",strtotime($dd));
		if ($idm) {
			$sqlzx  = "insert into ". $app['table']['container'] ."
			(id, id_import, num_container, qty, note, value, reorder, created_by, created_at,delivery_date,address) values ('$id_c', '$idm', '$num', '$jenis', '$desc', '$nominal', '$reorders','$me',now(),'$dd','$adr')";
			db::qry($sqlzx);
		}
		$_SESSION['container'][] = ['c'=>$num, 'j'=>$jenis, 'd'=>$desc, 'n'=>$nominal, 'dd'=>$dd,'adr'=>$adr];
		exit;
	}elseif($step == 'update')
	{
		// $jenis = preg_replace('/[^A-Za-z0-9\-]/', '', $jenis);
		$reorders = db::lookup("max(reorder)","container","1=1");
		$nominal = str_replace('Rp ', '', $nil);
		$nominal = str_replace(',', '', $nominal);
		$id_c  = rand(1, 100).date("dmYHis");
		$dd = date("Y-m-d",strtotime($dd));
		$sqlsq  = "update ". $app['table']['container'] ." 
				set num_container 	= '$contain',
					note 			= '$ket',
					value 			= '$nominal',
					qty 	 		= '$qty',
					delivery_date 	= '$dd',
					address 		= '$adr',
					updated_by		= '$me',
					updated_at		= now()
					WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')
		  ";
		db::qry($sqlsq);
		$_SESSION['container'][$indx] = ['c'=>$contain, 'j'=>$qty, 'd'=>$ket, 'n'=>$nominal, 'dd'=>$dd,'adr'=>$adr ];
		exit;
	}elseif($step == 'delete'){
		$sqls 		= "DELETE FROM ". $app['table']['container'] ." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		$sqls321 	= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id_container` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		db::qry($sqls);
		db::qry($sqls321);
		unset($_SESSION['container'][$indx]);
		exit;
	}
	include "dsp_list.php";
}


/*******************************************************************************
* Action : View
*******************************************************************************/
if($act == 'view'):
	// admlib::validate('APPR');
	admlib::validate('DSPL');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		// $form = db::get_record("status", "id_container", $id);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
		$rs['container'] = db::get_recordset("container","id_import='".$id."'");
		$do = db::get_record("delivery_order", "id_import", $id);
		$rs['exp_date'] = db::get_record_select("expired_do", "expired_do", "id_delivery_order='".$do['id']."' order by reorder ASC");
		$rs['containers'] = db::get_record_select("id, CONCAT(num_container,', ',qty,', ',note,', ',DATE_FORMAT(delivery_date, '%d - %m - %Y')) AS num_container" , "container","id_import='".$id."' AND status='stay' ORDER BY reorder ASC");

		// $rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS armada" ,"armada","status='stay' ORDER BY reorder ASC");
		$rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS armada" ,"armada");

		
		$rs['container123'] = db::get_record_select("num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr", "container", "id_import", $id);

		$rs['trucking'] = db::get_record_select("a.num_container c,c a", "trucking", "id_import", $id);

		// $sqltr = "select cont.num_container c, arm.name a,tr.id_import im,tr.id_container ic,tr.id pk,tr.id_armada idarm,tr.date_delivery jt from ".$app['table']['trucking']." tr inner JOIN ".$app['table']['container']." cont on tr.id_container = cont.id inner join ".$app['table']['armada']." arm on tr.id_armada = arm.id where tr.id_import='$id'";
		$sqltr = "select cont.num_container c, arm.name a,tr.id_import im,tr.id_container ic,tr.id pk,tr.id_armada idarm,cont.delivery_date jt,cont.address adr from ".$app['table']['trucking']." tr inner JOIN ".$app['table']['container']." cont on tr.id_container = cont.id inner join ".$app['table']['armada']." arm on tr.id_armada = arm.id where tr.id_import='$id'";

		db::query($sqltr, $rs['trucking321'], $nr['trucking321']);
// if ($status_n =="inactive") {
	// $sqlupt="UPDATE ".$app['table']['notif']." set status = 0 where id = '$id_notif' ";
		
	$form_id_rule = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);
	$sqlupt		  = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '$form_id_rule' ";
	db::qry($sqlupt);
// }	
	$_SESSION['container'] = [];
		while($row = db::fetch($rs['container123'])){
			$_SESSION['container'][] = $row;
		}
		$_SESSION['trucking'] = [];
		while($row = db::fetch($rs['trucking321'])){
			$_SESSION['trucking'][] = $row;
		}
		// print_r($_SESSION['container']);
		form::populate($form);
		include "dsp_view.php";
		exit;
	endif;
if ($step == "import") {
form::serialize_form();
		// print_r($_SESSION['income']);
		form::validate('empty','p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$p_date_receipt = date("Y-m-d",strtotime($p_date_receipt));
		$sql = "update ". $app['table']['import'] ."
				set num_pib 		= '$p_num_pib',
					num_bl 			= '$p_num_bl',
					name_ship 		= '$p_name_ship',
					eta		 		= '$p_eta',
					id_customer 	= '$customer_name',
					id_company	 	= '$company',
					date_receipt 	= '$p_date_receipt',
					amount_payment 	= '$p_amount_payment',
					nopen		 	= '$p_nopen',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('modify'));
		// header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		header("location: " .url::get_referer());
		exit;
}

	if ($step == 2):
		admlib::validate('APPR');
		form::serialize_form();
		// form::validate('empty', 'p_no_do,p_date_receipt_do,p_expired_do');
		// if (form::is_error()):
			// msg::build_msg();
			// header("location: ". admlib::$page_active['module'] .".mod&act=view&error=1&id=" . $id);
			// exit;
		// endif;
		$do = db::get_record("delivery_order", "id_import", $id);
		// print_r($_POST);
		// echo "<br>";
		// echo "<br>";
		// echo count($do);
		// print_r($do);
		// exit;
		
		if($do['id_import'] == $id){
			$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$sql = "update ". $app['table']['delivery_order'] ."
					set no_do 				= '$p_no_do',
						date_receipt_do 	= '$p_date_receipt_do',
						updated_by 			= '". $app['me']['id'] ."',
						updated_at 			= now()
					where id = '".$do['id']."'";
			db::qry($sql);
			if(!empty($p_expired_do)){
				$ids = rand(1, 100).date("dmYHis");
				$sqls = "insert into ".$app['table']['expired_do']."
						(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '".$do['id']."', '$p_expired_do', '". $app['me']['id'] ."', now())";
				db::qry($sqls);
			}
		}else{
			$ids = rand(1, 100).date("dmYHis");
			$idx = rand(1, 100).date("dmYHis");
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$reorder = db::lookup("max(reorder)","delivery_order","1=1");
			if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
			$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
			$sql = "insert into ".$app['table']['delivery_order']."
					(id, id_import, no_do, date_receipt_do, id_expired_do, reorder, created_by, created_at) values
					('$ids', '$id', '$p_no_do', '$p_date_receipt_do', '$idx', '$reorder', '". $app['me']['id'] ."', now())";
			db::qry($sql);
			if(!empty($p_expired_do)){
				$sqls = "insert into ".$app['table']['expired_do']."
						(id, id_delivery_order, expired_do, created_by, created_at) values ('$idx', '$ids', '$p_expired_do', '". $app['me']['id'] ."', now())";
				db::qry($sqls);
			}
		}
		
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
if ($act == 'del_or') {
	if($step == "add"){
		form::serialize_form();
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;

		if (strtotime($p_expired_do) < time()) {
			$status_do = ",'expired'";	
		}
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);
		$id = rand(1, 100).date("dmYHis");
		$ids = rand(1, 100).date("dmYHis");
		
		$reorder = db::lookup("max(reorder)","delivery_order","1=1");
		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
		$sql = "insert into ".$app['table']['delivery_order']."
				(id, no_do, date_receipt_do, id_expired_do, reorder, created_by, created_at,id_import,status) values
				('$id', '$p_no_do', '$p_date_receipt_do', '$ids', '$reorder', '$me', now(),
				'$id_imp'$status_do)";
		$sqls = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id', '$p_expired_do', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		db::qry($sqls);
		msg::set_message('success', app::getliblang('create'));
		header("location: " .url::get_referer());
		exit;
}
	if ($step == "edit") {
		form::serialize_form();
//		form::validate('empty','p_no_do,p_date_receipt_do');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		if (strtotime($p_expired_do) > time()) {
			$status_do = ",status = 'active' ";
		}
		else{
			$status_do = ",status = 'expired' ";	
		}
		$p_date_receipt_do = date("Y-m-d",strtotime($p_date_receipt_do));
		app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
		$sql = "update ". $app['table']['delivery_order'] ."
				set no_do 				= '$p_no_do',
					date_receipt_do 	= '$p_date_receipt_do',
					updated_by 			= '". $app['me']['id'] ."',
					updated_at 			= now()
					$status_do
				where id_import = '$id_imp'";
		db::qry($sql);
		if(!empty($p_expired_do)){
		$ids = rand(1, 100).date("dmYHis");
		$sqls = "update ".$app['table']['expired_do']." set
				 status = 'inactive' where id_delivery_order = '$id_idv_ord' ";
		db::qry($sqls);
		$sqlss = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id_idv_ord', '$p_expired_do','$me',now())";
		msg::set_message('success', app::getliblang('modify'));
		header("location: " .url::get_referer());
		db::qry($sqlss);
		}
	}
}
if ($act =="kiriman") {
admlib::validate('ATV');
		$row['kiriman'] = "<center><a href='".admlib::getext()."&act=kiriman&id=".$row['id']."&id_imp=".$row['id_imp']."'><i class='fa fa-paper-plane'></i></a></center>";
	 	$sql = "update ". $app['table']['trucking'] ."
				set status_kirim 		= 1,
					updated_by 			= '". $app['me']['id'] ."',
					updated_at 			= now()
					$status_do
				where id = '$id_truck'";
		db::qry($sql);
}
?>