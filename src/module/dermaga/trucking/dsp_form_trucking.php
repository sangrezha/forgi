<?php
(isset($abc)? null:admlib::display_block_header());
admlib::get_component('texteditorlib');
admlib::get_component('uploadlib');
(isset($abc)? null:admlib::get_component('formstart'));
			// admlib::get_component('view',
			// 	array(
			// 		"name"=>"num_pib",
			// 		"value"=>app::ov($form['num_pib'])
			// 	)
			// );
	
if (!isset($abc)) {
			admlib::get_component('inputtext',
				array(
					"name"=>"num_pib",
					"value"=>app::ov($form['num_pib'])
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"pib",
					"value"=>app::ov($form['id']),
					"type"=>"hidden"
				)
			);
}
		/*	$rs['num_container'] = db::get_record_select('id, num_container name', 'container', 'status', 'stay');
			// print_r($rs['num_container']);exit;
			admlib::get_component('select2',
				array(
					"name"=>"num_container",
					"value"=>app::ov($form['num_container']),
					"items"=>$rs['num_container']
				)
			);*/
			
?>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-12 col-xs-12" for="cont_trucking">&nbsp;</label>
				<div class="col-md-5 col-sm-12 col-xs-12">
					<label class="control-label col-md-4" for="cont_trucking"><?php echo app::getliblang('num_container'); ?><span class="required"></span>		</label>
					<select name="cont_trucking" id="cont_trucking" class="form-control">
						<option value="">Select</option>
						<?php while($row = db::fetch($rs['container'])){ ?>
						<option value="<?php echo $row['id'] ?>"><?php echo $row['num_container'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<label class="control-label col-md-2" for="arm_trucking"><?php echo app::getliblang('armada'); ?><span class="required"></span>		</label>
					<select name="arm_trucking" id="arm_trucking" class="form-control">
						<option value="">Select</option>
						<?php while($rows = db::fetch($rs['armada'])){ ?>
						<option value="<?php echo $rows['id'] ?>"><?php echo $rows['armada'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-12 col-xs-12" for="add-trucking">&nbsp;</label>
				<div class="col-md-9 col-sm-12 col-xs-12">
					<a href="#" class="btn btn-default add" val="trucking"><span class="fa fa-plus" aria-hidden="true"></span> Add</a>
					<br/>
					<div id="trucking"></div>
				</div>
			</div>
<?php			
if ($act=="view") {
	echo "<input type='hidden' name='step' value='trucking'/>";
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"no_hidden" => "yes",
							"act"=>$act
					)
			);
}
else{
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
					)
			);
}
(isset($abc)? null:admlib::get_component('formend'));
(isset($abc)? null:admlib::display_block_footer());
?>
<script>
	$(function(){
		function loadData(act){
			$('#'+ act).load('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : act }, deleteData);
		}
		function addData(act, param){
			var _param = $.extend({ act : act, step : 'add' }, param);
			$.post('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){ loadData(act) });
		}
		function deleteData(){
			$('.delete').click(function(){
				var act = $(this).attr('act');
				$.get('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : act, step : 'delete', indx : $(this).attr('val') }, function(){ loadData(act) });
				return false;
			});
		}
		<?php if(count($_SESSION['trucking'])>0){ ?>
			loadData('trucking');
		<?php } ?>
		$('.add').click(function(){
			var act = $(this).attr('val'), cont = $('#cont_' + act).val(), arm = $('#arm_' + act).val();
			if(!cont || !arm){
				alert('tidah boleh kosong');
				return;
			}
			addData(act, { cont : cont, arm : arm });
			return false;
		});
	});
</script>