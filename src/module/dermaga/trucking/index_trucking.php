<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'trucking','caption'=>'Trucking');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$_SESSION['trucking'] 	= null;
	$option = ['sortable'=>true];
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'container', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
	if($ref)
	{
		$q = "WHERE num_pib	 LIKE '%". $ref ."%' OR a.delivery_date LIKE '%". $ref ."%' OR num_container LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.id, a.status,a.num_container num_container, d.num_pib AS num_pib, a.delivery_date as delivery_date, b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
	FROM ". $app['table']['container'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
			right JOIN ". $app['table']['import']." d ON (a.id_import=d.id)
			 $q ORDER BY a.reorder ASC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['num_pib','num_container','port','delivery_date'];
	while($row = db::fetch($rs['row']))
	{
		// $row['num_container'] = db::lookup("group_concat(num_container)","container","id_import='".$row['id']."'");
		$results[] 		= $row;
	}
	$option = ['sortable'=>true];
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		$validate ='p_name';
		form::serialize_form();
		form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_name');
		$reorder = db::lookup("max(reorder)","ship","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$sql = "insert into ".$app['table']['ship']."
				(id, name, reorder, created_by, created_at) values
				('$id', '$p_name', '$reorder', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		// header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		header("location: " .url::get_referer());
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$rs['container'] = db::get_record_select(" id, CONCAT(num_container,', ',qty,', ',note) AS num_container" , "container","id_import='".$id."' AND id NOT IN ('". $cont ."') AND status='stay' ORDER BY reorder ASC");
		$rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS armada" , "armada","status='stay' ORDER BY reorder ASC");
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		app::mq_encode('p_pib');
		$trucking 		= $_SESSION['trucking'];
		
		if (empty($trucking)):
			msg::set_message('error', app::getliblang('select_first'));
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		
		if(count($trucking)>0){
			foreach($trucking as $val){
			$id = rand(1, 100).date("dmYHis");
			// print_r($val);
			$sql = "insert into ".$app['table']['trucking']."
				(id, id_import, id_container, id_armada, created_by, created_at) values
				('$id', '$p_pib', '".$val['c']."', '".$val['a']."', '". $app['me']['id'] ."', now())";
			db::qry($sql);
				
			$sqls = "update ". $app['table']['container'] ."
				set status 			= 'depart',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '".$val['c']."'";
			db::qry($sqls);
				
			$sqlx = "update ". $app['table']['armada'] ."
				set status 			= 'depart',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '".$val['a']."'";
			db::qry($sqlx);
			}
		}
		msg::set_message('success', app::getliblang('update'));
		// header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		header("location: " .url::get_referer());
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
// if ($act == "status"):
	// admlib::validate('UPDT');
	// if ( $status == "active" ):
		// $statusnya = "inactive";
	// elseif ( $status == "inactive" ):
		// $statusnya = "active";
	// endif;
	// $sql = "UPDATE ".$app['table']['ship']."
			// SET status = '$statusnya'
			// WHERE id = '$id'";
	// db::qry($sql);
	// msg::set_message('success', app::getliblang('update'));
	// header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	// exit;
// endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
// if ($act == "reorder"):
	// admlib::validate('UPDT');
	// if(count($reorder) > 0)
	// {
		// foreach ($reorder as $idval) { $min++;
			// $sql = "UPDATE ".$app['table']['ship']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			// db::qry($sql);
		// }
		// echo true;
		// exit;
	// }
	// echo false;
	// exit;
// endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT id,name as title FROM ". $app['table']['trucking'] ." WHERE id IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);
		$sql 	= "DELETE FROM ". $app['table']['ship'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;

if($act == 'trucking')
{	
	if($step == 'add')
	{
		// $nominal = str_replace('Rp ', '', $nom);
		// $nominal = str_replace(',', '', $nominal);
		$id_c  = rand(1, 100).date("dmYHis");
		$sql = "insert into ".$app['table']['trucking']." 
				(id,id_import,id_container,id_armada,created_by,created_at) values
				('$id_c','$pib','$cont','$arm','$me',now())";
		db::qry($sql);
		$_SESSION['trucking'][] = ['c'=>$cont, 'a'=>$arm];
		// $myfile = fopen("abcde.txt", "w") or die("Unable to open file!");
		// fwrite($myfile, $sql);
		// fclose($myfile);
		exit;
	}elseif($step == 'delete'){
		unset($_SESSION['trucking'][$indx]);
		exit;
	}
	// $myfile = fopen("abcde.txt", "w") or die("Unable to open file!");
	// fwrite($myfile,"../trucking/dsp_list.php");
	// fclose($myfile);
	include "dsp_list.php";
	// include "../trucking/dsp_list.php";
}
?>
