<?php 
admlib::display_block_header();
admlib::get_component('texteditorlib');
admlib::get_component('datepickerlib');
admlib::get_component('select2lib');
admlib::get_component('formstart');



		admlib::get_component('view', 
					array(
						"name"=>"num_pib", 
						"value"=>app::ov($form2['num_pib'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"num_bl", 
						"value"=>app::ov($form2['num_bl'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"name_ship", 
						"value"=>app::ov($form2['name_ship'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"eta",
						"value"=>app::format_datetime($form2['eta'],"ina","MM")
					)
				);
				$cust_name=db::lookup("name","customer","id",$form2['id_customer']);
				admlib::get_component('view', 
					array(
						"name"=>"customer_name", 
						"value"=>app::ov($cust_name)
					)
				);
				$comp_name=db::lookup("name","company","id",$form2['id_company']);
				admlib::get_component('view', 
					array(
						"name"=>"company", 
						"value"=>app::ov($comp_name)
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"date_receipt", 
						"value"=>app::format_datetime($form2['created_at'],"ina","MM")
					)
				);
				// admlib::get_component('view', 
				// 	array(
				// 		"name"=>"amount_payment", 
				// 		"value"=>app::ov($form['amount_payment'])
				// 	)
				// );
				// admlib::get_component('view', 
				// 	array(
				// 		"name"=>"nopen", 
				// 		"value"=>app::format_datetime($form['nopen'],"ina","MM")
				// 	)
				// );
				admlib::get_component('view', 
					array(
						"name"=>"nopen", 
						"value"=>app::ov($form2['nopen'])
					)
				);
?>

				<div class="form-group">
				<label class="control-label col-md-3 col-sm-12 col-xs-12" for="tgl_countainer">Container :</label>
				<div class="col-md-9 col-sm-12 col-xs-12">
					<div class="table-responsive"> 
						<table class="table table-bordered"> 
							<thead> 
								<tr> 
									<th>#</th> 
									<th>Container</th> 
									<th>QTY</th> 
									<th>Note</th>
									<th>Address</th>
									<th>Delivery Date</th>
								</tr> 
							</thead> 
							<tbody> 
								<?php 
									$no = 0;
									while($row = db::fetch($rs['container'])){ $no++;
										$date_delivnya=db::lookup("date_delivery","trucking","id_import ='".$row['id_import']."' AND id_container ='".$row['id']."' ");
								?>
									<tr> 
										<th width="5%" scope="row"><?php echo $no;?></th> 
										<td><?php echo $row['num_container'];?></td> 
										<td><?php echo $row['qty'];?></td> 
										<td><?php echo $row['note'];?></td> 
										<td><?php echo $row['address']; ?></td>
										<td><?php echo date("d-m-Y", strtotime($date_delivnya)); ?></td>
									</tr> 
								<?php 
										} 
								?>
							</tbody> 
						</table> 
					</div>
				</div>
				
			</div>
			<hr>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="arm_trucking"><?php echo app::getliblang('armada'); ?><span class="required"> *</span>		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select  name="arm_trucking" id="arm_trucking" class="select form-control">
				<option value="">Select</option>
				<?php while($rows = db::fetch($rs['armada'])){ ?>
				<option value="<?php echo $rows['id'] ?>" <?php echo $rows['id']==$form['armada']?"selected":""; ?> ><?php echo $rows['name'] ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
<?php

/*admlib::get_component('select2',
	array(
		"name"=>"arm_trucking",
		"value"=>app::ov($form['armada']),
		"items"=>$rs['armada']
		// ,
		// "validate"=>"required"
	)
);*/
admlib::get_component('textarea',
	array(
		"name"=>"keterangan",
		"value"=>app::ov($form['keterangan'])
	)
);
admlib::get_component('inputtext',
	array(
		"type"=>"hidden",
		"name"=>"id_trucking",
		"value"=>app::ov($id_trucking)
	)
);
admlib::get_component('submit',
		array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
		)
);
admlib::get_component('formend');
admlib::display_block_footer();
?>