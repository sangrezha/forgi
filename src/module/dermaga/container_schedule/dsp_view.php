<?php
admlib::display_block_header();
admlib::get_component('datepickerlib');
	admlib::get_component('formstart');
			admlib::get_component('view',
				array(
					"name"=>"num_pib",
					"value"=>app::ov($form['num_pib'])
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"num_bl",
					"value"=>app::ov($form['num_bl'])
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"name_ship",
					"value"=>app::ov($form['name_ship'])
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"eta",
					"value"=>app::ov($form['eta'])
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"customer_name",
					"value"=>app::ov($form['id_customer'])
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"date_receipt",
					"value"=>app::ov($form['date_receipt'])
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"amount_payment",
					"value"=>app::ov($form['amount_payment'])
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"nopen",
					"value"=>app::ov($form['nopen'])
				)
			);
?>			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-12 col-xs-12" for="tgl_countainer">Container :</label>
				<div class="col-md-5 col-sm-12 col-xs-12">
					<div class="table-responsive"> 
						<table class="table table-bordered"> 
							<thead> 
								<tr> 
									<th>#</th> 
									<th>Container</th> 
									<th>QTY</th> 
									<th>Note</th>
								</tr> 
							</thead> 
							<tbody> 
								<?php 
									$no = 0;
									while($row = db::fetch($rs['container'])){ $no++;
										// print_r($row);     
								?>
									<tr> 
										<th width="5%" scope="row"><?php echo $no;?></th> 
										<td><?php echo $row['num_container'];?></td> 
										<td><?php echo $row['qty'];?></td> 
										<td><?php echo $row['note'];?></td> 
									</tr> 
								<?php 
										}
								?>
							</tbody> 
						</table> 
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="row text_container_schedule">
						<div class="col-md-5">
							<h4>Delivery Date</h4>
						</div>
						<div class="col-md-5">
							<h4>Delivery Address</h4>
						</div>
					</div>
					<?php for($i=1; $i <= $no; $i++){ ?>
					<div class="row container_schedule">
						<div class="col-md-5">
							<div class="control-group">
								<div class="controls">
									<input type="text" name="p_delivery_date<?php echo $i ?>" id="delivery_date<?php echo $i ?>" value="" class="form-control has-feedback-left datepickers active" validate="" rel="p_delivery_date<?php echo $i ?>">
									<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="control-group">
								<div class="controls">
									<input type="text" name="ket_expense" id="ket_expense" value="" class="form-control has-feedback-left " validate="">
									<span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true"></span>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
<?php			
			// admlib::get_component('view',
				// array(
					// "name"=>"delivery_date",
					// "value"=>($form['delivery_date'] != '0000-00-00')?app::ov($form['delivery_date']):null
				// )
			// );
			// admlib::get_component('datepicker',
				// array(
					// "name"=>"delivery_date",
					// "value"=>($form['delivery_date'] != '0000-00-00')?app::ov($form['delivery_date']):null,
					// "validate"=>"required"
				// )
			// );
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
					)
			);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
