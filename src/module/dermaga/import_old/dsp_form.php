<?php
admlib::display_block_header();
admlib::get_component('texteditorlib');
admlib::get_component('datepickerlib');
	admlib::get_component('formstart');
			admlib::get_component('inputtext',
				array(
					"name"=>"num_pib",
					"value"=>app::ov($form['num_pib']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"num_bl",
					"value"=>app::ov($form['num_bl']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"name_ship",
					"value"=>app::ov($form['name_ship']),
					"validate"=>"required"
				)
			);
			admlib::get_component('datepicker',
				array(
					"name"=>"eta",
					"value"=>app::ov($form['eta']),
					"validate"=>"required"
				)
			);
			admlib::get_component('select2',
				array(
					"name"=>"customer_name",
					"value"=>app::ov($form['id_customer']),
					"items"=>$rs['customer'],
					"validate"=>"required"
				)
			);
			admlib::get_component('select2',
				array(
					"name"=>"company",
					"value"=>app::ov($form['id_company']),
					"items"=>$rs['company'],
					"validate"=>"required"
				)
			);
			admlib::get_component('datepicker',
				array(
					"name"=>"date_receipt",
					"value"=>app::ov($form['date_receipt']),
					"validate"=>"required"
				)
			);
			// admlib::get_component('inputtext',
				// array(
					// "name"=>"amount_payment",
					// "value"=>app::ov($form['amount_payment']),
					// "validate"=>"required"
				// )
			// );
?>
			<div class="form-group" >
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount_payment">Amount Payment* :</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="p_amount_payment" id="amount_payment" value="<?php echo app::ov($form['amount_payment']) ?>" class="form-control has-feedback-left money" validate="">
					<span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
				</div>
			</div>
<?php
			admlib::get_component('inputtext',
				array(
					"name"=>"nopen",
					"value"=>app::ov($form['nopen']),
					"validate"=>"required"
				)
			);
?>
			<div class="form-group">
			<label class="control-label col-md-3 col-sm-12 col-xs-12" for="tgl_countainer">&nbsp;</label>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<label class="control-label col-md-3" for="num_container">Container<span class="required"></span>		</label>
				<input type="text" name="p_num_container" id="num_container" value="" class="form-control has-feedback-left " validate="">
				<span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<label class="control-label col-md-3" for="jenis_container">QTY<span class="required"></span>		</label>
				<select name="jenis_container" id="jenis_container" class="form-control">
					<option value="">Select</option>
					<option value="20">20'</option>
					<option value="40">40'</option>
					<option value="45">45'</option>
					<option value="LCL'">LCL</option>
				</select>
			</div>
			<div class="col-md-3 col-sm-12 col-xs-12">
				<label class="control-label col-md-3" for="ket_container">Keterangan<span class="required"></span>		</label>
				<input type="text" name="p_ket_container" id="ket_container" value="" class="form-control has-feedback-left " validate="">
				<span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<label class="control-label col-md-3" for="nilai_container">Nilai<span class="required"></span>		</label>
				<input type="text" name="p_nilai_container" id="nilai_container" value="" class="form-control has-feedback-left money" validate="">
				<span class="fa fa-money form-control-feedback left" aria-hidden="true" style="top:25px;"></span>		
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-12 col-xs-12" for="add-countainer">&nbsp;</label>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<a href="#" class="btn btn-default add" val="container"><span class="fa fa-plus" aria-hidden="true"></span> Add</a>
				<br/>
				<div id="container"></div>
			</div>
		</div>
<?php			
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
					)
			);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
<script>
	$(function(){
		function loadData(act){
			$('#'+ act).load('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : act }, deleteData);
		}
		function addData(act, param){
			var _param = $.extend({ act : act, step : 'add' }, param);
			$.post('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){ loadData(act) });
		}
		function deleteData(){
			$('.delete').click(function(){
				var act = $(this).attr('act');
				$.get('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : act, step : 'delete', indx : $(this).attr('val') }, function(){ loadData(act) });
				return false;
			});
		}
		<?php if(count($_SESSION['container'])>0){ ?>
			loadData('container');
		<?php } ?>
		$('.add').click(function(){
			var act = $(this).attr('val'), num = $('#num_' + act).val(), jenis = $('#jenis_' + act).val(), desc = $('#ket_' + act).val(), nom = $('#nilai_' + act).val();
			if(!num || !jenis || !desc || !nom){
				alert('tidah boleh kosong');
				return;
			}
			addData(act, { num : num, jenis : jenis, desc : desc, nom : nom });
			return false;
		});
		var formatCurrency = function(number){
			var n = number.split('').reverse().join("");
			var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
			return 'Rp ' + n2.split('').reverse().join('');
		};
		$( document ).ready(function() {
		$('.money').on('input', function(e){
            $(this).val(formatCurrency(this.value.replace(/[,Rp ]/g,'')));
        }).on('keypress',function(e){
            if(!$.isNumeric(String.fromCharCode(e.which))) e.preventDefault();
        }).on('paste', function(e){
            var cb = e.originalEvent.clipboardData || window.clipboardData;
            if(!$.isNumeric(cb.getData('text'))) e.preventDefault();
        });
		$('#amount_payment').val(formatCurrency($('.money').val().replace(/[,Rp ]/g,'')));
	});
	});
</script>