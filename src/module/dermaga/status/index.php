<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib', 'parse');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('module'=>'status','caption'=>'Status');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		// $option = ['sortable'=>true, 'status'=>false];
		admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'status', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = "WHERE 1=1";
	if($ref)
	{
		$q .= " AND a.title LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.*,e.num_pib num_pib,f.num_container num_container,a.id identity,b.name as postby,c.name as modifyby,d.alias as lang FROM ". $app['table']['status'] ." a
	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	LEFT JOIN ". $app['table']['import'] ." e ON (a.id_import=e.id)
	LEFT JOIN ". $app['table']['container'] ." f ON (a.id_container=f.id)
	$q ORDER BY a.lang";
	// app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".do&act=". $act);
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod&act=". $act);
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = [];
	$numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	if($numlang > 1)
	{
		$columns = ['duplicate', 'lang'];
	}
	$columns = array_merge($columns,['num_pib','num_container','positions']);
	// $columns = ['title'];
	while($row = db::fetch($rs['row']))
	{
		if(!file_exists($app['dist_path_page'] .'/'. $row['lang'] .'/'. $row['alias'] .'.html'))
		{
			//$row['build'] = '<a class="btn btn-default" href="'.   $app['webmin'] .'/'. admlib::$page_active['module'] .'.mod&act=build&lang='. $row['lang'] .'&page='. $row['identity'] .'.html">Build</a>';
		}
		if($numlang > 1)
		{
			$row['duplicate'] = '<a href="'.  $app['webmin'] .'/'. admlib::$page_active['module'] .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		}
		else 
		{
			$row['duplicate'] = null;
		}
		$results[] = $row;
	}
	$option = ['sortable'=>false, 'status'=>"salah",'bold'=>"benar"];
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');	
	form::init();
	if ($step == 1):
		form::populate($form);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
			exit;
		endif;
		list($id0, $id1) = explode(",", $import_name);
		$alias		= strtolower(str_replace(' ','-', $p_title));
		$sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
			$fname 	= $app['dist_path_page'] .'/'. $row['alias'] .'/'. $alias .'.html';
			while($row 	= db::fetch($rs['lang']))
		{
			if (!$p_lang) $p_lang=$row['id'];
			$id 	= rand(1, 100).date("dmYHis");
			$urut 	= db::lookup('max(reorder)','status', '1=1');
			if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
			$sql = "insert into ". $app['table']['status']."
					(id,id_import,id_container,positions,lang,created_by, created_at) values
					('$id', '$id0', '$id1','$qty', '".$row['id']."', '" .$app['me']['id'] ."', now())";
			db::qry($sql);
			$sql2= "update ". $app['table']['container']." set proses = 1 WHERE id = '$id1' ";
			db::qry($sql2);
		}
		msg::set_message('success', app::i18n('create'));
		header("location: " . $app['webmin'] .'/status.mod');
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	$p_alias	= app::slugify($p_title);
	if ($step == 1):
		// $form = db::get_record('status', "id", $id);
		$form = db::get_record('*,id identity,concat(id_import,",",id_container) id','status', "id", $id);
		// print_r($form);exit;
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		list($id0, $id1) = explode(",", $import_name);
		app::mq_encode('p_title,p_content');
		$sql = "update ". $app['table']['status'] ." set
				updated_by = '". $app['me']['id'] ."',
				updated_at = now(),
				id_import='$id0',
				id_container='$id1',
				positions=concat('$qty,','<br>',positions)
				WHERE id ='$id_status'";
		db::qry($sql);
		if($id1!=$id_container_val)
		{
			echo $sql2= "update ". $app['table']['container']." set proses = 0 WHERE id = '$id_container_val' ";
			db::qry($sql2);
			echo $sql3= "update ". $app['table']['container']." set proses = 1 WHERE id = '$id1' ";
			db::qry($sql3);
		}
		$p_meta_image = $p_image;
		include $app['src_path'] ."/meta.php";
		msg::set_message('success', app::i18n('modify'));
		header("location: " . $app['webmin'] .'/status.mod');
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['status']."
			SET status = '$statusnya'
			WHERE id = '$identity'";
//			WHERE identity = '$identity'";
			// WHERE id IN ('". $delid ."')";

	db::qry($sql);
	msg::set_message('success', app::i18n('modify'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : del
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
		if($id) $items = $id;
		else $items = implode("','", $p_del);
		$sql = "SELECT a.id,concat(e.num_pib,',',f.num_container) title FROM ". $app['table']['status'] ." a
					   LEFT JOIN ". $app['table']['import'] ." e ON (a.id_import=e.id)
					   LEFT JOIN ". $app['table']['container'] ." f ON (a.id_container=f.id)
				 	   WHERE a.id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		list($id0, $id1) = explode(",", $nama);
		$delid 	= implode("','", $p_id);
		$sql_unlink = "SELECT a.title,b.alias FROM ". $app['table']['status'] ." a INNER JOIN ". $app['table']['lang'] ." b ON(a.lang=b.id) WHERE a.id IN ('". $delid ."')";
		db::query($sql_unlink, $rs['row_unlink'], $nr['row_unlink']);
		while($row = db::fetch($rs['row_unlink']))
		{
			$alias	= strtolower(str_replace(' ','-', $row['title']));
			unlink($app['dist_path_page'] .'/'. $row['alias'] .'/'. $alias .'.html');
		}
		$sql 	= "DELETE FROM ". $app['table']['status'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		$sql2= "update ". $app['table']['container']." set proses = 0 WHERE id = '$id1' ";
		db::qry($sql2);
		msg::set_message('success', app::i18n('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table']['status'] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$p_alias 		= $row['alias'];
			$p_title 		= $row['title'];
			$p_thumbnail 	= $row['thumbnail'];
			$p_image 		= $row['image'];
			$p_identity 	= $row['identity'];
			$p_lang 		= $row['lang'];
			$lang 			= db::lookup('alias', 'lang', 'id');
			// $fname  		= $app['dist_path_page'] .'/'. $lang .'/'. $alias .'.html';
			// if(!file_exists($fname))
			// {
			// 	copy($app['src_path'] .'/assets/src/status_blank.html', $fname);
			// }
			$urut = db::lookup('max(reorder)', 'status', 'lang', $p_lang);
			if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
			$_id = rand(1, 100).$urut.date("dmYHis");
			app::mq_encode('p_title');
			$sql = "insert into ".$app['table']['status']."
				(id, alias, title, thumbnail, image, identity, status, reorder, created_by, created_at) values
				('$_id', '$p_alias', '$p_title', '$p_thumbnail', '$p_image', '$p_identity', 'inactive', '$urut', '". $app['me']['id'] ."', now())";
			db::qry($sql);
		}
		msg::set_message('success', app::i18n('create'));
	}
	else
	{
		msg::set_message('error', app::i18n('notfound'));
	}
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;

/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $count ++;
			$sql = "UPDATE ".$app['table']['status']." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
if($act == 'build') 
{
	$fname 	= $app['dist_path_page'] .'/'. $lang .'/'. $alias .'.html';
	if(!file_exists($fname))
	{
		$source = file_get_contents($app['src_path'] .'/assets/src/status_blank.html');
		Parse::module($source);
		Parse::widget($source);
		Parse::detail($source, $fname);
		//copy($app['src_path'] .'/assets/src/status_blank.html', $fname);
	}
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
}
?>
