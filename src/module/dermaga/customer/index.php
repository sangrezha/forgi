<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'customer','caption'=>'Customer');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$option = ['sortable'=>true];
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'customer', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.id, a.status, a.name AS name, b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
	FROM ". $app['table']['customer'] ." a
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id) $q ORDER BY a.reorder ASC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['name','forget_pass'];

#	$abcdef="<center><a class='btn btn-success' href='".admlib::getext()."&act=ganti_pass&id=".$row['id']."&num_pib=".$row['num_pib']."&status=".$row['sb']."'>Ubah ke SPPB</a></center>";
	while($row = db::fetch($rs['row']))
	{
		$row['forget_pass'] = "<center><a class='btn btn-success' href='".admlib::getext()."&act=ganti_pass&id=".$row['id']."'>Ganti Password</a></center>";
		$results[] 		= $row;
	}
	$option = ['sortable'=>true];
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		$validate ='p_company,p_address,p_name_pic,p_phone,p_password,p_username';
		form::serialize_form();
		form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_company,p_address,p_name_pic,p_phone,p_password,p_username');
		$reorder = db::lookup("max(reorder)","customer","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$p_password = md5(serialize($p_password));
 		$sql = "insert into ".$app['table']['customer']."
				(id, name, address, name_pic, phone, reorder, created_by, created_at, username, password) values
				('$id', '$p_company', '$p_address', '$p_name_pic', '$p_phone', '$reorder', '". $app['me']['id'] ."', now(), '$p_username', '$p_password')";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : ganti_pass
*******************************************************************************/
if ($act == "ganti_pass"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		// $validate ='p_company,p_address,p_name_pic,p_phone';
		$validate ='p_password';
		form::serialize_form();
		form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		// $id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_password');
		$reorder = db::lookup("max(reorder)","customer","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$p_password = md5(serialize($p_password));
	 	$sql = "update ". $app['table']['customer'] ."
				set password 	= '$p_password'
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record_filter("id, name, address, name_pic, phone, username, password ","customer", "id", $id);
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		form::validate('empty','p_company,p_address,p_name_pic,p_phone');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		app::mq_encode('p_company,p_address,p_name_pic,p_phone');
		$p_password = md5(serialize($p_password));
		$sql = "update ". $app['table']['customer'] ."
				set name 		= '$p_company',
					address 	= '$p_address',
					name_pic 	= '$p_name_pic',
					username 	= '$p_username',
					password 	= '$p_password',
					phone	 	= '$p_phone',
					updated_by 	= '". $app['me']['id'] ."',
					updated_at 	= now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['customer']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['customer']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT id,name as title FROM ". $app['table']['customer'] ." WHERE id IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql 	= "DELETE FROM ". $app['table']['customer'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;
?>
