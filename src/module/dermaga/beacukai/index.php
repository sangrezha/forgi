<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'beacukai','caption'=>'beacukai');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	// $option = ['sortable'=>true];
	// admlib::display_block_grid();
	admlib::display_block_grid("","","beacukai");
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	// $total = db::lookup('COUNT(id)', 'beacukai', '1=1');
 	$sql = "SELECT a.id, a.status, a.num_pib AS num_pib,  b.name as created_by,a.created_at,a.updated_at,a.status_beacukai sb
			FROM ". $app['table']['import'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  $q ORDER BY a.created_at DESC";
	db::query($sql, $rs['row'], $nr['row']);
	$total=$nr['row'];
	// app::set_default($page_size, (isset($all)?$total:10));
	// app::set_default($page_size, (isset($all)?$total:10));
	if (($act_search !="") && ($p_search !="" || $p_stat_bea !="all" || $p_customer_s !="all")) {
		app::set_default($page_size, $total);
	}else{
		app::set_default($page_size, (isset($all)?$total:10));		
	}
	$q = null;
/*	if($ref)
	{
		#$q = "where a.name LIKE '%". $ref ."%'";
		$q = "where a.num_pib LIKE '%". $ref ."%' OR a.status_beacukai LIKE '%".$ref."%' OR a.tgl_nopen LIKE '%".date("Y-m-d",strtotime($ref))."%' ";
	}*/
	if ($act_search == "beacukai") {
		$q = "WHERE 1=1";
		if ($p_search !="") {
			$q .=" AND (a.num_pib LIKE '%". $p_search ."%')";
		}
		if ($p_stat_bea !="" && $p_stat_bea !="all" ) {
			$q .=" AND (a.status_beacukai LIKE '%".strtoupper($p_stat_bea)."%')";
		}
		if ($p_customer_s !="" && $p_customer_s !="all") {
			$q .=" AND (a.id_customer LIKE '%".$p_customer_s."%')";
		}
	}
/*	$sql = "SELECT a.id, a.status, a.num_pib AS num_pib,  b.name as created_by,a.created_at,c.name as updated_by,a.updated_at,a.status_beacukai sb,d.kirim kirim
			FROM ". $app['table']['import'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)  
			LEFT JOIN ". $app['table']['beacukai'] ." d ON (a.id=d.id_import)  
			where (a.status_beacukai IN ('SPJH','SPJK') AND a.status_sppb = '0') $q ORDER BY a.created_at DESC";*/
/* 	$sql = "SELECT a.id, a.status, a.num_pib AS num_pib,  b.name as created_by,a.created_at,a.updated_at,a.status_beacukai sb
			FROM ". $app['table']['import'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  
			where a.status_beacukai IN ('SPJH','SPJK') $q ORDER BY a.created_at DESC";*/
 	$sql = "SELECT a.id, a.status, a.num_pib AS num_pib,b.name as created_by,a.created_at,a.updated_at,a.status_beacukai sb
			FROM ". $app['table']['import'] ." a 	
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)  
			 $q ORDER BY a.created_at DESC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	// $total =$nr['row'];
	$results = [];
	// $columns = ['view','send_email','status_beacukai','num_pib','num_bl'];
	$columns = ['status_beacukai','num_pib'];
	$abc="";
	while($row = db::fetch($rs['row']))
	{
/*		if ($row['kirim']==1) {
			$row["editx"] ="yes";
		}else{
			$row["editx"] =null;
		}*/
/*		if ($row['kirim']==2 OR $row['kirim']==1) {
		// $row['send_email'] = "<center><a href='".admlib::getext()."&act=send_email_beacukai&id=".$row['id']."&num_pib=".$row['num_pib']."&status=".$row['sb']."'><i class='fa fa-paper-plane'></i></a></center>";
		$row['send_email'] = "<center><i class='fa fa-paper-plane'></i></center>";
			// $row['send_email'] = "<center><button onclick='tombolKirimBeacukai('".$row['id']."','".$row['sb']."','".$row['num_pib']."')'>
			// <i class='fa fa-paper-plane'></i></button></center>";
		}*/
		if ($row['sb'] == "SPJH") {
			$row['sb'] = "<font color='green'>".$row['sb']."</font>";
		}elseif($row['sb'] == "SPJK"){
			$row['sb'] = "<font color='orange'>".$row['sb']."</font>";
		}else{
			$row['sb'] = "<font color='red'>".$row['sb']."</font>";
		}
		$row['status_beacukai'] =$row['sb'];
		// $row['view']	= '<center><a title="CRM MAX" href="'.admlib::getext().'&act=view&id='.$row['id'].'"><i class="fa fa-eye" aria-hidden="true"></i></a></center>';
		/*$cek_beacukai = db::lookup("id","beacukai","id_import",$row['id']);
		if ($cek_beacukai!="") {
			$total = $total -1;
			$nr['row'] =  $nr['row'] -1;
		}else{*/
			$results[] = $row;
		// }
	}
	// echo $abc;
	// $option = ['sortable'=>true];
	$statusx ="no";
	include $app['pwebmin'] ."/include/blk_list_mod.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):

		form::serialize_form();

		$place_room = "/sppb/";
		admlib::updatemultipleimage($p_dokumen_mod,$place_room,"name","file_bea","id_import",$id);
		$image_room = array();
		for($a=0; $a<=count($_FILES['p_dokumen']['size']);$a++):
			if (isset($_FILES['p_dokumen']['size'][$a]) && $_FILES['p_dokumen']['size'][$a] > 0):
				$image_room[] = file::save_picture_multiple_rand_name('p_dokumen', $app['doc_path'] ."/sppb/",'photo', $a);
			endif;		
		endfor;	
		if(count($image_room) > 0){
			for($i=0;$i<count($image_room);$i++){
				$idroom	= rand(1, 100).$i.rand(1, 100).date("dmYHis");
				$sql_room = "insert into ".$app['table']['file_bea']."
						(id, id_import, name) values
						('$idroom', '$id', '".$image_room[$i]."')";
				db::qry($sql_room);
			}
		}

/*		form::validate('empty','p_dokumen');
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;*/
		// print_r($_REQUEST);echo "<br><br><br>";print_r($_FILES[$p_dokumen]['tmp_name']);echo "<br><br>";
		// $dokumen = null;
		// if($p_dokumen_size > 0){

		// 	// $dokumen = file::save_file_orig('p_dokumen',$app['doc_path']."/sppb");
		// 	$dokumen = $_FILES["p_dokumen"]["name"];

 	// 		if (!@copy($_FILES["p_dokumen"]['tmp_name'], $app['doc_path']."/sppb/".$dokumen))
		// 	header("location: error.do&ref=411");
		// 	// $dokumen = file::save_file_orig($p_dokumen,$app['doc_path']."/sppb");
		// }
		// print_r($_REQUEST);
		// echo "<br><br>";
		// print_r($dokumen);exit;
		$ids = rand(1, 100).date("dmYHis");
		$id_pel = rand(1, 100).date("dmYHis");
		app::mq_encode('p_dokumen');
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$sql = "insert into ".$app['table']['beacukai']."
				(id, id_import,revisi ,sppb, created_by, created_at,kirim) values
				('$ids', '$id',0,'$dokumen','". $app['me']['id'] ."', now(),2)";
		db::qry($sql);
		if ($p_status_beacukai_orig != "SPJM") {
			$cek_pelabuhan = db::lookup("id","pelabuhan","id_import",$id);
			if (!$cek_pelabuhan) {
				$sql2 = "insert into ".$app['table']['pelabuhan']."
						(id, created_by, created_at, id_import, eir) values
						('$id_pel', '". $app['me']['id'] ."', now(), '$id', '$dokumen')";
				db::qry($sql2);
			}
		}
		$form_import = db::get_record("import", "id", $id);
		
		// $sql_lang 	= "SELECT id,alias FROM ". $app['table']['lang'] ." WHERE status='active'";
		// db::query($sql_lang, $rs['lang'], $nr['lang']);
		// while ($row321	= db::fetch($rs['lang'])) {
		// 	$sqls ="UPDATE ".$app['table']['status']." set 
		// 			positions=concat('".app::getliblang('proses_urus_beacukai',$row321['alias']).",','<br>',positions),
		// 			updated_by = '". $app['me']['id'] ."',
		// 			updated_at = now()
		// 			WHERE id_import ='$p_pib' AND lang = '".$row321['alias']."' ";
		// 		db::qry($sql);	
		// }
		$message  .= "<p>Kiriman dengan pib : ".$form_import['num_pib']." Bisa di buatkan delivery ordernya </p>";
		// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='5403092018114622' ";

/*		$rs['devices'] = db::get_recordset("user_devices","category='pelabuhan' AND status='active'");
		while($user_devices = db::fetch($rs['devices'])){
			$datapush = array(
				// "to" => db::lookup("token","device","id",$user_devices['id_device']),
				"to" => db::lookup("token","device","id",$user_devices['id_device']),
				"notification" => array(
					"title" => "Admin Pelabuhan/eir",
					"text" => "$p_num_pib : Status SPPB Telah Dirubah",
					"sound" => "default"
				),
				'data' => array(
					'method' => 'pelabuhan',
					'ncr_id' => $id,
					"title" => "Admin Pelabuhan/eir",
					"body" => "$p_num_pib : Status SPPB Telah Dirubah"
				)
			);
			app::pushNotifSend($datapush);
		}*/
					   
		$path_do = $app['doc_path']."/sppb/".$dokumen;

		db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
		while($row = db::fetch($rs['sql_bea_cukai'])){
			// app::sendmail($row['email'], "Ada dokumen sppb baru", $message, $path_do);
			app::sendmail($row['email'], "Kiriman dengan pib : ".$form_import['num_pib']." Bisa dibuatkan do ", $message, $path_do);
		}

		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$form2 = db::get_record("beacukai", "id_import", $id);
		if ($form2['id_import'] =="") {
			$act="add";
		}
		$form_id_rule = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);
		$sqlupt		  = "UPDATE ".$app['table']['notif']." set status = 0 where id_import = '$id' AND id_rule = '".$form_id_rule['id_rule']."' ";
		db::qry($sqlupt);

		$image_slide = db::get_record_filter("GROUP_CONCAT(name SEPARATOR '|') AS gambar_bea,GROUP_CONCAT(id SEPARATOR '|') AS id_gambar","file_bea", "id_import='".$id."'");
		$form['id_gambar'] 			= $image_slide['id_gambar'];
		$form['gambar_bea'] 		= strip_tags($image_slide['gambar_bea']);

		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):

		form::serialize_form();
		// form::validate('empty','p_dokumen');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;

		$data = db::get_record("beacukai", "id_import", $id);
		// if ($p_dokumen_del){

		// 	// if (@unlink($app['doc_path']."/sppb/". $data['sppb'])) {
		// 	/*		$data_sppb 	 = "sppb	 	= '',";
		// 			$data_kirim	 = "kirim	 	= '0',";

		// 			$sql_sppb 	 = "update ". $app['table']['import'] ."
		// 							set status_sppb = '0',
		// 							updated_by 	 = '". $app['me']['id'] ."',
		// 							updated_at 	 = now()
		// 							where id 		 = '$id'";
		// 			db::qry($sql_sppb);*/
		// 	// }
		// 	// @unlink($app['doc_path']."/sppb/". $data['sppb']);		
		// }else{
		// 	if ($p_dokumen_size > 0):
		// 		@unlink($app['doc_path']."/sppb/". $data['sppb']);	
		// 		// $file = file::save_file_orig('p_dokumen', $app['doc_path']."/sppb");
		// 		// if (!$err_dokumen_web):
		// 			// @unlink($app['doc_path']."/sppb". $data['sppb']);	
		// 			// $data['sppb'] = $file;
		// 		// endif;
		// 		$dokumen = $_FILES["p_dokumen"]["name"];
		// 		if (!@copy($_FILES["p_dokumen"]['tmp_name'], $app['doc_path']."/sppb/".$dokumen))
		// 			header("location: error.do&ref=411");
		// 			$data_sppb	  = "sppb	 	= '$dokumen',";
		// 			$data_eir 	  = "eir	 	= '$dokumen',";
		// 			$data_kirim	  = "kirim	 	= '2',";
		// 		endif;	
		// }
	
		
		$place_room = "/sppb/";
		admlib::updatemultipleimage($p_dokumen_mod,$place_room,"name","file_bea","id_import",$id);
		$image_room = array();
		for($a=0; $a<=count($_FILES['p_dokumen']['size']);$a++):
			if (isset($_FILES['p_dokumen']['size'][$a]) && $_FILES['p_dokumen']['size'][$a] > 0):
				$image_room[] = file::save_picture_multiple_rand_name('p_dokumen', $app['doc_path'] ."/sppb/",'photo', $a);
			endif;		
		endfor;	
		if(count($image_room) > 0){
			for($i=0;$i<count($image_room);$i++){
				$idroom	= rand(1, 100).$i.rand(1, 100).date("dmYHis");
			 	$sql_room = "insert into ".$app['table']['file_bea']."
						(id, id_import, name) values
						('$idroom', '$id', '".$image_room[$i]."')";
				db::qry($sql_room);
			}
		}


		app::mq_encode('p_dokumen');
/* 		$sql = "update ". $app['table']['beacukai'] ."
				set $data_sppb
					$data_kirim
					updated_by 	= '". $app['me']['id'] ."',
					updated_at 	= now()
				where id_import = '$id'";*/
 		$sql = "update ". $app['table']['beacukai'] ."
				set revisi = 0,
					updated_by 	= '". $app['me']['id'] ."',
					updated_at 	= now()
				where id_import = '$id'";
		db::qry($sql);

		if ($p_status_beacukai_orig != "SPJM") {
	 		$sql = "update ". $app['table']['pelabuhan'] ."
					set	updated_by 	= '". $app['me']['id'] ."',
						updated_at 	= now()
					where id_import = '$id'";
			db::qry($sql);
		}
		$form_import = db::get_record("import", "id", $id);
		
		$cek_revisi = db::lookup("revisi","beacukai","id_import",$id_imp);
		if ($cek_revisi==1) {

		$rs['devices'] = db::get_recordset("user_device","category='pelabuhan' AND status='active'");
			while($user_devices = db::fetch($rs['devices'])){
				$datapush = array(
					// "to" => db::lookup("token","device","id",$user_devices['id_device']),
					"to" => db::lookup("token","device","id",$user_devices['id_device']),
					"notification" => array(
						"title" => "Admin Pelabuhan/EIR",
						"text" => $form_imp['num_pib']." : Telah Direvisi Oleh Admin Beacukai",
						"sound" => "default"
					),
					'data' => array(
						'method' => 'pelabuhan',
						'ncr_id' => $form_imp['id'],
						"title" => "Admin Pelabuhan/EIR",
						"body" => $form_imp['num_pib']." : Telah Direvisi Oleh Admin Beacukai"
					)	
				);
				app::pushNotifSend($datapush);
			}
				$id_notif   = rand(1, 100).date("dmYHis");
				$sql_notif = "insert into ".$app['table']['notif']." 
						(id,id_rule,id_import,status,pesan,created_by,created_at) values
						('$id_notif','5403092018114622','".$form_imp['id']."',1,'".$form_imp['num_pib']."  : Telah Direvisi Oleh Admin Beacukai','".$app['me']['id']."',now())";
				db::qry($sql_notif);
		}
		$message  .= "<p>Kiriman dengan pib : ".$form_import['num_pib']." Bisa di buatkan delivery ordernya </p>";
		// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='5403092018114622' ";
		$path_do = $app['doc_path']."/sppb/".$file;

		db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
		while($row = db::fetch($rs['sql_bea_cukai'])){
			app::sendmail($row['email'], "Perubahan dokumen sppb", $message, $path_do);
		}
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['beacukai']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['beacukai']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
		 	$sql = "SELECT a.id_import id,b.num_pib as title FROM ". $app['table']['beacukai'] ." a
					LEFT JOIN dmg_import b ON (a.id_import = b.id) 
					WHERE id_import IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		 $sql 	= "DELETE FROM ". $app['table']['beacukai'] ." WHERE `id_import` IN ('". $delid ."')";
		db::qry($sql);
		if ($p_status_beacukai_orig != "SPJM") {
		 	$sql 	= "DELETE FROM ". $app['table']['pelabuhan'] ." WHERE `id_import` IN ('". $delid ."')";	
		 }			
		db::qry($sql);
		$sql 	= "update ". $app['table']['import'] ."
					set status_sppb = '0',
					updated_by 	 = '". $app['me']['id'] ."',
					updated_at 	 = now()
					WHERE id IN ('". $delid ."')";
		db::qry($sql);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;
if ($act=="send_email_beacukai") {
	$id_notif = rand(1, 100).date("dmYHis");
	$sql1231 = "UPDATE ".$app['table']['beacukai']." set kirim = '1' where id_import='$id' ";
	db::qry($sql1231);
	$sqls3 = "insert into ".$app['table']['notif']." 
				  (id,id_rule,id_import,status,pesan,created_by,created_at) values
				  ('$id_notif','5403092018114622','$id',1,'Kiriman baru dengan no pib = $num_pib','".$app['me']['id']."',now())";
	db::qry($sqls3);
	$rs['devices'] = db::get_recordset("user_devices","category='pelabuhan' AND status='active'");
	while($user_devices = db::fetch($rs['devices'])){
		$datapush = array(
			"to" => db::lookup("token","device","id",$user_devices['id_device']),
			"notification" => array(
				"title" => "Admin Pelabuhan/eir",
				"text" => "$p_num_pib : Status SPPB Telah Dirubah",
				"sound" => "default"
			),
			'data' => array(
				'method' => 'pelabuhan',
				'ncr_id' => $id,
				"title" => "Admin Pelabuhan/eir",
				"body" => "$p_num_pib : Status SPPB Telah Dirubah"
			)
		);
		app::pushNotifSend($datapush);
	}
		$form_import = db::get_record("import", "id", $id);
		$message  .= "<p>Dokumen SPPB dengan pib : ".$form_import['num_pib']." </p>";
		// $message .= "<p><a href='". $app['http'] ."/import.mod&act=view&id=".$id."&id_unit_kerja=". $id_unit_kerja ."'>Setujui Pelatihan</a></p>";
		$message .= "<p>Terima kasih</p>";
		$sql_bea_cukai = "select c.email from ". $app['table']['user_det'] ." a 
					   inner join ". $app['table']['user'] ." c on(a.id_user=c.id) 
					   where a.id_rule ='5403092018114622' ";
		$path_do = $app['doc_path']."/sppb/".$file;

		db::query($sql_bea_cukai, $rs['sql_bea_cukai'], $nr['sql_bea_cukai']);
		while($row = db::fetch($rs['sql_bea_cukai'])){
			app::sendmail($row['email'], "Perubahan dokumen sppb", $message, $path_do);
		}
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
}

if ($act=="download_jpeg") {
	// $ch = curl_init('http://example.com/image.php');
	// $fp = fopen('/my/folder/flower.gif', 'wb');
	// curl_setopt($ch, CURLOPT_FILE, $fp);
	// curl_setopt($ch, CURLOPT_HEADER, 0);
	// curl_exec($ch);
	// curl_close($ch);
	// fclose($fp);
// print_r($app['httpbase'].$path);
// 	exit;
#	$file = "template-user.xlsx";
	// echo "khalid ".file_exists($app['doc_path'].'/sppb/'.$path);
	// exit;
	if (file_exists($app['doc_path'].'/sppb/'.$path)) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($app['doc_path'].'/sppb/'.$path).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($app['doc_path'].'/sppb/'.$path));
		readfile($path);

	}
		#file_put_contents($img, file_get_contents($url));
}
?>
