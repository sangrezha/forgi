<?php
admlib::display_block_header();
admlib::get_component('texteditorlib');
admlib::get_component('uploadlib');
	admlib::get_component('formstart');
			
		admlib::get_component('view', 
					array(
						"name"=>"num_pib", 
						"value"=>app::ov($form['num_pib'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"num_bl", 
						"value"=>app::ov($form['num_bl'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"name_ship", 
						"value"=>app::ov($form['name_ship'])
					)
				);
/*				admlib::get_component('view', 
					array(
						"name"=>"eta",
						"value"=>app::format_datetime(date("d-m-Y",strtotime($form['eta'])),"ina","MM")
					)
				);*/
				admlib::get_component('view', 
					array(
						"name"=>"eta",
						"value"=>app::format_date(date("d-m-Y",strtotime($form['eta'])),"id","MM")
					)
				);
				// admlib::get_component('view', 
				// 	array(
				// 		"name"=>"eta",
				// 		"value"=>app::format_datetime($form['eta'],"ina","MM")
				// 	)
				// );
				$form['id_customer'] = db::lookup("name","customer","id",$form['id_customer']);
				admlib::get_component('view', 
					array(
						"name"=>"customer_name", 
						"value"=>app::ov($form['id_customer'])
					)
				);
				$form['id_company'] = db::lookup("name","company","id",$form['id_company']);
				admlib::get_component('view', 
					array(
						"name"=>"company", 
						"value"=>app::ov($form['id_company'])
					)
				);
/*				admlib::get_component('view', 
					array(
						"name"=>"date_receipt", 
						"value"=>app::format_datetime(date("d-m-Y",strtotime($form['date_receipt'])),"ina","MM")
					)
				);*/
				admlib::get_component('view', 
					array(
						"name"=>"date_receipt", 
						"value"=>app::format_date(date("d-m-Y",strtotime($form['created_at'])),"id","MM")
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"pfpd", 
						"value"=>app::ov($form['pfpd'])
					)
				);
				//app::format_datetime($form['nopen'],"ina","MM")
				admlib::get_component('view', 
					array(
						"name"=>"nopen", 
						"value"=>app::ov($form['nopen'])
					)
				);
				admlib::get_component('view', 
					array(
						"name"=>"status_beacukai", 
						"value"=>$form['status_beacukai']
					)
				);
				admlib::get_component('inputtext', 
					array(
						"type"=>"hidden",
						"name"=>"status_beacukai_orig", 
						"value"=>$form['status_beacukai']
					)
				);
				//contoh ada di residence citragarden
				admlib::get_component('inputfilemulti2',
					array(
						"name"=>"dokumen",
						"value"=> (isset($form['gambar_bea'])?$form['gambar_bea']:null),
						"accept"=>"jpeg|jpg|png",
                		"path"=>$app['doc_http']."/sppb/",
						// "filemedia"=>true
					)
				);
// '.admlib::getext().'&act=view&id='.$row['id'].'
			#echo '<div class="col-md-12 offsite"><div class="col-md-3"></div><div class="input-group col-md-9"><label for="surat_pendukung1">Download : </label><a href="'.$app['doc_www'] .'/sppb/'.$form2['sppb'].'">'.$form2['sppb'].'</a></div>';
		#	echo '<div class="form-group"><div class="col-md-6 col-sm-6 col-xs-12"><label for="surat_pendukung1">Download : </label><a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$app['doc_www'] .'/sppb/'.$form2['sppb'].'">'.$form2['sppb'].'</a></div>';
?>
<div class="form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Download : </label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<?php 
#		echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$app['doc_www'] .'/sppb/'.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 
		#echo '<a href="'.admlib::getext().'&act=download_jpeg&id='.$row['id'].'&path='.$form2['sppb'].'">'.$form2['sppb'].'</a>'; 

			$sql321="SELECT * FROM ".$app['table']['file_bea']." WHERE id_import ='".$id."' ";
			db::query($sql321, $rs['row'], $nr['row']);
			while ($row = db::fetch($rs['row'])) { 
				echo '<a href="'.$app['doc_http'].'/sppb/'.$row['name'].'" download>'.$row['name'].'</a><br>'; 
			}
		?>
	</div>
</div>
<?php
				admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
					)
			);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
