<!DOCTYPE html>
<html>
<head>
	<title>FORGI - Digital Document View</title>
</head>
<body>
	<style type="text/css">
	body{
		height: 100%;
		margin: 0;
		font-family: sans-serif;
	}
	.frame{
	  width: 100%; height: 100%;position: absolute; top: 0; left: 0;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;

	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>
    <!-- <link href="excel/jexcel.css" rel="stylesheet" type="text/css"> -->
	<?php
	// header("Content-type: application/vnd-ms-excel");
	// header("Content-Disposition: attachment; filename=Report_excel_".(empty($start_date)?"":(empty($end_date)?$start_date:$start_date." sampai ".$end_date)).".xls");
	?>

<!-- <iframe src="http://docs.google.com/gview?url=http://remote.url.tld/path/to/document.doc&embedded=true"></iframe> -->
<div class="frame">
<iframe style="width: 100%;height: 100%;" src="http://docs.google.com/gview?url=<?= $app['http'] ?>/src/assets/<?= $module ?>/<?= $data['path'] ?>&embedded=true"></iframe>
</div>
<!-- <script src="excel/jexcel.js"></script> -->
</body>
</html>