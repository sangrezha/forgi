<?php
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('module'=>'form_pengajuan_user','caption'=>ucwords('Pengajuan user komputer'));
$p_masa_simpan = 0;


   $ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
else
    $ipaddress = 'UNKNOWN';
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$module = admlib::$page_active['module'];
/*	$total 	= db::lookup('COUNT(id)', $module, '1=1');
	app::set_default($page_size, (isset($all)?$total:10));*/
	if($app['me']['level']==1){
		if($app['me']['code']!="it" && $app['me']['code']!="hrd"){
			$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.id ='".$app['me']['id']."'";
		}elseif($app['me']['code']=="hrd"){
           /* $cek_gm   = db::lookup("id","title","id_section='".$app['me']['id_section']."' AND level='4'");
            if ($cek_gm) {*/
				$where = " AND a.approve_4 ='iya'";
    //         }else{
				// $where = " AND a.approve_3 ='iya'";
    //         }
		}else{
			$where = " AND a.status_progress NOT LIKE 'unverified' AND a.status_progress NOT LIKE 'partly'";
		}
	}
	if($app['me']['level']==2){
		$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.level <='2'";
	}
	if($app['me']['level']>2){
	   if(($app['me']['code']!="hrd" && $app['me']['code']!="it") && $app['me']['level']=="3"){
			// $where = " AND b.id_section ='".$app['me']['id_section']."'";
            $cek_spv   = db::lookup("id","title","id_section='".$app['me']['id_section']."' AND level='2'");
            if ($cek_spv) {
            	$where = " AND b.id_section ='".$app['me']['id_section']."' AND a.approve_2 != 'tidak'";
            }else{
            	$where = " AND b.id_section ='".$app['me']['id_section']."'";
            }
		}elseif( $app['me']['level'] == 4){
           	$where = " AND b.id_section ='".$app['me']['id_section']."' AND a.approve_3 != 'tidak'";
		}elseif( $app['me']['code']=="it"){
			// $where = " AND b.id_section ='".$app['me']['id_section']."'";
			$where = " AND a.approve_it !='tidak'";
		}
	}
	if(($app['me']['level']==2 || $app['me']['level']==3 )&& $app['me']['code']=="hrd"){
		$where = " AND a.approve_hrd !='tidak'";
	}
	$q = "WHERE 1=1";
	if($ref)
	{
		#$q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		$q .= " AND (a.no_pengajuan LIKE '%". $ref ."%' OR  (b.name LIKE '%". $ref ."%' OR  b.nik LIKE '%". $ref ."%') OR a.status_progress LIKE '%". $ref ."%')";
	}
	// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$module] ." a
	// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	// 	$q ORDER BY a.lang,a.reorder";
	$sql = "SELECT a.*, a.approve_2 approve_2_status, a.approve_3 approve_3_status, a.approve_4 approve_4_status, a.approve_hrd approve_hrd_status, a.approve_hrd_3 approve_hrd_3_status, a.created_by id_created_by, b.name as created_by, c.name as modifyby FROM ". $app['table'][$module] ." a
	LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['view']['user'] ." c ON (a.updated_by=c.id)
	$q $where ORDER BY a.created_at desc";


	db::query($sql, $rs['test'], $nr['test']);
	$total 	= $nr['test'];
	app::set_default($page_size, (isset($all)?$total:10));
	
	app::set_navigator($sql, $nav, $page_size, $module .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	// $columns = ["no_pengajuan"];
	$columns = ["no_pengajuan","yg_mengajukan","tgl_pengajuan_tbl","name"];
	// progress
	// $columns = array_merge($columns,['print','name','status_progress']);
	$columns = array_merge($columns,['status_progress']);
	if($app['me']['code'] == "it"){
		// $columns = ["view",'approve_it',"approve_it_3"];
		// $columns = array_merge($columns,["view",'approve_it',"approve_it_3"]);
		$columns = array_merge($columns,["view"]);
	}else if($app['me']['code'] == "hrd"){
		// $columns = array_merge($columns,["feedback",'approve_hrd','approve_hrd_3']);
		// $columns = array_merge($columns,['approve_hrd','approve_hrd_3']);
	}else{
		// $columns = array_merge($columns,['approve_2','approve_3','approve_4','confirm_done']);
		// $columns = array_merge($columns,['confirm_done']);
		// $columns = array_merge($columns,['approve_2','approve_3','confirm_done']);
		$columns = array_merge($columns,['approve_2','approve_3','approve_4','approve_hrd','approve_hrd_3','confirm_done']);
	}
	// $columns = ['approve_2','approve_3','approve_it','confirm_done','name'];
	$numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	if($numlang > 1)
	{
		$columns = array_merge($columns,['lang','duplicate']);
	}
	$columns = array_merge($columns,['print']);
	// $columns = array_merge($columns,["approve"]);
	while($row = db::fetch($rs['row']))
	{
		$row['name'] = $row['name'];
		$row['yg_mengajukan'] = $row['created_by'];
		$row['tgl_pengajuan_tbl'] = $row['created_at'];
		///////////////////
		$row['view'] = '<a style="margin-left: 45% !important;" href="'.$app['webmin'] .'/'. $module .'.mod&act=view&id='. $row['id'].'"><i class="fa fa-eye" aria-hidden="true"></i></a>';
		// $row['approve'] = '<a style="margin-left: 45% !important;" href="'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'
		if($row['approve_2']=="tidak" && $app['me']['level'] =="2"){
			// $row['approve_2'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
			$row['approve_2'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="on_reject(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
				$row['approve_3'] ="";
				$row['approve_4'] ="";
			// fa-times
		}elseif($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			if($row['approve_2']!="tidak"){
				$row['approve_2'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';

				if($row['approve_3']=="tidak" && $app['me']['level'] =="3"){
					// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
					// $row['approve_3'] = '<a style="margin-left: 45% !important;color:orange;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
					$row['approve_3'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="on_reject(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';

					$row['approve_4'] ="";
				}elseif($row['status_progress']=="rejected"){
					// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
					$row['approve_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
				}else{
					// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
					if($row['approve_3']!="tidak"){
						$row['approve_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
						
						if($row['approve_4']=="tidak" && $app['me']['level'] =="4"){
							// $row['approve_4'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
							// $row['approve_4'] = '<a style="margin-left: 45% !important;color:orange;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
							$row['approve_4'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_4&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="on_reject(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_4&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
						}elseif($row['status_progress']=="rejected"){
							// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
							$row['approve_4'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
						}else{
							// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
							if($row['approve_4']!="tidak"){
								$row['approve_4'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
							}else{
								$row['approve_4'] ="";
							}
						}
					}else{
						$row['approve_3'] ="";
						$row['approve_4'] ="";
					}
				}
			}else{
				$row['approve_2'] ="";
				$row['approve_3'] ="";
				$row['approve_4'] ="";
			}
		}
		////////////////////////////////////////////
		////////////////////////////////////////////
		if($row['approve_hrd']=="tidak" ){	
			$cek_gm = db::lookup("id","log","id_form='".$row['id']."' AND act='APR_GM'");
			if($cek_gm && $app['me']['level'] == "1"){		
				$row['approve_hrd'] = '<span style="margin-left: 35% !important;"><a style="color:green;" href="'.$app['webmin'] .'/'. $module .'.mod&type=approve_hrd&act=approve_hrd&id='. $row['id'].'"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_hrd&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
			}else{
				$row['approve_hrd'] = "";
			}
			$row['finish_it'] = "";
			$row['approve_it'] = '';
			$row['approve_it_3'] = "";
			$row['approve_hrd_3'] = '';

		}else{
			if($row['approve_hrd_3']=="tidak" && $app['me']['level'] == "3"){
				$row['approve_hrd_3'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_hrd_3&act=approve_hrd_3&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="on_reject(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_hrd_3&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
			}else{
				if($row['approve_hrd_3']!="iya"){
					$row['approve_hrd_3'] = '';
				}else{
					$row['approve_hrd_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				}
			}
			// $row['approve_hrd'] = '<a style="color:green;" href="'.$app['webmin'] .'/'. $module .'.mod&type=approve_hrd&act=approve_hrd&id='. $row['id'].'"><i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i></a>';
			if ($app['me']['level'] == "1") {
				$row['approve_hrd'] = '<a style="color:green;" href="'.$app['webmin'] .'/'. $module .'.mod&type=approve_hrd&act=approve_hrd&id='. $row['id'].'"><i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i></a>';	
			}else{
				$row['approve_hrd'] = '<a style="color:green;" ><i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i></a>';	
			}
	


			if($row['approve_it']=="tidak"){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				
				if($app['me']['level'] == "1"){
					$row['approve_it'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
				}else{
					$row['approve_it'] = '';
				}
				$row['finish_it'] = "";
				$row['approve_it_3']="";
			}else{
				// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				$row['approve_it'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				
			if($row['approve_it_3']!="iya" && $app['me']['level'] == "3"){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				// echo "disini";
				$row['approve_it_3'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it_3&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			}else{
				if($row['approve_it_3']!="iya"){
					// $row['approve_it_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
					$row['approve_it_3'] = "";
				}else{
					$row['approve_it_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				}
				// if($row['finish_it']=="tidak" || empty($row['finish_it'])){
				// 	// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				// 	$row['finish_it'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=finish_it&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
				// }else{
				// 	// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				// 	$row['finish_it'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				// }
			}
			}
		}
		if(($row['status_progress']=="progress" || $row['status_progress']=="finished") && $row['id_created_by'] == $app['me']['id']){
			if($row['confirm_done']=="tidak"){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				$row['confirm_done'] = '<a style="margin-left: 45% !important;color:orange;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&act=confirm_done&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			}else{
				// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				$row['confirm_done'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}
		}else{
			if($row['confirm_done']=="tidak"){
				$row['confirm_done'] = '';
			}else{
				$row['confirm_done'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}
		}
		if($row['status_progress']=="progress" || $row['status_progress']=="finished"){
			$row['print']='<a  target="_blank" href="'.$app['webmin'] .'/'. $module .'.mod&act=print&id='. $row['id'].'&name='.$row['name'].'" ><i style="margin-left: 30% !important;color:grey;font-size:18px" class="fa fa-print"></i></a>';
		}else{
			// $row['print']='<a href="'.$app['webmin'] .'/'. $module .'.mod&act=print&id='. $row['id'].'&name='.$row['name'].'" ><i style="margin-left: 30% !important;color:grey;font-size:18px" class="fa fa-print"></i></a>';
			$row['print']='';
		}
		// if($row['status_progress']!="iya"){
		// 	// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		// 	$row['status_progress'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
		// }else{
		// 	// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
		// 	$row['status_progress'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
		// }
		if($app['me']['code'] == "hrd"){
			if ($app['me']['level'] == "1") {
				$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=approve_hrd&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
			}else{
				$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=view&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
			}
		}elseif($app['me']['code'] == "it"){
			$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=view&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
		}else{
			$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=edit&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
		}
		if($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_4'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_5'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_hrd'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_hrd_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_it'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_it_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_it_5'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['confirm_done'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}
		if ($row['approve_2_status'] == "iya") {
			$row['approve_2']="Disetujui";
		}elseif($row['approve_2_status'] == "reject" || $row['status_progress']=="rejected"){
			$row['approve_2']="Ditolak";
		}else{
			$row['approve_2']="Menunggu";
		}
		if ($row['approve_3_status'] == "iya") {
			$row['approve_3']="Disetujui";
		}elseif($row['approve_3_status'] == "reject" || $row['status_progress']=="rejected"){
			$row['approve_3']="Ditolak";
		}else{
			$row['approve_3']="Menunggu";
		}
		if ($row['approve_4_status'] == "iya") {
			$row['approve_4']="Disetujui";
		}elseif($row['approve_4_status'] == "reject" || $row['status_progress']=="rejected"){
			$row['approve_4']="Ditolak";
		}else{
			$row['approve_4']="Menunggu";
		}
		if ($row['approve_hrd_status'] == "iya") {
			$row['approve_hrd']="Disetujui";
		}elseif($row['approve_hrd_status'] == "reject" || $row['status_progress']=="rejected"){
			$row['approve_hrd']="Ditolak";
		}else{
			$row['approve_hrd']="Menunggu";
		}
		if ($row['approve_hrd_3_status'] == "iya") {
			$row['approve_hrd_3']="Disetujui";
		}elseif($row['approve_hrd_3_status'] == "reject" || $row['status_progress']=="rejected"){
			$row['approve_hrd_3']="Ditolak";
		}else{
			$row['approve_hrd_3']="Menunggu";
		}
		if($numlang > 1)
		{
			$row['duplicate'] = '<a href="'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		}
		else {
			$row['duplicate'] = null;
		}
		$row_status_lama = $row['status_progress'];
		$row['status_progress'] = app::getliblang("table_".$row['status_progress']);
		if ($row_status_lama =="rejected") {
			// $row['status_progress'] = db::lookup("id_user","log", "(id_form='".$row['id']."' AND act ='APR') AND module ='".$module."' order by created_at desc");
			$get_reject = db::lookup("id_user","log", "(id_form='".$row['id']."' AND act ='APR') AND module ='".$module."' order by created_at desc");
			$row['status_progress'] = $row['status_progress']."<br>[ ".db::viewlookup("name","user", "id='$get_reject'")." ] ";
		}
		$results[] = $row;
	}
	$option = ['sortable'=>false, 'status'=>true];
	if($app['me']['code']=="it" || $app['me']['code']=="hrd"){
		$option = array_merge($option,['no_edit'=>true]);
	}
	$unpost = 1;
	$unmodify=1;
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
?>
<?php
// print_r($_REQUEST);
// exit;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		// form::populate($module);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		if($app['me']["code"] != "it"){
			if($app['me']["code"] != "hrd"){
				// $validate = 'p_name,p_id_departement_new,p_id_section,p_list_permintaan,p_memperjelas';
				$validate = 'p_name,p_memperjelas';
			}else{
				// $validate = 'p_name,p_id_departement_new,p_id_section,p_list_permintaan,p_memperjelas,p_nik,p_status_ker,p_join_date';
				$validate = 'p_name,p_memperjelas,p_nik,p_status_ker,p_join_date';
			}
			$get_spv =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='2'");
			$get_mng =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='3'");
			$cek_gm =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='4'");
			$cek_dir =db::viewlookup("id","user","level ='5'");
			form::serialize_form();
			form::validate('empty', $validate);
			if (form::is_error()):
				msg::build_msg();
				header("location: ". $module .".mod&act=add&error=1");
				exit;
			endif;
			app::mq_encode('p_name,p_nik,p_id_departement,p_username,p_alasan');
			// $identity = rand(1, 100).date("dmYHis");
			// $p_images = str_replace("%20"," ","$p_images");
			// $sql_lang 	= "SELECT id FROM ". $app['table']['lang'] ." WHERE status='active'";
			$p_alias = app::slugify($p_title);
			// db::query($sql_lang, $rs['lang'], $nr['lang']);
			// while($row 	= db::fetch($rs['lang']))
			// {
				if($app['me']['code'] != "hrd"){
					if($app['me']['level'] == 5){
						$approved 		= ",approve_2,approve_3,approve_4,approve_5";
						$approved_val 	= ",'iya','iya','iya','iya'";
						$status_progress 	= ",status_progress";
						$status_progress_val 	= ",'fully'";
						$log_approve = "APR_DIR";
					}
					if($app['me']['level'] == 4){
						$approved 		= ",approve_2,approve_3,approve_4";
						$approved_val 	= ",'iya','iya','iya'";
						$status_progress 	= ",status_progress";
						$status_progress_val 	= ",'fully'";
						$log_approve = "APR_GM";
					}
					if($app['me']['level'] == 3){
						$cek_gm =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='4'");
						if($cek_gm){
							$approved 		= ",approve_2,approve_3";
							$approved_val 	= ",'iya','iya'";
							$status_progress 	= ",status_progress";
							$status_progress_val 	= ",'partly'";
						}else{
							$approved 		= ",approve_2,approve_3,approve_4";
							$approved_val 		= ",'iya','iya','iya'";
							$status_progress 	= ",status_progress";
							$status_progress_val 	= ",'fully'";
						}
						$log_approve = "APR_MNG";
					}
					if($app['me']['level'] == 2){
						if($get_mng){
							$approved 		= ",approve_2";
							$approved_val 	= ",'iya'";
							$status_progress 	= ",status_progress";
							$status_progress_val 	= ",'partly'";
							$log_approve = "APR_SPV";
						}else{
							if($cek_gm){
							$approved 		= ",approve_2,approve_3";
							$approved_val 	= ",'iya','iya'";
							$status_progress 	= ",status_progress";
							$status_progress_val 	= ",'partly'";
							$log_approve = "APR_MNG";
							}else{
							$approved 		= ",approve_2,approve_3,approve_4";
							$approved_val 	= ",'iya','iya','iya'";
							$status_progress 	= ",status_progress";
							$status_progress_val 	= ",'fully'";
							$log_approve = "APR_GM";
							}
						}
					}
				}else{
					if($app['me']['level'] == 3){
						$approved 		= ",approve_2,approve_3,approve_4,approve_hrd,approve_hrd_3";
						$approved_val 	= ",'iya','iya','iya','iya','iya'";
						$status_progress 	= ",status_progress";
						$status_progress_val 	= ",'fully'";
						$log_approve = "APR_HRD_3";
					}
					if($app['me']['level'] == 2){
						$approved 		= ",approve_2,approve_3,approve_4,approve_hrd,approve_hrd_3";
						$approved_val 	= ",'iya','iya','iya','iya','iya'";
						$status_progress 	= ",status_progress";
						$status_progress_val 	= ",'fully'";
						$log_approve = "APR_HRD_3";
					}
					if($app['me']['level'] == 1){
			
						if($get_mng){
						$approved 		= ",approve_2,approve_3,approve_4,approve_hrd";
						$approved_val 	= ",'iya','iya','iya','iya'";
						$status_progress 	= ",status_progress";
						$status_progress_val 	= ",'partly'";
						$log_approve = "APR_HRD";
						}else{
						$approved 		= ",approve_2,approve_3,approve_4,approve_hrd,approve_hrd_3";
						$approved_val 	= ",'iya','iya','iya','iya','iya'";
						$status_progress 	= ",status_progress";
						$status_progress_val 	= ",'fully'";
						$log_approve = "APR_HRD_3";
						}
					}
				}
				$id 		= rand(1, 100).date("dmYHis");
				// $p_lang 	= $row['id'];
				$id 		= db::cek_id($id,$module);
				$urut 		= db::lookup("max(reorder)",$module,"1=1");
				$p_list_permintaan 	= implode(";", $p_list_permintaan);

				$no_urut	= db::lookup("max(no_urut)",$module,"1=1");
				$no_urut	= $no_urut+1;
				$p_no_pengajuan = "GN-011-".date("dmy")."-".$no_urut;
				// $data = db::get_record("feedback","id_project ='$id' AND id_client = '$p_client_id'");
				if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
				// print_r($_REQUEST);
				if ($p_attach_size > 0):
					// save_file_rename
					// $file = file::save_picture('p_attach',$app['pwebmin'] ."/assets/".$module."/", 'attach');
					$file = file::save_file_rename('p_attach',$app['pwebmin'] ."/assets/".$module."/", $p_no_pengajuan);
					if (!$err_attach):
						// @unlink($app['pwebmin'] ."/assets/".$module."/". $data['attach']);
						$data['attach'] = $file;
					endif;
				endif;
				$sql 		= "INSERT INTO ".$app['table'][$module]."
						(id, name, no_pengajuan, no_urut, id_section, id_section_old, id_departement_new, id_departement_old, nik, status_user, list_permintaan, memperjelas, join_date, end_date, status_ker, attach, reorder, created_by, created_at$approved $status_progress) VALUES
						('$id', '$p_name', '$p_no_pengajuan', '$no_urut', '$p_id_section', '$p_id_section_old', '$p_id_departement_new', '$p_id_departement_old', '$p_nik', '$p_status_user', '$p_list_permintaan', '$p_memperjelas', ".(empty($p_join_date)?'null':"'".$p_join_date."'").", ".(empty($p_end_date)?'null':"'".$p_end_date."'").", ".(empty($p_status_ker)?'null':"'".$p_status_ker."'").", '".$data['attach']."', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";
				
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "INS",$id,$p_masa_simpan);

					$code="";
					$id_section = $app['me']['id_section'];
					$link = $app['http'] ."/". $module .".mod";
					if($app['me']['code'] != "hrd"){
						if($app['me']['level'] == 1){
							#$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
							if($get_spv){
								$level = "2";
							}else{
								if($get_mng){
									$level = "3";
								}else{
								     if($cek_gm){
									$level = "4";
								      }else{
									$level = "1";
									$code = "hrd";
									$id_user = "";
									$id_section = db::lookup("id","section","code","hrd");
								    }
								}
							}
						}elseif($app['me']['level'] == 2){
							if($get_mng){
								$level = "3";
							}else{
									if($cek_gm){
									$level = "4";
								      }else{
									$level = "1";
									$code = "hrd";
									$id_user = "";
									$id_section = db::lookup("id","section","code","hrd");
								    }
							}
						}elseif($app['me']['level'] == 3){

							$cek_gm =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='4'");
							if($cek_gm){
								$level = "4";
							}else{
								
								$level = "1";
								$code = "hrd";
								$id_user = "";
								$id_section = db::lookup("id","section","code","hrd");
							}
							// $code = "it";
							// $id_section = db::lookup("id","section","code","it");
						}elseif($app['me']['level'] == 4){
							$level = "1";
							$code = "hrd";
							$id_user = "";
							$id_section = db::lookup("id","section","code","hrd");
						}
					}else{
						if($app['me']['level'] == 1){
							if($get_mng){
								$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
								$code = "hrd";
								$id_section = db::lookup("id","section","code","hrd");
  								$level = "3";
							}else{
								$level = "1";
								$code = "it";
								$id_section = db::lookup("id","section","code","it");
							}
							// }
						}elseif($app['me']['level'] == 2){
							$level = "1";
							$code = "it";
							$id_section = db::lookup("id","section","code","it");
						}elseif($app['me']['level'] == 3){
							$level = "1";
							$code = "it";
							$id_section = db::lookup("id","section","code","it");
						}
					}
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level = null)
					// $id_it = db::lookup("name","section");
					db::add_notif($id, $code, $app['me']['id'], "Form Pengajuan user telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);

					$config = db::get_record("configuration","1","1");
					$get_caption =  admlib::$page_active['caption'];
					$sql_email = "SELECT * FROM ". $app['view']["user"] ." where
								  (level = '".$level."' AND id_section = '".$id_section."') AND status = 'active' ORDER BY created_at desc";
					

					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
					while($row = db::fetch($rs['sql_email']))
					{
						
						$msg_email .= "<p>Yth. ".$row['name'].",</p>";
						$msg_email .= "Dengan ini kami beritahukan bahwa ada sebuah permohonan yang dibuat di dalam aplikasi ForgiSystem menunggu pernyataan Anda.
			Berikut adalah informasi singkat terkait permohonan dimaksud.";
						$msg_email .= "<table>";
						$msg_email .= "<tr><td>No. Permohonan</td><td>:</td><td><td>".$p_no_pengajuan."</td></tr>";
						$msg_email .= "<tr><td>Pemohon </td><td>:</td><td><td>".$app['me']['name']."</td></tr>";
						$msg_email .= "<tr><td>Tanggal Dibuat</td><td>:</td><td><td>".date("d-m-Y")."</td></tr>";
						$msg_email .= "<tr><td>Jenis Permohonan</td><td>:</td><td><td>".$get_caption."</td></tr>";
						$msg_email .= "</table>";
						$msg_email .= "<p>Silakan masuk ke aplikasi <a href='".$app['http']."'>ForgiSystem.cmwi.co.id</a> untuk merespon permohonan tersebut.</p>";
						$msg_email .= "<p>Hormat Kami,</p>";
						$msg_email .= "<p>ForgiSystem</p>";
						$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
						$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
						// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
						if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
							// print_r();
							app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
						}
						$msg_email = "";
					}
					if($app['me']['level'] > 1){
						db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id,$p_masa_simpan);
					}
				}
				
			// }
			msg::set_message('success', app::i18n('create'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}else{
			msg::set_message('error', app::i18n('it_not_add'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		// echo $module;
		// echo $id;
		$form = db::get_record($module, "id", $id);
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");
		form::populate($module);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
			if($app['me']["code"] != "hrd"){
				// $validate = 'p_name,p_id_departement_new,p_id_section,p_list_permintaan,p_memperjelas';
				$validate = 'p_name,p_memperjelas';
			}else{
				// $validate = 'p_name,p_id_departement_new,p_id_section,p_list_permintaan,p_memperjelas,p_nik,p_status_ker,p_join_date';
				$validate = 'p_name,p_memperjelas,p_nik,p_status_ker,p_join_date';
			}
		form::validate('empty', $validate);
		if (form::is_error()):
			msg::build_msg();
			header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$p_images = str_replace("%20"," ","$p_images");
		$p_alias = app::slugify($p_title);
		// $sql 		= "INSERT INTO ".$app['table'][$module]."
		// 		(id, name, id_section, id_departement_new, id_departement_old, nik, status_user, list_permintaan, memperjelas, join_date, end_date, status_ker, attach, reorder, created_by, created_at$approved $status_progress) VALUES
		// 		('$id', '$p_name', '$p_id_section', '$p_id_departement_new', '$p_id_departement_old', '$p_nik', '$p_status_user', '$p_list_permintaan', '$p_memperjelas', '$p_join_date', '$p_end_date', '$p_status_ker', '$p_attach', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";
		$p_list_permintaan 	= implode(";", $p_list_permintaan);
		app::mq_encode('p_title,p_content,p_image,p_lang');


		$data = db::get_record($module, "id", $id);
		$edit_before= "";
		$edit_after = "";

		if ($data['name'] !=$p_name) {
			$edit_before .= "name : ".$data['name'].";";
			$edit_after .= "name : ".$p_name.";";
		}
		if ($data['id_section'] !=$p_id_section) {
			$edit_before .= "section : ".$data['id_section'].";";
			$edit_after .= "section : ".$p_id_section.";";
		}
		if ($data['id_section_old'] !=$p_id_section_old) {
			$edit_before .= "section_old : ".$data['id_section_old'].";";
			$edit_after .= "section_old : ".$p_id_section_old.";";
		}
		if ($data['id_departement_new'] !=$p_id_departement_new) {
			$edit_before .= "departement_new : ".$data['id_departement_new'].";";
			$edit_after .= "departement_new : ".$p_id_departement_new.";";
		}
		if ($data['id_departement_old'] !=$p_id_departement_old) {
			$edit_before .= "departement_old : ".$data['id_departement_old'].";";
			$edit_after .= "departement_old : ".$p_id_departement_old.";";
		}
		if ($data['memperjelas'] !=$p_memperjelas) {
			$edit_before .= "memperjelas : ".$data['memperjelas'].";";
			$edit_after .= "memperjelas : ".$p_memperjelas.";";
		}
		if ($data['status_user'] !=$p_status_user) {
			$edit_before .= "status_user : ".$data['status_user'].";";
			$edit_after .= "status_user : ".$p_status_user.";";
		}
		if ($data['list_permintaan'] !=$p_list_permintaan) {
			$edit_before .= "list_permintaan : ".$data['list_permintaan'].";";
			$edit_after .= "list_permintaan : ".$p_list_permintaan.";";
		}

		if ($app['me']['code']=="hrd") {
			if ($data['nik'] !=$p_nik) {
				$edit_before .= "nik : ".$data['nik'].";";
				$edit_after .= "nik : ".$p_nik.";";
			}
			if ($data['join_date'] !=$p_join_date) {
				$edit_before .= "join_date : ".$data['join_date'].";";
				$edit_after .= "join_date : ".$p_join_date.";";
			}
			if ($data['end_date'] !=$p_end_date) {
				$edit_before .= "end_date : ".$data['end_date'].";";
				$edit_after .= "end_date : ".$p_end_date.";";
			}
			if ($data['status_ker'] !=$p_status_ker) {
				$edit_before .= "status_ker : ".$data['status_ker'].";";
				$edit_after .= "status_ker : ".$p_status_ker.";";
			}
			if ($data['attach'] !=$p_attach) {
				$edit_before .= "attach : ".$data['attach'].";";
				$edit_after .= "attach : ".$p_attach.";";
			}
		}
		// if ($data['name'] !=$p_name) {
		// 	$edit_before .= $data['name'].";";
		// 	$edit_after .= $p_name.";";
		// }
		$edit_before = rtrim($edit_before,";");
		$edit_after  = rtrim($edit_after,";");
		
		$form = db::get_record("*",$module, "id", $id);
		if ($p_attach_del){
			@unlink($app['pwebmin'] ."/assets/".$module."/". $form['attach']);	
			$data['attach'] = null;
		}else{
			if ($p_attach_size > 0):
				// $file = file::save_picture('p_attach',$app['pwebmin'] ."/assets/".$module."/", 'attach');
				// echo $app['pwebmin'] ."/assets/".$module."/". $form['attach'];
				// exit;
				@unlink($app['pwebmin'] ."/assets/".$module."/". $form['attach']);
				$file = file::save_file_rename('p_attach',$app['pwebmin'] ."/assets/".$module."/", $form['no_pengajuan']);
				// echo $file;exit;
				if (!$err_attach):
					// @unlink($app['pwebmin'] ."/assets/".$module."/". $form['attach']);
					$data['attach'] = $file;
				endif;
			endif;	
		}
		$sql = "update ". $app['table'][$module] ."
				set name = '$p_name',
					nik = '$p_nik',
					id_section = '$p_id_section',
					id_section_old = '$p_id_section_old',
					id_departement_new = '$p_id_departement_new',
					id_departement_old = '$p_id_departement_old',
					status_user = '$p_status_user',
					list_permintaan = '$p_list_permintaan',
					memperjelas = '$p_memperjelas',
					join_date = ".(empty($p_join_date)?'null':"'".$p_join_date."'").",
					end_date = ".(empty($p_end_date)?'null':"'".$p_end_date."'").",
					status_ker = ".(empty($p_status_ker)?'null':"'".$p_status_ker."'").",
					attach = '".$data['attach']."',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "UPD",$id, $p_masa_simpan,$ipaddress, "",$edit_before, $edit_after);
			
			$code="";
			$id_section = $app['me']['id_section'];
			$created_user = $app['me']['id'];
			$link = $app['http'] ."/". $module .".mod";
			
			$level = "1";
			$code = "id";
			$created_user = db::lookup("created_by",$module,"id",$id);
			
			$id_section = db::lookup("id","section","code","it");
			// }
			// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level = null)
			// $id_it = db::lookup("name","section","code","it");
			db::add_notif($id, $code, $app['me']['id'], "Form Pengajuan user telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
		}
		msg::set_message('success', app::i18n('modify'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	else:
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table'][$module]."
			SET status = '$statusnya'
			WHERE identity = '$identity'";
	db::qry($sql);
	msg::set_message('success', app::i18n('update'));
	header("location: " . $app['webmin'] ."/". $module .".mod");
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	$module = admlib::$page_active['module'];
	if($step == 1)
	{
		if($id) $items = $id;
		else $items = implode("','", $p_del);
		$sql = "SELECT id, name title FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		/*$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $delid ."')";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "DEL",$delid, $p_masa_simpan);
			$sql2 	= "DELETE FROM ". $app['table']["notif"] ." WHERE `id_form` = '". $delid ."' AND from_form = '".$module."'";
			db::qry($sql2);
		}*/
		if(count($p_id) > 1){	
			foreach ($p_id as $value2) {
				$get_file = db::lookup("attach",$module,"id",$value2);
				$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $value2 ."')";
				if(db::qry($sql)){
					@unlink($app['pwebmin'] ."/assets/attach/". $get_file);
					// db::add_log_doc($app['me']['id'],$value2,"DEL_DOC",$ipaddress,"");
					db::ins_call_func("insert_log", $app['me']['id'], $module, "DEL", $value2, $p_masa_simpan,$ipaddress, "");
					$sql2 	= "DELETE FROM ". $app['table']["notif"] ." WHERE `id_form` = '". $value2 ."' AND from_form = '".$module."'";
					db::qry($sql2);
				}
			} 
		}else{
			$get_file = db::lookup("attach",$module,"id",$delid);
			$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $delid ."')";
			// db::qry($sql);
			if(db::qry($sql)){
				@unlink($app['pwebmin'] ."/assets/attach/". $get_file);
				// db::add_log_doc($app['me']['id'],$id,"DEL_DOC",$ipaddress,"");
				db::ins_call_func("insert_log", $app['me']['id'], $module, "DEL",$delid, $p_masa_simpan,$ipaddress, "");
				$sql2 	= "DELETE FROM ". $app['table']["notif"] ." WHERE `id_form` = '". $delid ."' AND from_form = '".$module."'";
				db::qry($sql2);
			}
		}
		msg::set_message('success', app::i18n('delete'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$p_title 		= $row['title'];
			$p_content	 	= $row['content'];
			$p_image 		= $row['image'];

			$urut = db::lookup("max(reorder)", $module, "1=1");
			if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
			$id = rand(1, 100).$urut.date("dmYHis");

			app::mq_encode('p_title,p_content');
			$sql = "INSERT INTO ". $app['table'][$module] ."
				(id, title, content, status, reorder, created_by, created_at) VALUES
				('$id', '$p_title', '$p_content', 'inactive', '$urut', '". $app['me']['id'] ."', now())";
			db::qry($sql);
		}
		msg::set_message('success', app::i18n('create'));
	}
	else
	{
		msg::set_message('error', app::i18n('notfound'));
	}
	header("location: " . $app['webmin'] ."/". $module .".mod");
	exit;
endif;

/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $count ++;
			$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : approve_hrd
*******************************************************************************/
if ($act == "approve_hrd"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		// echo $module;
		// echo $id;
		$form = db::get_record($module, "id", $id);
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");
		form::populate($module);
		include "dsp_form_hrd.php";
		exit;
	endif;
	if ($step == 2):
// print_r($_REQUEST);
// exit;

			$data = db::get_record($module,"id",$id);
	if (empty($reject) || $reject !="1") {
			form::serialize_form();
			// $validate = 'p_name,p_id_departement_new,p_id_section,p_list_permintaan,p_memperjelas,p_nik,p_status_ker,p_join_date';

			if($app['me']["code"] != "hrd"){
				// $validate = 'p_name,p_id_departement_new,p_id_section,p_list_permintaan,p_memperjelas';
				$validate = 'p_name,p_memperjelas';
			}else{
				// $validate = 'p_name,p_id_departement_new,p_id_section,p_list_permintaan,p_memperjelas,p_nik,p_status_ker,p_join_date';
				$validate = 'p_name,p_memperjelas,p_nik,p_status_ker,p_join_date';
			}
			form::validate('empty', $validate);
			if (form::is_error()):
				msg::build_msg();
				header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
				exit;
			endif;
			$p_images = str_replace("%20"," ","$p_images");
			$p_alias = app::slugify($p_title);
			// $sql 		= "INSERT INTO ".$app['table'][$module]."
			// 		(id, name, id_section, id_departement_new, id_departement_old, nik, status_user, list_permintaan, memperjelas, join_date, end_date, status_ker, attach, reorder, created_by, created_at$approved $status_progress) VALUES
			// 		('$id', '$p_name', '$p_id_section', '$p_id_departement_new', '$p_id_departement_old', '$p_nik', '$p_status_user', '$p_list_permintaan', '$p_memperjelas', '$p_join_date', '$p_end_date', '$p_status_ker', '$p_attach', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";
			$p_list_permintaan 	= implode(";", $p_list_permintaan);
			app::mq_encode('p_title,p_content,p_image,p_lang');

			$data = db::get_record($module, "id", $id);
			$edit_before= "";
			$edit_after = "";

			if ($data['name'] !=$p_name) {
				$edit_before .= "name : ".$data['name'].";";
				$edit_after .= "name : ".$p_name.";";
			}
			if ($data['nik'] !=$p_nik) {
				$edit_before .= "nik : ".$data['nik'].";";
				$edit_after .= "nik : ".$p_nik.";";
			}
			if ($data['id_section'] !=$p_id_section) {
				$edit_before .= "section : ".$data['id_section'].";";
				$edit_after .= "section : ".$p_id_section.";";
			}
			if ($data['id_section_old'] !=$p_id_section_old) {
				$edit_before .= "section_old : ".$data['id_section_old'].";";
				$edit_after .= "section_old : ".$p_id_section_old.";";
			}
			if ($data['id_departement_new'] !=$p_id_departement_new) {
				$edit_before .= "departement_new : ".$data['id_departement_new'].";";
				$edit_after .= "departement_new : ".$p_id_departement_new.";";
			}
			if ($data['id_departement_old'] !=$p_id_departement_old) {
				$edit_before .= "departement_old : ".$data['id_departement_old'].";";
				$edit_after .= "departement_old : ".$p_id_departement_old.";";
			}
			if ($data['status_user'] !=$p_status_user) {
				$edit_before .= "status_user : ".$data['status_user'].";";
				$edit_after .= "status_user : ".$p_status_user.";";
			}
			if ($data['list_permintaan'] !=$p_list_permintaan) {
				$edit_before .= "list_permintaan : ".$data['list_permintaan'].";";
				$edit_after .= "list_permintaan : ".$p_list_permintaan.";";
			}
			if ($data['memperjelas'] !=$p_memperjelas) {
				$edit_before .= "memperjelas : ".$data['memperjelas'].";";
				$edit_after .= "memperjelas : ".$p_memperjelas.";";
			}
			if ($data['join_date'] !=$p_join_date) {
				$edit_before .= "join_date : ".$data['join_date'].";";
				$edit_after .= "join_date : ".$p_join_date.";";
			}
			if ($data['end_date'] !=$p_end_date) {
				$edit_before .= "end_date : ".$data['end_date'].";";
				$edit_after .= "end_date : ".$p_end_date.";";
			}
			if ($data['status_ker'] !=$p_status_ker) {
				$edit_before .= "status_ker : ".$data['status_ker'].";";
				$edit_after .= "status_ker : ".$p_status_ker.";";
			}
			if ($data['attach'] !=$p_attach) {
				$edit_before .= "attach : ".$data['attach'].";";
				$edit_after .= "attach : ".$p_attach.";";
			}
			// if ($data['name'] !=$p_name) {
			// 	$edit_before .= $data['name'].";";
			// 	$edit_after .= $p_name.";";
			// }
			$edit_before = rtrim($edit_before,";");
			$edit_after  = rtrim($edit_after,";");

			$form = db::get_record("*",$module, "id", $id);
			if ($p_attach_del){
				@unlink($app['pwebmin'] ."/assets/".$module."/". $form['attach']);	
				$data['attach'] = null;
			}else{
				if ($p_attach_size > 0):
					// $file = file::save_picture('p_attach',$app['pwebmin'] ."/assets/".$module."/", 'attach');
					// echo $app['pwebmin'] ."/assets/".$module."/". $form['attach'];
					// exit;
					@unlink($app['pwebmin'] ."/assets/".$module."/". $form['attach']);
					$file = file::save_file_rename('p_attach',$app['pwebmin'] ."/assets/".$module."/", $form['no_pengajuan']);
					// echo $file;exit;
					if (!$err_attach):
						@unlink($app['pwebmin'] ."/assets/".$module."/". $form['attach']);
						$data['attach'] = $file;
					endif;
				endif;	
			}
			$get_spv =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='2'");
			$get_mng =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='3'");
			$cek_gm =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='4'");
			$cek_dir =db::viewlookup("id","user","level ='5'");
			if(!$get_mng){
				$approve_hrd	= "approve_hrd_3 = 'iya',";
			}
			
			$sql = "update ". $app['table'][$module] ."
					set name = '$p_name',
						nik = '$p_nik',
						id_section = '$p_id_section',
						id_section_old = '$p_id_section_old',
						id_departement_new = '$p_id_departement_new',
						id_departement_old = '$p_id_departement_old',
						nik = '$p_nik',
						status_user = '$p_status_user',
						list_permintaan = '$p_list_permintaan',
						memperjelas = '$p_memperjelas',
						join_date = ".(empty($p_join_date)?'null':"'".$p_join_date."'").",
						end_date = ".(empty($p_end_date)?'null':"'".$p_end_date."'").",
						status_ker = ".(empty($p_status_ker)?'null':"'".$p_status_ker."'").",
						attach = '".$data['attach']."',
						approve_hrd = 'iya',
						$approve_hrd
						updated_by = '". $app['me']['id'] ."',
						updated_at = now()
					where id = '$id'";
			// db::qry($sql);
			if(db::qry($sql)){
				db::ins_call_func("insert_log", $app['me']['id'], $module, "APR_HRD", $id, $p_masa_simpan,$ipaddress, "",$edit_before, $edit_after);
			
				$link = $app['http'] ."/". $module .".mod";
				if($app['me']['level'] == 1){
					if($get_mng){
					#echo "disini";
						#$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
						$code = "hrd";
						$id_section = db::lookup("id","section","code","hrd");
						$level = "3";
					}else{
					#echo "disana";
						$level = "1";
						$code = "it";
						$id_section = db::lookup("id","section","code","it");
					}		
#exit;		
				}elseif($app['me']['level'] == 2){
					$level = "1";
					$code = "it";
					$id_section = db::lookup("id","section","code","it");
				}elseif($app['me']['level'] == 3){
					$level = "1";
					$code = "it";
					$id_section = db::lookup("id","section","code","it");
				}
				db::add_notif($id, $code, $app['me']['id'], "Form Pengajuan user telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);

				$config = db::get_record("configuration","1","1");
				$get_form_user =  db::lookup("name","member","id",$data['created_by']);
				$get_caption =  admlib::$page_active['caption'];
				$sql_email = "SELECT * FROM ". $app['view']["user"] ." where
							  (level = '".$level."' AND id_section = '".$id_section."') AND status = 'active' ORDER BY created_at desc";
				db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
				while($row = db::fetch($rs['sql_email']))
				{
					
					$msg_email .= "<p>Yth. ".$row['name'].",</p>";
					$msg_email .= "Dengan ini kami beritahukan bahwa ada sebuah permohonan yang dibuat di dalam aplikasi ForgiSystem menunggu pernyataan Anda.
		Berikut adalah informasi singkat terkait permohonan dimaksud.";
					$msg_email .= "<table>";
					$msg_email .= "<tr><td>No. Permohonan</td><td>:</td><td><td>".$data['no_pengajuan']."</td></tr>";
					$msg_email .= "<tr><td>Pemohon </td><td>:</td><td><td>".$get_form_user."</td></tr>";
					$msg_email .= "<tr><td>Tanggal Dibuat</td><td>:</td><td><td>".date("d-m-Y",strtotime($data['created_at']))."</td></tr>";
					$msg_email .= "<tr><td>Jenis Permohonan</td><td>:</td><td><td>".$get_caption."</td></tr>";
					$msg_email .= "</table>";
					$msg_email .= "<p>Silakan masuk ke aplikasi <a href='".$app['http']."'>ForgiSystem.cmwi.co.id</a> untuk merespon permohonan tersebut.</p>";
					$msg_email .= "<p>Hormat Kami,</p>";
					$msg_email .= "<p>ForgiSystem</p>";
					$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
					$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
					// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
					if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
						app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
					}
					$msg_email = "";
				}
			}
			msg::set_message('success', app::i18n('modify'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}else{
					// $approved = rtrim($approved,",");
					$sql = "update ". $app['table'][$module] ."
							set approve_2 = 'reject',
								approve_3 = 'reject',
								approve_4 = 'reject',
								approve_hrd = 'reject',
								approve_hrd_3 = 'reject',
								approve_it = 'reject',
								confirm_done = 'reject',
								status_progress = 'rejected'
								where id = '$id'";
								
					// db::qry($sql);
					if(db::qry($sql)){
						db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
						
						$code = "";
						$id_section = $app['me']['id_section'];
						$link = $app['http'] ."/". $module .".mod";
						if($app['me']['level'] == 1){
							$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
							if($get_spv !=""){
								$level = "2";
							}else{
								$level = "3";
							}
						}elseif($app['me']['level'] == 2){
							$level = "3";
						}elseif($app['me']['level'] == 3){
							$level = "1";
							$code = "it";
							$id_section = db::lookup("id","section","code","it");
						}
						// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
						// $id_it = db::lookup("name","section");
						db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
						
						$config = db::get_record("configuration","1","1");
						$get_form_user =  db::lookup("name","member","id",$data['created_by']);
						$get_caption =  admlib::$page_active['caption'];
						$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
						db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
						while($row = db::fetch($rs['sql_email']))
						{
							
							$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
							$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." <b> Ditolak </b>.";
							$msg_email .= "<p>Salam,</p>";
							$msg_email .= "<p>ForgiSystem</p>";
							$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
							$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
							// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
							if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
								app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
							}
							$msg_email = "";
						}
					}
					msg::set_message('success', app::i18n('approved_msg'));
					header("location: " . $app['webmin'] ."/". $module .".mod");
					exit;
			}
	endif;
endif;
/*******************************************************************************
* Action : view
*******************************************************************************/
if ($act == "view"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		// echo $module;
		// echo $id;
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");
		$form = db::get_record($module, "id", $id);
		form::populate($module);
		include "dsp_view.php";
		exit;
	endif;
endif;
/*******************************************************************************
* Action : approve
*******************************************************************************/
if ($act == "approve"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
			$data = db::get_record($module,"id",$id);
	// print_r($_REQUEST);
	// if($type=="approve_2"){

		// if($app["me"]["level"] >= 2){
		if($app["me"]["level"] > 1){
				if($reject !="1"){
					$get_spv =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='2'");
					$get_mng =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='3'");
					$cek_gm =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='4'");
					$cek_dir =db::viewlookup("id","user","level ='5'");

				if($app["me"]["level"] >= 4){
					// approve_4
					$approved .= "approve_2 = 'iya',";
					$approved .= "approve_3 = 'iya',";
					$approved .= "approve_4 = 'iya',";
					$status = "partly";
					$log_approve = "APR_GM";
				}
				if($app["me"]["level"] == 3){
					// approve_4
					$cek_gm =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='4'");
					if($cek_gm){
						$approved .= "approve_2 = 'iya',";
						$approved .= "approve_3 = 'iya',";
						$status = "partly";
					}else{
						$approved .= "approve_2 = 'iya',";
						$approved .= "approve_3 = 'iya',";
						$approved .= "approve_4 = 'iya',";
						$status = "partly";
					}
					$log_approve = "APR_MNG";
				}
				if($app["me"]["level"] == 2){
					if($get_spv){
						$approved .= "approve_2 = 'iya',";
						$log_approve = "APR_SPV";
						$status = "partly";
					}else{
						if($get_mng){
							$approved .= "approve_2 = 'iya',";
							$log_approve = "APR_SPV";
							$status = "partly";
						}else{
							if($cek_gm){
								$approved .= "approve_2 = 'iya',";
								$approved .= "approve_3 = 'iya',";
								$status = "partly";
								$log_approve = "APR_MNG";
							}else{
								$approved .= "approve_2 = 'iya',";
								$approved .= "approve_3 = 'iya',";
								$approved .= "approve_4 = 'iya',";
								$status = "partly";
								$log_approve = "APR_GM";
							}
						}
					}
				}
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					// db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id, $p_masa_simpan);
					
				/* 	$code="";
					$id_section = $app['me']['id_section'];
					$link = $app['http'] ."/". $module .".mod";
					if($app['me']['level'] == 1){
						$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
						$code = "hrd";
						$id_section = db::lookup("id","section","code","hrd");
						if($get_spv !=""){
							$level = "2";
						}else{
							$level = "3";
						}
					}elseif($app['me']['level'] == 2){
						$level = "1";
						$code = "it";
						$id_section = db::lookup("id","section","code","it");
					}elseif($app['me']['level'] == 3){
						$level = "1";
						$code = "it";
						$id_section = db::lookup("id","section","code","it");
					} */
					$id_user = $app['me']['id'];
					$code="";
					$id_section = $app['me']['id_section'];
					$link = $app['http'] ."/". $module .".mod";
					if($app['me']['code'] != "hrd"){
						if($app['me']['level'] == 1){
							#$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
							if($get_spv){
								$id_user = "";
								$level = "2";
							}else{
								if($get_mng){
									$id_user = "";
									$level = "3";
								}else{
									if($get_gm){
										$id_user = "";
										$level = "4";
									}else{
										$level = "1";
										$code = "hrd";
										$id_user = "";
										$id_section = db::lookup("id","section","code","hrd");
									}
								}
							}
						}elseif($app['me']['level'] == 2){
							if($get_mng){
								$id_user = "";
								$level = "3";
							}else{
								if($get_gm){
									$id_user = "";
									$level = "4";
								}else{
									$level = "1";
									$code = "hrd";
									$id_user = "";
									$id_section = db::lookup("id","section","code","hrd");
								}
							}
						}elseif($app['me']['level'] == 3){

							$cek_gm =db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='4'");
							if($cek_gm){
								$id_user = "";
								$level = "4";
							}else{
								$level = "1";
								$code = "hrd";
								$id_user = "";
								$id_section = db::lookup("id","section","code","hrd");
							}
						}elseif($app['me']['level'] == 4){
							$level = "1";
							$code = "hrd";
							$id_user = "";
							$id_section = db::lookup("id","section","code","hrd");
						}
					}else{
						if($app['me']['level'] == 1){
							if($cek_mng){
								#$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
								$code = "hrd";
								$id_section = db::lookup("id","section","code","hrd");
								$level = "3";
							}else{
								$level = "1";
								$code = "it";
								$id_user = "";
								$id_section = db::lookup("id","section","code","it");
							}
						}elseif($app['me']['level'] == 2){
							$level = "1";
							$code = "it";
							$id_user = "";
							$id_section = db::lookup("id","section","code","it");
						}elseif($app['me']['level'] == 3){
							$level = "1";
							$code = "it";
							$id_user = "";
							$id_section = db::lookup("id","section","code","it");
						}
					}
					// echo $id;
					// echo $code;
					// echo $app['me']['id'];
					// echo "Form Pengajuan user telah di buat silahkan Approve Form ini";
					// echo $id_section;
					// echo $link;
					// echo "";
					// echo $level;
					// echo $module;
					// exit;
					db::add_notif($id, $code, $app['me']['id'], "Form Pengajuan user telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);

					$config = db::get_record("configuration","1","1");
					$get_form_user =  db::lookup("name","member","id",$data['created_by']);
					$get_caption =  admlib::$page_active['caption'];
					$sql_email = "SELECT * FROM ". $app['view']["user"] ." where
								  (level = '".$level."' AND id_section = '".$id_section."') AND status = 'active' ORDER BY created_at desc";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
					while($row = db::fetch($rs['sql_email']))
					{
						
						$msg_email .= "<p>Yth. ".$row['name'].",</p>";
						$msg_email .= "Dengan ini kami beritahukan bahwa ada sebuah permohonan yang dibuat di dalam aplikasi ForgiSystem menunggu pernyataan Anda.
			Berikut adalah informasi singkat terkait permohonan dimaksud.";
						$msg_email .= "<table>";
						$msg_email .= "<tr><td>No. Permohonan</td><td>:</td><td><td>".$data['no_pengajuan']."</td></tr>";
						$msg_email .= "<tr><td>Pemohon </td><td>:</td><td><td>".$get_form_user."</td></tr>";
						$msg_email .= "<tr><td>Tanggal Dibuat</td><td>:</td><td><td>".date("d-m-Y",strtotime($data['created_at']))."</td></tr>";
						$msg_email .= "<tr><td>Jenis Permohonan</td><td>:</td><td><td>".$get_caption."</td></tr>";
						$msg_email .= "</table>";
						$msg_email .= "<p>Silakan masuk ke aplikasi <a href='".$app['http']."'>ForgiSystem.cmwi.co.id</a> untuk merespon permohonan tersebut.</p>";
						$msg_email .= "<p>Hormat Kami,</p>";
						$msg_email .= "<p>ForgiSystem</p>";
						$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
						$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
						// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
						if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
							app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
						}
						$msg_email = "";
					}
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				if($app["me"]["level"] >= 4){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_4 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				if($app["me"]["level"] >= 3){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_4 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				if($app["me"]["level"] == 2){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_4 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");

					$config = db::get_record("configuration","1","1");
					$get_form_user =  db::lookup("name","member","id",$data['created_by']);
					$get_caption =  admlib::$page_active['caption'];
					$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
					while($row = db::fetch($rs['sql_email']))
					{
						
						$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
						$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." <b> Ditolak </b>.";
						$msg_email .= "<p>Salam,</p>";
						$msg_email .= "<p>ForgiSystem</p>";
						$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
						$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
						// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
						if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
							app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
						}
						$msg_email = "";
					}

				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}
		}else{
			// echo "disini";
				// if($app["me"]["level"] >= 3){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$approved .= "approve_3 = 'tidak',";
				// 	$status = "rejected";
				// }
				// if($app["me"]["level"] == 2){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$status = "rejected";
				// }
				// // $approved = rtrim($approved,",");
				// $sql = "update ". $app['table'][$module] ."
				// set $approved
				// status_progress = '$status'
				// where id = '$id'";
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "REJ",$id, $p_masa_simpan);
				// }
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	// }

	// if(count($reorder) > 0)
	// {
		// foreach ($reorder as $idval) { $count ++;
		// 	$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
		// 	db::qry($sql);
		// }
		// echo true;
		// exit;
	// }
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : approve
*******************************************************************************/
if ($act == "approve_hrd_3"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	// print_r($_REQUEST);
	// if($type=="approve_2"){

		// if($app["me"]["level"] >= 2){
		if($app["me"]["level"] > 1){
			if($reject !="1"){
				// if($app["me"]["level"] >= 4){
				// 	// approve_4
				// 	$approved .= "approve_2 = 'iya',";
				// 	$approved .= "approve_3 = 'iya',";
				// 	$approved .= "approve_4 = 'iya',";
				// 	$status = "fully";
				// 	$log_approve = "APR_GM";
				// }
				// if($app["me"]["level"] == 3){
				// 	// approve_4
				// 	$approved .= "approve_2 = 'iya',";
				// 	$approved .= "approve_3 = 'iya',";
				// 	$status = "partly";
				// 	$log_approve = "APR_MNG";
				// }
				// if($app["me"]["level"] == 2){
					$approved .= "approve_hrd_3 = 'iya',";
					$log_approve = "APR_HRD_3";
					$status = "fully";
				// }
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id, $p_masa_simpan);
					
					$code="";
					$id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					$link = $app['http'] ."/". $module .".mod";
					/*if($app['me']['level'] == 1){
						$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
						if($get_spv !=""){
							$level = "2";
						}else{
							$level = "3";
						}
					}elseif($app['me']['level'] == 2){
						$level = "3";
					}elseif($app['me']['level'] == 3){*/
						$level = "1";
						// $code = "it";
						// $created_user = db::lookup("created_by",$module,"id",$id);
						$id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form Pengajuan user telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				if($app["me"]["level"] >= 4){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_4 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$approved .= "approve_hrd = 'reject',";
					$approved .= "approve_hrd_3 = 'reject',";
					$status = "rejected";
				}
				if($app["me"]["level"] >= 3){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				if($app["me"]["level"] == 2){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}
		}else{
			// echo "disini";
				// if($app["me"]["level"] >= 3){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$approved .= "approve_3 = 'tidak',";
				// 	$status = "rejected";
				// }
				// if($app["me"]["level"] == 2){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$status = "rejected";
				// }
				// // $approved = rtrim($approved,",");
				// $sql = "update ". $app['table'][$module] ."
				// set $approved
				// status_progress = '$status'
				// where id = '$id'";
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "REJ",$id, $p_masa_simpan);
				// }
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	// }

	// if(count($reorder) > 0)
	// {
		// foreach ($reorder as $idval) { $count ++;
		// 	$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
		// 	db::qry($sql);
		// }
		// echo true;
		// exit;
	// }
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : finish_it
*******************************************************************************/
// if ($act == "finish_it"):
// 	admlib::validate('UPDT');
// 	$module = admlib::$page_active['module'];
// 	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
// 		if($app["me"]["code"] == "it"){
// 			// if($app["me"]["level"] >= 3){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// 	$approved .= "approve_3 = 'iya',";
// 			// }
// 			// if($app["me"]["level"] == 2){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// }
// 			// $approved = rtrim($approved,",");;
// 			if($get_approve_mng=="iya"){
// 				$sql = "update ". $app['table'][$module] ."
// 				set finish_it ='iya',
// 					status_progress= 'progress'
// 				where id = '$id'";
// 				// db::qry($sql);
// 				if(db::qry($sql)){
// 					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
// 				}
// 				msg::set_message('success', app::i18n('approved_msg'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 				exit;
// 			}else{
// 				msg::set_message('error', app::i18n('not_fully'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 			}
// 		}else{
// 			// echo "disini";
// 			msg::set_message('error', app::i18n('not_qualified'));
// 			header("location: " . $app['webmin'] ."/". $module .".mod");
// 		}

// 	echo false;
// 	exit;
// endif;
/*******************************************************************************
* Action : view_file
*******************************************************************************/
if ($act == "view_file"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);

	$get_ext = explode(".", $form['attach']);
	// print_r($get_ext);
	// exit;
	if ($get_ext[1] == "jpg" || $get_ext[1] == "png" || $get_ext[1] == "jpeg" || $get_ext[1] == "bmp" || $get_ext[1] == "gif" || $get_ext[1] == "svg" || $get_ext[1] == "tiff" || $get_ext[1] == "gif") {
		header("location: ".$app['http'] ."/src/assets/".$module."/".$form['attach']);
	}else{
		$data['path'] = $form['attach']; 
		include "dsp_view_file.php";
	}

	exit;
endif;
/*******************************************************************************
* Action : view_file
*******************************************************************************/
if ($act == "download"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);
	$filepath = $app['path']."/src/assets/".$module."/".$form['attach'];
	header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer
        readfile($filepath);
        die();

	exit;
endif;
/*******************************************************************************
* Action : confirm_done
*******************************************************************************/
if ($act == "confirm_done"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_status = db::lookup("status_progress",$module,"id",$id);
			if($get_status=="progress"){
				$sql = "update ". $app['table'][$module] ."
				set confirm_done ='iya',
					status_progress= 'finished'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_finished'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : print
*******************************************************************************/
if ($act == "print"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);
	$section = db::lookup("name","section", "id", $form['id_section']);
	$departement_baru = db::lookup("name","departement", "id", $form['id_departement_new']);
	$departement_lama = db::lookup("name","departement", "id", $form['id_departement_old']);
	$revisi				 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='UPD'");
	$approve_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$approve_it_name	 = db::lookup("name","member", "id", $approve_it_id);
	$approve_mng_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG' order by created_at desc");
	$approve_mng_name	 = db::lookup("name","member", "id", $approve_mng_id);
	$approve_spv_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_SPV' order by created_at desc");
	$approve_spv_name	 = db::lookup("name","member", "id", $approve_spv_id);


	$approve_gm_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_GM' order by created_at desc");
	$approve_gm_name	 = db::lookup("name","member", "id", $approve_gm_id);

	
	$approve_hrd_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_HRD' order by created_at desc");
	$approve_hrd_name	 = db::lookup("name","member", "id", $approve_hrd_id);

	
	$approve_hrd_3_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_HRD_3' order by created_at desc");
	$approve_hrd_3_name	 = db::lookup("name","member", "id", $approve_hrd_3_id);

	$approve_it_3_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$approve_it_3_name	 = db::lookup("name","member", "id", $approve_it_3_id);


	$created_spv_id		 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$created_done_id	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRD' order by created_at desc");
	$config = db::get_record("configuration", "id", 1);
	// $created = db::lookup("created_at","log", "id", $form['id_departement']);
	

	///////////////////
	$revisi				 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='UPD'");
	$created_revisi	 	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='UPD' order by created_at desc");
	
    if (empty($created_revisi)) {
		$created_revisi ="-";
	}
    
	$created_approve_it = db::lookup("created_at","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	///////////////////
	include "dsp_print.php";
	exit;
	// $get_status = db::lookup("status_progress",$module,"id",$id);
			// if($get_status=="progress"){
				// $sql = "update ". $app['table'][$module] ."
				// set confirm_done ='iya',
				// 	status_progress= 'progress'
				// where id = '$id'";
				// // db::qry($sql);
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan);
				// }
				// msg::set_message('success', app::i18n('approved_msg'));
				// header("location: " . $app['webmin'] ."/". $module .".mod");
				// exit;
			// }else{
			// 	msg::set_message('error', app::i18n('not_finished'));
			// 	header("location: " . $app['webmin'] ."/". $module .".mod");
			// }

	// echo false;
	// exit;
endif;
/*******************************************************************************
* Action : approve_it
*******************************************************************************/
if ($act == "approve_it"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
		$data = db::get_record($module, "id", $id);
	if (empty($reject) || $reject !="1") {
		if($app["me"]["code"] == "it"){
			// if($app["me"]["level"] >= 3){
			// 	$approved .= "approve_2 = 'iya',";
			// 	$approved .= "approve_3 = 'iya',";
			// }
			// if($app["me"]["level"] == 2){
			// 	$approved .= "approve_2 = 'iya',";
			// }
			// $approved = rtrim($approved,",");;
			if($get_approve_mng=="iya"){
				$get_mng = db::viewlookup("id","user","id_section='".$app['me']['id_section']."' AND level ='3'");
				$status_progress= 'accepted';
				if(!$get_mng){
					$approve_it_3 = "approve_it_3 ='iya'";
				}
				#$status_progress = 'accepted';
				$sql = "update ". $app['table'][$module] ."
				set approve_it ='iya',
					$approve_it_3
					status_progress= '$status_progress'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
					if($get_mng){
					$code="";
					$id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					$link = $app['http'] ."/". $module .".mod";
					/*if($app['me']['level'] == 1){
						#$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
						if($get_spv !=""){
							$level = "2";
						}else{
							$level = "3";
						}
					}elseif($app['me']['level'] == 2){
						$level = "3";
					}elseif($app['me']['level'] == 3){*/


						$level = "3";
						// $code = "it";
						// $created_user = db::lookup("created_by",$module,"id",$id);
						$id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form Pengajuan user telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);

					$config = db::get_record("configuration","1","1");
					$get_form_user =  db::lookup("name","member","id",$data['created_by']);
					$get_caption =  admlib::$page_active['caption'];
					$sql_email = "SELECT * FROM ". $app['view']["user"] ." where
								  (level = '".$level."' AND id_section = '".$id_section."') AND status = 'active' ORDER BY created_at desc";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
					while($row = db::fetch($rs['sql_email']))
					{
						
						$msg_email .= "<p>Yth. ".$row['name'].",</p>";
						$msg_email .= "Dengan ini kami beritahukan bahwa ada sebuah permohonan yang dibuat di dalam aplikasi ForgiSystem menunggu pernyataan Anda.
			Berikut adalah informasi singkat terkait permohonan dimaksud.";
						$msg_email .= "<table>";
						$msg_email .= "<tr><td>No. Permohonan</td><td>:</td><td><td>".$data['no_pengajuan']."</td></tr>";
						$msg_email .= "<tr><td>Pemohon </td><td>:</td><td><td>".$get_form_user."</td></tr>";
						$msg_email .= "<tr><td>Tanggal Dibuat</td><td>:</td><td><td>".date("d-m-Y",strtotime($data['created_at']))."</td></tr>";
						$msg_email .= "<tr><td>Jenis Permohonan</td><td>:</td><td><td>".$get_caption."</td></tr>";
						$msg_email .= "</table>";
						$msg_email .= "<p>Silakan masuk ke aplikasi <a href='".$app['http']."'>ForgiSystem.cmwi.co.id</a> untuk merespon permohonan tersebut.</p>";
						$msg_email .= "<p>Hormat Kami,</p>";
						$msg_email .= "<p>ForgiSystem</p>";
						$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
						$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
						// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
						if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
						  // $emailErr = "Invalid email format";
							app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
						}
						$msg_email = "";
					}
				      }else{

					$code="";
					$id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					$link = $app['http'] ."/". $module .".mod";
					
					$level = "1";
					// $code = "it";
					$created_user = db::lookup("created_by",$module,"id",$id);
					
					// $id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form recovery data telah di buat silahkan Approve Form ini", "", $link, $created_user, $level, $module);

					$config = db::get_record("configuration","1","1");
					$get_form_user =  db::lookup("name","member","id",$data['created_by']);
					$get_caption =  admlib::$page_active['caption'];
					$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
					while($row = db::fetch($rs['sql_email']))
					{
						
						$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
						$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." telah <b> Selesai </b> diproses.";
						$msg_email .= "<p>Salam,</p>";
						$msg_email .= "<p>ForgiSystem</p>";
						$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
						$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
						// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
						if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
							app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
						}
						$msg_email = "";
					}
				      }
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_fully'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	}else{
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
						set approve_2 = 'reject',
							approve_3 = 'reject',
							approve_it = 'reject',
							confirm_done = 'reject',
							status_progress = 'rejected'
							where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
					
					$code = "";
					$id_section = $app['me']['id_section'];
					$link = $app['http'] ."/". $module .".mod";
					if($app['me']['level'] == 1){
						$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
						if($get_spv !=""){
							$level = "2";
						}else{
							$level = "3";
						}
					}elseif($app['me']['level'] == 2){
						$level = "3";
					}elseif($app['me']['level'] == 3){
						$level = "1";
						$code = "it";
						$id_section = db::lookup("id","section","code","it");
					}
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section");
					db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
					
					$config = db::get_record("configuration","1","1");
					$get_form_user =  db::lookup("name","member","id",$data['created_by']);
					$get_caption =  admlib::$page_active['caption'];
					$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
					while($row = db::fetch($rs['sql_email']))
					{
						
						$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
						$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." <b> Ditolak </b>.";
						$msg_email .= "<p>Salam,</p>";
						$msg_email .= "<p>ForgiSystem</p>";
						$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
						$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
						// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
						if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
							app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
						}
						$msg_email = "";
					}
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
		}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : finish_it
*******************************************************************************/
if ($act == "finish_it"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if($app["me"]["code"] == "it"){
		$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
		if($get_approve_mng=="iya"){
			if ($step == 1):
				// echo $module;
				// echo $id;
				$form = db::get_record($module, "id", $id);
				form::populate($module);
				include "dsp_feedback.php";
				exit;
			endif;
			if ($step == 2):
				form::serialize_form();
				// form::validate('empty', 'p_name,p_nik,p_id_departement,p_username,p_alasan');
				// if (form::is_error()):
				// 	msg::build_msg();
				// 	header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
				// 	exit;
				// endif;
				$p_images = str_replace("%20"," ","$p_images");
				$p_alias = app::slugify($p_title);
				app::mq_encode('p_title,p_content,p_image,p_lang');
				$sql = "update ". $app['table'][$module] ."
						set finish_it ='iya',
							status_progress= 'progress',
							note= '$p_note'
						where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			endif;
		}else{
			msg::set_message('error', app::i18n('not_fully'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	}else{
		msg::set_message('error', app::i18n('not_qualified'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
	}
	exit;
endif;
/*******************************************************************************
* Action : finish_it
*******************************************************************************/
// if ($act == "finish_it"):
// 	admlib::validate('UPDT');
// 	$module = admlib::$page_active['module'];
// 	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
// 		if($app["me"]["code"] == "it"){
// 			// if($app["me"]["level"] >= 3){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// 	$approved .= "approve_3 = 'iya',";
// 			// }
// 			// if($app["me"]["level"] == 2){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// }
// 			// $approved = rtrim($approved,",");;
// 			if($get_approve_mng=="iya"){
// 				$sql = "update ". $app['table'][$module] ."
// 				set finish_it ='iya',
// 					status_progress= 'progress'
// 				where id = '$id'";
// 				// db::qry($sql);
// 				if(db::qry($sql)){
// 					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
// 				}
// 				msg::set_message('success', app::i18n('approved_msg'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 				exit;
// 			}else{
// 				msg::set_message('error', app::i18n('not_fully'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 			}
// 		}else{
// 			// echo "disini";
// 			msg::set_message('error', app::i18n('not_qualified'));
// 			header("location: " . $app['webmin'] ."/". $module .".mod");
// 		}

// 	echo false;
// 	exit;
// endif;
/*******************************************************************************
* Action : approve_it
*******************************************************************************/
// if ($act == "approve_it"):
// 	admlib::validate('UPDT');
// 	$module = admlib::$page_active['module'];
// 		$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
// 			if($app["me"]["code"] == "it"){
// 			// if($app["me"]["level"] >= 3){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// 	$approved .= "approve_3 = 'iya',";
// 			// }
// 			// if($app["me"]["level"] == 2){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// }
// 			// $approved = rtrim($approved,",");;
// 			if($get_approve_mng=="iya"){
// 				$sql = "update ". $app['table'][$module] ."
// 				set approve_it ='iya',
// 					status_progress= 'progress'
// 				where id = '$id'";
// 				// db::qry($sql);
// 				if(db::qry($sql)){
// 					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
// 				}
// 				msg::set_message('success', app::i18n('approved_msg'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 				exit;
// 			}else{
// 				msg::set_message('error', app::i18n('not_fully'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 			}
// 		}else{
// 			// echo "disini";
// 			msg::set_message('error', app::i18n('not_qualified'));
// 			header("location: " . $app['webmin'] ."/". $module .".mod");
// 		}

// 	echo false;
// 	exit;
// endif;

/*******************************************************************************
* Action : get-section;
*******************************************************************************/
if($act == "get-section"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$section = "SELECT id,name as name FROM ". $app['table']['section'] . " WHERE id_departement IN ('". $id_section."') AND status='active'";
	db::query($section, $rs['section'], $nr['section']);
	if($nr['section'] > 0){
		while($row = db::fetch($rs['section'])){
			$callback['section'][] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	echo json_encode($callback);
	exit;
endif;

/*******************************************************************************
* Action : approve_it_3
*******************************************************************************/
if ($act == "approve_it_3"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
		$data = db::get_record($module, "id", $id);
	$get_approve_it = db::lookup("approve_it",$module,"id",$id);
	// echo $get_approve_it;
	// exit;
	if (empty($reject) || $reject !="1") {
		if($app["me"]["code"] == "it" && $app["me"]["level"] == "3"){
			// if($app["me"]["level"] >= 3){
			// 	$approved .= "approve_2 = 'iya',";
			// 	$approved .= "approve_3 = 'iya',";
			// }
			// if($app["me"]["level"] == 2){
			// 	$approved .= "approve_2 = 'iya',";
			// }
			// $approved = rtrim($approved,",");;
			if($get_approve_it=="iya"){
				$sql = "update ". $app['table'][$module] ."
				set approve_it_3 ='iya',
					status_progress= 'progress'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR_MNG_IT",$id, $p_masa_simpan);
					
					$code="";
					$id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					$link = $app['http'] ."/". $module .".mod";
					
					$level = "1";
					// $code = "it";
					$created_user = db::lookup("created_by",$module,"id",$id);
					
					// $id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form recovery data telah di buat silahkan Approve Form ini", "", $link, $created_user, $level, $module);

					$config = db::get_record("configuration","1","1");
					$get_form_user =  db::lookup("name","member","id",$data['created_by']);
					$get_caption =  admlib::$page_active['caption'];
					$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
					while($row = db::fetch($rs['sql_email']))
					{
						
						$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
						$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." telah <b> Selesai </b> diproses.";
						$msg_email .= "<p>Salam,</p>";
						$msg_email .= "<p>ForgiSystem</p>";
						$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
						$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
						// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
						if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
							app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
						}
						$msg_email = "";
					}
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_fully'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	}else{
					// $approved = rtrim($approved,",");
					$sql = "update ". $app['table'][$module] ."
							set approve_2 = 'reject',
								approve_3 = 'reject',
								approve_it = 'reject',
								confirm_done = 'reject',
								status_progress = 'rejected'
								where id = '$id'";
					// db::qry($sql);
					if(db::qry($sql)){
						db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
						
						$code = "";
						$id_section = $app['me']['id_section'];
						$link = $app['http'] ."/". $module .".mod";
						if($app['me']['level'] == 1){
							$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
							if($get_spv !=""){
								$level = "2";
							}else{
								$level = "3";
							}
						}elseif($app['me']['level'] == 2){
							$level = "3";
						}elseif($app['me']['level'] == 3){
							$level = "1";
							$code = "it";
							$id_section = db::lookup("id","section","code","it");
						}
						// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
						// $id_it = db::lookup("name","section");
						db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
						
						$config = db::get_record("configuration","1","1");
						$get_form_user =  db::lookup("name","member","id",$data['created_by']);
						$get_caption =  admlib::$page_active['caption'];
						$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
						db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
						while($row = db::fetch($rs['sql_email']))
						{
							
							$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
							$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." <b> Ditolak </b>.";
							$msg_email .= "<p>Salam,</p>";
							$msg_email .= "<p>ForgiSystem</p>";
							$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
							$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
							// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
							if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
								app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
							}
							$msg_email = "";
						}
					}
					msg::set_message('success', app::i18n('approved_msg'));
					header("location: " . $app['webmin'] ."/". $module .".mod");
					exit;
			}

	echo false;
	exit;
endif;
?>
