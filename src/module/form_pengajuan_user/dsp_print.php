<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM PENGAJUAN USER KOMPUTER</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">
    <section class="sheet padding-10mm">
        <table width="100%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;font-size: 12px;">
            <tbody>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td width="65%">&nbsp;</td>
                </tr>
                <tr>
                    <td>	        	
                        <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                    <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 0px;color:#fff;">	        	
                    FORM PENGAJUAN USER KOMPUTER
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        
                        
                       <!--  <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                            <tbody>
                                <tr align="center">
                                    <td>Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?></td>
                                </tr>
                            </tbody>
                        </table> -->
                        <table cellspacing="1" cellpadding="3" style="font-size: 12px;float:left;" align="left" width="30%">
                            <tbody>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                    <td style="background:#e6e6e6;color:#222;"></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">No Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                                </tr>
                                
                           </tbody>
                        </table>
                        <table cellspacing="1" cellpadding="3" style="font-size: 12px;float:right;" align="right" width="30%">
                            <tbody>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                                </tr>
                           </tbody>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:14px;float:left;" cellpadding="3">
                            <tbody>
                                <tr align="center">
                                    <td colspan="6" style="background:#000;color:#fff;">DIISI OLEH DEPARTEMEN TERKAIT ( MINIMAL SL / SV )</td>
                                </tr>
                                <tr>
                                    <td width="5%"><input disabled class="" id="new_worker" name="p_status_user[]" type="checkbox" value="new_worker" <?php  echo ($form['status_user'] == "new_worker")? "checked":null ?>></td>
                                    <td width="28%">User Baru</td>
                                    <td width="5%"><input disabled class="" id="mutasi_worker" name="p_status_user[]" type="checkbox" value="mutasi_worker"   <?php  echo ($form['status_user'] == "mutasi_worker")? "checked":null ?>></td>
                                    <td width="29%">Pindah Departemen/Mutasi</td>
                                    <td width="5%"><input disabled class="" id="resign_worker" name="p_status_user[]" type="checkbox" value="resign_worker" <?php  echo ($form['status_user'] == "resign_worker")? "checked":null ?>></td>
                                    <td width="28%">Resign/Mengundurkan Diri</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:14px;" cellpadding="3">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td colspan="3"><?= $form['name'] ?></td>
                                </tr>
                                <tr>
                                    <td>Bagian</td>
                                    <td>:</td>
                                    <td colspan="3"><?= $section ?></td>
                                </tr>
                                <tr>
                                    <td width="20%">Departemen</td>
                                    <td width="2%">:</td>
                                    <td width="30%">Baru: <?=($departement_baru !="")? $departement_baru:"-" ?></td>
                                    <td width="18%">&nbsp;</td>
                                    <td width="30%">Lama: <?=($departement_lama !="")? $departement_lama:"-" ?></td>
                                </tr>
                                <tr>
                                    <td>List Permintaan</td>
                                    <td>:</td>
                                    <?php
                                        $pos = strpos(";".$form['list_permintaan'], "pc");
                                        $pos2 = strpos(";".$form['list_permintaan'], "ms_office");
                                        $pos3 = strpos(";".$form['list_permintaan'], "printer");
                                    ?>
                                    <td>
                                        <label>
                                        <input disabled class="" id="pc" name="p_list_permintaan[]" type="checkbox" value="pc" <?php  echo ($pos !="")? "checked":null ?>>
                                        </label> PC/Notebook
                                    </td>
                                    <td>
                                        <label>
                                        <input disabled class="" id="ms_office" name="p_list_permintaan[]" type="checkbox" value="ms_office" <?php  echo ($pos2 !="")? "checked":null ?>>
                                        </label> MSOFFICE
                                    </td>
                                    <td>
                                        <label>
                                        <input disabled class="" id="printer" name="p_list_permintaan[]" type="checkbox" value="printer" <?php  echo ($pos3 !="")? "checked":null ?>> 
                                        </label> Setting Printer
                                    </td>
                                </tr>
                                <tr>
                                    <td>Memperjelas</td>
                                    <td>:</td>
                                    <td colspan="3"><?= $form['memperjelas'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:14px;" cellpadding="3">
                            <tbody>
                                <tr align="center">
                                    <td colspan="6" style="background:#000;color:#fff;">DIISI OLEH HRD DEPARTEMEN</td>
                                </tr>
                                <tr>
                                    <td>NIK</td>
                                    <td>:</td>
                                    <td><?= $form['nik'] ?></td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>:</td>
                                    <td>
                                        <label>
                                        <input disabled id="kontrak" name="p_status_ker" type="radio" value="kontrak" <?php echo ($form['status_ker'] == "kontrak")? "checked":null ?>> Kontrak 
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <label>
                                        <input disabled id="tetap" name="p_status_ker" type="radio" value="tetap" <?php echo ($form['status_ker'] == "tetap")? "checked":null ?>> Tetap 
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <label>
                                        <input disabled id="magang" name="p_status_ker" type="radio" value="magang" <?php echo ($form['status_ker'] == "magang")? "checked":null ?> > Magang 
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Karyawan</td>
                                    <td>:</td>
                                    <td>a. Join Date Kontrak/Magang | <?= date('d/m/Y', strtotime($form['join_date'])) ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>b. End Date Kontrak/Magang | <?= date('d/m/Y', strtotime($form['end_date'])) ?></td>
                                </tr>
                                <tr>
                                    <td valign="top">Denah Lokasi</td>
                                    <td valign="top">:</td>
                                    <td><img style="width: 300px;" src="<?= $app['data_lib'].'/'.$form['attach'] ?>" alt="Gambar Denah Lokasi"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:14px;" cellpadding="3">
                            <tbody>
                                <tr align="center">
                                    <td colspan="5" style="background:#000;color:#fff;">ALUR FORM</td>
                                </tr>
                                <tr>
                                    <td width="30%">
                                        <div style="float:left;border:1px solid #10069f;width:100%;height:30px;font-size:12px;padding:3px 0;text-align:center;border-radius:10px;background:#10069f;color:#fff;">
                                            <span><b>Form Pengajuan<br>Departemen Terkait</b></span>
                                        </div>
                                    </td>
                                    <td width="5%">
                                        <span style="font-size:20px;font-weight:bold;">&#8594;</span>
                                    </td>
                                    <td width="30%">
                                        <div style="float:left;border:1px solid #10069f;width:100%;height:30px;font-size:12px;padding:3px 0;text-align:center;border-radius:10px;background:#10069f;color:#fff;">
                                            <span><b>HRD <br>Departemen</b></span>
                                        </div>
                                    </td>
                                    <td width="5%">
                                        <span style="font-size:20px;font-weight:bold;">&#8594;</span>
                                    </td>
                                    <td width="30%">
                                        <div style="float:left;border:1px solid #10069f;width:100%;height:30px;font-size:12px;padding:3px 0;text-align:center;border-radius:10px;background:#10069f;color:#fff;">
                                            <span><b>IT <br>Departemen</b></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:12px;" cellpadding="5">
                            <tbody>
                                <tr align="right">
                                  <td colspan="9">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</td>
                                </tr>
                                <tr align="center">
                                  <td width="13%" style="background:#000;color:#fff;">Supervisor</td>
                                  <td width="13%" style="background:#000;color:#fff;">Manager</td>
                                  <td width="13%" style="background:#000;color:#fff;">GM</td>
                                  <td width="4%">&nbsp;</td>
                                  <td width="13%" style="background:#000;color:#fff;">HRD</td>
                                  <td width="14%" style="background:#000;color:#fff;">Manager HRD</td>
                                    <td width="4%">&nbsp;</td>
                                  <td width="13%" style="background:#000;color:#fff;">Staff IT</td>
                                  <td width="13%" style="background:#000;color:#fff;">Manager IT</td>
                                </tr>
                                <tr align="center" style="height:75px;">
                                  <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td><span style="font-size:20px;font-weight:bold;">&#8594;</span></td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td><span style="font-size:20px;font-weight:bold;">&#8594;</span></td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr align="center">
                                  <td style="background:#000;color:#fff;"><?= $approve_spv_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_gm_name ?></td>
                                  <td>&nbsp;</td>
                                  <td style="background:#000;color:#fff;"><?= $approve_hrd_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_hrd_3_name ?></td>
                                  <td>&nbsp;</td>
                                  <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_it_3_name ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
</body>
</html>