<?php
admlib::display_block_header();
	$module = admlib::$page_active['module'];
	// admlib::get_component('texteditorlib');
	// datepickerlib
	admlib::get_component('datepickerlib');
	// admlib::get_component('datepickerlib_time_only');
	// admlib::get_component('timepickerlib');
	admlib::get_component('uploadlib');
	// $form_option = ['onsubmit'=>"cek_attach('attach')"];
	// admlib::get_component('formstart');

	admlib::get_component('formstart',
		array(
			"onsubmit"=>"cek_attach('attach')",
		)
	);
	// timepickerlib

	?>
	<style>
			.monthselect{
				color: black;
			}
			.yearselect{
				color: black;
			}
			.tgl_pengajuan{
				width: 50%!important;
			}
		</style> <?php
		if($form['no_pengajuan']){
			admlib::get_component('inputtext',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"validate"=>true,
					"readonly"=>"yes",
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('inputtext',
			array(
				"name"=>"name_p",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				// "style_input"=>"width: 40%;", 
				"validate"=>true,
				"readonly"=>"yes",
				"value"=>$act=="add"?$app['me']['name']:$form['name_p']
			)
		);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"company",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['company'])
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"nik",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['nik'])
		// 	)
		// );
		// id_kategori
		// $rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		// // $rs['jenis']
		// admlib::get_component('select',
		// 	array(
		// 		"name"=>"id_section",
		// 		"value"=>app::ov($form['id_section']),
		// 		"items"=>$rs['section']
		// 	)
		// );
		$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section",
				"readonly"=>"yes",
				// "hide_label"=>"iya",
				// "style"=>"style='width: 45%;margin-left: 473px;position: relative;bottom: 43px;'",
				// "style_input"=>"width: 100%;",
				"validate"=>true,
				"value"=>$act=="add"?$app['me']['id_section']:$form['id_section'],
				"items"=>$rs['section']
			)
		);
		// $rs["kategori"] = db::get_record_select("id, name","kategori","status='active' ORDER BY name ASC");
		$rs["kategori"] = db::get_record_select("id, name","sistem_informasi","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_kategori",
				"validate"=>true,
				"value"=>app::ov($form['id_kategori']),
				"items"=>$rs['kategori']
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"keluhan",
				"validate"=>true,
				"value"=>app::ov($form['keluhan'])
			)
		);
		// echo $form['waktu_kejadian'];
		$p_new_waktu_kejadian="";
		if($form['waktu_kejadian']){
			$p_new_waktu_kejadian =  date('d/m/Y h:i A', strtotime($form['waktu_kejadian']));
		}
		if ($act=="add") {
			$p_new_waktu_kejadian = date('d/m/Y h:i A');
		}
		// admlib::get_component('datetimepicker',
		// 	array(
		// 		"name"=>"tgl_pengajuan",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($p_new_waktu_kejadian)
		// 	)
		// );
		// admlib::get_component('datepicker',
		admlib::get_component('inputtext',
			array(
				"name"=>"tgl_pengajuan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"readonly"=>"yes",
				"style_input"=>"width: 56%;",
				"validate"=>true,
				"value"=>$act=="add"?date('Y-m-d'):$form['date_pengajuan'],
				// "disabled"=>"yes"
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"jam_pengajuan",
				"hide_label"=>"iya",
				"type"=>"time",
				"style"=>'style="width: 29%;margin-left: 55%;position: relative;bottom: 43px;"',
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>$act=="add"?date('H:i'):$form['jam_pengajuan']
			)
		);
/*		admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			"value"=> app::ov($form['attach']),
				"validate"=>true,
			"filemedia"=>true
			)
		);*/
		admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			// "value"=> (!empty($form['attach']) AND file_exists($app['pwebmin'] ."/assets/".$module."/". $form['attach']))?$app['pwebmin'] ."/assets/".$module."/". $form['attach']:null,
			"value"=> (!empty($form['attach']) AND file_exists($app['pwebmin'] ."/assets/".$module."/". $form['attach']))?$app['pwebmin'] ."/assets/".$module."/". $form['attach']:null,
			"link"=>"yes",
			"id_link"=>$id,
			"accept"=>"jpg|png|jpeg|gif|pdf|docx|doc|xlsx|ico|txt",
			// "validate"=>"required",
			// "filemedia"=>true
			)
		);
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"kat_si",
		// 		"datas"=>["js","andon","email","tp"],
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['kat_si'])
		// 	)
		// );
		// admlib::get_component('textarea',
		// 	array(
		// 		"name"=>"saat_ini",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['saat_ini'])
		// 	)
		// );
		// admlib::get_component('textarea',
		// 	array(
		// 		"name"=>"di_inginkan",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['di_inginkan'])
		// 	)
		// );
		// admlib::get_component('textarea',
		// 	array(
		// 		"name"=>"tujuan",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['tujuan'])
		// 	)
		// );
		
		// admlib::get_component('inputupload', 
		// array(
		// 	"name"=>"attach", 
		// 	// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
		// 	"value"=> app::ov($form['attach']),
		// 	"filemedia"=>true
		// 	)
		// );
		// if($form['status_progress'] == "progress" || $form['status_progress'] == "finished"){
		// 		admlib::get_component('textarea',
		// 			array(
		// 				"name"=>"note",
		// 				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 				// "value"=>"",
		// 				"validate"=>true,
		// 				"value"=>app::ov($form['note'])
		// 			)
		// 		);
		// }
		$cek_creator = db::lookup("created_by",$module,"id",$id);
		if(!empty($id)){
			// echo $cek_creator."==".$app['me']["id"];
			if($cek_creator==$app['me']["id"]){
				$cek_approve = db::lookup("approve_it",$module,"id",$id);
				if ($cek_approve =="iya") {
					admlib::get_component('submit',
						array(
							"id"=>(isset($id))?$id:"",
							"no_submit"=>"iya",
							"act"=>$act
						)
					);
				}else{
					admlib::get_component('submit',
						array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
						)
					);
				}
			}else{
				// admlib::get_component('submit',
				admlib::get_component('submit_aprove',
					array(
						"id"=>(isset($id))?$id:"",
						// "no_submit"=>"iya",
						"act"=>"approve"
					)
				);
			}
		}else{
			admlib::get_component('submit',
				array(
					"id"=>(isset($id))?$id:"",
					"act"=>$act
				)
			);
		}
	admlib::get_component('formend');
	?>
	
<script>

/*function cek_attach(id_alert=null){
	// alert(id_alert);
	if( document.getElementById("file_attach").files.length == 0 ){
    	alert("attachment kosong");
    	e.preventDefault();
		return false;
	}
}*/
<?php 
		if($cek_creator==$app['me']["id"] || $act=="add"){ ?>
 $(document).ready(function() {
    //option A
    $("form").submit(function(e){
        // alert('submit intercepted');
        // e.preventDefault(e);
		if( document.getElementById("file_attach").files.length == 0 ){
	    	alert("Attachment tidak boleh kosong");
	    	e.preventDefault();
			return false;
		}
    });
});
<?php } ?>
// $(document).ready(function () {
// 	<?php //if(empty($form['used_sys']) || $form['used_sys'] == "other"){ 
// 		  if($form['used_sys'] != "other"){ 
// 		?>
// 		$("#g_other").hide();
// 	<?php// } ?>
// 		$('input[type=radio][name=p_used_sys]').change(function() {
// 			if (this.value == 'other') {
// 				// alert("Allot Thai Gayo Bhai");
// 				$("#g_other").show();
// 			}
// 			else{
// 				// alert("Transfer Thai Gayo");
//               	$("#g_other").hide();
// 				  $('#other').value(""); 
// 			}
// 		});
// });

</script>
	<?php
admlib::display_block_footer();
?>