<?php
admlib::display_block_header();
	admlib::get_component('timepickerlib');
	admlib::get_component('texteditorlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');

		if($form['no_pengajuan']){
			admlib::get_component('view',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"validate"=>true,
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('view',
			array(
				"name"=>"name_p",
				"validate"=>true,
				"value"=>app::ov($form['name_p'])
			)
		);

		admlib::get_component('view',
			array(
				"name"=>"keluhan",
				"validate"=>true,
				"value"=>app::ov($form['keluhan'])
			)
		);
		$p_new_waktu_kejadian="";
		if($form['waktu_kejadian']){
			$p_new_waktu_kejadian =  date('d/m/Y h:i A', strtotime($form['waktu_kejadian']));
		}
		admlib::get_component('view',
			array(
				"name"=>"tgl_pengajuan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>$form['date_pengajuan']." ".$form['jam_pengajuan']
			)
		);
		// echo "disini ".$form['attach'];
		admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			"value"=> app::ov($form['attach']),
			"filemedia"=>true
			)
		);

		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"kat_si",
		// 		"value"=>$form['kat_si']
		// 	)
		// );
		$rs["kat_si"] = db::get_record_select("id, name","sistem_informasi","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"kat_si",
				"value"=>$form['kat_si'],
				"items"=>$rs['kat_si']
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"penyebab",
				"value"=>$form['penyebab']
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"penanganan",
				"value"=>$form['penanganan']
			)
		);
		// echo $form['tgl_penanganan'];
		// echo $form['tgl_penanganan'];
		if ($form['tgl_penanganan'] != "1970-01-01 07:00:00") {
			$p_new_tgl_penanganan =  date('d/m/Y H:i', strtotime($form['tgl_penanganan']));
		}
		admlib::get_component('datetimepicker',
			array(
				"name"=>"tgl_penanganan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>empty($form['tgl_penanganan'])?"":$p_new_tgl_penanganan
			)
		);
		// echo $form['tgl_selesai'];
		if ($form['tgl_selesai'] != "1970-01-01 07:00:00") {
			// $p_new_tgl_penanganan =  date('d/m/Y H:i', strtotime($form['tgl_penanganan']));
			$p_new_tgl_selesai =  date('d/m/Y H:i', strtotime($form['tgl_selesai']));
		}
		admlib::get_component('datetimepicker',
			array(
				"name"=>"tgl_selesai",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>empty($form['tgl_selesai'])?"":$p_new_tgl_selesai
			)
		);
		// echo $form['down_time'];
		// $p_new_down_time =  date('d/m/Y H:i', strtotime($form['down_time']));
		admlib::get_component('textarea',
			array(
				"name"=>"down_time",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['down_time'])
			)
		);
		admlib::get_component('radio',
			array(
				"name"=>"status_penanganan",
				"datas"=>["permanent","sementara"],
				"validate"=>true,
				"value"=>app::ov($form['status_penanganan'])
			)
		);
		admlib::get_component('radio',
			array(
				"name"=>"hasil",
				"datas"=>["ok","rework"],
				"validate"=>true,
				"value"=>app::ov($form['hasil'])
			)
		);
		if ($form['note_confirm']) {
			admlib::get_component('view',
				array(
					"name"=>"note_confirm",
					// "datas"=>["ok","rework"],
					// "validate"=>true,
					"value"=>app::ov($form['note_confirm'])
				)
			);
		}
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
