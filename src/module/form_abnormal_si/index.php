<?php
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');
$p_masa_simpan = 3;
## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('module'=>'form_abnormal_si','caption'=>ucwords('Informasi abnormal sistem informasi'));
   $ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
else
    $ipaddress = 'UNKNOWN';
// $p_masa_simpan = 0;
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$module = admlib::$page_active['module'];
/*	$total 	= db::lookup('COUNT(id)', $module, '1=1');
	app::set_default($page_size, (isset($all)?$total:10));*/
	if($app['me']['level']==1){
		if($app['me']['code']!="it"){
			$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.id ='".$app['me']['id']."'";
		}else{
			// $where = " AND a.status_progress NOT LIKE 'unverified' AND a.status_progress NOT LIKE 'partly'";
			$where = " AND a.status_progress NOT LIKE 'partly'";
		}
	}
	// if($app['me']['level']==2){
	// 	$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.level <='2'";
	// }
	// if($app['me']['level']>2){
	// 	$where = " AND b.id_section ='".$app['me']['id_section']."'";
	// }
	if($app['me']['level']>2 && $app['me']['code']=="it"){
		/*if($app['me']['code']!="it"){
			// $where = " AND b.id_section ='".$app['me']['id_section']."' AND b.id ='".$app['me']['id']."'";
			$where = " AND b.id_section ='".$app['me']['id_section']."'";
		}else{*/
			// $where = " AND a.status_progress NOT LIKE 'unverified' AND a.status_progress NOT LIKE 'partly'";
			// echo "disini";
			// exit;
			$where = " AND (a.status_progress NOT LIKE 'unverified' AND a.status_progress NOT LIKE 'partly') AND approve_it !='tidak' ";
			// $where = " AND b.id_section ='".$app['me']['id_section']."'";
		// }
	}
	if ($app['me']['code']!="it" && $app['me']['code'] != "-") {
		$where = " AND id_user='".$app['me']['id']."'";
	}
	/*if($app['me']['level']==2){
		$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.level <='2'";
	}*/
	// if($app['me']['level']==3){
	// 	$where = " AND b.id_section ='".$app['me']['id_section']."'";
	// }
	$q = "WHERE 1=1";
	if($ref)
	{
		#$q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		$q .= " AND (a.no_pengajuan LIKE '%". $ref ."%' OR  (b.name LIKE '%". $ref ."%' OR  b.nik LIKE '%". $ref ."%') OR a.status_progress LIKE '%". $ref ."%')";
	}
	// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$module] ." a
	// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	// 	$q ORDER BY a.lang,a.reorder";
	$sql = "SELECT a.*, a.created_by id_created_by, b.name as created_by, c.name as modifyby,b.nik nik, b.id_section section_use FROM ". $app['table'][$module] ." a
	LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['view']['user'] ." c ON (a.updated_by=c.id)
	$q $where OR a.created_by ='".$app['me']['id']."' ORDER BY a.created_at desc ";

	db::query($sql, $rs['test'], $nr['test']);
	$total 	= $nr['test'];
	app::set_default($page_size, (isset($all)?$total:10));



	app::set_navigator($sql, $nav, $page_size, $module .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	// $columns = ["no_pengajuan"];
	// $columns = ["no_pengajuan","yg_mengajukan","tgl_pengajuan_tbl","name"];
	$columns = ["no_pengajuan","yg_mengajukan","tgl_pengajuan_tbl"];
	// progress
	// $columns = array_merge($columns,['print','name','status_progress']);
	$columns = array_merge($columns,['status_progress']);
	if($app['me']['code'] =="it"){
		// $columns = ["feedback",'approve_it',"finish_it"];
		// $columns = array_merge($columns,['approve_it',"approve_it_3"]);
	}else{
		$columns = array_merge($columns,['confirm_done']);
	}
	// $columns = ['approve_2','approve_3','approve_it','confirm_done','name'];
	$numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	if($numlang > 1)
	{
		$columns = array_merge($columns,['lang','duplicate']);
	}
	$columns = array_merge($columns,['print']);
	// $columns = array_merge($columns,["approve"]);
	while($row = db::fetch($rs['row']))
	{
		$row['name'] = $row['name'];
		// $row['yg_mengajukan'] = $row['created_by'];

		$section = db::lookup("name","section","id",$row['section_use']);
		$row['yg_mengajukan'] = $row['nik']." - ".$row['created_by']." - ".$section;
		$row['tgl_pengajuan_tbl'] = $row['created_at'];
		///////////////////
		// $row['approve'] = '<a style="margin-left: 45% !important;" href="'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'
		/* if($row['approve_2']=="tidak"){
			// $row['approve_2'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
			$row['approve_2'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
			$row['approve_3']="";
			// fa-times
		}elseif($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_3']="";
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			
			if($row['approve_3']=="tidak"){
				$row['approve_3'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
			}elseif($row['status_progress']=="rejected"){
				// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				$row['approve_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			}else{
				// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				$row['approve_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}
		} */
		// echo "disini".$row['approve_it_3']." oke";
		if($row['approve_it']=="tidak" ){
			// $row['approve_it'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			if($app['me']['level'] == "1"){
				$row['approve_it'] = '<a style="margin-left: 45% !important;color:red;" href="'.$app['webmin'] .'/'. $module .'.mod&act=approve_it&id='. $row['id'].'"><i class="fa fa-check" aria-hidden="true"></i></a>';
			}else{
				$row['approve_it'] = "";
			}
			$row['finish_it'] = "";
			$row['approve_it_3'] = "";
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			// $row['approve_it'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			if($app['me']['level'] == "1"){
				$row['approve_it'] = '<a style="margin-left: 40% !important;color:green;" href="'.$app['webmin'] .'/'. $module .'.mod&act=approve_it&id='. $row['id'].'"><i class="fa fa-pencil" aria-hidden="true"></i></a><i style="margin-left: 5% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}else{
				$row['approve_it'] = '<a style="margin-left: 40% !important;color:green;"><i class="fa fa-pencil" aria-hidden="true"></i></a><i style="margin-left: 5% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}
			// approve_3_it
			if($row['approve_it_3']!="iya" && $app['me']['level'] == "3"){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				// echo "disini";
				$row['approve_it_3'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it_3&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			}else{
				if($row['approve_it_3']!="iya"){
					$row['approve_it_3'] = '';
				}else{
					$row['approve_it_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				}
			}
		}
//////////////////////////////////////////

if($row['status_progress']=="progress" || $row['status_progress']=="finished"){
	$row['print']='<a  target="_blank" href="'.$app['webmin'] .'/'. $module .'.mod&act=print&id='. $row['id'].'&name='.$row['name'].'" ><i style="margin-left: 30% !important;color:grey;font-size:18px" class="fa fa-print"></i></a>';
/////////////////////////////////////////////////////////////////////
	// if($row['confirm_done']=="iya"){
	// 	if($row['approve_it_2']=="tidak"){
	// 		$row['approve_it_2'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it_2&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
	// 		$row['finish_it'] = "";
	// 		$row['approve_it_3_2'] = "";
	// 	}else{
	// 		$row['approve_it_2'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
	// 		// approve_3_it
	// 		if($row['approve_it_3_2']!="iya"){
	// 			// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
	// 			// echo "disini";
	// 			$row['approve_it_3_2'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it_3_2&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
	// 		}else{
	// 			$row['approve_it_3_2'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
	// 		}
	// 	}
	// }else{
	// 	$row['approve_it_3_2'] = "";
	// 	$row['approve_it_2'] = "";
	// }
/////////////////////////////////////////////////////////////////////
}else{
	// $row['print']='<a href="'.$app['webmin'] .'/'. $module .'.mod&act=print&id='. $row['id'].'&name='.$row['name'].'" ><i style="margin-left: 30% !important;color:grey;font-size:18px" class="fa fa-print"></i></a>';
	$row['print']="";
}
/////////////////////////////////////////

		// $cek_creator = db::lookup("created_by",$module,"id",$id);

		if(($row['status_progress']=="progress" || $row['status_progress']=="finished") && $row['id_created_by'] == $app['me']['id'] ){
			if($row['confirm_done']=="tidak" || empty($row['confirm_done'])){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				//onkonfirm_note
				$cek_log = db::lookup("id","log","act = 'APR_MNG_IT' AND id_form = '".$row['id']."' ");
				
				if (empty($cek_log)) {
					$row['confirm_done'] = "";
				}else{
					// $row['confirm_done'] = '<a style="margin-left: 45% !important;color:orange;" onclick="onkonfirm_note(\''.$app['webmin'] .'/'. $module .'.mod&act=confirm_done&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
					$row['confirm_done'] = '<span style="margin-left: -14% !important;"><a style="margin-left: 45% !important;color:orange;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&act=confirm_done&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a></a><a style="margin-left: 10% !important;color:red;" onclick="onkonfirm_note_reject(\''.$app['webmin'] .'/'. $module .'.mod&act=reject_done&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
				}
			}else{
				// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				$row['confirm_done'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}
		}else{
			$row['confirm_done'] = '';
		}
		// if($row['status_progress']!="iya"){
		// 	// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		// 	$row['status_progress'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
		// }else{
		// 	// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
		// 	$row['status_progress'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
		// }

		// if ($row['approve_2']) {
		// 	# code...
		// }


		if($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_it'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_it_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['confirm_done'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}
		// $row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=edit&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
		if($app['me']['code'] == "it"){
			if ($app['me']['level']=="1") {
				$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=approve_it&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
			}
			if ($app['me']['level']=="3") {
				$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=view&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
			}
		}else{
			$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=edit&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
		}
		if($numlang > 1)
		{
			$row['duplicate'] = '<a href="'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		}
		else {
			$row['duplicate'] = null;
		}
		$row_status_lama = $row['status_progress'];
		$row['status_progress'] = app::getliblang("table_".$row['status_progress']);
		if ($row_status_lama =="rejected") {
			// $row['status_progress'] = db::lookup("id_user","log", "(id_form='".$row['id']."' AND act ='APR') AND module ='".$module."' order by created_at desc");
			$get_reject = db::lookup("id_user","log", "(id_form='".$row['id']."' AND act ='APR') AND module ='".$module."' order by created_at desc");
			$row['status_progress'] = $row['status_progress']."<br>[ ".db::viewlookup("name","user", "id='$get_reject'")." ] ";
		}
		$results[] = $row;
	}
	$option = ['sortable'=>false, 'status'=>true];
	if($app['me']['code']=="it"){
		$option = array_merge($option,['no_edit'=>true]);
	}
	$unpost = 1;
	$unmodify=1;
	// db::remove_notif("",$module,$app['me']['level'],$app['me']['id']);
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
?>
<?php
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		form::populate($module);
		$_SESSION["add_member"] = "";
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		if($app['me']["code"] != "it"){
			// if($p_used_sys == "other"){
			// 	$validate = 'p_name,p_company,p_keperluan,p_waktu,p_satuan,p_used_sys,p_other';
			// }else{
				$validate = 'p_id_kategori,p_keluhan,p_tgl_pengajuan';
			// }
			form::serialize_form();
			form::validate('empty', $validate);
			if (form::is_error()):
				msg::build_msg();
				header("location: ". $module .".mod&act=add&error=1");
				exit;
			endif;


			// print_r($_SESSION);
			// var_dump($_SESSION);
			// exit;
			app::mq_encode('p_name_p,p_company,p_keperluan,p_waktu,p_satuan,p_used_sys,p_attach');
			// $identity = rand(1, 100).date("dmYHis");
			// $p_images = str_replace("%20"," ","$p_images");
			// $sql_lang 	= "SELECT id FROM ". $app['table']['lang'] ." WHERE status='active'";
			$p_alias = app::slugify($p_title);
			// db::query($sql_lang, $rs['lang'], $nr['lang']);
			// while($row 	= db::fetch($rs['lang']))
			// {
				
				if($app['me']['level'] >= 3){
					$approved 		= ",approve_2,approve_3";
					$approved_val 	= ",'iya','iya'";
					// $status_progress 	= ",status_progress";
					// $status_progress_val 	= ",'fully'";
					$log_approve = "APR_MNG";
				}
				if($app['me']['level'] == 2){
					$approved 		= ",approve_2";
					$approved_val 	= ",'iya'";
					// $status_progress 	= ",status_progress";
					// $status_progress_val 	= ",'partly'";
					$log_approve = "APR_SPV";
				}
				// if($app['me']['level'] == 2){
				// 	$approved 		= ",approve_2";
				// 	$approved_val 	= ",'iya'";
				// 	$status_progress 	= ",status_progress";
				// 	$status_progress_val 	= ",'partly'";
				// 	$log_approve = "APR_SPV";
				// }
				$p_attach = str_replace("%20"," ","$p_attach");
				$id 		= rand(1, 1000).date("dmYHis");
				// $p_lang 	= $row['id'];
				$id 		= db::cek_id($id,$module);
				$urut 		= db::lookup("max(reorder)",$module,"1=1");

				$no_urut	= db::lookup("max(no_urut)",$module,"1=1");
				$no_urut	= $no_urut+1;
				$p_no_pengajuan = "GN-006-".date("dmy")."-".$no_urut;
				// $data = db::get_record("feedback","id_project ='$id' AND id_client = '$p_client_id'");
				if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }

				// echo $newDateTime = date('Y-m-d H:i', strtotime($p_tgl_pengajuan));
				// print_r($_REQUEST);
				// echo "<br>";
				// echo $p_tgl_pengajuan;
				// echo "<br>";
				// echo $newDateTime = date('H:i', strtotime($p_tgl_pengajuan));
				// exit;
				// $new_p_tgl_pengajuan = date('Y-m-d H:i', strtotime($p_tgl_pengajuan));


				// $new_p_tgl_pengajuan = date('Y-m-d H:i', strtotime($p_tgl_pengajuan));
				$new_explode_tgl = explode(" ", $p_tgl_pengajuan);
				$new_date_pengajuan = explode("/", $new_explode_tgl[0]);


				// $sql 		= "INSERT INTO ".$app['table'][$module]."
				// 		(id, no_pengajuan, no_urut, id_kategori, attach, keluhan, name_p, id_user, waktu_kejadian, id_section, reorder, created_by, created_at$approved $status_progress) VALUES
				// 		('$id', '$p_no_pengajuan', '$no_urut', '$p_id_kategori', '$p_attach', '$p_keluhan', '$p_name_p', '".$app['me']['id']."', '".$new_date_pengajuan[2]."-".$new_date_pengajuan[1]."-".$new_date_pengajuan[0]." ".$new_explode_tgl[1]."', '$p_id_section', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";


				if ($p_attach_size > 0):
					// save_file_rename
					// $file = file::save_picture('p_attach',$app['pwebmin'] ."/assets/form_abnormal_si/", 'attach');
					$file = file::save_file_rename('p_attach',$app['pwebmin'] ."/assets/form_abnormal_si/", $p_no_pengajuan);
					if (!$err_attach):
						// @unlink($app['pwebmin'] ."/assets/form_abnormal_si/". $data['attach']);
						$data['attach'] = $file;
					endif;
				endif;	

				$sql 		= "INSERT INTO ".$app['table'][$module]."
						(id, no_pengajuan, no_urut, id_kategori, attach, keluhan, name_p, id_user, date_pengajuan, jam_pengajuan, id_section, reorder, created_by, created_at$approved $status_progress) VALUES
						('$id', '$p_no_pengajuan', '$no_urut', '$p_id_kategori', '".$data['attach']."', '$p_keluhan', '$p_name_p', '".$app['me']['id']."', '$p_tgl_pengajuan', '$p_jam_pengajuan', '$p_id_section', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "INS",$id,$p_masa_simpan);
					if($app['me']['level'] > 1){
						db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id,$p_masa_simpan);
					}
					
					$code="";
					$id_section = $app['me']['id_section'];
					$link = $app['http']."/". $module .".mod";
				
					$level = "1";
					$code = "it";
					$id_section = db::lookup("id","section","code","it");
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section");
					db::add_notif($id, $code, $app['me']['id'], "Form Abnormal SI telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);


					$config = db::get_record("configuration","1","1");
					$get_caption =  admlib::$page_active['caption'];
					$sql_email = "SELECT * FROM ". $app['view']["user"] ." where
								  (level = '".$level."' AND id_section = '".$id_section."') AND status = 'active' ORDER BY created_at desc";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
					while($row = db::fetch($rs['sql_email']))
					{
						
						$msg_email .= "<p>Yth. ".$row['name'].",</p>";
						$msg_email .= "Dengan ini kami beritahukan bahwa ada sebuah permohonan yang dibuat di dalam aplikasi ForgiSystem menunggu pernyataan Anda.
			Berikut adalah informasi singkat terkait permohonan dimaksud.";
						$msg_email .= "<table>";
						$msg_email .= "<tr><td>No. Permohonan</td><td>:</td><td><td>".$p_no_pengajuan."</td></tr>";
						$msg_email .= "<tr><td>Pemohon </td><td>:</td><td><td>".$app['me']['name']."</td></tr>";
						$msg_email .= "<tr><td>Tanggal Dibuat</td><td>:</td><td><td>".date("d-m-Y")."</td></tr>";
						$msg_email .= "<tr><td>Jenis Permohonan</td><td>:</td><td><td>".$get_caption."</td></tr>";
						$msg_email .= "</table>";
						$msg_email .= "<p>Silakan masuk ke aplikasi <a href='".$app['http']."'>ForgiSystem.cmwi.co.id</a> untuk merespon permohonan tersebut.</p>";
						$msg_email .= "<p>Hormat Kami,</p>";
						$msg_email .= "<p>ForgiSystem</p>";
						$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
						$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
						// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
						if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
							app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
						}
						$msg_email ="";
					}

				}

				
			// }
			msg::set_message('success', app::i18n('create'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}else{
			msg::set_message('error', app::i18n('it_not_add'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		// echo $module;
		// echo $id;
		$form = db::get_record($module, "id", $id);
		$_SESSION["add_member"] = "";
		// $_SESSION['add_member'][$id] = ['nik'=>$nik, 'name'=>$name, 'id'=>$id, 'id_section'=>$id_section, 'name_section'=>$name_section, 'premission'=>$premission, 'name_section'=>$name_section];
		$rs['add_member'] = db::get_record_select("id_user id, nik, id_user, id_form, premission", "folder_user", "id_form=".$id);
		$_SESSION['add_member'] = [];
		while($row = db::fetch($rs['add_member'])){
			$user_form = db::get_record("member", "id", $row['id_user']);
			$name_section = db::lookup("name","section", "id", $user_form['id_section']);
			$row['name'] = $user_form['name'];
			$row['name_section'] = $name_section;
			$_SESSION['add_member'][$row['id']]= $row;
		}
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");
		// print_r($_SESSION['add_member']);
		form::populate($module);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		// if($p_used_sys == "other"){
		// 	if(empty($p_other)){
		// 		$validate = 'p_name,p_company,p_keperluan,p_waktu,p_satuan,p_used_sys';
		// 		$p_other = "";
		// 	}else{
		// 		$validate = 'p_name,p_company,p_keperluan,p_waktu,p_satuan,p_used_sys,p_other';
		// 	}
		// }else{
		// 	$validate = 'p_name,p_company,p_keperluan,p_waktu,p_satuan,p_used_sys';
		// 	$p_other = "";
		// }
				$validate = 'p_id_kategori,p_keluhan,p_tgl_pengajuan';
		form::serialize_form();
		form::validate('empty', $validate);
		if (form::is_error()):
			msg::build_msg();
			header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;

		// $sql 		= "INSERT INTO ".$app['table'][$module]."
		// (id, name, hp, id_user, id_section, tgl_pengajuan, kat_si, di_inginkan, tujuan, attach, reorder, created_by, created_at$approved $status_progress) VALUES
		// ('$id', '$p_name', '$p_hp', '".$app['me']['id']."', '$p_id_section', '$p_tgl_pengajuan', '$p_kat_si', '$p_di_inginkan', '$p_tujuan', '$p_attach', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";


		$p_attach = str_replace("%20"," ","$p_attach");
		$p_alias = app::slugify($p_title);

		$data = db::get_record($module, "id", $id);
		$edit_before= "";
		$edit_after = "";

		if ($data['name_p'] !=$p_name) {
			$edit_before .= "Pemohon : ".$data['name_p'].";";
			$edit_after .= "Pemohon : ".$p_name_p.";";
		}
		if ($data['keluhan'] !=$p_keluhan) {
			$edit_before .= "keluhan : ".$data['keluhan'].";";
			$edit_after .= "keluhan : ".$p_keluhan.";";
		}
		if ($data['id_kategori'] !=$p_id_kategori) {
			$edit_before .= "kategori : ".$data['id_kategori'].";";
			$edit_after .= "kategori : ".$p_id_kategori.";";
		}
		if ($data['attach'] !=$p_attach) {
			$edit_before .= "attach : ".$data['attach'].";";
			$edit_after .= "attach : ".$p_attach.";";
		}
		if ($data['waktu_kejadian'] !=$p_waktu_kejadian) {
			$edit_before .= "waktu_kejadian : ".$data['waktu_kejadian'].";";
			$edit_after .= "waktu_kejadian : ".$p_waktu_kejadian.";";
		}
		// if ($data['name'] !=$p_name) {
		// 	$edit_before .= $data['name'].";";
		// 	$edit_after .= $p_name.";";
		// }
		$edit_before = rtrim($edit_before,";");
		$edit_after  = rtrim($edit_after,";");
		$new_p_tgl_pengajuan = date('Y-m-d H:i', strtotime($p_tgl_pengajuan));
		$new_explode_tgl = explode(" ", $p_tgl_pengajuan);
		$new_date_pengajuan = explode("/", $new_explode_tgl[0]);
		// $sql 		= "INSERT INTO ".$app['table'][$module]."
		// (id, keluhan, name, id_user, waktu_kejadian, id_section, reorder, created_by, created_at$approved $status_progress) VALUES
		// ('$id', '$p_keluhan', '$p_name', '".$app['me']['id']."', '$new_p_tgl_pengajuan', '$p_id_section', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";

		$data = db::get_record("*",$module, "id", $id);
		if ($p_attach_del){
			@unlink($app['pwebmin'] ."/assets/form_abnormal_si/". $data['attach']);	
			$data['attach'] = null;
		}else{
			if ($p_attach_size > 0):
				// $file = file::save_picture('p_attach',$app['pwebmin'] ."/assets/form_abnormal_si/", 'attach');
				@unlink($app['pwebmin'] ."/assets/form_abnormal_si/". $data['attach']);
				$file = file::save_file_rename('p_attach',$app['pwebmin'] ."/assets/form_abnormal_si/", $data['no_pengajuan']);
				// echo $file;exit;
				if (!$err_attach):
					// @unlink($app['pwebmin'] ."/assets/form_abnormal_si/". $data['attach']);
					$data['attach'] = $file;
				endif;
			endif;	
		}
		app::mq_encode('p_name,p_keluhan,p_id_section,p_waktu_kejadian,p_tujuan,p_attach');
		$sql = "update ". $app['table'][$module] ."
				set name_p = '$p_name_p',
					keluhan = '$p_keluhan',
					id_kategori = '$p_id_kategori',
					attach = '".$data['attach']."',
					date_pengajuan = '$p_tgl_pengajuan',
					jam_pengajuan = '$p_jam_pengajuan',
					id_section = '$p_id_section',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "UPD",$id, $p_masa_simpan,$ipaddress, "",$edit_before, $edit_after);
		}
		msg::set_message('success', app::i18n('modify'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	else:
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table'][$module]."
			SET status = '$statusnya'
			WHERE identity = '$identity'";
	db::qry($sql);
	msg::set_message('success', app::i18n('update'));
	header("location: " . $app['webmin'] ."/". $module .".mod");
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	$module = admlib::$page_active['module'];
	if($step == 1)
	{
		if($id) $items = $id;
		else $items = implode("','", $p_del);
		$sql = "SELECT id, name_p title FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		if(count($p_id) > 1){	
			foreach ($p_id as $value2) {
				$get_file = db::lookup("attach",$module,"id",$value2);
				$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $value2 ."')";
				if(db::qry($sql)){
					@unlink($app['pwebmin'] ."/assets/attach/". $get_file);
					// db::add_log_doc($app['me']['id'],$value2,"DEL_DOC",$ipaddress,"");
					db::ins_call_func("insert_log", $app['me']['id'], $module, "DEL", $value2, $p_masa_simpan,$ipaddress, "");
					$sql2 	= "DELETE FROM ". $app['table']["notif"] ." WHERE `id_form` = '". $value2 ."' AND from_form = '".$module."'";
					db::qry($sql2);
				}
			} 
		}else{
			$get_file = db::lookup("attach",$module,"id",$delid);
			$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $delid ."')";
			// db::qry($sql);
			if(db::qry($sql)){
				@unlink($app['pwebmin'] ."/assets/attach/". $get_file);
				// db::add_log_doc($app['me']['id'],$id,"DEL_DOC",$ipaddress,"");
				db::ins_call_func("insert_log", $app['me']['id'], $module, "DEL",$delid, $p_masa_simpan,$ipaddress, "");
				$sql2 	= "DELETE FROM ". $app['table']["notif"] ." WHERE `id_form` = '". $delid ."' AND from_form = '".$module."'";
				db::qry($sql2);
			}
		}


		/*$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $delid ."')";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "DEL",$delid, $p_masa_simpan,$ipaddress, "");
			$sql2 	= "DELETE FROM ". $app['table']["notif"] ." WHERE `id_form` = '". $delid ."' AND from_form = '".$module."'";
			db::qry($sql2);
		}*/
		msg::set_message('success', app::i18n('delete'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$p_title 		= $row['title'];
			$p_content	 	= $row['content'];
			$p_image 		= $row['image'];

			$urut = db::lookup("max(reorder)", $module, "1=1");
			if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
			$id = rand(1, 100).$urut.date("dmYHis");

			app::mq_encode('p_title,p_content');
			$sql = "INSERT INTO ". $app['table'][$module] ."
				(id, title, content, status, reorder, created_by, created_at) VALUES
				('$id', '$p_title', '$p_content', 'inactive', '$urut', '". $app['me']['id'] ."', now())";
			db::qry($sql);
		}
		msg::set_message('success', app::i18n('create'));
	}
	else
	{
		msg::set_message('error', app::i18n('notfound'));
	}
	header("location: " . $app['webmin'] ."/". $module .".mod");
	exit;
endif;

/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $count ++;
			$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : approve
*******************************************************************************/
if ($act == "approve"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	// print_r($_REQUEST);
	// if($type=="approve_2"){

		// if($app["me"]["level"] >= 2){
		if($app["me"]["level"] > 1){
			if($reject !="1"){
				
				if($app["me"]["level"] >= 3){
					$approved .= "approve_2 = 'iya',";
					$approved .= "approve_3 = 'iya',";
					$status = "fully";
					$log_approve = "APR_MNG";
				}
				if($app["me"]["level"] == 2){
					$approved .= "approve_2 = 'iya',";
					$log_approve = "APR_SPV";
					$status = "partly";
				}
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id, $p_masa_simpan,$ipaddress, "");
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				if($app["me"]["level"] >= 3){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				if($app["me"]["level"] == 2){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
				}
				if ($form['created_by']== $app['me']['id']) {
					db::remove_notif($id,$module,$app['me']['level'],$app['me']['id'],$ipaddress, "");
				}
				db::remove_notif($id,$module,$app['me']['level'],"");
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}
		}else{
			// echo "disini";
				// if($app["me"]["level"] >= 3){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$approved .= "approve_3 = 'tidak',";
				// 	$status = "rejected";
				// }
				// if($app["me"]["level"] == 2){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$status = "rejected";
				// }
				// // $approved = rtrim($approved,",");
				// $sql = "update ". $app['table'][$module] ."
				// set $approved
				// status_progress = '$status'
				// where id = '$id'";
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "REJ",$id, $p_masa_simpan,$ipaddress, "");
				// }
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	// }

	// if(count($reorder) > 0)
	// {
		// foreach ($reorder as $idval) { $count ++;
		// 	$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
		// 	db::qry($sql);
		// }
		// echo true;
		// exit;
	// }
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : confirm_done
*******************************************************************************/
if ($act == "confirm_done"):
	// print_r($_REQUEST);exit;
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_status = db::lookup("status_progress",$module,"id",$id);
	$id_user = db::lookup("id_user",$module,"id",$id);
	// if($id_user==$app['me']['id']){
		if($get_status=="progress"){
			$sql = "update ". $app['table'][$module] ."
			set confirm_done ='iya',
				status_progress= 'finished'
			where id = '$id'";
			// db::qry($sql);
			if(db::qry($sql)){
				db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan,$ipaddress, "");
			}
			msg::set_message('success', app::i18n('approved_msg'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}else{
			msg::set_message('error', app::i18n('not_finished'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	// }else{
	// 	msg::set_message('error', app::i18n('not_finished'));
	// 	header("location: " . $app['webmin'] ."/". $module .".mod");
	// }

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : reject_done
*******************************************************************************/
if ($act == "reject_done"):
	admlib::validate('UPDT');
	// echo "disini ";
	// exit;
	$module = admlib::$page_active['module'];

		// $approved = rtrim($approved,",");
		// echo 
		$sql = "update ". $app['table'][$module] ."
		set approve_it = 'tidak',
			approve_it_3 = 'tidak',
			note_confirm ='$note',
			status_progress = 'fully'	
		where id = '$id'";
		// exit;
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "REJ_DONE",$id, $p_masa_simpan,$ipaddress, "");
		}
		msg::set_message('success', app::i18n('approved_msg'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	msg::set_message('error', app::i18n('not_qualified'));
	header("location: " . $app['webmin'] ."/". $module .".mod");
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : finish_it
*******************************************************************************/
// if ($act == "finish_it"):
// 	admlib::validate('UPDT');
// 	$module = admlib::$page_active['module'];
// 	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
// 		if($app["me"]["code"] == "it"){
// 			// if($app["me"]["level"] >= 3){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// 	$approved .= "approve_3 = 'iya',";
// 			// }
// 			// if($app["me"]["level"] == 2){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// }
// 			// $approved = rtrim($approved,",");;
// 			if($get_approve_mng=="iya"){
// 				$sql = "update ". $app['table'][$module] ."
// 				set finish_it ='iya',
// 					status_progress= 'progress'
// 				where id = '$id'";
// 				// db::qry($sql);
// 				if(db::qry($sql)){
// 					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan,$ipaddress, "");
// 				}
// 				msg::set_message('success', app::i18n('approved_msg'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 				exit;
// 			}else{
// 				msg::set_message('error', app::i18n('not_fully'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 			}
// 		}else{
// 			// echo "disini";
// 			msg::set_message('error', app::i18n('not_qualified'));
// 			header("location: " . $app['webmin'] ."/". $module .".mod");
// 		}

// 	echo false;
// 	exit;
// endif;
/*******************************************************************************
* Action : view_file
*******************************************************************************/
if ($act == "view_file"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);

	$get_ext = explode(".", $form['attach']);
	// print_r($get_ext);
	// exit;
	if ($get_ext[1] == "jpg" || $get_ext[1] == "png" || $get_ext[1] == "jpeg" || $get_ext[1] == "bmp" || $get_ext[1] == "gif" || $get_ext[1] == "svg" || $get_ext[1] == "tiff" || $get_ext[1] == "gif") {
		header("location: ".$app['http'] ."/src/assets/".$module."/".$form['attach']);
	}else{
		$data['path'] = $form['attach']; 
		include "dsp_view_file.php";
	}

	exit;
endif;
/*******************************************************************************
* Action : view_file
*******************************************************************************/
if ($act == "download"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);
	$filepath = $app['path']."/src/assets/".$module."/".$form['attach'];
	header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer
        readfile($filepath);
        die();

	exit;
endif;
/*******************************************************************************
* Action : print
*******************************************************************************/
if ($act == "print"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);
	$departement = db::lookup("name","departement", "id", $form['id_departement']);
	$section = db::lookup("name","section", "id", $form['id_section']);
	$revisi				 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='UPD'");
	$approve_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$approve_it_name	 = db::lookup("name","member", "id", $approve_it_id);
	$approve_mng_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG' order by created_at desc");
	$approve_mng_name	 = db::lookup("name","member", "id", $approve_mng_id);
	$approve_spv_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_SPV' order by created_at desc");
	$approve_spv_name	 = db::lookup("name","member", "id", $approve_spv_id);
	
	$approved_date		 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRD' order by created_at desc");
	// APRD

	// $approve_it		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	// $approve_it_name	 = db::lookup("name","member", "id", $approve_it);

	
	$approve_mng_it		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$approve_mng_it_name	 = db::lookup("name","member", "id", $approve_mng_it);

	$created_spv_id		 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$created_done_id	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRD' order by created_at desc");
	$config = db::get_record("configuration", "id", 1);
	// $created = db::lookup("created_at","log", "id", $form['id_departement']);
	////////////////////////////////////////////////////////////////////////////
	$rs['add_member'] = db::get_record_select("id_user id, nik, id_user, id_form, premission", "folder_user", "id_form=".$id);


	///////////////////
	$revisi			 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='UPD'");
	$created_revisi	 	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='UPD' order by created_at desc");
     if (empty($created_revisi)) {
		$created_revisi ="-";
	}
	$created_approve_it  = db::lookup("created_at","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	///////////////////

		// $_SESSION['add_member'] = [];
		// while($row = db::fetch($rs['add_member'])){
		// 	$user_form = db::get_record("member", "id", $row['id_user']);
		// 	$name_section = db::lookup("name","section", "id", $user_form['id_section']);
		// 	$row['name'] = $user_form['name'];
		// 	$row['name_section'] = $name_section;
		// 	$_SESSION['add_member'][$row['id']]= $row;
		// }
	include "dsp_print.php";
	exit;
	// $get_status = db::lookup("status_progress",$module,"id",$id);
			// if($get_status=="progress"){
				// $sql = "update ". $app['table'][$module] ."
				// set confirm_done ='iya',
				// 	status_progress= 'progress'
				// where id = '$id'";
				// // db::qry($sql);
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan,$ipaddress, "");
				// }
				// msg::set_message('success', app::i18n('approved_msg'));
				// header("location: " . $app['webmin'] ."/". $module .".mod");
				// exit;
			// }else{
			// 	msg::set_message('error', app::i18n('not_finished'));
			// 	header("location: " . $app['webmin'] ."/". $module .".mod");
			// }

	// echo false;
	// exit;
endif;
/*******************************************************************************
* Action : view
*******************************************************************************/
if ($act == "view"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
		$form = db::get_record($module, "id", $id);
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");
		if($app["me"]["code"] == "it"){
			if ($step == 1 || empty($step)):
				// echo $module;
				// echo $id;
				$form = db::get_record($module, "id", $id);
				form::populate($module);
				include "dsp_view.php";
				exit;
			endif;
			if ($step == 2):
				form::serialize_form();
				// form::validate('empty', 'p_name,p_nik,p_id_departement,p_username,p_alasan');
				// if (form::is_error()):
				// 	msg::build_msg();
				// 	header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
				// 	exit;
				// endif;
				
				// $new_p_tgl_penanganan = date('Y-m-d H:i', strtotime($p_tgl_penanganan));
				$new_p_tgl_penanganan = explode(" ", $p_tgl_penanganan);
				// print_r($new_p_tgl_penanganan);
				// $new_p_tgl_penanganan_date = date_format('Y-m-d', strtotime($new_p_tgl_penanganan[0]));
				// $new_p_tgl_penanganan[0] = str_replace("/","-","Hello world!");
				$new_p_tgl_penanganan_date = explode("/", $new_p_tgl_penanganan[0]);
				// $new_p_tgl_penanganan_date = date_format($new_p_tgl_penanganan[0],"Y-m-d");
				// $new_p_tgl_selesai = date('Y-m-d H:i', strtotime($p_tgl_selesai));

				$new_p_tgl_selesai = explode(" ", $p_tgl_selesai);
				$new_p_tgl_selesai_date = explode("/", $new_p_tgl_selesai[0]);


				// $new_p_tgl_selesai = date('Y-m-d H:i', strtotime($p_tgl_selesai));
				// $new_p_down_time = date('Y-m-d H:i', strtotime($p_down_time));


				$p_images = str_replace("%20"," ","$p_images");
				$p_alias = app::slugify($p_title);
				app::mq_encode('p_title,p_content,p_image,p_lang');
				// print_r($_REQUEST);
				// echo "<br><br>";
//accepted
							//status_progress= 'progress'
				$sql = "update ". $app['table'][$module] ."
						set kat_si ='$p_kat_si',
							penyebab ='$p_penyebab',
							penanganan ='$p_penanganan',
							tgl_penanganan ='".$new_p_tgl_penanganan_date[2]."-".$new_p_tgl_penanganan_date[1]."-".$new_p_tgl_penanganan_date[0]." ".$new_p_tgl_penanganan[1]."',
							tgl_selesai ='".$new_p_tgl_selesai_date[2]."-".$new_p_tgl_selesai_date[1]."-".$new_p_tgl_selesai_date[0]." ".$new_p_tgl_selesai[1]."',
							down_time ='$p_down_time',
							status_penanganan = '$p_status_penanganan',
							hasil = '$p_hasil',
							approve_it = 'iya',
							status_progress= 'progress'
						where id = '$id'";
						 //exit;
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan,$ipaddress, "");
					// if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "UPD_IT",$id, $p_masa_simpan,$ipaddress, "");
					// }
					
					$code="";
					$id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					$link = $app['http']."/". $module .".mod";
					
						$level = "3";
						$code = "it";
						// $created_user = db::lookup("created_by",$module,"id",$id);
						
						$id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form Abnormal SI telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			endif;
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : approve_it
*******************************************************************************/
if ($act == "approve_it"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
		$form = db::get_record($module, "id", $id);
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");
		if($app["me"]["code"] == "it"){
			if ($step == 1 || empty($step)):
				// echo $module;
				// echo $id;
				$form = db::get_record($module, "id", $id);
				form::populate($module);
				include "dsp_feedback.php";
				exit;
			endif;
			if ($step == 2):
				$data = db::get_record($module, "id", $id);
				if (empty($reject) || $reject !="1") {
					form::serialize_form();
					// form::validate('empty', 'p_name,p_nik,p_id_departement,p_username,p_alasan');
					// if (form::is_error()):
					// 	msg::build_msg();
					// 	header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
					// 	exit;
					// endif;
					
					// $new_p_tgl_penanganan = date('Y-m-d H:i', strtotime($p_tgl_penanganan));
					$new_p_tgl_penanganan = explode(" ", $p_tgl_penanganan);
					// print_r($new_p_tgl_penanganan);
					// $new_p_tgl_penanganan_date = date_format('Y-m-d', strtotime($new_p_tgl_penanganan[0]));
					// $new_p_tgl_penanganan[0] = str_replace("/","-","Hello world!");
					$new_p_tgl_penanganan_date = explode("/", $new_p_tgl_penanganan[0]);
					// $new_p_tgl_penanganan_date = date_format($new_p_tgl_penanganan[0],"Y-m-d");
					// $new_p_tgl_selesai = date('Y-m-d H:i', strtotime($p_tgl_selesai));

					$new_p_tgl_selesai = explode(" ", $p_tgl_selesai);
					$new_p_tgl_selesai_date = explode("/", $new_p_tgl_selesai[0]);


					// $new_p_tgl_selesai = date('Y-m-d H:i', strtotime($p_tgl_selesai));
					// $new_p_down_time = date('Y-m-d H:i', strtotime($p_down_time));


					$p_images = str_replace("%20"," ","$p_images");
					$p_alias = app::slugify($p_title);
					app::mq_encode('p_title,p_content,p_image,p_lang');
					// print_r($_REQUEST);
					// echo "<br><br>";
//accepted
								//status_progress= 'progress'
					$sql = "update ". $app['table'][$module] ."
							set kat_si ='$p_kat_si',
								penyebab ='$p_penyebab',
								penanganan ='$p_penanganan',
								tgl_penanganan ='".$new_p_tgl_penanganan_date[2]."-".$new_p_tgl_penanganan_date[1]."-".$new_p_tgl_penanganan_date[0]." ".$new_p_tgl_penanganan[1]."',
								tgl_selesai ='".$new_p_tgl_selesai_date[2]."-".$new_p_tgl_selesai_date[1]."-".$new_p_tgl_selesai_date[0]." ".$new_p_tgl_selesai[1]."',
								down_time ='$p_down_time',
								status_penanganan = '$p_status_penanganan',
								hasil = '$p_hasil',
								approve_it = 'iya',
								status_progress= 'accepted'
							where id = '$id'";
							// exit;
					// db::qry($sql);
					if(db::qry($sql)){
						db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan,$ipaddress, "");
						// if(db::qry($sql)){
						db::ins_call_func("insert_log", $app['me']['id'], $module, "UPD_IT",$id, $p_masa_simpan,$ipaddress, "");
						// }
						
						$code="";
						$id_section = $app['me']['id_section'];
						$created_user = $app['me']['id'];
						$link = $app['http']."/". $module .".mod";
						
							$level = "3";
							$code = "it";
							// $created_user = db::lookup("created_by",$module,"id",$id);
							
							$id_section = db::lookup("id","section","code","it");
						// }
						// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
						// $id_it = db::lookup("name","section","code","it");

						$get_notif_id = db::lookup("id","notif","from_form='".$module."' AND id_form = '".$id."' AND code='".$code."' AND level = '".$level."'");
						if(empty($get_notif_id)){
							db::add_notif($id, $code, $app['me']['id'], "Form Abnormal SI telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
						}

						$config = db::get_record("configuration","1","1");
						$get_form_user =  db::lookup("name","member","id",$data['created_by']);
						$get_caption =  admlib::$page_active['caption'];
						$sql_email = "SELECT * FROM ". $app['view']["user"] ." where
									  (level = '".$level."' AND id_section = '".$id_section."') AND status = 'active' ORDER BY created_at desc";
						db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
						while($row = db::fetch($rs['sql_email']))
						{
							
							$msg_email .= "<p>Yth. ".$row['name'].",</p>";
							$msg_email .= "Dengan ini kami beritahukan bahwa ada sebuah permohonan yang dibuat di dalam aplikasi ForgiSystem menunggu pernyataan Anda.
				Berikut adalah informasi singkat terkait permohonan dimaksud.";
							$msg_email .= "<table>";
							$msg_email .= "<tr><td>No. Permohonan</td><td>:</td><td><td>".$data['no_pengajuan']."</td></tr>";
							$msg_email .= "<tr><td>Pemohon </td><td>:</td><td><td>".$get_form_user."</td></tr>";
							$msg_email .= "<tr><td>Tanggal Dibuat</td><td>:</td><td><td>".date("d-m-Y",strtotime($data['created_at']))."</td></tr>";
							$msg_email .= "<tr><td>Jenis Permohonan</td><td>:</td><td><td>".$get_caption."</td></tr>";
							$msg_email .= "</table>";
							$msg_email .= "<p>Silakan masuk ke aplikasi <a href='".$app['http']."'>ForgiSystem.cmwi.co.id</a> untuk merespon permohonan tersebut.</p>";
							$msg_email .= "<p>Hormat Kami,</p>";
							$msg_email .= "<p>ForgiSystem</p>";
							$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
							$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
							// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
							if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
							  // $emailErr = "Invalid email format";
								app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
							}
							$msg_email ="";
						}
					}
					msg::set_message('success', app::i18n('approved_msg'));
					header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
		}else{
			// $approved = rtrim($approved,",");
			$sql = "update ". $app['table'][$module] ."
					set approve_2 = 'reject',
						approve_3 = 'reject',
						approve_it = 'reject',
						approve_it_3 = 'reject',
						confirm_done = 'reject',
						status_progress = 'rejected'
						where id = '$id'";
			// db::qry($sql);
			if(db::qry($sql)){
				db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
				
				$code = "";
				$id_section = $app['me']['id_section'];
				$link = $app['http'] ."/". $module .".mod";
				if($app['me']['level'] == 1){
					$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
					if($get_spv !=""){
						$level = "2";
					}else{
						$level = "3";
					}
				}elseif($app['me']['level'] == 2){
					$level = "3";
				}elseif($app['me']['level'] == 3){
					$level = "1";
					$code = "it";
					$id_section = db::lookup("id","section","code","it");
				}
				// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
				// $id_it = db::lookup("name","section");
				db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
				
				$config = db::get_record("configuration","1","1");
				$get_form_user =  db::lookup("name","member","id",$data['created_by']);
				$get_caption =  admlib::$page_active['caption'];
				$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
				db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
				while($row = db::fetch($rs['sql_email']))
				{
					
					$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
					$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." <b> Ditolak</b>.";
					$msg_email .= "<p>Salam,</p>";
					$msg_email .= "<p>ForgiSystem</p>";
					$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
					$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
					// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
					if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
						app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
					}
					$msg_email ="";
				}
			}
			msg::set_message('success', app::i18n('approved_msg'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}
			endif;
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}

	echo false;
	exit;
endif;
//approve_it_2
/*******************************************************************************
* Action : approve_it_2
*******************************************************************************/
if ($act == "approve_it_2"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
		if($app["me"]["code"] == "it"){
			// if($app["me"]["level"] >= 3){
			// 	$approved .= "approve_2 = 'iya',";
			// 	$approved .= "approve_3 = 'iya',";
			// }
			// if($app["me"]["level"] == 2){
			// 	$approved .= "approve_2 = 'iya',";
			// }
			// $approved = rtrim($approved,",");;
			if($get_approve_mng=="iya"){
				$sql = "update ". $app['table'][$module] ."
				set approve_it_2 ='iya'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan,$ipaddress, "");
					
					$code="";
					$id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					$link = $app['http']."/". $module .".mod";
					
						$level = "3";
						$code = "it";
						// $created_user = db::lookup("created_by",$module,"id",$id);
						
						$id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form Abnormal SI telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_fully'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : feedback
*******************************************************************************/
if ($act == "feedback"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if (empty($reject) || $reject !="1") {
		if($app["me"]["code"] == "it"){
			// $get_approve_mng = db::lookup("approve_3",$module,"id",$id);
			// if($get_approve_mng=="iya"){
				if ($step == 1 || empty($step)):
					// echo $module;
					// echo $id;
					$form = db::get_record($module, "id", $id);
					form::populate($module);
					include "dsp_feedback.php";
					exit;
				endif;
				if ($step == 2):
					form::serialize_form();
					// form::validate('empty', 'p_name,p_nik,p_id_departement,p_username,p_alasan');
					// if (form::is_error()):
					// 	msg::build_msg();
					// 	header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
					// 	exit;
					// endif;
					$p_images = str_replace("%20"," ","$p_images");
					$p_alias = app::slugify($p_title);
					app::mq_encode('p_title,p_content,p_image,p_lang');
					$sql = "update ". $app['table'][$module] ."
							set finish_it ='iya',
								status_progress= 'progress',
								note= '$p_note'
							where id = '$id'";
					// db::qry($sql);
					if(db::qry($sql)){
						db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan,$ipaddress, "");
						
						$code="";
						$id_section = $app['me']['id_section'];
						$created_user = $app['me']['id'];
						$link = $app['http']."/". $module .".mod";
						
						$level = "1";
						// $code = "it";
						$created_user = db::lookup("created_by",$module,"id",$id);
						
						$id_section = db::lookup("id","section","code","it");
						// }
						// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
						// $id_it = db::lookup("name","section","code","it");
						db::add_notif($id, $code, $app['me']['id'], "Form Abnormal SI telah di buat silahkan Approve Form ini", "", $link, $created_user, $level, $module);
					}
					msg::set_message('success', app::i18n('approved_msg'));
					header("location: " . $app['webmin'] ."/". $module .".mod");
					exit;
				endif;
			// }else{
			// 	msg::set_message('error', app::i18n('not_fully'));
			// 	header("location: " . $app['webmin'] ."/". $module .".mod");
			// }
		}else{
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	}else{
					// $approved = rtrim($approved,",");
					$sql = "update ". $app['table'][$module] ."
							set approve_2 = 'reject',
								approve_3 = 'reject',
								approve_it = 'reject',
								confirm_done = 'reject',
								status_progress = 'rejected'
								where id = '$id'";
					// db::qry($sql);
					if(db::qry($sql)){
						db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
						
						$code = "";
						$id_section = $app['me']['id_section'];
						$link = $app['http'] ."/". $module .".mod";
						if($app['me']['level'] == 1){
							$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
							if($get_spv !=""){
								$level = "2";
							}else{
								$level = "3";
							}
						}elseif($app['me']['level'] == 2){
							$level = "3";
						}elseif($app['me']['level'] == 3){
							$level = "1";
							$code = "it";
							$id_section = db::lookup("id","section","code","it");
						}
						// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
						// $id_it = db::lookup("name","section");
						db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
					}
					msg::set_message('success', app::i18n('approved_msg'));
					header("location: " . $app['webmin'] ."/". $module .".mod");
					exit;
			}
	exit;
endif;
// approve_it_3
/*******************************************************************************
* Action : approve_it_3
*******************************************************************************/
if ($act == "approve_it_3"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_it = db::lookup("approve_it",$module,"id",$id);
		$data = db::get_record($module, "id", $id);
	// echo $get_approve_it;
	// exit;
		if (empty($reject) || $reject !="1") {
		if($app["me"]["code"] == "it" && $app["me"]["level"] == "3"){
			// if($app["me"]["level"] >= 3){
			// 	$approved .= "approve_2 = 'iya',";
			// 	$approved .= "approve_3 = 'iya',";
			// }
			// if($app["me"]["level"] == 2){
			// 	$approved .= "approve_2 = 'iya',";
			// }
			// $approved = rtrim($approved,",");;
			if($get_approve_it=="iya"){
				$sql = "update ". $app['table'][$module] ."
				set approve_it_3 ='iya',
					status_progress= 'progress'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR_MNG_IT",$id, $p_masa_simpan,$ipaddress, "");
					
					$code="";
					$id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					$link = $app['http']."/". $module .".mod";
					
						$level = "1";
						// $code = "it";
						$created_user = db::lookup("created_by",$module,"id",$id);
						
						// $id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form Abnormal SI telah di buat silahkan Approve Form ini", "", $link, $created_user, $level, $module);

						$config = db::get_record("configuration","1","1");
						$get_form_user =  db::lookup("name","member","id",$data['created_by']);
						$get_caption =  admlib::$page_active['caption'];
						$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
						db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
						while($row = db::fetch($rs['sql_email']))
						{
							
							$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
							$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." telah <b> Selesai </b> diproses.";
							$msg_email .= "<p>Salam,</p>";
							$msg_email .= "<p>ForgiSystem</p>";
							$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
							$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
							// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
							if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
								app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
							}
							$msg_email ="";
						}
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_fully'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	}else{
					// $approved = rtrim($approved,",");
					$sql = "update ". $app['table'][$module] ."
							set approve_2 = 'reject',
								approve_3 = 'reject',
								approve_it = 'reject',
								confirm_done = 'reject',
								status_progress = 'rejected'
								where id = '$id'";
					// db::qry($sql);
					if(db::qry($sql)){
						db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
						
						$code = "";
						$id_section = $app['me']['id_section'];
						$link = $app['http'] ."/". $module .".mod";
						if($app['me']['level'] == 1){
							$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
							if($get_spv !=""){
								$level = "2";
							}else{
								$level = "3";
							}
						}elseif($app['me']['level'] == 2){
							$level = "3";
						}elseif($app['me']['level'] == 3){
							$level = "1";
							$code = "it";
							$id_section = db::lookup("id","section","code","it");
						}
						// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
						// $id_it = db::lookup("name","section");
						db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);

						$config = db::get_record("configuration","1","1");
						$get_form_user =  db::lookup("name","member","id",$data['created_by']);
						$get_caption =  admlib::$page_active['caption'];
						$sql_email = "SELECT * FROM ". $app['view']["user"] ." where id = '".$data['created_by']."' ORDER BY created_at desc";
						db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
						while($row = db::fetch($rs['sql_email']))
						{
							
							$msg_email .= "<p>Dear . ".$get_form_user.",</p>";
							$msg_email .= "Kami memberi tahu Anda bahwa permintaan Anda bernomor ".$data['no_pengajuan']." <b> Ditolak</b>.";
							$msg_email .= "<p>Salam,</p>";
							$msg_email .= "<p>ForgiSystem</p>";
							$msg_email .= "<p><img src='".$app['http'] .'/src/assets/files/'. $config['logo']."'> </p>";
							$msg_email .= "<p>#email ini tidak memerlukan balasan </p>";
							// if (!filter_var($row['email'], FILTER_VALIDATE_EMAIL)) {
							if (stristr($row['email'],"@") OR stristr($row['email'],".")) {
								app::sendmail2($row['email'], $app['mail_subject'], $msg_email);
							}
							$msg_email ="";
						}
					}
					msg::set_message('success', app::i18n('approved_msg'));
					header("location: " . $app['webmin'] ."/". $module .".mod");
					exit;
			}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : approve_it_3_2
*******************************************************************************/
if ($act == "approve_it_3_2"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_it = db::lookup("approve_it",$module,"id",$id);
	// echo $get_approve_it;
	// exit;
		if($app["me"]["code"] == "it" && $app["me"]["level"] == "3"){
			// if($app["me"]["level"] >= 3){
			// 	$approved .= "approve_2 = 'iya',";
			// 	$approved .= "approve_3 = 'iya',";
			// }
			// if($app["me"]["level"] == 2){
			// 	$approved .= "approve_2 = 'iya',";
			// }
			// $approved = rtrim($approved,",");;
			if($get_approve_it=="iya"){
				$sql = "update ". $app['table'][$module] ."
				set approve_it_3_2 ='iya'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR_MNG_IT",$id, $p_masa_simpan,$ipaddress, "");
					$code="";
					$id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					$link = $app['http']."/". $module .".mod";
					
						$level = "1";
						// $code = "it";
						$created_user = db::lookup("created_by",$module,"id",$id);
						
						// $id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form Abnormal SI telah di buat silahkan Approve Form ini", "", $link, $created_user, $level, $module);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_fully'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : get-member;
*******************************************************************************/
if($act == "get-member"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$member = "SELECT id,id_section,nik,name FROM ". $app['table']['member'] . " WHERE id IN ('". $id."') AND status='active' limit 1";
	db::query($member, $rs['member'], $nr['member']);
	if($nr['member'] > 0){
		while($row = db::fetch($rs['member'])){
			$callback['member'] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	// $callback["sql"] = $member;
	echo json_encode($callback);
	exit;
endif;
?>
