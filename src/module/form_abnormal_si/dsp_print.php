<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM ABNORMAL SI</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">
    <section class="sheet padding-10mm">
        <table width="100%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;font-size: 12px;">
            <tbody>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td width="65%">&nbsp;</td>
                </tr>
                <tr>
                    <td>	        	
                        <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                    <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 0px;color:#fff;">	        	
                    FORM INFORMASI ABNORMAL <br>
                        SISTEM INFORMASI
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" style="font-size:14px;float:left;" cellpadding="6" cellspacing="0" width="70%">
                            <tbody>
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td width="25%">Pemohon</td>
                                    <td width="2%">:</td>
                                    <td width="73%"><?php 
                                    $Section = db::lookup("name","section","id",$form['id_section']);
                                    $nik = db::lookup("nik","member","id",$form['created_by']);
                                    echo $nik." - ".$form['name_p']." - ".$section; ?></td>
                                </tr>
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td valign="top">Keluhaan</td>
                                    <td valign="top">:</td>
                                    <td><?=  $form['keluhan'] ?></td>
                                </tr>
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td valign="top">Tanggal & jam kejadian</td>
                                    <td valign="top">:</td>
                                    <td><?php 
                                    $p_new_waktu_kejadian =  date('d-m-Y h:i A', strtotime($form['waktu_kejadian'])); ?>
                                    <?=  date('d-m-Y', strtotime($form['date_pengajuan']))." | ".date('h:i A', strtotime($form['jam_pengajuan'])) ?>
                                    </td>
                                </tr>
                                <tr align="center" style="background:#10069f;color:#fff;">
                                    <td colspan="3">DIISI OLEH USER</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                            <tbody>
                                <tr align="center">
                                    <td>Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?></td>
                                </tr>
                            </tbody>
                        </table> -->
                        <table cellspacing="1" cellpadding="3" style="font-size: 12px;" align="right" width="30%">
                        <tbody>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $form['no_pengajuan'] ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Revisi</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                            </tr>
                       </tbody>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="100%" style="font-size:14px;" cellpadding="10" cellspacing="0">
                            <tbody>
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td>Kategori SI</td>
                                    <td>:</td>
                                    <td colspan="7"><?php 
                                    $kategori = db::lookup("name","sistem_informasi","id",$form['id_kategori']);
                                    echo $kategori; ?></td>
                                </tr>
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td>Analisa Penyebab</td>
                                    <td>:</td>
                                    <td colspan="7"><?=  $form['penyebab'] ?></td>
                                </tr>
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td>Penanganan</td>
                                    <td>:</td>
                                    <td colspan="7"><?=  $form['penanganan'] ?></td>
                                </tr>
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td width="22%">Tgl  penanganan</td>
                                    <td width="1%">:</td>
                                    <td width="15%"><u><?php 
                                    $p_new_tgl_penanganan =  date('d-m-Y h:i A', strtotime($form['tgl_penanganan']));
                                    echo $p_new_tgl_penanganan ?></u></td>
                                    <td width="15%">Jam Selesai</td>
                                    <td width="1%">:</td>
                                    <td width="15%"><u><?php 
                                    $p_new_tgl_selesai =  date('d-m-Y h:i A', strtotime($form['tgl_selesai']));
                                    echo $p_new_tgl_selesai ?></u></td>
                                    <td width="15%">Down Time</td>
                                    <td width="1%">:</td>
                                    <td width="15%"><u><?php echo ucwords($form['down_time']) ?></u></td>
                                </tr> 
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td>Status Penanganan</td>
                                    <td>:</td>
                                    <td colspan="7">
                                    <input type="radio" id="baru" name="p_status_penanganan" value="permanent" <?= ($form['status_penanganan']=="permanent"?"checked":"") ?> disabled>&nbsp;
                                    <label for="baru">Permanent</label>&nbsp;
                                    <input type="radio" id="ada" name="p_status_penanganan" value="sementara" <?= ($form['status_penanganan']=="sementara"?"checked":($form['status_penanganan']!="permanent"?"checked":"")) ?> disabled>&nbsp;
                                    <label for="ada">Sementara</label></td>
                                </tr>
                                <tr style="background:#e6e6e6;color:#222;">
                                    <td>Hasil</td>
                                    <td>:</td>
                                    <td colspan="7">
                                    <input type="radio" id="baru" name="p_rework" value="js" <?= ($form['hasil']=="ok"?"checked":"") ?> disabled>&nbsp;
                                    <label for="baru">Ok</label>&nbsp;
                                    <input type="radio" id="Rework" name="p_rework" value=" ework" <?= ($form['hasil']=="rework"?"checked":"") ?> disabled>&nbsp;
                                    <label for="Rework">Rework</label></td>
                                </tr>
                                <tr align="center" style="background:#10069f;color:#fff;">
                                    <td colspan="9">DIISI OLEH IT</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr align="right">
                    <td colspan="2">
                        Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <table border="0" style="font-size:12px;" cellpadding="5" width="100%">
                            <tbody>
                                <tr style="text-align: center;">
                                  <td width="25%" style="background:#000;color:#fff;">Manager IT</td>
                                  <td width="25%" style="background:#000;color:#fff;">Staff IT</td>
                                  <td width="25%"></td>
                                  <td width="125%" style="background:#000;color:#fff;">Pemohon</td>
                                </tr>
                               <tr align="center" style="height:75px;">
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td><span style="font-size:20px;font-weight:bold;">&#10229;</span></td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr style="text-align: center;">
                                  <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                  <td style="border: 0px;"></td>
                                  <td style="background:#000;color:#fff;"><?= $form['name_p'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                
            </tbody>
        </table>
    </section>
</body>
</html>