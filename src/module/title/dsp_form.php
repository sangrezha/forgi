<?php
admlib::display_block_header();
admlib::get_component('texteditorlib');
admlib::get_component('uploadlib');
	admlib::get_component('formstart');
			// admlib::get_component('inputtext',
			// 	array(
			// 		"name"=>"code",
			// 		"value"=>app::ov($form['code']),
			// 		"validate"=>"required"
			// 	)
			// );
			admlib::get_component('inputtext',
				array(
					"name"=>"name",
					"value"=>app::ov($form['name']),
					"validate"=>"required"
				)
			);
			admlib::get_component('select',
				array(
					"name"=>"level",
					"value"=>app::ov($form['level']),
					// "items"=>[["id"=>1,"name"=>"1 - staff"],["id"=>2,"name"=>"2 - supervisor"],["id"=>3,"name"=>"3 - Manager"],["id"=>4,"name"=>"4 - General Manager"],["id"=>5,"name"=>"5 - Direksi"],["id"=>6,"name"=>6],["id"=>7,"name"=>7]],
					"items"=>[["id"=>1,"name"=>"1 - staff"],["id"=>2,"name"=>"2 - supervisor"],["id"=>3,"name"=>"3 - Manager"],["id"=>4,"name"=>"4 - General Manager"],["id"=>5,"name"=>"5 - Direksi"]],
					"validate"=>"required"
				)
			);
			$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
			admlib::get_component('select',
				array(
					"name"=>"id_section",
					"value"=>app::ov($form['id_section']),
					"items"=>$rs['section']
				)
			);
			// admlib::get_component('inputupload', 
			// 	array(
			// 		"name"=>"logo", 
			// 		"value"=> (isset($form['logo']) AND file_exists($app['logo_lib_path'] ."/". $form['logo']))?'/'. app::ov($form['logo']):null,
			// 		"filemedia"=>true
			// 	)
			// );
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
					)
			);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
