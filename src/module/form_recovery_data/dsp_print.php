<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM PERMINTAAN RECOVERY DATA</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 landscape }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">
	<table width="90%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;">
	    <tbody>
            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="50%">&nbsp;</td>
            </tr>
            <tr>
                <td>	        	
                    <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 5px;color:#fff;">	        	
                FORM PERMINTAAN RECOVERY DATA
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="1" cellpadding="3" style="font-size: 12px;float:left;" align="right" width="100%">
                            <tbody>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                    <td style="background:#e6e6e6;color:#222;"></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">No Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                                </tr>
                                
                           </tbody>
                        </table>
                </td>
                <td>&nbsp;</td>
                <td colspan="3">
                   <!--  <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                        <tbody>
                            <tr align="center">
                                <td>Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?></td>
                            </tr>
                        </tbody>
                    </table> -->
                    
                        <table cellspacing="1" cellpadding="3" style="font-size: 12px;float:right;" align="right" width="50%">
                            <tbody>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                                </tr>
                           </tbody>
                        </table>
            </tr>
            <tr>
                <td colspan="3">
                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5">
                        <tbody>
                             <tr align="center" style="background:#10069f;color:#fff;">
                                <td>Nama Pemohon</td>
                                <td>Departemen</td>
                                <td>Jenis data</td>
                                <td>Lokasi Data</td>
                                <td>Keterangan</td>
                             </tr>
                             <tr align="center" style="background:#e6e6e6;color:#222;">
                                <td><?= $name ?></td>
                                <td><?= $section ?></td>
                                <td><?= ($form['jenis_data']=="lainnya"?$form['jenis_data_other']:$form['jenis_data']) ?></td>
                                <td><?= $form['lokasi_file'] ?></td>
                                <td><?= $form['keterangan'] ?></td>
                             </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <!--
                    <div style="float:left;border:2px solid #333;width:60%;border-radius:30px;font-size:12px;padding:1.5%;">
                        <!-- <span><b>Lampirkan gambar atau keterangan lain yang memperjelas:</b></span>
                        <br>
                        <img src="<?php echo $app['data_lib'] ."/". $form['attach']; ?>" style="width:200px;height:auto;margin:3px 0;" alt="">
                        <br> 
                        <span><b>Keterangan:</b> <br> <?= $form['keterangan'] ?></span>
                    </div>-->
                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5" align="">
                        <tbody>
                            <tr>
                                <td width="33%">
                                   <table border="0" width="100%" style="font-size:12px;" cellpadding="5" align="right">
                                        <tbody>
                                            <tr align="center" style="background:#10069f;color:#fff;">
                                                <td colspan="3">Diajukan</td>
                                            </tr>
                                            <tr align="center" style="background:#10069f;color:#fff;">
                                                <th>Tgl</th>
                                                <th>Bln</th>
                                                <th>Thn</th>
                                            </tr>
                                            <tr align="center" style="background:#e6e6e6;color:#222;">
                                                <td><?= $p_day ?></td>
                                                <td><?= $p_month ?></td>
                                                <td><?= $p_year ?></td>
                                            </tr>
                                        </tbody>
                                    </table> 
                                </td>
                                <td width="34%">
                                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5" align="right">
                                        <tbody>
                                            <tr align="center" style="background:#f58220;color:#fff;height:50px;">
                                                <td colspan="3">Waktu Hilang*</td>
                                            </tr>
                                            <tr align="center" style="background:#e6e6e6;color:#222;">
                                                <td colspan="3"><?= date('d/m/Y', strtotime($form['recovery_start_date']))." | ".date('h:i A', strtotime($form['recovery_jam'])) ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="33%">
                                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5" align="right">
                                        <tbody>
                                            <tr align="center" style="background:#10069f;color:#fff;">
                                                <td colspan="3">Penyelesaian</td>
                                            </tr>
                                            <tr align="center" style="background:#10069f;color:#fff;">
                                                <th>Tgl</th>
                                                <th>Bln</th>
                                                <th>Thn</th>
                                            </tr>
                                            <tr align="center" style="background:#e6e6e6;color:#222;">
                                                <td><?= $p_day ?></td>
                                                <td><?= $p_month ?></td>
                                                <td><?= $p_year ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table> 
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table border="0" style="font-size:12px;" cellpadding="5" width="100%">
                        <tbody>
                            <tr style="text-align: center;">
                                <td width="15%" style="background:#000;color:#fff;">Pemohon</td>
                                <td width="15%" style="background:#000;color:#fff;">Supervisor</td>
                                <td width="15%" style="background:#000;color:#fff;">Manager</td>
                                <td width="5%">&nbsp;</td>
                                <td width="15%" style="background:#000;color:#fff;">Staff IT</td>
                                <td width="15%" style="background:#000;color:#fff;">Manager IT</td>
                                <td width="5%">&nbsp;</td>
                                <td width="15%" style="background:#000;color:#fff;">Pemohon</td>
                            </tr>
                            <tr align="center" style="height:50px;">
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td><span style="font-size:20px;font-weight:bold;">&#8594;</span></td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td><span style="font-size:20px;font-weight:bold;">&#8594;</span></td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                            </tr>
                            <tr style="text-align: center;">
                                <td style="background:#000;color:#fff;"><?= $form['name'] ?></td>
                                <td style="background:#000;color:#fff;"><?= $approve_spv_name ?></td>
                                <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                                <td>&nbsp;</td>
                                <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                                <td>&nbsp;</td>
                                <td style="background:#000;color:#fff;"><?= $form['name'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5">
                        <tbody>
                            <tr align="center" style="background:#10069f;color:#fff;">
                                <td>No</td> 
                                <td>Media</td> 
                                <td>Size</td> 
                                <td>Status file</td> 
                                <td>Lokasi</td> 
                                <td>Bukti</td> 
                            </tr>
                            <?php 
                            $no = 1;
                            while($row = db::fetch($rs['add_reco'])){
                            ?>
                            <tr align="center" style="background:#e6e6e6;color:#222;">
                                <td><?= $no ?></td>
                                <td><?= $row['media'] ?></td>
                                <td><?= $row['size'] ?></td>
                                <td><?= $row['status_file'] ?></td>
                                <td><?= $row['lokasi'] ?></td>
                                <td><?= $row['bukti'] ?></td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="float:left;border:2px solid #333;width:97%;font-size:12px;padding:1%;">
                        <span><b>Catatan dari IT :</b><br><?= $form['catatan'] ?></span>
                    </div>
                </td>
            </tr>
	   </tbody>
    </table>
    </section>
    

</body>

</html>