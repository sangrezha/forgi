<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	// admlib::get_component('datepickerlib');
	admlib::get_component('datepickerlib_max');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		if($form['no_pengajuan']){
			admlib::get_component('inputtext',
				array(
					"name"=>"no_pengajuan",
				"readonly"=>"yes",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"readonly"=>"yes",
					// "validate"=>true,
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				"readonly"=>"yes",
					// "validate"=>true,
				"value"=>$act=="add"?$app['me']['name']:$form['name']
			)
		);
		$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section",
				"readonly"=>"yes",
					// "validate"=>true,
				"value"=>$act=="add"?$app['me']['id_section']:$form['id_section'],
				"items"=>$rs['section']
			)
		);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"jenis_data",
		// 		"value"=>$form['jenis_data']
		// 	)
		// );
		admlib::get_component('radio',
			array(
				"name"=>"jenis_data",
				"datas"=>["file","database","lainnya"],
				"validate"=>true,
				"value"=>app::ov($form['jenis_data'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"jenis_data_other",
					// "validate"=>true,
				"value"=>$form['jenis_data_other']
			)
		);
		// admlib::get_component('textarea',
		// 	array(
		// 		"name"=>"alasan",
		// 			"validate"=>true,
		// 		"value"=>$form['alasan']
		// 	)
		// );
		/*admlib::get_component('datepicker',
			array(
				// "name"=>"backup_start_date",
				"name"=>"recovery_start_date",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['recovery_start_date'])
			)
		);*/
?>
<br>
<hr>
<?php
		admlib::get_component('blank',
			array(
				"name"=>"kronologi_kehilangan",
				// "hide_label"=>"iya",
				// "style"=>'style="width: 29%;margin-left: 549px;position: relative;bottom: 44px;"',
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				// "validate"=>true,
				"value"=>$act=="add"?date('H:i'):$form['recovery_jam']
			)
		);
		?>
<hr>
		<?php
		admlib::get_component('datepicker',
			array(
				"name"=>"recovery_start_date",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"style_input"=>"width: 56%;",
				"validate"=>true,
				"value"=>$act=="add"?date('Y-m-d'):$form['recovery_start_date']
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"recovery_jam",
				"hide_label"=>"iya",
				"type"=>"time",
				"style"=>'style="width: 29%;margin-left: 549px;position: relative;bottom: 44px;"',
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>$act=="add"?date('H:i'):$form['recovery_jam']
			)
		);

		/*admlib::get_component('datepicker',
			array(
				"name"=>"recovery_end_date",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['recovery_end_date']),
				"note"=>"Tanggal Max. Backup adalah < 7 hari dari Tanggal saat ini"
			)
		);*/
		/*admlib::get_component('datepicker',
			array(
				"name"=>"recovery_end_date",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"style_input"=>"width: 56%;",
				"validate"=>true,
				"value"=>$act=="add"?date('Y-m-d'):$form['recovery_end_date'],
				"note"=>"Tanggal Max. Backup adalah < 7 hari dari Tanggal saat ini"
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"recovery_end_jam",
				"hide_label"=>"iya",
				"style"=>'style="width: 29%;margin-left: 551px;position: relative;bottom: 63px;',
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>$act=="add"?date('H:i'):$form['recovery_end_jam']
			)
		);*/
		// admlib::get_component('inputupload', 
		// array(
		// 	"name"=>"attach", 
		// 	// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
		// 	"value"=> app::ov($form['attach']),
		// 	"filemedia"=>true
		// 	)
		// );
		admlib::get_component('inputtext',
			array(
				"name"=>"lokasi_file",
				// "readonly"=>"yes",
					"validate"=>true,
				"value"=>$form['lokasi_file']
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"keterangan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
					"validate"=>true,
				// "validate"=>true,
				"value"=>app::ov($form['keterangan'])
			)
		);
		
	
		$cek_creator = db::lookup("created_by",$module,"id",$id);
		if(!empty($id)){
			if($cek_creator==$app['me']["id"]){
				/*admlib::get_component('submit',
					array(
						"id"=>(isset($id))?$id:"",
						"act"=>$act
					)
				);*/
				$cek_approve_2 = db::lookup("approve_2",$module,"id",$id);
				$cek_approve_3 = db::lookup("approve_3",$module,"id",$id);
				if ($cek_approve_2 =="iya" || $cek_approve_3 =="iya") {
					admlib::get_component('submit',
						array(
							"id"=>(isset($id))?$id:"",
							"no_submit"=>"iya",
							"act"=>$act
						)
					);
				}else{
					admlib::get_component('submit',
						array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
						)
					);
				}
			}else{
				// admlib::get_component('submit',
				// 	array(
				// 		"id"=>(isset($id))?$id:"",
				// 		"no_submit"=>"iya",
				// 		"act"=>$act
				// 	)
				// );			
				admlib::get_component('submit_aprove',
					array(
						"id"=>(isset($id))?$id:"",
						// "no_submit"=>"iya",
						"act"=>"approve"
					)
				);
			}
		}else{
			admlib::get_component('submit',
				array(
					"id"=>(isset($id))?$id:"",
					"act"=>$act
				)
			);
		}
	admlib::get_component('formend');
	?>
<script type="text/javascript">
	<?php
if ($form['jenis_data'] == "lainnya"){	
	?>
	$("#g_jenis_data_other").show();
<?php }else{ ?>
   	$("#g_jenis_data_other").hide();
<?php } ?>

$('input[type=radio][name=p_jenis_data]').change(function() {
	// alert(this.value);
	if (this.value == 'lainnya') { 
		$("#g_jenis_data_other").show();
	}else{
      	$("#g_jenis_data_other").hide();
	}
});
</script>
	<?php
admlib::display_block_footer();
?>
