<?php
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('module'=>'report_document','caption'=>ucwords('Laporan Dokumen'));

   $ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
else
    $ipaddress = 'UNKNOWN';
function get_diffrence($tgl_selesai=null,$tgl_created = null){
	// $seconds = strtotime($tgl_selesai) - strtotime($tgl_created);
	// $days    = floor($seconds / 86400);
	// $hours   = floor(($seconds - ($days * 86400)) / 3600);
	// $minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
	// $seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
	// return $days." ".$hours.":".$hours.":".$seconds;

	
	$date_a = new DateTime($tgl_selesai);
	$date_b = new DateTime($tgl_created);

	$interval = date_diff($date_a,$date_b);

	// return $interval->format('%h:%i:%s');
	if(!empty($interval->format('%h'))){
		return $interval->format('%h')." Jam";
	}elseif(!empty($interval->format('%i'))){
		return $interval->format('%i')." Menit";
	}elseif(!empty($interval->format('%s'))){
		return $interval->format('%s')." Menit";
	}
	

}

/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		admlib::display_block_grid("_doc_report");
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	if ($start_date || $end_date || $jenis_form) {
		$all=1;
	}
	// print_r($_REQUEST);
	$module = admlib::$page_active['module'];
/*	$total 	= db::viewlookup('COUNT(id)', "form", '1=1');
	app::set_default($page_size, (isset($all)?$total:10));*/
	$q = "WHERE 1=1";
	if($start_date)
	{
		$q .= " AND a.created_at >= '$start_date' ";
	}
	if($end_date)
	{
		$q .= " AND a.created_at <= '$end_date' ";
	}
	if($end_date && $start_date){
		// $q .= " AND created_at BETWEEN '#01/07/1996#' AND '#31/07/1996#' ";
		$yesterday = date('Y-m-d',strtotime($start_date . "-1 days"));
		$tomorrow = date('Y-m-d',strtotime($end_date . "+1 days"));
		// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		$q = "WHERE 1=1 AND (a.created_at >= '$start_date' AND a.created_at <= '$tomorrow') ";
	}
	// if($jenis_form && $jenis_form !="all")
	// {
	//  	$q .= " AND a.module = '$jenis_form' ";
	// }
	// if($section && $section !="all")
	// {
	// 	// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
	// 	$q .= "AND a.id_section = '". $section ."' ";
	// }
	// if($jenis_form && $jenis_form !="all")
	// {
	// 	// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
	// 	$q .= " AND a.jenis_form LIKE '%". $jenis_form ."%' ";
	// }
	if(empty($tgl_selesai) && empty($end_date)){
		$lastDayThisMonth = date("Y-m-t");
		$q = "WHERE 1=1 AND (a.created_at >= '". date('Y-m')."-1' AND a.created_at <= '$lastDayThisMonth') ";
	}
	// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$form] ." a
	// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	// 	$q ORDER BY a.lang,a.reorder";
	

	$sql = "SELECT a.*, b.name name FROM ". $app['table']["log_document"] ." a
	LEFT JOIN ". $app['view']['user'] ." b ON (a.id_user=b.id)
	$q ORDER BY a.created_at desc";
	// $sql2 = "SELECT * FROM ". $app['view']["form"] ."
	// $q ORDER BY created_at desc";
	 db::query($sql, $rs['row2'], $nr['row2']);
	 $total = $nr['row2'];

	 app::set_default($page_size, (isset($all)?$total:10));
	app::set_navigator($sql, $nav, $page_size, $module .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	// $columns = ['name','ip_address','kapan','yang_di_akses','melakukan_apa'];
	$columns = ['name','ip_address','kapan','yang_di_akses','melakukan_apa'];
	// $numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	// if($numlang > 1)
	// {
	// 	 $columns = array_merge($columns,['lang','duplicate']);
	// }
	while($row = db::fetch($rs['row']))
	{
		$get_doc = db::lookup("name","module_file","id",$row['id_doc']);
		$row['kapan'] = $row['created_at'];
		// $row['yang_di_akses'] = ucwords(str_replace("_"," ",$row['module']));
		$row['yang_di_akses'] = $get_doc;
		$row['melakukan_apa'] = app::i18n($row['act'])."".(!empty($row['edit_before'])?" | ".$row['edit_before']:"").(!empty($row['edit_after'])?" | ".$row['edit_after']:"");
		///////////////////
		if($row['jenis_form']=="form_cpassword"){
			$tgl_selesai = db::lookup("created_at", "log","id_form='".$row['id']."' AND act='APRIT' ORDER BY created_at DESC");
		}elseif($row['status_progress']=="rejected"){
			$tgl_selesai = "<span style='font-size:30px;'>-</span>";
		}else{
			$tgl_selesai = db::lookup("created_at", "log","id_form='".$row['id']."' AND act='APR_MNG_IT' ORDER BY created_at DESC");
		}
		if ($row['no_pengajuan']) {
			// $row['no_pengajuan'] = "IT-4-".$row['no_pengajuan'];
			$row['no_pengajuan'] = $row['no_pengajuan'];
		}else{
			$row['no_pengajuan'] = "";
		}
		$lead_time = $tgl_selesai - $row['created_at'];
		$row['status'] = $row['status_form'];
		$row['progress'] = ucwords($row['status_progress']);
		$row['tgl_pengajuan_dev'] = $row['created_at'];
		$row['tgl_selesai'] = $tgl_selesai;
		if(empty($tgl_selesai) || $tgl_selesai == "<span style='font-size:30px;'>-</span>"){
			$row['lead_time'] = "";
		}else{
			$row['lead_time'] = get_diffrence($tgl_selesai,$row['created_at']);
		}
		// if($numlang > 1)
		// {
		// 	$row['duplicate'] = '<a href="'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		// }
		// else {
		// 	$row['duplicate'] = null;
		// }

			switch ($row['jenis_form']) {
				case 'form_cpassword':
					$code_form = "IT-4-DS-SW-001";
					break;
				case 'form_recovery_data':
					$code_form = "IT-4-DS-SW-002";
					break;
				case 'form_acc_folder':
					$code_form = "IT-4-DS-GN-014";
					break;
				case 'form_tmp_access':
					$code_form = "IT-4-DS-GN-008";
					break;
				case 'form_perubahan_si':
					$code_form = "IT-4-DS-GN-009";
					break;
				case 'form_abnormal_si':
					$code_form = "IT-4-DS-GN-006";
					break;
				case 'form_get_soft':
					$code_form = "IT-4-DS-GN-015";
					break;
				case 'form_hardware_software':
					$code_form = "IT-4-DS-GN-001";
					break;
				case 'form_pengajuan_user':
					$code_form = "IT-4-DS-GN-011";
					break;
				case 'form_register_device':
					$code_form = "IT-4-DS-HW-006";
					break;
				
				default:
					$code_form = "";
					break;
			}
		$row['jenis_form'] = ucwords(str_replace("_"," ",$row['jenis_form']))."<br>".$code_form;
		$results[] = $row;
	}
	// db::remove_notif("",$module,$app['me']['level']);
	// $option = ['sortable'=>true, 'status'=>true];
	$unpost = 1;
	$unmodify=1;
	$option = ['status'=>true,'no_chk'=>"iya","no_edit"=>"iya"];
	include $app['pwebmin'] ."/include/blk_list_doc_report.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "export"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		form::populate($module);

		// if($start_date)
		// {
		// 	$q .= " AND created_at >= '$start_date' ";
		// }
		// if($end_date)
		// {
		// 	$q .= " AND created_at <= '$end_date' ";
		// }
		// if($end_date && $start_date){
		// 	// $q .= " AND created_at BETWEEN '#01/07/1996#' AND '#31/07/1996#' ";
		// 	$yesterday = date('Y-m-d',strtotime($start_date . "-1 days"));
		// 	$tomorrow = date('Y-m-d',strtotime($end_date . "+1 days"));
		// 	// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		// 	// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		// 	$q = "WHERE 1=1 AND (created_at >= '$start_date' AND created_at <= '$tomorrow') ";
		// }


		// if($start_date){
		// 	$rs['report'] = db::get_record_select_view("*", "form", "created_at >= '$start_date'");
		// }
		// if($end_date)
		// {
		// 	$rs['report'] = db::get_record_select_view("*", "form", "created_at <= '$end_date'");
		// }
		// if($end_date && $start_date){
		// 	// $q .= " AND created_at BETWEEN '#01/07/1996#' AND '#31/07/1996#' ";
		// 	$yesterday = date('Y-m-d',strtotime($start_date . "-1 days"));
		// 	$tomorrow = date('Y-m-d',strtotime($end_date . "+1 days"));
		// 	// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		// 	// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		// 	// $q = "WHERE 1=1 AND (created_at >= '$start_date' AND created_at <= '$tomorrow') ";
		// 	$rs['report'] = db::get_record_select_view("*", "form", "created_at >= '$start_date' AND created_at <= '$tomorrow'");
		// }
		// if(empty($end_date) && empty($start_date)){
		// 	// $validate = 'end_date,start_date';
		// 	$rs['report'] = db::get_record_select_view("*", "form", "1=1");
		// }
		// print_r($_REQUEST);
		/*$q = "WHERE 1=1";
		if($start_date)
		{
			$q .= " AND created_at >= '$start_date' ";
		}
		if($end_date)
		{
			$q .= " AND created_at <= '$end_date' ";
		}
		if($end_date && $start_date){
			// $q .= " AND created_at BETWEEN '#01/07/1996#' AND '#31/07/1996#' ";
			$yesterday = date('Y-m-d',strtotime($start_date . "-1 days"));
			$tomorrow = date('Y-m-d',strtotime($end_date . "+1 days"));
			// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
			// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
			$q = "WHERE 1=1 AND (created_at >= '$start_date' AND created_at <= '$tomorrow') ";
		}
		if($section && $section !="all")
		{
			// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
			// $q .= "AND id_section LIKE '%". $section ."%' ";
			$q .= "AND id_section = '". $section ."' ";
		}
		if($jenis_form && $jenis_form !="all")
		{
			// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
			$q .= " AND jenis_form LIKE '%". $jenis_form ."%' ";
		}
		// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$form] ." a
		// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
		// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
		// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
		// 	$q ORDER BY a.lang,a.reorder";
		
		$sql = "SELECT * FROM ". $app['view']["form"] ."
		$q ORDER BY created_at desc";*/

		$q = "WHERE 1=1";
		if($start_date)
		{
			$q .= " AND a.created_at >= '$start_date' ";
		}
		if($end_date)
		{
			$q .= " AND a.created_at <= '$end_date' ";
		}
		if($end_date && $start_date){
			// $q .= " AND created_at BETWEEN '#01/07/1996#' AND '#31/07/1996#' ";
			$yesterday = date('Y-m-d',strtotime($start_date . "-1 days"));
			$tomorrow = date('Y-m-d',strtotime($end_date . "+1 days"));
			// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
			// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
			$q = "WHERE 1=1 AND (a.created_at >= '$start_date' AND a.created_at <= '$tomorrow') ";
		}
		// if($jenis_form && $jenis_form !="all")
		// {
		//  	$q .= " AND a.module = '$jenis_form' ";
		// }
		// if($section && $section !="all")
		// {
		// 	// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		// 	$q .= "AND a.id_section = '". $section ."' ";
		// }
		// if($jenis_form && $jenis_form !="all")
		// {
		// 	// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		// 	$q .= " AND a.jenis_form LIKE '%". $jenis_form ."%' ";
		// }
		if(empty($tgl_selesai) && empty($end_date)){
			$lastDayThisMonth = date("Y-m-t");
			$q = "WHERE 1=1 AND (a.created_at >= '". date('Y-m')."-1' AND a.created_at <= '$lastDayThisMonth') ";
		}
		// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$form] ." a
		// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
		// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
		// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
		// 	$q ORDER BY a.lang,a.reorder";
		

		$sql = "SELECT a.*, b.name name FROM ". $app['table']["log_document"] ." a
		LEFT JOIN ". $app['view']['user'] ." b ON (a.id_user=b.id)
		$q ORDER BY a.created_at desc";


		// app::set_navigator($sql, $nav, $page_size, $module .".mod");
		db::query($sql, $rs['report'], $nr['report']);
		// $results = [];
		// $columns = ['no_urut','nik','NAME','section','jenis_form','status','progress','tgl_pengajuan_dev','tgl_selesai','lead_time'];
		// $numlang = db::count_record('id', 'lang', 'WHERE status="active"');
		// if($numlang > 1)
		// {
		// 	 $columns = array_merge($columns,['lang','duplicate']);
		// }
		// while($row = db::fetch($rs['row']))
		// {

		// }
	    include "dsp_export.php";
		exit;
	endif;
	// if ($step == 2):
	// 	$module = admlib::$page_active['module'];
	// 	$validate = 'p_title,p_images';
	// 	form::serialize_form();
	// 	form::validate('empty', $validate);
	// 	if (form::is_error()):
	// 		msg::build_msg();
	// 		header("location: ". $module .".mod&act=add&error=1");
	// 		exit;
	// 	endif;
	// 	app::mq_encode('p_title,p_content,p_images');
	// 	$identity = rand(1, 100).date("dmYHis");
	// 	$p_images = str_replace("%20"," ","$p_images");
	// 	$sql_lang 	= "SELECT id FROM ". $app['table']['lang'] ." WHERE status='active'";
	// 	$p_alias = app::slugify($p_title);
	// 	db::query($sql_lang, $rs['lang'], $nr['lang']);
	// 	while($row 	= db::fetch($rs['lang']))
	// 	{
	// 		$id 		= rand(1, 100).date("dmYHis");
	// 		$p_lang 	= $row['id'];
	// 		$urut = db::lookup("max(reorder)",$module,"1=1");
	// 		if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
	// 		$sql 		= "INSERT INTO ".$app['table'][$module]."
	// 				(id, title, link, images, identity, alias, lang, reorder, created_by, created_at) VALUES
	// 				('$id', '$p_title', '$p_link', '$p_images', '$p_identity', '$p_alias', '$p_lang', '$urut', '". $app['me']['id'] ."', now())";
	// 		db::qry($sql);
	// 	}
	// 	msg::set_message('success', app::i18n('create'));
	// 	$unpost = 1;
	// 	$unmodify=1;
	// 	header("location: " . $app['webmin'] ."/". $module .".mod");
	// 	exit;
	// endif;
endif;