<!DOCTYPE html>
<html>
<head>
	<title>Export Data Ke Excel Dengan PHP</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;

	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>

	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Report_excel_".(empty($start_date)?"":(empty($end_date)?$start_date:$start_date." sampai ".$end_date)).".xls");
	?>

	<center>
		<p>
			Export Excel Report form
			<br>
			<?php if($start_date && empty($end_date)){ ?>
			Data dimuat dari tanggal <?= $start_date ?>  hingga yang terbaru
			<?php } ?>
			<?php if(empty($start_date) && $end_date){ ?>
			Data dimuat dari report awal hingga tanggal <?= $end_date ?>
			<?php } ?>
			<?php if($start_date && $end_date){ ?>
			Data dimuat dari tanggal <?= $start_date ?> sampai tanggal <?= $end_date ?>
			<?php } ?>
			<?php if(empty($start_date) && empty($end_date)){ ?>
			Semua data Report telah di tampilkan
			<?php } ?>
		</p>
	</center>

	<table style="text-align: center;" border="1">
		<tr>
			<th>No</th>
			<th>No Pengajuan</th>
			<!-- <th>No Urut</th> -->
			<th>NIK</th>
			<th>Name</th>
			<th>section</th>
			<th>jenis_form</th>
			<th>Status</th>
			<th>Progress</th>
			<th>Tanggal pengajuan</th>
			<th>Tanggal Selesai</th>
			<th>Lead Time</th>
		</tr>
		<?php 
		$n = 1;
		while($row = db::fetch($rs['report'])){ 	
			if($row['jenis_form']=="form_cpassword"){
				$tgl_selesai = db::lookup("created_at", "log","id_form='".$row['id']."' AND act='APRIT' ORDER BY created_at DESC");
			}else{
				$tgl_selesai = db::lookup("created_at", "log","id_form='".$row['id']."' AND act='APR_MNG_IT' ORDER BY created_at DESC");
			}
			if($row['status_progress']=="rejected"){
				$tgl_selesai = "<span style='font-size:30px;'>-</span>";
			}
			
			$lead_time = $tgl_selesai - $row['created_at'];
			//switch ($row['jenis_form']) {
			//	case 'form_cpassword':
			//		$code_form = "IT-4-DS-SW-001";
			//		break;
			//	case 'form_recovery_data':
			//		$code_form = "IT-4-DS-SW-002";
			//		break;
			//	case 'form_acc_folder':
			//		$code_form = "IT-4-DS-GN-014";
			//		break;
			//	case 'form_tmp_access':
			//		$code_form = "IT-4-DS-GN-008";
			//		break;
			//	case 'form_perubahan_si':
			//		$code_form = "IT-4-DS-GN-009";
			//		break;
			//	case 'form_abnormal_si':
			//		$code_form = "IT-4-DS-GN-006";
			//		break;
			//	case 'form_get_soft':
			//		$code_form = "IT-4-DS-GN-015";
			//		break;
			//	case 'form_hardware_software':
			//		$code_form = "IT-4-DS-GN-001";
			//		break;
			//	case 'form_pengajuan_user':
			//		$code_form = "IT-4-DS-GN-011";
			//		break;
			//	case 'form_register_device':
			//		$code_form = "IT-4-DS-HW-006";
			//		break;
			//	
			//	default:
			//		$code_form = "";
			//		break;
			//}
			$code_form = "";
			// $row['status'] = $row['status_form'];
			// $row['progress'] = ucwords($row['status_progress']);
			// $row['tgl_pengajuan_dev'] = $row['created_at'];
			// $row['tgl_selesai'] = $tgl_selesai;
			if($row['status_progress']=="rejected"){
				$row['lead_time'] = "<span style='font-size:30px;'>-</span>";
			}elseif(empty($tgl_selesai) || $tgl_selesai == "<span style='font-size:30px;'>-</span>"){
				$row['lead_time'] = "";
			}else{
				$row['lead_time'] = get_diffrence($tgl_selesai,$row['created_at']);
			}
			
			?>
		<tr>
			<td><?= $n ?></td>
			<?php if ($row['no_pengajuan']){ ?>
			<!-- <td><?= "IT-4-".$row['no_pengajuan'] ?></td> -->
			<td><?= $row['no_pengajuan'] ?></td>
			<?php }else{ ?>
			<td></td>
			<?php } 
				$nik = strval($row['nik']);  ?>
			<!-- <td><?= $row['no_urut'] ?></td> -->
			<td><?= "` {$nik} `" ?></td>
			<td><?= $row['NAME'] ?></td>
			<td><?= $row['section'] ?></td>
			<td><?= ucwords(str_replace("_"," ",$row['jenis_form']))."<br>".$code_form ?></td>
			<td><?= $row['status_form'] ?></td>
			<td><?= $row['status_progress'] ?></td>
			<td><?= $row['created_at'] ?></td>
			<td><?= $tgl_selesai ?></td>
			<td><?= $row['lead_time'] ?></td>
		</tr>
		<?php $n++; } ?>
<!-- 
		<tr>
			<td>2</td>
			<td>Diki Alfarabi Hadi</td>
			<td>Jakarta</td>
			<td>08291212211</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Zakaria</td>
			<td>Medan</td>
			<td>0829121223</td>
		</tr>
		<tr>
			<td>4</td>
			<td>Alvinur</td>
			<td>Jakarta</td>
			<td>02133324344</td>
		</tr>
		<tr>
			<td>5</td>
			<td>Muhammad Rizani</td>
			<td>Jakarta</td>
			<td>08231111223</td>
		</tr>
		<tr>
			<td>6</td>
			<td>Rizaldi Waloni</td>
			<td>Jakarta</td>
			<td>027373733</td>
		</tr>
		<tr>
			<td>7</td>
			<td>Ferdian</td>
			<td>Jakarta</td>
			<td>0829121223</td>
		</tr>
		<tr>
			<td>8</td>
			<td>Fatimah</td>
			<td>Jakarta</td>
			<td>23432423423</td>
		</tr>
		<tr>
			<td>9</td>
			<td>Aminah</td>
			<td>Jakarta</td>
			<td>0829234233</td>
		</tr>
		<tr>
			<td>10</td>
			<td>Jafarudin</td>
			<td>Jakarta</td>
			<td>0829239323</td>
		</tr> -->
	</table>
</body>
</html>