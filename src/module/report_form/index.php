<?php
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('module'=>'report_form','caption'=>ucwords('report form'));

function get_diffrence($tgl_selesai=null,$tgl_created = null){
	// $seconds = strtotime($tgl_selesai) - strtotime($tgl_created);
	// $days    = floor($seconds / 86400);
	// $hours   = floor(($seconds - ($days * 86400)) / 3600);
	// $minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
	// $seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
	// return $days." ".$hours.":".$hours.":".$seconds;

	
	$date_a = new DateTime($tgl_selesai);
	$date_b = new DateTime($tgl_created);

	$interval = date_diff($date_a,$date_b);

	// return $interval->format('%h:%i:%s');
	if(!empty($interval->format('%d'))){
		return $interval->format('%d')." hari ".$interval->format('%h')." Jam ";
	}elseif(!empty($interval->format('%h'))){
		return $interval->format('%h')." Jam ";
	}elseif(!empty($interval->format('%i'))){
		return $interval->format('%i')." Menit";
	}elseif(!empty($interval->format('%s'))){
		return $interval->format('%s')." detik";
	}
	

}

/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		admlib::display_block_grid("_report");
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	if ($start_date || $end_date || $section || $jenis_form) {
		$all=1;
	}
	$module = admlib::$page_active['module'];
	// $total 	= db::viewlookup('COUNT(id)', "form", '1=1');
	// app::set_default($page_size, (isset($all)?$total:10));
	$q = "WHERE 1=1";
	if($start_date && empty($end_date) )
	{
		$q .= " AND created_at >= '$start_date' ";
	}
	if($end_date && empty($start_date))
	{
		$q .= " AND created_at <= '$end_date' ";
	}
	if($end_date && $start_date){
		// $q .= " AND created_at BETWEEN '#01/07/1996#' AND '#31/07/1996#' ";
		$yesterday = date('Y-m-d',strtotime($start_date . "-1 days"));
		$tomorrow = date('Y-m-d',strtotime($end_date . "+1 days"));
		// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		$q .= " AND (created_at >= '$start_date' AND created_at <= '$tomorrow') ";
	}
	if($section && $section !="all")
	{
		// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		$q .= " AND id_section = '". $section ."' ";
	}
	if((($status_p == "fully" || $status_p == "unverified") && $status_p !="all") && ($app['me']['level']=="1" && $app['me']['code'] == "it"))
	{
		// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		$q .= " AND status_progress =  IF(`jenis_form` != 'form_abnormal_si', '".$status_p."', 'unverified') ";
	}/*elseif(($status_p && $status_p !="all") && ($app['me']['level']=="3" && $app['me']['code'] == "it")){
		$q .= " AND status_progress =  'accepted'";
		$q .= "AND jenis_form != 'form_cpassword' ";
	}*/
	if($jenis_form && $jenis_form !="all")
	{
		// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		$q .= " AND jenis_form LIKE '%". $jenis_form ."%' ";
	}
	if(empty($tgl_selesai) && empty($end_date) && $status_p){
		$lastDayThisMonth = date("Y-m-t");
		$q .= " AND (created_at >= '". date('Y-m')."-1' AND created_at <= '$lastDayThisMonth') ";
	}elseif((empty($tgl_selesai) && empty($end_date)) && (empty($status_p))  && (($app['me']['level']=="1" && $app['me']['code'] == "it")) ){
		$lastDayThisMonth = date("Y-m-t");
		$q .= " AND (created_at >= '". date('Y-m')."-1' AND created_at <= '$lastDayThisMonth') ";
		$q .= "AND status_progress =  IF(`jenis_form` != 'form_abnormal_si', 'fully', 'unverified') ";
	}
	// elseif(((empty($tgl_selesai) && empty($end_date)) && (empty($status_p))) && (($app['me']['level']=="3" && $app['me']['code'] == "it"))){
	elseif((empty($tgl_selesai) && empty($end_date)) && (empty($section) && $section == "all") && (empty($status_p) && $status_p =='all' ) && (($app['me']['level']=="3" && $app['me']['code'] == "it"))){
		$lastDayThisMonth = date("Y-m-t");
		$q = "WHERE 1=1 AND (created_at >= '". date('Y-m')."-1' AND created_at <= '$lastDayThisMonth') ";
		$q .= "AND status_progress =  'accepted' ";
		$q .= "AND jenis_form != 'form_cpassword' ";
	}


	if($status_p && $status_p !="all"){
		$q .= " AND status_progress =  '$status_p'";
	}

	if($status_p && $status_p !="all"){
		$q .= " AND status_progress =  '$status_p'";
	}


	if ((empty($tgl_selesai) && empty($end_date)) && (empty($section) || $section == "all") && empty($status_p) && ($app['me']['level']=="3" && $app['me']['code'] == "it") ) {
		$lastDayThisMonth = date("Y-m-t");
		$q = "WHERE 1=1 AND (created_at >= '". date('Y-m')."-1' AND created_at <= '$lastDayThisMonth') ";
		$q .= "AND status_progress =  'accepted' ";
		$q .= "AND jenis_form != 'form_cpassword' ";
	}
	// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$form] ." a
	// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	// 	$q ORDER BY a.lang,a.reorder";
	

	$sql = "SELECT * FROM ". $app['view']["form"] ."
	$q ORDER BY created_at desc";
	// $sql2 = "SELECT * FROM ". $app['view']["form"] ."
	// $q ORDER BY created_at desc";
	// db::query($sql2, $rs['row2'], $nr['row2']);
	// $total = $nr['row2'];

	db::query($sql, $rs['row'], $nr['row']);
	$total 	= $nr['row'];
	app::set_default($page_size, (isset($all)?$total:10));
	// app::set_default($page_size, (isset($all)?$total:10));
	app::set_navigator($sql, $nav, $page_size, $module .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = [];
	if ($app['me']['code']=="it") {
		$columns = array_merge($columns, ['edit']);
	}
	// $columns = array_merge($columns, ['no_pengajuan','nik','NAME','section','jenis_form','status','progress','tgl_pengajuan_dev','tgl_selesai','lead_time']);
	$columns = array_merge($columns, ['no_pengajuan','nik','NAME','section','jenis_form','progress','tgl_pengajuan_dev','tgl_selesai','lead_time']);
	// $numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	// if($numlang > 1)
	// {
	// 	 $columns = array_merge($columns,['lang','duplicate']);
	// }
	while($row = db::fetch($rs['row']))
	{
		if($row['jenis_form']=="form_cpassword"){
			$tgl_selesai = db::lookup("created_at", "log","id_form='".$row['id']."' AND act='APRIT' ORDER BY created_at DESC");
		}elseif($row['status_progress']=="rejected"){
			$tgl_selesai = "<span style='font-size:30px;'>-</span>";
		}else{
			$tgl_selesai = db::lookup("created_at", "log","id_form='".$row['id']."' AND act='APR_MNG_IT' ORDER BY created_at DESC");
		}
		$lead_time = (strtotime($tgl_selesai) - strtotime($row['created_at']))/3600;
		$row['status'] = $row['status_form'];
		$row['progress'] = ucwords($row['status_progress']);
		$row['tgl_pengajuan_dev'] = $row['created_at'];
		$row['tgl_selesai'] = $tgl_selesai;
		if(empty($tgl_selesai) || $tgl_selesai == "<span style='font-size:30px;'>-</span>"){
			$row['lead_time'] = "";
		}else{
			$row['lead_time'] = get_diffrence($tgl_selesai,$row['created_at']);
			//$row['lead_time'] = $lead_time." jam ";
		}
		// if($numlang > 1)
		// {
		// 	$row['duplicate'] = '<a href="'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		// }
		// else {
		// 	$row['duplicate'] = null;
		// }

		if ($app['me']['code']=="it" && $app['me']['level']=="1") {
			$act_aksi = "approve_it";
			if ($row['jenis_form'] == "form_hardware_software" || $row['jenis_form'] == "form_tmp_access" || $row['jenis_form'] == "form_perubahan_si" || $row['jenis_form'] == "form_pengajuan_user" || $row['jenis_form'] == "form_acc_folder" || $row['jenis_form'] == "form_get_soft" || $row['jenis_form'] == "form_cpassword") {
				$act_aksi = "view";
			}
			if ($row['jenis_form'] == "form_recovery_data") {
				$act_aksi = "feedback";
			}

				
			$row['edit'] = '<a  target="_blank" title="Edit" href="'.$row['jenis_form'].'.mod&act='.$act_aksi.'&id='.$row['id'].'"><i class="fa fa-pencil" aria-hidden="true"></i></a>';	

			if ($row['no_pengajuan']) {
				// $row['no_pengajuan'] = "IT-4-".$row['no_pengajuan'];
				$row['no_pengajuan'] = '<a  target="_blank" title="Edit" href="'.$row['jenis_form'].'.mod&act='.$act_aksi.'&id='.$row['id'].'">'.$row['no_pengajuan'].'</i></a>';
			}else{
				$row['no_pengajuan'] = "";
			}
		}elseif ($app['me']['code']=="it" && $app['me']['level']=="3") {
			$act_aksi = "view";
			$row['edit'] = '<a  target="_blank" title="Edit" href="'.$row['jenis_form'].'.mod&act='.$act_aksi.'&id='.$row['id'].'"><i class="fa fa-pencil" aria-hidden="true"></i></a>';	
			$row['no_pengajuan'] = '<a  target="_blank" title="Edit" href="'.$row['jenis_form'].'.mod&act='.$act_aksi.'&id='.$row['id'].'">'.$row['no_pengajuan'].'</i></a>';
		}




			switch ($row['jenis_form']) {
				case 'form_cpassword':
					$code_form = "IT-4-DS-SW-001";
					break;
				case 'form_recovery_data':
					$code_form = "IT-4-DS-SW-002";
					break;
				case 'form_acc_folder':
					$code_form = "IT-4-DS-GN-014";
					break;
				case 'form_tmp_access':
					$code_form = "IT-4-DS-GN-008";
					break;
				case 'form_perubahan_si':
					$code_form = "IT-4-DS-GN-009";
					break;
				case 'form_abnormal_si':
					$code_form = "IT-4-DS-GN-006";
					break;
				case 'form_get_soft':
					$code_form = "IT-4-DS-GN-015";
					break;
				case 'form_hardware_software':
					$code_form = "IT-4-DS-GN-001";
					break;
				case 'form_pengajuan_user':
					$code_form = "IT-4-DS-GN-011";
					break;
				case 'form_register_device':
					$code_form = "IT-4-DS-HW-006";
					break;
				
				default:
					$code_form = "";
					break;
			}

		$row['progress'] = app::getliblang("table_".$row['status_progress']);


		$row_status_lama = $row['status_progress'];
		//$row['status_progress'] = app::getliblang("table_".$row['status_progress']);
		if ($row_status_lama =="rejected") {
			// $row['status_progress'] = db::lookup("id_user","log", "(id_form='".$row['id']."' AND act ='APR') AND module ='".$row['jenis_form']."' order by created_at desc");
			$get_reject = db::lookup("id_user","log", "(id_form='".$row['id']."' AND act ='APR') AND module ='".$row['jenis_form']."' order by created_at desc");
			$row['progress'] = $row['progress']."<br>[ ".db::viewlookup("name","user", "id='$get_reject'")." ] ";
		}
		$row['jenis_form'] = ucwords(str_replace("_"," ",$row['jenis_form']))."<br>".$code_form;
		$results[] = $row;
	}
	db::remove_notif("",$module,$app['me']['level']);
	// $option = ['sortable'=>true, 'status'=>true];
	$option = ['status'=>true,'no_chk'=>"iya","no_edit"=>"iya"];
	include $app['pwebmin'] ."/include/blk_list_report.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "export"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		form::populate($module);
$q = "WHERE 1=1";
	if($start_date && empty($end_date) )
	{
		$q .= " AND created_at >= '$start_date' ";
	}
	if($end_date && empty($start_date))
	{
		$q .= " AND created_at <= '$end_date' ";
	}
	if($end_date && $start_date){
		// $q .= " AND created_at BETWEEN '#01/07/1996#' AND '#31/07/1996#' ";
		$yesterday = date('Y-m-d',strtotime($start_date . "-1 days"));
		$tomorrow = date('Y-m-d',strtotime($end_date . "+1 days"));
		// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		// $q = "WHERE 1=1 AND created_at BETWEEN '$yesterday' AND '$tomorrow' ";
		$q .= " AND (created_at >= '$start_date' AND created_at <= '$tomorrow') ";
	}
	if($section && $section !="all")
	{
		// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		$q .= " AND id_section = '". $section ."' ";
	}
	if((($status_p == "fully" || $status_p == "unverified") && $status_p !="all") && ($app['me']['level']=="1" && $app['me']['code'] == "it"))
	{
		// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		$q .= " AND status_progress =  IF(`jenis_form` != 'form_abnormal_si', '".$status_p."', 'unverified') ";
	}/*elseif(($status_p && $status_p !="all") && ($app['me']['level']=="3" && $app['me']['code'] == "it")){
		$q .= " AND status_progress =  'accepted'";
		$q .= "AND jenis_form != 'form_cpassword' ";
	}*/
	if($jenis_form && $jenis_form !="all")
	{
		// $q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
		$q .= " AND jenis_form LIKE '%". $jenis_form ."%' ";
	}
	if(empty($tgl_selesai) && empty($end_date) && $status_p){
		$lastDayThisMonth = date("Y-m-t");
		$q .= " AND (created_at >= '". date('Y-m')."-1' AND created_at <= '$lastDayThisMonth') ";
	}elseif((empty($tgl_selesai) && empty($end_date)) && (empty($status_p))  && (($app['me']['level']=="1" && $app['me']['code'] == "it")) ){
		$lastDayThisMonth = date("Y-m-t");
		$q .= " AND (created_at >= '". date('Y-m')."-1' AND created_at <= '$lastDayThisMonth') ";
		$q .= "AND status_progress =  IF(`jenis_form` != 'form_abnormal_si', 'fully', 'unverified') ";
	}
	// elseif(((empty($tgl_selesai) && empty($end_date)) && (empty($status_p))) && (($app['me']['level']=="3" && $app['me']['code'] == "it"))){
	elseif((empty($tgl_selesai) && empty($end_date)) && (empty($section) && $section == "all") && (empty($status_p) && $status_p =='all' ) && (($app['me']['level']=="3" && $app['me']['code'] == "it"))){
		$lastDayThisMonth = date("Y-m-t");
		$q = "WHERE 1=1 AND (created_at >= '". date('Y-m')."-1' AND created_at <= '$lastDayThisMonth') ";
		$q .= "AND status_progress =  'accepted' ";
		$q .= "AND jenis_form != 'form_cpassword' ";
	}


	if($status_p && $status_p !="all"){
		$q .= " AND status_progress =  '$status_p'";
	}
	if ((empty($tgl_selesai) && empty($end_date)) && (empty($section) || $section == "all") && empty($status_p) && ($app['me']['level']=="3" && $app['me']['code'] == "it") ) {
		$lastDayThisMonth = date("Y-m-t");
		$q = "WHERE 1=1 AND (created_at >= '". date('Y-m')."-1' AND created_at <= '$lastDayThisMonth') ";
		$q .= "AND status_progress =  'accepted' ";
		$q .= "AND jenis_form != 'form_cpassword' ";
	}
		// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$form] ." a
		// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
		// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
		// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
		// 	$q ORDER BY a.lang,a.reorder";
		
		$sql = "SELECT * FROM ". $app['view']["form"] ."
		$q ORDER BY created_at desc";
		// app::set_navigator($sql, $nav, $page_size, $module .".mod");
		db::query($sql, $rs['report'], $nr['report']);
		// $results = [];
		// $columns = ['no_urut','nik','NAME','section','jenis_form','status','progress','tgl_pengajuan_dev','tgl_selesai','lead_time'];
		// $numlang = db::count_record('id', 'lang', 'WHERE status="active"');
		// if($numlang > 1)
		// {
		// 	 $columns = array_merge($columns,['lang','duplicate']);
		// }
		// while($row = db::fetch($rs['row']))
		// {

		// }
	    include "dsp_export.php";
		exit;
	endif;
	// if ($step == 2):
	// 	$module = admlib::$page_active['module'];
	// 	$validate = 'p_title,p_images';
	// 	form::serialize_form();
	// 	form::validate('empty', $validate);
	// 	if (form::is_error()):
	// 		msg::build_msg();
	// 		header("location: ". $module .".mod&act=add&error=1");
	// 		exit;
	// 	endif;
	// 	app::mq_encode('p_title,p_content,p_images');
	// 	$identity = rand(1, 100).date("dmYHis");
	// 	$p_images = str_replace("%20"," ","$p_images");
	// 	$sql_lang 	= "SELECT id FROM ". $app['table']['lang'] ." WHERE status='active'";
	// 	$p_alias = app::slugify($p_title);
	// 	db::query($sql_lang, $rs['lang'], $nr['lang']);
	// 	while($row 	= db::fetch($rs['lang']))
	// 	{
	// 		$id 		= rand(1, 100).date("dmYHis");
	// 		$p_lang 	= $row['id'];
	// 		$urut = db::lookup("max(reorder)",$module,"1=1");
	// 		if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
	// 		$sql 		= "INSERT INTO ".$app['table'][$module]."
	// 				(id, title, link, images, identity, alias, lang, reorder, created_by, created_at) VALUES
	// 				('$id', '$p_title', '$p_link', '$p_images', '$p_identity', '$p_alias', '$p_lang', '$urut', '". $app['me']['id'] ."', now())";
	// 		db::qry($sql);
	// 	}
	// 	msg::set_message('success', app::i18n('create'));
	// 	$unpost = 1;
	// 	$unmodify=1;
	// 	header("location: " . $app['webmin'] ."/". $module .".mod");
	// 	exit;
	// endif;
endif;