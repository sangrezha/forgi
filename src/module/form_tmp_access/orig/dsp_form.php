<?php
admlib::display_block_header();
	admlib::get_component('texteditorlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		admlib::get_component('inputtext',
			array(
				"name"=>"company",
				"value"=>app::ov($form['company']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				"value"=>app::ov($form['name']),
			)
		);
		// $form['sys_need'] = "laptop";
		admlib::get_component('radio',
			array(
				"name"=>"sys_need",
				"datas"=>["komputer","laptop"],
				"value"=>app::ov($form['sys_need'])
			)
		);
		?>
		<hr><hr>
		<?php
		admlib::get_component('inputtext',
			array(
				"name"=>"user",
				"value"=>app::ov($form['user']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"password",
				"value"=>app::ov($form['password']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"ipaddress",
				"value"=>app::ov($form['ipaddress']),
			)
		);
		// admlib::get_component('textarea',
		// 	array(
		// 		"editor"=>true,
		// 		"name"=>"content",
		// 		"value"=>app::ov($form['content']),
		// 	)
		// );
		admlib::get_component('inputupload',
			array(
				"name"=>"images",
				"value"=>app::ov($form['images']),
				"filemedia"=>true
			)
		);
		if(isset($id))
		{
			admlib::get_component('language',
				array(
					"name"=>"lang",
					"value"=>app::ov($form['lang'])
				)
			);
		}
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
