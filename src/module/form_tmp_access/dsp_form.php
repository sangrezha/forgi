<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	admlib::get_component('select2lib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		if($form['no_pengajuan']){
			admlib::get_component('inputtext',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"readonly"=>"yes",
					"validate"=>true,
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('inputtext',
			array(
				"name"=>"company",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['company'])
			)
		);
		?>
		<hr>
		<?php
		admlib::get_component('blank',
			array(
				"name"=>"contact_person",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
			)
		);
		?>
		<!-- <hr> -->
		<?php
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['name'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"hp",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				// "validate"=>true,
				"value"=>app::ov($form['hp'])
			)
		);
		?>
		<hr>
		<?php
/*		admlib::get_component('radio',
			array(
				"name"=>"used_sys",
				"datas"=>["hr","andon","erp","internet","other"],
				"validate"=>true,
				"value"=>app::ov($form['used_sys'])
			)
		);*/
		$rs["used_sys"] = db::get_record_select("id, name","sistem_informasi","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"used_sys",
				"items"=>$rs['used_sys'],
				"validate"=>true,
				"other"=>"iya",
				"value"=>app::ov($form['used_sys'])
			)
		);
		// $rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		/*admlib::get_component('select',
			array(
				"name"=>"id_section",
				"value"=>app::ov($form['id_section']),
				"readonly"=>"iya",
				"items"=>$rs['section']
			)
		);*/
		admlib::get_component('inputtext',
			array(
				"name"=>"other",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				// "validate"=>true,
				"value"=>app::ov($form['other'])
			)
		);
		// print_r($app['me']);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"function",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		"value"=>app::ov($form['function'])
		// 	)
		// );
		// $rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		// admlib::get_component('select',
		// 	array(
		// 		"name"=>"id_departement",
		// 		"value"=>app::ov($app['me']['id_departement']),
		// 		"items"=>$rs['departement']
		// 	)
		// );
		?>
		<hr>
		<?php
		admlib::get_component('blank',
			array(
				"name"=>"penjelasan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
			)
		);
		?>
		<br>
		<?php
		admlib::get_component('textarea',
			array(
				"name"=>"keperluan",
				"validate"=>true,
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['keperluan'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"waktu",
				"style_input"=>"width: 56%;",
				"validate"=>true,
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['waktu'])
			)
		);
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"satuan",
		// 		"datas"=>["jam","hari"],
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['satuan'])
		// 	)
		// );
		admlib::get_component('select',
			array(
				"name"=>"satuan",
				// "items"=>["jam"=>"jam","hari"=>"hari"],
				"hide_label"=>"iya",
				"style"=>'style="width: 29%;margin-left: 55%;position: relative;bottom: 42px;"',
				"items"=>[["id"=>"jam","name"=>"jam"],["id"=>"hari","name"=>"hari"]],
				"validate"=>true,
				"value"=>app::ov($form['satuan'])
			)
		);
		

		$cek_creator = db::lookup("created_by",$module,"id",$id);
		if(!empty($id)){
			if($cek_creator==$app['me']["id"]){
				admlib::get_component('submit',
					array(
						"id"=>(isset($id))?$id:"",
						"act"=>$act
					)
				);
			}else{
				// admlib::get_component('submit',
				// 	array(
				// 		"id"=>(isset($id))?$id:"",
				// 		"no_submit"=>"iya",
				// 		"act"=>$act
				// 	)
				// );			
				admlib::get_component('submit_aprove',
					array(
						"id"=>(isset($id))?$id:"",
						// "no_submit"=>"iya",
						"act"=>"approve"
					)
				);
			}
		}else{
			admlib::get_component('submit',
				array(
					"id"=>(isset($id))?$id:"",
					"act"=>$act
				)
			);
		}
	admlib::get_component('formend');
	?>
	
<script>

$(document).ready(function () {
	<?php //if(empty($form['used_sys']) || $form['used_sys'] == "other"){ 
		  if($form['used_sys'] != "other"){ 
		?>
		$("#g_other").hide();
	<?php } ?>
		// $('input[type=radio][name=p_used_sys]').change(function() {
		// 	if (this.value == 'other') {
		// 		// alert("Allot Thai Gayo Bhai");
		// 		$("#g_other").show();
		// 	}
		// 	else{
		// 		// alert("Transfer Thai Gayo");
  //             	$("#g_other").hide();
		// 		  $('#other').value(""); 
		// 	}
		// });
		// $('input[type=select][name=p_used_sys]').change(function() {
		$('#used_sys').change(function() {
			// alert(this.value);
			if (this.value == 'other') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_other").show();
			}
			else{
				// alert("Transfer Thai Gayo");
              	$("#g_other").hide();
				  $('#other').value(""); 
			}
		});
});

</script>
	<?php
admlib::display_block_footer();
?>