<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM PENGAJUAN TEMPORARY ACCESS</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">
    <section class="sheet padding-10mm">
        <table width="100%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;font-size: 12px;">
            <tbody>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td width="65%">&nbsp;</td>
                </tr>
                <tr>
                    <td>	        	
                        <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                    <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 0px;color:#fff;">	        	
                    FORM PENGAJUAN TEMPORARY ACCESS <br>
                        (Perangkat Milik Non CMWI)
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" style="font-size:14px;float:left;" cellpadding="0" width="70%">
                            <tbody>
                                <tr>
                                    <td><b>Company</b></td>
                                    <td>:</td>
                                    <td><?=  $form['company'] ?></td>
                                </tr>
                                <tr>
                                    <!-- <td  style="border-top: 1px solid black;" colspan="3"><b>Contact Person</b></td> -->
                                    <td colspan="3"><b>Contact Person</b></td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>:</td>
                                    <td><?=  $form['name'] ?></td>
                                </tr>
                                <tr>
                                    <td>HP</td>
                                    <td>:</td>
                                    <td><?=  $form['hp'] ?></td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><b>A. System yang dibutuhkan</b></td>
                                    <td>:</td>
                                    <td>
                                    <?php 
                                    $used_sys = db::lookup("name","sistem_informasi","id='".$form['used_sys']."'");	 ?>
                                    <?=  (empty($used_sys)?$form['used_sys']:$used_sys) ?>
                                    <!-- 				<input type="radio" id="baru" name="p_used_sys" value="hr" <?= ($form['used_sys']=="hr"?"checked":"") ?> >
                                    <label for="baru">HR System</label>
                                    <input type="radio" id="ada" name="p_used_sys" value="andon" <?= ($form['used_sys']=="andon"?"checked":"") ?> >
                                    <label for="ada">Andon System</label>
                                    <input type="radio" id="ada" name="p_used_sys" value="erp" <?= ($form['used_sys']=="erp"?"checked":"") ?> >
                                    <label for="ada">ERP System</label>
                                    <input type="radio" id="ada" name="p_used_sys" value="internet" <?= ($form['used_sys']=="internet"?"checked":"") ?> >
                                    <label for="ada">Internet</label>
                                    <input type="radio" id="ada" name="p_used_sys" value="ada" <?= ($form['used_sys']=="other"?"checked":"") ?> >
                                    <label for="ada">Others</label> -->
                                    </td>
                                </tr>
                                <?php if($form['used_sys']=="other"){ ?>
                                <tr>
                                    <td>Other</td>
                                    <td>:</td>
                                    <td><?= $form["other"] ?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="3"><b>B. Penjelasaan</b></td>
                                </tr>
                                <tr>
                                    <td>Keperluan</td>
                                    <td>:</td>
                                    <td><?=  $form['keperluan'] ?></td>
                                </tr>
                                <tr>
                                    <td>Waktu</td>
                                    <td>:</td>
                                    <td><?=  $form['waktu']." ".$form["satuan"] ?> </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                            <tbody>
                                <tr align="center">
                                    <td>Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?></td>
                                </tr>
                            </tbody>
                        </table> -->
                    <table cellspacing="1" cellpadding="3" style="font-size: 12px;" align="right" width="30%">
                        <tbody>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                <td style="background:#e6e6e6;color:#222;"></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Revisi</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                            </tr>
                       </tbody>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" style="font-size:12px;" cellpadding="5">
                            <tbody>
                                <tr align="center">
                                    <td colspan="2" style="background:#000;color:#fff;">Penanggung Jawab Departemen</td>
                                </tr>
                                <tr align="center">
                                    <td width="50%" style="background:#000;color:#fff;">Leader</td>
                                    <td width="50%" style="background:#000;color:#fff;">Manager</td>
                                </tr>
                                <tr align="center" style="height:75px;">
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr align="center">
                                    <td style="background:#000;color:#fff;"><?= $approve_spv_name ?></td>
                                    <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <div style="width: 100%; height: 120px;border:1px solid #000; padding:0 0 7px 0; line-height:20px;">
                            <ul style="list-style:number;"><strong>Keterangan:</strong>
                                <li>Dengan Pertimbangan Keamanan Sumber Daya IT CMWI, maka IT Depart berhak menolak atau menerima pengajuan ini.</li>
                                <li>IT Depart secara otomatis akan menonaktifkan pengajuan ini sesuai dengan batas waktu yang disepakati.</li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr align="center">
                    <td colspan="2" style="font-size:16px;font-weight:bold;letter-spacing:0px;background:#f58220;border:2px solid #f58220; height:40px;border-radius: 10px;">
                        PEMOHON  <span style="font-size:20px;font-weight:bold;">&#8594;</span>  LEADER  <span style="font-size:20px;font-weight:bold;">&#8594;</span>  IT STAFF  <span style="font-size:20px;font-weight:bold;">&#8594;</span>  IT MANAGER
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" style="font-size:14px;"><b>C. Informasi dan Data yang disampaikan</b></td>
                </tr>
                <tr>
                    <td>
                       <table border="0" style="font-size:14px;" cellpadding="3" valign="top">
                            <tbody>
                                <tr>
                                    <td width="40%">User</td>
                                    <td width="5%">:</td>
                                    <td width="55%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td>:</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>IP Address</td>
                                    <td>:</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <table border="0" width="52%" style="font-size:12px;" cellpadding="5" align="right">
                            <tbody>
                                <tr align="center">
                                    <td colspan="2" style="background:#000;color:#fff;">IT Depart</td>
                                </tr>
                                <tr align="center">
                                    <td width="50%" style="background:#000;color:#fff;">Staff IT</td>
                                    <td width="50%" style="background:#000;color:#fff;">Manager IT</td>
                                </tr>
                                <tr align="center" style="height:75px;">
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr align="center">
                                    <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                    <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
</body>
</html>