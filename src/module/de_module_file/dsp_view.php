<?php
admlib::display_block_header();
// admlib::get_component('texteditorlib');
admlib::get_component('select2lib');
//select2lib
	admlib::get_component('datepickerlib');
admlib::get_component('uploadlib');
	admlib::get_component('formstart');
			// admlib::get_component('inputtext',
			// 	array(
			// 		"name"=>"code",
			// 		"value"=>app::ov($form['code']),
			// 		"validate"=>"required"
			// 	)
			// );

	?>
	<style>
		.monthselect{
			color: black;
		}
		.yearselect{
			color: black;
		}
	</style>
	<?php
			admlib::get_component('view',
				array(
					"name"=>"judul",
					"value"=>app::ov($form['name']),
					"validate"=>"required"
				)
			);
/* 			admlib::get_component('inputupload', 
				array(
					"name"=>"logo_crm", 
					"value"=> ($form['logo_crm'] != "" && file_exists($app['data_path'] ."/static/". $form['logo_crm']))?$app['_static'] ."/". $form['logo_crm']:null,
					"validate"=>"validate",
					"category"=>"image",
					"delete"=>"no",
					"size"=>"210px x 66px",
					"accept"=>"jpg|png|jpeg|gif|ico",
				)
			); */
			admlib::get_component('inputupload', 
				array(
					"name"=>"path", 
					"value"=> (!empty($form['path']) AND file_exists($app['pwebmin'] ."/assets/cmwi_file/". $form['path']))?$app['pwebmin'] ."/assets/cmwi_file/". $form['path']:null,
					"accept"=>"jpg|png|jpeg|gif|pdf|xls|docx|doc|xlsx|ico|txt",
					"validate"=>true
					// "value"=> $form['path'],
					// "filemedia"=>true
				)
			);
		/*	$rs["template_dokumen"] = db::get_record_select("name id, name","template_dokumen","status='active' ORDER BY name ASC");
			admlib::get_component('select2',
				array(
					"name"=>"template_dokumen",
					// "readonly"=>"yes",
					// "hide_label"=>"iya",
					// "style"=>"style='width: 45%;margin-left: 473px;position: relative;bottom: 43px;'",
					// "style_input"=>"width: 100%;",
					"validate"=>true,
					"value"=>$act=="add"?$app['me']['template_dokumen']:$form['template_dokumen'],
					"items"=>$rs['template_dokumen']
				)
			);*/
			$no_doc = db::lookup("max(no)",$module,"1=1");
			$no_doc = $no_doc +1;
			admlib::get_component('view',
				array(
					"name"=>"no_dokumen",
					// "readonly"=>"yes",
					// "value"=>($act=="add"?"IT-2-PK-".$no_doc:$form['no_dokumen']),
					"img_tooltip"=>$app['http']."/src/assets/imgs/static/img_no_dok.jpeg",
					"value"=>app::ov($form['no_dokumen']),
					// "validate"=>"required"
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"no_revisi",
					"value"=>app::ov($form['no_revisi']),
					// "validate"=>"required"
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"tgl_revisi",
					"value"=>app::ov($form['tgl_revisi']),
					// "validate"=>"required"
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"tgl_terbit",
					"value"=>app::ov($form['tgl_terbit']),
					"validate"=>"required"
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"tgl_berlaku",
					"value"=>app::ov($form['tgl_berlaku']),
					"validate"=>"required"
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"jml_hal",
					// "type"=>"number",
					"value"=>app::ov($form['jml_hal']),
					"validate"=>"required"
				)
			);
			admlib::get_component('view',
				array(
					"name"=>"dok_terkait",
					"value"=>app::ov($form['dok_terkait']),
					"validate"=>"required"
				)
			);
			/* admlib::get_component('select',
				array(
					"name"=>"level",
					"value"=>app::ov($form['level']),
					// "items"=>[["id"=>1,"name"=>"1 - staff"],["id"=>2,"name"=>"2 - supervisor"],["id"=>3,"name"=>"3 - Manager"],["id"=>4,"name"=>"4 - General Manager"],["id"=>5,"name"=>"5 - Direksi"],["id"=>6,"name"=>6],["id"=>7,"name"=>7]],
					"items"=>[["id"=>1,"name"=>"1 - staff"],["id"=>2,"name"=>"2 - supervisor"],["id"=>3,"name"=>"3 - Manager"],["id"=>4,"name"=>"4 - General Manager"],["id"=>5,"name"=>"5 - Direksi"]],
					"validate"=>"required"
				)
			);
			$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
			admlib::get_component('select',
				array(
					"name"=>"id_section",
					"value"=>app::ov($form['id_section']),
					"items"=>$rs['section']
				)
			); */
			// admlib::get_component('inputupload', 
			// 	array(
			// 		"name"=>"logo", 
			// 		"value"=> (isset($form['logo']) AND file_exists($app['logo_lib_path'] ."/". $form['logo']))?'/'. app::ov($form['logo']):null,
			// 		"filemedia"=>true
			// 	)
			// );
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"no_submit"=>"yes",
							// "cancel_link"=>$app['webmin'] ."/". admlib::$page_active['module'] .".mod",
							"status"=>$form['status'],
							"act"=>$act
					)
			);
	admlib::get_component('formend');
	?>
<script>
		/*$('#info_img [data-toggle="tooltip"]').tooltip({
		    animated: 'fade',
		    placement: 'bottom',
		    html: true
		});*/
		// alert("disini ");


	  $(document).ready(function() {
	  	$("#testing123").html('<a style="font-size: 20px;" id="magilla" href="#" title="" ><i class="test fa fa-info fa-fw" ></i></a>');
/*	   	$("#testing123 #magilla").tooltip({ content: '<img src="https://i.etsystatic.com/18461744/r/il/8cc961/1660161853/il_794xN.1660161853_sohi.jpg" />' }); */
	   	$("#testing123 #magilla").tooltip({ content: '<img width="550px" height="350px" src="<?= $app['http']."/src/assets/imgs/static/img_no_dok.jpeg" ?>" />' }); 

		});

		$('#template_dokumen').change(function() {
			// $("#g_function").show();
			var get_value = this.value;
			if (get_value) {
			// alert(get_value);
				$('#no_dokumen').val(get_value);			
			}else{
				$('#no_dokumen').val("");	
			}
			/*if (this.value == 'baru') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_function").show();
              	$("#g_own").show();
			}else if (this.value == 'ada') {
			 	$("#g_function").hide();
              	$("#g_own").hide();
			}*/
		});
</script>
	<?php
admlib::display_block_footer();
?>
