<?php
admlib::display_block_header();
admlib::get_component('texteditorlib');
admlib::get_component('uploadlib');
	admlib::get_component('datepickerlib');
	admlib::get_component('formstart');
			// admlib::get_component('inputtext',
			// 	array(
			// 		"name"=>"code",
			// 		"value"=>app::ov($form['code']),
			// 		"validate"=>"required"
			// 	)
			// );
	?>
	<style>
		.monthselect{
			color: black;
		}
		.yearselect{
			color: black;
		}
	</style>
	<?php
			admlib::get_component('datepicker',
				array(
					"name"=>"tgl_distribusi",
				"type"=>"number",
					"value"=>app::ov($form['tgl_distribusi']),
					"validate"=>"required"
				)
			);
/* 			admlib::get_component('inputupload', 
				array(
					"name"=>"logo_crm", 
					"value"=> ($form['logo_crm'] != "" && file_exists($app['data_path'] ."/static/". $form['logo_crm']))?$app['_static'] ."/". $form['logo_crm']:null,
					"validate"=>"validate",
					"category"=>"image",
					"delete"=>"no",
					"size"=>"210px x 66px",
					"accept"=>"jpg|png|jpeg|gif|ico",
				)
			); */
			/*admlib::get_component('inputupload', 
				array(
					"name"=>"path", 
					"value"=> (!empty($form['path']) AND file_exists($app['pwebmin'] ."/assets/cmwi_file/". $form['path']))?$app['pwebmin'] ."/assets/cmwi_file/". $form['path']:null,
					"accept"=>"jpg|png|jpeg|gif|pdf|xls|docx|doc|xlsx|ico|txt",
					// "value"=> $form['path'],
					// "filemedia"=>true
				)
			);*/
/*		$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section_penerbit",
				// "validate"=>true,
				"value"=>app::ov($form['id_section']),
				"items"=>$rs['section']
			)
		);*/
		// $rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('inputtext',
			array(
				"name"=>"id_section_penerbit",
				// "validate"=>true,
				"type"=>"number",
				"note"=>"(berapa copy)",
				"ganti_label"=>"bag_penerbit",
				"value"=>app::ov($form['id_section']),
				// "items"=>$rs['section']
			)
		);
/*		$rs["section_2"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section_terkait",
				// "validate"=>true,
				"value"=>app::ov($form['id_section_terkait']),
				"items"=>$rs['section_2']
			)
		);*/
		// $rs["section_2"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");

		admlib::get_component('inputtext',
			array(
				"name"=>"id_section_terkait",
				// "validate"=>true,
				"type"=>"number",
				"note"=>"(berapa copy)",
				"ganti_label"=>"bag_terkait",
				"value"=>app::ov($form['id_section_terkait']),
				// "items"=>$rs['section_2']
			)
		);		?>
<hr>
		<?php
		admlib::get_component('blank',
			array(
				"name"=>"lokasi_distribusi",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
			)
		);
		$rs["section_2"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section_distri",
				"validate"=>true,
				"value"=>app::ov($form['id_section_distri']),
				"items"=>$rs['section_2']
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"ket_distri",
				"value"=>app::ov($form['ket_distri']),
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"jum_distri",
				"type"=>"number",
				// "pattern"=>"yes",
				"value"=>app::ov($form['jum_distri']),
				"validate"=>"required"
			)
		);?>
<hr>
		<?php
		admlib::get_component('blank',
			array(
				"name"=>"dokumen_terkendali_kembali",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
			)
		);
		$rs["section_2"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section_terkendali",
				"validate"=>true,
				"value"=>app::ov($form['id_section_terkendali']),
				"items"=>$rs['section_2']
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"ket_terkendali",
				"value"=>app::ov($form['ket_terkendali']),
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"jum_terkendali",
				"type"=>"number",
				// "pattern"=>"yes",
				"value"=>app::ov($form['jum_terkendali']),
				"validate"=>"required"
			)
		);
			/*admlib::get_component('textarea',
				array(
					"name"=>"dokumen_terkendali_kembali",
					"value"=>app::ov($form['dokumen_terkendali_kembali']),
					"validate"=>"required"
				)
			);*/
			/* admlib::get_component('select',
				array(
					"name"=>"level",
					"value"=>app::ov($form['level']),
					// "items"=>[["id"=>1,"name"=>"1 - staff"],["id"=>2,"name"=>"2 - supervisor"],["id"=>3,"name"=>"3 - Manager"],["id"=>4,"name"=>"4 - General Manager"],["id"=>5,"name"=>"5 - Direksi"],["id"=>6,"name"=>6],["id"=>7,"name"=>7]],
					"items"=>[["id"=>1,"name"=>"1 - staff"],["id"=>2,"name"=>"2 - supervisor"],["id"=>3,"name"=>"3 - Manager"],["id"=>4,"name"=>"4 - General Manager"],["id"=>5,"name"=>"5 - Direksi"]],
					"validate"=>"required"
				)
			);
			$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
			admlib::get_component('select',
				array(
					"name"=>"id_section",
					"value"=>app::ov($form['id_section']),
					"items"=>$rs['section']
				)
			); */
			// admlib::get_component('inputupload', 
			// 	array(
			// 		"name"=>"logo", 
			// 		"value"=> (isset($form['logo']) AND file_exists($app['logo_lib_path'] ."/". $form['logo']))?'/'. app::ov($form['logo']):null,
			// 		"filemedia"=>true
			// 	)
			// );
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"no_submit"=>"yes",
							"act"=>$act
					)
			);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
