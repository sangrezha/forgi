<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	admlib::get_component('select2lib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"status_f",
		// 		"datas"=>["baru","ada"],
		// 		"value"=>app::ov($module['status_f'])
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"name_f",
		// 		"value"=>app::ov($module['name_f']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"type"=>"hidden",
		// 		"name"=>"name",
		// 		"value"=>app::ov($module['name']),
		// 	)
		// );
		if($form['no_pengajuan']){
			admlib::get_component('inputtext',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"validate"=>true,
					"readonly"=>"yes",
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('radio',
			array(
				"name"=>"status_f",
				"datas"=>["baru","ada"],
				"validate"=>true,
				"value"=>app::ov($form['status_f'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				"ganti_label"=>"nama_folder",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['name'])
			)
		);
		// print_r($app['me']);
		admlib::get_component('inputtext',
			array(
				"name"=>"function",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['function'])
			)
		);
/*		$rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_departement",
				"value"=>app::ov($app['me']['id_departement']),
				"items"=>$rs['departement']
			)
		);*/
/*		admlib::get_component('inputtext',
			array(
				"name"=>"own",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['own'])
			)
		);*/
		$rs["member"] = db::get_record_select("id, concat(nik,' - ',name) name","member","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"own",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"items"=>$rs['member'],
				"value"=>app::ov($form['own'])
			)
		);
		 ?>
		<div class="form-group">
		<label class="control-label col-md-1 col-sm-12 col-xs-12" for="tgl_countainer">&nbsp;</label>
		<!-- <div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="num_container">Container<span class="required"></span>		</label>
			<input type="text" name="p_num_container" id="num_container" value="" class="form-control has-feedback-left " validate="">
			<span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span>
		</div> -->
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="jenis_container">Nik<span class="required"></span>		</label>
			<select id="nik" name="p_nik" class="select2 form-control">
				<option value="" disabled selected><?php echo app::i18n('select');  ?></option>
				<?php 	
				$sql = "SELECT id,nik,name,id_section FROM ". $app['table']["member"] ."  ORDER BY nik asc	";
				db::query($sql, $rs['row'], $nr['row']);
				while($row = db::fetch($rs['row'])){ ?>
						<option value="<?= $row['id']; ?>"><?= $row['nik']; ?> - <?= $row['name']; ?></option>
				<?php } ?>
		</select>

		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="name_member">Name<span class="required"></span>		</label>
			<input type="text" name="p_name_member" id="name_member" value="" class="form-control has-feedback-left " validate="">


			<input type="text" style="display:none;" name="p_nik_m" id="nik_m" value="" class="form-control has-feedback-left " validate="">
			<input type="text" style="display:none;" 	name="p_name_m" id="name_m" value="" class="form-control has-feedback-left " validate="">
			<input type="text" style="display:none;" name="p_id_m" id="id_m" value="" class="form-control has-feedback-left " validate="">	

			<span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span>
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="jenis_container">Section<span class="required"></span>		</label>
			<input type="hidden" style="display: none;" name="p_id_section" id="id_section">
			<select id="id_section2" name="p_id_section2" class="form-control" readonly disabled>
				<option value=""><?php echo app::i18n('select');  ?></option>
				<?php 	
				$sql = "SELECT id,name FROM ". $app['table']["section"] ."  ORDER BY name asc";
				db::query($sql, $rs['row'], $nr['row']);
				while($row = db::fetch($rs['row'])){ ?>
						<option value="<?= $row['id']; ?>"><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label" for="jenis_container">Hak Akses<span class="required"></span>		</label>
			<select id="premission" name="p_premission" class="form-control">
				<option value="" selected disabled><?php echo app::i18n('select');  ?></option>
				<?php 	
				// $sql = "SELECT id,name FROM ". $app['table']["section"] ."  ORDER BY name asc";
				// db::query($sql, $rs['row'], $nr['row']);
				// while($row = db::fetch($rs['row'])){ ?>
						<option value="<?= "r" ?>">Read</option>
						<option value="<?= "w" ?>">Read & Write</option>
				<?php // } ?>
			</select>
		</div>
		<div style="margin-top:28px;">
			<!-- <button class="btn btn-success" onclick="add_user()">add</button> -->
			<a href="#" class="btn btn-success" onclick="add_user()">add</a>
		</div>
		<br>
				<div id="user_total"></div>
		
<?php 

		// admlib::get_component('submit',
		// 	array(
		// 		"id"=>(isset($id))?$id:"",
		// 		"act"=>$act
		// 	)
		// );
		
		$cek_creator = db::lookup("created_by",$module,"id",$id);
		if(!empty($id)){
			if($cek_creator==$app['me']["id"]){
			/*	admlib::get_component('submit',
					array(
						"id"=>(isset($id))?$id:"",
						"act"=>$act
					)
				);*/
				$cek_approve_2 = db::lookup("approve_2",$module,"id",$id);
				$cek_approve_3 = db::lookup("approve_3",$module,"id",$id);
				if ($cek_approve_2 =="iya" || $cek_approve_3 =="iya") {
					admlib::get_component('submit',
						array(
							"id"=>(isset($id))?$id:"",
							"no_submit"=>"iya",
							"act"=>$act
						)
					);
				}else{
					admlib::get_component('submit',
						array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
						)
					);
				}
			}else{
				// admlib::get_component('submit',
				// 	array(
				// 		"id"=>(isset($id))?$id:"",
				// 		"no_submit"=>"iya",
				// 		"act"=>$act
				// 	)
				// );
				admlib::get_component('submit_aprove',
					array(
						"id"=>(isset($id))?$id:"",
						// "no_submit"=>"iya",
						"act"=>"approve"
					)
				);
			}
		}else{
			admlib::get_component('submit',
				array(
					"id"=>(isset($id))?$id:"",
					"act"=>$act
				)
			);
		}
#echo "disini";
	admlib::get_component('formend');
	?>
	
<script>

$(document).ready(function () {

/*    $("form").submit(function(e){
	$.ajax({
			type:"GET",
			url:"<?php echo admlib::$page_active['module'] ?>.mod&act=cek_session",
				success:function(res){         
				var result = JSON.parse(res);

                                 if(result.session.cek=="no"){
                                    alert("<?= app::getliblang('detail_kosong') ?>");
				    stopEvent(e);
                                    e.preventDefault();
				    e.stop(true);
				    e.stopPropagation();
                                    return false;
                                  }else{
}
                                    return true;
				}
			}); 
	alert("<?= app::getliblang('input_diproses') ?>");
    }); */
<?php if($act == "add"){ ?>
        $("#form_submit").prop( "disabled", true );
<?php } ?>
	<?php if(empty($form['status_f']) || $form['status_f'] == "ada"){ ?>
		$("#g_function").hide();
        $("#g_own").hide();
	<?php } ?>
		$('input[type=radio][name=p_status_f]').change(function() {
			if (this.value == 'baru') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_function").show();
              	$("#g_own").show();
			}
			else if (this.value == 'ada') {
				// alert("Transfer Thai Gayo");
              	$("#g_function").hide();
              	$("#g_own").hide();
			}
		});
		$('#nik').change(function() {
			//alert("<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value);
			$.ajax({
			type:"GET",
			url:"<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value,
				success:function(res){         
						var result = JSON.parse(res);
						if(result.error == 0){
							// var loop = "";
							// console.log(result);12 
							// "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section
							console.log(JSON.stringify(result));
							// alert(JSON.stringify(result));
							// alert(result.member.id_section);
							$("#id_m").val(result.member.id);
							$("#name_m").val(result.member.name);
							$("#nik_m").val(result.member.nik);
							$("#name_member").val(result.member.name);
							////////////////////////////
							$("#id_section").val(result.member.id_section);
							///////////////////////////
							$("[name=p_id_section2]").val(result.member.id_section);
							// $('select[name="p_id_section"]').find('option:contains("'+result.member.id_section+'")').attr("selected",true);


							// loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
							// $.each(result.section,function(key,value){
							// 	loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
							// });
							// $("#id_title").html(loop);
							// console.log("loop " + loop);
						}else{
							alert("error");
							// $("#id_title").html('');
						}
				}
			}); 
		});
});

		function addData(act, param){
			// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>');
			// alert(param);
			var _param = $.extend({ act : act, step : 	'add' }, param);
			// alert('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			// console.log('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			$.post('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){ loadData();
        $("#form_submit").prop( "disabled", false ); });
			
			// $.ajax({
			// type:"POST",
			// url:"<?php echo admlib::$page_active['module'] ?>.mod",
			// data:_param,
			// 	success:function(res){ 
			// 		loadData();
			// 	}
			// }); 
			return false;
		}
		function add_user(){
			var act = "add_user", nik = $('#nik_m').val(), name = $('#name_m').val(), id = $('#id_m').val(), id_section = $('#id_section').val(), premission = $('#premission').val();
			// alert(act);
			// alert(name);
			// alert(id);
			// alert(id_section);
			// alert(nik);
			if(!act || !name || !id || !id_section || !nik || !premission){
				// if (!pib || !cont) 
				// {
					alert('tidah boleh kosong');
					return;
				// }
			}
			addData(act, { nik : nik, name : name, id : id, id_section : id_section , premission : premission });
			return false;
		
    };
	function loadData(){
		$('#user_total').load('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : "add_user" });
	};
	loadData();
	function delete_member(id_member){
		var r = confirm("Apakah anda yang ingin menghapus ?");
		// alert(id_member);
		// var _param = $.extend({ act : "add_user", step : 'delete', id : id_member }, param);
		var _param = { act : "add_user", step : 'delete', id : id_member };
		// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>'+_param);
		if (r == true) {
			$.post('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){  loadData(); });
		}
		return false;

	}
</script>
	<?php
admlib::display_block_footer();
?>