<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM PERMINTAAN AKSES FOLDER</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 landscape }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">
	<table width="90%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;">
	    <tbody>
            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="50%">&nbsp;</td>
            </tr>
            <tr>
                <td>	        	
                    <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 5px;color:#fff;">	        	
                FORM PENGAJUAN AKSES FOLDER <br> FILE SHARING SERVER
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <table width="100%" border="0" align="right" style="font-size:14px;">
                        <tbody>
                            <tr>
                                <td>Folder</td>
                                <td>:</td>
                                <td> 
                                    <input type="radio" id="baru" name="status_f" value="baru" <?= ($form['status_f']=="baru"?"checked":"") ?> disabled>
                                    <label for="baru">Baru</label>
                                    <input type="radio" id="ada" name="status_f" value="ada" <?= ($form['status_f']=="ada"?"checked":"") ?> disabled>
                                    <label for="ada">Ada</label>
                                </td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td><?=  $form['name'] ?></td>
                            </tr>
                            <?php if($form['status_f']=="baru"){ ?>
                            <tr>
                                <td>Function<br>(Isi bila folder baru )</td>
                                <td>:</td>
                                <td><?= $form['function']  ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td>Penanggung Jawab Folder</td>
                                <td>:</td>
                                <td><?= $form['own'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <!-- <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                        <tbody>
                            <tr align="center">
                                <td>
                                    Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
                             
                                    </td>
                            </tr>
                        </tbody>
                    </table> -->           
                    <table cellspacing="1" cellpadding="3" style="font-size: 12px;" align="right" width="50%">
                        <tbody>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $form['no_pengajuan'] ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Revisi</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                            </tr>
                       </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5">
                        <tr align="center" style="background:#10069f;color:#fff;">
                            <td>No</td>
                            <td>NIK</td>
                            <td>Nama</td>
                            <td>Jabatan</td>
                            <td colspan="2">Hak Akses
                            </td>
                        </tr>
                        <?php 
                            // $_SESSION['add_member'] = [];
                            $no = 1;
                            while($row = db::fetch($rs['add_member'])){
                                $user_form = db::get_record("member", "id", $row['id_user']);
                                $name_section = db::lookup("name","section", "id", $user_form['id_section']);
                                ?>
                        <tr align="center" style="background:#e6e6e6;color:#222;">
                            <td><?= $no ?></td>
                            <td><?= $row['nik'] ?></td>
                            <td><?= $user_form['name'] ?></td>
                            <td><?= $name_section ?></td>
                            <?php if($row['premission']=="w"){ ?>
                                <td>Read & Write</td>
                            <?php }else{ ?>
                                <td>Read</td>
                            <?php } ?>
                        </tr>
                         <?php $no++; } ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="font-size:12px;" align="right">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</td>
            </tr>
            <tr>
                <td colspan="3" align="right">
                    <table border="0" style="font-size:12px;" cellpadding="5">
                        <tbody>
                            <tr align="center">
                              <td width="20%" style="background:#000;color:#fff;">Manager IT</td>
                              <td width="20%" style="background:#000;color:#fff;">Staff IT</td>
                              <td width="10%" style="border:0px;">&nbsp;</td>
                              <td width="20%" style="background:#000;color:#fff;">Manager</td>
                              <td width="20%" style="background:#000;color:#fff;">Pemohon</td>
                            </tr>
                            <tr align="center" style="height:75px;">
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td><span style="font-size:20px;font-weight:bold;">&#10229;</span></td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                            </tr>
                            <tr align="center">
                              <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                              <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                              <td style="border:0px;">&nbsp;</td>
                              <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                              <td style="background:#000;color:#fff;"><?= $name_create ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
	   </tbody>
    </table>
    </section>
    

</body>

</html>