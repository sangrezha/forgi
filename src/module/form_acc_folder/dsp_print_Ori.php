<html><head>
	<style>
        body {
          background: rgb(204,204,204);
            /*width:842px;
            height:595px;*/
            border:1px solid black;
        }
        .black{background-color:#000;}
        page {
          background: white;
          display: block;
          margin: 0 auto;
           /*margin-bottom: 0.5cm;
         box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
        }
        page[size="A4"] {  
          width: 21cm;
          height: 29.7cm; 
        }
        page[size="A4"][layout="landscape"] {
          width: 29.7cm;
          height: 20cm;  
        }
        @media print {
          body, page[size="A4"] {
            margin: 0;
            box-shadow: 0;
          }
        }
	</style>
	<title></title>
</head>
<body>
    <page size="A4" layout="landscape">
	<table width="80%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;">
	    <tbody>
            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="50%">&nbsp;</td>
            </tr>
            <tr>
                <td>	        	
                    <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                <td class="black" colspan="2" align="center" style="background:;font-size: 20px;font-weight: bold;letter-spacing: 5px;color:#fff;">	        	
                FORM PERMINTAAN AKSES FOLDER
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <table width="100%" border="0" align="right" style="font-size:14px;">
                        <tbody>
                            <tr>
                                <td>Revisi</td>
                                <td>:</td>
                                <td><?= $revisi ?> - <?= date('d/m/Y', strtotime($form['created_at'])) ?></td>
                            </tr>
                            <tr>
                                <td>Folder</td>
                                <td>:</td>
                                <td> 
                                    <input type="radio" id="baru" name="status_f" value="baru" <?= ($form['status_f']=="baru"?"checked":"") ?> disabled>
                                    <label for="baru">Baru</label>
                                    <input type="radio" id="ada" name="status_f" value="ada" <?= ($form['status_f']=="ada"?"checked":"") ?> disabled>
                                    <label for="ada">Ada</label>
                                </td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td><?=  $form['name'] ?></td>
                            </tr>
                            <?php if($form['status_f']=="baru"){ ?>
                            <tr>
                                <td>Function<br>(Isi bila folder baru )</td>
                                <td>:</td>
                                <td><?= $form['function']  ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td>Penanggung Jawab Folder</td>
                                <td>:</td>
                                <td><?= $form['own'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <table border="0" width="100%" style="font-size:14px;" cellpadding="5">
                        <tr align="center" style="background:#10069f;color:#fff;">
                            <td>No</td>
                            <td>NIK</td>
                            <td>Nama</td>
                            <td>Jabatan</td>
                            <td colspan="2">Hak Akses
                            </td>
                        </tr>
                        <?php 
                            // $_SESSION['add_member'] = [];
                            $no = 1;
                            while($row = db::fetch($rs['add_member'])){
                                $user_form = db::get_record("member", "id", $row['id_user']);
                                $name_section = db::lookup("name","section", "id", $user_form['id_section']);
                                ?>
                        <tr align="center" style="background:#e6e6e6;color:#222;">
                            <td><?= $no ?></td>
                            <td><?= $row['nik'] ?></td>
                            <td><?= $user_form['name'] ?></td>
                            <td><?= $name_section ?></td>
                            <?php if($row['premission']=="w"){ ?>
                                <td>Read & Write</td>
                            <?php }else{ ?>
                                <td>Read</td>
                            <?php } ?>
                        </tr>
                         <?php $no++; } ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="font-size:14px;" align="right">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right">
                    <table border="0" style="font-size:14px;" cellpadding="5">
                        <tbody>
                            <tr align="center">
                              <td width="20%" style="background:#000;color:#fff;">Manager IT</td>
                              <td width="20%" style="background:#000;color:#fff;">Staff IT</td>
                              <td width="10%" style="border:0px;">&nbsp;</td>
                              <td width="20%" style="background:#000;color:#fff;">Manager</td>
                              <td width="20%" style="background:#000;color:#fff;">Pemohon</td>
                            </tr>
                            <tr align="center" style="height:75px;">
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td><span style="font-size:20px;font-weight:bold;">&#10229;</span></td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                            </tr>
                            <tr align="center">
                              <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                              <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                              <td style="border:0px;"></td>
                              <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                              <td style="background:#000;color:#fff;"><?= $form['name'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
	   </tbody>
    </table>
    <script>
         //window.print();
    </script>
    </page>
</body>
</html>