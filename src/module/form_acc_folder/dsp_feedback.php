<?php
admlib::display_block_header();
	admlib::get_component('texteditorlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
	

		admlib::get_component('radio',
			array(
				"name"=>"status_f",
				"datas"=>["baru","ada"],
				"validate"=>true,
				"value"=>app::ov($form['status_f'])
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"name",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['name'])
			)
		);
		// print_r($app['me']);
		admlib::get_component('view',
			array(
				"name"=>"function",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['function'])
			)
		);


		admlib::get_component('textarea',
			array(
				"name"=>"note",
				"value"=>$form['note']
			)
		);
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
