<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM PERMINTAAN HARDWARE & SOFTWARE</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 landscape }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">
	<table width="90%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;">
	    <tbody>
            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="50%">&nbsp;</td>
            </tr>
            <tr>
                <td>	        	
                    <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 5px;color:#fff;">	        	
                FORM PERMINTAAN HARDWARE & SOFTWARE
                </td>
            </tr>
            <tr valign="top">
                <td colspan="2">
                    <table border="0" width="100%" style="font-size:14px;" cellpadding="5">
                        <tbody>
                            <tr>
                                <td>Pemohon</td>
                                <td>:</td>
                                <td><?php 
                                $nik = db::lookup("nik","member","id",$form['created_by']);
                                $name_p = db::viewlookup("name","user","id",$form['created_by']);
                                $id_section = db::lookup("id_section","member","id",$form['created_by']);
                                $Section = db::lookup("name","section","id",$id_section);
                                echo $nik." - ".$name_p." - ".$Section; ?></td>
                            </tr>
                            <tr>
                                <td><?= app::getliblang('cost_to') ?></td>
                                <td>:</td>
                                <td><?php   
                                $cost_to = db::lookup("name","cost_centre","id",$form['cost_to']);
                                echo ($cost_to?$cost_to:$form['cost_to']); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <!-- <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                        <tbody>
                            <tr align="center">
                                <td>
                                    Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
                                

                                    </td>
                            </tr>
                        </tbody>
                    </table> -->

                    <table cellspacing="1" cellpadding="3" style="font-size: 12px;" align="right" width="50%">
                        <tbody>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $form['no_pengajuan'] ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Revisi</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                            </tr>
                       </tbody>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td colspan="2">
                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5" align="right">
                        <tbody>
                            <tr align="left" style="background:#000;color:#fff;height:30px;font-weight:bold;font-size:16px;">
                                <td colspan="5">&nbsp;&nbsp;HARDWARE</td>
                            </tr>
                            <tr align="center" style="background:#10069f;color:#fff;">
                                <td>No</td>
                                <td>Deskripsi</td>
                                <td>Jumlah</td>
                                <td>Satuan</td>
                                <td>Tujuan</td>
                            </tr>
                            <?php 
                            $categorynya ="hardware";
                            $no = 1;
                            if(count($hardware) > 0){
                            foreach($hardware as $key => $val){ 
                            ?>
                            <tr align="center" style="background:#e6e6e6;color:#222;">
                                <td><?= $no ?></td>
                                <?php if($categorynya == "hardware"){ ?>
                                <td><?= $val['desk'] ?></td>
                                <td><?= $val['jumlah'] ?></td>
                                <td><?= $val['satuan'] ?></td>
                                <?php } ?>
                                <td><?= $val['tujuan'] ?></td>
                            </tr>
                            <?php $no++; } }else{
                            while($no <= 4){ ?>
                            <tr align="center" style="background:#e6e6e6;color:#222;">
                                <td><?= $no ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php $no++; } } ?>
                        </tbody>
                    </table> 
                </td>
                <td>
                    <div style="font-size:12px;width: 100%; height: 120px;border:1px solid #000; padding:0 0 7px 0; line-height:15px;">
                        <ul style="list-style:number;"><strong>Catatan:</strong>
                            <li>Hardware dapat berupa Komputer, Printer, Flashdisk dan sejenisnya.</li>
                            <li>Software dapat berupa Aplikasi gratis maupun berlisensi.</li>
                            <li>Bila user memiliki refrensi silahkan dilampirkan untuk kebutuhan pembelian barang.</li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr valign="top">
                <td colspan="2">
                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5" align="right">
                        <tbody>
                            <tr align="left" style="background:#000;color:#fff;height:30px;font-weight:bold;font-size:16px;">
                                <td colspan="5">&nbsp;&nbsp;SOFTWARE</td>
                            </tr>
                            <tr align="center" style="background:#10069f;color:#fff;">
                                <td>No</td>
                                <td>Nama Software</td>
                                <td>Tujuan</td>
                            </tr>
                            <?php 
                            $categorynya ="software";
                            $no = 1;
                            if(count($software) > 0){
                            foreach($software as $key => $val){
                            ?>
                            <tr align="center" style="background:#e6e6e6;color:#222;">
                                <td><?= $no ?></td>
                                <td><?= $val['name_soft'] ?></td>
                                <td><?= $val['tujuan'] ?></td>
                            </tr>
                            <?php $no++; } }else{
                            while($no <= 4){ ?>
                            <tr align="center" style="background:#e6e6e6;color:#222;">
                                <td><?= $no ?></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php $no++; } } ?>
                        </tbody>
                    </table> 
                </td>
                <td>
                    <div style="font-size:12px;width: 100%; height: 120px;border:1px solid #000; padding:0 0 7px 0; line-height:15px;">
                        <ul style="list-style:number;"><strong>Catatan:</strong>
                            <li>Sehubungan dengan Undang-Undang tentang Hak Cipta, maka IT tidak melayani permintaan produk ilegal.</li>
                            <li><ul style="margin-left:-40px;">Software khusus
                                    <li style="margin-left:12px;">Benar - benar dipakai untuk pekerjaan sehari-hari</li>
                                    <li style="margin-left:12px;">User minimal sudah mempunyai background pengetahuan tentang software yang diminta dan mampu mengoprasikannya</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr align="right">
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr style="font-size:12px;" align="right">
                <td colspan="3">
                    Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td>
                    <table border="0" style="font-size:12px;" cellpadding="5" width="100%">
                        <tbody>
                            <tr style="text-align: center;">
                                <td width="20%" style="background:#000;color:#fff;">Staff IT</td>
                                <td width="20%" style="background:#000;color:#fff;">Manager IT</td>
                                <td width="5%">&nbsp;</td>
                                <td width="20%" style="background:#000;color:#fff;">Manager</td>
                                <td width="20%" style="background:#000;color:#fff;">Supervisor</td>
                            </tr>
                            <tr align="center" style="height:50px;">
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td><span style="font-size:20px;font-weight:bold;">&#10229;</span></td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                            </tr>
                            <tr style="text-align: center;">
                                <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                                <td>&nbsp;</td>
                                <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                                <td style="background:#000;color:#fff;"><?= $approve_spv_name ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
	   </tbody>
    </table>
    </section>
    

</body>

</html>