<?php
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');
$p_masa_simpan = 2;
## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('module'=>'form_hardware_software','caption'=>ucwords('Permintaan Hardware & Software'));
// $p_masa_simpan = 0;
   $ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
else
    $ipaddress = 'UNKNOWN';
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$module = admlib::$page_active['module'];
/*	$total 	= db::lookup('COUNT(id)', $module, '1=1');
	app::set_default($page_size, (isset($all)?$total:10));*/
	if($app['me']['level']==1){
		if($app['me']['code']!="it"){
			$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.id ='".$app['me']['id']."'";
		}else{
			$where = " AND a.status_progress NOT LIKE 'unverified' AND a.status_progress NOT LIKE 'partly'";
		}
	}
	// if($app['me']['level']==2){
	// 	$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.level <='2'";
	// }
	// if($app['me']['level']>2){
	// 	$where = " AND b.id_section ='".$app['me']['id_section']."'";
	// }
	if($app['me']['level']==2){
		$where = " AND b.id_section ='".$app['me']['id_section']."'";
	}
	if($app['me']['level']>2){
		if($app['me']['code']!="it"){
			// $where = " AND b.id_section ='".$app['me']['id_section']."' AND b.id ='".$app['me']['id']."'";

            $cek_spv   = db::lookup("id","title","id_section='".$app['me']['id_section']."' AND level='2'");
            if ($cek_spv) {
				$where = " AND b.id_section ='".$app['me']['id_section']."' AND a.approve_2 !='tidak' ";
            }else{
				$where = " AND b.id_section ='".$app['me']['id_section']."'";
            }
		}else{
			$where = " AND (a.status_progress NOT LIKE 'unverified' AND a.status_progress NOT LIKE 'partly') AND a.approve_it !='tidak'";
			// $where = " AND b.id_section ='".$app['me']['id_section']."'";
		}
	}
	$q = "WHERE 1=1";
	if($ref)
	{
		$q .= " AND a.name LIKE '%". $ref ."%' OR a.status_progress LIKE '%". $ref ."%'";
	}
	// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$module] ." a
	// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	// 	$q ORDER BY a.lang,a.reorder";
	$sql = "SELECT a.*, a.approve_2 approve_2_status, a.approve_3 approve_3_status, a.created_by id_created_by, b.name as created_by,b.nik nik,b.id_section id_section, c.name as modifyby FROM ". $app['table'][$module] ." a
	LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['view']['user'] ." c ON (a.updated_by=c.id)
	$q $where OR a.created_by ='".$app['me']['id']."' ORDER BY a.created_at desc";


	db::query($sql, $rs['test'], $nr['test']);
	$total 	= $nr['test'];
	app::set_default($page_size, (isset($all)?$total:10));
	
	app::set_navigator($sql, $nav, $page_size, $module .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ["no_pengajuan","yg_mengajukan","tgl_pengajuan_tbl"];
	$columns = array_merge($columns,['status_progress']);
	// progress
    //echo $sql;exit;
	if($app['me']['code'] =="it"){
		// $columns = ["feedback",'approve_it',"finish_it"];
		// $columns = array_merge($columns,["view",'approve_it',"approve_it_3"]);
		$columns = array_merge($columns,["view"]);
	}else{
		// $columns = array_merge($columns,['approve_2','approve_3','confirm_done']);
		// $columns = array_merge($columns,['confirm_done']);
		$columns = array_merge($columns,['approve_2','approve_3','confirm_done']);
	}
	// $columns = ['approve_2','approve_3','approve_it','confirm_done','name'];
	$numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	if($numlang > 1)
	{
		$columns = array_merge($columns,['lang','duplicate']);
	}
	$columns = array_merge($columns,['print']);
	// $columns = array_merge($columns,["approve"]);
	while($row = db::fetch($rs['row']))
	{
		$row['name'] = $row['name'];

		// $row['yg_mengajukan'] = $row['created_by'];
		$section = db::lookup("name","section","id",$row['id_section']);
		$row['yg_mengajukan'] = $row['nik']." - ".$row['created_by']." - ".$section;



		$row['tgl_pengajuan_tbl'] = $row['created_at'];
		///////////////////
		$row['view'] = '<a style="margin-left: 45% !important;" href="'.$app['webmin'] .'/'. $module .'.mod&act=view&id='. $row['id'].'"><i class="fa fa-eye" aria-hidden="true"></i></a>';
		if($row['approve_2']=="tidak" && $app['me']['level'] =="2"){
			// $row['approve_2'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
			$row['approve_2'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="on_reject(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
				$row['approve_3'] ="";
				$row['approve_4'] ="";
				$row['approve_5'] ="";
			// fa-times
		}elseif($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			if($row['approve_2']!="tidak"){
				$row['approve_2'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				if($row['approve_3']=="tidak" && $app['me']['level'] =="3"){
					if($app['me']['level'] == "3"){
						$row['approve_3'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="on_reject(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
					}else{
						$row['approve_3'] = '';
					}
				}elseif($row['status_progress']=="rejected"){
					// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
					$row['approve_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
				}else{
					// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
					if($row['approve_3']!="tidak"){
						$row['approve_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
					}else{
						$row['approve_3'] ="";
					}
				}
			}else{
				$row['approve_2'] ="";
				$row['approve_3'] ="";
			}
		}
		// echo "disini".$row['approve_it_3']." oke";
		if($row['approve_it']=="tidak"){
			// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';

			if($app['me']['level'] == "1"){
				$row['approve_it'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			}else{
				$row['approve_it'] = "";
			}
			$row['finish_it'] = "";
			$row['approve_it_3'] = "";
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			if($row['approve_it']=="iya"){
				$row['approve_it'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}else{
				$row['approve_it'] = '';
			}
			// approve_3_it
			if($row['approve_it_3']!="iya" && $app['me']['level'] == "3"){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				// echo "disini";
				$row['approve_it_3'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it_3&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			}else{
				if($row['approve_it_3']!="iya"){
					$row['approve_it_3'] = '';
				}else{
					$row['approve_it_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				}
				// if($row['finish_it']=="tidak" || empty($row['finish_it'])){
				// 	// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				// 	$row['finish_it'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=finish_it&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
				// }else{
				// 	// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				// 	$row['finish_it'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				// }
			}
		}
		if(($row['status_progress']=="progress" || $row['status_progress']=="finished") && $row['id_created_by'] == $app['me']['id']){
			if($row['confirm_done']=="tidak" || empty($row['confirm_done'])){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				$row['confirm_done'] = '<a style="margin-left: 45% !important;color:orange;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&act=confirm_done&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			}else{
				// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				$row['confirm_done'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}
		}else{
			if($row['confirm_done']=="tidak" || empty($row['confirm_done'])){
				$row['confirm_done'] = '';
			}else{
				$row['confirm_done'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}
		}
		if($row['status_progress']=="progress" || $row['status_progress']=="finished"){
			$row['print']='<a  target="_blank" href="'.$app['webmin'] .'/'. $module .'.mod&act=print&id='. $row['id'].'&name='.$row['name'].'" ><i style="margin-left: 30% !important;color:grey;font-size:18px" class="fa fa-print"></i></a>';
		}else{
			// $row['print']='<a href="'.$app['webmin'] .'/'. $module .'.mod&act=print&id='. $row['id'].'&name='.$row['name'].'" ><i style="margin-left: 30% !important;color:grey;font-size:18px" class="fa fa-print"></i></a>';
			$row['print']='';
		}
		// if($row['status_progress']!="iya"){
		// 	// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		// 	$row['status_progress'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
		// }else{
		// 	// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
		// 	$row['status_progress'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
		// }
		// $row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=edit&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
		if($app['me']['code'] == "it"){
			$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=view&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
		}else{
			$row['no_pengajuan'] = '<a title="Edit" href="'.$module.'.mod&amp;act=edit&amp;id='.$row['id'].' ">'.$row['no_pengajuan'].'</a>';
		}
		if($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_it'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['approve_it_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
			$row['confirm_done'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}
		if ($row['approve_2_status'] == "iya") {
			$row['approve_2']="Disetujui";
		}elseif($row['approve_2_status'] == "reject" || $row['status_progress']=="rejected"){
			$row['approve_2']="Ditolak";
		}else{
			$row['approve_2']="Menunggu";
		}
		if ($row['approve_3_status'] == "iya") {
			$row['approve_3']="Disetujui";
		}elseif($row['approve_3_status'] == "reject" || $row['status_progress']=="rejected"){
			$row['approve_3']="Ditolak";
		}else{
			$row['approve_3']="Menunggu";
		}
		if($numlang > 1)
		{
			$row['duplicate'] = '<a href="'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		}
		else {
			$row['duplicate'] = null;
		}
		$row_status_lama = $row['status_progress'];
		$row['status_progress'] = app::getliblang("table_".$row['status_progress']);
		if ($row_status_lama =="rejected") {
			// $row['status_progress'] = db::lookup("id_user","log", "(id_form='".$row['id']."' AND act ='APR') AND module ='".$module."' order by created_at desc");
			$get_reject = db::lookup("id_user","log", "(id_form='".$row['id']."' AND act ='APR') AND module ='".$module."' order by created_at desc");
			$row['status_progress'] = $row['status_progress']."<br>[ ".db::viewlookup("name","user", "id='$get_reject'")." ] ";
		}
		$results[] = $row;
	}
	$option = ['sortable'=>false, 'status'=>true];
	if($app['me']['code']=="it"){
		$option = array_merge($option,['no_edit'=>true]);
	}
	$unpost = 1;
	$unmodify=1;
	include $app['pwebmin'] ."/include/blk_list.php";





	// app::sendmail("sam.developer009@gmail.com", "Ada Form yng harus di approve", "<h3>disini</h3>");
	app::sendmail2("sam.developer009@gmail.com", "Ada Form yng harus di approve", "<h3>disini</h3>");
	exit;
endif;
?>
<?php
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		form::populate($module);
		// $_SESSION["add_devices"] = "";
		unset($_SESSION['add_devices']);
		$rs['chk_cost'] = db::get_record_select("id, name", "cost_centre", "1=1");
	
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		if($app['me']["code"] != "it"){
			// if($status_f == "baru"){
			// 	$validate = 'p_name,p_cost_to,p_function';
			// }else{
				$validate = 'name_p,p_cost_to';
			// }
				// print_r($_REQUEST);exit;
			form::serialize_form();
			form::validate('empty', $validate);
			if (form::is_error()):
				msg::build_msg();
				header("location: ". $module .".mod&act=add&error=1");
				exit;
			endif;
			// print_r($_SESSION);
			// var_dump($_SESSION);
			// exit;
			// print_r($_REQUEST);
			app::mq_encode('p_name,p_cost_to,p_function,p_own');
			// $identity = rand(1, 100).date("dmYHis");
			// $p_images = str_replace("%20"," ","$p_images");
			// $sql_lang 	= "SELECT id FROM ". $app['table']['lang'] ." WHERE status='active'";
			$p_alias = app::slugify($p_title);
			// db::query($sql_lang, $rs['lang'], $nr['lang']);
			// while($row 	= db::fetch($rs['lang']))
			// {
				if($app['me']['level'] >= 3){
					$approved 		= ",approve_2,approve_3";
					$approved_val 	= ",'iya','iya'";
					$status_progress 	= ",status_progress";
					$status_progress_val 	= ",'fully'";
					$log_approve = "APR_MNG";


					$sql_email = "select a.email from ". $app['table']['member'] ." a 
								   inner join ". $app['table']['title'] ." c on(a.id_title=c.id) 
								   where c.code = 'it' AND c.level = '1' ";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
				}
				if($app['me']['level'] == 2){
					$approved 		= ",approve_2";
					$approved_val 	= ",'iya'";
					$status_progress 	= ",status_progress";
					$status_progress_val 	= ",'partly'";
					$log_approve = "APR_SPV";

					$sql_email = "select a.email from ". $app['table']['member'] ." a 
								   inner join ". $app['table']['title'] ." c on(a.id_title=c.id) 
								   where c.level = '3' AND a.id_section = '".$app['me']['id_section']."'";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
				}
				if ($app['me']['level']==1) {
					$sql_email = "select a.email from ". $app['table']['member'] ." a 
								   inner join ". $app['table']['title'] ." c on(a.id_title=c.id) 
								   where c.level = '2' AND a.id_section = '".$app['me']['id_section']."'";
					db::query($sql_email, $rs['sql_email'], $nr['sql_email']);
				}
				// echo $sql_email;
				// exit;
				while($row = db::fetch($rs['sql_email'])){
					// app::sendmail($row['email'], "Ada Form yng harus di approve", $message);
					$recipient .= "'".$row['email']."',";
				}
				$p_req_hardware =  $_SESSION["add_devices"]['hardware'];
				$p_req_software =  $_SESSION["add_devices"]['software'];
				$p_device ="";
				if (count($p_req_hardware)) {
					$p_device .="hardware;";
				}
				if (count($p_req_software)) {
					$p_device .="software;";
				}
				$p_device = rtrim($p_device,";");

				// $p_category = $p_device;

				$id 		= rand(1, 100).date("dmYHis");
				// $p_lang 	= $row['id'];
				$id 		= db::cek_id($id,$module);
				$urut 		= db::lookup("max(reorder)",$module,"1=1");
				// $p_cost_to 	= implode(";", $p_cost_to);

				// $p_cost_to 	= implode(";", $p_cost_to);
				$no_urut	= db::lookup("max(no_urut)",$module,"1=1");
				$no_urut	= $no_urut+1;
				$p_no_pengajuan = "GN-001-".date("dmy")."-".$no_urut;
				$get_name_p = db::lookup("name","member","id",$name_p);
				// $data = db::get_record("feedback","id_project ='$id' AND id_client = '$p_client_id'");
				if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
				$sql 		= "INSERT INTO ".$app['table'][$module]."
						(id, name_p, id_pemohon, no_pengajuan, no_urut, id_user, category, cost_to, reorder, created_by, created_at$approved $status_progress) VALUES
						('$id', '$get_name_p', '$name_p', '$p_no_pengajuan', '$no_urut', '".$app['me']['id']."', '$p_device', '$p_cost_to', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";

				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "INS",$id,$p_masa_simpan);
					
					$code = "";
					$id_section = $app['me']['id_section'];
					$link = $app['http'] ."/". $module .".mod";
					if($app['me']['level'] == 1){
						$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
						if($get_spv !=""){
							$level = "2";
						}else{
							$level = "3";
						}
					}elseif($app['me']['level'] == 2){
						$level = "3";
					}elseif($app['me']['level'] == 3){
						$level = "1";
						$code = "it";
						$id_section = db::lookup("id","section","code","it");
					}
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section");
					db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
					
					if($app['me']['level'] > 1){
						db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id,$p_masa_simpan);
					}


					// app::sendmail($recipient, "Ada form yang harus di Approve", $message);
					
				// $add_devices 		= $_SESSION['add_devices'][$p_category];
				/*if(count($add_devices)>0)
				{
					foreach($add_devices as $row )
					{
						// print_r($row);
						// echo "<br>";
						// echo $row["id"];
						// exit;
						if (!empty($row['id'])) 
						{
							if($p_category == "software"){
								$idz 		= rand(1, 100).date("dmYHis");
								$idz 		= db::cek_id($idz,"data_form_software");
								$sqlx = "insert into ". $app['table']['data_form_software'] ."
								(id, id_user, name_soft, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['name_soft']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";
							}else{
								$idz 		= rand(1, 100).date("dmYHis");
								$idz 		= db::cek_id($idz,"data_form_hardware");
								$sqlx = "insert into ". $app['table']['data_form_hardware'] ."
								(id, id_user, deskripsi, jumlah, satuan, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['desk']."', '".$row['jumlah']."', '".$row['satuan']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";
							}
							db::qry($sqlx);
		
						}
					}
					unset($_SESSION['add_devices']);
				}*/
					// if(count($add_devices)>0)
					// {
						foreach($p_req_hardware as $row )
						{
							if (!empty($row['id'])) 
							{
								// if($p_category == "software"){
								// 	$idz 		= rand(1, 100).date("dmYHis");
								// 	$idz 		= db::cek_id($idz,"data_form_software");
								// 	$sqlx = "insert into ". $app['table']['data_form_software'] ."
								// 	(id, id_user, name_soft, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['name_soft']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";
								// }else{
									$idz 		= rand(1, 100).date("dmYHis");
									$idz 		= db::cek_id($idz,"data_form_hardware");
									$sqlx = "insert into ". $app['table']['data_form_hardware'] ."
									(id, id_user, deskripsi, jumlah, satuan, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['desk']."', '".$row['jumlah']."', '".$row['satuan']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";
								// }
								db::qry($sqlx);
							}
						}
						foreach ($p_req_software as $row) {

							if (!empty($row['id'])) 
							{
									$idz 		= rand(1, 100).date("dmYHis");
									$idz 		= db::cek_id($idz,"data_form_software");
									$sqlx = "insert into ". $app['table']['data_form_software'] ."
									(id, id_user, name_soft, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['name_soft']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";

									db::qry($sqlx);
							}
						}
						unset($_SESSION['add_devices']);
					// }
				}

				
			// }
			msg::set_message('success', app::i18n('create'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}else{
			msg::set_message('error', app::i18n('it_not_add'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		// echo $module;
		// echo $id;
		$form = db::get_record($module, "id", $id);
		$categorynya = $form['category'];
		// $_SESSION["add_devices"] = "";
		unset($_SESSION['add_devices']);
		// $_SESSION['add_devices'][$id] = ['nik'=>$nik, 'name'=>$name, 'id'=>$id, 'id_section'=>$id_section, 'name_section'=>$name_section, 'premission'=>$premission, 'name_section'=>$name_section];
		// $rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		$categorynya = "software";
		// if($categorynya=="software"){
			$rs['add_devices'] = db::get_record_select("*", "data_form_".$categorynya, "id_form=".$id);
		/*}else{
			$rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		}*/
		$_SESSION['add_devices'] = [];
		while($row = db::fetch($rs['add_devices'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
			// $row['name'] = $user_form['name'];
			// $row['name_section'] = $name_section;
			$_SESSION['add_devices'][$categorynya][$row['id']]= $row;
		}
		$categorynya = "hardware";
		/*if($categorynya=="hardware"){
			$rs['add_devices'] = db::get_record_select("*", "data_form_".$categorynya, "id_form=".$id);
		}else{*/
			$rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		// }
		// $_SESSION['add_devices'] = [];
		while($row = db::fetch($rs['add_devices'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
			// $row['name'] = $user_form['name'];
			// $row['name_section'] = $name_section;
			$_SESSION['add_devices'][$categorynya][$row['id']]= $row;
		}

		// print_r($_SESSION['add_devices']);
		// echo $form['created_by'] ."==". $app['me']['id'];exit;
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");
		// print_r($_SESSION['add_devices']);
		$rs['chk_cost'] = db::get_record_select("id, name", "cost_centre", "1=1");
		form::populate($module);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		// if($status_f == "baru"){
		// 	$validate = 'p_name,p_status_f,p_function';
		// }else{
			$validate = 'name_p,p_cost_to';
		// }
		$form_lama = db::get_record($module, "id", $id);
		form::serialize_form();
		form::validate('empty', $validate);
		if (form::is_error()):
			msg::build_msg();
			header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$p_images = str_replace("%20"," ","$p_images");
		$p_alias = app::slugify($p_title);
		$p_category = $p_device;
		// $p_cost_to 	= implode(";", $p_cost_to);
		app::mq_encode('p_title,p_content,p_image,p_lang');


		// $sql 		= "INSERT INTO ".$app['table'][$module]."
		// (id, name, id_user, category, cost_to, reorder, created_by, created_at$approved $status_progress) VALUES
		// ('$id', '$p_name', '".$app['me']['id']."', '$p_category', '$p_cost_to', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";


		$data = db::get_record($module, "id", $id);
		$edit_before= "";
		$edit_after = "";

		$get_name_p = db::lookup("name","member","id",$name_p);
		if ($data['name_p'] !=$name_p) {
			$edit_before .= "name_p : ".$data['name_p'].";";
			$edit_after .= "name_p : ".$get_name_p .";";
		}
		if ($data['category'] !=$p_category) {
			$edit_before .= "category : ".$data['category'].";";
			$edit_after .= "category : ".$p_category.";";
		}
		if ($data['alasan'] !=$p_alasan) {
			$edit_before .= "alasan : ".$data['alasan'].";";
			$edit_after .= "alasan : ".$p_alasan.";";
		}
		if ($data['cost_to'] !=$p_cost_to) {
			$edit_before .= "cost_to : ".$data['cost_to'].";";
			$edit_after .= "cost_to : ".$p_cost_to.";";
		}
		// if ($data['name'] !=$p_name) {
		// 	$edit_before .= $data['name'].";";
		// 	$edit_after .= $p_name.";";
		// }
		$edit_before = rtrim($edit_before,";");
		$edit_after  = rtrim($edit_after,";");
		
		$p_req_hardware =  $_SESSION["add_devices"]['hardware'];
		$p_req_software =  $_SESSION["add_devices"]['software'];
		$p_device ="";
		if (count($p_req_hardware)) {
			$p_device .="hardware;";
		}
		if (count($p_req_software)) {
			$p_device .="software;";
		}
		$p_device = rtrim($p_device,";");

		/*$sql = "update ". $app['table'][$module] ."
				set name_p = '$name_p',
					category = '$p_category',
					cost_to = '$p_cost_to',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";*/
		$sql = "update ". $app['table'][$module] ."
				set name_p = '$get_name_p',
					id_pemohon = '$name_p',
					category = '$p_device',
					cost_to = '$p_cost_to',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "UPD",$id, $p_masa_simpan,$ipaddress, "",$edit_before, $edit_after);
			// $add_devices 		= $_SESSION['add_devices'][$p_category];
			// if(count($add_devices)>0)
			// {
			// 	// echo "DELETE FROM ". $app['table']["folder_user"] ." WHERE `id_form` IN ('". $id ."')";
			// 	// exit;
			// 	db::qry("DELETE FROM ". $app['table']["data_form_".	$form_lama['category']] ." WHERE `id_form` IN ('". $id ."')");
			// 	foreach($add_devices as $row )
			// 	{
			// 		// print_r($row);
			// 		// echo "<br>";
			// 		// echo $row["id"];
			// 		// exit;

			// 		if (!empty($row['id'])) 
			// 		{
			// 			if($p_category == "software"){
			// 				$idz 		= rand(1, 100).date("dmYHis");
			// 				$idz 		= db::cek_id($idz,"data_form_software");
			// 				$sqlx = "insert into ". $app['table']['data_form_software'] ."
			// 				(id, id_user, name_soft, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['name_soft']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";
			// 			}else{
			// 				$idz 		= rand(1, 100).date("dmYHis");
			// 				$idz 		= db::cek_id($idz,"data_form_hardware");
			// 				 $sqlx = "insert into ". $app['table']['data_form_hardware'] ."
			// 				(id, id_user, deskripsi, jumlah, satuan, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['desk']."', '".$row['jumlah']."', '".$row['satuan']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";
			// 			}
			// 			// echo $sqlx;
			// 			// exit;
			// 			db::qry($sqlx);
	
			// 		}
			// 	}
			// 	unset($_SESSION['add_devices']);
			// }

		// $p_req_hardware =  $_SESSION["add_devices"]['hardware'];
		// $p_req_software =  $_SESSION["add_devices"]['software'];
			if(count($p_req_hardware)>0)
			{
				db::qry("DELETE FROM ". $app['table']["data_form_hardware"] ." WHERE `id_form` IN ('". $id ."')");
				foreach($p_req_hardware as $row )
				{
					if (!empty($row['id'])) 
					{
						// if($p_category == "software"){
						// 	$idz 		= rand(1, 100).date("dmYHis");
						// 	$idz 		= db::cek_id($idz,"data_form_software");
						// 	$sqlx = "insert into ". $app['table']['data_form_software'] ."
						// 	(id, id_user, name_soft, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['name_soft']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";
						// }else{
							$idz 		= rand(1, 100).date("dmYHis");
							$idz 		= db::cek_id($idz,"data_form_hardware");
							$sqlx = "insert into ". $app['table']['data_form_hardware'] ."
							(id, id_user, deskripsi, jumlah, satuan, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['desk']."', '".$row['jumlah']."', '".$row['satuan']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";
						// }
						db::qry($sqlx);
					}
				}
			}

			if(count($p_req_software)>0)
			{
				db::qry("DELETE FROM ". $app['table']["data_form_software"] ." WHERE `id_form` IN ('". $id ."')");
				foreach ($p_req_software as $row) {

					if (!empty($row['id'])) 
					{
							$idz 		= rand(1, 100).date("dmYHis");
							$idz 		= db::cek_id($idz,"data_form_software");
							$sqlx = "insert into ". $app['table']['data_form_software'] ."
							(id, id_user, name_soft, created_by, id_form, tujuan) values ('$idz', '".$row['id']."', '".$row['name_soft']."', '".$app['me']['id']."', '$id', '".$row['tujuan']."')";

							db::qry($sqlx);
					}
				}
			}
		}
			unset($_SESSION['add_devices']);
		msg::set_message('success', app::i18n('modify'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : view
*******************************************************************************/
if ($act == "view"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		// echo $module;
		// echo $id;
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");
		$form = db::get_record($module, "id", $id);
		$categorynya = $form['category'];
		// $_SESSION["add_devices"] = "";
		unset($_SESSION['add_devices']);
		// $_SESSION['add_devices'][$id] = ['nik'=>$nik, 'name'=>$name, 'id'=>$id, 'id_section'=>$id_section, 'name_section'=>$name_section, 'premission'=>$premission, 'name_section'=>$name_section];
		// $rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		/*if($categorynya=="software"){
			$rs['add_devices'] = db::get_record_select("*", "data_form_".$categorynya, "id_form=".$id);
		}else{
			$rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		}
		$_SESSION['add_devices'] = [];
		while($row = db::fetch($rs['add_devices'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
			// $row['name'] = $user_form['name'];
			// $row['name_section'] = $name_section;
			$_SESSION['add_devices'][$categorynya][$row['id']]= $row;
		}*/
		
		$categorynya = "software";
		// if($categorynya=="software"){
			$rs['add_devices'] = db::get_record_select("*", "data_form_".$categorynya, "id_form=".$id);
		/*}else{
			$rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		}*/
		$_SESSION['add_devices'] = [];
		while($row = db::fetch($rs['add_devices'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
			// $row['name'] = $user_form['name'];
			// $row['name_section'] = $name_section;
			$_SESSION['add_devices'][$categorynya][$row['id']]= $row;
		}
		$categorynya = "hardware";
		/*if($categorynya=="hardware"){
			$rs['add_devices'] = db::get_record_select("*", "data_form_".$categorynya, "id_form=".$id);
		}else{*/
			$rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		// }
		// $_SESSION['add_devices'] = [];
		while($row = db::fetch($rs['add_devices'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
			// $row['name'] = $user_form['name'];
			// $row['name_section'] = $name_section;
			$_SESSION['add_devices'][$categorynya][$row['id']]= $row;
		}

		// print_r($_SESSION['add_devices']);
		$rs['chk_cost'] = db::get_record_select("id, name", "cost_centre", "1=1");
		form::populate($module);
		include "dsp_view.php";
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	else:
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table'][$module]."
			SET status = '$statusnya'
			WHERE identity = '$identity'";
	db::qry($sql);
	msg::set_message('success', app::i18n('update'));
	header("location: " . $app['webmin'] ."/". $module .".mod");
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	$module = admlib::$page_active['module'];
	if($step == 1)
	{
		if($id) $items = $id;
		else $items = implode("','", $p_del);
		$sql = "SELECT id, name_p title FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $delid ."')";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "DEL",$delid, $p_masa_simpan);
			db::qry("DELETE FROM ". $app['table']["folder_user"] ." WHERE `id_form` IN ('". $delid ."')");
			$sql2 	= "DELETE FROM ". $app['table']["notif"] ." WHERE `id_form` = '". $delid ."' AND from_form = '".$module."'";
			db::qry($sql2);
		}
		msg::set_message('success', app::i18n('delete'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$p_title 		= $row['title'];
			$p_content	 	= $row['content'];
			$p_image 		= $row['image'];

			$urut = db::lookup("max(reorder)", $module, "1=1");
			if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
			$id = rand(1, 100).$urut.date("dmYHis");

			app::mq_encode('p_title,p_content');
			$sql = "INSERT INTO ". $app['table'][$module] ."
				(id, title, content, status, reorder, created_by, created_at) VALUES
				('$id', '$p_title', '$p_content', 'inactive', '$urut', '". $app['me']['id'] ."', now())";
			db::qry($sql);
		}
		msg::set_message('success', app::i18n('create'));
	}
	else
	{
		msg::set_message('error', app::i18n('notfound'));
	}
	header("location: " . $app['webmin'] ."/". $module .".mod");
	exit;
endif;

/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $count ++;
			$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : approve
*******************************************************************************/
if ($act == "approve"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	// print_r($_REQUEST);
	// if($type=="approve_2"){

		// if($app["me"]["level"] >= 2){
		if($app["me"]["level"] > 1){
			if($reject !="1"){
				if($app["me"]["level"] >= 3){
					$approved .= "approve_2 = 'iya',";
					$approved .= "approve_3 = 'iya',";
					$status = "fully";
					$log_approve = "APR_MNG";
				}
				if($app["me"]["level"] == 2){
					$approved .= "approve_2 = 'iya',";
					$log_approve = "APR_SPV";
					$status = "partly";
				}
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id, $p_masa_simpan);
					
					$code = "";
					$id_section = $app['me']['id_section'];
					$link = $app['http'] ."/". $module .".mod";
					if($app['me']['level'] == 1){
						$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
						if($get_spv !=""){
							$level = "2";
						}else{
							$level = "3";
						}
					}elseif($app['me']['level'] == 2){
						$level = "3";
					}elseif($app['me']['level'] == 3){
						$level = "1";
						$code = "it";
						$id_section = db::lookup("id","section","code","it");
					}
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section");
					db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				if($app["me"]["level"] >= 3){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				if($app["me"]["level"] == 2){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
					
					$code = "";
					$id_section = $app['me']['id_section'];
					$link = $app['http'] ."/". $module .".mod";
					if($app['me']['level'] == 1){
						$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
						if($get_spv !=""){
							$level = "2";
						}else{
							$level = "3";
						}
					}elseif($app['me']['level'] == 2){
						$level = "3";
					}elseif($app['me']['level'] == 3){
						$level = "1";
						$code = "it";
						$id_section = db::lookup("id","section","code","it");
					}
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section");
					db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
					
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}
		}else{
			// echo "disini";
				// if($app["me"]["level"] >= 3){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$approved .= "approve_3 = 'tidak',";
				// 	$status = "rejected";
				// }
				// if($app["me"]["level"] == 2){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$status = "rejected";
				// }
				// // $approved = rtrim($approved,",");
				// $sql = "update ". $app['table'][$module] ."
				// set $approved
				// status_progress = '$status'
				// where id = '$id'";
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "REJ",$id, $p_masa_simpan);
				// }
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	// }

	// if(count($reorder) > 0)
	// {
		// foreach ($reorder as $idval) { $count ++;
		// 	$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
		// 	db::qry($sql);
		// }
		// echo true;
		// exit;
	// }
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : finish_it
*******************************************************************************/
// if ($act == "finish_it"):
// 	admlib::validate('UPDT');
// 	$module = admlib::$page_active['module'];
// 	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
// 		if($app["me"]["code"] == "it"){
// 			// if($app["me"]["level"] >= 3){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// 	$approved .= "approve_3 = 'iya',";
// 			// }
// 			// if($app["me"]["level"] == 2){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// }
// 			// $approved = rtrim($approved,",");;
// 			if($get_approve_mng=="iya"){
// 				$sql = "update ". $app['table'][$module] ."
// 				set finish_it ='iya',
// 					status_progress= 'progress'
// 				where id = '$id'";
// 				// db::qry($sql);
// 				if(db::qry($sql)){
// 					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
// 				}
// 				msg::set_message('success', app::i18n('approved_msg'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 				exit;
// 			}else{
// 				msg::set_message('error', app::i18n('not_fully'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 			}
// 		}else{
// 			// echo "disini";
// 			msg::set_message('error', app::i18n('not_qualified'));
// 			header("location: " . $app['webmin'] ."/". $module .".mod");
// 		}

// 	echo false;
// 	exit;
// endif;
/*******************************************************************************
* Action : confirm_done
*******************************************************************************/
if ($act == "confirm_done"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_status = db::lookup("status_progress",$module,"id",$id);
	$id_user = db::lookup("id_user",$module,"id",$id);
	// if($id_user==$app['me']['id']){
		if($get_status=="progress"){
			$sql = "update ". $app['table'][$module] ."
			set confirm_done ='iya',
				status_progress= 'finished'
			where id = '$id'";
			// db::qry($sql);
			if(db::qry($sql)){
				db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan);
			}
			msg::set_message('success', app::i18n('approved_msg'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}else{
			msg::set_message('error', app::i18n('not_finished'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	// }else{
	// 	msg::set_message('error', app::i18n('not_finished'));
	// 	header("location: " . $app['webmin'] ."/". $module .".mod");
	// }

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : print
*******************************************************************************/
if ($act == "print"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);
	$departement = db::lookup("name","departement", "id", $form['id_departement']);
	$revisi				 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='UPD'");
	$approve_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$approve_it_name	 = db::lookup("name","member", "id", $approve_it_id);
	$approve_mng_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG' order by created_at desc");
	$approve_mng_name	 = db::lookup("name","member", "id", $approve_mng_id);


	// $approve_it		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	// $approve_it_name	 = db::lookup("name","member", "id", $approve_it);

	
	$approve_mng_it		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$approve_mng_it_name	 = db::lookup("name","member", "id", $approve_mng_it);

	$created_spv_id		 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$created_done_id	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRD' order by created_at desc");
	
	$approve_spv		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_SPV' order by created_at desc");
	$approve_spv_name	 = db::lookup("name","member", "id", $approve_spv);
	$config = db::get_record("configuration", "id", 1);
	// $created = db::lookup("created_at","log", "id", $form['id_departement']);
	////////////////////////////////////////////////////////////////////////////
	$rs['add_devices'] = db::get_record_select("id_user id, nik, id_user, id_form, premission", "folder_user", "id_form=".$id);
	$param['datas'] = db::get_record_select("id, name", "cost_centre", "1=1");



	///////////////////
	$revisi				 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='UPD'");
	$created_revisi	 	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='UPD' order by created_at desc");
     if (empty($created_revisi)) {
		$created_revisi ="-";
	}
	$created_approve_it = db::lookup("created_at","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	///////////////////

	/*$categorynya = $form["category"];
	if($categorynya=="software"){
		$rs['add_devices'] = db::get_record_select("*", "data_form_".$categorynya, "id_form=".$id);
	}else{
		$rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
	}*/


		$categorynya = $form['category'];
		unset($_SESSION['add_devices']);
		// $_SESSION['add_devices'][$id] = ['nik'=>$nik, 'name'=>$name, 'id'=>$id, 'id_section'=>$id_section, 'name_section'=>$name_section, 'premission'=>$premission, 'name_section'=>$name_section];
		// $rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		$categorynya = "software";
		// if($categorynya=="software"){
			$rs['add_devices'] = db::get_record_select("*", "data_form_".$categorynya, "id_form=".$id);
		/*}else{
			$rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		}*/
		$software = [];
		while($row = db::fetch($rs['add_devices'])){
			$software[]= $row;
		}
		$categorynya = "hardware";
		/*if($categorynya=="hardware"){
			$rs['add_devices'] = db::get_record_select("*", "data_form_".$categorynya, "id_form=".$id);
		}else{*/
			$rs['add_devices'] = db::get_record_select("*,deskripsi desk", "data_form_".$categorynya, "id_form=".$id);
		// }
		// $_SESSION['add_devices'] = [];
			$hardware= [];
		while($row = db::fetch($rs['add_devices'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
			// $row['name'] = $user_form['name'];
			// $row['name_section'] = $name_section;
			// $_SESSION['add_devices'][$categorynya][$row['id']]= $row;
			// $_SESSION['add_devices'][$categorynya][$row['id']]= $row;
			$hardware[] = $row;
		}

		// print_r($_SESSION['add_devices']);
		if ($form['created_by']== $app['me']['id']) {
			db::remove_notif($id,$module,$app['me']['level'],$app['me']['id']);
		}
		db::remove_notif($id,$module,$app['me']['level'],"");


	// $param['datas'] = $rs['chk_cost'];
		// $_SESSION['add_devices'] = [];
		// while($row = db::fetch($rs['add_devices'])){
		// 	$user_form = db::get_record("member", "id", $row['id_user']);
		// 	$name_section = db::lookup("name","section", "id", $user_form['id_section']);
		// 	$row['name'] = $user_form['name'];
		// 	$row['name_section'] = $name_section;
		// 	$_SESSION['add_devices'][$row['id']]= $row;
		// }
	include "dsp_print.php";
	exit;
	// $get_status = db::lookup("status_progress",$module,"id",$id);
			// if($get_status=="progress"){
				// $sql = "update ". $app['table'][$module] ."
				// set confirm_done ='iya',
				// 	status_progress= 'progress'
				// where id = '$id'";
				// // db::qry($sql);
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan);
				// }
				// msg::set_message('success', app::i18n('approved_msg'));
				// header("location: " . $app['webmin'] ."/". $module .".mod");
				// exit;
			// }else{
			// 	msg::set_message('error', app::i18n('not_finished'));
			// 	header("location: " . $app['webmin'] ."/". $module .".mod");
			// }

	// echo false;
	// exit;
endif;
/*******************************************************************************
* Action : approve_it
*******************************************************************************/
if ($act == "approve_it"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
	if (empty($reject) || $reject !="1") {
		if($app["me"]["code"] == "it"){
			// if($app["me"]["level"] >= 3){
			// 	$approved .= "approve_2 = 'iya',";
			// 	$approved .= "approve_3 = 'iya',";
			// }
			// if($app["me"]["level"] == 2){
			// 	$approved .= "approve_2 = 'iya',";
			// }
			// $approved = rtrim($approved,",");;
			if($get_approve_mng=="iya"){
				$sql = "update ". $app['table'][$module] ."
				set approve_it ='iya',
					status_progress= 'accepted'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
					
					$code="";
					// $id_section = $app['me']['id_section'];
					$created_user = $app['me']['id'];
					// $link = "http://localhost/cmwi_webapp/webapp/form_recovery_data.mod";
					$link = $app['http'] ."/". $module .".mod";
					
					$level = "3";
					$code = "it";
					// $created_user = db::lookup("created_by",$module,"id",$id);
					
					// $id_section = db::lookup("id","section","code","it");


					$level = "3";
					$code = "it";
					$created_user = db::lookup("created_by",$module,"id",$id);
					$id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form Hardware Software data telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);


					/*$level = "3";
					// $code = "it";
					// $created_user = db::lookup("created_by",$module,"id",$id);
					$id_section = db::lookup("id","section","code","it");
					// }
					// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level = null)
					// $id_it = db::lookup("name","section","code","it");
					db::add_notif($id, $code, $app['me']['id'], "Form Pengajuan user telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);*/
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_fully'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	}else{
		// $approved = rtrim($approved,",");
		$sql = "update ". $app['table'][$module] ."
				set approve_2 = 'reject',
					approve_3 = 'reject',
					approve_it = 'reject',
					approve_it_3 = 'reject',
					confirm_done = 'reject',
					status_progress = 'rejected'
					where id = '$id'";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
			
			$code = "";
			$id_section = $app['me']['id_section'];
			$link = $app['http'] ."/". $module .".mod";
			if($app['me']['level'] == 1){
				$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
				if($get_spv !=""){
					$level = "2";
				}else{
					$level = "3";
				}
			}elseif($app['me']['level'] == 2){
				$level = "3";
			}elseif($app['me']['level'] == 3){
				$level = "1";
				$code = "it";
				$id_section = db::lookup("id","section","code","it");
			}
			// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
			// $id_it = db::lookup("name","section");
			db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
			
		}
		msg::set_message('success', app::i18n('approved_msg'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : finish_it
*******************************************************************************/
// if ($act == "finish_it"):
// 	admlib::validate('UPDT');
// 	form::init();
// 	$module = admlib::$page_active['module'];
// 	if($app["me"]["code"] == "it"){
// 		$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
// 		if($get_approve_mng=="iya"){
// 			if ($step == 1):
// 				// echo $module;
// 				// echo $id;
// 				$form = db::get_record($module, "id", $id);
// 				form::populate($module);
// 				include "dsp_feedback.php";
// 				exit;
// 			endif;
// 			if ($step == 2):
// 				form::serialize_form();
// 				// form::validate('empty', 'p_name,p_nik,p_id_departement,p_username,p_alasan');
// 				// if (form::is_error()):
// 				// 	msg::build_msg();
// 				// 	header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
// 				// 	exit;
// 				// endif;
// 				$p_images = str_replace("%20"," ","$p_images");
// 				$p_alias = app::slugify($p_title);
// 				app::mq_encode('p_title,p_content,p_image,p_lang');
// 				$sql = "update ". $app['table'][$module] ."
// 						set finish_it ='iya',
// 							status_progress= 'progress',
// 							note= '$p_note'
// 						where id = '$id'";
// 				// db::qry($sql);
// 				if(db::qry($sql)){
// 					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
// 				}
// 				msg::set_message('success', app::i18n('approved_msg'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 				exit;
// 			endif;
// 		}else{
// 			msg::set_message('error', app::i18n('not_fully'));
// 			header("location: " . $app['webmin'] ."/". $module .".mod");
// 		}
// 	}else{
// 		msg::set_message('error', app::i18n('not_qualified'));
// 		header("location: " . $app['webmin'] ."/". $module .".mod");
// 	}
// 	exit;
// endif;
// approve_it_3
/*******************************************************************************
* Action : approve_it_3
*******************************************************************************/
if ($act == "approve_it_3"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_it = db::lookup("approve_it",$module,"id",$id);
	// echo $get_approve_it;
	// exit;
	if (empty($reject) || $reject !="1") {
			if($app["me"]["code"] == "it" && $app["me"]["level"] == "3"){
				// if($app["me"]["level"] >= 3){
				// 	$approved .= "approve_2 = 'iya',";
				// 	$approved .= "approve_3 = 'iya',";
				// }
				// if($app["me"]["level"] == 2){
				// 	$approved .= "approve_2 = 'iya',";
				// }
				// $approved = rtrim($approved,",");;
				if($get_approve_it=="iya"){
					$sql = "update ". $app['table'][$module] ."
					set approve_it_3 ='iya',
						status_progress= 'progress'
					where id = '$id'";
					// db::qry($sql);
					if(db::qry($sql)){
						db::ins_call_func("insert_log", $app['me']['id'], $module, "APR_MNG_IT",$id, $p_masa_simpan);
						
						$code="";
						$id_section = $app['me']['id_section'];
						$created_user = $app['me']['id'];
						$link = $app['http'] ."/". $module .".mod";
						
							$level = "1";
							// $code = "it";
							$created_user = db::lookup("created_by",$module,"id",$id);
							
							// $id_section = db::lookup("id","section","code","it");
						// }
						// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
						// $id_it = db::lookup("name","section","code","it");
						db::add_notif($id, $code, $app['me']['id'], "Form recovery data telah di buat silahkan Approve Form ini", "", $link, $created_user, $level, $module);
					}
					msg::set_message('success', app::i18n('approved_msg'));
					header("location: " . $app['webmin'] ."/". $module .".mod");
					exit;
				}else{
					msg::set_message('error', app::i18n('not_fully'));
					header("location: " . $app['webmin'] ."/". $module .".mod");
				}
			}else{
				// echo "disini";
				msg::set_message('error', app::i18n('not_qualified'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
		// $approved = rtrim($approved,",");
		$sql = "update ". $app['table'][$module] ."
				set approve_2 = 'reject',
					approve_3 = 'reject',
					approve_it = 'reject',
					approve_it_3 = 'reject',
					confirm_done = 'reject',
					status_progress = 'rejected'
					where id = '$id'";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan,$ipaddress, "");
			
			$code = "";
			$id_section = $app['me']['id_section'];
			$link = $app['http'] ."/". $module .".mod";
			if($app['me']['level'] == 1){
				$get_spv = db::lookup("GROUP_CONCAT(id)","title","id_section='".$app['me']['id_section']."' AND level = '2' ");
				if($get_spv !=""){
					$level = "2";
				}else{
					$level = "3";
				}
			}elseif($app['me']['level'] == 2){
				$level = "3";
			}elseif($app['me']['level'] == 3){
				$level = "1";
				$code = "it";
				$id_section = db::lookup("id","section","code","it");
			}
			// add_notif($id_form=null, $code=null, $created_by=null, $desk=null, $to_section = null, $link=null, $id_user = null, $level, $module = null)
			// $id_it = db::lookup("name","section");
			db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
			
		}
		msg::set_message('success', app::i18n('approved_msg'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : get-member;
*******************************************************************************/
if($act == "get-member"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$member = "SELECT id,id_section,nik,name FROM ". $app['table']['member'] . " WHERE id IN ('". $id."') AND status='active' limit 1";
	db::query($member, $rs['member'], $nr['member']);
	if($nr['member'] > 0){
		while($row = db::fetch($rs['member'])){
			$callback['member'] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	// $callback["sql"] = $member;
	echo json_encode($callback);
	exit;
endif;
if($act == "add_device")
{	
	// admlib::validate('UPDT');
	if($step == "add")
	{
		// $req_dump = print_r($_REQUEST, TRUE);
		// $fp = fopen('request.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		$name_section = db::lookup("name","section","id",$id_section);
		$_SESSION['add_devices'][$category][$id] = ['name_soft'=>$name_soft, 'tujuan'=>$tujuan, 'id'=>$id, 'category'=>$category, 'desk'=>$desk, 'jumlah'=>$jumlah, 'satuan'=>$satuan];
		// print_r($_SESSION['add_devices']);
		exit;
	}elseif($step == "delete"){
		// $req_dump = print_r($_SESSION['add_devices'], TRUE);
		// $fp = fopen('request.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		unset($_SESSION['add_devices'][$category][$id]);
		exit;
	}elseif($step == "delete_device"){
		unset($_SESSION['add_devices']);
	}
	include "dsp_list.php";
}
?>
