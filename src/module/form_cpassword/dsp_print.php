<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM PENGAJUAN PENGGANTIAN PASSWORD</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">
    <section class="sheet padding-10mm">
        <table width="100%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;font-size: 12px;">
            <tbody>
                <tr>
                    <td width="30%">&nbsp;</td>
                    <td width="70%">&nbsp;</td>
                </tr>
                <tr>
                    <td>	        	
                        <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; display: block; text-align: left;">
                    </td>
                    <td colspan="2" align="center" style="background:#000;font-size: 18px;font-weight: bold;letter-spacing: 0px;color:#fff;">FORM PENGAJUAN PENGGANTIAN PASSWORD
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table width="70%" border="0" align="left" style="font-size:14px;float:left;">
                            <tbody>
                                <tr>
                                    <td>Nama Pemohon</td>
                                    <td>:</td>
                                    <td><?= $form['name'] ?></td>
                                </tr>
                                <tr>
                                    <td>NIK</td>
                                    <td>:</td>
                                    <td><?= $form['nik'] ?></td>
                                </tr>
                                <tr>
                                    <td>Departemen</td>
                                    <td>:</td>
                                    <td><?= $departement ?></td>
                                </tr>
                                <tr>
                                    <td>Kategori Infrastruktur</td>
                                    <td>:</td>
                                    <td><?php 
                                        $kat_si = db::lookup("name","sistem_informasi","id",$form['id_si']);
                                        echo $kat_si;
                                     ?></td>
                                </tr>
                                <tr>
                                    <td>User Login</td>
                                    <td>:</td>
                                    <td><?= $form['username'] ?></td>
                                </tr>
                                <tr>
                                    <td>Alasan</td>
                                    <td>:</td>
                                    <td><?= $form['alasan'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                        
<!--                         <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                            <tbody>
                                <tr align="center">
                                    <td>
                                         Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?> 
                                            
                                        
                                        </td>
                                </tr>
                            </tbody>
                        </table> -->
                        <table cellspacing="1" cellpadding="3" style="font-size: 12px;" align="right" width="30%">
                            <tbody>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= $form['no_pengajuan'] ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">No Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                                </tr>
                                <tr>
                                    <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                                </tr>
                               <!-- <tr>
                                    <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                                </tr>-->
                           </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" style="font-size:12px;" cellpadding="5">
                            <tbody>
                                <tr align="center">
                                    <td width="20%" style="background:#000;color:#fff;">Pemohon</td>
                                    <?php if(!empty($approve_spv_name)){ ?>
                                    <td width="20%" style="background:#000;color:#fff;">Supervisor</td>
                                    <?php } ?>
                                    <td width="20%" style="background:#000;color:#fff;">Manager</td>
                                </tr>
                                <tr align="center" style="height:75px;">
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <?php if(!empty($approve_spv_name)){ ?>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <?php } ?>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr style="text-align: center;">
                                    <td style="background:#000;color:#fff;"><?= $form['name'] ?></td>
                                    <?php if(!empty($approve_spv_name)){ ?>
                                    <td style="background:#000;color:#fff;"><?= $approve_spv_name ?></td>
                                    <?php } ?>
                                    <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Setting Password Default <span style="font-size:20px;font-weight:bold;">&#8594;</span> CMWI1
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Pasuruan, <?= date('d/m/Y', strtotime($created_spv_id)) ?> (dd/mm/yy)
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="90%" border="0" style="font-size:12px;" cellpadding="5" align="left">
                            <tbody>
                                <tr align="center">
                                    <td style="background:#000;color:#fff;">IT Dept.</td>
                                </tr>
                                <tr align="center" style="height:75px;">
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr align="center">
                                    <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                </tr>                       
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <strong>Catatan:</strong><br /><br />
                        <div style="width: 98%; height: 85px;border:1px solid #000; padding:5px;">
                            <?= $form['note'] ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        Serah Terima Password
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Pasuruan, <?= (empty($created_done_id)?"....................":date('d/m/Y', strtotime($created_done_id))) ?> (dd/mm/yy)
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="90%" border="0" style="font-size:12px;" cellpadding="5" align="left">
                            <tbody>
                                <tr align="center">
                                    <td style="background:#000;color:#fff;">Pemohon</td>
                                </tr>
                                <tr align="center" style="height:75px;">
                                    <td style="background:#e6e6e6;color:#222;"> <?= (empty($created_done_id)?"":"DTO") ?> </td>
                                </tr>
                                <tr align="center">
                                    <td style="background:#000;color:#fff;"><?= $form['name'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <div style="width: 100%; height: 100px;border:1px solid #000; padding:0 0 7px 0; line-height:15px;">
                            <ul style="list-style: disclosure-closed;"><strong>Keterangan:</strong>
                                <li>Password minimal 5 karakter</li>
                                <li>Password menggunakan kombinasi huruf , angka dan simbol. contoh:Rp.10.000</li>
                                <li>Jangan Menggunakan password yang mudah di tebak. contoh: tanggal lahir / nama lengkap</li>
                                <li>Dilarang menggunakan yang sama</li>
                            </ul>
                        </div>
                    </td>
                </tr>
           </tbody>
        </table>
    </section>
</body>
</html>