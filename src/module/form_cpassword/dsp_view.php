<?php
admlib::display_block_header();
	admlib::get_component('texteditorlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"status_f",
		// 		"datas"=>["baru","ada"],
		// 		"value"=>app::ov($module['status_f'])
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"name_f",
		// 		"value"=>app::ov($module['name_f']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"type"=>"hidden",
		// 		"name"=>"name",
		// 		"value"=>app::ov($module['name']),
		// 	)
		// );
		if($form['no_pengajuan']){
			admlib::get_component('view',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"validate"=>true,
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('view',
			array(
				"name"=>"name",
				"type"=>"hidden",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>$form['name']
			)
		);
		// print_r($app['me']);
		admlib::get_component('view',
			array(
				"name"=>"nik",
				"type"=>"hidden",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>$form['nik']
			)
		);
		// $rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		$departement = db::lookup("name","departement","id",$form['id_departement']);
		admlib::get_component('view',
			array(
				"name"=>"id_departement",
				"type"=>"hidden",
				"value"=>app::ov($departement),
				"items"=>$rs['departement']
			)
		);
		$rs["si"] = db::get_record_select("id, name","sistem_informasi","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_si",
				// "type"=>"hidden",
				// "value"=>app::ov($app['me']['id_si']),
				"value"=>($act=="add"?$app['me']['id_si']:$form['id_si']),
				"items"=>$rs['si'],
					"validate"=>true,
				"readonly"=>"yes"
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"username",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>$form['username']
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"alasan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['alasan'])
			)
		);
		// print_r($form);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"ipaddress",
		// 		"value"=>app::ov($module['ipaddress']),
		// 	)
		// );
		// admlib::get_component('textarea',
		// 	array(
		// 		"editor"=>true,
		// 		"name"=>"content",
		// 		"value"=>app::ov($module['content']),
		// 	)
		// );
		// admlib::get_component('inputupload',
		// 	array(
		// 		"name"=>"images",
		// 		"value"=>app::ov($module['images']),
		// 		"filemedia"=>true
		// 	)
		// );
		// if(isset($id))
		// {
		// 	admlib::get_component('language',
		// 		array(
		// 			"name"=>"lang",
		// 			"value"=>app::ov($module['lang'])
		// 		)
		// 	);
		// }
		// admlib::get_component('submit',
		// 	array(
		// 		"id"=>(isset($id))?$id:"",
		// 		"no_submit"=>"yes",
		// 		"act"=>$act
		// 	)
		// );
		if ($app['me']['level']=="1") {

			admlib::get_component('textarea',
				array(
					"name"=>"note",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					"value"=>app::ov($form['note'])
				)
			);
			admlib::get_component('submit_aprove',
				array(
					"id"=>(isset($id))?$id:"",
					// "no_submit"=>"iya",
					"act"=>"finish_it"
				)
			);
		}
		/*if ($app['me']['level']=="3") {
			admlib::get_component('submit_aprove',
				array(
					"id"=>(isset($id))?$id:"",
					// "no_submit"=>"iya",
					"act"=>"approve_it"
				)
			);
		}*/
	admlib::get_component('formend');
admlib::display_block_footer();
?>
