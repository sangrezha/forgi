<html><head>
	<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
	</style>
	<title></title>
</head>
<!-- <body onload="doPrint()"> -->
<body onload="">
	<table width="900" border="0" align="center">
	    <tbody><tr>
	        <td style="display: flex; align-items: center;border: 1px solid black;">	        	
<!-- 	            <img src="logo.png" alt="" style="width: 200px; height: 150px; display: block; text-align: left;"> -->
	            <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 200px; height: auto; display: block; text-align: left;border-right: 1px solid black;">
	            <span style="display: block;font-size: 20px;font-weight: bold;margin-left: 100px;letter-spacing: 5px"><p>Form Pengajuan Penggantian Password</p></span>
	        </td>
	    </tr>
	</tbody></table>
	<div style="border: 1px solid black;width: 172px;height: 30px;margin-left: 730px;text-align: center;">
	   Revisi <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
	</div>
    <table width="900px" border="0" align="center">
        <tbody>
            <tr>
                <td>Nama Pemohon</td>
                <td>:</td>
                <td><?= $form['name'] ?></td>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td><?= $form['nik'] ?></td>
            </tr>
            <tr>
                <td>Departement</td>
                <td>:</td>
                <td><?= $departement ?></td>
            </tr>
            <tr>
                <td>Kategori Infrastruktur : </td>
                <td>:</td>
                <td><?php 
                    $kat_si = db::lookup("name","sistem_informasi","id",$form['id_si']);
                    echo $kat_si;
                 ?></td>
            </tr>
            <tr>
                <td>User Login</td>
                <td>:</td>
                <td><?= $form['username'] ?></td>
            </tr>
            <tr>
                <td>Alasan</td>
                <td>:</td>
                <td><?= $form['alasan'] ?></td>
            </tr>
        </tbody>
    </table>

	<div style="/*! border: 1px solid black; */width: 900px;margin-left: 20px;margin-top: 50px;">
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)
<table border="1">
    <tbody>
        <tr style="text-align: center;">
            <td>Pemohon</td>
            <?php if(!empty($approve_spv_name)){ ?>
            <td>Supervisor</td>
            <?php } ?>
            <td>Manager</td>
        </tr>
        <tr style="text-align:center;">
            <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
            <?php if(!empty($approve_spv_name)){ ?>
            <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
            <?php } ?>
            <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
        </tr>
        <tr style="text-align: center;">
            <td><?= $form['name'] ?></td>
            <?php if(!empty($approve_spv_name)){ ?>
            <td><?= $approve_spv_name ?></td>
            <?php } ?>
            <td><?= $approve_mng_name ?></td>
        </tr>
    </tbody>
</table>
</div>
	<div style="/*! border: 1px solid red; */ margin-left: 20px; background-color: transparent;padding-top: 10px;width: 880px;display: flex;">
	<div style="width:40%;/*! border: 1px solid black; */">
	 setting password default -&gt; CMWI
	<br>
		pasuruan, <?= date('d/m/Y', strtotime($created_spv_id)) ?> (dd/mm/yy)
	<br>
<table style="text-align: center;margin-top: 10px;" border="1">
    <tbody>
        <tr>
            <td>IT Dept.</td>
        </tr>
        <tr style="text-align:center;">
            <td style="width: 100px;height: 80px;">DTO</td>
        </tr>
        <tr>
            <td><?= $approve_it_name ?></td>
        </tr>                       
    </tbody>
</table>
        </div>
	     <div style="/*! border: 1px solid black; */width:  60%;height;height: 100px;margin-top: 40px;">
Note :  <textarea style="width: 98%; height: 118px;"><?= $form['note'] ?></textarea></div>
	</div>

	<div style="/*! border: 1px solid red; */ margin-left: 20px; background-color: transparent;padding-top: 10px;width: 880px;display: flex;margin-top: 20px;">
	<div style="width:40%;/*! border: 1px solid black; */">serah terima password<br>
		pasuruan, <?= (empty($created_done_id)?"....................":date('d/m/Y', strtotime($created_done_id))) ?> (dd/mm/yy)
	<br>
    <table style="text-align: center;margin-top: 10px;" border="1">
        <tbody>
            <tr>
                <td>Pemohon</td>
            </tr>
            <tr style="text-align:center;">
                <td style="width: 100px;height: 80px;"> <?= (empty($created_done_id)?"":"DTO") ?> </td>
            </tr>
            <tr>
                <td><?= $form['name'] ?></td>
            </tr>
        </tbody>
    </table>
        </div>
	     <div style="border: 1px solid black;width:  60%;height;height: 135px;margin-top: 40px;">
 
  <p style="padding-left: 10px;margin-top: 10px;">
     <u>Keterangan:</u>
    <br>
    -Password minimal 5 karakter
    <br>
    -Password menggunakan kombinasi huruf , angka dan simbol. contoh:Rp.10.000
    <br>
    -Jangan Menggunakan password yang mudah di tebak. contoh: tanggal lahir / nama lengkap 
    <br>
    -Dilarang menggunakan yang sama</p>
</div>
	</div>
<script>
	function doPrint() {
	    window.print();            
	    document.location.href = "trucking.mod"; 
	}
</script>
<div state="voice" class="placeholder-icon" id="tts-placeholder-icon" title="Click to show TTS button" style="background-image: url(&quot;moz-extension://d957ec0f-edb7-42ef-a0a8-6f64e9a2a6ec/data/content_script/icons/voice.png&quot;); display: none;"><canvas width="36" height="36" class="loading-circle" id="text-to-speech-loader" style="display: none;"></canvas></div></body></html>