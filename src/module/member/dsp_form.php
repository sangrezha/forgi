<?php
admlib::display_block_header();
// admlib::get_component('texteditorlib');
admlib::get_component('uploadlib');
	admlib::get_component('formstart');
			admlib::get_component('inputtext',
				array(
					"name"=>"nik",
					"value"=>app::ov($form['nik']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"name",
					"value"=>app::ov($form['name']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"username",
					"value"=>app::ov($form['username']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"email",
					"value"=>app::ov($form['email']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"telp",
					"value"=>app::ov($form['telp'])
					// "validate"=>"required"
				)
			);
			// print_r($form);
			admlib::get_component('checkbox',
				array(
					"name"=>"digital_form_chk",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					"datas"=>["iya_df"],
					"value"=>app::ov($form['digital_form'])
				)
			);
			admlib::get_component('checkbox',
				array(
					"name"=>"direksi",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					"datas"=>["iya"],
					"value"=>app::ov($form['direksi'])
				)
			);
			$rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
			admlib::get_component('select',
				array(
					"name"=>"id_departement",
					"value"=>app::ov($form['id_departement']),
					"items"=>$rs['departement']/* ,
					"validate"=>"required" */
				)
			);
			// $rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
			admlib::get_component('select',
				array(
					"name"=>"id_section",
					"value"=>app::ov($form['id_section']),
					"items"=>$rs['section']/* ,
					"validate"=>"required" */
				)
			);
			// $rs["title"] = db::get_record_select("id, name","title","status='active' ORDER BY name ASC");
			admlib::get_component('select',
				array(
					"name"=>"id_title",
					"value"=>app::ov($form['id_title']),
					"items"=>$rs['title']/* ,
					"validate"=>"required" */
				)
			);
			admlib::get_component('inputupload', 
				array(
					"name"=>"photo", 
					// "value"=> (isset($form['logo']) AND file_exists($app['logo_lib_path'] ."/". $form['logo']))?'/'. app::ov($form['logo']):null,
					"value"=> $form['photo'],
					"filemedia"=>true
				)
			);
			if(empty($id)){
				
				admlib::get_component('inputtext',
					array(
						"type"=>"password",
						"name"=>"password",
						"validate"=>"required"
					)
				);
				admlib::get_component('inputtext',
					array(
						"type"=>"password",
						"name"=>"repassword",
						"validate"=>"required"
					)
				);
			}
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
					)
			);
	admlib::get_component('formend');
	?>
	
<script type="text/javascript">
function get_section(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						// console.log(result); 
						// if(dis){
						// 	loop = "<option disabled><?php echo app::i18n('select');?></option>";
						// }else{
							loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						// }
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						// alert(loop);
						$("#id_section").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_section").html('');
						$("#id_title").html('');
					}
			}
		});     
	}
function get_title(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						console.log(result);
						loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						$("#id_title").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_title").html('');
					}
			}
		});     
	}
</script>
<script>
	<?php if($form['direksi']=="iya"){ ?>
		$("#g_id_departement").hide();
		$("#g_id_section").hide();
		$("#g_id_title").hide();
	<?php } ?>
		$(document).ready(function(){
			// input[type=radio][name=p_status_user]
			// $('#direksi').change(function(){
			// $('input[type=checkbox][name=p_direksi]').change(function(){
			$('#iya').change(function(){
				// $('#id_section').select("val","null");
				// $('#id_title').select("val","null");
				// var id_section = $(this).val();
				// alert(id_section);
				if(this.checked) {
					// alert(this.checked);
					$("#g_id_departement").hide();
					$("#g_id_section").hide();
					$("#g_id_title").hide();
					// var returnVal = confirm("Are you sure?");
					// $(this).prop("checked", returnVal);
				}else{
					$("#g_id_departement").show();
					$("#g_id_section").show();
					$("#g_id_title").show();
				}
			});


			
			// $('#checkbox1').change(function() {
			// 	if(this.checked) {
			// 		var returnVal = confirm("Are you sure?");
			// 		$(this).prop("checked", returnVal);
			// 	}
			// 	$('#textbox1').val(this.checked);        
			// });

			
			// Custom Company to Custom PIC & Maker-Model
			$('#id_departement').each(function(){
				var id_section = $(this).val();
				var dis = '<?= $form['id_section'] ?>';
				// alert(dis);
				// console.log(dis);
				var urlcustomer = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
				// var urlmaker = "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_customer;
				// console.log("ID " + urlcustomer);
			get_section(id_section, dis, urlcustomer);
				// get_makermodel(id_customer, dis, urlmaker);
			});

			$('#id_departement').change(function(){
				$('#id_section').select("val","null");
				// $('#id_title').select("val","null");
				var id_section = $(this).val();
				var dis = '';
				// console.log(dis);
				var urlsection = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
				// alert(urlsection);
				// var urlmaker = "<?php //echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_departement;
				// console.log("ID section: " + urlsection);
				// console.log("ID Maker: " + urlmaker);
				get_section(id_section, dis, urlsection);
				// get_makermodel(id_section, dis, urlmaker);
			});
			$('#id_title').each(function(){
				// var id_title = $(this).val();
				// var id_title = $("#id_section option:selected").val();
				var id_title = '<?= $form['id_section'] ?>';
				var dis = '<?= $form['id_title'] ?>';
				// alert(dis);
				// console.log(dis);
				// alert(dis);
				var urlcustomer = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-title&id_title="+id_title;
				// alert(urlcustomer);
				// var urlmaker = "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_customer;
				// console.log("ID " + urlcustomer);
				get_title(id_title, dis, urlcustomer);
				// get_makermodel(id_customer, dis, urlmaker);
			});

			$('#id_section').change(function(){
				$('#id_title').select("val","null");
				var id_title = $("#id_section option:selected").val();
				var dis = '';
				// console.log(dis);
				var urlsection = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-title&id_title="+id_title;
				// alert(urlsection);
				// var urlmaker = "<?php //echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_departement;
				// console.log("ID section: " + urlsection);
				// console.log("ID Maker: " + urlmaker);
				get_title(id_section, dis, urlsection);
				// get_makermodel(id_section, dis, urlmaker);
			});

		});
</script>
	<?php
admlib::display_block_footer();
?>