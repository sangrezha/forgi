<html><head>
	<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
	</style>
	<title></title>
</head>
<!-- <body onload="doPrint()"> -->
<body onload="">
	<table width="900" border="0" align="center">
	    <tbody><tr>
	        <td style="display: flex; align-items: center;border: 1px solid black;">	        	
<!-- 	            <img src="logo.png" alt="" style="width: 200px; height: 150px; display: block; text-align: left;"> -->
	            <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 200px; height: auto; display: block; text-align: left;border-right: 1px solid black;">
	            <span style="display: block;font-size: 20px;font-weight: bold;margin-left: 100px;letter-spacing: 5px"><p>FORM PERMINTAAN AKSES FOLDER</p></span>
	        </td>
	    </tr>
	</tbody></table>
	<div style="border: 1px solid black;width: 172px;height: 20px;margin-left: 730px;text-align: center;">
	   Revisi <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
	</div>
	<table width="900px" border="0" align="center">
	<tbody>
	<tr>
		<td style="border-top: 1px solid black;border-left: 1px solid black;">Name</td>
		<td style="border-top: 1px solid black;">:</td>
		<td style="border-top: 1px solid black;border-right: 1px solid black;"><?=  $form['name'] ?></td>
	</tr>
	<tr>
		<td style="border-left: 1px solid black;">Section</td>
		<td>:</td>
		<td style="border-right: 1px solid black;"><?= $section ?></td>
	</tr>
	<tr>
		<td style="border-left: 1px solid black;">Keluhaan</td>
		<td>:</td>
		<td style="border-right: 1px solid black;"><?=  $form['keluhan'] ?></td>
	</tr>
	<tr>
		<td style="border-left: 1px solid black;">Tanggal & jam kejadian</td>
		<td>:</td>
		<td style="border-right: 1px solid black;"><?php 
		$p_new_waktu_kejadian =  date('d-m-Y h:i A', strtotime($form['waktu_kejadian'])); ?>
			<?=  $p_new_waktu_kejadian ?></td>
	</tr>
	<tr>
		<td colspan="6" style="padding-bottom: 5px;"><div style="border: 1px solid black;text-align: center;padding: 10px 0px;">Di isi oleh user</div></td>
		
	</tr>
		<tr>
			<!-- <td  style="border-top: 1px solid black;" colspan="3"></td> -->
	 </tr>
	<tr>
		<td style="border-top: 1px solid black;border-left: 1px solid black;">Kategori SI</td>
		<td style="border-top: 1px solid black;">:</td>
		<td style="border-top: 1px solid black;border-right: 1px solid black;"><?=  $form['kat_si'] ?></td>
	</tr>
	<tr>
		<td style="border-left: 1px solid black;">Analisa Penyebab</td>
		<td>:</td>
		<td style="border-right: 1px solid black;"><?=  $form['penyebab'] ?></td>
	</tr>
	<tr>
		<td style="border-left: 1px solid black;">Penanganan</td>
		<td>:</td>
		<td style="border-right: 1px solid black;"><?=  $form['penanganan'] ?></td>
	</tr>
	<!-- <tr>
		<td>System yang dibutuhkan</td>
		<td>:</td>
		<td>
				<input type="radio" id="baru" name="p_kat_si" value="js" <?= ($form['kat_si']=="js"?"checked":"") ?> >
				<label for="baru">JS Systems ( ERP )</label>
				<input type="radio" id="ada" name="p_kat_si" value="andon" <?= ($form['kat_si']=="andon"?"checked":"") ?> >
				<label for="ada">Andon System</label>
				<input type="radio" id="ada" name="p_kat_si" value="email" <?= ($form['kat_si']=="email"?"checked":"") ?> >
				<label for="ada">Email</label>
				<input type="radio" id="ada" name="p_kat_si" value="tp" <?= ($form['kat_si']=="tp"?"checked":"") ?> >
				<label for="ada">TP PEB</label>
	</tr> -->
	<tr>
	<td style="padding: 10px 0px;border-left: 1px solid black;border-right:1px solid black;" colspan="6">Tgl  penanganan <u><?php 
		
		$p_new_tgl_penanganan =  date('d-m-Y h:i A', strtotime($form['tgl_penanganan']));
		echo $p_new_tgl_penanganan ?></u> Jam Selesai <u><?php 
		$p_new_tgl_selesai =  date('d-m-Y h:i A', strtotime($form['tgl_selesai']));
		echo $p_new_tgl_selesai ?></u> Down Time <u><?php 
		$p_new_down_time =  date('d-m-Y h:i A', strtotime($form['down_time']));
		echo $p_new_down_time ?></u></td>
	</tr> 
	<tr>
		<td style="border-left: 1px solid black;">Status Penanganan</td>
		<td>:</td>
		<td style="border-right: 1px solid black;">
				<input type="radio" id="baru" name="p_status_penanganan" value="permanent" <?= ($form['status_penanganan']=="permanent"?"checked":"") ?> >
				<label for="baru">Permanent</label>
				<input type="radio" id="ada" name="p_status_penanganan" value="sementara" <?= ($form['status_penanganan']=="sementara"?"checked":($form['status_penanganan']!="permanent"?"checked":"")) ?> >
				<label for="ada">Sementara</label>
	</tr>
	<tr>
		<td style="border-left: 1px solid black;">Hasil</td>
		<td>:</td>
		<td style="border-right: 1px solid black;">
				<input type="radio" id="baru" name="p_rework" value="js" <?= ($form['hasil']=="ok"?"checked":"") ?> >
				<label for="baru">Ok</label>
				<input type="radio" id="Rework" name="p_rework" value=" ework" <?= ($form['hasil']=="rework"?"checked":"") ?> >
				<label for="Rework">Rework</label>
	</tr>
	<tr>
		<td colspan="6" style="padding-bottom: 5px;"><div style="border: 1px solid black;text-align: center;padding: 10px 0px;">Di isi oleh IT</div></td>
		
	</tr>
	</tbody>
</table>
	<div style="/*! border: 1px solid black; */width: 900px;margin-left: 698px;padding-bottom: 20px;">
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
<!-- <p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p> -->
  <table border="1">
    <tbody><tr style="text-align: center;">
      <td>Staff IT</td>
      <td>Manager IT</td>
      <td style="border: 0px;"></td>
      <td>Pemohon</td>
    </tr>
    <tr style="text-align:center;">
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="border: 0px;border:0px;height: 80px;width: 100px;"><======</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
    </tr>
    <tr style="text-align: center;">
      <td><?= $approve_it_name ?></td>
      <td><?= $approve_mng_it_name ?></td>
      <td style="border: 0px;"></td>
	  <td><?= $form['name'] ?></td>
    </tr>
  </tbody></table>
</div>
 
<script>
	function doPrint() {
	    window.print();            
	    document.location.href = "trucking.mod"; 
	}
</script>
<div state="voice" class="placeholder-icon" id="tts-placeholder-icon" title="Click to show TTS button" style="background-image: url(&quot;moz-extension://d957ec0f-edb7-42ef-a0a8-6f64e9a2a6ec/data/content_script/icons/voice.png&quot;); display: none;"><canvas width="36" height="36" class="loading-circle" id="text-to-speech-loader" style="display: none;"></canvas></div></body></html>