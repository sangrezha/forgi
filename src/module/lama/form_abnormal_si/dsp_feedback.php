<?php
admlib::display_block_header();
	admlib::get_component('timepickerlib');
	admlib::get_component('texteditorlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		admlib::get_component('inputtext',
			array(
				"name"=>"kat_si",
				"value"=>$form['kat_si']
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"penyebab",
				"value"=>$form['penyebab']
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"penanganan",
				"value"=>$form['penanganan']
			)
		);
		// echo $form['tgl_penanganan'];
		if($form['tgl_penanganan']){
			$p_new_tgl_penanganan =  date('d/m/Y H:i', strtotime($form['tgl_penanganan']));
		}
		admlib::get_component('datetimepicker',
			array(
				"name"=>"tgl_penanganan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($p_new_tgl_penanganan)
			)
		);
		// echo $form['tgl_selesai'];
		if($form['tgl_selesai']){
			$p_new_tgl_selesai =  date('d/m/Y H:i', strtotime($form['tgl_selesai']));
		}
		admlib::get_component('datetimepicker',
			array(
				"name"=>"tgl_selesai",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($p_new_tgl_selesai)
			)
		);
		// echo $form['down_time'];
		if($form['down_time']){
			$p_new_down_time =  date('d/m/Y H:i', strtotime($form['down_time']));
		}
		admlib::get_component('datetimepicker',
			array(
				"name"=>"down_time",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($p_new_down_time)
			)
		);
		admlib::get_component('radio',
			array(
				"name"=>"status_penanganan",
				"datas"=>["permanent","sementara"],
				"validate"=>true,
				"value"=>app::ov($form['status_penanganan'])
			)
		);
		admlib::get_component('radio',
			array(
				"name"=>"hasil",
				"datas"=>["ok","rework"],
				"validate"=>true,
				"value"=>app::ov($form['hasil'])
			)
		);
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
