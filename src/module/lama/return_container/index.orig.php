<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'return_container','caption'=>'Return');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$_SESSION['container'] 	= null;
	$option = ['sortable'=>true];
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'complaint', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$unpost = 1;
	$unmodify = 1;
	
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
		$sql = "SELECT a.id, a.status,a.addressed addressed,d.num_container num_cont, e.name AS name, e.nopol nopol_truck,  b.name as created_by,a.created_at,c.name as updated_by,a.updated_at,d.id_import id_imp
	FROM ". $app['table']['return_container'] ." a
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id) 
			LEFT JOIN ". $app['table']['container'] ." d ON (a.id_container=d.id) 
			LEFT JOIN ". $app['table']['armada'] ." e ON (a.id_armada=e.id) 
			$q ORDER BY a.reorder ASC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['view','num_cont','name','nopol_truck','addressed'];
	while($row = db::fetch($rs['row']))
	{
		$datas = db::get_record("complaint_status","id_complaint='".$row['ncr_id']."' AND type='".$row['approve_by']."' ORDER BY created_at DESC");
		$row['step']			= app::getliblang($row['approval']);
		//$row['created_by']			= app::getliblang($row['approve_by']);
		if($row['code_cat'] == "TB" && $row['approval'] == "qc"){
			$row['step']		= app::getliblang('logistic');
		}
		if($row['code_cat'] == "TB" && $row['approve_by'] == "qc"){
			$row['created_by']	= app::getliblang('logistic');
		}
		$row['note']			= $datas['note'];
		$row['created_at']		= app::format_datetime($row['created_at'],"ina","MMM");

		$row['view']	= '<center><a title="CRM MAX" href="'.admlib::getext().'&act=view&id='.$row['id_imp'].'&abc=abcde"><i class="fa fa-eye" aria-hidden="true"></i></a></center>';
		$results[] 	= $row;
	}
	$id_exp = implode("','", $id_exp);
	$id_exp_act = implode("','", $id_exp_act);
	$sql321 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
	$sql1231 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
	db::qry($sql321);
	db::qry($sql1231);
	// $sortable = true;
	$statusx = true;
	include $app['pwebmin'] ."/include/blk_list_complaint.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
//		$validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen';
		$validate ='p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen';
		form::serialize_form();
		// form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		if(count($container)>0)
		{
			foreach($container as $row){
			$ids = rand(1, 100).date("dmYHis");
				$t_container 	= $t_container+$row['n'];
				$reorders = db::lookup("max(reorder)","container","1=1");
				if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
				$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at,address,delivery_date) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now(),'". $row['adr'] ."','". $row['dd'] ."')";
				db::qry($sqlx);
			}
		}
		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_date_receipt,p_amount_payment,p_nopen');
		$reorder = db::lookup("max(reorder)","import","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$sql = "insert into ".$app['table']['import']."
				(id, num_pib, num_bl, name_ship, eta, id_customer, id_company, date_receipt, amount_payment, nopen, reorder, created_by, created_at) values
				('$id', '$p_num_pib', '$p_num_bl', '$p_name_ship', '$p_eta', '$customer_name', '$company', '$p_date_receipt', '$p_amount_payment', '$p_nopen', '$reorder', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
		
		$rs['container'] = db::get_record_select("num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr", "container", "id_import", $id);
		$_SESSION['container'] = [];
		while($row = db::fetch($rs['container'])){
			$_SESSION['container'][] = $row;
		}
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		// print_r($_SESSION['income']);
		form::validate('empty','p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		if(count($container)>0)
		{
			db::qry('DELETE FROM '. $app['table']['container'] .' WHERE id_import="'. $id .'"');
			foreach($container as $row){
			$ids = rand(1, 100).date("dmYHis");
				$t_container 	= $t_container+$row['n'];
				$reorders = db::lookup("max(reorder)","container","1=1");
				if ($reorders==0){ $reorders = 1; }else{ $reorders = $reorders+1; }
				$sqlx = "insert into ". $app['table']['container'] ."
					(id, id_import, num_container, qty, note, value, reorder, created_by, created_at) values ('$ids', '$id', '". $row['c'] ."', '". $row['j'] ."', '". $row['d'] ."', '". $row['n'] ."', '$reorders', '". $app['me']['id'] ."', now())";
				db::qry($sqlx);
			}
		}
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);
		$sql = "update ". $app['table']['import'] ."
				set num_pib 		= '$p_num_pib',
					num_bl 			= '$p_num_bl',
					name_ship 		= '$p_name_ship',
					eta		 		= '$p_eta',
					id_customer 	= '$customer_name',
					id_company	 	= '$company',
					date_receipt 	= '$p_date_receipt',
					amount_payment 	= '$p_amount_payment',
					nopen		 	= '$p_nopen',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('modify'));
		// header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		header("location: " .url::get_referer());
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['import']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : trucking
*******************************************************************************/
if ($act == "trucking"):
admlib::validate('UPDT');
	if($step == 'add')
	{
		// $nominal = str_replace('Rp ', '', $nom);
		// $nominal = str_replace(',', '', $nominal);
		$id_c  = rand(1, 100).date("dmYHis");
		$sql   = "insert into ".$app['table']['trucking']." 
				(id,id_import,id_container,id_armada,created_by,created_at) values
				('$id_c','$pib','$cont','$arm','".$app['me']['id']."',now())";
		$sqlss ="update ". $app['table']['armada']." set status = 'depart' WHERE id = '$arm'";
		$sqlsss ="update ". $app['table']['container']." set status = 'depart' WHERE id = '$cont'";

		// $form_user_detail = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);
		$form_cont = db::get_record("num_container","container", "id", $cont);
		$form_arm  = db::get_record("name","armada", "id", $arm);
		$form_imp  = db::get_record("num_pib","import", "id", $pib);

		$nama_cont = $form_cont['num_container'];
		$nama_arm  = $form_arm['name'];
		$nama_imp  = $form_imp['num_pib'];

		// $sqlssss ="insert into ".$app['table']['notif']." 
		// 		(id,id_rule,status,pesan,created_by,created_at) values
		// 		('$id_c','".$form_user_detail['id_rule']."',1,'Kiriman baru dengan no pib = $nama_imp, nama containernya = $nama_cont,pengemudinya = $nama_arm ','".$app['me']['id']."',now())";
		$sqlssss ="insert into ".$app['table']['notif']." 
				(id,id_rule,status,pesan,created_by,created_at) values
				('$id_c','3220082018161340',1,'Kiriman baru dengan no pib = $nama_imp, nama containernya = $nama_cont,pengemudinya = $nama_arm ','".$app['me']['id']."',now())";
		db::qry($sql);
		db::qry($sqlss);
		db::qry($sqlsss);
		db::qry($sqlssss);
		// $myfile = fopen("abcde.txt", "w") or die("Unable to open file!");
		// fwrite($myfile, $sqlssss);
		// fclose($myfile);

		$_SESSION['trucking'][] = ['c'=>$nama_cont, 'a'=>$nama_arm];
		exit;
	}elseif($step == 'delete'){
		$sqls 		= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_import` IN ('". $_SESSION['trucking'][$indx]["im"]."') AND `id` IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND `id_armada` IN ('". $_SESSION['trucking'][$indx]["idarm"]."')";
		$sqlss 		="update ". $app['table']['armada']." set status = 'stay' WHERE id = '". $_SESSION['trucking'][$indx]["idarm"]."'";
		$sqlsss 	="update ". $app['table']['container']." set status = 'stay' WHERE id = '". $_SESSION['trucking'][$indx]["ic"]."'";
		$sqlssss 	= "DELETE FROM ". $app['table']['notif'] ." WHERE `id_import` IN ('". $_SESSION['trucking'][$indx]["im"]."') AND `id` IN ('". $_SESSION['trucking'][$indx]["pk"]."') AND `id_armada` IN ('". $_SESSION['trucking'][$indx]["idarm"]."')";
		db::qry($sqls);
		db::qry($sqlss);
		db::qry($sqlsss);
		db::qry($sqlssss);
		unset($_SESSION['trucking'][$indx]);
		exit;
	}
	include "dsp_list.php";
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['import']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT id,num_pib as title FROM ". $app['table']['import'] ." WHERE id IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql 	= "DELETE FROM ". $app['table']['import'] ." WHERE `id` IN ('". $delid ."')";
		$sqls 	= "DELETE FROM ". $app['table']['container'] ." WHERE `id_import` IN ('". $delid ."')";
		db::qry($sql);
		db::qry($sqls);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;

if($act == 'container')
{	
	admlib::validate('UPDT');
	if($step == 'add')
	{
		$jenis = preg_replace('/[^A-Za-z0-9\-]/', '', $jenis);
		$reorders = db::lookup("max(reorder)","container","1=1");
		$nominal = str_replace('Rp ', '', $nom);
		$nominal = str_replace(',', '', $nominal);
		$id_c  = rand(1, 100).date("dmYHis");
		$sqlzx  = "insert into ". $app['table']['container'] ."
		(id, id_import, num_container, qty, note, value, reorder, created_by, created_at,delivery_date,address) values ('$id_c', '$idm', '$num', '$jenis', '$desc', '$nominal', '$reorders','$me',now(),'$dd','$adr')";
		db::qry($sqlzx);
		$_SESSION['container'][] = ['c'=>$num, 'j'=>$jenis, 'd'=>$desc, 'n'=>$nominal, 'dd'=>$dd,'adr'=>$adr];
		exit;
	}elseif($step == 'update')
	{
		// $jenis = preg_replace('/[^A-Za-z0-9\-]/', '', $jenis);
		$reorders = db::lookup("max(reorder)","container","1=1");
		$nominal = str_replace('Rp ', '', $nil);
		$nominal = str_replace(',', '', $nominal);
		$id_c  = rand(1, 100).date("dmYHis");
		$sqlsq  = "update ". $app['table']['container'] ." 
				set num_container 	= '$contain',
					note 			= '$ket',
					value 			= '$nominal',
					qty 	 		= '$qty',
					delivery_date 	= '$dd',
					address 		= '$adr',
					updated_by		= '$me',
					updated_at		= now()
					WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')
		  ";
		db::qry($sqlsq);
		$_SESSION['container'][$indx] = ['c'=>$contain, 'j'=>$qty, 'd'=>$ket, 'n'=>$nominal, 'dd'=>$dd,'adr'=>$adr ];
		exit;
	}elseif($step == 'delete'){
		$sqls 		= "DELETE FROM ". $app['table']['container'] ." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		$sqls321 	= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id_container` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		db::qry($sqls);
		db::qry($sqls321);
		unset($_SESSION['container'][$indx]);
		exit;
	}
	include "dsp_list.php";
}


/*******************************************************************************
* Action : View
*******************************************************************************/
if($act == 'view'):
	// admlib::validate('APPR');
	admlib::validate('DSPL');
	form::init();
	if ($step == 1):
		$form = db::get_record("import", "id", $id);
		$rs['customer'] = db::get_record_select("id, name" , "customer","status='active' ORDER BY name ASC");
		$rs['company'] = db::get_record_select("id, name" , "company","status='active' ORDER BY name ASC");
		$rs['container'] = db::get_recordset("container","id_import='".$id."'");
		$do = db::get_record("delivery_order", "id_import", $id);
		$rs['exp_date'] = db::get_record_select("expired_do", "expired_do", "id_delivery_order='".$do['id']."' order by reorder ASC");
		
		$rs['containers'] = db::get_record_select(" id, CONCAT(num_container,', ',qty,', ',note) AS num_container" , "container","id_import='".$id."' AND status='stay' ORDER BY reorder ASC");

		// $rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS armada" ,"armada","status='stay' ORDER BY reorder ASC");
		$rs['armada'] = db::get_record_select(" id, CONCAT(name,', ',nopol) AS armada" ,"armada");

		
		$rs['container123'] = db::get_record_select("num_container as c, qty as j, note as d, value as n,id pk,id_import im,delivery_date dd,address adr", "container", "id_import", $id);

		$rs['trucking'] = db::get_record_select("a.num_container c,c a", "trucking", "id_import", $id);

		$sqltr = "select cont.num_container c, arm.name a,tr.id_import im,tr.id_container ic,tr.id pk,tr.id_armada idarm from ".$app['table']['trucking']." tr inner JOIN ".$app['table']['container']." cont on tr.id_container = cont.id inner join ".$app['table']['armada']." arm on tr.id_armada = arm.id where tr.id_import='$id'";
		db::query($sqltr, $rs['trucking321'], $nr['trucking321']);

		$_SESSION['container'] = [];
		while($row = db::fetch($rs['container123'])){
			$_SESSION['container'][] = $row;
		}
		$_SESSION['trucking'] = [];
		while($row = db::fetch($rs['trucking321'])){
			$_SESSION['trucking'][] = $row;
		}
		// print_r($_SESSION['container']);
		form::populate($form);
		include "dsp_view.php";
		exit;
	endif;
if ($step == "import") {
form::serialize_form();
		// print_r($_SESSION['income']);
		form::validate('empty','p_num_pib,p_num_bl,p_name_ship,p_eta,customer_name,company,p_date_receipt,p_amount_payment,p_nopen');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$sql = "update ". $app['table']['import'] ."
				set num_pib 		= '$p_num_pib',
					num_bl 			= '$p_num_bl',
					name_ship 		= '$p_name_ship',
					eta		 		= '$p_eta',
					id_customer 	= '$customer_name',
					id_company	 	= '$company',
					date_receipt 	= '$p_date_receipt',
					amount_payment 	= '$p_amount_payment',
					nopen		 	= '$p_nopen',
					updated_by 		= '". $app['me']['id'] ."',
					updated_at 		= now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('modify'));
		// header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		header("location: " .url::get_referer());
		exit;
}

	if ($step == 2):
		admlib::validate('APPR');
		form::serialize_form();
		// form::validate('empty', 'p_no_do,p_date_receipt_do,p_expired_do');
		// if (form::is_error()):
			// msg::build_msg();
			// header("location: ". admlib::$page_active['module'] .".mod&act=view&error=1&id=" . $id);
			// exit;
		// endif;
		$do = db::get_record("delivery_order", "id_import", $id);
		// print_r($_POST);
		// echo "<br>";
		// echo "<br>";
		// echo count($do);
		// print_r($do);
		// exit;
		
		if($do['id_import'] == $id){
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$sql = "update ". $app['table']['delivery_order'] ."
					set no_do 				= '$p_no_do',
						date_receipt_do 	= '$p_date_receipt_do',
						updated_by 			= '". $app['me']['id'] ."',
						updated_at 			= now()
					where id = '".$do['id']."'";
			db::qry($sql);
			if(!empty($p_expired_do)){
				$ids = rand(1, 100).date("dmYHis");
				$sqls = "insert into ".$app['table']['expired_do']."
						(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '".$do['id']."', '$p_expired_do', '". $app['me']['id'] ."', now())";
				db::qry($sqls);
			}
		}else{
			$ids = rand(1, 100).date("dmYHis");
			$idx = rand(1, 100).date("dmYHis");
			app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
			$reorder = db::lookup("max(reorder)","delivery_order","1=1");
			if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
			$sql = "insert into ".$app['table']['delivery_order']."
					(id, id_import, no_do, date_receipt_do, id_expired_do, reorder, created_by, created_at) values
					('$ids', '$id', '$p_no_do', '$p_date_receipt_do', '$idx', '$reorder', '". $app['me']['id'] ."', now())";
			db::qry($sql);
			if(!empty($p_expired_do)){
				$sqls = "insert into ".$app['table']['expired_do']."
						(id, id_delivery_order, expired_do, created_by, created_at) values ('$idx', '$ids', '$p_expired_do', '". $app['me']['id'] ."', now())";
				db::qry($sqls);
			}
		}
		
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
if ($act == 'del_or') {
	if($step == "add"){
		form::serialize_form();
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;

		if (strtotime($p_expired_do) < time()) {
			$status_do = ",'expired'";	
		}
		$t_containter 		= 0;
		$container 		= $_SESSION['container'];
		$p_amount_payment = str_replace('Rp ', '', $p_amount_payment);
		$p_amount_payment = str_replace(',', '', $p_amount_payment);
		$id = rand(1, 100).date("dmYHis");
		$ids = rand(1, 100).date("dmYHis");
		
		$reorder = db::lookup("max(reorder)","delivery_order","1=1");
		
		app::mq_encode('p_num_pib,p_num_bl,p_name_ship,p_eta,p_customer_name,p_company,p_date_receipt,p_amount_payment,p_nopen');
		$sql = "insert into ".$app['table']['delivery_order']."
				(id, no_do, date_receipt_do, id_expired_do, reorder, created_by, created_at,id_import,status) values
				('$id', '$p_no_do', '$p_date_receipt_do', '$ids', '$reorder', '$me', now(),
				'$id_imp'$status_do)";
		$sqls = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id', '$p_expired_do', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		db::qry($sqls);
		msg::set_message('success', app::getliblang('create'));
		header("location: " .url::get_referer());
		exit;
}
	if ($step == "edit") {
		form::serialize_form();
//		form::validate('empty','p_no_do,p_date_receipt_do');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		if (strtotime($p_expired_do) > time()) {
			$status_do = ",status = 'active' ";
		}
		else{
			$status_do = ",status = 'expired' ";	
		}
		app::mq_encode('p_no_do,p_date_receipt_do,p_expired_do');
		$sql = "update ". $app['table']['delivery_order'] ."
				set no_do 				= '$p_no_do',
					date_receipt_do 	= '$p_date_receipt_do',
					updated_by 			= '". $app['me']['id'] ."',
					updated_at 			= now()
					$status_do
				where id_import = '$id_imp'";
		db::qry($sql);
		if(!empty($p_expired_do)){
		$ids = rand(1, 100).date("dmYHis");
		$sqls = "update ".$app['table']['expired_do']." set
				 status = 'inactive' where id_delivery_order = '$id_idv_ord' ";
		db::qry($sqls);
		$sqlss = "insert into ".$app['table']['expired_do']."
				(id, id_delivery_order, expired_do, created_by, created_at) values ('$ids', '$id_idv_ord', '$p_expired_do','$me',now())";
		msg::set_message('success', app::getliblang('modify'));
		header("location: " .url::get_referer());
		db::qry($sqlss);
		}
	}
}
?>