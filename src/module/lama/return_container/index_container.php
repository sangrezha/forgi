<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'return_container','caption'=>'return_container');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$option = ['sortable'=>true];
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'return_container', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.id, a.status,a.addressed addressed,d.num_container num_cont, e.name AS name, e.nopol nopol_truck,  b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
	FROM ". $app['table']['return_container'] ." a
			LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id) 
			LEFT JOIN ". $app['table']['container'] ." d ON (a.id_container=d.id) 
			LEFT JOIN ". $app['table']['armada'] ." e ON (a.id_armada=e.id) 
			$q ORDER BY a.reorder ASC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['num_cont','name','nopol_truck','addressed'];
	while($row = db::fetch($rs['row']))
	{
		$results[] 		= $row;
	}
	$option = ['sortable'=>true,'status'=>false];
	$statusx="asads";
	include $app['pwebmin'] ."/include/blk_list_mod.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		// $validate ='p_name,p_ktp,p_phone,p_nopol,p_type';
		form::serialize_form();
		// form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_name,p_ktp,p_phone,p_nopol,p_type');
		$reorder = db::lookup("max(reorder)","return_container","1=1");
		var_dump($_REQUEST);
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$sql = "insert into ".$app['table']['return_container']."
				(id, id_container, id_armada, addressed, reorder, created_by, created_at) values
				('$id','$contain', '$armada', '$p_addressed', '$reorder', '". $app['me']['id'] ."', now())";
		$sqlss ="update ". $app['table']['armada']." set status = 'stay' WHERE id = '$armada'";
		$sqlsss ="update ". $app['table']['container']." set proses = 2 WHERE id = '$contain'";
		db::qry($sql);
		db::qry($sqlss);
		db::qry($sqlsss);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		// $form = db::get_record_filter("id, name, ktp, phone, nopol, type ","return_container", "id", $id);
		$form = db::get_record("return_container", "id", $id);
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		// form::validate('empty','p_name,p_ktp,p_phone,p_nopol,p_type');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		app::mq_encode('p_name,p_ktp,p_phone,p_nopol,p_type');
		$sql = "update ". $app['table']['return_container'] ."
				set id_armada	 = '$armada',
					id_container = '$contain',
					addressed 	 = '$addressed',
					updated_by 	 = '". $app['me']['id'] ."',
					updated_at 	 = now()
				where id = '$id'";
		$sqls ="update ". $app['table']['container']." set status = 1 WHERE id = '$contain'";
		db::qry($sql);
		db::qry($sqls);
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "stay" ):
		$statusnya = "depart";
	elseif ( $status == "depart" ):
		$statusnya = "stay";
	endif;
	$sql = "UPDATE ".$app['table']['return_container']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['return_container']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT a.id,num_container title,id_container FROM ". $app['table']['return_container'] ." a inner join ". $app['table']['container'] ." b on a.id_container = b.id
					WHERE a.id IN ('". $items ."')";
			// echo $sql;exit;
			db::query($sql, $rs['row'], $nr['row']);
			$abc="abc";
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid		= implode("','", $p_id);
		$contain 	= implode("','", $id_container);
		$sql 	= "DELETE FROM ". $app['table']['return_container'] ." WHERE `id` IN ('". $delid ."')";
		$sqls 	="update ". $app['table']['container']." set proses = 1 WHERE id = '$contain'";
		db::qry($sql);
		db::qry($sqls);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;
?>
