<?php
$income = $_SESSION[$act];
$trucking = $_SESSION["trucking"];
if((count($income) > 0) AND $act == "container"){ 
admlib::get_component('datepickerlib');
// var_dump($app['me']);
?>
<div class="table-responsive"> 
    <table class="table table-bordered"> 
        <thead> 
            <tr> 
                <th>#</th> 
                <th>Container</th> 
                <th style="text-align: center;">QTY<br>(20,40,45,LCL)</th> 
                <th>Keterangan</th>
                <th>Nilai</th>  
                <th>Delivery Date</th>  
                <th>Address</th>  
                <?php if($act == 'expense'){ ?>
                    <th>Expense Type</th> 
                <?php } ?>
                <!-- <th>&nbsp;</th> -->
                <th>Delete</th> 
                <th>Update</th>
            </tr> 
        </thead> 
        <tbody> 
            <?php 
                $total = 0;
                $no = 0;
                foreach($income as $key => $val){ $no++;
                    $total = $total + $val['n'];     
            ?>
                <tr> 
                    <th width="5%" scope="row"><?php echo $no;?></th> 
                    <td >
                    <input  type="text" style="width:100px" name="p_container<?php echo $no ?>" id="container321<?php echo $no ?>" value="<?php echo $val['c'] ?>" class="" >
                          </td>
                          <td >
                    <!-- <input  type="text" style="width:50px" name="p_qty<?php echo $no ?>" id="qty<?php echo $no ?>" value="<?php echo $val['j'] ?>" class="" > -->
               <select name="qty" id="qty<?php echo $no;?>" class="form-control">
<?php  $qtys=array(null=>"Select","20"=>"20'","40"=>"40'","45"=>"45'","LCL"=>"LCL");
// var_dump($qtys);
foreach ($qtys as $kunci => $nilai) {
?>      
        <option value="<?php echo $kunci ?>" <?php echo(($kunci == $val['j'])?"selected":null ) ?>><?php echo $nilai ?></option>
<?php } ?>
                </select> 
        </td> 
                    <td>
 <div class="col-md-12 col-sm-6 col-xs-12">
        <textarea class="form-control" id="keterangan<?php echo $no ?>" value=""><?php echo $val['d'] ?></textarea>
               </div>
            </div>
        </td> 
                    <td>   
                    <input type="text" style="width:100px" name="p_nilai<?php echo $no ?>" id="nilai<?php echo $no ?>" value="<?php echo $val['n'] ?>" class="money" >
                    </td> 

<td> <input type="text"  style="width:100px" name="p_delivery_date<?php echo $no ?>" value="<?php echo $val['dd'] ?>" class=" datepickersingle" aria-describedby="delivery_date" >
</td>
     <td>
        <textarea class="form-control" id="alamat<?php echo $no ?>"><?php echo $val['adr'] ?></textarea>
         </td> 
                    <?php if($act == 'expense'){ ?>
                        <td><?php echo db::lookup('name', 'expense', 'id', $val['e']); ?></td> 
                    <?php } ?>
                
                    <td width="5%" align="center"><a href="#" act="<?php echo $act;?>" val="<?php echo $key;?>" class="delete"><span class="fa fa-trash" aria-hidden="true"></span></td>
   

                     <!--    <td width="5%" align="center"><a href="#" act="<?php echo $act;?>" val="<?php echo $key;?>" class="update"><span class="fa fa-pencil" aria-hidden="true"></span></td> -->
                        <td width="5%" align="center"><a href="#" act="<?php echo $no;?>" val="<?php echo $key;?>" class="update"><span class="fa fa-pencil" aria-hidden="true"></span></td>
                </tr> 
            <?php 
                    }
            ?>
            <input type="hidden" name="p_me_id" id="me_container" value="<?php echo $app['me']['id']; ?>">
            <tr> 
                <th scope="row" colspan="2">&nbsp;</th> 
                <td><b>Total</b></td> 
                <td><b>:</b></td> 
                <td><b>Rp <?php echo number_format($total, 2, ',', '.');?></b></td> 
                <td colspan="4">&nbsp;</td>
            </tr> 
        </tbody> 
    </table> 
</div>
<?php }
elseif((count($trucking) > 0) AND $act=="trucking"){
 ?>

<div class="table-responsive"> 
    <table class="table table-bordered"> 
        <thead> 
            <tr> 
                <th>#</th> 
                <th><?php echo app::getliblang("container"); ?></th>
                <th><?php echo app::getliblang("armada"); ?></th> 
                <th><?php echo app::getliblang("date_return"); ?></th> 
                <th><?php echo app::getliblang("addressed"); ?></th>  
                <th><?php echo app::getliblang("status_return"); ?></th> 
                <th><?php echo app::getliblang("hapus"); ?></th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
                $total = 0;
                $no = 0;
                foreach($trucking as $key => $val){
                    $no++;
                    $total = $total + $val['n'];     
            ?>
                <tr> 
                    <th width="5%" scope="row"><?php echo $no;?></th> 
                    <td >
                    <?php echo $val['c'] ?>
                    </td>
                    <td >
                    <?php echo $val['c'] ?>
                    </td>
                    <td >
                    <?php echo $val['dr']!=""?date("d-m-Y",strtotime($val['dr'])):null; ?>
                    </td>
                    <td >
                    <?php echo $val['adr'] ?>
                    </td>
                    <td >
                    <?php  
        if ($val['st_rc']=="proses") {
            $proses= app::getliblang('proses');
            $val['st_rc']="<font color='red'>$proses</font>";
        }
        elseif ($val['st_rc']=="selesai") {
            $proses= app::getliblang('selesai');
            $val['st_rc']="<font color='green'>$proses</font>";
        }
        elseif ($val['st_rc']=="terjadwal") {
            $proses= app::getliblang('terjadwal');
            $val['st_rc']="<font color='blue'>$proses</font>";
        }
        else{
            $proses= app::getliblang('belum_buat');
            $val['st_rc']="<font color='orange'>$proses</font>";    
        }
echo $val['st_rc'];
                     ?>
                    </td>
                    <td width="5%" align="center"><a href="#" id="deletetr" act="<?php echo $act;?>" val="<?php echo $key;?>" class="deletetr"><span class="fa fa-trash" aria-hidden="true"></span></td>
                </tr> 
            <?php 
                    }
            ?>
            <input type="hidden" name="p_me_id" id="me_container" value="<?php echo $app['me']['id']; ?>">
        </tbody> 
    </table> 
</div>
<?php } ?>
<script>
        $(function(){
        var formatCurrency = function(number){
            var n = number.split('').reverse().join("");
            var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");
            return 'Rp ' + n2.split('').reverse().join('');
        };
        $( document ).ready(function() {
            $('.money').on('input', function(e){
                $(this).val(formatCurrency(this.value.replace(/[,Rp ]/g,'')));
            }).on('keypress',function(e){
                if(!$.isNumeric(String.fromCharCode(e.which))) e.preventDefault();
            }).on('paste', function(e){
                var cb = e.originalEvent.clipboardData || window.clipboardData;
                if(!$.isNumeric(cb.getData('text'))) e.preventDefault();
            });
            $('.money').val(formatCurrency($('.money').val().replace(/[,Rp ]/g,'')));
        });
    });

</script>