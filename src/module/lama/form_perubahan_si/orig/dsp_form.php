<?php
admlib::display_block_header();
	admlib::get_component('texteditorlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				"value"=>app::ov($form['name']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"nik",
				"value"=>app::ov($form['nik']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"dept",
				"value"=>app::ov($form['dept']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"tgl_pengajuan",
				"value"=>app::ov($form['tgl_pengajuan']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"cat_si",
				"value"=>app::ov($form['cat_si']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"now_con",
				"value"=>app::ov($form['now_con']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"want_cond",
				"value"=>app::ov($form['want_cond']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"goal",
				"value"=>app::ov($form['goal']),
			)
		);
		admlib::get_component('inputupload',
			array(
				"name"=>"attachment",
				"value"=>app::ov($form['attachment']),
				"filemedia"=>true
			)
		);
		// $form['sys_need'] = "laptop";
		admlib::get_component('radio',
			array(
				"name"=>"hasil",
				"datas"=>["done","revisi"],
				"value"=>app::ov($form['hasil'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"note",
				"value"=>app::ov($form['note']),
			)
		);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"user",
		// 		"value"=>app::ov($form['user']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"password",
		// 		"value"=>app::ov($form['password']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"ipaddress",
		// 		"value"=>app::ov($form['ipaddress']),
		// 	)
		// );
		// admlib::get_component('textarea',
		// 	array(
		// 		"editor"=>true,
		// 		"name"=>"content",
		// 		"value"=>app::ov($form['content']),
		// 	)
		// );
		if(isset($id))
		{
			admlib::get_component('language',
				array(
					"name"=>"lang",
					"value"=>app::ov($form['lang'])
				)
			);
		}
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
