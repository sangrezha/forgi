<html><head>
	<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
	</style>
	<title></title>
</head>
<!-- <body onload="doPrint()"> -->
<body onload="">
	<table width="900" border="0" align="center">
	    <tbody><tr>
	        <td style="display: flex; align-items: center;border: 1px solid black;">	        	
<!-- 	            <img src="logo.png" alt="" style="width: 200px; height: 150px; display: block; text-align: left;"> -->
	            <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 200px; height: auto; display: block; text-align: left;border-right: 1px solid black;">
	            <span style="display: block;font-size: 20px;font-weight: bold;margin-left: 100px;letter-spacing: 5px"><p>FORM PERMINTAAN AKSES FOLDER</p></span>
	        </td>
	    </tr>
	</tbody></table>
	<div style="border: 1px solid black;width: 172px;height: 30px;margin-left: 730px;text-align: center;">
	   Revisi <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
	</div>
	<table width="900px" border="0" align="center">
	<tbody>
	<tr>
		<td>Name</td>
		<td>:</td>
		<td><?=  $form['name'] ?></td>
	</tr>
	<tr>
		<td>NIK</td>
		<td>:</td>
		<td><?=  $form['nik'] ?></td>
	</tr>
	<tr>
		<td>Section</td>
		<td>:</td>
		<td><?= $section ?></td>
	</tr>
	<tr>
		<td>Tanggal Pengajuan</td>
		<td>:</td>
		<td><?=  $form['tgl_pengajuan'] ?></td>
	</tr>
		<tr>
			<!-- <td  style="border-top: 1px solid black;" colspan="3"></td> -->
	 </tr>
	<tr>
		<td>System yang dibutuhkan</td>
		<td>:</td>
		<td>
				<input type="radio" id="baru" name="p_kat_si" value="js" <?= ($form['kat_si']=="js"?"checked":"") ?> >
				<label for="baru">JS Systems ( ERP )</label>
				<input type="radio" id="ada" name="p_kat_si" value="andon" <?= ($form['kat_si']=="andon"?"checked":"") ?> >
				<label for="ada">Andon System</label>
				<input type="radio" id="ada" name="p_kat_si" value="email" <?= ($form['kat_si']=="email"?"checked":"") ?> >
				<label for="ada">Email</label>
				<input type="radio" id="ada" name="p_kat_si" value="tp" <?= ($form['kat_si']=="tp"?"checked":"") ?> >
				<label for="ada">TP PEB</label>
	</tr>
	<tr>
		<td>Penjelasan kondisi saat ini</td>
		<td>:</td>
		<td><?=  $form['saat_ini'] ?></td>
	</tr>
	<tr>
		<td>Kondisi yang di inginkan</td>
		<td>:</td>
		<td><?=  $form['di_inginkan'] ?> </td>
	</tr>
	<tr>
		<td>Tujuan perubahan</td>
		<td>:</td>
		<td><?=  $form['tujuan'] ?> </td>
	</tr>
		<!-- <tr>
			<td>Alasan</td>
			<td>:</td>
			<td><?= $form['alasan'] ?></td>
	 </tr> -->
		
	</tbody>
</table>
	<div style="/*! border: 1px solid black; */width: 900px;margin-left: 490px;margin-top: 50px;padding-bottom: 20px;">
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
<!-- <p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p> -->
  <table border="1">
    <tbody><tr style="text-align: center;">
      <td>Staff IT</td>
      <td>Manager IT</td>
      <td style="border: 0px;"></td>
      <td>Manager</td>
      <td>Supervisor</td>
      <td>Pemohon</td>
    </tr>
    <tr style="text-align:center;">
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="border: 0px;border:0px;height: 80px;width: 100px;"><======</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
    </tr>
    <tr style="text-align: center;">
      <td><?= $approve_mng_it_name ?></td>
      <td><?= $approve_it_name ?></td>
      <td style="border: 0px;"></td>
	  <td><?= $approve_spv_name ?></td>
	  <td><?= $approve_mng_name ?></td>
	  <td><?= $form['name'] ?></td>
    </tr>
  </tbody></table>
</div>
  <div style="border: 1px solid black;width: 900px;margin-left: 227px;">
	<table style="padding: 25px;">
		<tr>
			<td>Hasil</td>
			<td>
				:
			</td>
			<td>
				<input type="radio" id="done" name="p_done" value="done" <?= (empty($approved_date)?"":"checked") ?> ><label for="done">done</label>
			</td>
			<td>
				<input type="radio" id="undone" name="p_done" value="undone" <?= (empty($approved_date)?"checked":"") ?> ><label for="undone">undone</label>
			</td>
			<td style="width:450px;">
				
			</td>
			<td>
				tanggal
			</td>
			<td>
				:
			</td>
			<td>
				<?= $approved_date ?>
			</td>
		</tr>
		<tr>
			<td>Catatan</td>
			<td>
				:
			</td>
			<td>
				<?= $form['note'] ?>
			</td>
		</tr>
	</table>
</div>
	<div style="/*! border: 1px solid black; */width: 900px;margin-left: 807px;margin-top: 50px;padding-bottom: 20px;">
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
<!-- <p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p> -->
  <table border="1">
    <tbody><tr style="text-align: center;">
      <td>Leader</td>
      <td>Manager</td>
      <td>Pemohon</td>
    </tr>
    <tr style="text-align:center;">
	<td style="/*! border: 1px solid black; */height: 80px;width: 100px;"><?php if(!empty($approved_date)){ ?>DTO<?php } ?></td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;"><?php if(!empty($approved_date)){ ?>DTO<?php } ?></td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;"><?php if(!empty($approved_date)){ ?>DTO<?php } ?></td>
    </tr>
    <tr style="text-align: center;">
      <td><?= $approve_spv_name ?></td>
	  <td><?= $approve_mng_it_name ?></td>
	  <td><?= $form['name'] ?></td>
    </tr>
  </tbody></table>
</div>
<script>
	function doPrint() {
	    window.print();            
	    document.location.href = "trucking.mod"; 
	}
</script>
<div state="voice" class="placeholder-icon" id="tts-placeholder-icon" title="Click to show TTS button" style="background-image: url(&quot;moz-extension://d957ec0f-edb7-42ef-a0a8-6f64e9a2a6ec/data/content_script/icons/voice.png&quot;); display: none;"><canvas width="36" height="36" class="loading-circle" id="text-to-speech-loader" style="display: none;"></canvas></div></body></html>