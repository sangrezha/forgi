<html><head>
	<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
	</style>
	<title></title>
</head>
<!-- <body onload="doPrint()"> -->
<body onload="">
	<table width="900" border="0" align="center">
	    <tbody><tr>
	        <td style="display: flex; align-items: center;border: 1px solid black;">	        	
<!-- 	            <img src="logo.png" alt="" style="width: 200px; height: 150px; display: block; text-align: left;"> -->
	            <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 200px; height: auto; display: block; text-align: left;border-right: 1px solid black;">
	            <span style="display: block;font-size: 20px;font-weight: bold;margin-left: 100px;letter-spacing: 5px"><p>FORM PERMINTAAN AKSES FOLDER</p></span>
	        </td>
	    </tr>
	</tbody></table>
	<div style="border: 1px solid black;width: 172px;height: 30px;margin-left: 730px;text-align: center;">
	   Revisi <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
	</div>
	<table width="900px" border="0" align="center">
	<tbody>
		<tr>
			<td style="padding-bottom: 10px;"><b>Company</b></td>
			<td>:</td>
			<td><?=  $form['company'] ?></td>
	 	</tr>
		<tr>
			<!-- <td  style="border-top: 1px solid black;" colspan="3"><b>Contact Person</b></td> -->
			<td  colspan="3"><b>Contact Person</b></td>
	 </tr>
	<tr>
		<td style="padding-left: 20px;">Name</td>
		<td>:</td>
		<td><?=  $form['name'] ?></td>
	</tr>
	<tr>
		<td style="padding-left: 20px;">HP</td>
		<td>:</td>
		<td><?=  $form['hp'] ?></td>
	</tr>
		<tr>
			<!-- <td  style="border-top: 1px solid black;" colspan="3"></td> -->
	 </tr>
	<tr>
		<td style="padding-top: 10px;"><b>A. System yang dibutuhkan</b></td>
		<td>:</td>
		<td>
				<input type="radio" id="baru" name="p_used_sys" value="hr" <?= ($form['used_sys']=="hr"?"checked":"") ?> >
				<label for="baru">HR System</label>
				<input type="radio" id="ada" name="p_used_sys" value="andon" <?= ($form['used_sys']=="andon"?"checked":"") ?> >
				<label for="ada">Andon System</label>
				<input type="radio" id="ada" name="p_used_sys" value="erp" <?= ($form['used_sys']=="erp"?"checked":"") ?> >
				<label for="ada">ERP System</label>
				<input type="radio" id="ada" name="p_used_sys" value="internet" <?= ($form['used_sys']=="internet"?"checked":"") ?> >
				<label for="ada">Internet</label>
				<input type="radio" id="ada" name="p_used_sys" value="ada" <?= ($form['used_sys']=="other"?"checked":"") ?> >
				<label for="ada">Others</label>
	</tr>
	<?php if($form['used_sys']=="other"){ ?>
	<tr>
		<td style="padding-left:20px">Other</td>
		<td>:</td>
		<td><?= $form["other"] ?></td>
	</tr>
	<?php } ?>
	<tr>
			<!-- <td  style="border-top: 1px solid black;" colspan="3"><b>Contact Person</b></td> -->
			<td  colspan="3"><b>B. Penjelasaan</b></td>
	 </tr>
	<tr>
		<td style="padding-left: 20px;">Keperluan</td>
		<td>:</td>
		<td><?=  $form['keperluan'] ?></td>
	</tr>
	<tr>
		<td style="padding-left: 20px;">Waktu</td>
		<td>:</td>
		<td><?=  $form['waktu']." ".$form["satuan"] ?> </td>
	</tr>
		<!-- <tr>
			<td>Alasan</td>
			<td>:</td>
			<td><?= $form['alasan'] ?></td>
	 </tr> -->
		
	</tbody>
</table>
	<div style="/*! border: 1px solid black; */width: 900px;margin-left: 600px;margin-top: 50px;">
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
<!-- <p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p> -->
  <table border="1">
    <tbody><tr style="text-align: center;">
      <td>Leader</td>
      <td>Manager</td>
      <td>Staff IT</td>
      <td>Manager IT</td>
    </tr>
    <tr style="text-align:center;">
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <!-- <td style="border:0px;height: 80px;width: 100px;"><======</td> -->
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
    </tr>
    <tr style="text-align: center;">
      <td><?= $approve_spv_name ?></td>
      <td><?= $approve_mng_name ?></td>
	  <td><?= $approve_it_name ?></td>
	  <td><?= $approve_mng_it_name ?></td>
    </tr>
  </tbody></table>
  
</div>
<script>
	function doPrint() {
	    window.print();            
	    document.location.href = "trucking.mod"; 
	}
</script>
<div state="voice" class="placeholder-icon" id="tts-placeholder-icon" title="Click to show TTS button" style="background-image: url(&quot;moz-extension://d957ec0f-edb7-42ef-a0a8-6f64e9a2a6ec/data/content_script/icons/voice.png&quot;); display: none;"><canvas width="36" height="36" class="loading-circle" id="text-to-speech-loader" style="display: none;"></canvas></div></body></html>