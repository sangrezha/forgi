<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	admlib::get_component('select2lib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		admlib::get_component('inputtext',
			array(
				"name"=>"company",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['company'])
			)
		);
		?>
		<hr>
		<?php
		admlib::get_component('blank',
			array(
				"name"=>"contact_person",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
			)
		);
		?>
		<!-- <hr> -->
		<?php
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['name'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"hp",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['hp'])
			)
		);
		?>
		<hr>
		<?php
		admlib::get_component('radio',
			array(
				"name"=>"used_sys",
				"datas"=>["hr","andon","erp","internet","other"],
				"validate"=>true,
				"value"=>app::ov($form['used_sys'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"other",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['other'])
			)
		);
		// print_r($app['me']);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"function",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		"value"=>app::ov($form['function'])
		// 	)
		// );
		// $rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		// admlib::get_component('select',
		// 	array(
		// 		"name"=>"id_departement",
		// 		"value"=>app::ov($app['me']['id_departement']),
		// 		"items"=>$rs['departement']
		// 	)
		// );
		?>
		<hr>
		<?php
		admlib::get_component('blank',
			array(
				"name"=>"penjelasan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
			)
		);
		?>
		<br>
		<?php
		admlib::get_component('textarea',
			array(
				"name"=>"keperluan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['keperluan'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"waktu",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['waktu'])
			)
		);
		admlib::get_component('radio',
			array(
				"name"=>"satuan",
				"datas"=>["jam","hari"],
				"validate"=>true,
				"value"=>app::ov($form['satuan'])
			)
		);
		

		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
	?>
	
<script>

$(document).ready(function () {
	<?php //if(empty($form['used_sys']) || $form['used_sys'] == "other"){ 
		  if($form['used_sys'] != "other"){ 
		?>
		$("#g_other").hide();
	<?php } ?>
		$('input[type=radio][name=p_used_sys]').change(function() {
			if (this.value == 'other') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_other").show();
			}
			else{
				// alert("Transfer Thai Gayo");
              	$("#g_other").hide();
				  $('#other').value(""); 
			}
		});
});

</script>
	<?php
admlib::display_block_footer();
?>