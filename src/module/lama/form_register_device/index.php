<?php
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');
$p_masa_simpan = 3;
## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('module'=>'form_register_device','caption'=>ucwords('Registrasi Perangkat Penyimpanan Data'));
// $p_masa_simpan = 0;
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$module = admlib::$page_active['module'];
	$total 	= db::lookup('COUNT(id)', $module, '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	if($app['me']['level']==1){
		if($app['me']['code']!="it"){
			$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.id ='".$app['me']['id']."'";
		}else{
			$where = " AND a.status_progress NOT LIKE 'unverified' AND a.status_progress NOT LIKE 'partly'";
		}
	}
	// if($app['me']['level']==2){
	// 	$where = " AND b.id_section ='".$app['me']['id_section']."' AND b.level <='2'";
	// }
	// if($app['me']['level']>2){
	// 	$where = " AND b.id_section ='".$app['me']['id_section']."'";
	// }
	if($app['me']['level']>2){
		if($app['me']['code']!="it"){
			// $where = " AND b.id_section ='".$app['me']['id_section']."' AND b.id ='".$app['me']['id']."'";
			$where = " AND b.id_section ='".$app['me']['id_section']."'";
		}else{
			$where = " AND a.status_progress NOT LIKE 'unverified' AND a.status_progress NOT LIKE 'partly'";
			// $where = " AND b.id_section ='".$app['me']['id_section']."'";
		}
	}
	$q = "WHERE 1=1";
	if($ref)
	{
		$q .= " AND a.name LIKE '%". $ref ."%' OR a.lang LIKE '%". $ref ."%'";
	}
	// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$module] ." a
	// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	// 	$q ORDER BY a.lang,a.reorder";
	$sql = "SELECT a.*, b.name as created_by, c.name as modifyby FROM ". $app['table'][$module] ." a
	LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['view']['user'] ." c ON (a.updated_by=c.id)
	$q $where ORDER BY a.lang,a.reorder";
	app::set_navigator($sql, $nav, $page_size, $module .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	// progress
	if($app['me']['code'] =="it"){
		// $columns = ["feedback",'approve_it',"finish_it"];
		$columns = ["view",'approve_it',"approve_it_3","approve_it_5"];
	}else{
		$columns = ['approve_2','approve_3','approve_4','approve_5','confirm_done'];
	}
	$columns = array_merge($columns,['print','name','status_progress']);
	// $columns = ['approve_2','approve_3','approve_it','confirm_done','name'];
	$numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	if($numlang > 1)
	{
		$columns = array_merge($columns,['lang','duplicate']);
	}
	// $columns = array_merge($columns,["approve"]);
	while($row = db::fetch($rs['row']))
	{
		$row['view'] = '<a style="margin-left: 45% !important;" href="'.$app['webmin'] .'/'. $module .'.mod&act=view&id='. $row['id'].'"><i class="fa fa-eye" aria-hidden="true"></i></a>';
		// echo "disini".$row['approve_it_3']." oke";
		if($row['approve_2']=="tidak"){
			$row['approve_2'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
		}elseif($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_2'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
		}
		if($row['approve_3']=="tidak"){
			$row['approve_3'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
		}elseif($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_3'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
		}
		if($row['approve_4']=="tidak"){
			$row['approve_4'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
		}elseif($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_4'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_4'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
		}
		if($row['approve_5']=="tidak"){
			$row['approve_5'] = '<span style="margin-left: 35% !important;"><a style="color:green;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a><a style="margin-left: 10% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&reject=1&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-times" aria-hidden="true"></i></a></span>';
		}elseif($row['status_progress']=="rejected"){
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_5'] = '<i style="margin-left: 45% !important;color:red" class="fa fa-times"></i>';
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_5'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
		}
		if($row['approve_it']=="tidak"){
			// $row['approve_it'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			$row['approve_it'] = '<a style="margin-left: 45% !important;color:red;" href="'.$app['webmin'] .'/'. $module .'.mod&act=approve_it&id='. $row['id'].'"><i class="fa fa-check" aria-hidden="true"></i></a>';
			$row['finish_it'] = "";
			$row['approve_it_3'] = "";
		}else{
			// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
			$row['approve_it'] = '<a href="'.$app['webmin'] .'/'. $module .'.mod&act=approve_it&id='. $row['id'].'"><i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i></a>';
			// approve_3_it
			if($row['approve_it_3']!="iya"){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				// echo "disini";
				$row['approve_it_3'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it_3&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
				$row['approve_it_5'] = "";
				// approve_it_5
			}else{
				$row['approve_it_3'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
					if($row['approve_it_5']!="iya"){
						$row['approve_it_5'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve_it_5&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
						// approve_it_5
					}else{
						$row['approve_it_5'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
					}

				// if($row['finish_it']=="tidak" || empty($row['finish_it'])){
				// 	// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				// 	$row['finish_it'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=finish_it&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
				// }else{
				// 	// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				// 	$row['finish_it'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
				// }
			}
		}
		if($row['status_progress']=="progress" || $row['status_progress']=="finished"){
			if($row['confirm_done']=="tidak" || empty($row['confirm_done'])){
				// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
				$row['confirm_done'] = '<a style="margin-left: 45% !important;color:orange;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&act=confirm_done&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
			}else{
				// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
				$row['confirm_done'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
			}
		}else{
			$row['confirm_done'] = '';
		}
		if($row['status_progress']=="progress" || $row['status_progress']=="finished"){
			$row['print']='<a href="'.$app['webmin'] .'/'. $module .'.mod&act=print&id='. $row['id'].'&name='.$row['name'].'" ><i style="margin-left: 30% !important;color:grey;font-size:18px" class="fa fa-print"></i></a>';
		}else{
			$row['print']='<a href="'.$app['webmin'] .'/'. $module .'.mod&act=print&id='. $row['id'].'&name='.$row['name'].'" ><i style="margin-left: 30% !important;color:grey;font-size:18px" class="fa fa-print"></i></a>';
		}
		// if($row['status_progress']!="iya"){
		// 	// $row['approve_3'] = '<a style="margin-left: 45% !important;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		// 	$row['status_progress'] = '<a style="margin-left: 45% !important;color:red;" onclick="onkonfirm(\''.$app['webmin'] .'/'. $module .'.mod&type=approve_2&act=approve&id='. $row['id'].'\',name=\''.$row['name'].'\')"><i class="fa fa-check" aria-hidden="true"></i></a>';
		// }else{
		// 	// $row['approve_2']= '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-check" aria-hidden="true"></i>';
		// 	$row['status_progress'] = '<i style="margin-left: 45% !important;color:#4CAF50" class="fa fa-thumbs-up"></i>';
		// }
		if($numlang > 1)
		{
			$row['duplicate'] = '<a href="'.  $app['webmin'] .'/'. $module .'.mod&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		}
		else {
			$row['duplicate'] = null;
		}
		$results[] = $row;
	}
	$option = ['sortable'=>false, 'status'=>true];
	if($app['me']['code']=="it"){
		$option = array_merge($option,['no_edit'=>true]);
	}
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
?>
<?php
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		form::populate($module);
		// $_SESSION["add_register"] = "";
		unset($_SESSION['add_register']);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		if($app['me']["code"] != "it"){
			// if($status_f == "baru"){
			// 	$validate = 'p_name,p_status_f,p_function';
			// }else{
			// 	$validate = 'p_name,p_status_f';
			// }
			form::serialize_form();
			// form::validate('empty', $validate);
			if (form::is_error()):
				msg::build_msg();
				header("location: ". $module .".mod&act=add&error=1");
				exit;
			endif;
			// print_r($_SESSION);
			// var_dump($_SESSION);
			// exit;
			app::mq_encode('p_name,p_status_f,p_function,p_own');
			// $identity = rand(1, 100).date("dmYHis");
			// $p_images = str_replace("%20"," ","$p_images");
			// $sql_lang 	= "SELECT id FROM ". $app['table']['lang'] ." WHERE status='active'";
			$p_alias = app::slugify($p_title);
			// db::query($sql_lang, $rs['lang'], $nr['lang']);
			// while($row 	= db::fetch($rs['lang']))
			// {
				if($app['me']['level'] >= 3){
					$approved 		= ",approve_3";
					$approved_val 	= ",'iya'";
					$status_progress 	= ",status_progress";
					$status_progress_val 	= ",'fully'";
					$log_approve = "APR_MNG";
				}
				if($app['me']['level'] >= 3){
					$approved 		= ",approve_3";
					$approved_val 	= ",'iya'";
					$status_progress 	= ",status_progress";
					$status_progress_val 	= ",'partly'";
					$log_approve = "APR_MNG";
				}
				if($app['me']['level'] >= 3){
					$approved 		= ",approve_3";
					$approved_val 	= ",'iya'";
					$status_progress 	= ",status_progress";
					$status_progress_val 	= ",'partly'";
					$log_approve = "APR_MNG";
				}
				// if($app['me']['level'] == 2){
				// 	$approved 		= ",approve_2";
				// 	$approved_val 	= ",'iya'";
				// 	$status_progress 	= ",status_progress";
				// 	$status_progress_val 	= ",'partly'";
				// 	$log_approve = "APR_SPV";
				// }
				$id 		= rand(1, 100).date("dmYHis");
				// $p_lang 	= $row['id'];
				$id 		= db::cek_id($id,$module);
				$urut 		= db::lookup("max(reorder)",$module,"1=1");
				// $data = db::get_record("feedback","id_project ='$id' AND id_client = '$p_client_id'");
				if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
				$sql 		= "INSERT INTO ".$app['table'][$module]."
						(id, tgl_pengajuan, id_section, id_departement, reorder, created_by, created_at$approved $status_progress) VALUES
						('$id', '$p_tgl_pengajuan', '".$p_id_section."', '$p_id_departement', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";
				
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "INS",$id,$p_masa_simpan);
					if($app['me']['level'] > 1){
						db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id,$p_masa_simpan);
					}
					
				$add_register 		= $_SESSION['add_register'];
				if(count($add_register)>0)
				{
					foreach($add_register as $row )
					{
						if (!empty($row['id'])) 
						{
							// $idz 		= rand(1, 100).date("dmYHis");
							$idz 		= db::cek_id($row['id'],"data_form_register_device");
							$sqlx 		= "insert into ". $app['table']['data_form_register_device'] ."
							(id, jenis_perangkat, maker, tipe, pic, jabatan, created_by, id_form) values ('$idz', '".$row['jenis_perangkat']."', '".$row['maker']."', '".$row['tipe']."', '".$row['id_pic']."', '".$row["jabatan"]."', '".$app['me']['id']."', '$id')";
							db::qry($sqlx);
						}
					}
					unset($_SESSION['add_register']);
				}
				}

				
			// }
			msg::set_message('success', app::i18n('create'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}else{
			msg::set_message('error', app::i18n('it_not_add'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		// echo $module;
		// echo $id;
		$form = db::get_record($module, "id", $id);
		// $_SESSION["add_register"] = "";
		unset($_SESSION['add_register']);
		// $_SESSION['add_register'][$id] = ['nik'=>$nik, 'name'=>$name, 'id'=>$id, 'id_section'=>$id_section, 'name_section'=>$name_section, 'premission'=>$premission, 'name_section'=>$name_section];
		
		// $_SESSION['add_register'][$id] = ['id_pic'=>$pic, 'jenis_perangkat'=>$jenis_perangkat, 'maker'=>$maker, 'id'=>$id, 'tipe'=>$tipe, 'pic'=>$name_pic, 'jabatan'=>$jabatan];
		
		$rs['add_register'] = db::get_record_select("*, pic id_pic", "data_form_register_device", "id_form=".$id);
		$_SESSION['add_register'] = [];
		while($row = db::fetch($rs['add_register'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			$name_pc	  = db::lookup("name","member", "id", $row['id_pic']);
			// $row['name'] = $user_form['name'];
			$row['pic'] = $name_pc;
			$row['name_section'] = $name_section;
			$_SESSION['add_register'][$row['id']]= $row;
		}
		// print_r($_SESSION['add_register']);
		form::populate($module);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		// if($status_f == "baru"){
		// 	$validate = 'p_name,p_status_f,p_function';
		// }else{
		// 	$validate = 'p_name,p_status_f';
		// }
		form::serialize_form();
		// form::validate('empty', $validate);
		if (form::is_error()):
			msg::build_msg();
			header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$p_images = str_replace("%20"," ","$p_images");
		$p_alias = app::slugify($p_title);
		app::mq_encode('p_title,p_content,p_image,p_lang');
		// $sql 		= "INSERT INTO ".$app['table'][$module]."
		// 		(id, tgl_pengajuan, id_section, id_departement, reorder, created_by, created_at$approved $status_progress) VALUES
		// 		('$id', '$p_tgl_pengajuan', '".$p_id_section."', '$p_id_departement', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";
				
		$sql = "update ". $app['table'][$module] ."
				set tgl_pengajuan = '$p_tgl_pengajuan',
					id_section = '$p_id_section',
					id_departement = '$p_id_departement',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "UPD",$id, $p_masa_simpan);
			$add_register 		= $_SESSION['add_register'];
			if(count($add_register)>0)
			{
				db::qry("DELETE FROM ". $app['table']["data_form_register_device"] ." WHERE `id_form` IN ('". $id ."')");
				foreach($add_register as $row )
				{
					if (!empty($row['id'])) 
					{
						// $idz 		= rand(1, 100).date("dmYHis");
						// $idz 		= db::cek_id($idz,"data_form_register_device");
						// $sqlx = "insert into ". $app['table']['data_form_register_device'] ."
						// (id, id_user, nik, created_by, id_form, premission) values ('$idz', '".$row['id']."', '".$row['nik']."', '".$app['me']['id']."', '$id', '".$row['premission']."')";
						// db::qry($sqlx);
						// $idz 		= rand(1, 100).date("dmYHis");
						$idz 		= db::cek_id($row['id'],"data_form_register_device");
						$sqlx 		= "insert into ". $app['table']['data_form_register_device'] ."
						(id, jenis_perangkat, maker, tipe, pic, jabatan, created_by, id_form) values ('$idz', '".$row['jenis_perangkat']."', '".$row['maker']."', '".$row['tipe']."', '".$row['id_pic']."', '".$row["jabatan"]."', '".$app['me']['id']."', '$id')";
						db::qry($sqlx);
	
					}
				}
				unset($_SESSION['add_register']);
			}
		}
		msg::set_message('success', app::i18n('modify'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	else:
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table'][$module]."
			SET status = '$statusnya'
			WHERE identity = '$identity'";
	db::qry($sql);
	msg::set_message('success', app::i18n('update'));
	header("location: " . $app['webmin'] ."/". $module .".mod");
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	$module = admlib::$page_active['module'];
	if($step == 1)
	{
		if($id) $items = $id;
		else $items = implode("','", $p_del);
		$sql = "SELECT id, name title FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $delid ."')";
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "DEL",$delid, $p_masa_simpan);
			db::qry("DELETE FROM ". $app['table']["data_form_register_device"] ." WHERE `id_form` IN ('". $delid ."')");
		}
		msg::set_message('success', app::i18n('delete'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	$module = admlib::$page_active['module'];
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$p_title 		= $row['title'];
			$p_content	 	= $row['content'];
			$p_image 		= $row['image'];

			$urut = db::lookup("max(reorder)", $module, "1=1");
			if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
			$id = rand(1, 100).$urut.date("dmYHis");

			app::mq_encode('p_title,p_content');
			$sql = "INSERT INTO ". $app['table'][$module] ."
				(id, title, content, status, reorder, created_by, created_at) VALUES
				('$id', '$p_title', '$p_content', 'inactive', '$urut', '". $app['me']['id'] ."', now())";
			db::qry($sql);
		}
		msg::set_message('success', app::i18n('create'));
	}
	else
	{
		msg::set_message('error', app::i18n('notfound'));
	}
	header("location: " . $app['webmin'] ."/". $module .".mod");
	exit;
endif;

/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $count ++;
			$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : approve
*******************************************************************************/
if ($act == "approve"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	// print_r($_REQUEST);
	// if($type=="approve_2"){

		// if($app["me"]["level"] >= 2){
		if($app["me"]["level"] > 1){
			if($reject !="1"){
				if($app["me"]["level"] == 5){
					// $approved .= "approve_2 = 'iya',";
					$approved .= "approve_3 = 'iya',";
					$approved .= "approve_4 = 'iya',";
					$approved .= "approve_5 = 'iya',";
					$status = "fully";
					$log_approve = "APR_DIR";
				}
				if($app["me"]["level"] == 4){
					// $approved .= "approve_2 = 'iya',";
					$approved .= "approve_4 = 'iya',";
					$approved .= "approve_3 = 'iya',";
					$status = "partly";
					$log_approve = "APR_GM";
				}
				if($app["me"]["level"] == 3){
					// $approved .= "approve_2 = 'iya',";
					$approved .= "approve_3 = 'iya',";
					$status = "partly";
					$log_approve = "APR_MNG";
				}
				if($app["me"]["level"] == 2){
					$approved .= "approve_2 = 'iya',";
					// $approved .= "approve_3 = 'iya',";
					$status = "partly";
					$log_approve = "APR_SPV";
				}
				// if($app["me"]["level"] == 2){
				// 	// $approved .= "approve_2 = 'iya',";
				// 	$log_approve = "APR_SPV";
				// 	$status = "partly";
				// }
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, $log_approve,$id, $p_masa_simpan);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				if($app["me"]["level"] >= 3){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				if($app["me"]["level"] == 2){
					$approved .= "approve_2 = 'reject',";
					$approved .= "approve_3 = 'reject',";
					$approved .= "approve_it = 'reject',";
					$approved .= "confirm_done = 'reject',";
					$status = "rejected";
				}
				// $approved = rtrim($approved,",");
				$sql = "update ". $app['table'][$module] ."
				set $approved
				status_progress = '$status'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}
		}else{
			// echo "disini";
				// if($app["me"]["level"] >= 3){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$approved .= "approve_3 = 'tidak',";
				// 	$status = "rejected";
				// }
				// if($app["me"]["level"] == 2){
				// 	$approved .= "approve_2 = 'tidak',";
				// 	$status = "rejected";
				// }
				// // $approved = rtrim($approved,",");
				// $sql = "update ". $app['table'][$module] ."
				// set $approved
				// status_progress = '$status'
				// where id = '$id'";
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "REJ",$id, $p_masa_simpan);
				// }
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	// }

	// if(count($reorder) > 0)
	// {
		// foreach ($reorder as $idval) { $count ++;
		// 	$sql = "UPDATE ". $app['table'][$module] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
		// 	db::qry($sql);
		// }
		// echo true;
		// exit;
	// }
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : finish_it
*******************************************************************************/
// if ($act == "finish_it"):
// 	admlib::validate('UPDT');
// 	$module = admlib::$page_active['module'];
// 	$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
// 		if($app["me"]["code"] == "it"){
// 			// if($app["me"]["level"] >= 3){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// 	$approved .= "approve_3 = 'iya',";
// 			// }
// 			// if($app["me"]["level"] == 2){
// 			// 	$approved .= "approve_2 = 'iya',";
// 			// }
// 			// $approved = rtrim($approved,",");;
// 			if($get_approve_mng=="iya"){
// 				$sql = "update ". $app['table'][$module] ."
// 				set finish_it ='iya',
// 					status_progress= 'progress'
// 				where id = '$id'";
// 				// db::qry($sql);
// 				if(db::qry($sql)){
// 					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
// 				}
// 				msg::set_message('success', app::i18n('approved_msg'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 				exit;
// 			}else{
// 				msg::set_message('error', app::i18n('not_fully'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 			}
// 		}else{
// 			// echo "disini";
// 			msg::set_message('error', app::i18n('not_qualified'));
// 			header("location: " . $app['webmin'] ."/". $module .".mod");
// 		}

// 	echo false;
// 	exit;
// endif;
/*******************************************************************************
* Action : confirm_done
*******************************************************************************/
if ($act == "confirm_done"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_status = db::lookup("status_progress",$module,"id",$id);
	$id_user = db::lookup("id_user",$module,"id",$id);
	// if($id_user==$app['me']['id']){
		if($get_status=="progress"){
			$sql = "update ". $app['table'][$module] ."
			set confirm_done ='iya',
				status_progress= 'progress'
			where id = '$id'";
			// db::qry($sql);
			if(db::qry($sql)){
				db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan);
			}
			msg::set_message('success', app::i18n('approved_msg'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
			exit;
		}else{
			msg::set_message('error', app::i18n('not_finished'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}
	// }else{
	// 	msg::set_message('error', app::i18n('not_finished'));
	// 	header("location: " . $app['webmin'] ."/". $module .".mod");
	// }

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : print
*******************************************************************************/
if ($act == "print"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);
	$departement = db::lookup("name","departement", "id", $form['id_departement']);
	$bagian = db::lookup("name","section", "id", $form['id_section']);
	$revisi				 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='UPD'");
	$approve_spv_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_SPV' order by created_at desc");
	$approve_spv_name	 = db::lookup("name","member", "id", $approve_spv_id);
	$approve_mng_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG' order by created_at desc");
	$approve_mng_name	 = db::lookup("name","member", "id", $approve_mng_id);
	$approve_gm_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_GM' order by created_at desc");
	$approve_gm_name	 = db::lookup("name","member", "id", $approve_gm_id);
	$approve_dir_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_DIR' order by created_at desc");
	$approve_dir_name	 = db::lookup("name","member", "id", $approve_dir_id);
	$approve_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$approve_it_name	 = db::lookup("name","member", "id", $approve_it_id);
	$approve_3_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$approve_3_it_name	 = db::lookup("name","member", "id", $approve_3_it_id);
	$approve_5_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_DIR_IT' order by created_at desc");
	$approve_5_it_name	 = db::lookup("name","member", "id", $approve_5_it_id);


	// $approve_it		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	// $approve_it_name	 = db::lookup("name","member", "id", $approve_it);

	
	$created_name		 = db::lookup("name","member", "id", $form['created_by']);
	$approve_mng_it		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$approve_mng_it_name	 = db::lookup("name","member", "id", $approve_mng_it);

	$created_spv_id		 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$created_done_id	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRD' order by created_at desc");
	$created_revisi	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='UPD' order by created_at desc");
	$created_approve_it = db::lookup("created_at","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$config = db::get_record("configuration", "id", 1);
	// $created = db::lookup("created_at","log", "id", $form['id_departement']);
	////////////////////////////////////////////////////////////////////////////
	$rs['add_register'] = db::get_record_select("*, pic id_pic", "data_form_register_device", "id_form=".$id);
		// $_SESSION['add_register'] = [];
		// while($row = db::fetch($rs['add_register'])){
		// 	$user_form = db::get_record("member", "id", $row['id_user']);
		// 	$name_section = db::lookup("name","section", "id", $user_form['id_section']);
		// 	$row['name'] = $user_form['name'];
		// 	$row['name_section'] = $name_section;
		// 	$_SESSION['add_register'][$row['id']]= $row;
		// }
	include "dsp_print.php";
	exit;
	// $get_status = db::lookup("status_progress",$module,"id",$id);
			// if($get_status=="progress"){
				// $sql = "update ". $app['table'][$module] ."
				// set confirm_done ='iya',
				// 	status_progress= 'progress'
				// where id = '$id'";
				// // db::qry($sql);
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan);
				// }
				// msg::set_message('success', app::i18n('approved_msg'));
				// header("location: " . $app['webmin'] ."/". $module .".mod");
				// exit;
			// }else{
			// 	msg::set_message('error', app::i18n('not_finished'));
			// 	header("location: " . $app['webmin'] ."/". $module .".mod");
			// }

	// echo false;
	// exit;
endif;
/*******************************************************************************
* Action : view
*******************************************************************************/
if ($act == "view"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$form = db::get_record($module, "id", $id);
	$departement = db::lookup("name","departement", "id", $form['id_departement']);
	// $section = db::lookup("name","section", "id", $form['id_section']);
	$bagian = db::lookup("name","section", "id", $form['id_section']);
	$revisi				 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='UPD'");
	$approve_spv_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_SPV' order by created_at desc");
	$approve_spv_name	 = db::lookup("name","member", "id", $approve_spv_id);
	$approve_mng_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG' order by created_at desc");
	$approve_mng_name	 = db::lookup("name","member", "id", $approve_mng_id);
	$approve_gm_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_GM' order by created_at desc");
	$approve_gm_name	 = db::lookup("name","member", "id", $approve_gm_id);
	$approve_dir_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_DIR' order by created_at desc");
	$approve_dir_name	 = db::lookup("name","member", "id", $approve_dir_id);
	$approve_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$approve_it_name	 = db::lookup("name","member", "id", $approve_it_id);
	$approve_3_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$approve_3_it_name	 = db::lookup("name","member", "id", $approve_3_it_id);
	$approve_5_it_id		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_DIR_IT' order by created_at desc");
	$approve_5_it_name	 = db::lookup("name","member", "id", $approve_5_it_id);


	// $approve_it		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	// $approve_it_name	 = db::lookup("name","member", "id", $approve_it);

	
	$approve_mng_it		 = db::lookup("id_user","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$approve_mng_it_name	 = db::lookup("name","member", "id", $approve_mng_it);

	$created_spv_id		 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRIT' order by created_at desc");
	$created_done_id	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='APRD' order by created_at desc");
	$created_revisi	 = db::lookup("created_at","log", "id_form='".$id."' AND act ='UPD' order by created_at desc");
	$created_approve_it = db::lookup("created_at","log", "id_form='".$id."' AND act ='APR_MNG_IT' order by created_at desc");
	$config = db::get_record("configuration", "id", 1);
	// $created = db::lookup("created_at","log", "id", $form['id_departement']);
	////////////////////////////////////////////////////////////////////////////
	$rs['add_register'] = db::get_record_select("*, pic id_pic", "data_form_register_device", "id_form=".$id);
		// $_SESSION['add_register'] = [];
		// while($row = db::fetch($rs['add_register'])){
		// 	$user_form = db::get_record("member", "id", $row['id_user']);
		// 	$name_section = db::lookup("name","section", "id", $user_form['id_section']);
		// 	$row['name'] = $user_form['name'];
		// 	$row['name_section'] = $name_section;
		// 	$_SESSION['add_register'][$row['id']]= $row;
		// }
	include "dsp_view.php";
	exit;
	// $get_status = db::lookup("status_progress",$module,"id",$id);
			// if($get_status=="progress"){
				// $sql = "update ". $app['table'][$module] ."
				// set confirm_done ='iya',
				// 	status_progress= 'progress'
				// where id = '$id'";
				// // db::qry($sql);
				// if(db::qry($sql)){
				// 	db::ins_call_func("insert_log", $app['me']['id'], $module, "APRD",$id, $p_masa_simpan);
				// }
				// msg::set_message('success', app::i18n('approved_msg'));
				// header("location: " . $app['webmin'] ."/". $module .".mod");
				// exit;
			// }else{
			// 	msg::set_message('error', app::i18n('not_finished'));
			// 	header("location: " . $app['webmin'] ."/". $module .".mod");
			// }

	// echo false;
	// exit;
endif;
/*******************************************************************************
* Action : approve_it
*******************************************************************************/
if ($act == "approve_it"):
	// admlib::validate('UPDT');
	// $module = admlib::$page_active['module'];
	// $get_approve_mng = db::lookup("approve_3",$module,"id",$id);
	// 	if($app["me"]["code"] == "it"){
	// 		// if($app["me"]["level"] >= 3){
	// 		// 	$approved .= "approve_2 = 'iya',";
	// 		// 	$approved .= "approve_3 = 'iya',";
	// 		// }
	// 		// if($app["me"]["level"] == 2){
	// 		// 	$approved .= "approve_2 = 'iya',";
	// 		// }
	// 		// $approved = rtrim($approved,",");
	// 		if($get_approve_mng=="iya"){
	// 			$sql = "update ". $app['table'][$module] ."
	// 					set approve_it ='iya',
	// 						status_progress= 'accepted'
	// 						where id = '$id'";
	// 			// db::qry($sql);
	// 			if(db::qry($sql)){
	// 				db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
	// 			}
	// 			msg::set_message('success', app::i18n('approved_msg'));
	// 			header("location: " . $app['webmin'] ."/". $module .".mod");
	// 			exit;
	// 		}else{
	// 			msg::set_message('error', app::i18n('not_fully'));
	// 			header("location: " . $app['webmin'] ."/". $module .".mod");
	// 		}
	// 	}else{
	// 		// echo "disini";
	// 		msg::set_message('error', app::i18n('not_qualified'));
	// 		header("location: " . $app['webmin'] ."/". $module .".mod");
	// 	}

	// echo false;
	// exit;
	admlib::validate('UPDT');
	form::init();
	$module = admlib::$page_active['module'];
	if ($step == 1):
		$form = db::get_record($module, "id", $id);
		form::populate($module);
		unset($_SESSION['add_feedback']);


		// $maker = db::lookup("maker","data_form_register_device","id",$id);
		// $_SESSION['add_feedback'][$id] = ['kode'=>$kode, 'devices'=>$maker, 'enkripsi'=>$enkripsi, 'id'=>$id, 'aktif'=>$aktif];

		
		$rs['add_feedback'] = db::get_record_select("id, kode_perangkat kode, maker devices, enkripsi, aktif ", "data_form_register_device", "id_form=".$id);
		$_SESSION['add_feedback'] = [];
		while($row = db::fetch($rs['add_feedback'])){
			// $maker = db::lookup("maker","data_form_register_device","id",$id);
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_pc	  = db::lookup("name","member", "id", $row['id_pic']);
			// $row['name'] = $user_form['name'];
			// $row['pic'] = $name_pc;
			// $row['name_section'] = $name_section;
			if(!empty($row['kode']) && !empty($row['id']) && !empty($row['devices']) && !empty($row['enkripsi']) && !empty($row['aktif'])){
				$_SESSION['add_feedback'][$row['id']]= $row;
			}
		}
		include "dsp_feedback.php";
		exit;
	endif;
	if ($step == 2):
		// if($status_f == "baru"){
		// 	$validate = 'p_name,p_status_f,p_function';
		// }else{
		// 	$validate = 'p_name,p_status_f';
		// }
		form::serialize_form();
		// form::validate('empty', $validate);
		if (form::is_error()):
			msg::build_msg();
			header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$p_images = str_replace("%20"," ","$p_images");
		$p_alias = app::slugify($p_title);
		app::mq_encode('p_title,p_content,p_image,p_lang');
		// $sql 		= "INSERT INTO ".$app['table'][$module]."
		// 		(id, tgl_pengajuan, id_section, id_departement, reorder, created_by, created_at$approved $status_progress) VALUES
		// 		('$id', '$p_tgl_pengajuan', '".$p_id_section."', '$p_id_departement', '$urut', '". $app['me']['id'] ."', now()$approved_val $status_progress_val)";
				
		// $sql = "update ". $app['table'][$module] ."
		// 		set tgl_pengajuan = '$p_tgl_pengajuan',
		// 			id_section = '$p_id_section',
		// 			id_departement = '$p_id_departement',
		// 			updated_by = '". $app['me']['id'] ."',
		// 			updated_at = now()
		// 		where id = '$id'";
		$sql = "update ". $app['table'][$module] ."
		set approve_it ='iya',
			status_progress= 'accepted'
		where id = '$id'";
// db::qry($sql);
if(db::qry($sql)){
	$revisi				 = db::lookup("count(id)","log", "id_form='".$id."' AND act ='APRIT'");
	if($revisi < 1){
		db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
	}
	db::ins_call_func("insert_log", $app['me']['id'], $module, "UPD_IT",$id, $p_masa_simpan);
	$add_feedback 		= $_SESSION['add_feedback'];
	// print_r($add_feedback);exit;
	if(count($add_feedback)>0)
	{
		// db::qry("DELETE FROM ". $app['table']["data_form_register_device"] ." WHERE `id_form` IN ('". $id ."')");
		foreach($add_feedback as $row )
		{
			$data = "";
			$data = db::get_record("data_form_register_device","id",$row['id']);
			// echo "DELETE FROM ". $app['table']["data_form_register_device"] ." WHERE `id` = '". $row['id'] ."'";exit;
			db::qry("DELETE FROM ". $app['table']["data_form_register_device"] ." WHERE `id` = '". $row['id'] ."'");
			if (!empty($row['id'])) 
			{
				// $idz 		= rand(1, 100).date("dmYHis");
				// $idz 		= db::cek_id($idz,"data_form_register_device");
				// $sqlx = "insert into ". $app['table']['data_form_register_device'] ."
				// (id, id_user, nik, created_by, id_form, premission) values ('$idz', '".$row['id']."', '".$row['nik']."', '".$app['me']['id']."', '$id', '".$row['premission']."')";
				// db::qry($sqlx);
				// $idz 		= rand(1, 100).date("dmYHis");
				$idz 		= db::cek_id($data['id'],"data_form_register_device");
				$id_hapus  .= "'".$data['id']."',";
				$sqlx 		= "insert into ". $app['table']['data_form_register_device'] ."
				(id, jenis_perangkat, maker, tipe, pic, jabatan, created_by, created_at, id_form, kode_perangkat, enkripsi, aktif) values ('$idz', '".$data['jenis_perangkat']."', '".$data['maker']."', '".$data['tipe']."', '".$data['pic']."', '".$data["jabatan"]."', '".$data['created_by']."', '".$data['created_at']."', '$id', '".$row['kode']."', '".$row['enkripsi']."', '".$row['aktif']."')";
				// $sqlx 		= "update ". $app['table']['data_form_register_device'] ."
				// 				set kode_perangkat ='".$row['kode']."',
				// 					enkripsi = '".$row['enkripsi']."', 
				// 					aktif = '".$row['aktif']."'
				// 					where id = '".$row['id']."'";
				db::qry($sqlx);

			}
		}
		$id_hapus = rtrim($id_hapus,",");
		// echo "DELETE FROM ". $app['table']["data_form_register_device"] ." WHERE `id_form` IN ('". $id ."') AND id NOT IN (".$id_hapus.")";exit;
		$sqlx 		= "update ". $app['table']['data_form_register_device'] ."
						set kode_perangkat ='',
				 		enkripsi = '', 
				 		aktif = ''
						 where id NOT IN (".$id_hapus.") AND id_form = '$id' ";
		db::qry($sqlx);
		// db::qry("DELETE FROM ". $app['table']["data_form_register_device"] ." WHERE `id_form` NOT IN ('". $id ."') AND id IN (".$id_hapus.")");
		unset($_SESSION['add_feedback']);
	}
}
		// db::qry($sql);
		if(db::qry($sql)){
			db::ins_call_func("insert_log", $app['me']['id'], $module, "UPD_IT",$id, $p_masa_simpan);
		}
		msg::set_message('success', app::i18n('modify'));
		header("location: " . $app['webmin'] ."/". $module .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : finish_it
*******************************************************************************/
// if ($act == "finish_it"):
// 	admlib::validate('UPDT');
// 	form::init();
// 	$module = admlib::$page_active['module'];
// 	if($app["me"]["code"] == "it"){
// 		$get_approve_mng = db::lookup("approve_3",$module,"id",$id);
// 		if($get_approve_mng=="iya"){
// 			if ($step == 1):
// 				// echo $module;
// 				// echo $id;
// 				$form = db::get_record($module, "id", $id);
// 				form::populate($module);
// 				include "dsp_feedback.php";
// 				exit;
// 			endif;
// 			if ($step == 2):
// 				form::serialize_form();
// 				// form::validate('empty', 'p_name,p_nik,p_id_departement,p_username,p_alasan');
// 				// if (form::is_error()):
// 				// 	msg::build_msg();
// 				// 	header("location: ". $module .".mod&act=edit&error=1&id=" . $id);
// 				// 	exit;
// 				// endif;
// 				$p_images = str_replace("%20"," ","$p_images");
// 				$p_alias = app::slugify($p_title);
// 				app::mq_encode('p_title,p_content,p_image,p_lang');
// 				$sql = "update ". $app['table'][$module] ."
// 						set finish_it ='iya',
// 							status_progress= 'progress',
// 							note= '$p_note'
// 						where id = '$id'";
// 				// db::qry($sql);
// 				if(db::qry($sql)){
// 					db::ins_call_func("insert_log", $app['me']['id'], $module, "APRIT",$id, $p_masa_simpan);
// 				}
// 				msg::set_message('success', app::i18n('approved_msg'));
// 				header("location: " . $app['webmin'] ."/". $module .".mod");
// 				exit;
// 			endif;
// 		}else{
// 			msg::set_message('error', app::i18n('not_fully'));
// 			header("location: " . $app['webmin'] ."/". $module .".mod");
// 		}
// 	}else{
// 		msg::set_message('error', app::i18n('not_qualified'));
// 		header("location: " . $app['webmin'] ."/". $module .".mod");
// 	}
// 	exit;
// endif;
// approve_it_3
/*******************************************************************************
* Action : approve_it_3
*******************************************************************************/
if ($act == "approve_it_3"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_it = db::lookup("approve_it",$module,"id",$id);
	// echo $get_approve_it;
	// exit;
		if($app["me"]["code"] == "it" && $app["me"]["level"] == "3"){
			// if($app["me"]["level"] >= 3){
			// 	$approved .= "approve_2 = 'iya',";
			// 	$approved .= "approve_3 = 'iya',";
			// }
			// if($app["me"]["level"] == 2){
			// 	$approved .= "approve_2 = 'iya',";
			// }
			// $approved = rtrim($approved,",");;
			if($get_approve_it=="iya"){
				$sql = "update ". $app['table'][$module] ."
				set approve_it_3 ='iya',
					status_progress= 'progress'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR_MNG_IT",$id, $p_masa_simpan);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_fully'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : approve_it_5
*******************************************************************************/
if ($act == "approve_it_5"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	$get_approve_it = db::lookup("approve_it",$module,"id",$id);
	// echo $get_approve_it;
	// exit;
		if($app["me"]["code"] == "it" && $app["me"]["level"] == "5"){
			// if($app["me"]["level"] >= 3){
			// 	$approved .= "approve_2 = 'iya',";
			// 	$approved .= "approve_3 = 'iya',";
			// }
			// if($app["me"]["level"] == 2){
			// 	$approved .= "approve_2 = 'iya',";
			// }
			// $approved = rtrim($approved,",");;
			if($get_approve_it=="iya"){
				$sql = "update ". $app['table'][$module] ."
				set approve_it_5 ='iya',
					status_progress= 'progress'
				where id = '$id'";
				// db::qry($sql);
				if(db::qry($sql)){
					db::ins_call_func("insert_log", $app['me']['id'], $module, "APR_DIR_IT",$id, $p_masa_simpan);
				}
				msg::set_message('success', app::i18n('approved_msg'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
				exit;
			}else{
				msg::set_message('error', app::i18n('not_fully'));
				header("location: " . $app['webmin'] ."/". $module .".mod");
			}
		}else{
			// echo "disini";
			msg::set_message('error', app::i18n('not_qualified'));
			header("location: " . $app['webmin'] ."/". $module .".mod");
		}

	echo false;
	exit;
endif;
/*******************************************************************************
* Action : get-devices;
*******************************************************************************/
if($act == "get-devices"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$device = "SELECT * FROM ". $app['table']['data_form_register_device'] . " WHERE id IN ('". $id."') AND status='active' limit 1";
	db::query($device, $rs['device'], $nr['device']);
	if($nr['device'] > 0){
		while($row = db::fetch($rs['device'])){
			$row['pic_name'] = db::lookup("name","member","id",$row['pic']);
			$callback['device'] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	// $callback["sql"] = $device;
	echo json_encode($callback);
	exit;
endif;
/*******************************************************************************
* Action : get-member;
*******************************************************************************/
if($act == "get-member"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$member = "SELECT id,id_section,nik,name,id_title FROM ". $app['table']['member'] . " WHERE id IN ('". $id."') AND status='active' limit 1";
	db::query($member, $rs['member'], $nr['member']);
	if($nr['member'] > 0){
		while($row = db::fetch($rs['member'])){
			$row['jabatan'] = db::lookup("name","title","id",$row['id_title']);
			$callback['member'] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	// $callback["sql"] = $member;
	echo json_encode($callback);
	exit;
endif;
if($act == "add_register")
{	
	// admlib::validate('UPDT');
	if($step == "add")
	{
		// $req_dump = print_r($_REQUEST, TRUE);
		// $fp = fopen('request.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		$name_pic = db::lookup("name","member","id",$pic);
		$name_section = db::lookup("name","section","id",$id_section);
		$_SESSION['add_register'][$id] = ['id_pic'=>$pic, 'jenis_perangkat'=>$jenis_perangkat, 'maker'=>$maker, 'id'=>$id, 'tipe'=>$tipe, 'pic'=>$name_pic, 'jabatan'=>$jabatan];
		exit;
	}elseif($step == "delete"){

 		// $sql ="DELETE FROM  ".$app['table']['status']." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')  ";
		// db::qry($sql);

		// $sqls 		= "DELETE FROM ". $app['table']['container'] ." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		// $sqls321 	= "DELETE FROM ". $app['table']['trucking'] ." WHERE `id_import` IN ('". $_SESSION['container'][$indx]["im"]."') AND `id_container` IN ('". $_SESSION['container'][$indx]["pk"]."')";
	
		// $sqlss321 	= "DELETE FROM ". $app['table']['return_container'] ." WHERE `id_container` IN ('". $_SESSION['container'][$indx]["pk"]."')";
		// db::qry($sqls);
		// db::qry($sqls321);
		// db::qry($sqlss321);


		// $req_dump = print_r($_SESSION['add_register'], TRUE);
		// $fp = fopen('request.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		unset($_SESSION['add_register'][$id]);
		// unset($_SESSION['add_register']);
		exit;
	}
	include "dsp_list.php";
}
//////////////////////////////////////
//////////////////////////////////////

if($act == "add_feedback")
{	
	admlib::validate('UPDT');
	if($step == "add")
	{
		// $req_dump = print_r($_REQUEST, TRUE);
		// $fp = fopen('request.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		// $name_pic = db::lookup("name","member","id",$pic);
		// $name_section = db::lookup("name","section","id",$id_section);
		$maker = db::lookup("maker","data_form_register_device","id",$id);
		$_SESSION['add_feedback'][$id] = ['kode'=>$kode, 'devices'=>$maker, 'enkripsi'=>$enkripsi, 'id'=>$id, 'aktif'=>$aktif];
		exit;
	}elseif($step == "delete"){
		// $req_dump = print_r($_SESSION['add_feedback'], TRUE);
		// $fp = fopen('request.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		unset($_SESSION['add_feedback'][$id]);
		exit;
	}
	include "dsp_list_feedback.php";
}
/*******************************************************************************
* Action : get-section;
*******************************************************************************/
if($act == "get-section"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$section = "SELECT id,name as name FROM ". $app['table']['section'] . " WHERE id_departement IN ('". $id_section."') AND status='active'";
	db::query($section, $rs['section'], $nr['section']);
	if($nr['section'] > 0){
		while($row = db::fetch($rs['section'])){
			$callback['section'][] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	echo json_encode($callback);
	exit;
endif;

?>
