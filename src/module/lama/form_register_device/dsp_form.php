<?php
admlib::display_block_header();
?>

<style>
		.monthselect{
			color: black;
		}
		.yearselect{
			color: black;
		}
	</style> <?php
	// admlib::get_component('texteditorlib');
	admlib::get_component('datepickerlib');
	admlib::get_component('select2lib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"status_f",
		// 		"datas"=>["baru","ada"],
		// 		"value"=>app::ov($module['status_f'])
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"name_f",
		// 		"value"=>app::ov($module['name_f']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"type"=>"hidden",
		// 		"name"=>"name",
		// 		"value"=>app::ov($module['name']),
		// 	)
		// );
		admlib::get_component('datepicker',
			array(
				"name"=>"tgl_pengajuan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['tgl_pengajuan'])
			)
		);
		$rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_departement",
				"value"=>app::ov($form['id_departement']),
				"items"=>$rs['departement']
			)
		);
		// $rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section",
				"value"=>app::ov($form['id_section']),
				"items"=>$rs['section']
			)
		);
		 ?>
		<div class="form-group">
		<label class="control-label col-md-1 col-sm-12 col-xs-12" for="tgl_countainer">&nbsp;</label>
		<!-- <div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="num_container">Container<span class="required"></span>		</label>
			<input type="text" name="p_num_container" id="num_container" value="" class="form-control has-feedback-left " validate="">
			<span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span>
		</div> -->
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="jenis_perangkat">Jenis Perangkat<span class="required"></span>		</label>
			<input type="text" name="p_jenis_perangkat" id="jenis_perangkat" value="" class="form-control " validate="">


			
			<input style="display:none" type="hidden" name="p_id_m" id="id_m" value="" class="form-control " validate="">
			<input style="display:none" type="hidden" name="p_nik_m" id="nik_m" value="" class="form-control " validate="">
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="maker">Maker<span class="required"></span>		</label>
			<input type="text" name="p_maker" id="maker" value="" class="form-control " validate="">
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="tipe">Tipe<span class="required"></span>		</label>
			<input type="text" name="p_tipe" id="tipe" value="" class="form-control " validate="">
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="pic">PIC<span class="required"></span>		</label>
			<select id="pic" name="p_pic" class="select2 form-control">
				<option value="" disabled selected><?php echo app::i18n('select');  ?></option>
				<?php 	
				$sql = "SELECT id,nik,name,id_section FROM ". $app['table']["member"] ."  ORDER BY nik asc	";
				db::query($sql, $rs['row'], $nr['row']);
				while($row = db::fetch($rs['row'])){ ?>
						<option value="<?= $row['id']; ?>"><?= $row['nik']; ?> - <?= $row['name']; ?></option>
				<?php } ?>
		</select>
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="jabatan">Jabatan<span class="required"></span>		</label>
			<input type="text" name="p_jabatan" id="jabatan" value="" class="form-control " validate="">
		</div>

		

		<div style="margin-top:28px;">
			<!-- <button class="btn btn-success" onclick="add_register()">add</button> -->
			<a href="#" class="btn btn-success" onclick="add_register()">add</a>
		</div>
		<br>
				<div id="user_total"></div>
		
<?php 

		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
	?>
	
<script>


function get_section(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						// console.log(result); 
						// if(dis){
						// 	loop = "<option disabled><?php echo app::i18n('select');?></option>";
						// }else{
							loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						// }
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						// alert(loop);
						$("#id_section").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_section").html('');
						$("#id_title").html('');
					}
			}
		});     
	}
$(document).ready(function () {
	$('#id_departement').change(function(){
			// alert("disana");
			$('#id_section').select("val","null");
			// $('#id_title').select("val","null");
			var id_section = $(this).val();
			var dis = '';
			// console.log(dis);
			var urlsection = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
			// alert(urlsection);
			// var urlmaker = "<?php //echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_departement;
			// console.log("ID section: " + urlsection);
			// console.log("ID Maker: " + urlmaker);
			get_section(id_section, dis, urlsection);
			// get_makermodel(id_section, dis, urlmaker);
		});
	$('#id_departement').each(function(){
		// alert("disini");
			var id_section = $(this).val();
			var dis = '<?= $form['id_section'] ?>';
			// alert(dis);
			// console.log(dis);
			var urlcustomer = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
			// alert(urlcustomer);
			// var urlmaker = "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_customer;
			// console.log("ID " + urlcustomer);
		get_section(id_section, dis, urlcustomer);
			// get_makermodel(id_customer, dis, urlmaker);
		});
		$('input[type=radio][name=p_status_f]').change(function() {
			if (this.value == 'baru') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_function").show();
			}
			else if (this.value == 'ada') {
				// alert("Transfer Thai Gayo");
              	$("#g_function").hide();
			}
		});
		$('#pic').change(function() {
			// alert("<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value);
			$.ajax({
			type:"GET",
			url:"<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value,
				success:function(res){         
						var result = JSON.parse(res);
						if(result.error == 0){
							// var loop = "";
							// console.log(result);12 
							// "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section
							console.log(JSON.stringify(result));
							// alert(JSON.stringify(result));
							// alert(result.member.id_section);
							$("#jabatan").val(result.member.jabatan);
							// $("#jabatan").val(result.member.id);
							$("#nik_m").val(result.member.nik);
							$("#id_m").val(result.member.id);
							// $("[name=p_id_section]").val(result.member.id_section);
							// $('select[name="p_id_section"]').find('option:contains("'+result.member.id_section+'")').attr("selected",true);


							// loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
							// $.each(result.section,function(key,value){
							// 	loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
							// });
							// $("#id_title").html(loop);
							// console.log("loop " + loop);
						}else{
							alert("error");
							// $("#id_title").html('');
						}
				}
			}); 
		});
});

		function addData(act, param){
			// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>');
			// alert(param);
			var _param = $.extend({ act : act, step : 	'add' }, param);
			// alert('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			// console.log('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			$.post('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){ loadData() });
			
			// $.ajax({
			// type:"POST",
			// url:"<?php echo admlib::$page_active['module'] ?>.mod",
			// data:_param,
			// 	success:function(res){ 
			// 		loadData();
			// 	}
			// }); 
			return false;
		}
		function add_register(){
			var act = "add_register", jenis_perangkat = $('#jenis_perangkat').val(), maker = $('#maker').val(), tipe = $('#tipe').val(), pic = $('#pic').val(), jabatan = $('#jabatan').val();
			
			// alert(jenis_perangkat);
			// alert(maker);
			// alert(tipe);
			// alert(pic);
			// alert(jabatan);

			if(!act || !jenis_perangkat || !maker || !tipe || !pic || !jabatan){
				// if (!pib || !cont) 
				// {
					alert('tidah boleh kosong');
					return;
				// }
			}
			addData(act, { id: Math.floor(Math.random() * 100000000000000000), jenis_perangkat : jenis_perangkat, maker : maker, tipe : tipe, pic : pic , jabatan : jabatan });
			return false;
		
    };
	function loadData(){
		$('#user_total').load('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : "add_register" });
	};
	loadData();
	function delete_member(id_member){
		var r = confirm("Apakah anda yang ingin menghapus ?");
		// alert(id_member);
		// var _param = $.extend({ act : "add_register", step : 'delete', id : id_member }, param);
		var _param = { act : "add_register", step : 'delete', id : id_member };
		// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>'+_param);
		if (r == true) {
			$.post('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){  loadData(); });
		}
		return false;

	}
</script>
	<?php
admlib::display_block_footer();
?>