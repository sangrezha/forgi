<html><head>
	<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
	</style>
	<style>
		
		table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
		}
	</style>
	<title></title>
</head>
<!-- <body onload="doPrint()"> -->
<body onload="">
	<table width="900" border="0" align="center" style="border: 0px;">
	    <tbody>
			<tr>
	        <!-- <td style="display: flex; align-items: center;border: 1px solid black;">	 -->
	        <td style="display: flex; align-items: center;border: 0px;">	        	
<!-- 	            <img src="logo.png" alt="" style="width: 200px; height: 150px; display: block; text-align: left;"> -->
	            <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 200px; height: auto; display: block; text-align: left;border-right: 1px solid black;">
				<span style="display: block;font-size: 16px;font-weight: bold;margin-left: 20px;letter-spacing: 5px"><p>REGISTRASI PERANGKAT PENYIMPANAN DATA</p></span>
	        </td>
	    </tr>
		<tr>
			<td style="border: 0px;">
				<div style="display: flex;border: 0px;">
					<div style="display: flex;width: 25%;font-size: 10px;">
						PT. CENTRAL MOTOR WHEEL INDONESIA
					</div>
					<div style="width: 33%;">Tanggal Pengajuan : <span><?= $form['tgl_pengajuan'] ?></span></div>	
					<div style="width: 35%;">Departemen : <span></span><?= $departement ?></div>	
				</div>
				<!-- <div style="margin-left: 65%;display: flex;"> -->
				<div style="margin-left: 62%;">
					Bagian : <span><?= $bagian ?></span>
				</div>
			</td>
		</tr>
	</tbody></table>
	<!-- <div style="border-bottom: 1px solid black;width: 172px;height: 19px;margin-left: 450px;text-align: center;position: relative;bottom: 20px;">
	   Revisi <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
	</div> --><div style="width: 135px;height: 6px;margin-left: 1017px;text-align: center;position: relative;bottom: 88px;font-size: 10px;">
	<table style="font-size: 10px;" >
		<tbody><tr>
			<td>Tgl terbit</td>
			<td><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
		</tr>
		<tr>
			<td>Revisi</td>
			<td><?= $revisi ?></td>
		</tr>
		<tr>
			<td>Tgl revisi</td>
			<td><?= ((empty($created_revisi))?"":date('d/m/Y', strtotime($created_revisi))) ?></td>
		</tr>
		<tr>
			<td>Tgl berlaku</td>
			<td><?= ((empty($created_approve_it))?"":date('d/m/Y', strtotime($created_approve_it))) ?></td>
		</tr>
		<tr>
			<td>Masa simpan</td>
			<td><?= $p_masa_simpan ?> tahun</td>
		</tr>
	</tbody></table>
		</div>
<table style="margin-left: 224px;">

	<tr>
		<td colspan="6" style="padding: 10px;text-align: center;">DIISI OLEH USER</td>
		<td colspan="3" style="padding: 10px;text-align: center;">DIISI OLEH IT</td>
	</tr>
	<tr>
		<td style="padding: 10px;text-align: center;">No</td>
		<td style="padding: 10px;text-align: center;width: 216px;">Jenis Perangkat</td>
		<td style="padding: 10px;text-align: center;width: 60px;">Maker</td>
		<td style="padding: 10px;text-align: center;width: 60px;">Tipe</td>
		<td style="padding: 10px;text-align: center;width: 60px;">PIC</td>
		<td style="padding: 10px;text-align: center;width: 60px;">Jabatan</td>
		<td style="padding: 10px;text-align: center;">Kode Perangkat</td>
		<td style="padding: 10px;text-align: center;">Enkripsi</td>
		<td style="padding: 10px;text-align: center;">Aktif s/d</td>
	</tr>
	<?php 
		// $_SESSION['add_member'] = [];
		$no = 1;
		while($row = db::fetch($rs['add_register'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_section = db::lookup("name","section", "id", $row['id_section']);
			// $user_form = db::get_record("member", "id", $row['id_user']);
			$name_pc	  = db::lookup("name","member", "id", $row['id_pic']);
			// $row['name'] = $user_form['name'];
			// $row['pic'] = $name_pc;
			// $row['name_section'] = $name_section;
			?>
	<tr>
		<td style="padding: 10px;text-align: center;"><?= $no ?></td>

		<td style="padding: 10px;text-align: center;"><?= $row['jenis_perangkat'] ?></td>
		<td style="padding: 10px;text-align: center;"><?= $row['maker'] ?></td>
		<td style="padding: 10px;text-align: center;"><?= $row['tipe'] ?></td>
		<td style="padding: 10px;text-align: center;"><?= $name_pc ?></td>
		<td style="padding: 10px;text-align: center;"><?= $row['jabatan'] ?></td>
		<td style="padding: 10px;text-align: center;"><?= $row['kode_perangkat'] ?></td>
		<td style="padding: 10px;text-align: center;"><?= $row['enkripsi'] ?></td>
		<td style="padding: 10px;text-align: center;"><?= $row['aktif'] ?></td>
	</tr>
	 <?php $no++; } ?>
</table>
	<div style="width: 900px;margin-left: 224px;display: flex;">
		<div style="width: 40%;">
			<!-- <div style="border: 1px solid black;width: 90%;height: 80%;border-radius: 22%;margin-top: 37px;"> -->
			<div style="border: 1px solid black;width: 95%;height: 76%;border-radius: 22%;margin-top: 25px;font-size: 11px;">
			<div style="border: 1px solid black;height: 20px;position: relative;width: 100px;bottom: 7px;left: 15px;background: white;text-align: center;">Keterangan</div>
			<p style="padding: 10px;position: relative;bottom: 23px;">1. Perangkat yang tidak di daftarkan, Tidak bisa digunakan di area CMWI termasuk perangkat dari pihak luar ( Goverment, Supplier, Trainer, dll )<br>
			2. Jika Perangkat yang sudah didaftarkan akan digunakan di luar area CMWI, harus di Unregister terlebih dahulu dengan menggunakan Form ini (Mobile phone tidak termasuk)<br>3. Enkripsi data adalah untuk memproteksi data yang tercopy ke perangkat penyimpanan agar tidak bisa dibaca oleh pihak luar yang tidak berkepentingan</p>
			</div>
			<!-- </div> -->
		</div>
		<div style="width: 60%;">
			<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
			<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
			<p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p>
			<table border="1" style="font-size: 12px;">
				<tbody>
				<tr style="text-align: center;"> 	
					<td>User</td>
					<td>Supervisor</td>
					<td>Manager</td>
					<td>GM</td>
					<td>Direktur/VP</td>
					<td>Staff</td>
					<td>Manager</td>
					<td>Direktur/VP</td>
				</tr>
				<tr style="text-align:center;">
					<td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
					<td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
					<td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
					<td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
					<td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
					<td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
					<td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
					<td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
				</tr>
				<tr style="text-align: center;">
					<td><?= $created_name ?></td>
					<td><?= $approve_spv_name ?></td>
					<td><?= $approve_mng_name ?></td>
					<td><?= $approve_gm_name ?></td>
					<td><?= $approve_dir_name ?></td>
					<td><?= $approve_it_name ?></td>
					<td><?= $approve_3_it_name ?></td>
					<td><?= $approve_5_it_name ?></td>
				</tr>
			</tbody></table>
		</div>
	</div>
<script>
	function doPrint() {
	    window.print();            
	    document.location.href = "trucking.mod"; 
	}
</script>
<div state="voice" class="placeholder-icon" id="tts-placeholder-icon" title="Click to show TTS button" style="background-image: url(&quot;moz-extension://d957ec0f-edb7-42ef-a0a8-6f64e9a2a6ec/data/content_script/icons/voice.png&quot;); display: none;"><canvas width="36" height="36" class="loading-circle" id="text-to-speech-loader" style="display: none;"></canvas></div></body></html>