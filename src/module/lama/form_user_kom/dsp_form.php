<?php
admlib::display_block_header();
	admlib::get_component('texteditorlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		admlib::get_component('radio',
			array(
				"name"=>"u_status",
				"value"=>app::ov($form['u_status']),
				"datas"=>["u_baru","u_mutasi","u_resign"]
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"nik",
				"value"=>app::ov($form['nik']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"dept",
				"value"=>app::ov($form['dept']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"dept_mut",
				"value"=>app::ov($form['dept_mut']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"deskripsi",
				"value"=>app::ov($form['deskripsi']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"nik",
				"value"=>app::ov($form['nik']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"status",
				"value"=>app::ov($form['status']),
			)
		);
		?>
		<hr>
		<?php
		admlib::get_component('blank',
			array(
				"name"=>"karyawan",
				"value"=>app::ov($form['karyawan'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"start",
				"value"=>app::ov($form['start']),
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"end",
				"value"=>app::ov($form['end']),
			)
		);
		?>
		<hr>
		<?php
		admlib::get_component('inputupload',
			array(
				"name"=>"attachment",
				"value"=>app::ov($form['attachment']),
				"filemedia"=>true
			)
		);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"nik",
		// 		"value"=>app::ov($form['nik']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"want_cond",
		// 		"value"=>app::ov($form['want_cond']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"goal",
		// 		"value"=>app::ov($form['goal']),
		// 	)
		// );
		// admlib::get_component('inputupload',
		// 	array(
		// 		"name"=>"attachment",
		// 		"value"=>app::ov($form['attachment']),
		// 		"filemedia"=>true
		// 	)
		// );
		// // $form['sys_need'] = "laptop";
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"hasil",
		// 		"datas"=>["done","revisi"],
		// 		"value"=>app::ov($form['hasil'])
		// 	)
		// );
		?>
		<hr><hr>
		<?php
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"user",
		// 		"value"=>app::ov($form['user']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"password",
		// 		"value"=>app::ov($form['password']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"ipaddress",
		// 		"value"=>app::ov($form['ipaddress']),
		// 	)
		// );
		// admlib::get_component('textarea',
		// 	array(
		// 		"editor"=>true,
		// 		"name"=>"content",
		// 		"value"=>app::ov($form['content']),
		// 	)
		// );
		if(isset($id))
		{
			admlib::get_component('language',
				array(
					"name"=>"lang",
					"value"=>app::ov($form['lang'])
				)
			);
		}
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
