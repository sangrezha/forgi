<html><head>
	<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
	</style>
	<title></title>
</head>
<!-- <body onload="doPrint()"> -->
<body onload="">
	<table width="900" border="0" align="center">
	    <tbody><tr>
	        <td style="display: flex; align-items: center;border: 1px solid black;">	        	
<!-- 	            <img src="logo.png" alt="" style="width: 200px; height: 150px; display: block; text-align: left;"> -->
	            <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 200px; height: auto; display: block; text-align: left;border-right: 1px solid black;">
	            <span style="display: block;font-size: 20px;font-weight: bold;margin-left: 100px;letter-spacing: 5px"><p>FORM PERMINTAAN AKSES FOLDER</p></span>
	        </td>
	    </tr>
	</tbody></table>
	<div style="border: 1px solid black;width: 172px;height: 30px;margin-left: 730px;text-align: center;">
	   Revisi <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
	</div>
	<?php 
	$items = [];
	if(!is_array($param['datas']))
	{
		while($row=db::fetch($param['datas']))
		{
			$items[] = $row;
		}
	}
	else
	{
		$items = $param['datas'];
	}
	// print_r($items);
	?>
	<div class="form-group" style="width: 63.5%;margin-left: 225px;height:auto;padding-bottom: 20px;">
		
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cost_to"><?php echo app::getliblang('cost_to'); ?>:
			</label>
		
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?php foreach($items as $row){
					$pos = strpos($form['cost_to'], $row['id']);
					// print_r($pos);
					// echo "";
					?>		
			<!-- <div class="checkbox"> -->
				<label>
					<input class="" id="<?php echo $row['id'] ?>" name="p_<?php echo $param['name'] ?>[]" type="checkbox" value="<?php echo $row['id'] ?>" <?php  echo /* ($datas['value'] == $row) */($pos === false)? null:"checked" ?> <?php echo $datas['validate'] ?>  > <?php echo $row['name']; //echo " ".$param['datas'][$i]; ?>
				</label>
			<!-- </div> -->
			<?php } ?>
		</div>
	</div>
	<div style="height: 273px;">
		<table border="1" style="margin-left: 216px;">
		<tr>
			<td colspan="6">
				<input type="radio" id="hardware" name="device" value="hardware" <?= ($categorynya=="hardware"?"checked":"") ?>>
				Hardware
			</td>
		</tr>
			<tr>
				<td style="padding: 10px;text-align: center;">No</td>
					<td style="padding: 10px;text-align: center;">Deskripsi</td>
					<td style="padding: 10px;text-align: center;">Jumlah</td>
					<td style="padding: 10px;text-align: center;">Satuan</td>
					<td style="padding: 10px;text-align: center;">Tujuan</td>
			</tr>
			<?php 
				// $_SESSION['add_member'] = [];
				$no = 1;
				if($categorynya == "hardware"){
				while($row = db::fetch($rs['add_devices'])){
					// $user_form = db::get_record("member", "id", $row['id_user']);
					// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
					?>
			<tr>
				<td style="padding: 10px;text-align: center;"><?= $no ?></td>
				<?php if($categorynya == "hardware"){ ?>
				<td style="padding: 10px;text-align: center;"><?= $row['desk'] ?></td>
				<td style="padding: 10px;text-align: center;"><?= $row['jumlah'] ?></td>
				<td style="padding: 10px;text-align: center;"><?= $row['satuan'] ?></td>
				<?php } ?>
				<td style="padding: 10px;text-align: center;"><?= $row['tujuan'] ?></td>
			</tr>
			<?php $no++; } }else{
				while($no <= 4){ ?>

		<tr>
				<td style="padding: 10px;text-align: center;"><?= $no ?></td>
				<td style="padding: 10px;text-align: center;"></td>
				<td style="padding: 10px;text-align: center;"></td>
				<td style="padding: 10px;text-align: center;"></td>
				<td style="padding: 10px;text-align: center;"></td>
			</tr>
			<?php $no++; } } ?>
		</table>
		<div style="border: 1px solid black;width: 400px;height: 200px;position: absolute;left: 700px;bottom: 185px;">
			<div style="border: 1px solid black;height: 20px;width: 63px;position: relative;bottom: 31px;padding: 5px;right: 1px;">
				Catatan :
			</div>
			<div style="padding: 12px;position: relative;bottom: 33px;">
				<p>1.Hardware dapat berupa Komputer, Printer, Flashdisk dan sejenisnya.</p>
				<p>2.Software dapat berupa Aplikasi gratis maupun berlisensi.</p>
				<p>3.Bila user memiliki refrensi silahkan dilampirkan untuk kebutuhan pembelian barang.</p>
			</div>
		</div>
	</div>
<div style="height: 130px;">
	<table border="1" style="margin-left: 216px;">
	<tr>
		<td colspan="6">
			<input type="radio" id="software" name="device" value="software" <?= ($categorynya=="software"?"checked":"") ?>>
			Softaware
		</td>
	</tr>
		<tr>
			<td style="padding: 10px;text-align: center;">No</td>
				<td style="padding: 10px;text-align: center;">Name Software</td>
				<!-- <td style="padding: 10px;text-align: center;">Tujuan</td> -->
				<td style="padding: 10px;text-align: center;">Tujuan</td>
		</tr>
		<?php 
			// $_SESSION['add_member'] = [];
			$no = 1;
			if($categorynya == "software"){
			while($row = db::fetch($rs['add_devices'])){
				// $user_form = db::get_record("member", "id", $row['id_user']);
				// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
				?>
		<tr>
			<td style="padding: 10px;text-align: center;"><?= $no ?></td>
			<?php // if($categorynya == "hardware"){ ?>
			<!-- <td style="padding: 10px;text-align: center;"><?= $row['desk'] ?></td>
			<td style="padding: 10px;text-align: center;"><?= $row['jumlah'] ?></td>
			<td style="padding: 10px;text-align: center;"><?= $row['satuan'] ?></td> -->
			<?php // }else{ ?>
				<td style="padding: 10px;text-align: center;"><?= $row['name_soft'] ?></td>
			<?php // } ?>
			<td style="padding: 10px;text-align: center;"><?= $row['tujuan'] ?></td>
		</tr>
		<?php $no++; } } else{
			while($no <= 4){ ?>

	<tr>
			<td style="padding: 10px;text-align: center;"><?= $no ?></td>
			<td style="padding: 10px;text-align: center;"></td>
			<td style="padding: 10px;text-align: center;"></td>
		</tr>
		<?php $no++; } } ?>
	</table>
	<div style="border: 1px solid black;width: 500px;height: 150px;position: absolute;left: 600px;/*! bottom: 0px; */top: 510px;">
		<div style="border: 1px solid black;height: 20px;width: 63px;position: relative;bottom: 31px;padding: 5px;right: 1px;">
			Catatan :
		</div>
		<div style="padding: 12px;position: relative;bottom: 48px;">
			<p>1.Sehubungan dengan Undang-Undang tentang Hak Cipta, maka IT tidak melayani permintaan produk ilegal.</p>
			<p>2.Software khusus:</p>			
			<ul style="position: relative;bottom: 15px;">
				<li>Benar - benar dipakai untuk pekerjaan sehari-hari</li>
				<li>User minimal sudah mempunyai background pengetahuan tentang software yang diminta dan mampu mengoprasikannya</li>
			</ul>
		</div>
	</div>
</div>
	<div style="width: 900px;margin-left: 600px;margin-top: 90px;">
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
<p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p>
  <table border="1">
    <tbody><tr style="text-align: center;">
      <td>Staff IT</td>
      <td>Manager IT</td>
      <td style="border:0px;"> </td>
      <td>Manager</td>
      <td>Supervisor</td>
    </tr>
    <tr style="text-align:center;">
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="border:0px;height: 80px;width: 100px;"><======</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
    </tr>
    <tr style="text-align: center;">
	  <td><?= $approve_it_name ?></td>
	  <td><?= $approve_mng_it_name ?></td>
	  <td style="border:0px;"></td>
      <td><?= $approve_mng_name ?></td>
      <td><?= $approve_spv_name ?></td>
    </tr>
  </tbody></table>
  
</div>
<script>
	function doPrint() {
	    window.print();            
	    document.location.href = "trucking.mod"; 
	}
</script>
<div state="voice" class="placeholder-icon" id="tts-placeholder-icon" title="Click to show TTS button" style="background-image: url(&quot;moz-extension://d957ec0f-edb7-42ef-a0a8-6f64e9a2a6ec/data/content_script/icons/voice.png&quot;); display: none;"><canvas width="36" height="36" class="loading-circle" id="text-to-speech-loader" style="display: none;"></canvas></div></body></html>