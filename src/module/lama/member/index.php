<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'member','caption'=>'Member');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$option = ['sortable'=>true];
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'member', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	// $sql = "SELECT a.id, a.status, a.name AS name, d.name title, e.name section, b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
	// FROM ". $app['table']['member'] ." a 	
	// 		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
	// 		LEFT JOIN ". $app['table']['user'] ." c ON (a.created_by=b.id) 
	// 		LEFT JOIN ". $app['table']['title'] ." d ON (a.id_title=d.id) 
	// 		LEFT JOIN ". $app['table']['section'] ." e ON (a.id_section=e.id) $q ORDER BY a.reorder ASC";
	$sql = "SELECT a.id, a.status, a.name AS name, d.name title, e.name section, b.name as created_by,a.created_at,b.name as updated_by,a.updated_at
	FROM ". $app['table']['member'] ." a 	
			LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id) 
			LEFT JOIN ". $app['table']['title'] ." d ON (a.id_title=d.id) 
			LEFT JOIN ". $app['table']['section'] ." e ON (a.id_section=e.id) $q ORDER BY a.reorder ASC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['cp','name',"title","section"];
	while($row = db::fetch($rs['row']))
	{
		$row["cp"] 		= '<center><a href="'.admlib::getext().'&act=change&id='.$row['id'].'"><i class="fa fa-eye" aria-hidden="true"></i></a></center>';
		$results[] 		= $row;
	}
	// $sortable = true;
	// $option = ['sortable'=>true];
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		$validate ='p_name,p_username,p_nik,p_email,p_id_title,p_id_section,p_id_departement,p_password';
		form::serialize_form();
		form::validate('empty',$validate);
		if ($p_password != $p_repassword):
			msg::set_message('notmatch', app::getliblang('password_not_match'));
			form::set_error(1);
		endif;
		$passwordhash = md5(serialize($p_password));
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		// $id = rand(1, 100).date("dmYHis");
		$id = rand(1, 100).date("dmYHis");
		$id = db::cek_id($id,'member');
		app::mq_encode('p_name,p_code,p_photo');
		$reorder = db::lookup("max(reorder)","member","1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		$sql = "insert into ".$app['table']['member']."
				(id, id_title, username, photo, nik, id_departement, id_section, name, email, telp, reorder, created_by, created_at, password) values
				('$id', '$p_id_title', '$p_username', '$p_photo', '$p_nik', '$p_id_departement', '$p_id_section', '$p_name', '$p_email', '$p_telp', '$reorder', '". $app['me']['id'] ."', now(), '$passwordhash')";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record_filter("*","member", "id", $id);
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		form::validate('empty','p_name,p_username,p_nik,p_id_title,p_id_section,p_id_departement,p_email');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		app::mq_encode('p_name,id_title');
		$sql = "update ". $app['table']['member'] ."
				set id_title = '$p_id_title',
					username = '$p_username',
					nik = '$p_nik',
					id_section = '$p_id_section',
					id_departement = '$p_id_departement',
					photo = '$p_photo',
					name = '$p_name',
					email = '$p_email',
					telp  = '$p_telp',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : change password
*******************************************************************************/
if ($act == "change"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record_filter("*","member", "id", $id);
		form::populate($form);
		include "dsp_change.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		form::validate('empty','p_password');
		// $row = db::get_record('user', 'username', $p_username);
		// if ($row['username']):
		// // echo "asdasdasd";exit;
		// 	msg::set_message('uname_exists',app::getliblang('username_exists'));
		// 	form::set_error(1);
		// endif;
		if ($p_password != $p_repassword):
			msg::set_message('notmatch', app::getliblang('password_not_match'));
			form::set_error(1);
		endif;
		// if( count($p_rule) == 0 )
		// {
		// 	msg::set_message('rule_empty', app::getliblang('rule_empty'));
		// 	form::set_error(1);
		// }
		$passwordhash = md5(serialize($p_password));
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=change&error=1&id=" . $id);
			exit;
		endif;
		app::mq_encode('p_password');
		$sql = "update ". $app['table']['member'] ."
				set password ='$passwordhash'
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('change_pass'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['member']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table']['member']." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT id,name as title FROM ". $app['table']['member'] ." WHERE id IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql 	= "DELETE FROM ". $app['table']['member'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;
/*******************************************************************************
* Action : get-section;
*******************************************************************************/
if($act == "get-section"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$section = "SELECT id,name as name FROM ". $app['table']['section'] . " WHERE id_departement IN ('". $id_section."') AND status='active'";
	db::query($section, $rs['section'], $nr['section']);
	if($nr['section'] > 0){
		while($row = db::fetch($rs['section'])){
			$callback['section'][] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	echo json_encode($callback);
	exit;
endif;
/*******************************************************************************
* Action : get-section;
*******************************************************************************/
if($act == "get-title"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$section = "SELECT id,name as name FROM ". $app['table']['title'] . " WHERE id_section IN ('". $id_title."') AND status='active'";
	db::query($section, $rs['section'], $nr['section']);
	
	if($nr['section'] > 0){
		while($row = db::fetch($rs['section'])){
			$callback['section'][] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	// $callback["sql_title"] = $section;
	echo json_encode($callback);
	exit;
endif;
?>
