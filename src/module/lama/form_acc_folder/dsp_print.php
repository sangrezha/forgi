<html><head>
	<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
	</style>
	<title></title>
</head>
<!-- <body onload="doPrint()"> -->
<body onload="">
	<table width="900" border="0" align="center">
	    <tbody><tr>
	        <td style="display: flex; align-items: center;border: 1px solid black;">	        	
<!-- 	            <img src="logo.png" alt="" style="width: 200px; height: 150px; display: block; text-align: left;"> -->
	            <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 200px; height: auto; display: block; text-align: left;border-right: 1px solid black;">
	            <span style="display: block;font-size: 20px;font-weight: bold;margin-left: 100px;letter-spacing: 5px"><p>FORM PERMINTAAN AKSES FOLDER</p></span>
	        </td>
	    </tr>
	</tbody></table>
	<div style="border: 1px solid black;width: 172px;height: 30px;margin-left: 730px;text-align: center;">
	   Revisi <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
	</div>
	<table width="900px" border="0" align="center">
	<tbody>
		<tr>
			<td>Folder</td>
			<td>:</td>
			<td> 
				<input type="radio" id="baru" name="status_f" value="baru" <?= ($form['status_f']=="baru"?"checked":"") ?> disabled>
				<label for="baru">Baru</label>
				<input type="radio" id="ada" name="status_f" value="ada" <?= ($form['status_f']=="ada"?"checked":"") ?> disabled>
				<label for="ada">Ada</label>
<!-- <input type="radio" id="other" name="gender" value="other">
<label for="other">Other</label> </td> -->
	 </tr>
		<tr>
			<td>Name</td>
			<td>:</td>
			<td><?=  $form['name'] ?></td>
	 </tr>
	 <?php if($form['status_f']=="baru"){ ?>
	 <tr>
			<td>Function<br>(Isi bila folder baru )</td>
			<td>:</td>
			<td><?= $form['function']  ?></td>
	 </tr>
	 <?php } ?>
		<tr>
			<td>Penanggung Jawab Folder</td>
			<td>:</td>
			<td><?= $form['own'] ?></td>
	 </tr>
		<!-- <tr>
			<td>Alasan</td>
			<td>:</td>
			<td><?= $form['alasan'] ?></td>
	 </tr> -->
		
	</tbody></table>
	<br><br>
<table border="1" style="margin-left: 216px;">
	<tr>
		<td style="padding: 10px;text-align: center;">No</td>
		<td style="padding: 10px;text-align: center;">NIK</td>
		<td style="padding: 10px;text-align: center;">Nama</td>
		<td style="padding: 10px;text-align: center;">Jabatan</td>
		<td style="padding: 10px;text-align: center;" colspan="2">Hak Akses
		</td>
	</tr>
	<?php 
		// $_SESSION['add_member'] = [];
		$no = 0;
		while($row = db::fetch($rs['add_member'])){
			$user_form = db::get_record("member", "id", $row['id_user']);
			$name_section = db::lookup("name","section", "id", $user_form['id_section']);
			?>
	<tr>
		<td style="padding: 10px;text-align: center;"><?= $no ?></td>
		<td style="padding: 10px;text-align: center;"><?= $row['nik'] ?></td>
		<td style="padding: 10px;text-align: center;"><?= $user_form['name'] ?></td>
		<td style="padding: 10px;text-align: center;"><?= $name_section ?></td>
		<?php if($row['premission']=="w"){ ?>
			<td style="padding: 10px;text-align: center;">Read & Write</td>
		<?php }else{ ?>
			<td style="padding: 10px;text-align: center;">Read</td>
		<?php } ?>
	</tr>
	 <?php $no++; } ?>
</table>
	<div style="/*! border: 1px solid black; */width: 900px;margin-left: 600px;margin-top: 50px;">
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
<p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p>
  <table border="1">
    <tbody><tr style="text-align: center;">
      <td>Manager IT</td>
      <td>Staff IT</td>
      <td style="border:0px;"> </td>
      <td>Manager</td>
      <td>Pemohon</td>
    </tr>
    <tr style="text-align:center;">
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="border:0px;height: 80px;width: 100px;"><======</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
    </tr>
    <tr style="text-align: center;">
	  <td><?= $approve_mng_it_name ?></td>
	  <td><?= $approve_it_name ?></td>
	  <td style="border:0px;"></td>
      <td><?= $approve_mng_name ?></td>
      <td><?= $form['name'] ?></td>
    </tr>
  </tbody></table>
  
</div>
<script>
	function doPrint() {
	    window.print();            
	    document.location.href = "trucking.mod"; 
	}
</script>
<div state="voice" class="placeholder-icon" id="tts-placeholder-icon" title="Click to show TTS button" style="background-image: url(&quot;moz-extension://d957ec0f-edb7-42ef-a0a8-6f64e9a2a6ec/data/content_script/icons/voice.png&quot;); display: none;"><canvas width="36" height="36" class="loading-circle" id="text-to-speech-loader" style="display: none;"></canvas></div></body></html>