<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	admlib::get_component('select2lib');
	admlib::get_component('uploadlib');
	admlib::get_component('datepickerlib');
	admlib::get_component('formstart');
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"status_f",
		// 		"datas"=>["baru","ada"],
		// 		"value"=>app::ov($module['status_f'])
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"name_f",
		// 		"value"=>app::ov($module['name_f']),
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"type"=>"hidden",
		// 		"name"=>"name",
		// 		"value"=>app::ov($module['name']),
		// 	)
		// );
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"status_f",
		// 		"datas"=>["baru","ada"],
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['status_f'])
		// 	)
		// );
		// echo "disini";
		// print_r($_SESSION["add_soft"]);
		 ?>
		<div class="form-group">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<label class="control-label col-md-3" for="name_member">Media Simpan</label>
				<div style="margin-top: 30px;margin-left: 30px;">
					<input type="radio" id="tape" name="media" value="tape" <?= ($form['media']=="tape"?"checked":"") ?> >
					<label for="tape">Tape</label>
					<input type="radio" id="nas" name="media" value="nas" <?= ($form['media']=="nas"?"checked":"") ?> >
					<label for="nas">NAS</label>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<label class="control-label col-md-3" for="name_member">Total size</label>
				<input type="text" name="p_size" id="size" value="" class="form-control " validate="">
				<!-- <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span> -->
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<label class="control-label col-md-2" for="status">Status<span class="required"></span></label>
				
				<div style="margin-top: 30px;margin-left: 30px;">
					<input type="radio" id="ok" name="status_file" value="ok" <?= ($form['status_file']=="ok"?"checked":"") ?> >
					<label for="ok">Ok</label>
					<input type="radio" id="ng" name="status_file" value="ng" <?= ($form['status_file']=="ng"?"checked":"") ?> >
					<label for="ng">NG</label>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<label class="control-label col-md-3" for="lokasi">lokasi<span class="required"></span></label>
				<input type="text" name="p_lokasi" id="lokasi" value="" class="form-control ">
				<!-- <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span> -->
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<label class="control-label col-md-2" for="bukti">bukti<span class="required"></span></label>
				<input type="text" name="p_bukti" id="bukti" value="" class="form-control ">
				<!-- <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span> -->
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12" style="margin-top:27px;">
				<!-- <button class="btn btn-success" onclick="adding_recovery()">add</button> -->
				<a href="#" class="btn btn-success" onclick="adding_recovery()">add</a>
			</div>
		</div>
		<br>
				<div id="user_total"></div>
		
<?php 

		admlib::get_component('textarea',
			array(
				"name"=>"keterangan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				// "validate"=>true,
				"value"=>app::ov($form['catatan'])
			)
		);
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
	?>
	
<script>

$(document).ready(function () {

			$('#id_departement').each(function(){
				var id_section = $(this).val();
				var dis = '<?= $form['id_section'] ?>';
				// alert(dis);
				// console.log(dis);
				var urlcustomer = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
				// var urlmaker = "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_customer;
				// console.log("ID " + urlcustomer);
			get_section(id_section, dis, urlcustomer);
				// get_makermodel(id_customer, dis, urlmaker);
			});

			$('#id_departement').change(function(){
				// alert("disini");
				$('#id_section').select("val","null");
				// $('#id_title').select("val","null");
				var id_section = $(this).val();
				var dis = '';
				// console.log(dis);
				var urlsection = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
				// alert(urlsection);
				// var urlmaker = "<?php //echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_departement;
				// console.log("ID section: " + urlsection);
				// console.log("ID Maker: " + urlmaker);
				get_section(id_section, dis, urlsection);
				// get_makermodel(id_section, dis, urlmaker);
			});

	<?php // if(empty($form['status_f']) || $form['status_f'] == "ada"){ ?>
		// $("#g_function").hide();
	<?php // } ?>
		// $('input[type=radio][name=p_status_f]').change(function() {
		// 	if (this.value == 'baru') {
		// 		// alert("Allot Thai Gayo Bhai");
		// 		$("#g_function").show();
		// 	}
		// 	else if (this.value == 'ada') {
		// 		// alert("Transfer Thai Gayo");
        //       	$("#g_function").hide();
		// 	}
		// });
		// $('#nik').change(function() {
		// 	// alert("<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value);
		// 	$.ajax({
		// 	type:"GET",
		// 	url:"<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value,
		// 		success:function(res){         
		// 				var result = JSON.parse(res);
		// 				if(result.error == 0){
		// 					// var loop = "";
		// 					// console.log(result);12 
		// 					// "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section
		// 					console.log(JSON.stringify(result));
		// 					// alert(JSON.stringify(result));
		// 					// alert(result.member.id_section);
		// 					$("#id_m").val(result.member.id);
		// 					$("#name_m").val(result.member.name);
		// 					$("#nik_m").val(result.member.nik);
		// 					$("#name_member").val(result.member.name);
		// 					$("[name=p_id_section]").val(result.member.id_section);
		// 					// $('select[name="p_id_section"]').find('option:contains("'+result.member.id_section+'")').attr("selected",true);


		// 					// loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
		// 					// $.each(result.section,function(key,value){
		// 					// 	loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
		// 					// });
		// 					// $("#id_title").html(loop);
		// 					// console.log("loop " + loop);
		// 				}else{
		// 					alert("error");
		// 					// $("#id_title").html('');
		// 				}
		// 		}
		// 	}); 
		// });
});

function get_section(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						// console.log(result); 
						// if(dis){
						// 	loop = "<option disabled><?php echo app::i18n('select');?></option>";
						// }else{
							loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						// }
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						// alert(loop);
						$("#id_section").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_section").html('');
						$("#id_title").html('');
					}
			}
		});     
	}
		function addData2(act, param){
			// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>');
			// alert(param);
			var _param = $.extend({ act : act, step : 	'add' }, param);
			// alert('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			// console.log('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			// console.log("<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>/"+JSON.stringify(_param));
			$.post('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){ loadData() });
			
			// $.ajax({
			// type:"POST",
			// url:"<?php echo admlib::$page_active['module'] ?>.mod",
			// data:_param,
			// 	success:function(res){ 
			// 		loadData();
			// 	}
			// }); 
			return false;
		}
		function adding_recovery(){
			var act = "adding_recovery", media = $('input[name="media"]:checked').val(), size = $('#size').val(), status_file = $('input[name="status_file"]:checked').val(), lokasi = $('#lokasi').val(), bukti = $('#bukti').val();
			// alert(jenis_file);
			// alert("disini");
			// alert(act);
			// alert(media);
			// alert(size);
			// alert(status_file);
			// alert(lokasi);
			// alert(bukti);
			if(!act || !media || !size || !status_file || !lokasi || !bukti){
				alert('tidah boleh kosong');
				return;
			}
			addData2(act, {id: Math.floor(Math.random() * 100000000000000000), media : media, size : size, status_file : status_file, lokasi : lokasi, bukti : bukti });
			return false;
		
    };
	function loadData(){
		$('#user_total').load('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : "adding_recovery" });
	};
	loadData();
	function delete_member(id_member){
		var r = confirm("Apakah anda yang ingin menghapus ?");
		// alert(id_member);
		// var _param = $.extend({ act : "adding_recovery", step : 'delete', id : id_member }, param);
		var _param = { act : "adding_recovery", step : 'delete', id : id_member };
		// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>'+_param);
		if (r == true) {
			$.post('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){  loadData(); });
		}
		return false;

	}
</script>
	<?php
admlib::display_block_footer();
?>