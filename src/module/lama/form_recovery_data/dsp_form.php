<?php
admlib::display_block_header();
	admlib::get_component('texteditorlib');
	// admlib::get_component('datepickerlib');
	admlib::get_component('datepickerlib_max');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				"value"=>$form['name']
			)
		);
		$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section",
				"value"=>app::ov($form['id_section']),
				"items"=>$rs['section']
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"jenis_data",
				"value"=>$form['jenis_data']
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"alasan",
				"value"=>$form['alasan']
			)
		);
		admlib::get_component('datepicker',
			array(
				"name"=>"start_date",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['start_date'])
			)
		);
		admlib::get_component('datepicker',
			array(
				"name"=>"end_date",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['end_date'])
			)
		);
		admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			"value"=> app::ov($form['attach']),
			"filemedia"=>true
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"keterangan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				// "validate"=>true,
				"value"=>app::ov($form['keterangan'])
			)
		);
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
admlib::display_block_footer();
?>
