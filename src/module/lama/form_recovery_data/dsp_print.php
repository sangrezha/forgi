<html><head>
	<style>
		
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}
	</style>
	<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
	</style>
	<title></title>
</head>
<!-- <body onload="doPrint()"> -->
<body onload="">
	<table width="900" align="center">
	    <tbody><tr>
	        <td style="display: flex; align-items: center;border: 1px solid black;">	        	
<!-- 	            <img src="logo.png" alt="" style="width: 200px; height: 150px; display: block; text-align: left;"> -->
	            <img src="<?php echo $app['www'] ."/src/assets/files/". $config['logo']; ?>" alt="" style="width: 200px; height: auto; display: block; text-align: left;border-right: 1px solid black;">
	            <span style="display: block;font-size: 20px;font-weight: bold;margin-left: 100px;letter-spacing: 5px"><p>FORM PERMINTAAN AKSES FOLDER</p></span>
	        </td>
	    </tr>
	</tbody></table>
	<div style="border: 1px solid black;width: 172px;height: 30px;margin-left: 730px;text-align: center;">
	   Revisi <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?>
	</div>
	<br>
	<table width="900px" align="center">
	<tbody>
	 <tr>
		<td>Nama Pemohon</td>
		<td>Departemen</td>
		<td>Jenis data</td>
		<td>Alasan & penjelasan</td>
	 </tr>
	 <tr>
		<td><?= $name ?></td>
		<td><?= $section ?></td>
		<td><?= $form['jenis_data'] ?></td>
		<td><?= $form['alasan'] ?></td>
	 </tr>
		
	</tbody></table>
	<br><br>
	<div style="border:1px solid black;width:899px;height:200px;margin-left: 218px;">
		<span>
			Lampirkan gambar atau keterangan lain yang memperjelas
		</span>
		<br>
		<img src="<?php echo $app['data_lib'] ."/". $form['attach']; ?>" style="width:200px;height:150px;" alt="">
		<br>
		<span>
			Keterangan : <?= $form['keterangan'] ?>
		</span>

	</div>
	<div style="display: flex;margin-left: 218px;margin-top: 15px;">
		<!-- <div> -->

			<table>
				<tr>
					<td colspan="3">Diajukan</td>
				</tr>
				<tr>
					<th style="width: 70px!important;">Tgl</th>
					<th style="width: 70px!important;">Bln</th>
					<th style="width: 70px!important;">Thn</th>
				</tr>
				<tr>
					<td style="text-align:center;"><?= $p_day ?></td>
					<td style="text-align:center;"><?= $p_month ?></td>
					<td style="text-align:center;"><?= $p_year ?></td>
				</tr>
			</table>
		<!-- </div> -->
		<!-- <div> -->
			<table style="width:20%;margin-left: 125px;margin-right: 114px;">
				<tr>
					<th rowspan="2">Perhatian !!</th>
					<!-- <th>Tanggal Max. Backup adalah < 7 hari dari Tanggal saat ini</th> -->
					<td>Start recovery</td>
				</tr>
				<tr>
					<td><?= $form['start_date'] ?></td>
				</tr>
				<tr>
					<th rowspan ="2">Tanggal Max. Backup adalah < 7 hari dari Tanggal saat ini</th>
					<td>End recovery</td>
				</tr>
				<tr>
					<td><?= $form['end_date'] ?></td>
				</tr>
			</table>
		
			<table>
				<tr>
					<td colspan="3">Penyelesaian</td>
				</tr>
				<tr>
					<th style="width: 70px!important;">Tgl</th>
					<th style="width: 70px!important;">Bln</th>
					<th style="width: 70px!important;">Thn</th>
				</tr>
				<tr>
					<td style="text-align:center;"><?= $p_day ?></td>
					<td style="text-align:center;"><?= $p_month ?></td>
					<td style="text-align:center;"><?= $p_year ?></td>
				</tr>
			</table>
		<!-- </div> -->
	</div>
	<div style="/*! border: 1px solid black; */width: 900px;margin-left: 218px;margin-top: 50px;">
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
<!-- <p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p> -->
  <table>
    <tbody><tr style="text-align: center;">
      <td>Pemohon</td>
      <td>Supervisor</td>
      <td>Manager</td>
      <td style="border:0px;"> </td>
      <td>Staff IT</td>
      <td>Manager IT</td>
      <td style="border:0px;"> </td>
      <td>Pemohon</td>
    </tr>
    <tr style="text-align:center;">
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="border:0px;height: 80px;width: 100px;">======></td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="border:0px;height: 80px;width: 100px;">======></td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
    </tr>
    <tr style="text-align: center;">
      <td><?= $form['name'] ?></td>
      <td><?= $approve_spv_name ?></td>
      <td><?= $approve_mng_name ?></td>
	  <td style="border:0px;"></td>
	  <td><?= $approve_it_name ?></td>
	  <td><?= $approve_mng_it_name ?></td>
	  <td style="border:0px;"></td>
      <td><?= $form['name'] ?></td>
    </tr>
  </tbody></table>
  
</div>
<table style="width: 900px;margin-left: 216px;margin-top: 15px;">
	<tr>
		<td style="padding: 10px;text-align: center;">No</td> 
		<td style="padding: 10px;text-align: center;">Media</td> 
		<td style="padding: 10px;text-align: center;">Size</td> 
		<td style="padding: 10px;text-align: center;">Status file</td> 
		<td style="padding: 10px;text-align: center;">Lokasi</td> 
		<td style="padding: 10px;text-align: center;">Bukti</td> 
	</tr>
	<?php 
		// $_SESSION['add_member'] = [];
		$no = 1;
		while($row = db::fetch($rs['add_reco'])){
			// $user_form = db::get_record("member", "id", $row['id_user']);
			// $name_section = db::lookup("name","section", "id", $user_form['id_section']);
			?>
	<tr>
                    <td style="padding: 10px;text-align: center;"><?= $no ?></td>
                    <td style="padding: 10px;text-align: center;"><?= $row['media'] ?></td>
                    <td style="padding: 10px;text-align: center;"><?= $row['size'] ?></td>
                    <td style="padding: 10px;text-align: center;"><?= $row['status_file'] ?></td>
                    <td style="padding: 10px;text-align: center;"><?= $row['lokasi'] ?></td>
                    <td style="padding: 10px;text-align: center;"><?= $row['bukti'] ?></td>
	</tr>
	 <?php $no++; } ?>
</table>
<table style="width:900px;margin-left: 216px;margin-top: 15px;">
	<tr>
		<td>Catatan dari IT :</td>
	</tr>
	<tr>
		<td><?= $form['catatan'] ?></td>
	</tr>
</table>
	<!-- <div style="/*! border: 1px solid black; */width: 900px;margin-left: 600px;margin-top: 50px;"> -->
<!-- Pasuruan, <?= date_format($form['created_at'],"Y/m/d H:i:s") ?> (dd/mm/yy) -->
<!-- Pasuruan, <?= date('d F Y', strtotime($form['created_at'])) ?> (dd/mm/yy) -->
<!-- <p style="margin-left: 310px;">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</p>
  <table>
    <tbody><tr style="text-align: center;">
      <td>Pemohon</td>
      <td>Manager</td>
      <td style="border:0px;"> </td>
      <td>Staff IT</td>
      <td>Manager IT</td>
    </tr>
    <tr style="text-align:center;">
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="border:0px;height: 80px;width: 100px;"><======</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
      <td style="/*! border: 1px solid black; */height: 80px;width: 100px;">DTO</td>
    </tr>
    <tr style="text-align: center;">
      <td><?= $form['name'] ?></td>
      <td><?= $approve_mng_name ?></td>
	  <td style="border:0px;"></td>
	  <td><?= $approve_it_name ?></td>
	  <td><?= $approve_mng_it_name ?></td>
    </tr>
  </tbody></table>
  
</div> -->
<script>
	function doPrint() {
	    window.print();            
	    document.location.href = "trucking.mod"; 
	}
</script>
<div state="voice" class="placeholder-icon" id="tts-placeholder-icon" title="Click to show TTS button" style="background-image: url(&quot;moz-extension://d957ec0f-edb7-42ef-a0a8-6f64e9a2a6ec/data/content_script/icons/voice.png&quot;); display: none;"><canvas width="36" height="36" class="loading-circle" id="text-to-speech-loader" style="display: none;"></canvas></div></body></html>