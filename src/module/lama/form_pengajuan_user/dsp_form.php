<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	admlib::get_component('datepickerlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
	?>
	<style>
		.monthselect{
			color: black;
		}
		.yearselect{
			color: black;
		}
	</style>
	<?php
		admlib::get_component('radio',
			array(
				"name"=>"status_user",
				"datas"=>["new_worker","mutasi_worker","resign_worker"],
				"validate"=>true,
				"value"=>app::ov($form['status_user'])
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				"value"=>(empty($form['name'])?$app['me']['name']:$form['name'])
			)
		);
		$rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_departement_new",
				"value"=>app::ov($form['id_departement_new']),
				"items"=>$rs['departement']
			)
		);
		// $rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section",
				"value"=>app::ov($form['id_section']),
				"items"=>$rs['section']
			)
		);
		$rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		// $rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_departement_old",
				"value"=>app::ov($form['id_departement_old']),
				"items"=>$rs['departement']
			)
		);
		admlib::get_component('checkbox',
			array(
				"name"=>"list_permintaan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"datas"=>["pc","ms_office","printer"],
				"value"=>app::ov($form['list_permintaan'])
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"memperjelas",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				"value"=>app::ov($form['memperjelas'])
			)
		);
		// print_r($form);
		if($app['me']['code']=="hrd"){
			admlib::get_component('inputtext',
			array(
				"name"=>"nik",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					"value"=>$form['nik']
				)
			);
			admlib::get_component('radio',
				array(
					"name"=>"status_ker",
					"datas"=>["kontrak","tetap","magang"],
					"validate"=>true,
					"value"=>app::ov($form['status_ker'])
				)
			);
			admlib::get_component('datepicker',
				array(
					"name"=>"join_date",
					"validate"=>true,
					"value"=>($form['join_date']!="0000-00-00" && !empty($form['join_date'])?$form['join_date']:"")
				)
			);
			admlib::get_component('datepicker',
				array(
					"name"=>"end_date",
					"value"=>app::ov($form['end_date'])
				)
			);
			admlib::get_component('inputupload', 
			array(
				"name"=>"attach", 
				"value"=> app::ov($form['attach']),
				"filemedia"=>true
				)
			);
		}
		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
// admlib::display_block_footer();
?>

<script>


function get_section(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						// console.log(result); 
						// if(dis){
						// 	loop = "<option disabled><?php echo app::i18n('select');?></option>";
						// }else{
							loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						// }
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						// alert(loop);
						$("#id_section").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_section").html('');
						$("#id_title").html('');
					}
			}
		});     
	}
$(document).ready(function () {

	$('#id_departement_new').change(function(){
			// alert("disana");
			$('#id_section').select("val","null");
			// $('#id_title').select("val","null");
			var id_section = $(this).val();
			var dis = '';
			// console.log(dis);
			var urlsection = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
			// alert(urlsection);
			// var urlmaker = "<?php //echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_departement;
			// console.log("ID section: " + urlsection);
			// console.log("ID Maker: " + urlmaker);
			get_section(id_section, dis, urlsection);
			// get_makermodel(id_section, dis, urlmaker);
		});
	$('#id_departement_new').each(function(){
		// alert("disini");
			var id_section = $(this).val();
			var dis = '<?= $form['id_section'] ?>';
			// alert(dis);
			// console.log(dis);
			var urlcustomer = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
			// alert(urlcustomer);
			// var urlmaker = "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_customer;
			// console.log("ID " + urlcustomer);
		get_section(id_section, dis, urlcustomer);
			// get_makermodel(id_customer, dis, urlmaker);
		});

		
		$("#g_id_departement_old").hide();
		$('input[type=radio][name=p_status_user]').change(function() {
			// alert(this.value);
			if (this.value == 'mutasi_worker') { 
				$("#g_id_departement_old").show();
			}else{
              	$("#g_id_departement_old").hide();
			}
		});
		$('#nik').change(function() {
			// alert("<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value);
			$.ajax({
			type:"GET",
			url:"<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value,
				success:function(res){         
						var result = JSON.parse(res);
						if(result.error == 0){
							// var loop = "";
							// console.log(result);12 
							// "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section
							console.log(JSON.stringify(result));
							// alert(JSON.stringify(result));
							// alert(result.member.id_section);
							$("#id_m").val(result.member.id);
							$("#name_m").val(result.member.name);
							$("#nik_m").val(result.member.nik);
							$("#name_member").val(result.member.name);
							$("[name=p_id_section]").val(result.member.id_section);
							// $('select[name="p_id_section"]').find('option:contains("'+result.member.id_section+'")').attr("selected",true);


							// loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
							// $.each(result.section,function(key,value){
							// 	loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
							// });
							// $("#id_title").html(loop);
							// console.log("loop " + loop);
						}else{
							alert("error");
							// $("#id_title").html('');
						}
				}
			}); 
		});
});

		function addData(act, param){
			// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>');
			// alert(param);
			var _param = $.extend({ act : act, step : 	'add' }, param);
			// alert('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			// console.log('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			$.post('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){ loadData() });
			
			// $.ajax({
			// type:"POST",
			// url:"<?php echo admlib::$page_active['module'] ?>.mod",
			// data:_param,
			// 	success:function(res){ 
			// 		loadData();
			// 	}
			// }); 
			return false;
		}
		function add_user(){
			var act = "add_user", nik = $('#nik_m').val(), name = $('#name_m').val(), id = $('#id_m').val(), id_section = $('#id_section').val(), premission = $('#premission').val();
			// alert(act);
			// alert(name);
			// alert(id);
			// alert(id_section);
			// alert(nik);
			if(!act || !name || !id || !id_section || !nik || !premission){
				// if (!pib || !cont) 
				// {
					alert('tidah boleh kosong');
					return;
				// }
			}
			addData(act, { nik : nik, name : name, id : id, id_section : id_section , premission : premission });
			return false;
		
    };
	function loadData(){
		$('#user_total').load('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : "add_user" });
	};
	loadData();
	function delete_member(id_member){
		var r = confirm("Apakah anda yang ingin menghapus ?");
		// alert(id_member);
		// var _param = $.extend({ act : "add_user", step : 'delete', id : id_member }, param);
		var _param = { act : "add_user", step : 'delete', id : id_member };
		// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>'+_param);
		if (r == true) {
			$.post('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){  loadData(); });
		}
		return false;

	}
</script>
	<?php
admlib::display_block_footer();
?>