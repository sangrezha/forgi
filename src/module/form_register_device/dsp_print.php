<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM REGISTRASI PERANGKAT PENYIMPANAN DATA</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 landscape }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">
	<table width="90%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;">
	    <tbody>
            <tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="50%">&nbsp;</td>
            </tr>
            <tr>
                <td>	        	
                    <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 5px;color:#fff;">	        	
                FORM REGISTRASI <br> PERANGKAT PENYIMPANAN DATA
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" align="right" style="font-size:14px;">
                        <tbody>
                            <tr>
                                <td>Tanggal Pengajuan</td>
                                <td>:</td>
                                <td> 
                                    <?= date('d/m/Y', strtotime($form['tgl_pengajuan_dev']))?>
                                </td>
                            </tr>
                            <tr>
                                <td>Departemen</td>
                                <td>:</td>
                                <td><?= $departement ?></td>
                            </tr>
                            <?php if($form['status_f']=="baru"){ ?>
                            <tr>
                                <td>Function<br>(Isi bila folder baru )</td>
                                <td>:</td>
                                <td><?= $form['function']  ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td>Bagian</td>
                                <td>:</td>
                                <td><?= $bagian ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <table cellspacing="1" cellpadding="3" style="font-size: 12px;" align="right" width="50%">
                        <tbody>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                <td style="background:#e6e6e6;color:#222;"></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Revisi</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                            </tr>
                       </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table border="0" width="100%" style="font-size:12px;" cellpadding="5">
                        <tr align="center" style="background:#10069f;color:#fff;">
                            <td colspan="6" align="center">DIISI OLEH USER</td>
                            <td colspan="3" align="center">DIISI OLEH IT</td>
                        </tr>
                        <tr align="center" style="background:#10069f;color:#fff;">
                            <td>No</td>
                            <td>Jenis Perangkat</td>
                            <td>Maker</td>
                            <td>Tipe</td>
                            <td>PIC</td>
                            <td>Jabatan</td>
                            <td>Kode Perangkat</td>
                            <td>Enkripsi</td>
                            <td>Aktif s/d</td>
                        </tr>
                        <?php 
                        $no = 1;
                        while($row = db::fetch($rs['add_register'])){
                        $name_pc	  = db::lookup("name","member", "id", $row['id_pic']);
                        ?>
                        <tr align="center" style="background:#e6e6e6;color:#222;">
                            <td><?= $no ?></td>
                            <td><?= $row['jenis_perangkat'] ?></td>
                            <td><?= $row['maker'] ?></td>
                            <td><?= $row['tipe'] ?></td>
                            <td><?= $name_pc ?></td>
                            <td><?= $row['jabatan'] ?></td>
                            <td><?= $row['kode_perangkat'] ?></td>
                            <td><?= $row['enkripsi'] ?></td>
                            <td><?= date('d/m/Y', strtotime($row['aktif'])) ?></td>
                        </tr>
                        <?php $no++; } ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                <table border="0" width="100%" style="font-size:12px;" cellpadding="5">
                    <tbody>
                        <tr>
                            <td colspan="8" style="font-size:12px;" align="right">Pasuruan, <?= date('d/m/Y', strtotime($form['created_at'])) ?> (dd/mm/yy)</td>
                        </tr>
                        <tr style="text-align: center;"> 	
                            <td style="background:#000;color:#fff;">Yang Mengajukan</td>
                            <td colspan="4" style="background:#000;color:#fff;">Menyetujui (Dept/Divisi Terkait)</td>
                            <td colspan="3" style="background:#000;color:#fff;">Mengesahkan (IT Dept)</td>
                        </tr>
                        <tr style="text-align: center;"> 	
                            <td width="12.5%" style="background:#000;color:#fff;">User</td>
                            <td width="12.5%" style="background:#000;color:#fff;">Supervisor</td>
                            <td width="12.5%" style="background:#000;color:#fff;">Manager</td>
                            <td width="12.5%" style="background:#000;color:#fff;">GM</td>
                            <td width="12.5%" style="background:#000;color:#fff;">Direktur/VP</td>
                            <td width="12.5%" style="background:#000;color:#fff;">Staff</td>
                            <td width="12.5%" style="background:#000;color:#fff;">Manager</td>
                            <!-- <td width="12.5%" style="background:#000;color:#fff;">Direktur/VP</td> -->
                        </tr>
                        <tr align="center" style="height:75px;">
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <td style="background:#e6e6e6;color:#222;">DTO</td>
                              <!-- <td style="background:#e6e6e6;color:#222;">DTO</td> -->
                            </tr>
                        <tr style="text-align: center;">
                            <td style="background:#000;color:#fff;"><?= $app['me']['name'] ?></td>
                            <td style="background:#000;color:#fff;"><?= $approve_spv_name ?></td>
                            <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                            <td style="background:#000;color:#fff;"><?= $approve_gm_name ?></td>
                            <td style="background:#000;color:#fff;"><?= $approve_dir_name ?></td>
                            <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                            <td style="background:#000;color:#fff;"><?= $approve_3_it_name ?></td>
                            <!-- <td style="background:#000;color:#fff;"><?= "" ?></td> -->
                        </tr>
                    </tbody>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                     <div style="width: 60%; height: 165px;border:2px solid #000; padding:0 15px 7px 0px; line-height:20px;border-radius:30px;font-size:12px;">
                        <ul style="list-style:number;"><strong>Keterangan:</strong>
                            <li>Perangkat yang tidak di daftarkan, Tidak bisa digunakan di area CMWI termasuk perangkat dari pihak luar ( Goverment, Supplier, Trainer, dll )</li>
                            <li>Jika Perangkat yang sudah didaftarkan akan digunakan di luar area CMWI, harus di Unregister terlebih dahulu dengan menggunakan Form ini (Mobile phone tidak termasuk)</li>
                            <li>Enkripsi data adalah untuk memproteksi data yang tercopy ke perangkat penyimpanan agar tidak bisa dibaca oleh pihak luar yang tidak berkepentingan</li>
                        </ul>
                    </div>
                </td>
            </tr>
	   </tbody>
    </table>
    </section>
    

</body>

</html>