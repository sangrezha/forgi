<?php
admlib::display_block_header();
?>

<style>
		.monthselect{
			color: black;
		}
		.yearselect{
			color: black;
		}
	</style> <?php
	// admlib::get_component('texteditorlib');
	admlib::get_component('datepickerlib');
	admlib::get_component('select2lib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
	
		if($form['no_pengajuan']){
			admlib::get_component('view',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"validate"=>true,
					"value"=>$form['no_pengajuan']
				)
			);
		}
		 ?>
		 <div class="form-group" style="">
			 <table style="margin-left: 25%;width: 15%;">
				 <tr>
					 <td style="padding:10px;">Jenis Perangkat</td>
					 <td>:</td>
					 <td style="padding:10px;"><span id="jenis_perangkat"></span></td>
				 </tr>
				 <tr>
					 <td style="padding:10px;">Maker</td>
					 <td>:</td>
					 <td style="padding:10px;"><span id="maker"></span></td>
				 </tr>
				 <tr>
					 <td style="padding:10px;">Tipe</td>
					 <td>:</td>
					 <td style="padding:10px;"><span id="tipe"></span></td>
				 </tr>
				 <tr>
					 <td style="padding:10px;">PIC</td>
					 <td>:</td>
					 <td style="padding:10px;"><span id="pic"></span></td>
				 </tr>
				 <tr>
					 <td style="padding:10px;">Jabatan</td>
					 <td>:</td>
					 <td style="padding:10px;"><span id="jabatan"></span></td>
				 </tr>
			 </table>
			<!-- <div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-4" for="jenis_perangkat">Jenis Perangkat : <u><span id="jenis_perangkat"></span></u> </label>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-4" for="maker">Maker : <u><span id="maker"></span></u> </label>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-4" for="maker">Tipe : <u><span id="tipe"></span></u> </label>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-4" for="maker">PIC : <u><span id="pic"></span></u> </label>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-4" for="maker">Jabatan : <u><span id="jabatan"></span></u> </label>
			</div> -->
		 </div>
		<div class="form-group">
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="devices">Devices<span class="required"></span>		</label>
			<select id="devices" name="p_devices" class="select2 form-control">
				<option value="" disabled selected><?php echo app::i18n('select');  ?></option>
				<?php 	
				$sql = "SELECT id,maker FROM ". $app['table']["data_form_register_device"] ." where id_form='$id'  ORDER BY maker asc";
				db::query($sql, $rs['row'], $nr['row']);
				while($row = db::fetch($rs['row'])){ ?>
						<option value="<?= $row['id']; ?>"><?= $row['maker']; ?></option>
				<?php } ?>
			</select>
		</div>
		<!-- <label class="control-label col-md-1 col-sm-12 col-xs-12" for="tgl_countainer">&nbsp;</label> -->
		<!-- <div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="num_container">Container<span class="required"></span>		</label>
			<input type="text" name="p_num_container" id="num_container" value="" class="form-control has-feedback-left " validate="">
			<span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true" style="top:25px;"></span>
		</div> -->
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-10" for="kode">Kode Perangkat<span class="required"></span>		</label>
			<input type="text" name="p_kode" id="kode" value="" class="form-control " validate="">
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-3" for="enkripsi">Enkripsi<span class="required"></span>		</label>
			<input type="text" name="p_enkripsi" id="enkripsi" value="" class="form-control " validate="">
		</div>
		<div class="col-md-2 col-sm-12 col-xs-12">
			<label class="control-label col-md-6" for="aktif">Aktif s/d<span class="required"></span>		</label>
			<input type="text" name="p_aktif" id="aktif" value="" class="form-control datepickers" validate="">
		</div>

		

		<div style="margin-top:28px;">
			<!-- <button class="btn btn-success" onclick="add_feedback()">add</button> -->
			<a href="#" class="btn btn-success" onclick="add_feedback()">add</a>
		</div>
		<br>
				<div id="user_total"></div>
		
<?php 

/*		admlib::get_component('submit',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);*/
		admlib::get_component('submit_&_reject_4',
			array(
				"id"=>(isset($id))?$id:"",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
	?>
	
<script>


function get_section(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						// console.log(result); 
						// if(dis){
						// 	loop = "<option disabled><?php echo app::i18n('select');?></option>";
						// }else{
							loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						// }
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						// alert(loop);
						$("#id_section").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_section").html('');
						$("#id_title").html('');
					}
			}
		});     
	}
$(document).ready(function () {
	$('#id_departement').change(function(){
			// alert("disana");
			$('#id_section').select("val","null");
			// $('#id_title').select("val","null");
			var id_section = $(this).val();
			var dis = '';
			// console.log(dis);
			var urlsection = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
			// alert(urlsection);
			// var urlmaker = "<?php //echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_departement;
			// console.log("ID section: " + urlsection);
			// console.log("ID Maker: " + urlmaker);
			get_section(id_section, dis, urlsection);
			// get_makermodel(id_section, dis, urlmaker);
		});
		$('#id_departement').each(function(){
		// alert("disini");
			var id_section = $(this).val();
			var dis = '<?= $form['id_section'] ?>';
			// alert(dis);
			// console.log(dis);
			var urlcustomer = "<?php echo admlib::$page_active['module'] ?>.mod&act=get-section&id_section="+id_section;
			// alert(urlcustomer);
			// var urlmaker = "<?php // echo admlib::$page_active['module'] ?>.mod&act=get-maker-model&id_cus_pic="+id_customer;
			// console.log("ID " + urlcustomer);
			get_section(id_section, dis, urlcustomer);
			// get_makermodel(id_customer, dis, urlmaker);
		});
		$('input[type=radio][name=p_status_f]').change(function() {
			if (this.value == 'baru') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_function").show();
			}
			else if (this.value == 'ada') {
				// alert("Transfer Thai Gayo");
              	$("#g_function").hide();
			}
		});
		$('#devices').change(function() {
			// alert("<?php echo admlib::$page_active['module'] ?>.mod&act=get-member&id="+this.value);
			$.ajax({
			type:"GET",
			url:"<?php echo admlib::$page_active['module'] ?>.mod&act=get-devices&id="+this.value,
				success:function(res){         
						var result = JSON.parse(res);
						if(result.error == 0){
							console.log(JSON.stringify(result));
							// alert(result.device.jenis_perangkat);
							$("#jenis_perangkat").html(result.device.jenis_perangkat);
							$("#maker").html(result.device.maker);
							$("#tipe").html(result.device.tipe);
							// $("#pic").html(result.device.pic);
							$("#jabatan").html(result.device.jabatan);
							$("#pic").html(result.device.pic_name)
						}else{
							alert("error");
							// $("#id_title").html('');
						}
				}
			}); 
		});
});

		function addData(act, param){
			// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>');
			// alert(param);
			var _param = $.extend({ act : act, step : 	'add' }, param);
			// alert('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			// console.log('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>'+JSON.stringify(_param));
			$.post('<?php echo $app['http'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){ loadData() });
			
			// $.ajax({
			// type:"POST",
			// url:"<?php echo admlib::$page_active['module'] ?>.mod",
			// data:_param,
			// 	success:function(res){ 
			// 		loadData();
			// 	}
			// }); 
			return false;
		}
		function add_feedback(){
			var act = "add_feedback", maker = $('#maker_v').val(), id = $('#devices').val(), kode = $('#kode').val(), enkripsi = $('#enkripsi').val(), aktif = $('#aktif').val();
			
			// alert(jenis_perangkat);
			// alert(maker);
			// alert(tipe);
			// alert(devices);
			// alert(jabatan);

			if(!act || !id || !kode || !enkripsi || !aktif){
				// if (!pib || !cont) 
				// {
					alert('tidak boleh kosong');
					return;
				// }
			}
			addData(act, { id: id, kode : kode, enkripsi : enkripsi, aktif : aktif });
			return false;
		
    };
	function loadData(){
		$('#user_total').load('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', { act : "add_feedback" });
	};
	loadData();
	function delete_member(id_member){
		var r = confirm("Apakah anda yang ingin menghapus ?");
		// alert(id_member);
		// var _param = $.extend({ act : "add_feedback", step : 'delete', id : id_member }, param);
		var _param = { act : "add_feedback", step : 'delete', id : id_member };
		// alert('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>'+_param);
		if (r == true) {
			$.post('<?php echo $app['webmin'] ."/". admlib::$page_active['module'] .".mod"; ?>', _param, function(){  loadData(); });
		}
		return false;

	}
</script>
	<?php
admlib::display_block_footer();
?>