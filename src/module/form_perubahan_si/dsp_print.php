<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>FORM PERUBAHAN SI</title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/normalize.css?'. rand(1,1000); ?>">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo $app['_styles'] .'/print/paper.css?'. rand(1,1000); ?>">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A4 }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">
    <section class="sheet padding-10mm">
        <table width="100%" border="0" align="center" style="font-family: 'Helvetica Neue', Roboto, Arial, 'Droid Sans', sans-serif;font-size: 12px;">
            <tbody>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td width="65%">&nbsp;</td>
                </tr>
                <tr>
                    <td>	        	
                        <img src="<?php echo $app['www'] ?>/src/assets/imgs/logo_CMWI.png" alt="" style="width: 100%; height: auto; display: block; text-align: left;">
                    <td colspan="2" align="center" style="background:#000;font-size: 20px;font-weight: bold;letter-spacing: 0px;color:#fff;">	        	
                    FORM PERMINTAAN <br>
                        PERUBAHAN SISTEM INFORMASI
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" style="font-size:12px;float:left;" cellpadding="2" width="70%">
                            <tbody>
                                <tr>
                                    <td width="30%">Pemohon</td>
                                    <td width="2%">:</td>
                                    <!-- <td><?=  $form['name_p'] ?></td> -->
                                    <td width="68%"><?php 
                                    $Section = db::lookup("name","section","id",$form['id_section']);
                                    echo $form['nik']." - ".$form['name_p']." - ".$section; ?></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Pengajuan</td>
                                    <td>:</td>
                                    <!-- <td><?=  $form['tgl_pengajuan_dev'] ?></td> -->
                                    <td><?=  date("d-m-Y", strtotime($form['tgl_pengajuan_dev'])) ?></td>

                                </tr>
                                <tr>
                                    <td valign="top">System yang dibutuhkan</td>
                                    <td valign="top">:</td>
                                    <td>
                                    <?php 
                                    $used_sys = db::lookup("name","sistem_informasi","id='".$form['kat_si']."'");	 ?>
                                    <?=  (empty($used_sys)?$form['kat_si']:$used_sys) ?>
                                </tr>
                                <tr>
                                    <td valign="top">Penjelasan kondisi saat ini</td>
                                    <td valign="top">:</td>
                                    <td><?=  $form['saat_ini'] ?></td>
                                </tr>
                                <tr>
                                    <td valign="top">Kondisi yang di inginkan</td>
                                    <td valign="top">:</td>
                                    <td><?=  $form['di_inginkan'] ?> </td>
                                </tr>
                                <tr>
                                    <td valign="top">Tujuan perubahan</td>
                                    <td valign="top">:</td>
                                    <td><?=  $form['tujuan'] ?> </td>
                                </tr>
                            </tbody>
                        </table>
                        
                       <!--  <table width="40%" border="0" align="right" style="font-weight:bold;font-size:14px;padding:5px;background:#f58220;border-radius: 10px;">
                            <tbody>
                                <tr align="center">
                                    <td>Revisi: <?= $revisi ?> | <?= date('d/m/Y', strtotime($form['created_at'])) ?></td>
                                </tr>
                            </tbody>
                        </table> -->
                    <table cellspacing="1" cellpadding="3" style="font-size: 12px;" valign="top" align="right" width="30%">
                        <tbody>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Dokumen</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $form['no_pengajuan'] ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Terbit</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($form['created_at']))?"":date('d/m/Y', strtotime($form['created_at']))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">No Revisi</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $revisi ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Revisi</td>
                                    <td style="background:#e6e6e6;color:#222;"><?= (($created_revisi=="-")?"-":date('d/m/Y', strtotime($created_revisi))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Tgl Berlaku</td>
                                <td style="background:#e6e6e6;color:#222;"><?= ((empty($created_approve_it))?"-":date('d/m/Y', strtotime($created_approve_it))) ?></td>
                            </tr>
                            <tr>
                                <td style="background:#b7b2b2;color:#222;">Masa Simpan</td>
                                <td style="background:#e6e6e6;color:#222;"><?= $p_masa_simpan ?> tahun</td>
                            </tr>
                       </tbody>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <i>( Lampirkan ilustrasi, gambar, flowchart dsb untuk memperjelas. )</i>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <table border="0" style="font-size:12px;" cellpadding="5" width="100%">
                            <tbody>
                                <tr style="text-align: center;">
                                  <td width="19%" style="background:#000;color:#fff;">Manager IT</td>
                                  <td width="19%" style="background:#000;color:#fff;">Staff IT</td>
                                  <td width="5%"></td>
                                  <td width="19%" style="background:#000;color:#fff;">Manager</td>
                                  <td width="19%" style="background:#000;color:#fff;">Supervisor</td>
                                  <td width="19%" style="background:#000;color:#fff;">Pemohon</td>
                                </tr>
                               <tr align="center" style="height:75px;">
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td><span style="font-size:20px;font-weight:bold;">&#10229;</span></td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr style="text-align: center;">
                                  <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                  <td style="border: 0px;"></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_mng_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $approve_spv_name ?></td>
                                  <td style="background:#000;color:#fff;"><?= $form['name_p'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                         <table border="0" style="font-size:14px; background-color:#e6e6e6;padding:5px;" cellspacing="0" cellpadding="5" width="100%">
                            <tr>
                                <td width="20%">Hasil</td>
                                <td width="5%">:</td>
                                <td width="15%">
                                    <input type="radio" disabled id="done" name="p_done" value="done" <?= ($form['confirm_done']=="iya"?"checked":"") ?>>&nbsp;<label for="done">Done</label>
                                </td>
                                <td width="15%">
                                    <input type="radio" disabled id="undone" name="p_done" value="undone" <?= ($form['confirm_done']!="iya"?"checked":"") ?> >&nbsp;<label for="undone">Undone</label>
                                </td>
                                <td width="15%" align="right">Tanggal</td>
                                <td width="5%">:</td>
                                <td width="25%"><?= ($approved_date?date('d/m/Y H:i:s', strtotime($approved_date)):(!empty($form['confirm_date'])?date('d/m/Y', strtotime($form['confirm_date'])):"")    ) ?></td>
                            </tr>
                            <tr>
                                <td valign="top">Catatan</td>
                                <td valign="top">:</td>
                                <td colspan="5"><?= $form['note'] ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                
                <tr>
                    <td colspan="2">
                        <table border="0" style="font-size:12px;" cellpadding="5" width="100%">
                            <tbody>
                                <tr style="text-align: center;">
                                    <td width="20%" style="background:#000;color:#fff;">Manager IT</td>
                                    <td width="20%" style="background:#000;color:#fff;">Staff IT</td>
                                    <td width="20%">&nbsp;</td>
                                    <td width="20%">&nbsp;</td>
                                    <td width="20%" style="background:#000;color:#fff;">Pemohon</td>
                                </tr>
                                <tr align="center" style="height:75px;">
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                    <td width="20%">&nbsp;</td>
                                    <td width="20%">&nbsp;</td>
                                    <td style="background:#e6e6e6;color:#222;">DTO</td>
                                </tr>
                                <tr style="text-align: center;">
                                    <td style="background:#000;color:#fff;"><?= $approve_mng_it_name ?></td>
                                    <td style="background:#000;color:#fff;"><?= $approve_it_name ?></td>
                                    <td width="20%">&nbsp;</td>
                                    <td width="20%">&nbsp;</td>
                                    <td style="background:#000;color:#fff;"><?= $form['name_p'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </section>
</body>
</html>