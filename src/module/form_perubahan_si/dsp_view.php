<?php
admlib::display_block_header();
	// admlib::get_component('texteditorlib');
	// datepickerlib
	?>
	<style>
			.monthselect{
				color: black;
			}
			.yearselect{
				color: black;
			}
		</style> <?php
	admlib::get_component('datepickerlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		if($form['no_pengajuan']){
			admlib::get_component('view',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"validate"=>true,
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('view',
			array(
				"name"=>"name_p",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['name_p'])
			)
		);
		// admlib::get_component('view',
		// 	array(
		// 		"name"=>"company",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['company'])
		// 	)
		// );
		admlib::get_component('view',
			array(
				"name"=>"nik",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['nik'])
			)
		);
		// $rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		$name_section = db::lookup("name","section","id",$form['id_section']);
		admlib::get_component('view',
			array(
				"name"=>"id_section",
				"value"=>app::ov($name_section)/*,
				"items"=>$rs['section']*/
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"tgl_pengajuan_dev",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['tgl_pengajuan_dev'])
			)
		);
		$rs["kat_si"] = db::get_record_select("id, name","sistem_informasi","status='active' ORDER BY name ASC");
		// $name_section = db::lookup("name","section","id",$form['id_section']);
		$kat_si = db::lookup("name","sistem_informasi","id",$form['kat_si']);
		if (empty($kat_si)) {		
			$kat_si = db::lookup("name","sistem_informasi","id",$form['kat_si']);
		}
		admlib::get_component('view',
			array(
				"name"=>"kat_si",
				"value"=>app::ov($kat_si),
				"items"=>$rs['kat_si']
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"saat_ini",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['saat_ini'])
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"di_inginkan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['di_inginkan'])
			)
		);
		admlib::get_component('view',
			array(
				"name"=>"tujuan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['tujuan'])
			)
		);
		
		/*admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			"value"=> app::ov($form['attach']),
			"filemedia"=>true
			)
		);*/
		admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			// "value"=> (!empty($form['attach']) AND file_exists($app['pwebmin'] ."/assets/".$module."/". $form['attach']))?$app['pwebmin'] ."/assets/".$module."/". $form['attach']:null,
			"value"=> (!empty($form['attach']) AND file_exists($app['pwebmin'] ."/assets/".$module."/". $form['attach']))?$app['pwebmin'] ."/assets/".$module."/". $form['attach']:null,
			"link"=>"yes",
			"id_link"=>$id,
			"accept"=>"jpg|png|jpeg|gif|pdf|docx|doc|xlsx|ico|txt",
			// "validate"=>"required",
			// "filemedia"=>true
			)
		);
		if($form['status_progress'] == "progress" || $form['status_progress'] == "finished"){
				admlib::get_component('view',
					array(
						"name"=>"note",
						// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
						// "value"=>"",
						"validate"=>true,
						"value"=>app::ov($form['note'])
					)
				);
		}
		if ($form['note_confirm']) {
			admlib::get_component('view',
				array(
					"name"=>"note_confirm",
					// "datas"=>["ok","rework"],
					// "validate"=>true,
					"value"=>app::ov($form['note_confirm'])
				)
			);
		}

		// admlib::get_component('submit',
		// 	array(
		// 		"id"=>(isset($id))?$id:"",
		// 		"no_submit"=>"yes",
		// 		"act"=>$act
		// 	)
		// );	
		if ($app['me']['level'] =="3") {
			admlib::get_component('submit_aprove',
				array(
					"id"=>(isset($id))?$id:"",
					// "no_submit"=>"iya",
					"act"=>"approve_it_3"
				)
			);
		}else{

			admlib::get_component('submit_aprove',
					array(
						"id"=>(isset($id))?$id:"",
						// "no_submit"=>"iya",
						"act"=>"approve_it"
					)
				);	
		}
	admlib::get_component('formend');
	?>
	
<script>

$(document).ready(function () {
	<?php //if(empty($form['used_sys']) || $form['used_sys'] == "other"){ 
		  if($form['used_sys'] != "other"){ 
		?>
		$("#g_other").hide();
	<?php } ?>
		$('input[type=radio][name=p_used_sys]').change(function() {
			if (this.value == 'other') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_other").show();
			}
			else{
				// alert("Transfer Thai Gayo");
              	$("#g_other").hide();
				  $('#other').value(""); 
			}
		});
});

</script>
	<?php
admlib::display_block_footer();
?>