<?php
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('form'=>'form_perubahan_si','caption'=>ucwords('Form perubahan si'));

/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$form = admlib::$page_active['form'];
	$total 	= db::lookup('COUNT(id)', $form, '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = "WHERE 1=1";
	if($ref)
	{
		$q .= " AND a.title LIKE '%". $ref ."%' AND a.content LIKE '%". $ref ."%' OR a.lang LIKE '%". $ref ."%'";
	}
	// $sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$form] ." a
	// 	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	// 	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	// 	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	// 	$q ORDER BY a.lang,a.reorder";
	$sql = "SELECT a.*, b.name as postby, c.name as modifyby, d.name as lang FROM ". $app['table'][$form] ." a
	LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['view']['user'] ." c ON (a.updated_by=c.id)
	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	$q ORDER BY a.lang,a.reorder";
	app::set_navigator($sql, $nav, $page_size, $form .".form");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['title'];
	$numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	if($numlang > 1)
	{
		$columns = array_merge($columns,['lang','duplicate']);
	}
	while($row = db::fetch($rs['row']))
	{
		if($numlang > 1)
		{
			$row['duplicate'] = '<a href="'.  $app['webmin'] .'/'. $form .'.form&act=duplicate&id='. $row['id'] .'"><i class="fa fa-copy" aria-hidden="true"></i></a>';
		}
		else {
			$row['duplicate'] = null;
		}
		$results[] = $row;
	}
	$option = ['sortable'=>true, 'status'=>true];
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	$form = admlib::$page_active['form'];
	if ($step == 1):
		form::populate($form);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		$validate = 'p_title,p_images';
		form::serialize_form();
		form::validate('empty', $validate);
		if (form::is_error()):
			msg::build_msg();
			header("location: ". $form .".form&act=add&error=1");
			exit;
		endif;
		app::mq_encode('p_title,p_content,p_images');
		$identity = rand(1, 100).date("dmYHis");
		$p_images = str_replace("%20"," ","$p_images");
		$sql_lang 	= "SELECT id FROM ". $app['table']['lang'] ." WHERE status='active'";
		$p_alias = app::slugify($p_title);
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		while($row 	= db::fetch($rs['lang']))
		{
			$id 		= rand(1, 100).date("dmYHis");
			$p_lang 	= $row['id'];
			$urut = db::lookup("max(reorder)",$form,"1=1");
			if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
			$sql 		= "INSERT INTO ".$app['table'][$form]."
					(id, title, link, images, identity, alias, lang, reorder, created_by, created_at) VALUES
					('$id', '$p_title', '$p_link', '$p_images', '$p_identity', '$p_alias', '$p_lang', '$urut', '". $app['me']['id'] ."', now())";
			db::qry($sql);
		}
		msg::set_message('success', app::i18n('create'));
		header("location: " . $app['webmin'] ."/". $form .".form");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	$form = admlib::$page_active['form'];
	if ($step == 1):
		$form = db::get_record($form, "id", $id);
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_title,p_images,p_lang');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". $form .".form&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$p_images = str_replace("%20"," ","$p_images");
		$p_alias = app::slugify($p_title);
		app::mq_encode('p_title,p_content,p_image,p_lang');
		$sql = "update ". $app['table'][$form] ."
				set title = '$p_title',
					link = '$p_link',
					images = '$p_images',
					alias = '$p_alias',
					lang = '$p_lang',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::i18n('modify'));
		header("location: " . $app['webmin'] ."/". $form .".form");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	$form = admlib::$page_active['form'];
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	else:
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table'][$form]."
			SET status = '$statusnya'
			WHERE identity = '$identity'";
	db::qry($sql);
	msg::set_message('success', app::i18n('update'));
	header("location: " . $app['webmin'] ."/". $form .".form");
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	$form = admlib::$page_active['form'];
	if($step == 1)
	{
		if($id) $items = $id;
		else $items = implode("','", $p_del);
		$sql = "SELECT id, title FROM ". $app['table'][$form] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql 	= "DELETE FROM ". $app['table'][$form] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		msg::set_message('success', app::i18n('delete'));
		header("location: " . $app['webmin'] ."/". $form .".form");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	$form = admlib::$page_active['form'];
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table'][$form] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$p_title 		= $row['title'];
			$p_content	 	= $row['content'];
			$p_image 		= $row['image'];

			$urut = db::lookup("max(reorder)", $form, "1=1");
			if ($urut==0){ $urut = 1; }else{ $urut = $urut+1; }
			$id = rand(1, 100).$urut.date("dmYHis");

			app::mq_encode('p_title,p_content');
			$sql = "INSERT INTO ". $app['table'][$form] ."
				(id, title, content, status, reorder, created_by, created_at) VALUES
				('$id', '$p_title', '$p_content', 'inactive', '$urut', '". $app['me']['id'] ."', now())";
			db::qry($sql);
		}
		msg::set_message('success', app::i18n('create'));
	}
	else
	{
		msg::set_message('error', app::i18n('notfound'));
	}
	header("location: " . $app['webmin'] ."/". $form .".form");
	exit;
endif;

/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	admlib::validate('UPDT');
	$form = admlib::$page_active['form'];
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $count ++;
			$sql = "UPDATE ". $app['table'][$form] ." SET reorder = " . $count . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
?>
