<?php
admlib::display_block_header();
	$module = admlib::$page_active['module'];
	// admlib::get_component('texteditorlib');
	// datepickerlib
	?>
	<style>
			.monthselect{
				color: black;
			}
			.yearselect{
				color: black;
			}
		</style> <?php
	admlib::get_component('datepickerlib');
	admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		if($form['no_pengajuan']){
			admlib::get_component('inputtext',
				array(
					"name"=>"no_pengajuan",
					// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
					// "value"=>"",
					"readonly"=>"yes",
					"validate"=>true,
					"value"=>$form['no_pengajuan']
				)
			);
		}
		admlib::get_component('inputtext',
			array(
				"name"=>"name_p",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				// "validate"=>true,
				// "value"=>$form['name']
				// "style_input"=>"width: 40%;",
				"value"=>($act=="add"?$app['me']['name']:$form['name_p']),
				"readonly"=>"yes"
			)
		);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"name"=>"company",
		// 		// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
		// 		// "value"=>"",
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['company'])
		// 	)
		// );
		admlib::get_component('inputtext',
			array(
				"name"=>"nik",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				// "hide_label"=>"iya",
				// "style"=>"style='width: 33%;margin-left: 467px;position: relative;bottom: 44px;'",
				// "validate"=>true,
				// "style_input"=>"width: 100%;",
				"type"=>"hidden",
				"value"=>($act=="add"?$app['me']['nik']:$form['nik']),
				"readonly"=>"yes"
			)
		);
		$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		$get_set = db::lookup("name","section","id",$form['id_section']);
		admlib::get_component('select',
			array(
				"name"=>"id_section",
				// "value"=>$app['me']['id_section'],
				// "hide_label"=>"iya",
				// "style"=>"style='width: 45%;margin-left: 626px;position: relative;bottom: 87px;'",
				// "style_input"=>"width: 100%;",
				"value"=>($act=="add"?$app['me']['id_section']:$form['id_section']),
				"readonly"=>"yes",
				"type"=>"hidden",
				"items"=>$rs['section']
			)
		);
		// admlib::get_component('datepicker',
		admlib::get_component('inputtext',
			array(
				"name"=>"tgl_pengajuan_dev",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				// "value"=>$form['tgl_pengajuan_dev']
				"value"=>$act=="add"?date("Y-m-d"):$form['tgl_pengajuan_dev'],
				"readonly"=>"yes",
				// "disabled"=>"yes"
			)
		);
		// admlib::get_component('radio',
		// 	array(
		// 		"name"=>"kat_si",
		// 		"datas"=>["js","andon","email","tp"],
		// 		"validate"=>true,
		// 		"value"=>app::ov($form['kat_si'])
		// 	)
		// );
		$rs["kat_si"] = db::get_record_select("id, name","sistem_informasi","status='active' ORDER BY name ASC");
		$get_kat = db::lookup("name","sistem_informasi","id",$form['kat_si']);
		admlib::get_component('select',
			array(
				"name"=>"kat_si",
				"validate"=>true,
				"value"=>app::ov($form['kat_si']),
				"items"=>$rs['kat_si']
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"saat_ini",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['saat_ini'])
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"di_inginkan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['di_inginkan'])
			)
		);
		admlib::get_component('textarea',
			array(
				"name"=>"tujuan",
				// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
				// "value"=>"",
				"validate"=>true,
				"value"=>app::ov($form['tujuan'])
			)
		);
		
/*		admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			"value"=> app::ov($form['attach']),
			"validate"=>true,
			"filemedia"=>true
			)
		);*/
		admlib::get_component('inputupload', 
		array(
			"name"=>"attach", 
			// "value"=> (isset($form['attach']) AND file_exists($app['attach_lib_path'] ."/". $form['attach']))?'/'. app::ov($form['attach']):null,
			"value"=> (!empty($form['attach']) AND file_exists($app['pwebmin'] ."/assets/".$module."/". $form['attach']))?$app['pwebmin'] ."/assets/".$module."/". $form['attach']:null,
			"link"=>"yes",
			"id_link"=>$id,
			"accept"=>"jpg|png|jpeg|gif|pdf|docx|doc|xlsx|ico|txt",
			// "validate"=>"required",
			// "filemedia"=>true
			)
		);
		if($form['status_progress'] == "progress" || $form['status_progress'] == "finished"){
				admlib::get_component('textarea',
					array(
						"name"=>"note",
						// "value"=>app::ov(empty($module['name'])?$app['me']['name']:$module['name']),
						// "value"=>"",
						"validate"=>true,
						"value"=>app::ov($form['note'])
					)
				);
		}

		// admlib::get_component('submit',
		// 	array(
		// 		"id"=>(isset($id))?$id:"",
		// 		"act"=>$act
		// 	)
		// );
	
		$cek_creator = db::lookup("created_by",$module,"id",$id);
		if(!empty($id)){
			if($cek_creator==$app['me']["id"]){
				/*admlib::get_component('submit',
					array(
						"id"=>(isset($id))?$id:"",
						"act"=>$act
					)
				);*/
				$cek_approve_2 = db::lookup("approve_2",$module,"id",$id);
				$cek_approve_3 = db::lookup("approve_3",$module,"id",$id);
				if ($cek_approve_2 =="iya" || $cek_approve_3 =="iya") {
					admlib::get_component('submit',
						array(
							"id"=>(isset($id))?$id:"",
							"no_submit"=>"iya",
							"act"=>$act
						)
					);
				}else{
					admlib::get_component('submit',
						array(
							"id"=>(isset($id))?$id:"",
							"act"=>$act
						)
					);
				}
			}else{
				// admlib::get_component('submit',
				// 	array(
				// 		"id"=>(isset($id))?$id:"",
				// 		"no_submit"=>"iya",
				// 		"act"=>$act
				// 	)
				// );				
				admlib::get_component('submit_aprove',
					array(
						"id"=>(isset($id))?$id:"",
						// "no_submit"=>"iya",
						"act"=>"approve"
					)
				);
			}
		}else{
			admlib::get_component('submit',
				array(
					"id"=>(isset($id))?$id:"",
					"act"=>$act
				)
			);
		}
	admlib::get_component('formend');
	?>
	
<script>

$(document).ready(function () {
	<?php //if(empty($form['used_sys']) || $form['used_sys'] == "other"){ 
		  if($form['used_sys'] != "other"){ 
		?>
		$("#g_other").hide();
	<?php } ?>
		$('input[type=radio][name=p_used_sys]').change(function() {
			if (this.value == 'other') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_other").show();
			}
			else{
				// alert("Transfer Thai Gayo");
              	$("#g_other").hide();
				  $('#other').value(""); 
			}
		});

 // $(document).ready(function() {
    //option A
<?php if($cek_creator==$app['me']["id"]|| $act=="add"){ ?>
    $("form").submit(function(e){
        // alert('submit intercepted');
        // e.preventDefault(e);
		if( document.getElementById("file_attach").files.length == 0 ){
	    	alert("Attachment tidak boleh kosong");
	    	e.preventDefault();
			return false;
		}
    });
    
<?php } ?>
// });
});

</script>
	<?php
admlib::display_block_footer();
?>