<?php
admlib::display_block_header();
// admlib::get_component('texteditorlib');
admlib::get_component('select2lib');
//select2lib
	admlib::get_component('datepickerlib');
admlib::get_component('uploadlib');
	admlib::get_component('formstart');
			// admlib::get_component('inputtext',
			// 	array(
			// 		"name"=>"code",
			// 		"value"=>app::ov($form['code']),
			// 		"validate"=>"required"
			// 	)
			// );

	?>
	<style>
		.monthselect{
			color: black;
		}
		.yearselect{
			color: black;
		}
	</style>
	<?php
	if ($form['no_revisi'] > "0" && !empty($form['name_lama'])) {
admlib::get_component('blank',
				array(
					"name"=>"history",

				)
			);
?>
<hr>
<?php
		admlib::get_component('inputtext',
			array(
				"name"=>"tgl_daftar_v",
					"readonly"=>"yes",
				// "value"=>app::ov($form['tgl_revisi']),
				"value"=>app::ov($form['tgl_daftar_lama']),
				// "validate"=>"required"
			)
		);

			$get_id = db::lookup("id",$module,"no_dokumen",$form['no_dokumen']);
			$get_path = db::lookup("path_lama",$module,"no_dokumen",$form['no_dokumen']);
			$get_ext_2 = explode(".", $get_path);
			if ($get_id) {
				if ($get_ext_2[1] != "xlsx" && $get_ext_2[1] != "jpg" && $get_ext_2[1] != "jpeg" && $get_ext_2[1] != "png" && $get_ext_2[1] != "xls") {
					$form['no_dokumen2'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$get_id."'>".$form['no_dokumen']."</a>";
				}elseif($get_ext_2[1] == "jpg" || $get_ext_2[1] == "jpeg" || $get_ext_2[1] == "png"){
					$form['no_dokumen2'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$get_id."'>".$form['no_dokumen']."</a>";
				}
			}else{
						$form['no_dokumen2'] = $form['no_dokumen'].";";
			}
/*			admlib::get_component('inputtext',
				array(
					"name"=>"no_dokumen",
					"readonly"=>"yes",
					// "readonly"=>"yes",
					// "value"=>($act=="add"?"IT-2-PK-".$no_doc:$form['no_dokumen']),
					"img_tooltip"=>$app['http']."/src/assets/imgs/static/img_no_dok.jpeg",
					"value"=>app::ov($form['no_dokumen']),
					// "validate"=>"required"
				)
			);*/
			admlib::get_component('html_view',
				array(
					"name"=>"no_dokumen",
					"readonly"=>"yes",
					// "readonly"=>"yes",
					// "value"=>($act=="add"?"IT-2-PK-".$no_doc:$form['no_dokumen']),
					"img_tooltip"=>$app['http']."/src/assets/imgs/static/img_no_dok.jpeg",
					"value"=>app::ov($form['no_dokumen2']),
					// "validate"=>"required"
				)
			);

admlib::get_component('inputtext',
				array(
					"name"=>"judul_v",
					"readonly"=>"yes",
					"value"=>app::ov($form['name_lama']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"tgl_terbit_v",
					"readonly"=>"yes",
					"value"=>app::ov($form['tgl_terbit_lama']),
					"validate"=>"required"
				)
			);
/* 			admlib::get_component('inputupload', 
				array(
					"name"=>"logo_crm", 
					"value"=> ($form['logo_crm'] != "" && file_exists($app['data_path'] ."/static/". $form['logo_crm']))?$app['_static'] ."/". $form['logo_crm']:null,
					"validate"=>"validate",
					"category"=>"image",
					"delete"=>"no",
					"size"=>"210px x 66px",
					"accept"=>"jpg|png|jpeg|gif|ico",
				)
			); */
			/*admlib::get_component('inputupload', 
				array(
					"name"=>"path", 
					"value"=> (!empty($form['path']) AND file_exists($app['pwebmin'] ."/assets/cmwi_file/". $form['path']))?$app['pwebmin'] ."/assets/cmwi_file/". $form['path']:null,
					"accept"=>"jpg|png|jpeg|gif|pdf|xls|docx|doc|xlsx|ico|txt",
					"validate"=>true
					// "value"=> $form['path'],
					// "filemedia"=>true
				)
			);*/
		/*	$rs["template_dokumen"] = db::get_record_select("name id, name","template_dokumen","status='active' ORDER BY name ASC");
			admlib::get_component('select2',
				array(
					"name"=>"template_dokumen",
					// "readonly"=>"yes",
					// "hide_label"=>"iya",
					// "style"=>"style='width: 45%;margin-left: 473px;position: relative;bottom: 43px;'",
					// "style_input"=>"width: 100%;",
					"validate"=>true,
					"value"=>$act=="add"?$app['me']['template_dokumen']:$form['template_dokumen'],
					"items"=>$rs['template_dokumen']
				)
			);*/
			
			admlib::get_component('inputtext',
				array(
					"name"=>"no_revisi_v",
					"readonly"=>"yes",
					"value"=>app::ov($form['no_revisi_lama']),
					// "validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"tgl_revisi_v",
					"readonly"=>"yes",
					"value"=>(empty($form['tgl_revisi_lama']) || $form['tgl_revisi_lama'] =="0000-00-00" ?"":$form['tgl_revisi_lama']),
					// "validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"tgl_berlaku_v",
					"readonly"=>"yes",
					"value"=>app::ov($form['tgl_berlaku_lama']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"jml_hal_v",
					"readonly"=>"yes",
					// "type"=>"number",
					"value"=>app::ov($form['jml_hal_lama']),
					"validate"=>"required"
				)
			);


		if ($form['dok_terkait_lama']) {

			$array_terkait = explode(";", $form['dok_terkait_lama']);
			if (count($array_terkait)>1) {
				$form['dok_terkait2'] = "";
				foreach ($array_terkait as $key) {
					$get_id = db::lookup("id",$module,"no_dokumen",$key);
					$get_path = db::lookup("path_lama",$module,"no_dokumen",$key);
					$get_ext_2 = explode(".", $get_path);
					// print_r($get_ext_2);
					if ($get_id) {
						if ($get_ext_2[1] != "xlsx" && $get_ext_2[1] != "jpg" && $get_ext_2[1] != "png" && $get_ext_2[1] != "xls") {
							$form['dok_terkait2'] .= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$get_id."'>".$key.";</a>";
						}elseif($get_ext_2[1] == "jpg" || $get_ext_2[1] == "jpeg" || $get_ext_2[1] == "png"){
							$form['dok_terkait2'] .= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$get_id."'>".$key.";</a>";
						}
					}else{
							$form['dok_terkait2'] .= "<a>".$key.";</a>";
					}
				}
			}else{
				$get_id = db::lookup("id",$module,"no_dokumen",$form['dok_terkait_lama']);
				$get_path = db::lookup("path_lama",$module,"no_dokumen",$form['dok_terkait_lama']);
				$get_ext_2 = explode(".", $get_path);
				if ($get_id) {
					if ($get_ext_2[1] != "xlsx" && $get_ext_2[1] != "jpg" && $get_ext_2[1] != "png" && $get_ext_2[1] != "xls") {
						$form['dok_terkait2'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$get_id."'>".$form['dok_terkait2']."</a>";
					}elseif($get_ext_2[1] == "jpg" || $get_ext_2[1] == "jpeg" || $get_ext_2[1] == "png"){
						$form['dok_terkait2'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$get_id."'>".$form['dok_terkait2']."</a>";
					}
				}else{
							$form['dok_terkait2'] = $form['dok_terkait2'].";";
				}
			}
		}


			admlib::get_component('html_view',
				array(
					"name"=>"dok_terkait_v",
					"readonly"=>"yes",
					"value"=>app::ov($form['dok_terkait2']),
					"validate"=>"required"
				)
			);
	?>
	<hr>
	<?php
}
			admlib::get_component('inputtext',
				array(
					"name"=>"judul",
					"readonly"=>"yes",
					"value"=>app::ov($form['name']),
					"validate"=>"required"
				)
			);
/* 			admlib::get_component('inputupload', 
				array(
					"name"=>"logo_crm", 
					"value"=> ($form['logo_crm'] != "" && file_exists($app['data_path'] ."/static/". $form['logo_crm']))?$app['_static'] ."/". $form['logo_crm']:null,
					"validate"=>"validate",
					"category"=>"image",
					"delete"=>"no",
					"size"=>"210px x 66px",
					"accept"=>"jpg|png|jpeg|gif|ico",
				)
			); */
			/*admlib::get_component('inputupload', 
				array(
					"name"=>"path", 
					"value"=> (!empty($form['path']) AND file_exists($app['pwebmin'] ."/assets/cmwi_file/". $form['path']))?$app['pwebmin'] ."/assets/cmwi_file/". $form['path']:null,
					"accept"=>"jpg|png|jpeg|gif|pdf|xls|docx|doc|xlsx|ico|txt",
					"validate"=>true
					// "value"=> $form['path'],
					// "filemedia"=>true
				)
			);*/
		/*	$rs["template_dokumen"] = db::get_record_select("name id, name","template_dokumen","status='active' ORDER BY name ASC");
			admlib::get_component('select2',
				array(
					"name"=>"template_dokumen",
					// "readonly"=>"yes",
					// "hide_label"=>"iya",
					// "style"=>"style='width: 45%;margin-left: 473px;position: relative;bottom: 43px;'",
					// "style_input"=>"width: 100%;",
					"validate"=>true,
					"value"=>$act=="add"?$app['me']['template_dokumen']:$form['template_dokumen'],
					"items"=>$rs['template_dokumen']
				)
			);*/
			
			$get_id = db::lookup("id",$module,"no_dokumen",$form['no_dokumen']);
			$get_path = db::lookup("path",$module,"no_dokumen",$form['no_dokumen']);
			$get_ext_2 = explode(".", $get_path);
			if ($get_id) {
				if ($get_ext_2[1] != "xlsx" && $get_ext_2[1] != "jpg" && $get_ext_2[1] != "png" && $get_ext_2[1] != "xls") {
					$form['no_dokumen'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$get_id."'>".$form['no_dokumen']."</a>";
				}elseif($get_ext_2[1] == "jpg" || $get_ext_2[1] == "jpeg" || $get_ext_2[1] == "png"){
					$form['no_dokumen'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$get_id."'>".$form['no_dokumen']."</a>";
				}
			}else{
						$form['no_dokumen'] = $form['no_dokumen'].";";
			}
/*			admlib::get_component('inputtext',
				array(
					"name"=>"no_dokumen",
					"readonly"=>"yes",
					// "readonly"=>"yes",
					// "value"=>($act=="add"?"IT-2-PK-".$no_doc:$form['no_dokumen']),
					"img_tooltip"=>$app['http']."/src/assets/imgs/static/img_no_dok.jpeg",
					"value"=>app::ov($form['no_dokumen']),
					// "validate"=>"required"
				)
			);*/
			admlib::get_component('html_view',
				array(
					"name"=>"no_dokumen",
					"readonly"=>"yes",
					// "readonly"=>"yes",
					// "value"=>($act=="add"?"IT-2-PK-".$no_doc:$form['no_dokumen']),
					"img_tooltip"=>$app['http']."/src/assets/imgs/static/img_no_dok.jpeg",
					"value"=>app::ov($form['no_dokumen']),
					// "validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"no_revisi",
					"readonly"=>"yes",
					"value"=>app::ov($form['no_revisi']),
					// "validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"tgl_revisi",
					"readonly"=>"yes",
					"value"=>(empty($form['tgl_revisi']) || $form['tgl_revisi'] =="0000-00-00" ?"":$form['tgl_revisi']),
					// "validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"tgl_terbit",
					"readonly"=>"yes",
					"value"=>app::ov($form['tgl_terbit']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"tgl_berlaku",
					"readonly"=>"yes",
					"value"=>app::ov($form['tgl_berlaku']),
					"validate"=>"required"
				)
			);
			admlib::get_component('inputtext',
				array(
					"name"=>"jml_hal",
					"readonly"=>"yes",
					// "type"=>"number",
					"value"=>app::ov($form['jml_hal']),
					"validate"=>"required"
				)
			);


		if ($form['dok_terkait']) {

			$array_terkait = explode(";", $form['dok_terkait']);
			if (count($array_terkait)>1) {
				$form['dok_terkait'] = "";
				foreach ($array_terkait as $key) {
					$get_id = db::lookup("id",$module,"no_dokumen",$key);
					$get_path = db::lookup("path",$module,"no_dokumen",$key);
					$get_ext_2 = explode(".", $get_path);
					// print_r($get_ext_2);
					if ($get_id) {
						if ($get_ext_2[1] != "xlsx" && $get_ext_2[1] != "jpeg" && $get_ext_2[1] != "jpg" && $get_ext_2[1] != "png" && $get_ext_2[1] != "xls") {
							$form['dok_terkait'] .= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$get_id."'>".$key.";</a>";
						}elseif($get_ext_2[1] == "jpg" || $get_ext_2[1] == "jpeg" || $get_ext_2[1] == "png"){
							$form['dok_terkait'] .= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$get_id."'>".$key.";</a>";
						}
					}else{
							$form['dok_terkait'] .= "<a>".$key.";</a>";
					}
				}
			}else{
				$get_id = db::lookup("id",$module,"no_dokumen",$form['dok_terkait']);
				$get_path = db::lookup("path",$module,"no_dokumen",$form['dok_terkait']);
				$get_ext_2 = explode(".", $get_path);
				if ($get_id) {
					if ($get_ext_2[1] != "xlsx" && $get_ext_2[1] != "jpg" && $get_ext_2[1] != "png" && $get_ext_2[1] != "xls") {
						$form['dok_terkait'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$get_id."'>".$form['dok_terkait']."</a>";
					}elseif($get_ext_2[1] == "jpg" || $get_ext_2[1] == "jpeg" || $get_ext_2[1] == "png"){
						$form['dok_terkait'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$get_id."'>".$form['dok_terkait']."</a>";
					}
				}else{
							$form['dok_terkait'] = $form['dok_terkait'].";";
				}
			}
		}


			admlib::get_component('html_view',
				array(
					"name"=>"dok_terkait",
					"readonly"=>"yes",
					"value"=>app::ov($form['dok_terkait']),
					"validate"=>"required"
				)
			);
			/* admlib::get_component('select',
				array(
					"name"=>"level",
					"value"=>app::ov($form['level']),
					// "items"=>[["id"=>1,"name"=>"1 - staff"],["id"=>2,"name"=>"2 - supervisor"],["id"=>3,"name"=>"3 - Manager"],["id"=>4,"name"=>"4 - General Manager"],["id"=>5,"name"=>"5 - Direksi"],["id"=>6,"name"=>6],["id"=>7,"name"=>7]],
					"items"=>[["id"=>1,"name"=>"1 - staff"],["id"=>2,"name"=>"2 - supervisor"],["id"=>3,"name"=>"3 - Manager"],["id"=>4,"name"=>"4 - General Manager"],["id"=>5,"name"=>"5 - Direksi"]],
					"validate"=>"required"
				)
			);
			$rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
			admlib::get_component('select',
				array(
					"name"=>"id_section",
					"value"=>app::ov($form['id_section']),
					"items"=>$rs['section']
				)
			); */
			// admlib::get_component('inputupload', 
			// 	array(
			// 		"name"=>"logo", 
			// 		"value"=> (isset($form['logo']) AND file_exists($app['logo_lib_path'] ."/". $form['logo']))?'/'. app::ov($form['logo']):null,
			// 		"filemedia"=>true
			// 	)
			// );
			admlib::get_component('submit',
					array(
							"id"=>(isset($id))?$id:"",
							"no_submit"=>"yes",
							// "cancel_link"=>$app['webmin'] ."/". admlib::$page_active['module'] .".mod",
							"status"=>$form['status'],
							"act"=>$act
					)
			);
	admlib::get_component('formend');
	?>
<script>
		/*$('#info_img [data-toggle="tooltip"]').tooltip({
		    animated: 'fade',
		    placement: 'bottom',
		    html: true
		});*/
		// alert("disini ");


	  $(document).ready(function() {
	  	$("#testing123").html('<a  target="_blank" style="font-size: 20px;" id="magilla" href="<?= $app['http']."/src/assets/imgs/static/img_no_dok.jpeg" ?>" title="" ><i class="test fa fa-info fa-fw" ></i></a>');
/*	   	$("#testing123 #magilla").tooltip({ content: '<img src="https://i.etsystatic.com/18461744/r/il/8cc961/1660161853/il_794xN.1660161853_sohi.jpg" />' }); */
	   	$("#testing123 #magilla").tooltip({ content: '<img width="550px" height="350px" src="<?= $app['http']."/src/assets/imgs/static/img_no_dok.jpeg" ?>" />' }); 

		});

		$('#template_dokumen').change(function() {
			// $("#g_function").show();
			var get_value = this.value;
			if (get_value) {
			// alert(get_value);
				$('#no_dokumen').val(get_value);			
			}else{
				$('#no_dokumen').val("");	
			}
			/*if (this.value == 'baru') {
				// alert("Allot Thai Gayo Bhai");
				$("#g_function").show();
              	$("#g_own").show();
			}else if (this.value == 'ada') {
			 	$("#g_function").hide();
              	$("#g_own").hide();
			}*/
		});
</script>
	<?php
admlib::display_block_footer();
?>
