<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('module'=>'module_file','caption'=>'Digital Document');
   $ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
else
    $ipaddress = 'UNKNOWN';
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	$option = ['sortable'=>true];
	admlib::display_block_grid("_module");
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$module = admlib::$page_active['module'];
	// $total = db::lookup('COUNT(id)', $module, '1=1');
	// app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
 

	if($ref)
	{
		#$q = "AND a.name LIKE '%". $ref ."%' ";
		#$q = "AND (a.tgl_daftar LIKE '%". $ref ."%' AND a.name LIKE '%". $ref ."%' AND a.name LIKE '%". $ref ."%' AND a.name LIKE '%". $ref ."%' AND a.name LIKE '%". $ref ."%' AND a.name LIKE '%". $ref ."%' AND a.name LIKE '%". $ref ."%') ";
        	$q = "AND (a.no_dokumen LIKE '%". $ref ."%' OR a.name LIKE '%". $ref ."%' OR a.no_revisi LIKE '%". $ref ."%')"; 
		//if (DateTime::createFromFormat('Y-m-d', $ref) !== FALSE) {
  			// it's a date
			//echo "disni";

	            #$q = "AND (a.tgl_revisi LIKE '%". $ref ."%' OR a.tgl_daftar LIKE '%". $ref ."%' OR a.tgl_berlaku LIKE '%". $ref ."%' OR a.tgl_terbit LIKE '%". $ref ."%') ";
	            $q .= "OR (a.tgl_revisi LIKE '%". $ref ."%' OR a.tgl_daftar LIKE '%". $ref ."%' OR a.tgl_berlaku LIKE '%". $ref ."%' OR a.tgl_terbit LIKE '%". $ref ."%') ";
		//}
	}
	 $sql = "SELECT a.*,a.id, a.name, a.path, a.status, b.name as created_by,a.created_at,c.name as updated_by,a.updated_at
				FROM ". $app['table'][$module] ." a 	
				LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id) 
				LEFT JOIN ". $app['view']['user'] ." c ON (a.updated_by=c.id) WHERE a.status ='active' $q ORDER BY a.no_dokumen ASC";
	db::query($sql, $rs2['row'], $nr2['row']);
	$total = $nr2['row'];
	app::set_default($page_size, (isset($all)?$total:10));

	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['module'] .".mod");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	// $columns = ['distributor','view','name'];
	$columns = [];
	if (($app['me']['code']!="it" && $app['me']['id'] != "1") && $app['me']['digital_form'] != "iya_df") {
		$columns = array_merge($columns,['info']);
	}
	if ($app['me']['code']=="it" || $app['me']['digital_form'] == "iya_df") {
		$columns = array_merge($columns,['distributor']);
	}
	#$columns = array_merge($columns,['view', 'tgl_daftar','no_dokumen','judul',"tgl_terbit","no_revisi","tgl_revisi","tgl_berlaku","dok_terkait"]);
	$columns = array_merge($columns,['view', 'tgl_daftar','no_dokumen','judul',"tgl_terbit","no_revisi","tgl_revisi","tgl_berlaku"]);
	// $columns = array_merge($columns,['confirm_done']);
	// $columns = ['name',"ext",'view'];
	while($row = db::fetch($rs['row']))
	{
		$row['tgl_daftar'] = date("d-m-Y", strtotime($row['tgl_daftar']));
		$row['tgl_terbit'] = date("d-m-Y", strtotime($row['tgl_terbit']));
		$row['tgl_revisi'] = date("d-m-Y", strtotime($row['tgl_revisi']));
		$row['tgl_berlaku'] = date("d-m-Y", strtotime($row['tgl_berlaku']));
		$row['judul'] = $row['name'];

		if ($app['me']['code']!="it") {
			// $row['info'] ='<a target="_blank" id="info" href="'.$app['webmin'] .'/'. admlib::$page_active['module'] .'.mod&act=info&id='.$row['id'].'" title="" ><i class="test fa fa-info fa-fw" ></i></a>';
			$row['info'] ='<a id="info" href="'.$app['webmin'] .'/'. admlib::$page_active['module'] .'.mod&act=info&id='.$row['id'].'" title="" ><i class="test fa fa-info fa-fw" ></i></a>';
			// $columns = array_merge($columns,['info']);
		}
		if ($row['dok_terkait']) {
			$array_terkait = explode(";", $row['dok_terkait']);
			if (count($array_terkait)>1) {
				$row['dok_terkait'] = "";
				foreach ($array_terkait as $key) {
					$get_id = db::lookup("id",$module,"no_dokumen",$key);
					$get_path = db::lookup("path",$module,"no_dokumen",$key);
					$get_ext_2 = explode(".", $get_path);
					// print_r($get_ext_2);
					if ($get_id) {
						if ($get_ext_2[1] != "xlsx" && $get_ext_2[1] != "jpg" && $get_ext_2[1] != "png" && $get_ext_2[1] != "xls") {
							$row['dok_terkait'] .= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$get_id."'>".$key.";</a>";
						}elseif($get_ext_2[1] == "jpg" || $get_ext_2[1] == "jpeg" || $get_ext_2[1] == "png"){
							$row['dok_terkait'] .= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$get_id."'>".$key.";</a>";
						}
					}else{
							$row['dok_terkait'] .= "<a>".$key.";</a>";
					}
				}
			}else{
				$get_id = db::lookup("id",$module,"no_dokumen",$row['dok_terkait']);
				$get_path = db::lookup("path",$module,"no_dokumen",$row['dok_terkait']);
				$get_ext_2 = explode(".", $get_path);
				if ($get_id) {
					if ($get_ext_2[1] != "xlsx" && $get_ext_2[1] != "jpg" && $get_ext_2[1] != "png" && $get_ext_2[1] != "xls") {
						$row['dok_terkait'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$get_id."'>".$row['dok_terkait']."</a>";
					}elseif($get_ext_2[1] == "jpg" || $get_ext_2[1] == "jpeg" || $get_ext_2[1] == "png"){
						$row['dok_terkait'] = "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$get_id."'>".$row['dok_terkait']."</a>";
					}
				}else{
							$row['dok_terkait'] = $row['dok_terkait'].";";
				}
			}
		}


		// $row['distributor'] 	= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=distributor&id=".$row['id']."'><i class='fa fa-address-book' aria-hidden='true'></i></a>";
		$row['distributor'] 	= "<a href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=distributor&id=".$row['id']."'><i class='fa fa-address-book' aria-hidden='true'></i></a>";

		$get_ext = explode(".", $row['path']);
		// $row['ext'] = $get_ext[1];
/*		if ($get_ext[1] != "xlsx" && $get_ext[1] != "jpg" && $get_ext[1] != "png" && $get_ext[1] != "xls" && $get_ext[1] != "xlsx") {*/
		if ($get_ext[1] != "xlsx" && $get_ext[1] != "jpg" && $get_ext[1] != "jpeg" && $get_ext[1] != "png" && $get_ext[1] != "png" && $get_ext[1] != "xls") {
		// $row['view'] 	= "<a href='file:".$app['pwebmin'] ."/assets/cmwi_file/".$row['path']."'><i class='fa fa-eye' aria-hidden='true'></i></a>";
			$row['view'] 	= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_file&id=".$row['id']."'><i class='fa fa-eye' aria-hidden='true'></i></a>";
		}elseif($get_ext[1] == "jpg" || $get_ext[1] == "jpeg" || $get_ext[1] == "png"){
			// $row['view'] 	= "<a target='_blank' href='".$app['http']."/src/assets/cmwi_file/".$row['path']."'><i class='fa fa-eye' aria-hidden='true'></i></a>";
			$row['view'] 	= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_gambar&id=".$row['id']."'><i class='fa fa-eye' aria-hidden='true'></i></a>";
		}/*elseif($get_ext[1] == "xlsx"){
			$row['view'] 	= "<a target='_blank' href='".$app['webmin'] ."/". admlib::$page_active['module'] .".mod&act=view_xlsx&id=".$row['id']."'><i class='fa fa-eye' aria-hidden='true'></i></a>";
		}*/
		$results[] 		= $row;
	}
	// $sortable = true;
	// $option = ['sortable'=>true];
	$option = ['status'=>true];
	if (($app['me']['code'] != "it" && $app['me']['id'] != "1")  && $app['me']['digital_form'] != "iya_df") {
		$option = array_merge($option,['no_edit'=>true]);
	}
	$unpost ="yes";
	$unmodify ="yes";
	// $option = array_merge($option,['no_chk'=>"iya","no_edit"=>"iya"]);
	include $app['pwebmin'] ."/include/blk_list.php";
	// include $app['pwebmin'] ."/include/blk_list_report.php";
	exit;
endif;
/*******************************************************************************
* Action : view_file
*******************************************************************************/
if ($act == "view_file"):
	admlib::validate('DSPL');
	$module = admlib::$page_active['module'];
	// admlib::validate('UPDT');
	// if ( $status == "active" ):
	// 	$statusnya = "inactive";
	// elseif ( $status == "inactive" ):
	// 	$statusnya = "active";
	// endif;
	// $sql = "UPDATE ".$app['table'][$module]."
	// 		SET status = '$statusnya'
	// 		WHERE id = '$id'";
	// db::qry($sql);
	// msg::set_message('success', app::getliblang('update'));
	$data = db::get_record($module,"id",$id);
	db::add_log_doc($app['me']['id'],$id,"VIEW_DOC",$ipaddress,"");
	include "dsp_view_file.php";
	exit;
endif;
/*******************************************************************************
* Action : view_gambar
*******************************************************************************/
if ($act == "view_gambar"):
	admlib::validate('DSPL');
	$module = admlib::$page_active['module'];
	// admlib::validate('UPDT');
	// if ( $status == "active" ):
	// 	$statusnya = "inactive";
	// elseif ( $status == "inactive" ):
	// 	$statusnya = "active";
	// endif;
	// $sql = "UPDATE ".$app['table'][$module]."
	// 		SET status = '$statusnya'
	// 		WHERE id = '$id'";
	// db::qry($sql);
	// msg::set_message('success', app::getliblang('update'));
	$data = db::get_record($module,"id",$id);

	db::add_log_doc($app['me']['id'],$id,"VIEW_DOC",$ipaddress,"");
	// include "dsp_view_file.php";
	// header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
	header("location: ".$app['http']."/src/assets/cmwi_file/".$data['path']);
	exit;
endif;
/*******************************************************************************
* Action : view_file
*******************************************************************************/
if ($act == "view_file"):
	$module = admlib::$page_active['module'];
	// admlib::validate('UPDT');
	// if ( $status == "active" ):
	// 	$statusnya = "inactive";
	// elseif ( $status == "inactive" ):
	// 	$statusnya = "active";
	// endif;
	// $sql = "UPDATE ".$app['table'][$module]."
	// 		SET status = '$statusnya'
	// 		WHERE id = '$id'";
	// db::qry($sql);
	// msg::set_message('success', app::getliblang('update'));
	$data = db::get_record($module,"id",$id);
	db::add_log_doc($app['me']['id'],$id,"VIEW_DOC",$ipaddress,"");
	include "dsp_view_file_excel.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	$module = admlib::$page_active['module'];
	form::init();
	if ($step == 1):
		form::populate($form);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		$validate ='p_name,p_no_dokumen,p_tgl_berlaku';
		form::serialize_form();
		form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=add&error=1");
				exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_name');
		$reorder = db::lookup("max(reorder)",$module,"1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		
		// if ($p_path_del){
		// 	@unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);	
		// 	$data['path'] = null;
		// }else{
			if ($p_path_size > 0):
				// save_file_rename
				// $file = file::save_picture('p_path',$app['pwebmin'] ."/assets/cmwi_file/", 'path');
				$file = file::save_file_rename('p_path',$app['pwebmin'] ."/assets/cmwi_file/", $p_no_dokumen);
				if (!$err_path):
					@unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);
					$data['path'] = $file;
				endif;
			endif;	
		// }
		$no_doc = db::lookup("max(no)",$module,"1=1");
		$id  = rand(1, 100).date("dmYHis");
/*		$sql = "insert into ".$app['table'][$module]."
				(id, name, path, created_by) values
				('$id','$p_name', '".$data['path']."', '". $app['me']['id'] ."')";*/
		$no_doc = $no_doc +1;
		$sql = "insert into ".$app['table'][$module]."
				(id, tgl_daftar, name, path, no_dokumen, no_revisi, tgl_terbit, tgl_revisi, tgl_berlaku, jml_hal, dok_terkait, created_by, no) values
				('$id', '$p_tgl_daftar','$p_name', '".$data['path']."', '$p_no_dokumen', '$p_no_revisi', '$p_tgl_terbit', '$p_tgl_revisi', '$p_tgl_berlaku', '$p_jml_hal', '$p_dok_terkait', '". $app['me']['id'] ."', '$no_doc')";
		// db::qry($sql);
		if (db::qry($sql)) {
			// db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
			db::add_log_doc($app['me']['id'],$id,"ADD_DOC",$ipaddress,"");
		}
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : distributor
*******************************************************************************/
if ($act == "distributor"):
	admlib::validate('CRT');
	$module = admlib::$page_active['module'];
	form::init();
	if ($step == 1):
		form::populate($form);
		$form = db::get_record_filter("*",$module, "id", $id);
	    include "dsp_form_distri.php";
		exit;
	endif;
	if ($step == 2):
		// $validate ='p_name';
		form::serialize_form();
		// form::validate('empty',$validate);
		if (form::is_error()):
				msg::build_msg();
				header("location: ". admlib::$page_active['module'] .".mod&act=distributor&error=1");
				exit;
		endif;
		// $id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_name');
		$reorder = db::lookup("max(reorder)",$module,"1=1");
		if ($reorder==0){ $reorder = 1; }else{ $reorder = $reorder+1; }
		
		// if ($p_path_del){
		// 	@unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);	
		// 	$data['path'] = null;
		// }else{
		/*	if ($p_path_size > 0):
				$file = file::save_picture('p_path',$app['pwebmin'] ."/assets/cmwi_file/", 'path');
				if (!$err_path):
					@unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);
					$data['path'] = $file;
				endif;
			endif;	*/
		// }
		// $id  = rand(1, 100).date("dmYHis");
/*		$sql = "insert into ".$app['table'][$module]."
				(id, name, path, created_by) values
				('$id','$p_name', '".$data['path']."', '". $app['me']['id'] ."')";*/

		$sql = "update ". $app['table'][$module] ."
				set tgl_distribusi = '$p_tgl_distribusi',
					id_section = '$p_id_section_penerbit',
					id_section_terkait = '$p_id_section_terkait',
					id_section_distri = '$p_id_section_distri',
					ket_distri = '$p_ket_distri',
					jum_distri = '$p_jum_distri',
					id_section_terkendali = '$p_id_section_terkendali',
					ket_terkendali = '$p_ket_terkendali',
					jum_terkendali = '$p_jum_terkendali',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
				
		// db::qry($sql);
		if (db::qry($sql)) {
			// db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
			db::add_log_doc($app['me']['id'],$id,"distributor",$ipaddress,"");
		}
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "info"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	form::init();
	// print_r($_REQUEST);
	// exit;
	$form = db::get_record_filter("*",$module, "id", $id);
	// print_r($form);
	form::populate($form);
	include "dsp_view.php";
	exit;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	$module = admlib::$page_active['module'];
	form::init();
	if ($step == 1):
		$form = db::get_record_filter("*",$module, "id", $id);
		// print_r($form);
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
if ($step == 2):
		form::serialize_form();
		form::validate('empty','p_name,p_no_dokumen,p_tgl_berlaku');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['module'] .".mod&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$data = db::get_record("*",$module, "id", $id);
		// print_r($_FILES);
		// exit;
/*		if ($p_path_del){
			// @unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);	
			$data['path'] = null;
		}else{
			if ($p_path_size > 0):
				// $file = file::save_picture('p_path',$app['pwebmin'] ."/assets/cmwi_file/", 'path');
				#@unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);
				$file = file::save_file_rename('p_path',$app['pwebmin'] ."/assets/cmwi_file/", $p_no_dokumen);
				if (!$err_path):
					// @unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);
					$data['path'] = $file;
				endif;
			endif;	
		}*/
		if ($p_path_del){
			@unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);	
			$data['path'] = null;
		}else{
			if ($p_path_size > 0):
				// $file = file::save_picture('p_path',$app['pwebmin'] ."/assets/cmwi_file/", 'path');
				@unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);
//echo "disini";
//exit;
				$file = file::save_file_rename('p_path',$app['pwebmin'] ."/assets/cmwi_file/", $p_no_dokumen);
				if (!$err_path):
					//@unlink($app['pwebmin'] ."/assets/cmwi_file/". $data['path']);
					$data['path'] = $file;
				endif;
			endif;	
		}

		$data2 = db::get_record($module, "id", $id);
		$edit_before= "";
		$edit_after = "";

		if ($data2['name'] !=$p_name) {
			$edit_before .= "name : ".$data2['name'].";";
			$edit_after .= "name : ".$p_name.";";
		}
		if ($data2['path'] !=$p_path) {
			$edit_before .= "path : ".$data2['path'].";";
			$edit_after .= "path : ".$data['path'].";";
		}
		// if ($data['name'] !=$p_name) {
		// 	$edit_before .= $data['name'].";";
		// 	$edit_after .= $p_name.";";
		// }
		$edit_before = rtrim($edit_before,";");
		$edit_after  = rtrim($edit_after,";");
		app::mq_encode('p_name,p_code');

		// $sql = "insert into ".$app['table'][$module]."
		// 		(id, name, path, no_dokumen, no_revisi, tgl_terbit, tgl_revisi, tgl_berlaku, jml_hal, dok_terkait, created_by, no) values
		// 		('$id','$p_name', '".$data['path']."', '$p_no_dokumen', '$p_no_revisi', '$p_tgl_terbit', '$p_tgl_revisi', '$p_tgl_berlaku', '$p_jml_hal', '$p_dok_terkait', '". $app['me']['id'] ."', '$no_doc')";

		$sql = "update ". $app['table'][$module] ."
				set name = '$p_name',
					path = '".$data['path']."',
					no_revisi = '$p_no_revisi',
					tgl_daftar = '$p_tgl_daftar',
					tgl_terbit = '$p_tgl_terbit',
					tgl_revisi = '$p_tgl_revisi',
					tgl_berlaku = '$p_tgl_berlaku',
					jml_hal = '$p_jml_hal',
					dok_terkait = '$p_dok_terkait',
					name_lama = '".$data2['name']."',
					path_lama = '".$data2['path']."',
					no_revisi_lama = '".$data2['no_revisi']."',
					tgl_daftar_lama = '".$data2['tgl_daftar']."',
					tgl_terbit_lama = '".$data2['tgl_terbit']."',
					tgl_revisi_lama = '".$data2['tgl_revisi']."',
					tgl_berlaku_lama = '".$data2['tgl_berlaku']."',
					jml_hal_lama = '".$data2['jml_hal']."',
					dok_terkait_lama = '".$data2['dok_terkait']."',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		// db::qry($sql);		
		if (db::qry($sql)) {
			// db::add_notif($id, $code, $app['me']['id'], "Form Permintaan Hardware Software telah di buat silahkan Approve Form ini", $id_section, $link, "", $level, $module);
			db::add_log_doc($app['me']['id'],$id,"UPD_DOC",$ipaddress,"",$edit_before,$edit_after);
		}
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	$module = admlib::$page_active['module'];
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table'][$module]."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
	exit;
endif;
/*******************************************************************************
* Action : reorder
*******************************************************************************/
if ($act == "reorder"):
	$module = admlib::$page_active['module'];
	admlib::validate('UPDT');
	if(count($reorder) > 0)
	{
		foreach ($reorder as $idval) { $min++;
			$sql = "UPDATE ".$app['table'][$module]." SET reorder = " . $min . " WHERE id = '" . $idval ."'";
			db::qry($sql);
		}
		echo true;
		exit;
	}
	echo false;
	exit;
endif;
/*******************************************************************************
* Action : del;
*******************************************************************************/
if ($act == "delete"):
	$module = admlib::$page_active['module'];
	admlib::validate('DEL');
	if($step == 1)
	{
			$items = implode("','", $p_del);
			$sql = "SELECT id,name as title FROM ". $app['table'][$module] ." WHERE id IN ('". $items ."')";
			db::query($sql, $rs['row'], $nr['row']);
			include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);
		if(count($p_id) > 1){	
			foreach ($p_id as $value2) {
				$get_file = db::lookup("path",$module,"id",$value2);
				$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $value2 ."')";
				if(db::qry($sql)){
					@unlink($app['pwebmin'] ."/assets/cmwi_file/". $get_file);
					db::add_log_doc($app['me']['id'],$value2,"DEL_DOC",$ipaddress,"");
				}
			} 
		}else{
			$get_file = db::lookup("path",$module,"id",$delid);
			$sql 	= "DELETE FROM ". $app['table'][$module] ." WHERE `id` IN ('". $delid ."')";
			// db::qry($sql);
			if(db::qry($sql)){
				@unlink($app['pwebmin'] ."/assets/cmwi_file/". $get_file);
				db::add_log_doc($app['me']['id'],$id,"DEL_DOC",$ipaddress,"");
			}
		}
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['module'] .".mod");
		exit;
	}
endif;
?>
