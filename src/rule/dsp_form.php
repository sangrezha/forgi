<?php
    admlib::display_block_header();
    admlib::get_component('formstart');
    // var_dump($error);
    // if(count($error) > 0){
    //     foreach($error as $err){
    //         if(isset($err) AND $err){
    //             echo msg::display_notif_msg("error", app::getliblang('error')." ". $err );
    //         }
    //     }
    // }
?>
	<script type="text/javascript">
	  $(document).ready(function() {
		$('.checkall').change(function(){
			var checkitem 	= $('.' + $(this).attr('var'));
			if(this.checked)
			{
				checkitem.prop('checked', true);
			}
			else
			{
				checkitem.prop('checked', false);
			}
		});
	  });
	</script>
	<div class="row">
		<div class="col-md-5">
			<?php
				admlib::get_component('inputtext',
					array(
						"name"=>"name",
						"value"=>app::ov($form['name']),
						"validate"=>"required"
					)
				);
				admlib::get_component('submit',
					array(
						"id"=>isset($id)?$id:null,
						"act"=>$act
					)
				);
			?>
		</div>
		<div class="col-md-7">
			<h1><?php echo app::getliblang('previlage'); ?></h1>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php $privilege = privilege::init();
						foreach ($privilege as $num => $role) {
							$rtotal = str_replace(" ","",$role['title']);
					?>
						<div class="panel panel-warning">
							<div class="panel-heading" role="tab" style="background-color:#e09502;color:#fff;">
								<h4 class="panel-title pull-left">
									<a role="button" data-toggle="collapse" data-parent="#<?php echo $rtotal; ?>" href="#<?php echo $rtotal; ?>" aria-expanded="true" aria-controls="collapseOne">
									<?php echo $role['title']; ?>
									</a>
								</h4>
								<input type="checkbox" class="pull-right checkall" var="<?php echo $rtotal ?>"/>
								<span class="clearfix">&nbsp;</span>
							</div>
							<div id="<?php echo $rtotal; ?>" class="panel-collapse collapse in" role="tabpanel">
								<ul class="list-group">
									<?php
										if(isset($role['items']) AND count($role['items'])> 0){
											foreach ($role['items'] as $item) {
									?>
												<li class="list-group-item">
													<?php echo $item['title']; ?>
													<div class="row">
														<div class="col-md-5">&nbsp;</div>
														<div class="col-md-5">
															<ul class="list-group">
																<?php
																	if(isset($item['acc'])){
																		foreach ($item['acc'] as $kacc => $vacc) {
																			$chk = null;
																			if(isset($id))
																			{
																				$chk = db::lookup('rule', 'rule_det', 'id_rule="'. $id .'" AND module="'. (isset($item['module'])?$item['module']:$item['system']) .'" AND rule="'. $vacc .'"');
																			}

																?>
																			<li class="list-group-item">
																				<input type="checkbox" class="checkitem <?php echo $rtotal ?>" name="p_acc[<?php echo $role['title']; ?>][<?php echo (isset($item['module'])?$item['module']:$item['system']); ?>][]" value="<?php echo $vacc; ?>" <?php echo ($chk==$vacc?"checked":null); ?>/>
																				<?php echo $kacc; ?>
																			</li>
																<?php
																		}
																	}
																?>
															</ul>
														</div>
													</div>
												</li>
									<?php
											}
										}
									?>
								</ul>
							</div>
						</div>
				<?php } ?>
			</div>
		</div>
	</div>
<?php
admlib::get_component('formend');
admlib::display_block_footer();
?>
