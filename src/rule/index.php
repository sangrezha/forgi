<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('system'=>'rule','caption'=>'Rule');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'rule', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.*,b.name as created_by,c.name as updated_by FROM ". $app['table']['rule'] ." a
	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	$q ORDER BY a.created_at DESC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['system'] .".do");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['name'];
	while($row = db::fetch($rs['row']))
	{

		$results[] = $row;
	}
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
		$sql_lang = "SELECT id,name,alias FROM ". $app['table']['lang'] ." WHERE status='active' ORDER BY created_at";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		$rule = msg::get_message('rule_empty');
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_name');
		if( count($p_acc) == 0 )
		{
			msg::set_message('rule_empty', app::getliblang('rule_empty'));
			form::set_error(1);
		}
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=add&error=1");
			exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_name');
		$sql = "insert into ".$app['table']['rule']."
				(id, name, created_by, created_at) values
				('$id', '$p_name', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		if( isset($p_acc) AND count($p_acc) > 0 )
		{
			foreach ($p_acc as $kroot => $vroot)
			{
				foreach ($vroot as $kacc => $vacc)
				{
					if( count($vacc) > 0 )
					{
						foreach ($vacc as $krole => $vrole)
						{
							$_id = $krole.rand(10, 100).rand(10, 100).date("dmYHis");
							$_sql = "insert into ". $app['table']['rule_det'] ."
									(id, id_rule, module, rule, menu) values
									('$_id', '$id', '$kacc', '$vrole', '$kroot')";
							db::qry($_sql);
						}
					}
				}
			}
		}
		msg::set_message('success', app::getliblang('create'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("rule", "id", $id);
		form::populate($form);
		$sql_lang = "SELECT id,name,alias FROM ". $app['table']['lang'] ." WHERE status='active' ORDER BY created_at";
		db::query($sql_lang, $rs['lang'], $nr['lang']);
		$name = msg::get_message('name');
		$rule = msg::get_message('rule_empty');
		if($name)
		{
			$rule = null;
		}
		$app['errors'] = array($name,$rule);

		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_name');
		if( count($p_acc) == 0 )
		{
			msg::set_message('rule_empty', app::getliblang('rule_empty'));
			form::set_error(1);
		}
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=add&error=1&id=" . $id);
			exit;
		endif;

		app::mq_encode('p_name');
		$sql = "update ". $app['table']['rule'] ."
				set name = '$p_name',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		db::qry($sql);
		if( isset($p_acc) AND count($p_acc) > 0 )
		{
			$sql_del = "DELETE FROM ". $app['table']['rule_det'] ." WHERE id_rule='$id'";
			db::qry($sql_del);
			foreach ($p_acc as $kroot => $vroot)
			{
				foreach ($vroot as $kacc => $vacc)
				{
					if( count($vacc) > 0 )
					{
						foreach ($vacc as $krole => $vrole)
						{
							$_id = $krole.rand(10, 100).rand(10, 100).date("dmYHis");
							$_sql = "insert into ". $app['table']['rule_det'] ."
									(id, id_rule, module, rule, menu) values
									('$_id', '$id', '$kacc', '$vrole', '$kroot')";
							db::qry($_sql);
						}
					}
				}
			}
		}
		msg::set_message('success', app::getliblang('update'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['rule']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	msg::build_msg(1);
	header("location: ". admlib::$page_active['system'] .".do");
endif;
/*******************************************************************************
* Action : del
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
		$items = implode("','", $p_del);
		$sql = "SELECT id,name as title FROM ". $app['table']['rule'] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);

		$sql_del_rule	= "DELETE FROM ". $app['table']['rule_det'] ." WHERE `id_rule` IN ('". $delid ."')";
		db::qry($sql_del_rule);
		$sql 	= "DELETE FROM ". $app['table']['rule'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		// echo $sql;exit;
		msg::set_message('success', app::getliblang('delete'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	}
endif;
/*******************************************************************************
* Action : duplicate
*******************************************************************************/
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table']['rule'] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$id 		= rand(1, 100).date("dmYHis");
			$p_name 	= $row['name'];
			$p_satus 	= $row['status'];

			app::mq_encode('p_name');
			$sql_insert = "INSERT INTO ". $app['table']['rule'] ."
				(id, name, status, created_by, created_at) values
				('$id', '$p_name', 'inactive', '". $app['me']['id'] ."', now())";
				// echo $sql_insert;
			db::qry($sql_insert);

			$sql_det = "SELECT * FROM ". $app['table']['rule_det'] ." WHERE id_rule='". $row['id'] ."'";
			db::query($sql_det, $rs['row_det'], $nr['row_det']);
			if( $nr['row_det'] > 0 )
			{
			// echo $id;
				$num = 0;
				while($rowdet = db::fetch($rs['row_det']))
				{ $num++;
					$iddet = $num.rand(1, 100).rand(1, 100).date("dmYHis");
					$sql_insert_det = "INSERT INTO ". $app['table']['rule_det'] ."
						(id, id_rule, name, module, rule, menu) values
						('$iddet', '". $id ."', '". $rowdet['name'] ."', '". $rowdet['module'] ."', '". $rowdet['rule'] ."','". $rowdet['menu'] ."')";
						// echo $sql_insert_det."<br>";
					db::qry($sql_insert_det);
				}
				// exit;
			}
		}
		msg::set_message('success', app::getliblang('create'));
	}
	else
	{
		msg::set_message('error', app::getliblang('notfound'));
	}
	header("location: ". admlib::$page_active['system'] .".do");
	exit;
endif;
?>
