<?php
/*******************************************************************************
* Filename : index.php
* Description : site configuration modul
*******************************************************************************/
include "../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');
## START #######################################################################
app::set_default($act, 'view');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
/*******************************************************************************
* Action : change
*******************************************************************************/
admlib::$page_active = array('system'=>'preference','caption'=>'Preference');
if ($act == "view"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("configuration", "id", 1);
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_title_cms');
		// if (form::is_error()):
		// 	msg::build_msg();
		// 	header("location: ". admlib::$page_active['system'] .".do&act=view&error=1");
		// 	exit;
		// endif;	
		// $version = "";
		// if($app['me']['id'] == 1){
		// 	$version = "beacukai_version = '$p_beacukai_version',
		// 				pelabuhan_version = '$p_pelabuhan_version',";
		// }
		// $data = db::get_record("configuration", "id", 1);
		// if ($p_logo_crm_del){
		// 	@unlink($app['data_path'] ."/static/". $data['logo_crm']);	
		// 	$data['logo_crm'] = null;
		// }else{
		// 	if ($p_logo_crm_size > 0):
		// 		$file = file::save_picture('p_logo_crm', $app['data_path']."/static", 'logo');
		// 		if (!$err_logo_cms):
		// 			@unlink($app['data_path'] ."/static/". $data['logo_crm']);
		// 			$data['logo_crm'] = $file;
		// 		endif;
		// 	endif;	
		// }
		
		// if ($p_icon_crm_del){
		// 	@unlink($app['data_path'] ."/static/". $data['icon_crm']);	
		// 	$data['icon_crm'] = null;
		// }else{
		// 	if ($p_icon_crm_size > 0):
		// 		$file = file::save_picture('p_icon_crm', $app['data_path']."/static", 'icon');
		// 		if (!$err_icon_cms):
		// 			@unlink($app['data_path'] ."/static/". $data['icon_crm']);
		// 			$data['icon_crm'] = $file;
		// 		endif;
		// 	endif;	
		// }
		
		// $passwordhash = md5($smtp_pass);
		app::mq_encode('p_title_cms');
/*		$sql = "UPDATE ".$app['table']['configuration']."
				SET title_crm 	 = '$p_title_crm',
					logo_crm	 = '". $data['logo_crm'] ."',
					icon_crm	 = '". $data['icon_crm'] ."',
					smtp_host	 = '$p_smtp_host',
					smtp_user	 = '$p_smtp_user',
					smtp_pass	 = '$p_smtp_pass',
					smtp_port	 = '$p_smtp_port',
					smtp_secure	 = '$p_smtp_secure',
					$version
					update_by 	 = '". $app['me']['id'] ."',
					update_at 	 = now()
				where id = '1'";*/
		$sql = "UPDATE ".$app['table']['configuration']."
				SET title_cms = '$p_title_cms',
					desk_form_cpassword = '$p_desk_form_cpassword',
					desk_form_acc_folder = '$p_desk_form_acc_folder',
					desk_form_tmp_access = '$p_desk_form_tmp_access',
					desk_form_perubahan_si = '$p_desk_form_perubahan_si',
					desk_form_abnormal_si = '$p_desk_form_abnormal_si',
					desk_form_get_soft = '$p_desk_form_get_soft',
					desk_form_recovery_data = '$p_desk_form_recovery_data',
					desk_form_hardware_software = '$p_desk_form_hardware_software',
					desk_form_pengajuan_user = '$p_desk_form_pengajuan_user',
					desk_form_register_device = '$p_desk_form_register_device',
					logo	  = '$p_logo'
				where id = '1'";
		// echo $sql;exit;
		db::qry($sql);
		msg::set_message('success', app::getliblang('update'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do&act=view");
		exit;
	endif;
endif;
?>