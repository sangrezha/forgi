<?php 
admlib::display_block_header();
admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		admlib::get_component('inputtext', 
			array(
				"name"=>"title_cms", 
				"value"=>app::ov($form['title_cms']),
				"validate"=>"required"
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_cpassword", 
				"value"=>app::ov($form['desk_form_cpassword'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_acc_folder", 
				"value"=>app::ov($form['desk_form_acc_folder'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_tmp_access", 
				"value"=>app::ov($form['desk_form_tmp_access'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_perubahan_si", 
				"value"=>app::ov($form['desk_form_perubahan_si'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_abnormal_si", 
				"value"=>app::ov($form['desk_form_abnormal_si'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_get_soft", 
				"value"=>app::ov($form['desk_form_get_soft'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_recovery_data", 
				"value"=>app::ov($form['desk_form_recovery_data'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_hardware_software", 
				"value"=>app::ov($form['desk_form_hardware_software'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_pengajuan_user", 
				"value"=>app::ov($form['desk_form_pengajuan_user'])
			)
		);
		admlib::get_component('textarea', 
			array(
				"name"=>"desk_form_register_device", 
				"value"=>app::ov($form['desk_form_register_device'])
			)
		);
		// admlib::get_component('inputtext', 
		// 	array(
		// 		"name"=>"logo", 
		// 		"value"=>app::ov($form['logo']),
		// 		"validate"=>"required"
		// 	)
		// );
		admlib::get_component('inputupload', 
			array(
				"name"=>"logo", 
				// "value"=> (isset($form['logo']) AND file_exists($app['logo_lib_path'] ."/". $form['logo']))?'/'. app::ov($form['logo']):null,
				"value"=> $form['logo'],
				"filemedia"=>true
			)
		);
		// admlib::get_component('inputupload', 
		// 	array(
		// 		"name"=>"logo_crm", 
		// 		"value"=> ($form['logo_crm'] != "" && file_exists($app['data_path'] ."/static/". $form['logo_crm']))?$app['_static'] ."/". $form['logo_crm']:null,
		// 		"validate"=>"validate",
		// 		"category"=>"image",
		// 		"delete"=>"no",
		// 		"size"=>"210px x 66px",
		// 		"accept"=>"jpg|png|jpeg|gif|ico",
		// 	)
		// );
		// admlib::get_component('inputupload', 
		// 	array(
		// 		"name"=>"icon_crm", 
		// 		"value"=> ($form['icon_crm'] != "" && file_exists($app['data_path'] ."/static/". $form['icon_crm']))?$app['_static'] ."/". $form['icon_crm']:null,
		// 		"validate"=>"validate",
		// 		"category"=>"image",
		// 		"delete"=>"no",
		// 		"size"=>"32px x 32px",
		// 		"accept"=>"jpg|png|jpeg|gif|ico",
		// 	)
		// );
/*		admlib::get_component('inputtext', 
			array(
				"name"=>"smtp_host", 
				"value"=>app::ov($form['smtp_host']),
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext', 
			array(
				"type"=>"text", 
				"name"=>"smtp_user", 
				"value"=>app::ov($form['smtp_user']),
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext', 
			array(
				"type"=>"password", 
				"name"=>"smtp_pass", 
				"value"=>app::ov($form['smtp_pass']),
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext', 
			array(
				"name"=>"smtp_port", 
				"value"=>app::ov($form['smtp_port']),
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext', 
			array(
				"name"=>"smtp_secure", 
				"value"=>app::ov($form['smtp_secure']),
				"validate"=>"required"
			)
		);*/
		// if($app['me']['id'] == 1){
			
		// admlib::get_component('inputtext', 
		// 	array(
		// 		"type"=>"text", 
		// 		"name"=>"beacukai_version", 
		// 		"value"=>app::ov($form['beacukai_version']),
		// 		"validate"=>"required"
		// 	)
		// );
		// admlib::get_component('inputtext', 
		// 	array(
		// 		"type"=>"text", 
		// 		"name"=>"pelabuhan_version", 
		// 		"value"=>app::ov($form['pelabuhan_version']),
		// 		"validate"=>"required"
		// 	)
		// );
		// }
		admlib::get_component('submit', 
			array(
				"id"=>$form['id'], 
				"act"=>$act
			)
		);
admlib::get_component('formend');

admlib::display_block_footer(); 
?>
		