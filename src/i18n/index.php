<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');
## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('system'=>'i18n','caption'=>'liblang');
// echo $act;exit;
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'i18n', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = "";
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.*,a.name as title,b.name as postby,c.name as modifyby,d.name as language FROM ". $app['table']['i18n'] ." a
	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	$q ORDER BY a.created_at DESC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['system'] .".do&act=". $act);
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['title','alias'];
	$numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	if($numlang > 1)
	{
		$columns = array_merge($columns,['language']);
	}
	while($row = db::fetch($rs['row']))
	{
		$row['alias'] 	= $row['alias'];
		$results[] 		= $row;
	}
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_name,p_alias,p_lang');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=add&error=1");
			exit;
		endif;
		$id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_name');
		$sql = "insert into ".$app['table']['i18n']."
				(id, name, alias, lang, created_by, created_at) values
				('$id', '$p_name', '$p_alias', '$p_lang', '". $app['me']['id'] ."', now())";
		// echo $sql;exit;
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/

if ($act == "edit"):

	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("i18n", "id", $id);
		form::populate($form);
	// echo "sahjdgsdjhas";exit;
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_name,p_alias,p_lang');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=edit&error=1&id=" . $id);
			exit;
		endif;
		app::mq_encode('p_name,p_alias');
		$sql = "update ". $app['table']['i18n'] ."
				set name = '$p_name',
				alias = '$p_alias',
				lang = '$p_lang',
				updated_by = '". $app['me']['id'] ."',
				updated_at = now()
			where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('update'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['i18n']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	msg::build_msg(1);
	header("location: ". admlib::$page_active['system'] .".do");
endif;
/*******************************************************************************
* Action : del
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
		$items = implode("','", $p_del);
		$sql = "SELECT id,name as title FROM ". $app['table']['i18n'] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);
		$sql 	= "DELETE FROM ". $app['table']['i18n'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		msg::set_message('success', app::getliblang('delete'));
		msg::build_msg(1);
		header("location: ". admlib::$page_active['system'] .".do");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table']['i18n'] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$id 		= rand(1, 100).date("dmYHis");
			$p_lang 	= $row['lang'];
			$p_name 	= $row['name'];
			$p_alias 	= $row['alias'];
			$created_at = $row['created_at'];

			app::mq_encode('p_name');
			$sql_insert = "INSERT INTO ". $app['table']['i18n'] ."
				(id, name, alias, lang, status, created_by, created_at) values
				('$id', '$p_name', '$p_alias', '$p_lang', 'inactive', '". $app['me']['id'] ."', '$created_at')";
			db::qry($sql_insert);

		}
		msg::set_message('success', app::getliblang('create'));
	}
	else
	{
		msg::set_message('error', app::getliblang('notfound'));
	}
	header("location: ". admlib::$page_active['system'] .".do");
	exit;
endif;
?>
