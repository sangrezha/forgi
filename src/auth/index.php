<?php
  include "../../application.php";
  app::load_lib('url', 'msg', 'form','admlib');
  ## START #######################################################################
  app::set_default($act, "");
  db::connect();
  
   $ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
else
    $ipaddress = 'UNKNOWN';
// if(isset($_SESSION[$app['session']]))
//    header("location: ". $app['webmin'] ."/dashboard.do");
// else
//   header("location: ". $app['webmin'] ."/auth.do");
  $config = db::get_record("configuration", "id", 1);
  if (isset($act) && $act=="login"){
  	form::init();
  	form::serialize_form();
  	form::validate('empty', 'p_username,p_password');
  	if (form::is_error()){
			msg::set_message('error', app::getliblang('login_invalid'));
    		form::set_error(1);
    		header("location: ". $app['webmin'] ."/auth.do&error=1");
    		exit;
  	}
  	if( db::anti_sql_injection($_POST['p_username']) and db::anti_sql_injection($_POST['p_password']))
  	{
		$passwordhash = md5(serialize($p_password));
		$sql = "SELECT * FROM " .$app['view']['user'] ." WHERE (username = '$p_username' OR email = '$p_username') AND password = '$passwordhash' AND `status` = 'active' LIMIT 1";
		db::query($sql, $rs['login'], $nr['login']);
		if($nr['login']){
			form::reset();
            // $_SESSION[$app['session']] = app::serialize64(db::fetch($rs['login']));
            $get_photo = db::lookup("photo","member","(username = '$p_username' OR email = '$p_username') AND  password = '$passwordhash' AND `status` = 'active'");
            $get_digital_form = db::lookup("digital_form","member","(username = '$p_username' OR email = '$p_username') AND  password = '$passwordhash' AND `status` = 'active'");
            // $_SESSION[$app['session']] = app::serialize64(["photo"=>$get_photo]);
            // $_SESSION[$app['session']] = app::serialize64(array_merge(db::fetch($rs['login']),["photo"=>$get_photo]));
            $_SESSION[$app['session']] = app::serialize64(array_merge(db::fetch($rs['login']),["photo"=>$get_photo,"digital_form"=>$get_digital_form]));
            // $data= db::fetch($rs['login']);
            $data = app::unserialize64($_SESSION[$app['session']]);
            // print_r($data);
            // echo "disini ".$data['id'];
            // exit;
            // db::ins_call_func("insert_log", $app['me']['id'], $module, "APR",$id, $p_masa_simpan);
            // db::ins_call_func("insert_log", $data['id'], "Login", "login", $data['id'], "",$ipaddress, "");
            // db::ins_call_func("insert_log", $data['id'], "Login", "login", "",$ipaddress, "");
            db::ins_call_func("insert_log", $data['id'], "Login", "login", "", "",$ipaddress, "");
            header("location: ". $app['webmin'] ."/dashboard.do");
		}else{
            msg::set_message('error', app::getliblang('login_invalid'));
            msg::build_msg();
            header("location: ". $app['webmin'] ."/auth.do&error=1");
		}
  	    exit;
  	}
  	else
  	{
        msg::set_message('error', app::getliblang('login_invalid'));
        header("location: ". $app['webmin'] ."/auth.do&error=1");
        exit;
  	}
}elseif(isset($act) && $act=="logout"){
  /*******************************************************************************
  * Action : logout
  * Description : clear all cookies redirect to admin to login form
  *******************************************************************************/
    	if(!empty($_SESSION[$app['session']])){
      		$_SESSION[$app['session']] = array();
      		unset($_SESSION[$app['session']]);
      		msg::set_message('success', app::getliblang('logout'));
    	}
      header("location: ". $app['webmin']);
  	exit;
}else{
	form::init();
	form::populate($form);
	$error = msg::get_message('error');
	$success = msg::get_message('success');
	$display = "none";
	$color = "";
	$message = "";
	if(isset($error)){
		$message = $error;
		$color = "danger";
		$display = "show";
	}elseif(isset($success)){
		$message = $success;
		$color = "success";
		$display = "show";
	}
  include "view.html";
}
