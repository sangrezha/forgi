<?php
  include "../../application.php";
  app::load_lib('url', 'msg', 'form','admlib');
  ## START #######################################################################
  app::set_default($act, "notification");
  db::connect();

					// $req_dump = print_r($_REQUEST, TRUE);
					// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
					// fwrite($myfile,$act);
					// fclose($myfile);
  
	/*******************************************************************************
	* Action : browse
	*******************************************************************************/
	if ($act == "notification"):
		// $icon['distributor'] = "fa-paper-plane";
		// $icon['sales'] = "fa-user-secret";
		// $icon['sbo'] = "fa-female";
		// $icon['qc'] = "fa-search";
		// $icon['result'] = "fa-check";
		// $where =" WHERE ".db::dec('a.approval')."='sbo' ORDER BY created_at DESC ";
		$callback = array("data"=>array(), "badge"=>0);
		// $sql = "SELECT a.id, ".db::dec('a.approval')." AS approval, a.`updated_at`, b.`created_at`, ".db::dec('a.approve_by')." AS approve_by, b.ncr_id FROM ".$app['table']['complaint']." AS a LEFT JOIN (SELECT SUBSTRING_INDEX( SUBSTRING_INDEX(data, '{\"act\":\"view\",\"id\":\"', - 1), '\"}', 1 ) AS ncr_id, created_at FROM ".$app['table']['user_activity']."  WHERE module = 'complaint.do' AND data LIKE '{\"act\":\"view\",\"id\":\"%%\"}') AS b ON (a.id = b.ncr_id) $where ";

		$app['me'] 	= app::unserialize64($_SESSION[$app['session']]);

		$module = admlib::$page_active['module'];
		// if($module){
			// db::remove_notif("",$module,$app['me']['level']);
		// }
		// $form = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);

		// $form = implode(",",$form);

		// $sql = "SELECT * FROM ".$app['table']['notif']." where id_rule = '$form'";
		if ($app['me']['level'] != 5) {
			if ($app['me']['code'] == "hrd" && $app['me']['level'] == "2") {
				$sql = "SELECT  a.*, b.name  created_by FROM ".$app['table']['notif']." a
					LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id) 
					where ((a.level = '".$app['me']['level']."' OR (a.from_form = 'form_pengajuan_user' AND a.level = '3')) AND a.to_section = '".$app['me']['id_section']."') OR (a.id_user = '".$app['me']['id']."') order by a.created_at DESC ";
			}else{
				$sql = "SELECT  a.*, b.name  created_by FROM ".$app['table']['notif']." a
					LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id) 
					where (a.level = '".$app['me']['level']."' AND a.to_section = '".$app['me']['id_section']."') OR (a.id_user = '".$app['me']['id']."') order by a.created_at DESC ";
			}
		}else{
			$sql = "SELECT  a.*, b.name  created_by FROM ".$app['table']['notif']." a
					LEFT JOIN ". $app['view']['user'] ." b ON (a.created_by=b.id) 
					where a.level = '".$app['me']['level']."' order by a.created_at DESC ";
		}


		db::query($sql,$rs['notif'],$nr['notif']);
		$data = array();
		// $htmls = '<li class="dropdown-item">
		// 			<h6 class="dropdown-header"><div class="col-md-6 col-sm-6 col-xs-6"><font align="center">No data</font></div></h6>
		// 		</li>
		// 	';
		$htmls = '<li class="dropdown-item">
					<h6 class="dropdown-header"><div class="col-md-6 col-sm-6 col-xs-6"><font align="center">No data</font></div></h6>
				</li>';
		$html = '';
		$i = 0;
		$n=0;
		// while($row = db::fetch($rs['notif'])){
		// 	$rows[] = $row;
		// }
		// // function unique_multidim_array($array, $key) { 
		// // 	$temp_array = array(); 
		// // 	$i = 0; 
		// // 	$key_array = array(); 
			
		// // 	foreach($array as $val) { 
		// // 		if (!in_array($val[$key], $key_array)) { 
		// // 			$key_array[$i] = $val[$key]; 
		// // 			$temp_array[$i] = $val; 
		// // 		} 
		// // 		$i++; 
		// // 	} 
		// // 	return $temp_array; 
		// // }
		// // $row = unique_multidim_array($rows,"id");
		// foreach($row as $row){
		while($row = db::fetch($rs['notif'])){
			// print_r(expression)
			// if ($row['']) {
				// $link = $row['link'];
			// }
			$name_form = "";
			switch ($row['from_form']) {
				case 'form_acc_folder':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
					}
					$name_form = "Permintaan akses folder File Sharing Server"; 
					break;
				
				case 'form_cpassword':
					// $link = $row['link']."&act=view&id=4514112020153244";
					// $link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
					}
					$name_form = "Pengajuan penggantian password"; 
					break;
				case 'form_tmp_access':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
					}
					$name_form = "Pengajuan temporary access"; 
					break;
				case 'form_perubahan_si':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
					}
					$name_form = "Permintaan perubahan sistem informasi"; 
					break;
				case 'form_abnormal_si':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=approve_it&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=approve_it&id=".$row['id_form'];
						if ($app['me']['level'] =="3") {
							$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
						}
					}
					$name_form = "Informasi abnormal sistem informasi"; 
					break;
				case 'form_get_soft':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
					}
					$name_form = "Memasukkan/mengeluarkan data softcopy"; 
					break;
				case 'form_recovery_data':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=feedback&id=".$row['id_form'];
						if ($app['me']['level'] == "3") {
							$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
						}
					}
					$name_form = "Permintaan recovery data"; 
					break;
				case 'form_hardware_software':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
					}
					$name_form = "Permintaan Hardware & Software"; 
					break;
				case 'form_pengajuan_user':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "hrd") {
						if ($app['me']['level'] == "1") {
							$link = $app['http']."/".$row['from_form'].".mod&act=approve_hrd&id=".$row['id_form'];
						}else{
							$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
						}
					}
					if ($app['me']['code'] == "it") {
						$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
					}
					$name_form = "Pengajuan user komputer"; 
					break;
				case 'form_register_device':
					// $link = $row['link']."&act=view&id=4514112020153244";
					$link = $app['http']."/".$row['from_form'].".mod&act=edit&id=".$row['id_form'];
					if ($app['me']['code'] == "it") {
						if ($app['me']['level']=="1") {
							$link = $app['http']."/".$row['from_form'].".mod&act=approve_it&id=".$row['id_form'];
						}else{
							$link = $app['http']."/".$row['from_form'].".mod&act=view&id=".$row['id_form'];
						}
					}
					$name_form = "Registrasi perangkat penyimpanan data"; 
					break;
				default:
					# code...
					break;
			}
			if ($row['from_form']) {
				$get_no_pengajuan = db::lookup("no_pengajuan",$row['from_form'],"id='".$row['id_form']."'");
				$get_status = db::lookup("status_progress",$row['from_form'],"id='".$row['id_form']."'");
			}
			$pesan="";
			$cek_approve="";
			if ($app['me']['level']=="2" && ($app['me']['code']!="it" && $app['me']['code']!="hrd")) {
				$cek_approve=db::lookup("approve_2",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="3" && ($app['me']['code']!="it" && $app['me']['code']!="hrd")) {
				$cek_approve=db::lookup("approve_3",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="4" && ($app['me']['code']!="it" && $app['me']['code']!="hrd")) {
				$cek_approve=db::lookup("approve_4",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="5" && ($app['me']['code']!="it" && $app['me']['code']!="hrd")) {
				$cek_approve=db::lookup("approve_5",$row['from_form'],"id='".$row['id_form']."'");
			}


			if ($app['me']['level']=="2" && ($app['me']['code']=="hrd" && $row['from_form'] !="form_pengajuan_user")) {
				$cek_approve=db::lookup("approve_2",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="3" && ($app['me']['code']=="hrd" && $row['from_form'] !="form_pengajuan_user")) {
				$cek_approve=db::lookup("approve_3",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="4" && ($app['me']['code']=="hrd" && $row['from_form'] !="form_pengajuan_user")) {
				$cek_approve=db::lookup("approve_4",$row['from_form'],"id='".$row['id_form']."'");
			}

			if ($app['me']['level']=="1" && $app['me']['code']=="it") {
				 $cek_approve=db::lookup("approve_it",$row['from_form'],"id='".$row['id_form']."'");
			}
			if (($app['me']['level']=="1" && $app['me']['code']=="it") && $row['from_form'] == "form_cpassword") {
				 $cek_approve=db::lookup("finish_it",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="3" && $app['me']['code']=="it") {
				$cek_approve=db::lookup("approve_it_3",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="1" && ($app['me']['code']=="hrd" && $row['from_form'] == "form_pengajuan_user")) {
				$cek_approve=db::lookup("approve_hrd",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="2" && ($app['me']['code']=="hrd" && $row['from_form'] == "form_pengajuan_user")) {
				$cek_approve=db::lookup("approve_hrd_3",$row['from_form'],"id='".$row['id_form']."'");
			}
			if ($app['me']['level']=="3" && ($app['me']['code']=="hrd" && $row['from_form'] == "form_pengajuan_user")) {
				$cek_approve=db::lookup("approve_hrd_3",$row['from_form'],"id='".$row['id_form']."'");
			}
				 $cek_approve_it=db::lookup("approve_it",$row['from_form'],"id='".$row['id_form']."'");
				 
			// echo "disini ".$cek_approve;exit;
							// '."disini ".$row['id'].'

			if ((($get_status != "finished" && $get_status != "rejected")  && $cek_approve=="tidak" && !empty($get_status) && $app['me']['code']!="it") || (($get_status != "finished" && $get_status != "rejected") && $cek_approve=="tidak" && !empty($get_status) && $app['me']['code']=="it" )) {
			// if ((($get_status != "finished" && $get_status != "rejected") && !empty($get_status) && $app['me']['code']!="it") || (($get_status != "finished" && $get_status != "rejected") && empty($cek_approve_it) && !empty($get_status) && $app['me']['code']=="it")) {
				
				if ($row['new']==1) {
					$pesan ="&nbsp;&nbsp;&nbsp;<font color='red'> New</font>";
				}
				// if (condition) {
				// }
					$n++;
				$html .= '<li class="dropdown-item">
							<a href="'.$link.'">
							<div class="col-md-2 col-sm-2 col-xs-2"><div class="notify-img"><span><i class="fa '.$icon[$row['type']].'" aria-hidden="true" style="color:#64DD17"></i></span></div></div>
							<strong>'.app::getliblang($row['type']).'</strong>
							<div class="col-md-10 col-sm-10 col-xs-10 pd-l0"> 
								'.$get_no_pengajuan.' '.($cek_approve=="iya"?'<i style="color:green;" class="fa fa-thumbs-up" aria-hidden="true"></i>':($cek_approve=="tidak"?'<i style="color:orange;" class="fa fa-check" aria-hidden="true"></i>':($cek_approve=="reject"?'<i style="color:red;" class="fa fa-times" aria-hidden="true"></i>':null))).$pesan.' 

								<div class="dropdown-message small">
								'.
								//ucfirst(str_replace("_"," ",$row['from_form']))
ucwords($name_form)
								.' 
								<br>'.app::format_datetime($row['created_at'],"ina","MM").'
								</div>
							</div>
							</a>
							
						</li>';
					}
					/*elseif (($get_status != "finished" && $get_status != "rejected") && $cek_approve_it=="tidak" && !empty($get_status) && $app['me']['code']=="it") {
			// if ((($get_status != "finished" && $get_status != "rejected") && !empty($get_status) && $app['me']['code']!="it") || (($get_status != "finished" && $get_status != "rejected") && empty($cek_approve_it) && !empty($get_status) && $app['me']['code']=="it")) {
				
				if ($row['new']==1) {
					$pesan ="&nbsp;&nbsp;&nbsp;<font color='red'> New</font>";
				}
				// if (condition) {
				// }
					$n++;
				$html .= '<li class="dropdown-item">
							<a href="'.$link.'">
							<div class="col-md-2 col-sm-2 col-xs-2"><div class="notify-img"><span><i class="fa '.$icon[$row['type']].'" aria-hidden="true" style="color:#64DD17"></i></span></div></div>
							<strong>'.app::getliblang($row['type']).'</strong>
							<div class="col-md-10 col-sm-10 col-xs-10 pd-l0"> 
								'.$get_no_pengajuan.' '.($cek_approve=="iya"?'<i style="color:green;" class="fa fa-thumbs-up" aria-hidden="true"></i>':($cek_approve=="tidak"?'<i style="color:orange;" class="fa fa-check" aria-hidden="true"></i>':($cek_approve=="reject"?'<i style="color:red;" class="fa fa-times" aria-hidden="true"></i>':null))).$pesan.' 

								<div class="dropdown-message small">
								'.ucfirst(str_replace("_"," ",$row['from_form'])).' 
								<br>'.app::format_datetime($row['created_at'],"ina","MM").'
								</div>
							</div>
							</a>
							
						</li>';
					}*/
				// $html .= $_REQUEST;
				$i++;		
		}
				// $req_dump = print_r($sql, TRUE);
				// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
				// fwrite($myfile, $req_dump);
				// fclose($myfile);
		
		$callback['data'] = $htmls;
		if($i > 0){
			$callback['data'] = $html;
			// $n==0?$n=0:null;
			$callback['badge'] = $n;
		}
		// $callback['sql'] = $sql ;
		
		echo json_encode($callback);
		exit;
	endif;

?>
