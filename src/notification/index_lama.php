<?php
  include "../../application.php";
  app::load_lib('url', 'msg', 'form','admlib');
  ## START #######################################################################
  app::set_default($act, "notification");
  db::connect();

					// $req_dump = print_r($_REQUEST, TRUE);
					// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
					// fwrite($myfile,$act);
					// fclose($myfile);
  
	/*******************************************************************************
	* Action : browse
	*******************************************************************************/
	if ($act == "notification"):
		$icon['distributor'] = "fa-paper-plane";
		$icon['sales'] = "fa-user-secret";
		$icon['sbo'] = "fa-female";
		$icon['qc'] = "fa-search";
		$icon['result'] = "fa-check";
		$where =" WHERE ".db::dec('a.approval')."='sbo' ORDER BY created_at DESC ";
		$callback = array("data"=>array(), "badge"=>0);
		// $sql = "SELECT a.id, ".db::dec('a.approval')." AS approval, a.`updated_at`, b.`created_at`, ".db::dec('a.approve_by')." AS approve_by, b.ncr_id FROM ".$app['table']['complaint']." AS a LEFT JOIN (SELECT SUBSTRING_INDEX( SUBSTRING_INDEX(data, '{\"act\":\"view\",\"id\":\"', - 1), '\"}', 1 ) AS ncr_id, created_at FROM ".$app['table']['user_activity']."  WHERE module = 'complaint.do' AND data LIKE '{\"act\":\"view\",\"id\":\"%%\"}') AS b ON (a.id = b.ncr_id) $where ";

		$app['me'] 	= app::unserialize64($_SESSION[$app['session']]);

		$form = db::get_record("id_rule","user_det", "id_user", $app['me']['id']);

		$form = implode(",",$form);

		// $sql = "SELECT * FROM ".$app['table']['notif']." where id_rule = '$form'";
		$sql = "SELECT a.*, b.name  created_by FROM ".$app['table']['notif']." a
				LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
				where id_rule = '$form' order by created_at DESC ";


		db::query($sql,$rs['notif'],$nr['notif']);
		$data = array();
		// $htmls = '<li class="dropdown-item">
		// 			<h6 class="dropdown-header"><div class="col-md-6 col-sm-6 col-xs-6"><font align="center">No data</font></div></h6>
		// 		</li>
		// 	';
		$htmls = '<li class="dropdown-item">
					<h6 class="dropdown-header"><div class="col-md-6 col-sm-6 col-xs-6"><font align="center">No data</font></div></h6>
				</li>';
		$html = '';
		$i = 0;
		$n=0;
		while($row = db::fetch($rs['notif'])){
			$rows[] = $row;
		}
		function unique_multidim_array($array, $key) { 
			$temp_array = array(); 
			$i = 0; 
			$key_array = array(); 
			
			foreach($array as $val) { 
				if (!in_array($val[$key], $key_array)) { 
					$key_array[$i] = $val[$key]; 
					$temp_array[$i] = $val; 
				} 
				$i++; 
			} 
			return $temp_array; 
		}
		$row = unique_multidim_array($rows,"id");
		foreach($row as $row){
			switch ($row['id_rule']) {
				#admin pelayaran
				case '2120082018161449':
					$link = "import.mod&act=view&status_n=inactive&id_notif=".$row['id']."&id=".$row['id_import'];
					break;
				#admin dokumen
				case '3220082018161340':
					$link = "import.mod&act=view&status_n=inactive&id_notif=".$row['id']."&id=".$row['id_import'];
					break;
				#Administrasi Pelabuhan/EIR
				case '5403092018114622':
					
					break;
				#admin trucking
				case '5703092018105342':
				#	$link = "return_container.mod&act=view&status_n=inactive&aksi=ret_con&id_notif=".$row['id']."&id=".$row['id_import'];
					$link = "trucking.mod?id_notif=".$row['id'];
					#$link = "trucking.mod&act=jadwalkan&status_n=inactive&aksi=ret_con&id_trucking=&id_notif=".$row['id']."&id=".$row['id_import'];
					break;
				#admin bea cukai
				case '7903092018114332':
					$link = "beacukai.mod&act=edit&status_n=inactive&aksi=ret_con&id_notif=".$row['id']."&id=".$row['id_import'];
					break;

				case 'admin bea cukai':
					
					break;
				
				default:
					$linl = "error.do&ref=101";
					break;
			}
										// <a href="complaint.mod&act=view&id='.$row['id'].'">
			if ($row['status']==1) {
				$row ['pesan'] .="&nbsp;&nbsp;&nbsp;<font color='red'> New</font>";
				$n++;
			}
				$html .= '<li class="dropdown-item">
							<a href="'.$link.'">
							<div class="col-md-2 col-sm-2 col-xs-2"><div class="notify-img"><span><i class="fa '.$icon[$row['type']].'" aria-hidden="true" style="color:#64DD17"></i></span></div></div>
							<strong>'.app::getliblang($row['type']).'</strong>
							<div class="col-md-10 col-sm-10 col-xs-10 pd-l0"> 
								'.$row['pesan'].'
								<div class="dropdown-message small">
								Created by '.$row['created_by'].'
								<br>'.app::format_datetime($row['created_at'],"ina","MM").'
								</div>
							</div>
							</a>
							
						</li>';
				// $html .= $_REQUEST;
				$i++;		
		}
				// $req_dump = print_r($sql, TRUE);
				// $myfile = fopen("abcdefgh.txt", "w") or die("Unable to open file!");
				// fwrite($myfile, $req_dump);
				// fclose($myfile);
		
		$callback['data'] = $htmls;
		if($i > 0){
			$callback['data'] = $html;
			// $n==0?$n=0:null;
			$callback['badge'] = $n;
		}
		
		echo json_encode($callback);
		exit;
	endif;

?>
