<?php
  include "../../application.php";
  app::load_lib('url', 'msg', 'form','admlib');
  ## START #######################################################################
  app::set_default($act, "notification");
  db::connect();
	/*******************************************************************************
	* Action : browse
	*******************************************************************************/
	if ($act == "notification"):
		$icon['distributor'] = "fa-paper-plane";
		$icon['sales'] = "fa-user-secret";
		$icon['sbo'] = "fa-female";
		$icon['qc'] = "fa-search";
		$icon['result'] = "fa-check";
		$where =" WHERE ".db::dec('a.approval')."='sbo' ORDER BY created_at DESC ";
		$callback = array("data"=>array(), "badge"=>0);
		$sql = "SELECT a.id, ".db::dec('a.approval')." AS approval, a.`updated_at`, b.`created_at`, ".db::dec('a.approve_by')." AS approve_by, b.ncr_id FROM ".$app['table']['complaint']." AS a LEFT JOIN (SELECT SUBSTRING_INDEX( SUBSTRING_INDEX(data, '{\"act\":\"view\",\"id\":\"', - 1), '\"}', 1 ) AS ncr_id, created_at FROM ".$app['table']['user_activity']."  WHERE module = 'complaint.do' AND data LIKE '{\"act\":\"view\",\"id\":\"%%\"}') AS b ON (a.id = b.ncr_id) $where ";
		db::query($sql,$rs['notif'],$nr['notif']);
		$data = array();
		$htmls = '<li class="dropdown-item">
					<h6 class="dropdown-header"><div class="col-md-6 col-sm-6 col-xs-6"><font align="center">No data</font></div></h6>
				</li>
			';
		$html = '';
		$i = 0;
		while($row = db::fetch($rs['notif'])){
			$rows[] = $row;
		}
		function unique_multidim_array($array, $key) { 
			$temp_array = array(); 
			$i = 0; 
			$key_array = array(); 
			
			foreach($array as $val) { 
				if (!in_array($val[$key], $key_array)) { 
					$key_array[$i] = $val[$key]; 
					$temp_array[$i] = $val; 
				} 
				$i++; 
			} 
			return $temp_array; 
		}
		$row = unique_multidim_array($rows,"id");
		foreach($row AS $row){
			$rows = $row;
			$row = db::get_record_filter("id, type, status, note, ".db::dec('accepted')." AS accepted, ".db::dec('rejected')." AS rejected, ".db::dec('cn_number')." AS cn_number, created_at, id_complaint","complaint_status"," id_complaint='".$row['id']."' AND type='".$row['approve_by']."' ORDER BY created_at DESC");
			$notes = "";
			$row['status'] = (isset($row['status']) && $row['status']!=null?app::getliblang($row['status']):"-");
			if($p_type=="notification"){
			$notes = "NCR ID : ".$row['id_complaint']."<br />";
			}
			$notes .= "Status : ".$row['status']."<br />";
			if($row['note'] != ""){
				$notes .= "Note : ".$row['note']."<br />";
			}
			
			if($row['cn_number'] != ""){
				$notes .= "CN Number : ".$row['cn_number']."<br />";
			}
			if($row['accepted'] != ""){
				$notes .= "Accepted : ".$row['accepted']."<br />";
				$notes .= "Rejected : ".$row['rejected']."<br />";
			}	
			
			if(is_null($rows['ncr_id'])){
				$html .= '<li class="dropdown-item">
							<a href="complaint.mod&act=view&id='.$row['id_complaint'].'">
							<div class="col-md-2 col-sm-2 col-xs-2"><div class="notify-img"><span><i class="fa '.$icon[$row['type']].'" aria-hidden="true" style="color:#64DD17"></i></span></div></div>
							<strong>'.app::getliblang($row['type']).'</strong>
							<div class="col-md-10 col-sm-10 col-xs-10 pd-l0"> 
								'.$notes.'
								<div class="dropdown-message small">'.app::format_datetime($row['created_at'],"ina","MM").'</div>
							</div>
							</a>
							
						</li>';
				$i++;
			}elseif($rows['ncr_id'] != "" && ($rows['updated_at'] > $rows['created_at'])){
				$html .= '<li class="dropdown-item">
							<a href="complaint.mod&act=view&id='.$row['id_complaint'].'">
							<div class="col-md-2 col-sm-2 col-xs-2"><div class="notify-img"><span><i class="fa '.$icon[$row['type']].'" aria-hidden="true" style="color:#64DD17"></i></span></div></div>
							<strong>'.app::getliblang($row['type']).'</strong>
							<div class="col-md-10 col-sm-10 col-xs-10 pd-l0"> 
								'.$notes.'
								<div class="dropdown-message small">'.app::format_datetime($row['created_at'],"ina","MM").'</div>
							</div>
							</a>
							
						</li>';
				$i++;
			}
		}
		
		$callback['data'] = $htmls;
		if($i > 0){
			$callback['data'] = $html;
			$callback['badge'] = $i;
		}
		
		echo json_encode($callback);
		exit;
	endif;
