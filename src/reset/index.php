<?php
  include "../../application.php";
  app::load_lib('url', 'msg', 'form','admlib');
  ## START #######################################################################
  app::set_default($act, "");
  db::connect();
   /* Delete SAM */
    $idCOmpfix = "";
    $compregfix = "";
    $iddistSAM = "'5627122017110303','8709012018111412','909012018111527'";
    $sql_idcomp = "SELECT id FROM ".$app['table']['complaint']." WHERE ".db::dec('id_distributor')." IN ($iddistSAM)";
    db::query($sql_idcomp,$rs['idcomp'],$nr['idcomp']);
    
    while($idcomp = db::fetch($rs['idcomp'])){
        $idCOmp .= "'".$idcomp['id']."',";
        $compreg .= $idcomp['id'].".*|";
    }
    $idCOmpfix = substr($idCOmp,0,strlen($idCOmp)-1);
    $compregfix = substr($compreg,0,strlen($compreg)-1);
	$rs['photo']= db::get_recordset("complaint_photo","id_complaint IN($idCOmpfix)");
	while($row = db::fetch($rs['photo'])){
		$photo = $app['data_path']."/evidence/".$row['photo'];
		@unlink($photo);
	}
	$sql 	= "DELETE FROM ". $app['table']['complaint']." WHERE ".db::dec('id_distributor')." IN ($iddistSAM)"; //id_dist
	db::qry($sql);
	$sql 	= "DELETE FROM ". $app['table']['complaint_status']." WHERE id_complaint IN ($idCOmpfix)"; //id_complaint
	db::qry($sql);
	$sql 	= "DELETE FROM ". $app['table']['complaint_photo']." WHERE id_complaint IN($idCOmpfix)"; //id_complaint
	db::qry($sql);
	$sql 	= "DELETE FROM ". $app['table']['user_activity']." WHERE module='complaint.do' AND REGEXP '$compregfix' ";
	db::qry($sql);
	echo "Data Complaint Telah Terhapus";
	
	// /* Delete All */
	// // $idCOmpfix = "";
    // // $compregfix = "";
	// // $rs['photo']= db::get_recordset("complaint_photo","1=1");
	// // while($row = db::fetch($rs['photo'])){
		// // $photo = $app['data_path']."/evidence/".$row['photo'];
		// // @unlink($photo);
	// // }  
	// // $sql 	= "DELETE FROM ". $app['table']['complaint']." "; //id_dist
	// // db::qry($sql);
	// // $sql 	= "DELETE FROM ". $app['table']['complaint_status']." "; //id_complaint
	// // db::qry($sql);
	// // $sql 	= "DELETE FROM ". $app['table']['complaint_photo']." "; //id_complaint
	// // db::qry($sql);
	// // $sql 	= "DELETE FROM ". $app['table']['user_activity']." WHERE module='complaint.do' ";
	// // db::qry($sql);
	// // echo "Data Complaint Telah Terhapus";
