<?php
/*******************************************************************************
* Filename : index.php
* Description : admin main file
*******************************************************************************/
include "../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'admlib');
## START #######################################################################
app::set_default($act, "");
//app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
   $ipaddress = '';
if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
else
    $ipaddress = 'UNKNOWN';
/*******************************************************************************
* Action : -
* Description : show login form
*******************************************************************************/
// if(isset($_SESSION[$app['session']]))
// 	 header("location: ". $app['webmin'] ."/dashboard.do");
// else
// 	header("location: ". $app['webmin'] ."/auth.do");

$config = db::get_record("configuration", "id", 1);
if (empty($act)):
	form::populate($form);
	if(isset($_SESSION[$app['session']])){
		//header("location: ". $app['webmin'] ."/dashboard.do");
	}else{
		header("location: ". $app['webmin'] ."/auth.do");
	}
	exit;
endif;
/*******************************************************************************
* Action : login
* Description : validate login, create cookies and redirect to admin
*               main interface
*******************************************************************************/
if (isset($act) && $act=="logout"):
	exit();
	if(!empty($_SESSION[$app['session']])):
		$_SESSION[$app['session']] = array();
		print_r($_SESSION[$app['session']]);
		exit;
        db::ins_call_func("insert_log", $data['id'], "Logout", "logout", $data['id'], "");
		// unset($_SESSION[$app['session']]);
		session_destroy();
    
	
		msg::set_message('success', app::getliblang('logout'));
		msg::build_msg(1);
	endif;
    header("location: ". $app['webmin']);
	exit;
endif;

if($act == 'notfound')
{
	header("location: ". $app['webmin'] ."/error.do&ref=404");
	exit;
}
?>
