<!DOCTYPE html>
<html lang="en">
<head>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<title>...</title>
	<!-- Bootstrap core CSS -->
	<link href="<?php echo $app['webmin']; ?>/assets/bootstrap/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="<?php echo $app['webmin']; ?>/assets/mdb/mdb.min.css" rel="stylesheet">
</head>
<body style="background:#f5f5f5;">
	<div class="container">
		<div class="row">
			<div class="col-md-4">&nbsp; Error</div>
			<div class="col-md-4" style="padding-top:50px;">
				<div class="card">
					<div class="card-content">
						<h4 class="card-title" style="color:#303030;"><?php echo $msg_title ?></h4>
						<p><?php echo $msg_content ?></p>
					</div>
					<div class="card-action">
						<?php if(isset($msg_back) && $msg_back == 'Login'){ ?>
							<a href="<?php echo $app['webmin']; ?>" class="btn btn-border-primary waves-effect"><?php echo $msg_back ?></a>
						<?php } ?>
						<?php if(isset($msg_back) && $msg_back == 'Back'){ ?>
							<a href="<?php echo url::get_referer(); ?>" class="btn btn-border-primary waves-effect"><?php echo $msg_back ?></a>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="col-md-4">&nbsp;</div>
		</div>
    </div>
	<script type="text/javascript" src="<?php echo $app['webmin']; ?>/assets/jquery/jquery.min.js"></script>
	<!-- Material Design Bootstrap -->
    <script type="text/javascript" src="<?php echo $app['webmin']; ?>/assets/mdb/mdb.min.js"></script>
</body>
</html>