<?php
/*******************************************************************************
* Filename : index.php
* Description : admin main file
*******************************************************************************/
include "../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'admlib');
## START #######################################################################
app::set_default($act, "dbrowse");
db::connect();
admlib::validate();
admlib::$page_active = array('system'=>'dashboard','caption'=>'Dashboard');

$pc = db::lookup("COUNT(id)","complaint",db::dec("id_category")."='7426122017150337'");
$tb = db::lookup("COUNT(id)","complaint",db::dec("id_category")."='3826122017143150'");
$process = db::lookup("COUNT(id)","complaint",db::dec("approval")."!=''");
$closed = db::lookup("COUNT(id)","complaint","status = 'closed'");
$today = db::lookup("COUNT(id)","complaint","DATE(created_at) = CURRENT_DATE() ");
$week = db::lookup("COUNT(id)","complaint","WEEKOFYEAR(created_at)=WEEKOFYEAR(NOW())");
$month = db::lookup("COUNT(id)","complaint","YEAR(created_at)=YEAR(CURDATE()) AND MONTH(created_at)= MONTH(CURDATE())");
$total = $process + $closed;

// $datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
// 		while($row = db::fetch($datas['expired_data'])){

// 			if (strtotime($row['expired_do']) < time()) {
// 				$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;
// 			}
// 			else{
// 				$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
// 			}
// 		}

// 	$id_exp = implode("','", $id_exp321);
// 	$id_exp_act = implode("','", $id_exp_act321);
// 	$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
// 	$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
// 	db::qry($sql321123);
// 	db::qry($sql1231321);
/*
############################ EXPIRED DELIVERY ORDER ####################################
			$tgl_untuk_expired = db::lookup("hari_ini","hari_ini","id=1");
			if (strtotime($tgl_untuk_expired) != strtotime(date("Y-m-d"))) {
				$datas['expired_data'] = db::get_record_select("id_delivery_order,expired_do","expired_do","status","active");
					while($row = db::fetch($datas['expired_data'])){
					$id_importnya =	db::get_record("delivery_order","id",$row['id_delivery_order']);
					$id_importnya321123 = db::get_record("import","id",$id_importnya['id_import']);
						if ($id_importnya321123['status_kiriman'] == "0") {
							if (strtotime($row['expired_do']) < time()) {
								$row['id_delivery_order']!=""?$id_exp321 [] = $row['id_delivery_order']:null;	
									$id_notif   = rand(1, 100).date("dmYHis");
									$sqls = "insert into ".$app['table']['notif']." 
											(id,id_rule,id_import,status,pesan,created_by,created_at) values
											('$id_notif','2120082018161449','".$id_importnya['id_import']."',1,'DO Dokumen No. PIB ".$id_importnya321123['num_pib']." perlu diproses DO','".$app['me']['id']."',now())";
									db::qry($sqls);
							}else{
								$row['id_delivery_order']!=""?$id_exp_act321 [] = $row['id_delivery_order']:null;
							}
						}
					}

					$id_exp = implode("','", $id_exp321);
					$id_exp_act = implode("','", $id_exp_act321);
					$sql321123 = "UPDATE ".$app['table']['delivery_order']." set status = 'expired' where id in('".$id_exp."') ";
					$sql1231321 = "UPDATE ".$app['table']['delivery_order']." set status = 'active' where id in('".$id_exp_act."') ";
					db::qry($sql321123);
					db::qry($sql1231321);
					db::qry("UPDATE ".$app['table']['hari_ini']." SET hari_ini = '".date("Y-m-d")."' ");
				}
                
########################################################################################

  	$sql = "SELECT g.id id, g.status,g.num_container num_container, d.num_pib AS num_pib,/* h.date_delivery as delivery_date,
b.name as created_by,a.created_at,c.name as updated_by,a.updated_at,d.id id_imp, g.note note,g.address address
		FROM ". $app['table']['pelabuhan'] ." a 	
		LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id) 
		LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id)
		left JOIN ". $app['table']['import']." d ON (a.id_import=d.id)
		left JOIN ". $app['table']['delivery_order']." f ON (f.id_import=a.id_import)
		LEFT JOIN ". $app['table']['container'] ." g ON (a.id_import=g.id_import)
		/*LEFT JOIN ". $app['table']['trucking'] ." h ON (a.id_import= h.id_import)
		where f.status = 'active' AND a.kelengkapan !='' $q ORDER BY a.created_at DESC";
	db::query($sql, $rs['row'], $nr['row']);
	while($row = db::fetch($rs['row']))
	{
		$datas321 = db::get_record("trucking","id_container='".$row['id']."' ");
		$row['status_return'] = db::lookup("status","return_container","id_trucking IN ('".$datas321['id']."') AND id_container IN ('".$row['id']."')");

		if ($row['status_return']=="selesai") {
			db::qry("UPDATE ".$app['table']['import']." set status_kiriman ='1' where id = '".$row['id_imp']."' ");
		}
	}
#############################################################################################

######################   ubah status kirim   ################################
	$asdqwezxcasd= "SELECT * FROM ".$app['table']['import']." ORDER BY a.id_import DESC";
	db::query($asdqwezxcasd, $rs11['row'], $nr11['row']);
	while($row11 = db::fetch($rs11['row']))
	{
		$cek_selesai = db::lookup("GROUP_CONCAT(stat_kir)","container", "id_import", $row11['id']);
		if (!preg_match("/0/i", $cek_selesai)) {
			db::qry("update ".$app['table']['import']." set	send_status = 'selesai' where id='".$row11['id']."' ");
		}elseif (preg_match("/1/i", $cek_selesai)) {
			db::qry("update ".$app['table']['import']." set	send_status = 'sebagian' where id='".$row11['id']."' ");
		}else{
			db::qry("update ".$app['table']['import']." set	send_status = 'belum' where id='".$row11['id']."' ");
		}
	}
######################   /ubah status kirim   #################################

	$select = "SELECT ";
	$select .= "a.id AS ncr_id, ";
	$select .= db::dec('a.dn')." AS dn,  ";
	$select .= db::dec('d.name')." AS distributor, ";
	$select .= "a.created_at AS post_date";
	
	$table = "FROM ".$app['table']['complaint']." AS a";
	$join = "";
	$join .= "LEFT JOIN ".$app['table']['distributor']." AS d ON (".db::dec('a.id_distributor')."=d.id) ";
	
	$where = " WHERE ".db::dec('a.approval')."='sbo' ";
	
	$sql = " $select $table $join $where ORDER BY a.created_at DESC";
	db::query($sql, $rs['row'], $nr['row']);
    */
	
include "dsp_home.php";
?>
