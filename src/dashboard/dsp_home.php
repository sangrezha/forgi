<?php admlib::display_block_header('Dashboard'); ?>
        <style>
            .test{cursor:pointer;}
            .test + .tooltip > .tooltip-inner {background-color: blue;}
            .test + .tooltip > .tooltip-arrow { border-bottom-color: blue; }
        </style>
        
        <!-- page content -->
		<div class="right_col" role="main">
			<div class="container">
				<div class="page-title">
					<div class="title">
						<p>Welcome to CMWI - FORGI Administrator.</p>
						<?php // if ($app['me']['level'] == "-" || $app['me']['code'] == "it"): ?>
							
						<div class="row">
                            
							<div class="col-md-3 col-sm-12 col-xs-12">
								<!-- <div class="panel panel-danger text-center complaint_today"> -->
								<div style="background-color: #52a6f8 !important;border:0px;" class="panel text-center complaint_today">
									<div style="background-color: #52a6f8 !important;border:1px solid #52a6f8 !important;" class="panel-heading">
										<h3 style="" class="panel-title">Total Pengajuan<i class="test fa fa-info fa-fw" data-toggle="tooltip" data-placement="bottom" title="Total dokumen pengajuan yang masuk per hari ini"></i></h3>
									</div>
									<div class="panel-body" style="border:1px solid #52a6f8 !important;background-color: #ebebeb!important;color:black;">
										<?php if(($app['me']['level'] == 1  && $app['me']['level'] != "-") && ($app['me']['code'] != "it" && $app['me']['code'] != "hrd")){ ?>
										<h1><?= db::viewlookup("count(id)","form","1=1 AND created_by = '".$app['me']['id']."'") ?></h1>
										<?php }elseif((($app['me']['level'] > 1 && $app['me']['level'] < 5)  && $app['me']['level'] != "-") && ($app['me']['code'] != "it" && $app['me']['code'] != "hrd")){ 
											$get_atasan = db::viewlookup("GROUP_CONCAT(id)","user","level > ".$app['me']['level']."  AND (id_section ='".$app['me']['id_section']."' OR level = 5)");
											$get_atasan = explode(",",$get_atasan);	
											$get_atasan = implode("','",$get_atasan);
											$get_atasan = rtrim($get_atasan,",'");
											?>
										<h1><?= db::viewlookup("count(id)","form","created_by NOT IN ('".$get_atasan."') AND id_section ='".$app['me']['id_section']."'") ?></h1>	
 										<?php }elseif($app['me']['code'] == "hrd"){ 
											$get_atasan = db::viewlookup("GROUP_CONCAT(id)","user","level > ".$app['me']['level']."  AND (id_section ='".$app['me']['id_section']."' OR level = 5)");
											$get_atasan = explode(",",$get_atasan);	
											$get_atasan = implode("','",$get_atasan);
											$get_atasan = rtrim($get_atasan,",'");
											?>
										<h1><?= db::viewlookup("count(id)","form","(created_by NOT IN ('".$get_atasan."') AND id_section ='".$app['me']['id_section']."') OR jenis_form = 'form_pengajuan_user'") ?></h1>	
 										<?php }else{ ?>
										<h1><?= db::viewlookup("count(id)","form","1=1") ?></h1>
										<?php } ?>
										<!-- <i class="fa fa-calendar-check-o" aria-hidden="true"></i> -->
									</div>
                                    <!--
    								<div style="background-color: white!important;color:black;text-align:left;" class="panel-footer">Total form/pengajuan yang masuk per hari ini.</div>-->
								</div>
								
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div style="background-color: #f7d229 !important;border:0 !important;" class="panel text-center complaint_week">
									<div style="background-color: #f7d229 !important;border:1px solid #f7d229 !important;" class="panel-heading">
										<h3 class="panel-title">Dalam Proses<i class="test fa fa-info fa-fw" data-toggle="tooltip" data-placement="bottom" title="Jumlah dokumen pengajuan yang sedang dalam proses atau ditangani oleh IT"></i></h3>
									</div>
									<div class="panel-body" style="background-color: #ebebeb!important;color:black;border:1px solid #f7d229 !important;">
										<?php if(($app['me']['level'] == 1 && $app['me']['level'] != "-") && ($app['me']['code'] != "it" && $app['me']['code'] != "hrd")){ ?>
										<!-- <h1><?= db::viewlookup("count(id)","form","status_form='Diterima' AND status_progress!='finished'") ?> </h1> -->
										<h1><?= db::viewlookup("count(id)","form","(status_form='Diterima' AND status_progress!='finished') AND created_by = '".$app['me']['id']."' ") ?></h1>
										<?php }elseif((($app['me']['level'] > 1 && $app['me']['level'] < 5)  && $app['me']['level'] != "-") && ($app['me']['code'] != "it" && $app['me']['code'] != "hrd")){ 
											$get_atasan = db::viewlookup("GROUP_CONCAT(id)","user","level > ".$app['me']['level']."  AND (id_section ='".$app['me']['id_section']."' OR level = 5)");
											$get_atasan = explode(",",$get_atasan);	
											$get_atasan = implode("','",$get_atasan);
											$get_atasan = rtrim($get_atasan,",'");
											?>
										<h1><?= db::viewlookup("count(id)","form","(created_by NOT IN ('".$get_atasan."') AND id_section ='".$app['me']['id_section']."') AND (status_form='Diterima' AND status_progress!='finished')") ?></h1>	
 										<?php }elseif($app['me']['code'] == "hrd"){ 
											$get_atasan = db::viewlookup("GROUP_CONCAT(id)","user","level > ".$app['me']['level']."  AND (id_section ='".$app['me']['id_section']."' OR level = 5)");
											$get_atasan = explode(",",$get_atasan);	
											$get_atasan = implode("','",$get_atasan);
											$get_atasan = rtrim($get_atasan,",'");
											?>
										<h1><?= db::viewlookup("count(id)","form","((created_by NOT IN ('".$get_atasan."') AND id_section ='".$app['me']['id_section']."') OR jenis_form = 'form_pengajuan_user') AND (status_form='Diterima' AND status_progress!='finished')") ?></h1>	
 										<?php }else{ ?>

											<h1><?= db::viewlookup("count(id)","form","status_form='Diterima' AND status_progress!='finished'") ?> </h1>
										<?php } ?>
										<!-- <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> -->
									</div>
                                    <!--
    									<div style="background-color: white!important;color:black;text-align:left;" class="panel-footer">Jumlah form/pengajuan yang sedang dalam proses atau ditangani oleh IT</div>
                                    -->
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div style="background-color: #70bf42 !important;border: 0 !important;" class="panel text-center complaint_week">
									<div style="background-color: #70bf42 !important;border:1px solid #70bf42 !important;" class="panel-heading">
										<h3 class="panel-title">Selesai<i class="test fa fa-info fa-fw" data-toggle="tooltip" data-placement="bottom" title="Jumlah dokumen pengajuan yang selesai di tangani oleh IT (termasuk yang belum dikonfirmasi oleh user)"></i></h3>
									</div>
									<div class="panel-body" style="background-color: #ebebeb!important;border:1px solid #70bf42 !important;color:black;">

										<?php if(($app['me']['level'] == 1 && $app['me']['level'] != "-") && ($app['me']['code'] != "it" && $app['me']['code'] != "hrd")){ ?>
											<h1><?= db::viewlookup("count(id)","form","status_progress='finished' AND created_by = '".$app['me']['id']."' ") ?></h1>
										<?php }elseif((($app['me']['level'] > 1 && $app['me']['level'] < 5)  && $app['me']['level'] != "-") && ($app['me']['code'] != "it" && $app['me']['code'] != "hrd")){ 
											$get_atasan = db::viewlookup("GROUP_CONCAT(id)","user","level > ".$app['me']['level']."  AND (id_section ='".$app['me']['id_section']."' OR level = 5)");
											$get_atasan = explode(",",$get_atasan);	
											$get_atasan = implode("','",$get_atasan);
											$get_atasan = rtrim($get_atasan,",'");
											?>
										<h1><?= db::viewlookup("count(id)","form","(created_by NOT IN ('".$get_atasan."') AND id_section ='".$app['me']['id_section']."') AND (status_progress='finished')") ?></h1>	
 										<?php }elseif($app['me']['code'] == "hrd"){ 
											$get_atasan = db::viewlookup("GROUP_CONCAT(id)","user","level > ".$app['me']['level']."  AND (id_section ='".$app['me']['id_section']."' OR level = 5)");
											$get_atasan = explode(",",$get_atasan);	
											$get_atasan = implode("','",$get_atasan);
											$get_atasan = rtrim($get_atasan,",'");
											?>
										<h1><?= db::viewlookup("count(id)","form","((created_by NOT IN ('".$get_atasan."') AND id_section ='".$app['me']['id_section']."') OR jenis_form = 'form_pengajuan_user') AND status_progress='finished'") ?></h1>	
 										<?php }else{ ?>
											<h1><?= db::viewlookup("count(id)","form","status_progress='finished'") ?></h1>
										<?php } ?>
										<!-- <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> -->
									</div>
                                    <!--
									<div style="background-color: white!important;color:black;text-align:left;" class="panel-footer">Jumlah form/pengajuan yang selesai di tangani oleh IT (termasuk yang belum dikonfirmasi oleh user) </div>-->
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div style="background-color: #ec5d57 !important;border: 0 !important;" class="panel text-center complaint_week">
									<div style="background-color: #ec5d57 !important;border:1px solid #ec5d57 !important;" class="panel-heading">
                                        <h3 class="panel-title">Ditolak<i class="test fa fa-info fa-fw" data-toggle="tooltip" data-placement="bottom" title="Total dokumen pengajuan yang ditolak"></i></h3>
									</div>
									<div class="panel-body" style="background-color: #ebebeb!important;border:1px solid #ec5d57 !important;color:black;">
										<!-- <h1><?= db::viewlookup("count(id)","form","status_form='Ditolak'") ?></h1> -->
										<?php if(($app['me']['level'] == 1 && $app['me']['level'] != "-") && ($app['me']['code'] != "it" && $app['me']['code'] != "hrd")){ ?>
											<h1><?= db::viewlookup("count(id)","form","status_form='Ditolak' AND created_by = '".$app['me']['id']."' ") ?></h1>
										<?php }elseif((($app['me']['level'] > 1 && $app['me']['level'] < 5)  && $app['me']['level'] != "-") && ($app['me']['code'] != "it" && $app['me']['code'] != "hrd")){ 
											$get_atasan = db::viewlookup("GROUP_CONCAT(id)","user","level > ".$app['me']['level']."  AND (id_section ='".$app['me']['id_section']."' OR level = 5)");
											$get_atasan = explode(",",$get_atasan);	
											$get_atasan = implode("','",$get_atasan);
											$get_atasan = rtrim($get_atasan,",'");
											?>
										<h1><?= db::viewlookup("count(id)","form","(created_by NOT IN ('".$get_atasan."') AND id_section ='".$app['me']['id_section']."') AND (status_progress='rejected')") ?></h1>	
 										<?php }elseif($app['me']['code'] == "hrd"){ 
											$get_atasan = db::viewlookup("GROUP_CONCAT(id)","user","level > ".$app['me']['level']."  AND (id_section ='".$app['me']['id_section']."' OR level = 5)");
											$get_atasan = explode(",",$get_atasan);	
											$get_atasan = implode("','",$get_atasan);
											$get_atasan = rtrim($get_atasan,",'");
											?>
										<h1><?= db::viewlookup("count(id)","form","((created_by NOT IN ('".$get_atasan."') AND id_section ='".$app['me']['id_section']."') OR jenis_form = 'form_pengajuan_user') AND status_form='Ditolak'") ?></h1>	
 										<?php }else{ ?>
											<h1><?= db::viewlookup("count(id)","form","status_form='Ditolak'") ?></h1>
										<?php } ?>
										<!-- <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> -->
									</div>
                                        <!--
									<div style="background-color: white!important;color:black;text-align:left;" class="panel-footer">Total form/pengajuan yang ditolak</div>-->
								</div>
							</div>

						</div>
						<?php // endif ?>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>

		</div>
	<link rel="stylesheet" type="text/css" href="<?php echo $app['_scripts'] ?>/tablesorted/theme.default.min.css">
	<script src="<?php  echo $app['_scripts'] ?>/jqui/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo $app['_scripts'] ?>/tablesorted/jquery.tablesorter.min.js"></script>
<?php admlib::display_block_footer(); ?>