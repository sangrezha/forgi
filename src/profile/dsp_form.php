	<?php
$html = '<div class="notif-msg"></div>';
admlib::display_block_header();
admlib::get_component('uploadlib');
	admlib::get_component('formstart');
		// echo $html;
/*		admlib::get_component('inputtext',
			array(
				"name"=>"nik",
				"value"=>app::ov($form['nik']),
				"validate"=>"required"
			)
		);*/
		admlib::get_component('view',
			array(
				"name"=>"nik",
				"value"=>app::ov($form['nik']),
				"validate"=>"required"
			)
		);
/*		admlib::get_component('inputtext',
			array(
				"name"=>"name",
				"value"=>app::ov($form['name']),
				"validate"=>"required"
			)
		);*/
		admlib::get_component('view',
			array(
				"name"=>"name",
				"value"=>app::ov($form['name']),
				"validate"=>"required"
			)
		);
/*		admlib::get_component('inputtext',
			array(
				"name"=>"username",
				"value"=>app::ov($form['username']),
				"validate"=>"required"
			)
		);*/
		admlib::get_component('view',
			array(
				"name"=>"username",
				"value"=>app::ov($form['username']),
				"validate"=>"required"
			)
		);
/*		admlib::get_component('inputtext',
			array(
				"name"=>"email",
				"value"=>app::ov($form['email']),
				"validate"=>"required"
			)
		);*/
		admlib::get_component('view',
			array(
				"name"=>"email",
				"value"=>app::ov($form['email']),
				"validate"=>"required"
			)
		);
/*		admlib::get_component('inputtext',
			array(
				"name"=>"telp",
				"value"=>app::ov($form['telp'])
			)
		);*/
		admlib::get_component('view',
			array(
				"name"=>"telp",
				"value"=>app::ov($form['telp'])
			)
		);

		$rs["departement"] = db::get_record_select("id, name","departement","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_departement",
				"value"=>app::ov($form['id_departement']),
				"readonly"=>"iya",
				"items"=>$rs['departement']
			)
		);
		// $rs["section"] = db::get_record_select("id, name","section","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_section",
				"value"=>app::ov($form['id_section']),
				"readonly"=>"iya",
				"items"=>$rs['section']
			)
		);
		// $rs["title"] = db::get_record_select("id, name","title","status='active' ORDER BY name ASC");
		admlib::get_component('select',
			array(
				"name"=>"id_title",
				"value"=>app::ov($form['id_title']),
				"readonly"=>"iya",
				"items"=>$rs['title']
			)
		);
		admlib::get_component('inputupload', 
			array(
				"name"=>"photo", 
				// "value"=> (isset($form['logo']) AND file_exists($app['logo_lib_path'] ."/". $form['logo']))?'/'. app::ov($form['logo']):null,
				"value"=> $form['photo'],
				"no_attach"=> true,
				"default"=> true,
				"filemedia"=>true
			)
		);
		// admlib::get_component('inputtext',
		// 	array(
		// 		"type"=>"password",
		// 		"name"=>"password",
		// 		"validate"=>"required"
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"type"=>"password",
		// 		"name"=>"new_password",
		// 		"validate"=>"required"
		// 	)
		// );
		// admlib::get_component('inputtext',
		// 	array(
		// 		"type"=>"password",
		// 		"name"=>"re_password",
		// 		"validate"=>"required"
		// 	)
		// );
		admlib::get_component('submit',
			array(
				"id"=>(isset($app['me']['id'])?$app['me']['id']:$id),
				"no_submit"=>"iya",
				"act"=>$act
			)
		);
	admlib::get_component('formend');
?>
	<script type="text/javascript">
	function get_section(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						// console.log(result); 
						// if(dis){
						// 	loop = "<option disabled><?php echo app::i18n('select');?></option>";
						// }else{
							loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						// }
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						// alert(loop);
						$("#id_section").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_section").html('');
						$("#id_title").html('');
					}
			}
		});     
	}
	function get_title(id_customer, dis, url2){
		$.ajax({
			type:"GET",
			url:url2,
			success:function(res){         
					var result = JSON.parse(res);
					if(result.error == 0){
						var loop = "";
						console.log(result);
						loop = "<option selected disabled><?php echo app::i18n('select');?></option>";
						$.each(result.section,function(key,value){
							loop += "<option "+(value.id===dis?'selected':'')+" value="+value.id+">"+value.name+"</option>";
						});
						$("#id_title").html(loop);
						// console.log("loop " + loop);
					}else{
						$("#id_title").html('');
					}
			}
		});     
	}
	</script>
	<script>
		$(document).ready(function(){
			// Custom Company to Custom PIC & Maker-Model
			$('#id_departement').each(function(){
				var id_section = $(this).val();
				var dis = '<?= $form['id_section'] ?>';
				// alert(dis);
				// console.log(dis);
				var urlcustomer = "<?php echo admlib::$page_active['system'] ?>.do&act=get-section&id_section="+id_section;
				// alert(urlcustomer);
				// var urlmaker = "<?php // echo admlib::$page_active['system'] ?>.do&act=get-maker-model&id_cus_pic="+id_customer;
				// console.log("ID " + urlcustomer);
				get_section(id_section, dis, urlcustomer);
				// get_makermodel(id_customer, dis, urlmaker);
			});

			$('#id_departement').change(function(){
				$('#id_section').select("val","null");
				// $('#id_title').select("val","null");
				var id_section = $(this).val();
				var dis = '';
				// console.log(dis);
				var urlsection = "<?php echo admlib::$page_active['system'] ?>.do&act=get-section&id_section="+id_section;
				// alert(urlsection);
				// var urlmaker = "<?php //echo admlib::$page_active['system'] ?>.do&act=get-maker-model&id_cus_pic="+id_departement;
				// console.log("ID section: " + urlsection);
				// console.log("ID Maker: " + urlmaker);
				get_section(id_section, dis, urlsection);
				// get_makermodel(id_section, dis, urlmaker);
			});
			$('#id_title').each(function(){
				// var id_title = $(this).val();
				// var id_title = $("#id_section option:selected").val();
				var id_title = '<?= $form['id_section'] ?>';
				var dis = '<?= $form['id_title'] ?>';
				// alert(dis);
				// console.log(dis);
				// alert(dis);
				var urlcustomer = "<?php echo admlib::$page_active['system'] ?>.do&act=get-title&id_title="+id_title;
				// alert(urlcustomer);
				// var urlmaker = "<?php // echo admlib::$page_active['system'] ?>.do&act=get-maker-model&id_cus_pic="+id_customer;
				// console.log("ID " + urlcustomer);
				get_title(id_title, dis, urlcustomer);
				// get_makermodel(id_customer, dis, urlmaker);
			});

			$('#id_section').change(function(){
				$('#id_title').select("val","null");
				var id_title = $("#id_section option:selected").val();
				var dis = '';
				// console.log(dis);
				var urlsection = "<?php echo admlib::$page_active['system'] ?>.do&act=get-title&id_title="+id_title;
				// alert(urlsection);
				// var urlmaker = "<?php //echo admlib::$page_active['system'] ?>.do&act=get-maker-model&id_cus_pic="+id_departement;
				// console.log("ID section: " + urlsection);
				// console.log("ID Maker: " + urlmaker);
				get_title(id_section, dis, urlsection);
				// get_makermodel(id_section, dis, urlmaker);
			});

		});
</script>
<?php
admlib::display_block_footer();
?>
