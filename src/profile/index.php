<?php
/*******************************************************************************
* Filename : index.php
* Description : profile modul
*******************************************************************************/
include "../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');
## START #######################################################################
app::set_default($act, 'profile');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('system'=>'profile','caption'=>'Profile Account');
/*******************************************************************************
* Action : profile
*******************************************************************************/
if ($act == "profile"):
	admlib::validate();
	form::init();
	if ($step == 1):
		$form = db::get_record('user', 'id', $app['me']['id']);
		// print_r($form);
		if(empty($form[0])){
			$form = db::get_record('member', 'id', $app['me']['id']);
		}
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
	// echo "username='".$p_username."' AND passwordhash='".md5(serialize($p_password))."' ";exit;
		form::serialize_form();
		// print_r($_REQUEST);
		form::validate('empty','p_name,p_username,p_nik,p_id_section,p_id_departement,p_email');
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=profile&error=1&id=" . $id);
			exit;
		endif;
		// if( !db::lookup('passwordhash', 'user', "username='".$p_username."' AND passwordhash='".md5(serialize($p_password))."' ") )
		// {
		// 	msg::set_message('error', app::getliblang('password_invalid'));
		// 	msg::set_message('password', app::getliblang('password_invalid'));
		// 	form::set_error(1);
		// }
		// if ($p_new_password != $p_re_password)
		// {
		// 	msg::set_message('error', app::getliblang('password_not_match'));
		// 	msg::set_message('re_password', app::getliblang('password_not_match'));
		// 	msg::set_message('new_password', " ");
		// 	form::set_error(1);
		// }
		// if (form::is_error())
		// {
		// 	msg::build_msg();
		// 	header("location: ". $app['webmin'] ."/". admlib::$page_active['system'] .".do&act=profile&error=1&id=" . $id);
		// 	exit;
		// }
		// if($p_new_password)
		// {
		// 	$passwordhash = md5(serialize($p_new_password));
		// 	$sql = "update ". $app['table']['user'] ."
		// 			set passwordhash='$passwordhash'
		// 			where id='". $id ."'";
		// 	db::qry($sql);
		// }
		app::mq_encode('p_name,id_title');
		$sql = "update ". $app['table']['member'] ."
				set id_title = '$p_id_title',
					username = '$p_username',
					nik = '$p_nik',
					id_section = '$p_id_section',
					id_departement = '$p_id_departement',
					photo = '$p_photo',
					name = '$p_name',
					email = '$p_email',
					telp  = '$p_telp',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
					where id = '$id'";	
		db::qry($sql);
		// if ($id == $app['me']['id']):
		$user = db::get_record_view("user", "id", $id);
		// $_SESSION[$app['session']] = app::serialize64(array_merge(db::fetch($rs['login']),["photo"=>$get_photo]));
		$_SESSION[$app['session']] = app::serialize64(array_merge($user,["photo"=>$p_photo]));
		// endif;
		msg::set_message('success', app::getliblang('update'));
		msg::build_msg(1);
		header("location: ". $app['webmin'] ."/". admlib::$page_active['system'] .".do");
	endif;
endif;
/*******************************************************************************
* Action : get-section;
*******************************************************************************/
if($act == "get-section"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$section = "SELECT id,name as name FROM ". $app['table']['section'] . " WHERE id_departement IN ('". $id_section."') AND status='active'";
	db::query($section, $rs['section'], $nr['section']);
	if($nr['section'] > 0){
		while($row = db::fetch($rs['section'])){
			$callback['section'][] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	echo json_encode($callback);
	exit;
endif;
/*******************************************************************************
* Action : get-section;
*******************************************************************************/
if($act == "get-title"):
	$callback = array("error"=>0, "message"=>"", "model"=>array());
	$section = "SELECT id,name as name FROM ". $app['table']['title'] . " WHERE id_section IN ('". $id_title."') AND status='active'";
	db::query($section, $rs['section'], $nr['section']);
	
	if($nr['section'] > 0){
		while($row = db::fetch($rs['section'])){
			$callback['section'][] = $row;
		}
	}else{
		$callback['error'] = 1;
	}
	// $callback["sql_title"] = $section;
	echo json_encode($callback);
	exit;
endif;
?>
