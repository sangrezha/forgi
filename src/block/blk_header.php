<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php 
    	$config = db::get_record("configuration", "id", 1);
    	// echo "disini ";
    	// print_r($config);
     ?>
    <title><?php echo (isset($config['title_cms'])?$config['title_cms']:"SAM DESIGN") ?></title>
<!-- 

    <link href="<?php echo ($config['icon_crm'] != "" && file_exists($app['data_path'] ."/static/". $config['icon_crm']))?$app['_static'] ."/". $config['icon_crm']:null ?>" rel=icon> -->
    
    <link rel="icon" href="<?php echo $app['www'] ?>/favicon.ico" type="image/x-icon">

    
	<link rel="stylesheet" href="<?php echo $app['_scripts'] .'/manifest.json'; ?>">
	<!-- bootstrap_3.min.css -->
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->
    <!-- <link rel="stylesheet" href="<?php echo $app['_styles'] .'/bootstrap_3.min.css?'. rand(1,1000); ?>"> -->
    <!-- <link rel="stylesheet" href="<?php echo $app['_styles'] .'/bootstrap.min.css?'. rand(1,1000); ?>"> -->
    <link rel="stylesheet" href="<?php echo $app['_styles'] .'/bootstrap_35.min.css?'. rand(1,1000); ?>">
    <link rel="stylesheet" href="<?php echo $app['_styles'] .'/font-awesome.min.css?'. rand(1,1000); ?>">
    <link rel="stylesheet" href="<?php echo $app['_styles'] .'/font-awesome.animated.css?'. rand(1,1000); ?>">
    <link rel="stylesheet" href="<?php echo $app['_styles'] .'/custom.css?'. rand(1,1000); ?>">
    <link rel="stylesheet" href="<?php echo $app['_styles'] .'/style.css?'. rand(1,1000); ?>">
	<script src="<?php echo $app['_scripts']; ?>/jquery.min.js"></script>
	<script src="<?php echo $app['_scripts']; ?>/bootstrap-3.5.min.js"></script>
	<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>-->
	<style>
		.notify-img{
			text-align: center;
			padding: 5px;
		}
		
		.notify-img span{
			font-size: 20px;
		}
		.dropdown-menu.notify-drop .drop-content {
			max-height: 280px;
			overflow-y: scroll;
		}
		.drop-content li a {
			padding: 0px !important;
			background: none !important;
			color:grey !important;
		}
		.drop-content li {padding-top:10px;}
		.drop-content li:hover {
			background: #f2f2f2;
		}
		
	</style>
    
	<!-- <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script> -->
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
		    <?php
if ($forgot != "yes") {
		     admlib::display_block_menu(); } ?>
        <!-- top navigation -->
        <div class="<?php echo $forgot == "yes"?null:"top_nav"; ?>">


          <div class="nav_menu" <?php echo $forgot == "yes"?"style='background-color:#343ac2 !important;padding-left:0 px !important;' ":null; ?>>
            <nav>

              <div class="nav toggle">
<?php if ($forgot != "yes") { ?>
					      <a id="menu_toggle"><i class="fa fa-bars"></i></a>
					<?php } ?>
              </div>
              <div class="date navbar-nav navbar-left" <?php echo $forgot == "yes"?"style='color:white !important;' ":null; ?> >
				            <?php echo app::format_date(date("Y-m-d H:i:s"), "id", "N");?>
              </div>


<?php if ($forgot != "yes") { ?>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					<?php $app['me']= app::unserialize64($_SESSION[$app['session']]);
						// print_r ($app['me']);
					if(isset($app['me']['photo']) && $app['me']['photo']!="" ){
							if(file_exists($app['data_lib_path'] ."/". $app['me']['photo'])){ ?>
							   <img src="<?php echo $app['data_lib'] .'/'. $app['me']['photo']; ?>" width="50" class="circle responsive-img" />
						<?php }else{ ?>
							   <img src="<?php echo $app['_static'] .'/noimage.png'; ?>" width="50" class="circle responsive-img" />
						<?php } ?>
					<?php }else{ ?>
							   <img src="<?php echo $app['_static'] .'/noimage.png'; ?>" width="50" class="circle responsive-img" />
					<?php } ?>
					<?php echo $app['me']['name']; ?>
					<span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">				  
				<?php if(admlib::acc('DSPL','preference')){ ?>
					<li><a href="<?php echo $app['webmin']; ?>/preference.do?act=view"><?php echo app::getliblang('preference'); ?></a></li>
				<?php } ?>
					<li><a href="<?php echo $app['webmin']; ?>/change.do&id=<?php echo $app['me']['id']; ?>"><?php echo app::getliblang('change_password'); ?></a></li>
					<?php if(!empty($app['me']['id_departement'])){ ?>
						<li><a href="<?php echo $app['webmin']; ?>/profile.do&id=<?php echo $app['me']['id']; ?>"><?php echo app::getliblang('profile'); ?></a></li>
					<?php } ?>
					<li><a href="<?php echo $app['webmin']; ?>/index.php?act=logout&id=<?php echo $app['me']['id'];?>"><?php echo app::getliblang('logout'); ?></a></li>
                  </ul>
                </li>
				
				<?php //if(admlib::acc('APPR','complaint')){ ?>
				<li >		 
					<!--<nav class="navbar navbar-default">-->
					<nav class="navbar">
						<div id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<div id="sound"></div>
							<a href="javascript:;" class="dropdown-toggle1" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<center>
								<span class="fa fa-bell-o" id="fa_notif" style="font-size: 20px;"></span>
								<span id="badge_notif">&nbsp;(<b>0</b>)</span>&nbsp;
								<span class=" fa fa-angle-down"></span>
								</center>
							</a>
							<ul class="dropdown-menu notify-drop " id="box" style="width:360px;">
								<div class="notify-drop-title">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-6">
										<h6 class="dropdown-header">New Notification:</h6></div>
									</div>	
								</div>
								<!-- end notify title -->
								<!-- notify content -->
								<div class="drop-content"  id="nav_notification">
									<li class="dropdown-item">
										<h6 class="dropdown-header"><div class="col-md-6 col-sm-6 col-xs-6"><font align="center">No data</font></div></h6>
									</li>
								</div>
								
							</ul>
						</li>
				<?php //} ?>
              </ul>
			  </div>
            </nav>
			</li>
		</ul>
<?php } ?>
		</nav>
     </div>


</div>
        <!-- /top navigation -->