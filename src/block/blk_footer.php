		<!-- footer content -->
		<?php $module = admlib::$page_active['module']; ?>
		<?php $system = admlib::$page_active['system']; ?>
        <footer>
          <div class="pull-right">
            Content Management System by <a href="https://samdesign.com">SAM CGI</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <script src="<?php echo $app['_scripts']; ?>/custom.js?_v=<?php echo rand(1,1000);?>"></script>
	<script type="text/javascript">
		var errorElement = document.getElementsByClassName("error-style");
		for (var i = 0; i < errorElement.length; i++) {
			var parents = errorElement[i].parentNode;
			var errorText = errorElement[i].getAttribute("data-error");
			console.log(errorText);
			var createErrorText = document.createElement('i');
			createErrorText.innerHTML = errorText;
			parents.appendChild(createErrorText);
		}
		function playsound(){
			document.getElementById("sound").innerHTML='<audio autoplay="autoplay"><source src="<?php echo $app['webmin_http'] ."/src/assets/sound/"?>3.mp3" type="audio/mpeg" /><source src="<?php echo $app['webmin_http'] ."/src/assets/sound/"?>3.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="<?php echo $app['webmin_http'] ."/src/assets/sound/"?>3.mp3" /></audio>';
		}
		function notification(){ 
			$("#fa_notif").removeClass('faa-shake animated');
			$("#fa_notif").click
			$.ajax({
				// url: "<?php echo $app['http']?>/notification.do?module=<?= $module ?>",	
				url: "<?php echo $app['http']?>/notification.do?module=<?= (empty($module)?null:$module) ?>",	
				// type     : 'POST',
				// dataType : 'html',
				// data     : 'act=klik',
				success: function(result){
					var obj = jQuery.parseJSON(result);
					console.log(obj);
					// alert("<?php echo $app['http']?>/notification.do?module=<?= $module ?>");
					if(obj['badge'] > 0){
						playsound();
						$("#fa_notif").addClass('faa-shake animated');
					}
				$("#badge_notif").html('&nbsp;(<b>'+obj['badge']+'</b>)');
				$("#nav_notification").html(obj['data']);
			}
		});
		}
		notification();
/*		setInterval(function(){ 
			notification();		
		}, 5000);*/
	</script>
   
  </body>
</html>
