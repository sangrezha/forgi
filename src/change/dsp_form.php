<?php
$html = '<div class="notif-msg"></div>';
admlib::display_block_header();
	admlib::get_component('formstart');
		// echo $html;
		admlib::get_component('inputtext',
			array(
				"name"=>"username",
				"value"=>app::ov($form['username']),
				"disabled"=>true,
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext',
			array(
				"name"=>"username",
				"type"=>"hidden",
				"value"=>app::ov($form['username']),	
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext',
			array(
				"type"=>"password",
				"name"=>"password",
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext',
			array(
				"type"=>"password",
				"name"=>"new_password",
				"validate"=>"required"
			)
		);
		admlib::get_component('inputtext',
			array(
				"type"=>"password",
				"name"=>"re_password",
				"validate"=>"required"
			)
		);
		admlib::get_component('submit',
			array(
				"id"=>(isset($id)?$id:""),
				"act"=>$act
			)
		);
	admlib::get_component('formend');
?>
<!-- <script type="text/javascript">
	$(document).ready(function() {
		<?php if( $success ){ ?>
			$(".notif-msg").html('<?php echo msg::display_notif_msg("success", app::getliblang('success')." ".$success ) ?>');
		<?php } ?>
		<?php if( $error ){ ?>
			$(".notif-msg").html('<?php echo msg::display_notif_msg("danger", app::getliblang('error')." ".$error ) ?>');
		<?php } ?>
	});
</script> -->
<?php
admlib::display_block_footer();
?>
