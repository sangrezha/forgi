	<?php
/*******************************************************************************
* Filename : index.php
* Description : change modul
*******************************************************************************/
include "../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');
## START #######################################################################
app::set_default($act, 'change');
app::set_default($step, 1);
db::connect();
admlib::$page_active = array('system'=>'change','caption'=>'Change Account');
/*******************************************************************************
* Action : change
*******************************************************************************/
if ($act == "change"):
	admlib::validate();
	form::init();
	if ($step == 1):
//echo "disini";
		//print_r($app['me']);
//get_record_view
		//$form = db::get_record('user', 'id', $app['me']['id']);
		$form = db::get_record_view('user', 'id', $app['me']['id']);
//print_r($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
	// echo "username='".$p_username."' AND passwordhash='".md5(serialize($p_password))."' ";exit;
		form::serialize_form();
		form::validate('empty', 'p_password,p_new_password,p_re_password');
//echo db::viewlookup('passwordhash', 'user', "username='".$p_username."' AND passwordhash='".md5(serialize($p_password))."' ");
//exit;
		if( !db::viewlookup('password', 'user', "username='".$p_username."' AND password='".md5(serialize($p_password))."' ") )
		{
			msg::set_message('error', app::getliblang('password_invalid'));
			msg::set_message('password', app::getliblang('password_invalid'));
			form::set_error(1);
		}
		if ($p_new_password != $p_re_password)
		{
			msg::set_message('error', app::getliblang('password_not_match'));
			msg::set_message('re_password', app::getliblang('password_not_match'));
			msg::set_message('new_password', " ");
			form::set_error(1);
		}
		if (form::is_error())
		{
			msg::build_msg();
			header("location: ". $app['webmin'] ."/". admlib::$page_active['system'] .".do&act=change&error=1&id=" . $id);
			exit;
		}
		if($p_new_password)
		{
			$passwordhash = md5(serialize($p_new_password));
			if($app['me']['level']=="-"){
				$sql = "update ". $app['table']['user'] ."
					set passwordhash='$passwordhash'
					where id='". $id ."'";
			}else{
				$sql = "update ". $app['table']['member'] ."
					set password='$passwordhash'
					where id='". $id ."'";
			}
//echo $sql;
//exit;
			db::qry($sql);
		}
		msg::set_message('success', app::getliblang('update'));
		msg::build_msg(1);
		header("location: ". $app['webmin'] ."/". admlib::$page_active['system'] .".do");
	endif;
endif;
?>
