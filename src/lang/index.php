<?php

/*******************************************************************************
* Filename : index.php
* Description : page modul
*******************************************************************************/

include "../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');
## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();

db::qry("SET sql_mode = ''");
admlib::$page_active = array('system'=>'lang','caption'=>'Language');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
	admlib::validate('DSPL');
	admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'lang', '1=1');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = null;
	if($ref)
	{
		$q = "WHERE a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.*,b.name as postby,c.name as modifyby FROM ". $app['table']['lang'] ." a
	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['table']['user'] ." c ON (a.updated_by=c.id) $q ORDER BY a.created_at DESC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['system'] .".do");
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['name'];
	while($row = db::fetch($rs['row']))
	{
		$results[] = $row;
	}
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
	    include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_alias,p_name');

		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=add&error=1");
			exit;
		endif;
		$data['image'] = "";
		if ($p_image_size > 0):
			$data['image'] = file::save_picture('p_image', $app['data_path']."/lang", 'lang');
		endif;
		$id = rand(1, 100).date("dmYHis");
		app::mq_encode('p_alias,p_name');
		$sql = "insert into ".$app['table']['lang']."
				(id, alias, name, image, created_by, created_at) values
				('$id', '$p_alias', '$p_name', '". $data['image'] ."', '". $app['me']['id'] ."', now())";
		db::qry($sql);
		msg::set_message('success', app::getliblang('create'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("lang", "id", $id);
		form::populate($form);
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_alias,p_name');

		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$data = db::get_record("lang", "id", $id);
		if ($p_image_del):
			@unlink($app['data_path'] ."/lang/". $data['image']);
			$data['image'] = null;
		else:
			if ($p_image_size > 0):
			$file = file::save_picture('p_image', $app['data_path'] ."/lang", 'lang');
				if (!$err_image):
					@unlink($app['data_path'] ."/lang/". $data['image']);
					$data['image'] = $file;
				endif;
			endif;
		endif;
		app::mq_encode('p_alias,p_name');
		$sql = "update ". $app['table']['lang'] ."
				set alias = '$p_alias',
					name = '$p_name',
					image = '". $data['image'] ."',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		db::qry($sql);
		msg::set_message('success', app::getliblang('modify'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['lang']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
endif;
/*******************************************************************************
* Action : del
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
		$items = implode("','", $p_del);
		$sql = "SELECT id,name as title FROM ". $app['table']['lang'] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);
		$sql 	= "DELETE FROM ". $app['table']['lang'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql);
		msg::set_message('success', app::getliblang('delete'));
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table']['lang'] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$id 		= rand(1, 100).date("dmYHis");
			$p_alias 	= $row['alias'];
			$p_name 	= $row['name'];

			app::mq_encode('p_name');
			$sql = "INSERT INTO ". $app['table']['lang'] ."
				(id, alias, name, status, created_by, created_at) values
				('$id', '$p_alias', '$p_name', 'inactive', '". $app['me']['id'] ."', now())";
			db::qry($sql);

		}
		msg::set_message('success', app::getliblang('create'));
	}
	else
	{
		msg::set_message('error', app::getliblang('notfound'));
	}
	header("location: ". admlib::$page_active['system'] .".do");
	exit;
endif;
?>
