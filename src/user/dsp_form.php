<?php
admlib::display_block_header();
admlib::get_component('uploadlib');
admlib::get_component('formstart');
		if(count($error) > 0)
		{
			foreach($error as $err){
				if(isset($err) AND $err){
					echo msg::display_notif_msg("error", app::getliblang('error')." ". $err );
				}
			}
		}
		
	 $category = "select id, ".db::dec("name")." AS category 
				from ". $app['table']['category'] ."  ";
	db::query($category, $rs['category'],$nr['category']);
	?>
	<div class="row">
		<div class="col-md-5">
			<?php
				admlib::get_component('inputtext',
					array(
						"name"=>"username",
						"value"=>app::ov($form['username']),
						"validate"=>"required"
					)
				);
				if(!isset($id))
				{
					admlib::get_component('inputtext',
						array(
							"type"=>"password",
							"name"=>"password",
							"validate"=>"required"
						)
					);
					admlib::get_component('inputtext',
						array(
							"type"=>"password",
							"name"=>"repassword",
							"validate"=>"required"
						)
					);
				}
				admlib::get_component('inputtext',
					array(
						"name"=>"name",
						"value"=>app::ov($form['name']),
						"validate"=>"required"
					)
				);
				admlib::get_component('inputtext',
					array(
						"name"=>"email",
						"value"=>app::ov($form['email']),
						"validate"=>"required"
					)
				);
		        // $image = null;
		        // if($form['photo'] && file_exists($app['data_path'] ."/users/". $form['photo']))
		        // {
		        //     $image = $app['_imgs'] ."/users/". $form['photo'];
		        // }
				// admlib::get_component('inputupload',
				// 	array(
				// 		"name"=>"image",
				// 		"value"=> $image,
				// 		"validate"=>"validate",
				// 		"category"=>"image",
				// 		"size"=>"330px x 385px",
				// 		"accept"=>"jpg|png|jpeg",
				// 	)
				// );
				admlib::get_component('inputupload',
					array(
						"name"=>"image",
						// "value"=> (isset($form['image']) AND file_exists($app['data_lib_path'] ."/thumb/". $form['image']))?'/'. app::ov($form['image']):null,
						"value"=> $form['photo'],
						"validate"=>"",
						"size"=>"1366px x 442px",
						"filemedia"=>true
					)
				);
				?>
				</hr>
<!-- 				<div class="col-md-12 col-sm-12 col-xs-12">
				<label for="allow">Complaint Privilege :		</label>

				<hr>
				<div class="checkbox">
						<label for="allow">
						<?php $x=1; 
							while ($row=db::fetch($rs['category'])){
							?>
						<label><input id="category<?php echo $x;?>" name="p_category[]" type="checkbox"  onchange="checkAll()" value="<?php echo $row['id']; ?>" <?php echo ($row['id']==$form['category'][$row['id']]?'checked':null) ?>><?php echo $row['category']; ?></label>
						<br>
						<label><input id="notif<?php echo $x;?>" name="p_notif[<?php echo $row['id']; ?>]" type="checkbox" value="yes" <?php echo ('yes'==$form['notif'][$row['id']]?'checked':'disabled="disabled"') ?> >Email Notification</label>
						<br>
						<br>
						
					<?php $x++; } ?>
					</label>
				</div>
				</div> -->
		
<?php
				admlib::get_component('language',
					array(
						"name"=>"lang",
						"value"=>app::ov($form['lang']),
						"validate"=>"required"
					)
				);
				admlib::get_component('submit',
					array(
						"id"=>isset($id)?$id:null,
						"act"=>$act
					)
				);
		?>
		</div>
			
		<div class="col-md-7">
			<?php
				$sql_rule = "SELECT id,name FROM ". $app['table']['rule'] ." WHERE status='active' ORDER BY created_at DESC";
				db::query($sql_rule, $rs['rule'], $nr['rule']);
				$num = 0;
				while ($rowrule = db::fetch($rs['rule'])) {
					$rtotal = str_replace(" ","",$rowrule['name']);
					$sql_rule_det = "SELECT id,module,rule FROM ". $app['table']['rule_det'] ." WHERE id_rule='". $rowrule['id'] ."' GROUP BY module ORDER BY module";
					db::query($sql_rule_det, $rs['rule_det'], $nr['rule_det']);
			?>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#<?php echo $rtotal; ?>" href="#<?php echo $rtotal; ?>" aria-expanded="true" aria-controls="collapseOne">
									<?php echo $rowrule['name']; ?>
									</a>
									<?php
										$chk = null;
										if(isset($id))
										{
											$chk = db::lookup('id_rule', 'user_det', 'id_user="'. $id .'" AND id_rule="'. $rowrule['id'] .'"');
										}
									?>
									<input type="checkbox" class="pull-right" name="p_rule[]" value="<?php echo $rowrule['id']; ?>" <?php echo ($chk==$rowrule['id']?"checked":null); ?>/>
								</h4>
							</div>
							<div id="<?php echo $rtotal; ?>" class="panel-collapse collapse in" role="tabpanel">
								<ul class="list-group">
									<?php while ($rowruledet = db::fetch($rs['rule_det'])) { ?>
										<li class="list-group-item">
											<?php
												echo "<b>" . $rowruledet['module'] ."</b>";
												echo "<br/>";
												echo " [ ";
												$sql_rule_det2 = "SELECT id,module,rule FROM ". $app['table']['rule_det'] ." WHERE module='". $rowruledet['module'] ."' AND id_rule='". $rowrule['id'] ."' ORDER BY module";
												db::query($sql_rule_det2, $rs['rule_det2'], $nr['rule_det2']);
												$rule = null;
												while ($rowruledet2 = db::fetch($rs['rule_det2'])) {
													$rule .= (isset($app['rule'][$rowruledet2['rule']])?$app['rule'][$rowruledet2['rule']]:$rowruledet2['rule']) ." | ";
												}
												echo substr($rule, 0, -2);
												echo " ] ";
											?>
										</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
			<?php } ?>
		</div>
		</div>
<?php
admlib::get_component('formend');
admlib::display_block_footer();
?>
<script>
// $(function () {
        // $("#chkPassport").click(function () {
            // if ($(this).is(":checked")) {
                // $("#txtPassportNumber").removeAttr("disabled");
                // $("#txtPassportNumber").focus();
            // } else {
                // $("#txtPassportNumber").attr("disabled", "disabled");
            // }
        // });
    // });
	
 function checkAll() {
	var i;
	for (i=1;i<=2;i++){
		// console.log(i + ". Saya sedang belajar JavaScript");
		if (document.getElementById('category'+i).checked) {
		document.getElementById("notif"+i).disabled = false;
		} else {
		document.getElementById("notif"+i).disabled = true;
		document.getElementById("notif"+i).checked = false;
		}
	}
}


</script>
