<?php
/*******************************************************************************
* Filename : index.php
* Description : user modul
*******************************************************************************/
include "../../application.php";
app::load_lib('url', 'msg', 'form', 'nav', 'file', 'admlib');

## START #######################################################################
app::set_default($act, 'browse');
app::set_default($step, 1);
db::connect();
db::qry("SET sql_mode = ''");
admlib::$page_active = array('system'=>'user','caption'=>'User');
/*******************************************************************************
* Action : browse
*******************************************************************************/
if ($act == "browse"):
		admlib::validate('DSPL');
		admlib::display_block_grid();
    exit;
endif;
/*******************************************************************************
* Action : data
*******************************************************************************/
if ($act == "data"):
	admlib::validate('DSPL');
	$total = db::lookup('COUNT(id)', 'user', 'id!="1"');
	app::set_default($page_size, (isset($all)?$total:10));
	$q = "WHERE a.id != '1'";
	if($ref)
	{
		$q = "WHERE a.id != '1' AND a.name LIKE '%". $ref ."%'";
	}
	$sql = "SELECT a.*,b.name as created_by,c.name as update_by,d.name as lang FROM ". $app['table']['user'] ." a
	LEFT JOIN ". $app['table']['user'] ." b ON (a.created_by=b.id)
	LEFT JOIN ". $app['table']['user'] ." c ON (a.update_by=c.id)
	LEFT JOIN ". $app['table']['lang'] ." d ON (a.lang=d.id)
	$q ORDER BY a.created_at DESC";
	app::set_navigator($sql, $nav, $page_size, admlib::$page_active['system'] .".do&act=". $act);
	db::query($sql, $rs['row'], $nr['row']);
	$results = [];
	$columns = ['name'];
	// $numlang = db::count_record('id', 'lang', 'WHERE status="active"');
	// if($numlang > 1)
	// {
	// 	$columns = array_merge($columns,['lang']);
	// }
	while($row = db::fetch($rs['row']))
	{
		$results[] = $row;
	}
	include $app['pwebmin'] ."/include/blk_list.php";
	exit;
endif;
/*******************************************************************************
* Action : add
*******************************************************************************/
if ($act == "add"):
	admlib::validate('CRT');
	form::init();
	if ($step == 1):
		form::populate($form);
	    include "dsp_form.php";
		exit;
	endif;

	if ($step == 2):
		form::serialize_form();
		form::validate('empty','p_name','p_username','p_password','p_email');
		 form::validate('email', "p_email");
		## check duplicate username
		$row = db::get_record('user', 'username', $p_username);
		if ($row['username']):
		// echo "asdasdasd";exit;
			msg::set_message('uname_exists',app::getliblang('username_exists'));
			form::set_error(1);
		endif;
		if ($p_password != $p_repassword):
			msg::set_message('notmatch', app::getliblang('password_not_match'));
			form::set_error(1);
		endif;
		if( count($p_rule) == 0 )
		{
			msg::set_message('rule_empty', app::getliblang('rule_empty'));
			form::set_error(1);
		}
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=add&error=1");
			exit;
		endif;
		// if ($p_image_size > 0):
			// $data['photo'] = file::save_picture('p_image', $app['data_path']."/user", 'usr');
		// endif;
	$passwordhash = md5(serialize($p_password));
	
	$notif = array();
	if ( count($p_category) > 0 ):
		foreach($p_category as $val){
			$p_privilege=($p_notif[$val]=="yes"?"yes":"no");
			$notif[] = $val.",".$p_privilege;
			
		}
	endif;
	$notif = implode("|",$notif);
	// echo $notif;
	// print_r($_POST);exit;
	$id = rand(1, 100).date("dmYHis");
	app::mq_encode('p_name,p_username,p_notif');
	 $sql = "insert into ".$app['table']['user']."
				(id, username, passwordhash, name,email,photo,privilege, lang, created_by, created_at) values
				('$id','$p_username','$passwordhash','$p_name', '$p_email','$p_image','$notif', '$p_lang','". $app['me']['id'] ."', now())";
	// exit;	
		db::qry($sql);
// Tambahan
		if(count($p_rule) > 0)
		{
			foreach($p_rule as $rule)
			{
				$id_det = rand(1, 100).date("dmYHis");
				$sql_det = "insert into ". $app['table']['user_det'] ."
					(id, id_user, id_rule) values
					('$id_det', '$id', '$rule')";	
				db::qry($sql_det);
			}
		}
// /tambahan
	// $sql 1= "insert into ".$app['table']['karyawan'].""
		// exit;
		msg::set_message('success', app::getliblang('create'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : modify
*******************************************************************************/
if ($act == "edit"):
	admlib::validate('UPDT');
	form::init();
	if ($step == 1):
		$form = db::get_record("user", "id", $id);
		$data = explode("|",$form['privilege']);
		// print_r($data);
		foreach($data as $val){
			$hasil = explode(",",$val);
			$form['notif'][$hasil[0]] = $hasil[1];
			$form['category'][$hasil[0]] = $hasil[0];
		}

		// echo $form['privilege']; exit;
		// print_r($form);
		form::populate($form);
		$error = [
			msg::get_message('uname_exists'),
			msg::get_message('notmatch'),
			msg::get_message('rule_empty')
		];
		include "dsp_form.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		form::validate('empty', 'p_username,p_name,p_lang,p_email');
		form::validate('email', "p_email");
		// print_r($_POST);
		// exit;
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=edit&error=1&id=" . $id);
			exit;
		endif;
		$data = db::get_record("user", "id", $id);
		// if ($p_image_del):
			// @unlink($app['data_path'] ."/users/". $data['photo']);
			// $data['photo'] = null;
		// else:
			// if ($p_image_size > 0):
			// $file = file::save_picture('p_image', $app['data_path'] ."/users", 'usr');
				// if (!$err_image):
					// @unlink($app['data_path'] ."/users/". $data['photo']);
					// $data['photo'] = $file;
				// endif;
			// endif;
		// endif;
		$notif = array();
		if ( count($p_category) > 0 ):
			foreach($p_category as $val){
				$p_privilege=($p_notif[$val]=="yes"?"yes":"no");
				$notif[] = $val.",".$p_privilege;	
			}
		endif;
		$notif = implode("|",$notif);
		// echo $notif;
		// print_r($_POST);exit;
		
		app::mq_encode('p_name,p_username');
		 $sql = "update ".$app['table']['user']."
				set name = '$p_name',
				    username = '$p_username',
					email = '$p_email',
					lang = '$p_lang',
					photo  = '$p_image',
					privilege = '$notif',
					update_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		db::qry($sql);
		if(count($p_rule) > 0)
		{
			$sql_del = "DELETE FROM ". $app['table']['user_det'] ." WHERE id_user='$id'";
			db::qry($sql_del);
			foreach($p_rule as $rule)
			{
				$id_det = rand(1, 100).date("dmYHis");
				$sql_det = "insert into ". $app['table']['user_det'] ."
					(id, id_user, id_rule) values
					('$id_det', '$id', '$rule')";
				db::qry($sql_det);
			}
		}
		// $sql = "SELECT * FROM " .$app['table']['user'] ." WHERE id='$id'";
		// db::query($sql, $rs['login'], $nr['login']);
		 // $_SESSION[$app['session']] = app::serialize64(db::fetch($rs['login']));
		if(count($p_rule) > 0)
		{
			$sql_del = "DELETE FROM ". $app['table']['user_det'] ." WHERE id_user='$id'";
			db::qry($sql_del);
			foreach($p_rule as $rule)
			{
				$id_det = rand(1, 100).rand(1, 100).date("dmYHis");
				$sql_det = "insert into ". $app['table']['user_det'] ."
					(id, id_user, id_rule) values
					('$id_det', '$id', '$rule')";
				db::qry($sql_det);
			}
		}
		// ## am i updated ? if yes then update the session
		if ($id == $app['me']['id']):
			$user = db::get_record("user", "id", $id);
			$_SESSION[$app['session']] = app::serialize64($user);
		endif;
		msg::set_message('success', app::getliblang('update'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do");
		exit;
	endif;
endif;
/*******************************************************************************
* Action : profile
*******************************************************************************/
if ($act == "profile"):
	admlib::validate();
	form::init();
	if ($step == 1):
		$rs['divisi'] = db::get_record_select("id, name","divisi","1='1'");
		$rs['jabatan'] = db::get_record_select("id, name","jabatan","1='1'");

		$form = db::get_record("user", "id", $id);
		$form1 = db::get_record("karyawan", "id", $id);

		form::populate($form);
		$form1['name'] = $form['id_divisi'];
		$form1['name'] = $form['id_jabatan'];

		admlib::$page_active = array('system'=>'user','caption'=>'Profile');
		$success = msg::get_message('success');
		include "dsp_profile.php";
		exit;
	endif;
	if ($step == 2):
		form::serialize_form();
		$form1 = db::get_record("karyawan", "id", $id);
		$user = db::get_record("user", "id", $id);
		if (form::is_error()):
			msg::build_msg();
			header("location: ". admlib::$page_active['system'] .".do&act=profile&error=1&id=" . $id);
			exit;
		endif;

		$data = db::get_record("user", "id", $id);

		app::mq_encode('p_name,p_username','p_email');
		$sql = "update ".$app['table']['user']."
				set name = '$p_name',
					email = '$p_email',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
		// echo $sql;exit;
		db::qry($sql);

		app::mq_encode('p_name','p_address','p_birthdate','p_email','p_email_company','p_email_work','p_phone');
		// print_r($_POST);
		$sql1 = "update ". $app['table']['karyawan'] ."
				set name = '$p_name',
					address = '$p_address',
					birthdate = '$p_birthdate',
					email = '$p_email',
					email_company = '$p_email_company',
					email_work = '$p_email_work',
					phone = '$p_phone',
					updated_by = '". $app['me']['id'] ."',
					updated_at = now()
				where id = '$id'";
				// exit;
		db::qry($sql1);

		## am i updated ? if yes then update the session
		if ($id == $app['me']['id']):
			$user = db::get_record("user", "id", $id);
			$_SESSION['adminsession'] = app::serialize64($user);
		endif;
		msg::set_message('success', app::getliblang('update'));
		msg::build_msg(1);
		header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do&act=profile&id=" . $id);
		exit;
	endif;
endif;
/*******************************************************************************
* Action : set_status
*******************************************************************************/
if ($act == "status"):
	admlib::validate('UPDT');
	if ( $status == "active" ):
		$statusnya = "inactive";
	elseif ( $status == "inactive" ):
		$statusnya = "active";
	endif;
	$sql = "UPDATE ".$app['table']['user']."
			SET status = '$statusnya'
			WHERE id = '$id'";
	db::qry($sql);
	msg::set_message('success', app::getliblang('update'));
	msg::build_msg(1);
	header("location: " . $app['webmin'] ."/". admlib::$page_active['system'] .".do&act=profile&id=" . $id);
endif;
/*******************************************************************************
* Action : del
*******************************************************************************/
if ($act == "delete"):
	admlib::validate('DEL');
	if($step == 1)
	{
		$items = implode("','", $p_del);
		$sql = "SELECT id,name as title FROM ". $app['table']['user'] ." WHERE id IN ('". $items ."')";
		db::query($sql, $rs['row'], $nr['row']);
		include $app['pwebmin'] ."/include/blk_delete.php";
	}
	elseif($step == 2)
	{
		$delid 	= implode("','", $p_id);
		$sql = "SELECT photo FROM ". $app['table']['user'] ." WHERE id IN ('". $delid ."')";
		db::query($sql, $rs['row'], $nr['row']);
		while($row=db::fetch($rs['row']))
		{
			@unlink($app['data_path'] .'/users/'. $row['photo']);
		}

		$sql_user_det	= "DELETE FROM ". $app['table']['user_det'] ." WHERE `id_user` IN ('". $delid ."')";
		db::qry($sql_user_det);
		$sql_delete 	= "DELETE FROM ". $app['table']['user'] ." WHERE `id` IN ('". $delid ."')";
		db::qry($sql_delete);
		// echo $sql_user_det;exit;
		msg::set_message('success', app::getliblang('delete'));
		msg::build_msg(1);
		header("location: ". admlib::$page_active['system'] .".do");
		exit;
	}
endif;
if ($act == "duplicate"):
	admlib::validate('CRT');
	form::init();
	if( count($p_del) > 0 )
	{
		$items = implode("','", $p_del);
	}
	else
	{
		$items = $id;
	}
	$sql = "SELECT * FROM ". $app['table']['user'] ." WHERE id IN ('". $items ."')";
	db::query($sql, $rs['row'], $nr['row']);
	if($nr['row'] > 0)
	{
		while($row = db::fetch($rs['row']))
		{
			$id 			= rand(1, 100).date("dmYHis");
			$p_lang 		= $row['lang'];
			$p_name 		= $row['name'];
			$p_email 		= $row['email'];

			app::mq_encode('p_name');
			$sql_insert = "INSERT INTO ". $app['table']['user'] ."
				(id, name, email, lang, status, created_by, created_at) values
				('$id', '$p_name', '$p_email', '$p_lang', 'inactive', '". $app['me']['id'] ."', now())";
			db::qry($sql_insert);

			$sql_det = "SELECT * FROM ". $app['table']['user_det'] ." WHERE id_user='". $row['id'] ."'";
			db::query($sql_det, $rs['row_det'], $nr['row_det']);
			if( $nr['row_det'] > 0 )
			{
				$num = 0;
				while($rowdet = db::fetch($rs['row_det']))
				{ $num++;
					$iddet = $num.rand(1, 100).date("dmYHis");
					$sql_insert_det = "INSERT INTO ". $app['table']['user_det'] ."
						(id, id_user, id_rule) values
						('$iddet', '". $id ."', '". $rowdet['id_rule'] ."')";
					db::qry($sql_insert_det);
				}
			}
		}
		msg::set_message('success', app::getliblang('create'));
	}
	else
	{
		msg::set_message('error', app::getliblang('notfound'));
	}
	header("location: ". admlib::$page_active['system'] .".do");
	exit;
endif;
?>
