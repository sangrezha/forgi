<?php
include "application.php";
db::connect();
db::qry("SET sql_mode = ''");
app::set_default($act, "");

app::load_lib('url', 'msg', 'form', 'usrlib', 'file', 'parse');
if (isset($act) && $act=="logout"):
	if(!empty($_SESSION[$app['session']])):
		$_SESSION[$app['session']] = array();
		unset($_SESSION[$app['session']]);
		msg::set_message('success', app::getliblang('logout'));
		msg::build_msg(1);
	endif;
   # header("location: ". $app['webmin']);
    header("location: ". $app['http']);
	exit;
endif;
if(isset($_SESSION[$app['session']]))
	 header("location: ". $app['webmin'] ."/dashboard.do");
else
	header("location: ". $app['webmin'] ."/auth.do");

$config = db::get_record("configuration", "id", 1);
if (empty($act)):
	form::populate($form);
	if(isset($_SESSION[$app['session']])){
		//header("location: ". $app['webmin'] ."/dashboard.do");
	}else{
		header("location: ". $app['webmin'] ."/auth.do");
	}
	exit;
endif;
header("location: ". $app['www'] ."/auth.do"); 
$config = db::get_record('configuration', 'id', 1);
?>
